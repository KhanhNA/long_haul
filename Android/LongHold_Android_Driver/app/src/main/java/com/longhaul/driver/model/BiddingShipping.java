package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingShipping extends TsBaseModel {
    String toDepotFromDepot;
    int typeBidding;
    BiddingInformation biddingInformation;
}
