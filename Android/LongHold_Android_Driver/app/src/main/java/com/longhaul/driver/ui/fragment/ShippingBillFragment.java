package com.longhaul.driver.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.longhaul.driver.R;
import com.longhaul.driver.base.BaseAdapterV3;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.ShippingBillFragmentBinding;
import com.longhaul.driver.model.BiddingShipping;
import com.longhaul.driver.ui.viewcommon.DialogConfirm;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.viewmodel.CustomLoadMoreView;
import com.longhaul.driver.viewmodel.ShippingBillVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.longhaul.driver.base.Constants.DATA_PASS_FRAGMENT;
import static com.longhaul.driver.base.Constants.KEY_PAGE;
import static com.longhaul.driver.base.Constants.OBJECT_SERIALIZABLE;
import static com.longhaul.driver.base.Constants.REQUEST_UPDATE;

public class ShippingBillFragment extends BaseFragment {
    private int mKeyPage;
    private ShippingBillFragmentBinding mShippingBillFragmentBinding;
    private ShippingBillVM mShippingBillVM;
    private BaseAdapterV3 mShippingBillAdapter;
    private int mOffset = 0;
    private String txtSearch = "";
    private int mStatus = 0;
    private DialogConfirm dialog;

    @Override
    public int getLayoutRes() {
        return R.layout.shipping_bill_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ShippingBillVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mShippingBillFragmentBinding = (ShippingBillFragmentBinding) binding;
        mShippingBillVM = (ShippingBillVM) viewModel;
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        eventChangeEdtSearch();
        return view;
    }

    private void initView() {
        mShippingBillFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mShippingBillAdapter = new BaseAdapterV3(R.layout.item_bid, mShippingBillVM.getBiddingShipping().getValue(), this);
        mShippingBillFragmentBinding.recyclerView.setAdapter(mShippingBillAdapter);
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mShippingBillAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mShippingBillAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mShippingBillAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mShippingBillFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mShippingBillFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }


    private void getData() {
        assert getArguments() != null;
        mKeyPage = getArguments().getInt(Constants.KEY_PAGE);
        switch (mKeyPage) {
            case EnumBidding.EnumBiddingBehavior.NOT_SHIPPING:
                mStatus = 0;
                break;
            case EnumBidding.EnumBiddingBehavior.SHIPPING:
                mStatus = 1;
                break;
            case EnumBidding.EnumBiddingBehavior.SHIPPING_DONE:
                mStatus = 2;
                break;
        }
        requestBiddingShipping(txtSearch, mOffset, mStatus);
    }

    private void requestBiddingShipping(String txt_search, int offset, int status) {
        mShippingBillVM.requestListBiddingOrderShipping(txt_search, offset, status, this::runUi);
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            mShippingBillAdapter.setList(mShippingBillVM.getBiddingShipping().getValue());
            mShippingBillAdapter.notifyDataSetChanged();
            mShippingBillFragmentBinding.recyclerView.invalidate();
            offRefreshing();
            emptyData(mShippingBillVM.getInformationBidding().getValue().getRecords());
        }
        if (action.equals(Constants.FAIL_API)) {
            offRefreshing();
            if (mShippingBillVM.getInformationBidding().getValue() == null) return;
            emptyData(mShippingBillVM.getInformationBidding().getValue().getRecords());

        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.viewGroup:
                BiddingShipping biddingShipping = (BiddingShipping) o;
                Bundle args = new Bundle();
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailNotShipFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                intent.putExtra(KEY_PAGE, mStatus);
                intent.putExtra(OBJECT_SERIALIZABLE, biddingShipping.getBiddingInformation().getId());
                startActivityForResult(intent, REQUEST_UPDATE);
                break;
            case R.id.btnCancel:
                String note = (((BiddingShipping) o).getBiddingInformation().getNote() != null ? ((BiddingShipping) o).getBiddingInformation().getNote() : "");
                dialog = new DialogConfirm(getString(R.string.reason_close), getString(R.string.btnConfirm), note, false, v1 -> dialog.dismiss());
                dialog.show(getFragmentManager(), "tag");
                break;
        }
    }

    private void offRefreshing() {
        mShippingBillFragmentBinding.swipeLayout.setRefreshing(false);
        mShippingBillAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void refreshData() {
        // refresh data
        mShippingBillVM.clearData();
        mOffset = 0;
        requestBiddingShipping(txtSearch, mOffset, mStatus);
    }

    private void loadMore() {
        int length = mShippingBillVM.getInformationBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            requestBiddingShipping(txtSearch, mOffset, mStatus);
        } else {
            //thông báo không còn gì để load
            mShippingBillAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_UPDATE) {
            if (resultCode == RESULT_OK) {
                String returnedResult = data.getDataString();
                if (returnedResult != null){
                    refreshData();
                }
                // OR
                // String returnedResult = data.getDataString();
            }
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnSearch:
                if (mShippingBillFragmentBinding.edtSearch.getText().toString().trim().isEmpty())
                    return;
                txtSearch = mShippingBillFragmentBinding.edtSearch.getText().toString().trim();
                refreshData();
                break;
        }
    }

    private void eventChangeEdtSearch() {
        mShippingBillFragmentBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    txtSearch = "";
                    refreshData();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mShippingBillFragmentBinding.emptyData.setVisibility(View.GONE);
            mShippingBillFragmentBinding.recyclerView.setVisibility(View.VISIBLE);
        } else {
            mShippingBillFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            mShippingBillFragmentBinding.recyclerView.setVisibility(View.GONE);
        }
    }

}
