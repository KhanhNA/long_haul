package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmBiddingResponse extends TsBaseModel {
    private boolean result;
    private List<Integer> list_cargo_invalid;
}
