package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ToDepot extends TsBaseModel {
    private Integer id;
    private String name;
    private String phone;
    private String depot_code;
    private Double latitude;
    private Double longitude;
    private String address;
    private String street;
    private String street2;
    private String cityName;

}