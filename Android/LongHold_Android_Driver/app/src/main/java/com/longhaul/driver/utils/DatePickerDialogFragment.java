package com.longhaul.driver.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import com.longhaul.driver.base.RunUi;

import java.util.Calendar;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private OnDateSelectedListener listener;

    public interface OnDateSelectedListener {
        void onDateSelected(int year, int month, int dayOfMonth);
    }

    private int mMinYear = 0;
    private int mMinMoth = 0;
    private int mMinDay = 0;
    private int mMaxYear = 0;
    private int mMaxMoth = 0;
    private int mMaxDay = 0;


    public DatePickerDialogFragment(int minYear, int minMoth, int minDay, int maxYear, int maxMoth, int maxDay, RunUi runUi) {
        this.mMinYear = minYear;
        this.mMinMoth = minMoth;
        this.mMinDay = minDay;
        this.mMaxDay = maxDay;
        this.mMaxMoth = maxMoth;
        this.mMaxYear = maxYear;
    }

    public DatePickerDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMaxDate(c.getTimeInMillis());

        if (mMinYear != 0 && mMinMoth != 0 && mMinDay != 0 && mMaxYear != 0 && mMaxMoth != 0 && mMaxDay != 0) {
            Calendar minDate = Calendar.getInstance();
            Calendar maxDate = Calendar.getInstance();
            minDate.set(mMinYear, mMinMoth, mMinDay);
            maxDate.set(mMaxYear, mMinMoth, mMaxDay);
            dialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
            dialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        }
        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (listener != null) listener.onDateSelected(year, month, dayOfMonth);
    }

    public void setOnDateSelectedListener(OnDateSelectedListener listener) {
        this.listener = listener;
    }
}