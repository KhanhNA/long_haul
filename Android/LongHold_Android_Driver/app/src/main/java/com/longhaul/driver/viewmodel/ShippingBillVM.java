package com.longhaul.driver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.model.BiddingInformation;
import com.longhaul.driver.model.BiddingShipping;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class ShippingBillVM extends BaseViewModel {

    public MutableLiveData<OdooResultDto<List<BiddingInformation>>> mInformationBidding = new MutableLiveData<>();
    private OdooResultDto<List<BiddingInformation>> mInformationUpdate;
    private MutableLiveData<List<BiddingShipping>> mBiddingShipping = new MutableLiveData<>();

    public ShippingBillVM(@NonNull Application application) {
        super(application);
        mInformationUpdate = new OdooResultDto<>();
        mInformationBidding.setValue(mInformationUpdate);
    }

    public MutableLiveData<OdooResultDto<List<BiddingInformation>>> getInformationBidding() {
        return mInformationBidding;
    }


    public MutableLiveData<List<BiddingShipping>> getBiddingShipping() {
        return mBiddingShipping;
    }

    public void requestListBiddingOrderShipping(String txt_search, int offset, int status, RunUi runUi) {
        DriverApi.getListBiddingOrderShipping(txt_search, offset, status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                OdooResultDto<List<BiddingInformation>> response = (OdooResultDto<List<BiddingInformation>>) o;


                if (mInformationBidding.getValue() != null && mInformationBidding.getValue().getRecords() != null) {
                    mInformationBidding.getValue().getRecords().addAll(response.getRecords());
                    mInformationBidding.getValue().setLength(response.getLength());
                } else mInformationBidding.setValue((OdooResultDto<List<BiddingInformation>>) o);
                /**
                 * tạo một mảng chứa các bid và title của bid
                 */
                ArrayList<BiddingShipping> shippingArrayList = new ArrayList<>();
                // lấy ra các list bid trong Records response
                for (int i = 0; i < mInformationBidding.getValue().getRecords().size(); i++) {
                    // lấy ra các bid của list con trong Records response
                    for (int j = 0; j < mInformationBidding.getValue().getRecords().get(i).size(); j++) {
                        // tạo ra các biddingShipping
                        BiddingShipping biddingShipping = new BiddingShipping();
                        BiddingInformation biddingInformation = mInformationBidding.getValue().getRecords().get(i).get(j);
                        // set toDepot vs FromDepot cho bid Shipping
                        if (j == 0)
                            biddingShipping.setToDepotFromDepot(biddingInformation.getFrom_depot().getDepot_code() + "-" + biddingInformation.getTo_depot().getDepot_code());
                        biddingShipping.setTypeBidding(status);
                        biddingShipping.setBiddingInformation(biddingInformation);
                        shippingArrayList.add(biddingShipping);
                    }
                    mBiddingShipping.setValue(shippingArrayList);
                }

                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void clearData() {
        mInformationBidding.setValue(null);
    }
}
