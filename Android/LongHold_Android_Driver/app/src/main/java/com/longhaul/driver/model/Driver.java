package com.longhaul.driver.model;

import android.annotation.SuppressLint;

import com.longhaul.driver.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver {
    private Long id;
    private String driver_code;
    private String name;
    private String display_name;
    private String full_name;
    private String address;
    private String phone;
    private String tz;
    private String lang;
    private Integer nationality;
    private String user_driver;
    private String ssn;
    private OdooDate birth_date;
    private OdooDate hire_date;
    private OdooDate leave_date;
    private Float average_rating;
    private Integer point;
    private String image_driver;

    private String class_driver;
    private OdooDate expries_date;
    private String no;
    private OdooDate driver_license_date;

    private AssignationLog assignation_log;
    private Vehicle vehicle;

    private Company company;


    private ParkingPoint parking_point;


    public String getHire_date_str(){
        if(this.hire_date != null){
            return AppController.formatDate.format(this.hire_date);
        }
        return "";
    }
    public String getBirth_date_str(){
        if(this.birth_date != null){
            return AppController.formatDate.format(this.birth_date);
        }
        return "";
    }
    @SuppressLint("DefaultLocale")
    public String getAverage_rating_str(){
        if(this.average_rating != null){
            return String.format("%.1f", average_rating);
        }
        return "";
    }

}