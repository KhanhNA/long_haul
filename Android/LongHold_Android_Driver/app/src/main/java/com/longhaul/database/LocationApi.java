package com.longhaul.database;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationApi extends TsBaseModel {
    String van_id = "21";
    String latitude;
    String longitude;
    String create_time;
}
