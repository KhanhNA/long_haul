package com.longhaul.driver.model;

import lombok.Getter;

@Getter
public class MessageEvent {
    private CargoTypes cargoTypes;

    public MessageEvent(CargoTypes cargoTypes) {
        this.cargoTypes = cargoTypes;
    }
}
