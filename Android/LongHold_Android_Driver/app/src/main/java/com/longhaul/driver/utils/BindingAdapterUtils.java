package com.longhaul.driver.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.longhaul.driver.R;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.StaticData;
import com.longhaul.driver.model.BiddingInformation;
import com.longhaul.driver.model.BiddingShipping;
import com.longhaul.driver.model.CargoTypes;
import com.longhaul.driver.model.NotificationModel;
import com.longhaul.driver.model.OrderDetail;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import java.text.MessageFormat;

public class BindingAdapterUtils {


    /**
     * set toDate->FromDate cho item Bidding
     *
     * @param view
     * @param fromTime
     * @param toTime
     */
    @BindingAdapter({"setTimeBidding", "toTime"})
    public static void setTimeBidding(TextView view, OdooDateTime fromTime, OdooDateTime toTime) {
        if (fromTime == null && toTime != null)
            view.setText(AppController.formatDateTime.format(toTime));
        if (fromTime != null && toTime == null)
            view.setText(AppController.formatDateTime.format(fromTime));
        if (fromTime != null && toTime != null)
            view.setText(AppController.formatDateTime.format(fromTime) + " - " + AppController.formatDateTime.format(toTime));
        if (fromTime == null && toTime == null)
            view.setText("");
    }

    @SuppressLint("NewApi")
    @BindingAdapter({"setInformationDepotBid", "object"})
    public static void setInformationDepotBid(TextView textView, String tile, Object object) {
        if (tile != null && !tile.isEmpty()) {
            if (object instanceof BiddingShipping) {
                BiddingShipping biddingShipping = (BiddingShipping) object;
                int statusBid = biddingShipping.getTypeBidding();
                String tyeBid = biddingShipping.getBiddingInformation().getBidding_vehicles().getBidding_order_receive().getStatus();
                if (statusBid == 2 && tyeBid.equals("-1")) {
                    String depotBid = tile + " - " + textView.getContext().getString(R.string.cancel_bid);
                    setSpanStringColor(textView, depotBid, textView.getContext().getString(R.string.cancel_bid), ContextCompat.getColor(textView.getContext(), R.color.red));
                } else textView.setText(tile);

            }
        } else textView.setVisibility(View.GONE);
    }

    private static void setSpanStringColor(TextView view, String fulltext, String subtext, int color) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE);
        Spannable str = (Spannable) view.getText();
        int i = fulltext.indexOf(subtext);
        str.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * set listIDdCargo adpter
     *
     * @param linearLayout
     * @param
     */
    @BindingAdapter({"setListIdCargo"})
    public static void setListIdCargo(LinearLayout linearLayout, Object o) {
        linearLayout.removeAllViews();
        if (o instanceof BiddingShipping) {
            BiddingShipping biddingShipping = (BiddingShipping) o;
            // case bidding chưa vận chuyển sẽ k có thông tin quality và type của bidding
            if (biddingShipping.getTypeBidding() == 0) return;
            // set số lượng quality và type của bidding
            BiddingInformation biddingInformation = biddingShipping.getBiddingInformation();
            for (int j = 0; j < biddingInformation.getBidding_vehicles().getCargo_types().size(); j++) {
                CargoTypes cargoType = biddingInformation.getBidding_vehicles().getCargo_types().get(j);
                addItemChill(linearLayout, cargoType);
            }
        }
    }

    private static void addItemChill(LinearLayout linearLayout, Object o) {
        String value = "";
        LayoutInflater mInflater = LayoutInflater.from(linearLayout.getContext());
        View newsListRow = mInflater.inflate(R.layout.item_total_cargo_info, null, false);
        TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);
        if (o instanceof CargoTypes) {
            CargoTypes cargoTypes = (CargoTypes) o;
            String quality = cargoTypes.getCargo_quantity() + " cargo";
            String type = cargoTypes.getType();
            value = quality + " " + type;
        }
        textView.setText(value);
        linearLayout.addView(newsListRow);
    }

    @BindingAdapter({"setStatusBidClose"})
    public static void setStatusBidClose(TextView textView, Object o) {
        if (o instanceof BiddingShipping) {
            BiddingShipping biddingShipping = (BiddingShipping) o;
            int statusBid = biddingShipping.getTypeBidding();
            String tyeBid = biddingShipping.getBiddingInformation().getBidding_vehicles().getBidding_order_receive().getStatus();
            if (statusBid == 2 && tyeBid.equals("-1"))
                textView.setVisibility(View.VISIBLE);
            else textView.setVisibility(View.GONE);

        }
    }

    // chiến
    @BindingAdapter({"setEnableReceiveButton"})
    public static void setEnableReceiveButton(TextView view, Boolean isNotChanged) {
        if (isNotChanged == null) {
            view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_btn_receive_goods_disable));
            view.setEnabled(false);
            return;
        }
        if (!isNotChanged) {
            view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_btn_receive_goods_enable));
            view.setEnabled(true);
        } else {
            view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_btn_receive_goods_disable));
            view.setEnabled(false);
        }
    }

    @BindingAdapter({"setColorTag"})
    public static void setColorTag(TextView view, CargoTypes cargoType) {
        if (cargoType.checked == null) {
            view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_item_tab_cargo_type));
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.color_text_tab_cargo_checked));
            view.setTypeface(null, Typeface.NORMAL);
            return;
        }
        if (cargoType.checked) {
            view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_item_tab_cargo_type));
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.color_text_tab_cargo_checked));
            view.setTypeface(null, Typeface.NORMAL);
        } else {
            view.setBackgroundColor(Color.WHITE);
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.color_text_tab_cargo_nomal));
            view.setTypeface(null, Typeface.BOLD);
        }
    }

    @BindingAdapter({"setTitleToolBarDetail"})
    public static void setTitleToolBarDetail(TextView view, String status) {
        if (status == null) {
            return;
        }
        if ("0".equals(status)) {
            view.setText(R.string.not_yet_shipped);
        } else if ("1".equals(status)) {
            view.setText(R.string.being_transported);
        } else if ("2".equals(status)) {
            view.setText(R.string.text_returns);
        } else if ("3".equals(status)) {
            view.setText(R.string.shipped_successfully);
        } else if ("4".equals(status)) {
            view.setText(R.string.order_is_canceled);
        }
    }

    @BindingAdapter({"setDescription"})
    public static void setDescription(TextView view, OrderDetail orderDetail) {
        if (orderDetail == null) {
            return;
        }
        String fromPhone = orderDetail.getFromDepot().getPhone() == null ? "" : " | " + orderDetail.getFromDepot().getPhone() + "";
        String fromReceiveTime = orderDetail.getFrom_receive_time() != null ? AppController.formatDateTime.format(orderDetail.getFrom_receive_time()) : "";
        String toReceiveTime = orderDetail.getTo_receive_time() != null ? AppController.formatDateTime.format(orderDetail.getTo_receive_time()) : "";
        view.setText(MessageFormat.format("Từ {0} - {1}\nKho  {2}{3} \n{4}", fromReceiveTime, toReceiveTime, orderDetail.getFromDepot().getName(), fromPhone, orderDetail.getFromDepot().getAddress()));
    }

    @BindingAdapter({"setFromTo"})
    public static void setFromTo(TextView view, OrderDetail orderDetail) {
        if (orderDetail == null) {
            return;
        }
        String toPhone = orderDetail.getToDepot().getPhone() == null ? " " : " | " + orderDetail.getToDepot().getPhone() + " ";
        String fromReturnTime = orderDetail.getFrom_return_time() != null ? AppController.formatDateTime.format(orderDetail.getFrom_return_time()) : "";
        String toReturnTime = orderDetail.getTo_return_time() != null ? AppController.formatDateTime.format(orderDetail.getTo_return_time()) : "";
        view.setText(MessageFormat.format("Từ {0} - {1}\nKho  {2}{3} \n{4}", fromReturnTime,toReturnTime, orderDetail.getToDepot().getName(), toPhone, orderDetail.getToDepot().getAddress()));
    }

    @BindingAdapter("setActualReceiveTime")
    public static void setActualReceiveTime(TextView view, OrderDetail orderDetail) {
        if (orderDetail == null) {
            return;
        }
        if (orderDetail.getBiddingVehicles().getBidding_order_receive().getActual_time() != null) {
            view.setText(AppController.formatDateTime.format(orderDetail.getBiddingVehicles().getBidding_order_receive().getActual_time()));
        }
    }

    @BindingAdapter("setActualReturnTime")
    public static void setActualReturnTime(TextView view, OrderDetail orderDetail) {
        if (orderDetail == null) {
            return;
        }
        if (orderDetail.getBiddingVehicles().getBidding_order_return().getActual_time() != null) {
            view.setText(AppController.formatDateTime.format(orderDetail.getBiddingVehicles().getBidding_order_return().getActual_time()));
        }
    }

    @BindingAdapter("setVisibilityActualTime")
    public static void setVisibilityActualTime(TextView view, int type) {
        if (type == EnumBidding.EnumStatusOrder.RETURNS || type == EnumBidding.EnumStatusOrder.SHIPPING_IS_SUCCESSFUL || type == EnumBidding.EnumStatusOrder.CANCEL_ORDER) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("setVisibilityButtonRemoveQR")
    public static void setVisibilityButtonRemoveQR(ImageButton view, boolean visibility) {
        if (view != null) {
            if (visibility) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    @SuppressLint("NewApi")
    @BindingAdapter({"setStatusNotification"})
    public static void setStatusNotification(TextView textView, NotificationModel notificationModel) {
        if (notificationModel.is_read())
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.hint_text_color));
        else textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.color_normal_text));
    }

    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, OdooDateTime time) {
        if (time != null) {
            if ((System.currentTimeMillis() - time.getTime()) < 60 * 1000) {
                view.setText(R.string.now);
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        time.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.not_img));
            return;
        }
        loadImage(view, imageUrl, view.getContext());
    }

    private static void loadImage(ImageView imgReward, String icon, Context context) {
        GlideUrl url = new GlideUrl(AppController.BASE_URL_IMAGE + icon, new LazyHeaders.Builder()
                .addHeader("Cookie", StaticData.sessionCookie)
                .build());
        Glide.with(context)
                .load(url)
                .error(R.drawable.not_img)
                .into(imgReward);
    }
    @SuppressLint("NewApi")
    @BindingAdapter({"setBackgronudNotification"})
    public static void setBackgronudNotification(ConstraintLayout view, NotificationModel notificationModel) {
        if (notificationModel.is_read())
            view.setBackgroundColor(view.getContext().getColor(R.color.colorWhite));
        else view.setBackgroundColor(view.getContext().getColor(R.color.backgroud_notifi));
    }
}

