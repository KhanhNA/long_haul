package com.longhaul.driver.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.longhaul.driver.utils.StringUtils.API_GOOGLE_MAP_DESTINATION;

public class LatLngUtils {
    static String mLocation;
    static List<Address> addressList = null;
    static Address address;
    static Context mContext;
    private static String strUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=";

    public static LatLng getLatLngLocation(Context context, String location) {
        mLocation = location;
        mContext = context;
        do {
            getGeocoder();
        } while (addressList == null);
        return new LatLng(address.getLatitude(), address.getLongitude());
    }
    public static List<Address> getFromLocationName(String locationName, int maxResults)  {

        String address;
        try {
            address = "https://maps.google.com/maps/api/geocode/json?address=" + URLEncoder.encode(locationName,
                    "UTF-8") + "&ka&sensor=false&key="+API_GOOGLE_MAP_DESTINATION;
            return getAddress(address, maxResults);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    private static List<Address> getAddress(String url, int maxResult) {
        List<Address> retList = null;

        Request request = new Request.Builder().url(url)
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Accept", "application/json; q=0.5")
                .build();
        try {
            Response response = new OkHttpClient().newCall(request).execute();
            String responseStr = response.body().string();
            JSONObject jsonObject = new JSONObject(responseStr);

            retList = new ArrayList<Address>();

            if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
                JSONArray results = jsonObject.getJSONArray("results");
                if (results.length() > 0) {
                    for (int i = 0; i < results.length() && i < maxResult; i++) {
                        JSONObject result = results.getJSONObject(i);
                        Address addr = new Address(Locale.getDefault());

                        JSONArray components = result.getJSONArray("address_components");
                        String streetNumber = "";
                        String route = "";
                        for (int a = 0; a < components.length(); a++) {
                            JSONObject component = components.getJSONObject(a);
                            JSONArray types = component.getJSONArray("types");
                            for (int j = 0; j < types.length(); j++) {
                                String type = types.getString(j);
                                if (type.equals("locality")) {
                                    addr.setLocality(component.getString("long_name"));
                                } else if (type.equals("street_number")) {
                                    streetNumber = component.getString("long_name");
                                } else if (type.equals("route")) {
                                    route = component.getString("long_name");
                                }
                            }
                        }
                        addr.setAddressLine(0, route + " " + streetNumber);

                        addr.setLatitude(
                                result.getJSONObject("geometry").getJSONObject("location").getDouble("lat"));
                        addr.setLongitude(
                                result.getJSONObject("geometry").getJSONObject("location").getDouble("lng"));
                        retList.add(addr);
                    }
                }
            }
        } catch (IOException e) {
            Log.e("TAG", "Error calling Google geocode webservice.", e);
        } catch (JSONException e) {
            Log.e("TAG", "Error parsing Google geocode webservice response.", e);
        }

        return retList;
    }

    public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context, Locale.getDefault());
        List<Address> address;
        LatLng p1 = null;
        if (Geocoder.isPresent()) {
            try {
                // May throw an IOException
                address = coder.getFromLocationName(strAddress, 1);
                if (address == null) {
                    return null;
                }

                Address location = address.get(0);
                p1 = new LatLng(location.getLatitude(), location.getLongitude());

            } catch (IOException ex) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                if (!getFromLocationName(strAddress, 1).isEmpty()){
                    Address addr = getFromLocationName(strAddress, 1).get(0);
                    if (addr != null){
                        return new LatLng(addr.getLatitude(), addr.getLongitude());
                    }
                }

            }

        } else {
            try {
                return new Progress(context).execute().get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return p1;
    }

    public static String getAddressFromLocation(Context context, double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);

        StringBuilder strAddress = new StringBuilder();

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append(" ");
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return strAddress.toString();

    }

    private static void getGeocoder() {
        Geocoder geocoder = new Geocoder(mContext);
        try {
            addressList = geocoder.getFromLocationName(mLocation, 5);
            address = addressList.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class Progress extends AsyncTask<String, Void, LatLng> {
        private ProgressDialog dialog;
        private Context context;

        public Progress(Context context) {

            Log.i("class progress", "constructor");
            this.context = context;
            dialog = new ProgressDialog(context);
        }

        protected void onPreExecute() {
            this.dialog.setMessage("Searching latitude and longitude");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(final LatLng lnl) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (lnl == null){

            }else {

            }
        }

        protected LatLng doInBackground(final String... args) {

            URLConnection urlConn;
            BufferedReader bufferedReader;
            JSONObject jsonobj;

            try {
                URL url = new URL(strUrl);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                StringBuilder stringBuffer = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
                jsonobj = new JSONObject(stringBuffer.toString());
                JSONArray results = jsonobj.getJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject c = results.getJSONObject(i);
                    JSONObject geometry = c.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");

                    double lat = location.getDouble("lat");
                    double lng = location.getDouble("lnl");
                    return new LatLng(lat, lng);
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
