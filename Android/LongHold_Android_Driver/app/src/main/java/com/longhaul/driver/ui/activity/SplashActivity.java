package com.longhaul.driver.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.android.volley.VolleyError;
import com.longhaul.driver.R;
import com.longhaul.driver.api.BaseApi;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.model.UserInfo;
import com.longhaul.driver.utils.ApiResponse;
import com.longhaul.driver.utils.LanguageUtils;
import com.longhaul.driver.utils.NetworkUtils;
import com.longhaul.driver.utils.StringUtils;
import com.longhaul.driver.viewmodel.LoginVM;
import com.longhaul.permission.PremissionProvider;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class SplashActivity extends BaseActivity {
    Bundle extras;
    PremissionProvider premissionProvider = new PremissionProvider();
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (premissionProvider.checkPermissionGPS(this)) {
            if (premissionProvider.checkLocationPermission(this)) {
                OdooClient client = new OdooClient(this, AppController.BASE_URL);
                onConnect(client, null);
            }
        }


    }

    private void startLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivityV2.class));
        // close splash activity
        finish();
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case SUCCESS:
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra(Constants.INTENT_NOTIFICATION, getIntent().getIntExtra(Constants.INTENT_NOTIFICATION, 0));
                intent.putExtra(Constants.EXTRA_DATA,getIntent().getSerializableExtra(Constants.EXTRA_DATA));
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                startLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public void onConnect(OdooClient odooV2, VolleyError volleyError) {
        LanguageUtils.loadLocale(SplashActivity.this);
        BaseApi.setOdoo(odooV2);
        LoginVM loginVM = new LoginVM(getApplication());
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        SharedPreferences preferences = AppController.getInstance().getSharePre();
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass) && NetworkUtils.isNetworkConnected(SplashActivity.this)) {
                loginVM.getModel().set(UserInfo.builder().userName(userName).passWord(pass).build());
                loginVM.requestLogin();
            } else {
                startLogin();
            }
        } else {
            startLogin();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 12) {
            if (premissionProvider.checkPermissionGPS(this)) {
                if (premissionProvider.checkLocationPermission(this)) {
                    OdooClient client = new OdooClient(this, AppController.BASE_URL);
                    onConnect(client, null);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        Log.e("duongnk", "onRequestPermissionsResult: ");
                        if (premissionProvider.checkPermissionGPS(this)) {
                            OdooClient client = new OdooClient(this, AppController.BASE_URL);
                            onConnect(client, null);
                        }
//                         LocationManager locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    premissionProvider.checkLocationPermission(this);
                }
                return;
            }

        }
    }
}
