package com.longhaul.driver.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.longhaul.driver.base.HaiSer;
import com.longhaul.driver.base.IModel;
import com.longhaul.driver.base.IResponse;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ns.odoolib_retrofit.wrapper.OArguments;
import com.ns.odoolib_retrofit.wrapper.OdooClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class BaseApi {
    protected static final String METHOD_CREATE = "create";
    protected static final String METHOD_UPDATE = "write";
    protected static OdooClient mOdoo;
    protected static Gson mGson = new GsonBuilder()
            .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
            .registerTypeAdapter(OdooDate.class, Adapter.DATE)
            .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
            .create();

    public static void setOdoo(OdooClient odoo) {
        mOdoo = odoo;
    }

    public static void requestLogin(String username, String password, IOdooResponse result) {
        mOdoo.authenticate(username, password, "DB_HP", result);
    }

    public static void createRecord(IModel obj, IResponse response) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        callMethod(obj.getModelName(), METHOD_CREATE, null, obj, map, response);
    }

    public static void updateRecord(List<Integer> ids, IModel obj, IResponse response) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        callMethod(obj.getModelName(), METHOD_UPDATE, ids, obj, map, response);
    }

    protected static void callMethod(String model, String method, List<Integer> ids, IModel obj, HashMap kwargs, IResponse response) throws Exception {
        OArguments args = new OArguments();

//        GsonBuilder builder = new GsonBuilder().registerTypeAdapterFactory(new RelTypeAdapterFactory<OdooRelType>());
        if(obj != null){
            JSONObject jsonObject = new JSONObject(mGson.toJson(obj));
            args.add(jsonObject);
        }

        if (ids != null) {
            args.add(new JSONArray(ids.toString()));
        }

        mOdoo.callMethod(model, method, args, kwargs, null, new SharingOdooResponse2() {
            @Override
            public void onSuccess(Object result) {
                response.onSuccess(result);
            }

            @Override
            public void onFail(Throwable error) {
            }
        }.getResponse(obj.getClass()));


    }
}
