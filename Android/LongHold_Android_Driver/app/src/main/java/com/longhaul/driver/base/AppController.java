package com.longhaul.driver.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class AppController extends Application {
    public static final String BASE_URL = "http://demo.nextsolutions.com.vn:8070";
//    public static final String BASE_URL = "http://192.168.1.95:8070";

    public static final String BASE_URL_IMAGE = BASE_URL + "/images/";
    public static final String BASE_URL_GOOGLE_GEOCODE = "https://maps.googleapis.com/maps/api/directions/";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String DATE_PATTERN = "dd/MM/yyyy";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String DATE_TIME_PATTERN = "HH:mm dd/MM/yyyy";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CACHE_USER = "CACHE_USER";
    public static final String DATE_PATTERN_HISTORY = "yyyy-MM-dd";
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(AppController.DATE_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatTime = new SimpleDateFormat(AppController.TIME_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDateTime = new SimpleDateFormat(AppController.DATE_TIME_PATTERN);
    public static Long languageId = 1L;
    public static String[] LANGUAGE_CODE = {"vi", "en", "my"};
    private static AppController mInstance;
    private static Context context;
    private static DecimalFormat formatter;
    private static String mTimeMaxDate = "";
    HashMap<String, Object> clientCache;
    private SharedPreferences sharedPreferences;

    public static Context getAppContext() {
        return AppController.context;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static String formatCurrency(Integer money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money) + " VNĐ";
    }

    public static String formatCurrency(BigDecimal money) {
        if (money == null) {
            return "0 đ";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money) + " đ";
    }

    @SuppressLint("DefaultLocale")
    public static String formatNumber(double number) {
        if (number >= 1000000000) {
            return String.format("%.1fT", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.1fB", number / 1000000.0);
        }

        if (number >= 100000) {
            return String.format("%.1fM", number / 100000.0);
        }

        if (number >= 1000) {
            return String.format("%.1fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String convertDate(String input) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String outDate = "";
        String dateReplace = "";

        try {
            Date date = format.parse(input);
            System.out.println(date);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            Date GetDate = new Date();
            outDate = timeStampFormat.format(GetDate);
            dateReplace = outDate.replace(':', 'h');
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateReplace;
    }

    public static Long cvDateToTimeMillis(String dateInput) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(dateInput);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();

        Calendar calendar = Calendar.getInstance();
        Log.e("duongnk", "cvTime: " + millis + "/ timeNow /" + calendar.getTimeInMillis());
        long timeNow = calendar.getTimeInMillis();
        return Math.abs(millis - timeNow);
    }

    @SuppressLint("NewApi")
    public static Date formatDate(int year, int month, int dayOfMonth, String pattent) {
        final Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
        date.setYear(year);
        android.icu.text.SimpleDateFormat sdf = new android.icu.text.SimpleDateFormat(pattent);
        String dt = sdf.format(c.getTime());
        return date;
    }

    public static String convertUTCTime(String dateStr) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        return formattedDate;
    }

    public static String getDate(Date date, int current) {
        String currentDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, current);
        date = c.getTime();
        currentDate = new SimpleDateFormat(DATE_PATTERN_HISTORY, Locale.getDefault()).format(date);
        return currentDate;
    }

    public static Date conVertDate(String strDate) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppController.context = getApplicationContext();
        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
        // khởi tạo realm
        initRealm();

    }

    private void initRealm() {
        Realm.init(getApplicationContext());
        RealmConfiguration config =
                new RealmConfiguration.Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build();
        Realm.setDefaultConfiguration(config);
    }

    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    public String formatCurrencyV2(Double money) {
        if (money == null) {
            return "0";
        }
        if (formatter == null) {
            formatter = new DecimalFormat("###,###,###");
        }

        return formatter.format(money);
    }

    public void putCache(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
    }

    public void newActivity(Activity previousActivity, Class fragmentClazz, String extra, Serializable objExtra) {
        Intent intent = new Intent(previousActivity, CommonActivity.class);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (extra != null) {
            intent.putExtra(extra, objExtra);
        }
        previousActivity.startActivity(intent);
    }

    public void newActivity(BaseFragment fragment, Class fragmentClazz, boolean result, int code, Pair<String, Serializable>... objs) {
        Intent intent = new Intent(fragment.getBaseActivity(), CommonActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (objs != null) {
            for (Pair<String, Serializable> obj : objs) {
                intent.putExtra(obj.first, obj.second);
            }
        }
        if (result) {
            fragment.startActivityForResult(intent, code);
        } else {
            fragment.startActivity(intent);
        }
    }

    public OdooDateTime convertStringToDate(String datetime) {
        try {
            OdooDateTime date = (OdooDateTime) formatDateTime.parse(datetime);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
