package com.longhaul.driver.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.TsBaseModel;

import java.util.Objects;

public class Cargo extends TsBaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cargo_number")
    @Expose
    private String cargoNumber;
    @SerializedName("from_depot_id")
    @Expose
    private Integer fromDepotId;
    @SerializedName("to_depot_id")
    @Expose
    private Integer toDepotId;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("size_id")
    @Expose
    private Integer sizeId;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private Object price;
    @SerializedName("from_latitude")
    @Expose
    private Double fromLatitude;
    @SerializedName("to_latitude")
    @Expose
    private Double toLatitude;
    @SerializedName("bidding_package_id")
    @Expose
    private Integer biddingPackageId;
    @SerializedName("from_longitude")
    @Expose
    private Double fromLongitude;
    @SerializedName("to_longitude")
    @Expose
    private Double toLongitude;
    @SerializedName("size_standard")
    @Expose
    private String sizeStandard;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCargoNumber() {
        return cargoNumber;
    }

    public void setCargoNumber(String cargoNumber) {
        this.cargoNumber = cargoNumber;
    }

    public Integer getFromDepotId() {
        return fromDepotId;
    }

    public void setFromDepotId(Integer fromDepotId) {
        this.fromDepotId = fromDepotId;
    }

    public Integer getToDepotId() {
        return toDepotId;
    }

    public void setToDepotId(Integer toDepotId) {
        this.toDepotId = toDepotId;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Double getFromLatitude() {
        return fromLatitude;
    }

    public void setFromLatitude(Double fromLatitude) {
        this.fromLatitude = fromLatitude;
    }

    public Double getToLatitude() {
        return toLatitude;
    }

    public void setToLatitude(Double toLatitude) {
        this.toLatitude = toLatitude;
    }

    public Integer getBiddingPackageId() {
        return biddingPackageId;
    }

    public void setBiddingPackageId(Integer biddingPackageId) {
        this.biddingPackageId = biddingPackageId;
    }

    public Double getFromLongitude() {
        return fromLongitude;
    }

    public void setFromLongitude(Double fromLongitude) {
        this.fromLongitude = fromLongitude;
    }

    public Double getToLongitude() {
        return toLongitude;
    }

    public void setToLongitude(Double toLongitude) {
        this.toLongitude = toLongitude;
    }

    public String getSizeStandard() {
        return sizeStandard;
    }

    public void setSizeStandard(String sizeStandard) {
        this.sizeStandard = sizeStandard;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cargo cargo = (Cargo) o;
        return id.equals(cargo.id);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
