package com.longhaul.database;

import android.content.Context;
import android.util.Log;

import com.longhaul.driver.api.DriverApi;

import java.util.ArrayList;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

public class RealmDTB {
    private static RealmDTB realmDTB;
    private static Realm mRealm;
    private static Context mContext;

    public static RealmDTB getInstance(Context context) {
        if (realmDTB == null) {
            realmDTB = new RealmDTB();
        }
        mRealm = Realm.getDefaultInstance();
        if (context != null) {
            mContext = context;
        }
        return realmDTB;
    }


    public void addLocation(final String Latitude, final String Longitude, final String time) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try {
//                        Location user = realm.createObject(Location.class);
                        Calendar calndr2 = Calendar.getInstance();

                        Location user = realm.createObject(Location.class, calndr2.getTime().toString());
                        user.setLatitude(Latitude);
                        user.setLongitude(Longitude);
                        user.setVan_id("21");
                        user.setCheckPush(false);
                        realm.copyToRealm(user);
                    } catch (RealmPrimaryKeyConstraintException e) {
                        Log.e("duongnk", "execute: Primary Key exists, Press Update instead");
                    }
                }
            });
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public void readLocationRecords() {
        mRealm.executeTransaction(realm -> {

            RealmResults<Location> results = realm.where(Location.class).findAll();
            ArrayList<LocationApi> arrayList = new ArrayList<>();

            Log.e("duongnkRead", "readLocationRecords: " + results.toString());

            for (int i = 0; i < results.size(); i++) {
                if (!results.get(i).isCheckPush) {
                    LocationApi locationApi = new LocationApi();
                    locationApi.latitude = results.get(i).latitude;
                    locationApi.create_time = results.get(i).create_time;
                    locationApi.longitude = results.get(i).longitude;
                    arrayList.add(locationApi);
                    // Updating a boolean field using automatic input conversion as needed.

                }
            }
            Log.e("abc", "execute: " + arrayList.size());
            DriverApi.UpdateDataLocation(arrayList);
        });
    }

    public void updateEmployeeRecords() {
        mRealm.executeTransaction(realm -> {
            RealmResults<Location> location = realm.where(Location.class).findAll();
            for (int i = 0; i < location.size(); i++) {
                location.get(i).isCheckPush = true;
            }
        });
    }


}
