package com.longhaul.driver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetail extends TsBaseModel {
    private int orderType = 0;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("bidding_order_number")
    @Expose
    private String biddingOrderNumber;
    @SerializedName("from_depot")
    @Expose
    private FromDepot fromDepot;
    @SerializedName("to_depot")
    @Expose
    private ToDepot toDepot;
    @SerializedName("total_weight")
    @Expose
    private Long totalWeight;
    @SerializedName("total_cargo")
    @Expose
    private Long totalCargo;
    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("currency_name")
    @Expose
    private String currencyName;
    @SerializedName("distance")
    @Expose
    private Long distance;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("write_date")
    @Expose
    private String writeDate;
    @SerializedName("bidding_package_id")
    @Expose
    private Integer biddingPackageId;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private OdooDateTime max_confirm_time;
    @SerializedName("bidding_vehicles")
    @Expose
    private BiddingInformation.BiddingVehicle biddingVehicles = null;
    @SerializedName("cargo_type")
    @Expose
    private List<CargoTypes> cargoType;

}
