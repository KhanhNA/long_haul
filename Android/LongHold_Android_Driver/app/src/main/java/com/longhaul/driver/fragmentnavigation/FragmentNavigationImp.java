package com.longhaul.driver.fragmentnavigation;


import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentNavigationImp implements FragmentNavigation {
    private AppCompatActivity activity;
    private int containerId;

    public FragmentNavigationImp(AppCompatActivity activity, @IdRes int containerId) {
        this.activity = activity;
        this.containerId = containerId;
    }

    @Override
    public void addFragment(Fragment fragment) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(containerId, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void replaceFragment(Fragment fragment, boolean backStack) {
        FragmentTransaction ft = activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, fragment.getClass().getSimpleName());
        if (backStack) ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    @Override
    public void newRootFragment(Fragment fragment) {
        popAllStack();
        replaceFragment(fragment, false);
    }

    @Override
    public void newStackFragment(Fragment fragment) {
        popAllStack();
        addFragment(fragment);
    }

    @Override
    public void back() {
        activity.getSupportFragmentManager().popBackStack();
    }

    @Override
    public void backToFragment(String tag) {
        activity.getSupportFragmentManager().popBackStack(tag, 0);
    }

    @Override
    public void backBeforeFragment(String tag) {
        activity.getSupportFragmentManager().popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void replaceChildFragment(android.app.FragmentManager childFragmentManager, android.app.Fragment fragment, int childContainerId) {
        childFragmentManager.beginTransaction()
                .replace(childContainerId, fragment, fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void backAndRefreshFragment(Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        Fragment frg = fragmentManager.findFragmentByTag(tag);
        fragmentManager.popBackStack(tag, 0);
        if (frg != null) {
            final FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.remove(frg);
            ft.commit();
            addFragment(fragment);
        }

    }

    private void popAllStack() {
        if (activity.getSupportFragmentManager().getBackStackEntryCount() != 0) {
            String first = activity.getSupportFragmentManager().getBackStackEntryAt(0).getName();
            activity.getSupportFragmentManager().popBackStackImmediate(first, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
