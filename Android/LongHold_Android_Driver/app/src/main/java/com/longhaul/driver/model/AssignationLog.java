package com.longhaul.driver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignationLog extends TsBaseModel {
    private Integer id;
    private String assignation_log_code;
    private OdooDateTime date_start;
    private OdooDateTime date_end;
}
