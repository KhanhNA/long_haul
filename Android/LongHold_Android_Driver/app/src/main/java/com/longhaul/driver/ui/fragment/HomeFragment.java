package com.longhaul.driver.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.google.android.material.tabs.TabLayout;

import com.longhaul.driver.R;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.HomeFragmentBinding;
import com.longhaul.driver.ui.viewcommon.PagerAdapter;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.viewmodel.HomeVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class HomeFragment extends BaseFragment {
    private HomeVM homeVM;
    private HomeFragmentBinding mBinding;
    SharedPreferences preferences = AppController.getInstance().getSharePre();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (HomeFragmentBinding) binding;
        homeVM = (HomeVM) viewModel;
        try {
            final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData();
        initView();
        initViewPagerTablayout();
        return v;
    }

    private void initView() {
    }

    private void getData() {
        homeVM.getBidding(this::runUi);
    }


    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals("getSuccess")) {
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {

        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.home_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HomeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private void initViewPagerTablayout() {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.NOT_SHIPPING));
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.SHIPPING));
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.SHIPPING_DONE));
        mBinding.viewPager.setAdapter(adapter);
        mBinding.viewPager.setOffscreenPageLimit(4);
        mBinding.viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mBinding.tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private Fragment createFragment(int pos) {
        ShippingBillFragment biddingFragment = new ShippingBillFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_PAGE, pos);
        biddingFragment.setArguments(bundle);
        return biddingFragment;

    }
}
