package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class driver_info extends TsBaseModel {
    private int id;
    private String driver_name;
    private String driver_phone_number;
    private String lisence_plate;
    private String idCard;

    public String getDriver_name() {
        return driver_name;
    }

    public String getDriver_phone_number() {
        return driver_phone_number;
    }

    public String getLisence_plate() {
        return lisence_plate;
    }

    public String getIdCard() {
        return idCard;
    }
}
