package com.longhaul.driver.model;


import com.longhaul.database.Location;
import com.tsolution.base.TsBaseModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserInfo extends TsBaseModel {
    private String userName;
    private String passWord;
    private Boolean isSave;
    private String langCode;
    private Integer langId;
    private Long distanceCheckPoint;
    private String google_api_key_geocode;
    public UserInfo() {
    }

    public UserInfo(String userName, String passWord, Boolean isSave, String lang, Integer langId, Long distanceCheckPoint, String google_api_key_geocode) {
        this.userName = userName;
        this.passWord = passWord;
        this.isSave = isSave;
        this.langCode = lang;
        this.langId = langId;
        this.distanceCheckPoint = distanceCheckPoint;
        this.google_api_key_geocode = google_api_key_geocode;
    }
}
