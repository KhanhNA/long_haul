package com.longhaul.driver.base;

public interface RunUi {
    void run(Object... params);
}
