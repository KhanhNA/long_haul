package com.longhaul.driver.fragmentnavigation;



import android.app.FragmentManager;

import androidx.fragment.app.Fragment;

public interface FragmentNavigation {
    void addFragment(Fragment fragment);

    void replaceFragment(Fragment fragment, boolean backStack);

    void newRootFragment(Fragment fragment);

    void newStackFragment(Fragment fragment);

    void back();

    void backToFragment(String tag);

    void backAndRefreshFragment(Fragment fragment);

    void backBeforeFragment(String tag);

    void replaceChildFragment(FragmentManager childFragmentManager, android.app.Fragment fragment, int childContainerId);
}
