package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverCargo extends TsBaseModel {
    private int id;
    private String code;
    private String res_user_id;
    private String lisence_plate;
    private String driver_phone_number;
    private String driver_name;
    private String expiry_time;
    private int company_id;
    private String status;
    private int create_uid;
    private String create_date;
    private int write_uid;
    private String write_date;
    private String description;
    private String idCard;
    private String id_card;
}
