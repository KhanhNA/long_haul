package com.longhaul.driver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.model.PhoneLogin;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

public class DrivingConfirmVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<PhoneLogin>> mPhoneLogin = new MutableLiveData<>();

    public DrivingConfirmVM(@NonNull Application application) {
        super(application);
    }

    public void confirmDriving(String license_plate, RunUi runUi) {
        DriverApi.getBiddingVehicleInforLogin(license_plate, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<PhoneLogin> response = (OdooResultDto<PhoneLogin>) o;
                if (o != null)
                    if (response.getRecords() != null){
                        mPhoneLogin.setValue((OdooResultDto<PhoneLogin>) o);

                        runUi.run("Success");
                    }else {
                        runUi.run("Fail");
                    }else {
                    runUi.run("Fail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("Fail");
            }
        });

    }
}
