package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingPoint extends TsBaseModel {
    private Integer id;
    private String name;
    private String address;
    private String phone;
    private Float latitude;
    private Float longitude;

}
