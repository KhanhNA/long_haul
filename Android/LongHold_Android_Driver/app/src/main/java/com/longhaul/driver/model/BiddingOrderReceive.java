package com.longhaul.driver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BiddingOrderReceive extends TsBaseModel {
    private Integer id;
    private Integer bidding_order_id;
    private String from_expected_time;
    private String to_expected_time;
    private String depot_id;
    private OdooDateTime actual_time;
    private String stock_man_id;
    private String status;
    private String description;
    private String create_date;
    private String write_date;
    private Integer bidding_order_vehicle_id;
}
