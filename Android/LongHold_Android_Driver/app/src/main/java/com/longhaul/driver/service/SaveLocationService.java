package com.longhaul.driver.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;


import com.longhaul.database.RealmDTB;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class SaveLocationService extends android.app.Service {
    private Timer timer = new Timer();
    int counter = 0;
    LocationManager locationManager;
    LocationListener locationListener;
    Context context;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        doSomethingRepeatedly();
        context = this;
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();
        super.onStartCommand(intent, flags, startId);
        return START_REDELIVER_INTENT;
    }


    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            String longitude = "Longitude: " + loc.getLongitude();
            String latitude = "Latitude: " + loc.getLatitude();
            Calendar calndr2 = Calendar.getInstance();
            Log.e("duongnk", "onLocationChanged: " + latitude);
            Log.e("duongnk", "onLocationChanged: " + longitude);
            RealmDTB.getInstance(context).addLocation(loc.getLatitude() + "", loc.getLongitude() + "", calndr2.getTime().toString());
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    @SuppressLint("MissingPermission")
    private void doSomethingRepeatedly() {
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                new Handler(Looper.getMainLooper()).post(() -> locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 50000, 10, locationListener));

            }
        }, 0, 6000);
    }

}

