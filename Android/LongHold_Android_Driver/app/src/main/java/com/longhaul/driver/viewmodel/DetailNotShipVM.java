package com.longhaul.driver.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.longhaul.driver.R;
import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.model.BiddingInformation;
import com.longhaul.driver.model.Cargo;
import com.longhaul.driver.model.CargoTypes;
import com.longhaul.driver.model.ConfirmBiddingResponse;
import com.longhaul.driver.model.GeoCodedWayPoints;
import com.longhaul.driver.model.Leg;
import com.longhaul.driver.model.OrderDetail;
import com.longhaul.driver.model.QRCode;
import com.longhaul.driver.utils.EnumBidding;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;

@Getter
public class DetailNotShipVM extends BaseViewModel {
    ArrayList<CargoTypes> listCargoTypeCompare = new ArrayList<>();
    ArrayList<CargoTypes> listCargoTypeValidate = new ArrayList<>();
    ArrayList<String> listQROrigin = new ArrayList<>();
    //------ tung
    private List<OrderDetail> orderDetailListEdit = new ArrayList<>();
    private MutableLiveData<String> status = new MutableLiveData<>();
    private MutableLiveData<String> confirmBiddingStatus = new MutableLiveData<>();

    private MutableLiveData<List<OrderDetail>> orderDetailList = new MutableLiveData<>();
    private MutableLiveData<OrderDetail> orderDetail = new MutableLiveData<>();

    private MutableLiveData<List<CargoTypes>> cargoTypeList = new MutableLiveData<>();
    private MutableLiveData<CargoTypes> cargoType = new MutableLiveData<>();
    private MutableLiveData<List<QRCode>> qrCodeList = new MutableLiveData<>();

    private MutableLiveData<Boolean> isQRListEmpty = new MutableLiveData<>();

    private MutableLiveData<Boolean> isEditMode = new MutableLiveData<>();
    private MutableLiveData<Boolean> visibilityQR = new MutableLiveData<>();
    private MutableLiveData<Long> distance = new MutableLiveData<>();
    private MutableLiveData<Integer> orderStatus = new MutableLiveData<>();

    private MutableLiveData<String> distanceStatus = new MutableLiveData<>();
    private MutableLiveData<LatLng> currentDriverLocation = new MutableLiveData<>();
    private MutableLiveData<Leg> leg = new MutableLiveData<>();
    private MutableLiveData<ArrayList<CargoTypes>> getListCargoTypeCompare = new MutableLiveData<ArrayList<CargoTypes>>();
    private MutableLiveData<ArrayList<CargoTypes>> getListCargoTypeValidate = new MutableLiveData<ArrayList<CargoTypes>>();

    private HashMap<Integer, CargoTypes> cargoTypeMap = new HashMap<>();
    private HashMap<String, Cargo> cargoMap = new HashMap<>();
    private HashMap<Integer, CargoTypes> cargoTypeBiddingVehicleMap = new HashMap<>();
    private HashMap<String, Cargo> cargoBiddingVehicleMap = new HashMap<>();

    //
    public DetailNotShipVM(@NonNull Application application) {
        super(application);
    }

    public static <T> List<T> getObjectList(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static <T> String convertObjectToJson(List<T> arr) {
        return new Gson().toJson(arr);
    }

    private void addListQR(OdooResultDto<OrderDetail> response) {
        for (int i = 0; i < response.getRecords().size(); i++) {
            for (CargoTypes cargoType : response.getRecords().get(i).getCargoType()) {
                listCargoTypeCompare.add(cargoType);
                getListCargoTypeCompare.setValue(listCargoTypeCompare);
            }
            for (int j = 0; j < response.getRecords().get(i).getBiddingVehicles().getCargo_types().size(); j++) {
                List<CargoTypes> cargo_types = response.getRecords().get(i).getBiddingVehicles().getCargo_types();
                if (cargo_types != null)
                    for (CargoTypes cargoType : cargo_types) {
                        listCargoTypeValidate.add(cargoType);
                        getListCargoTypeValidate.setValue(listCargoTypeValidate);
                    }
            }
        }
    }

    //update data QRActivity
    public void updateDataQRValidate(OrderDetail orderDetail){
        listCargoTypeValidate.clear();
        for (int j = 0; j <orderDetail.getBiddingVehicles().getCargo_types().size(); j++) {
            List<CargoTypes> cargo_types = orderDetail.getBiddingVehicles().getCargo_types();
            if (cargo_types != null)
                for (CargoTypes cargoType : cargo_types) {
                    listCargoTypeValidate.add(cargoType);
                    getListCargoTypeValidate.setValue(listCargoTypeValidate);
                }
        }
    }

    public void cloneOrderDetailList(int currentCargoTypePosition) {
        if (getOrderDetailListEdit() != null && getOrderDetailListEdit().size() > 0) {
            String json = DetailNotShipVM.convertObjectToJson(getOrderDetailListEdit());
            List<OrderDetail> data = DetailNotShipVM.getObjectList(json, OrderDetail.class);
            ArrayList<OrderDetail> orderDetailArrayList = new ArrayList<OrderDetail>() {{
                add(data.get(data.size() - 1));
            }};
            getOrderDetailList().setValue(orderDetailArrayList);
            getOrderDetail().setValue(getOrderDetailList().getValue().get(0));
            setCargoTypeData(getOrderDetailList().getValue().get(0).getBiddingVehicles().getCargo_types(), currentCargoTypePosition);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public boolean isSimilarOrderDetail() {
        OrderDetail orderDetail =getOrderDetail().getValue();
        //lấy list qr update
        ArrayList<String> newQRList = new ArrayList<>();
        if (orderDetail != null) {
            List<CargoTypes> cargoTypesList = orderDetail.getBiddingVehicles().getCargo_types();
            for (int i = 0; i < cargoTypesList.size(); i++) {
                List<Cargo> cargoList = cargoTypesList.get(i).getCargos();
                if (cargoList != null && cargoList.size() > 0) {
                   for (int j =0;j<cargoList.size();j++){
                       newQRList.add(cargoList.get(j).getQrCode());
                   }
                }
            }
        }
        Collections.sort(newQRList);
        Collections.sort(listQROrigin);
        boolean isSimilar = true;
        if (newQRList.size() != listQROrigin.size()){
            isSimilar = false;
        }else {
            for (int i = 0; i < listQROrigin.size(); i++) {
                if (!listQROrigin.get(i).equals(newQRList.get(i))) {
                    isSimilar = false;
                    break;
                }
            }
        }

        return isSimilar;
    }

    //
    public void getOrderDetailList(int orderId) {
        status.setValue(EnumBidding.EnumStatus.LOADING);
        DriverApi.getOrderDetail(orderId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    OdooResultDto<OrderDetail> response = (OdooResultDto<OrderDetail>) o;
                    orderDetailList.setValue(response.getRecords());

                    ArrayList<OrderDetail> orderDetailArrayList = new ArrayList<>();
                    //convert gson to avoid change data
                    for (int i = 0; i < response.getRecords().size(); i++) {
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(response.getRecords().get(i));
                        OrderDetail orderDetailTmp = gson.fromJson(json, OrderDetail.class);
                        orderDetailArrayList.add(orderDetailTmp);
                    }

                    orderDetailListEdit.addAll(orderDetailArrayList);

                    orderDetail.setValue(response.getRecords().get(0));
                    setDataHashMap(response.getRecords().get(0));
                    List<CargoTypes> cargoTypes = new ArrayList<>(response.getRecords().get(0).getBiddingVehicles().getCargo_types());
                    setCargoTypeData(cargoTypes, -1);

                    distance.setValue(response.getRecords().get(0).getDistance());
                    addListQR(response);

                    status.setValue(EnumBidding.EnumStatus.SUCCESS);
                } else {
                    status.setValue(EnumBidding.EnumStatus.FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {
                status.setValue(EnumBidding.EnumStatus.FAIL);
            }
        });
    }

    public void setDataHashMap(OrderDetail orderDetail) {
        cargoTypeBiddingVehicleMap.clear();
        cargoBiddingVehicleMap.clear();
        cargoTypeMap.clear();
        cargoMap.clear();
        List<CargoTypes> cargoTypesList = orderDetail.getCargoType();

        List<CargoTypes> cargoTypesBiddingVehicleList = orderDetail.getBiddingVehicles().getCargo_types();
        //set data hash map cargo in BiddingVehicle
        for (int i = 0; i < cargoTypesBiddingVehicleList.size(); i++) {
            cargoTypeBiddingVehicleMap.put(cargoTypesBiddingVehicleList.get(i).getId(), cargoTypesBiddingVehicleList.get(i));
            List<Cargo> cargoBiddingVehicleList = cargoTypesBiddingVehicleList.get(i).getCargos();
            for (int j = 0; j < cargoBiddingVehicleList.size(); j++) {
                cargoBiddingVehicleMap.put(cargoBiddingVehicleList.get(j).getQrCode(), cargoBiddingVehicleList.get(j));
                listQROrigin.add(cargoBiddingVehicleList.get(j).getQrCode());
            }
        }
        //set data hash map cargo outside
        for (int i = 0; i < cargoTypesList.size(); i++) {
            cargoTypeMap.put(cargoTypesList.get(i).getId(), cargoTypesList.get(i));
            List<Cargo> cargoList = cargoTypesList.get(i).getCargos();
            for (int j = 0; j < cargoList.size(); j++) {
                cargoMap.put(cargoList.get(j).getQrCode(), cargoList.get(j));
            }
        }
    }

    public void setCargoTypeData(List<CargoTypes> cargoTypes, int postion) {
        //position == -1 là trạng thái khởi tạo
        if (postion == -1) {
            if (cargoTypes != null && cargoTypes.size() > 0) {
                for (int i = 0; i < cargoTypes.size(); i++) {
                    cargoTypes.get(i).checked = i == 0;
                }

                cargoTypeList.setValue(cargoTypes);
                cargoType.setValue(cargoTypes.get(0));

                setQrCodeList(cargoTypes.get(0));
            }
        } else {
            //position != -1 là trạng thái selected
            if (cargoTypes != null && cargoTypes.size() > 0) {
                for (int i = 0; i < cargoTypes.size(); i++) {
                    cargoTypes.get(i).checked = i == postion;
                }
                cargoTypeList.setValue(cargoTypes);
                cargoType.setValue(cargoTypes.get(postion));
                setQrCodeList(cargoTypes.get(postion));
            }
        }

    }

    public void confirmBidding(String bidding_order_id, String cargo_ids, String type, String confirm_time) {
        DriverApi.confirmBidding(bidding_order_id, cargo_ids, type, confirm_time, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    OdooResultDto<ConfirmBiddingResponse> response = (OdooResultDto<ConfirmBiddingResponse>) o;
                    if (response.getRecords() != null && response.getRecords().size() > 0) {
                        List<Integer> listCargoInvalid = response.getRecords().get(0).getList_cargo_invalid();
                        StringBuilder stringBuilder = new StringBuilder();
                        if (listCargoInvalid.size() > 0) {
                            for (int i = 0; i < listCargoInvalid.size(); i++) {
                                stringBuilder.append(listCargoInvalid.get(i)).append(",");
                            }
                            String cargoInvalid = stringBuilder.length() > 0 ? stringBuilder.substring(0, stringBuilder.length() - 1) : "";
                            if (cargoInvalid.isEmpty()) {
                                confirmBiddingStatus.setValue(EnumBidding.EnumStatus.SUCCESS);
                            } else {
                                confirmBiddingStatus.setValue(cargoInvalid + " " + getApplication().getApplicationContext().getString(R.string.is_exist_in_list_receive));
                            }
                        } else {
                            confirmBiddingStatus.setValue(EnumBidding.EnumStatus.SUCCESS);
                        }
                    }

                } else {
                    confirmBiddingStatus.setValue(getApplication().getApplicationContext().getString(R.string.check_empty_list_cargo));
                }
            }

            @Override
            public void onFail(Throwable error) {
                confirmBiddingStatus.setValue(error.getMessage());
            }
        });
    }

    public void getDrivingDistanceAndTime(String mode, String transit_routing_preference, LatLng origin, LatLng destination, String key) {
        DriverApi.getDrivingDistanceAndTime(getApplication().getBaseContext(), mode, transit_routing_preference, origin, destination, key, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    GeoCodedWayPoints geoCodedWayPoints = (GeoCodedWayPoints) o;
                    if (geoCodedWayPoints.getRoutes().size() > 0) {
                        Leg l = geoCodedWayPoints.getRoutes().get(0).getLegs().get(0);
                        leg.setValue(l);
                        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
                        long distanceCheckPoint = sharedPreferences.getLong(Constants.DISTANCE_CHECK_POINT, 0);
                        if (l.getDistance().getValue() > distanceCheckPoint) {
                            distanceStatus.setValue(getApplication().getApplicationContext().getString(R.string.distance_check_point));
                        } else {
                            distanceStatus.setValue(EnumBidding.EnumStatus.SUCCESS);
                        }
                    }

                } else {
                    Log.d("TAG", "onSuccess: Null");
                }
                Log.d("TAG", "onSuccess:" + o);
            }

            @Override
            public void onFail(Throwable error) {
                distanceStatus.setValue(error.getMessage());
            }
        });
    }

    public void setBiddingInformationUI(BiddingInformation biddingInformation) {
        if (biddingInformation != null) {
            List<OrderDetail> orderDetails = new ArrayList<>();
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setId(biddingInformation.getId());
            orderDetail.setStatus(biddingInformation.getStatus());
            orderDetail.setTotalCargo(Long.parseLong(biddingInformation.getTotal_cargo()));
            orderDetail.setTotalWeight(Long.parseLong(biddingInformation.getTotal_weight()));
            orderDetail.setFromDepot(biddingInformation.getFrom_depot());
            orderDetail.setToDepot(biddingInformation.getTo_depot());
            orderDetail.setFrom_receive_time(biddingInformation.getFrom_receive_time());
            orderDetail.setTo_receive_time(biddingInformation.getTo_receive_time());
            orderDetail.setTo_return_time(biddingInformation.getTo_return_time());
            orderDetail.setBiddingVehicles(biddingInformation.getBidding_vehicles());
            orderDetail.setDistance(Long.parseLong(biddingInformation.getDistance()));
            distance.setValue(Long.parseLong(biddingInformation.getDistance()));
            orderDetails.add(orderDetail);
            this.orderDetail.setValue(orderDetail);
            orderDetailList.setValue(orderDetails);

        }
    }

    public int getStatusOrder(OrderDetail orderDetail) {
        BiddingInformation.BiddingVehicle biddingVehicles = orderDetail.getBiddingVehicles();
        if ("2".equals(biddingVehicles.getBidding_order_return().getStatus())
                ) {
            // trả hàng
            return EnumBidding.EnumStatusOrder.SHIPPING_IS_SUCCESSFUL;
        }
        if ("-1".equals(biddingVehicles.getBidding_order_return().getStatus())){
            return EnumBidding.EnumStatusOrder.CANCEL_ORDER;
        }
        if ("0".equals(biddingVehicles.getBidding_order_receive().getStatus())) {
            // chưa vận chuyển
            return EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED;
        }
        if ("1".equals(biddingVehicles.getBidding_order_receive().getStatus())) {
            // đang vận chuyển
            return EnumBidding.EnumStatusOrder.BEING_TRANSPORTED;
        }

        return EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED;
    }

    public void setOrderDetail(OrderDetail orderDetail) {
        if (orderDetail != null) {
            List<OrderDetail> orderDetails = new ArrayList<>();
            distance.setValue(orderDetail.getDistance());
            orderDetails.add(orderDetail);
            this.orderDetail.setValue(orderDetail);
            orderDetailList.setValue(orderDetails);

        }
    }

    public void setQrCodeList(CargoTypes cargoType) {
        if (cargoType != null && cargoType.getCargos() != null) {
            int cargoLength = cargoType.getCargos().size();
            List<QRCode> qrCodes = new ArrayList<>();
            for (int i = 0; i < cargoLength; i++) {
                QRCode qrCode = new QRCode(cargoType.getCargos().get(i).getQrCode());
                qrCodes.add(qrCode);
            }
            qrCodeList.setValue(qrCodes);
        }

    }

    public void setSelectItemView(int position, RunUi runUi) {
        if (cargoTypeList.getValue() != null && position < cargoTypeList.getValue().size()) {
            int cargoTypeListLength = cargoTypeList.getValue().size();
            for (int i = 0; i < cargoTypeListLength; i++) {
                cargoTypeList.getValue().get(i).checked = false;
            }
            cargoTypeList.getValue().get(position).checked = true;
            cargoTypeList.setValue(cargoTypeList.getValue());
            runUi.run(Constants.UPDATE_ADAPTER);
        }

    }

    public void getCargoTypeByPosition(int pos) {
        cargoType.setValue(cargoTypeList.getValue().get(pos));
//        qrCodeList.postValue(cargoTypeList.getValue().get(pos).getCargos().get());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public CargoTypes handleReturnQR(String dataQr) {
        QRCode qrCode = new QRCode(dataQr);
        OrderDetail orderDetail = getOrderDetail().getValue();
        if (orderDetail != null) {
            List<CargoTypes> cargoTypeOfBiddingVehicles = orderDetail.getBiddingVehicles().getCargo_types();
            List<CargoTypes> ctList = orderDetail.getCargoType();
            if (ctList != null && ctList.size() > 0) {
                for (int i = 0; i < ctList.size(); i++) {
                    CargoTypes cargoType = ctList.get(i);

                    for (int j = 0; j < ctList.get(i).getCargos().size(); j++) {
                        Cargo cargo = cargoType.getCargos().get(j);

                        if (cargo.getQrCode().equals(qrCode.getCode())) {
                            boolean isContain = false;
                            for (int k = 0; k < cargoTypeOfBiddingVehicles.size(); k++) {
                                if (cargoTypeOfBiddingVehicles.get(k).equals(cargoType)) {
                                    isContain = true;
                                    if (cargoTypeOfBiddingVehicles.get(k).getCargos().contains(cargo)) {
                                        break;
                                    } else {
                                        cargoTypeOfBiddingVehicles.get(k).getCargos().add(cargo);
                                        this.cargoType.setValue(cargoTypeOfBiddingVehicles.get(k));
                                        setQrCodeList(cargoTypeOfBiddingVehicles.get(k));
                                    }
                                    break;
                                }
                            }
                            if (!isContain) {
                                Gson gson = new GsonBuilder().create();
                                String json = gson.toJson(cargoType);
                                CargoTypes cargoTypesTmp = gson.fromJson(json, CargoTypes.class);
                                cargoTypesTmp.setCargos(new ArrayList<Cargo>() {{
                                    add(cargo);
                                }});
                                cargoTypeOfBiddingVehicles.add(cargoTypesTmp);
                                setQrCodeList(cargoTypesTmp);
                                this.cargoType.setValue(cargoTypesTmp);
                                break;
                            }

                        }
                    }

                }
            }
            this.cargoTypeList.setValue(cargoTypeOfBiddingVehicles);
            this.orderDetail.setValue(orderDetail);
            getOrderDetailList().setValue(new ArrayList<OrderDetail>() {{
                add(orderDetail);
            }});
        }
        return cargoType.getValue();

    }

    public void handleReturnQRHashMap(String dataQr) {
        OrderDetail orderDetail = getOrderDetail().getValue();
        if (orderDetail != null) {
            // Key might be present...
            if (cargoMap.containsKey(dataQr)) {
                //lấy cargo với mã qr == dataQR
                Cargo cargo = cargoMap.get(dataQr);
                if (cargo != null) {
                    //lấy id cargoType
                    Integer cargoTypeId = cargo.getSizeId();
                    Set<Integer> cargoTypesSet = cargoTypeBiddingVehicleMap.keySet();
                    boolean isContainer = false;
                    for (Integer key : cargoTypesSet) {
                        if (key.equals(cargoTypeId)) {
                            //kiểm tra có cargo type chưa có thì tạo mới và thêm cargo
                            isContainer = true;
                            //Lấy cargo nếu cargo đã có trong list bidding vehicle thì
                            List<Cargo> cargoList = cargoTypeBiddingVehicleMap.get(key).getCargos();
                            if (cargoList == null) {
                                cargoList = new ArrayList<Cargo>() {{
                                    add(cargo);
                                }};
                            } else {
                                //nếu có cargo rồi thì không thêm nữa
                                if (cargoList.contains(cargo)) {
                                    break;
                                }
                                //chưa có
                                cargoList.add(cargo);

                            }
                            cargoTypeBiddingVehicleMap.get(key).setCargos(cargoList);
                            this.cargoType.setValue(cargoTypeBiddingVehicleMap.get(key));
                            setQrCodeList(cargoTypeBiddingVehicleMap.get(key));
                            cargoTypeBiddingVehicleMap.put(key, cargoTypeBiddingVehicleMap.get(key));
                            break;
                        }
                    }
                    //thêm cargo type mới
                    if (!isContainer) {
                        for (Integer key : cargoTypeMap.keySet()) {
                            if (key.equals(cargoTypeId)) {
                                Gson gson = new GsonBuilder().create();
                                String json = gson.toJson(cargoTypeMap.get(key));
                                CargoTypes cargoTypesTmp = gson.fromJson(json, CargoTypes.class);
                                cargoTypesTmp.setCargos(new ArrayList<Cargo>() {{
                                    add(cargo);
                                }});
                                cargoTypeBiddingVehicleMap.put(cargoTypesTmp.getId(), cargoTypesTmp);
                                this.cargoType.setValue(cargoTypesTmp);
                                setQrCodeList(cargoTypesTmp);

                                break;
                            }
                        }

                    }

                } else {
                    // cargo = null
                }
            } else {
                // Definitely no such key
            }
            //save new list cargo type
            List<CargoTypes> newCargoTypeList = new ArrayList<>();

            for (Integer key : cargoTypeBiddingVehicleMap.keySet()) {
                newCargoTypeList.add(cargoTypeBiddingVehicleMap.get(key));
            }
            this.cargoTypeList.setValue(newCargoTypeList);
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(orderDetail);
            OrderDetail orderDetailTmp = gson.fromJson(json, OrderDetail.class);
            orderDetailTmp.getBiddingVehicles().setCargo_types(newCargoTypeList);
            this.orderDetail.setValue(orderDetailTmp);
            getOrderDetailList().setValue(new ArrayList<OrderDetail>() {{
                add(orderDetailTmp);
            }});
        }
    }

    public int getPositionCargoTypeByQRCode(String qrCode) {
        List<OrderDetail> orderDetails = getOrderDetailList().getValue();
        if (orderDetails != null && orderDetails.size() > 0) {
            OrderDetail orderDetail = orderDetails.get(0);
            List<CargoTypes> ctList = orderDetail.getBiddingVehicles().getCargo_types();
            if (ctList != null) {
                for (int i = 0; i < ctList.size(); i++) {
                    List<Cargo> cargoList = ctList.get(i).getCargos();
                    for (int j = 0; j < cargoList.size(); j++) {
                        if (cargoList.get(j).getQrCode().equals(qrCode)) {
                            return i;
                        }
                    }
                }
            }

        }
        return -1;
    }

    public void handleConfirmBidding(int type) {
        ArrayList<String> cargoIdList = new ArrayList<>();
        OrderDetail orderDetail = getOrderDetailList().getValue().get(0);
        if (orderDetail != null) {
            List<CargoTypes> cargoTypesList = orderDetail.getBiddingVehicles().getCargo_types();
            for (int i = 0; i < cargoTypesList.size(); i++) {
                List<Cargo> cargoList = cargoTypesList.get(i).getCargos();
                for (int j = 0; j < cargoList.size(); j++) {
                    cargoIdList.add(String.valueOf(cargoList.get(j).getId()));
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            String cargo_ids = "";
            for (int i = 0; i < cargoIdList.size(); i++) {
                stringBuilder.append(cargoIdList.get(i)).append(",");
            }
            if (!stringBuilder.toString().isEmpty()) {
                cargo_ids = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
            }
            String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
            if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                confirmBidding(orderDetail.getId() + "", cargo_ids, "" + type, currentTime);
            } else {
                confirmBidding(orderDetail.getId() + "", cargo_ids, "", currentTime);
            }

        }
    }

    public boolean isEnableButtonReceiveGoods() {
        OrderDetail orderDetail = getOrderDetailList().getValue().get(0);
        boolean isEnable = false;
        if (orderDetail != null) {
            List<CargoTypes> cargoTypesList = orderDetail.getBiddingVehicles().getCargo_types();
            for (int i = 0; i < cargoTypesList.size(); i++) {
                List<Cargo> cargoList = cargoTypesList.get(i).getCargos();
                if (cargoList != null && cargoList.size() > 0) {
                    isEnable = true;
                    break;
                }
            }
        }
        return isEnable;
    }

    public void handleRemoveQRCode(QRCode qrCode, List<QRCode> qrList) {
        OrderDetail orderDetail = getOrderDetail().getValue();
        List<CargoTypes> cargoTypesList = orderDetail.getBiddingVehicles().getCargo_types();
        for (int i = 0; i < cargoTypesList.size(); i++) {
            List<Cargo> cargoList = cargoTypesList.get(i).getCargos();
            for (int j = 0; j < cargoList.size(); j++) {
                if (cargoList.get(j).getQrCode().equals(qrCode.getCode())) {
                    cargoList.remove(j);
                    break;
                }
            }
        }
        getOrderDetail().setValue(orderDetail);
        getOrderDetailList().setValue(new ArrayList<OrderDetail>() {{
            add(orderDetail);
        }});
        getQrCodeList().setValue(qrList);

    }


    public void filterCargoType(String type) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cargoType.setValue((CargoTypes) ((List) cargoTypeList.getValue().stream().filter(cargoType -> cargoType.getType() == type).collect(Collectors.toList())).get(0));
        }
    }

    public void caculateDistance(String mode, String transit_routing_preference, LatLng origin, LatLng destination, String key) {
        getDrivingDistanceAndTime(mode, transit_routing_preference, origin, destination, key);
    }
}
