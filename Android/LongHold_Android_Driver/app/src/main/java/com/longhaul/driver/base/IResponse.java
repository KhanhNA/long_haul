package com.longhaul.driver.base;


public interface IResponse<Result>{
    void onSuccess(Result result);
    void onFail(Throwable error);
}
