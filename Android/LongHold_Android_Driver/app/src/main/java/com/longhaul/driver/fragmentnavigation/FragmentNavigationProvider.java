package com.longhaul.driver.fragmentnavigation;

public interface FragmentNavigationProvider {
    FragmentNavigation provideNavigation();
}
