package com.longhaul.driver.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.longhaul.driver.R;
import com.longhaul.driver.databinding.DrivingConfirmFragmentBinding;
import com.longhaul.driver.ui.viewcommon.CustomToast;
import com.longhaul.driver.utils.ToastUtils;
import com.longhaul.driver.viewmodel.DrivingConfirmVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;


public class DrivingConfirmFragment extends BaseFragment {
    DrivingConfirmVM mDrivingConfirmVM;
    DrivingConfirmFragmentBinding mBinding;

    @Override
    public int getLayoutRes() {
        return R.layout.driving_confirm_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DrivingConfirmVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mDrivingConfirmVM = (DrivingConfirmVM) viewModel;
        mBinding = (DrivingConfirmFragmentBinding) binding;
        return view;
    }
    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.imageButtonBackOTP:
                getActivity().onBackPressed();
                break;
            case R.id.btnNext:
                //TODO check Biển số xe :
                String licensePlate = mBinding.edtIdDriving.getText().toString();
                mDrivingConfirmVM.confirmDriving("90F16916322",this::runUii);
                ToastUtils.showToast("next");
                break;
        }
    }
    protected void runUii(Object... objects) {
        String action = (String) objects[0];
        switch (action){
            case "Success":
                ToastUtils.showToast("Thành Công");
//                Bundle args = new Bundle();
//                args.putSerializable(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Serializable) o);
//                Intent intent = new Intent(getActivity(), CommonActivity.class);
//                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
//                intent.putExtra(DATA_PASS_FRAGMENT, args);
//                startActivity(intent);
                break;
            case "Fail":
                ToastUtils.showToast("Thất Bại");
                CustomToast.makeText(getContext(),"Thất bại", Toast.LENGTH_LONG, CustomToast.ERROR,false).show();
                break;
        }

    }

}
