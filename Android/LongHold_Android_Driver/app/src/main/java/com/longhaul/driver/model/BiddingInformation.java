package com.longhaul.driver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingInformation extends TsBaseModel {

    private Integer id;
    private String cargo_number;
    private String cargo_status;
    private String confirm_time;
    private OdooDateTime max_date;
    private String from_depot_id;
    private String to_depot_id;
    private String distance;
    private String size_id;
    private String quantity;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private String biding_order_id;
    private OdooDateTime create_date;
    private String product_type_id;
    private String biding_id;
    private String company_id;
    private String driver_name;
    private String phone;
    private String van_id;
    private String total_weight;
    private String total_cargo;
    private String from_latidude;
    private String from_longtitude;
    private String to_latidude;
    private String to_longtitude;
    private SizeStandard size_standard;
    private Integer price;
    private ProductType product_type;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private String bidding_status;
    private String bidding_type;
    private int from_latitude;
    private int to_latitude;
    private String type;
    private String status;
    private boolean isCheckDuration = true;
    private String bidding_order_number;
    private Integer create_uid;
    private String driver_id;
    private Integer cargo_id;
    private String note;
    private String bidding_order_receive_id;
    private String bidding_order_return_id;
    private Integer bidding_vehicle_id;
    private Integer price_bidding_order;
    private boolean isDurationCountdow = false;
    private String write_date;
    private int bidding_package_id;
    private OdooDateTime max_confirm_time;
    private BiddingVehicle bidding_vehicles;


    public int getBidding_package_id() {
        return bidding_package_id;
    }


    @Getter
    @Setter
    public class BiddingVehicle extends TsBaseModel {
        private Integer id;

        private Object code;

        private String lisence_plate;

        private String driver_phone_number;

        private String driver_name;

        private String expiry_time;

        private Integer company_id;

        private String status;

        private String description;

        private String id_card;

        private Integer res_partner_id;

        private Integer tonnage;

        private Integer vehicle_type;

        private Integer weight_unit;

        private String bidding_vehicle_seq;
        private BiddingOrderReceive bidding_order_receive;
        private BiddingOrderReturn bidding_order_return;
        private List<CargoTypes> cargo_types;
    }

}
