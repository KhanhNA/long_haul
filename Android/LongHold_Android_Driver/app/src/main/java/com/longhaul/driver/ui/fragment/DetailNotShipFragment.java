package com.longhaul.driver.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.longhaul.database.RealmDTB;
import com.longhaul.driver.R;
import com.longhaul.driver.base.BaseAdapterV3;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.XBaseAdapter;
import com.longhaul.driver.databinding.FragmentDetailNotShipBinding;
import com.longhaul.driver.model.CargoTypes;
import com.longhaul.driver.model.FromDepot;
import com.longhaul.driver.model.OrderDetail;
import com.longhaul.driver.model.QRCode;
import com.longhaul.driver.ui.activity.QrScanActivity;
import com.longhaul.driver.ui.adapter.QRCodeAdapter;
import com.longhaul.driver.ui.viewcommon.Dialog;
import com.longhaul.driver.ui.viewcommon.DialogConfirm;
import com.longhaul.driver.utils.BindingAdapterUtils;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.utils.ToastUtils;
import com.longhaul.driver.viewmodel.DetailNotShipVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.longhaul.driver.base.Constants.KEY_PAGE;
import static com.longhaul.driver.base.Constants.OBJECT_SERIALIZABLE;
import static com.longhaul.driver.utils.StringUtils.API_GOOGLE_MAP_DESTINATION;

public class DetailNotShipFragment extends BaseFragment implements ActionsListener {
    private static final int ZXING_CAMERA_PERMISSION = 1;
    int type = 0, currentCargoTypePosition = -1;
    FragmentDetailNotShipBinding mBinding;
    DetailNotShipVM detailNotShipVM;
    XBaseAdapter itemTabCargoTypeAdapter;
    //    BiddingInformation biddingInformation;
    int biddingId;
    boolean isClick = false;
    LocationManager locationManager;
    LocationListener locationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub
            double speed = location.getSpeed(); //speed in meter/minute
            speed = (speed * 3600) / 1000;      // speed in km/minute
            Toast.makeText(getContext(), "Current location:" + location.getLatitude() + ", " + location.getLongitude(), Toast.LENGTH_SHORT).show();
//            checkDistancePointer(location);

        }
    };
    private BaseAdapterV3 receiveGoodsAdapter;
    private Class<?> mClss;
    private QRCodeAdapter qrCodeListAdapter;
    private Dialog mDialogConfirm;
    private DialogConfirm dialogConfirm, dialogConfirmRemove, dialogConfirmReceive;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail_not_ship;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailNotShipVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        switch (v.getId()) {
            case R.id.ivMaps:
                if (detailNotShipVM.getOrderDetail().getValue() != null) {
                    Gson gson = new Gson();
                    OrderDetail orderDetail = detailNotShipVM.getOrderDetailList().getValue().get(0);
                    if (orderDetail != null) {
                        String json = gson.toJson(orderDetail);
                        Intent intent = new Intent(getActivity(), MapsFragment.class);
                        intent.putExtra(KEY_PAGE, type);
                        intent.putExtra(OBJECT_SERIALIZABLE, json);
                        startActivity(intent);
                    }

                } else {
                    Intent intent = new Intent(getActivity(), MapsFragment.class);
                    intent.putExtra(KEY_PAGE, type);
                    startActivity(intent);
                }

                break;
            case R.id.imgQr:
                launchActivity(QrScanActivity.class);
                break;
            case R.id.imgBack:
                String status = detailNotShipVM.getConfirmBiddingStatus().getValue();
                if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED && status != null && status.equals(EnumBidding.EnumStatus.SUCCESS)) {
                    Intent data = new Intent();
                    String text = getString(R.string.app_name);
                    //---set the data to pass back---
                    data.setData(Uri.parse(text));
                    getActivity().setResult(RESULT_OK, data);
                    //---close the activity---
                    getActivity().finish();
                } else {
                    getActivity().onBackPressed();
                }
                break;
            case R.id.btnReceiveGoods:
            case R.id.btnConfirm:
                if (!detailNotShipVM.isEnableButtonReceiveGoods() && type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                    ToastUtils.showToast(getString(R.string.check_empty_list_cargo));
                } else {
                    dialogConfirmReceive = new DialogConfirm(getString(R.string.confirm_receive_goods), null, getString(R.string.btnConfirm), view -> {
                        int type = detailNotShipVM.getStatusOrder(detailNotShipVM.getOrderDetailList().getValue().get(0));
                        if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            fusedLocationClient.getLastLocation()
                                    .addOnSuccessListener(getActivity(), location -> {
                                        if (location != null) {
                                            FromDepot fromDepot = detailNotShipVM.getOrderDetailList().getValue().get(0).getFromDepot();
                                            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
                                            LatLng destination = new LatLng(fromDepot.getLatitude(), fromDepot.getLongitude());
                                            detailNotShipVM.caculateDistance("driving", "less_driving", origin, destination, API_GOOGLE_MAP_DESTINATION);
                                        } else {
//                                            ToastUtils.showToast("Your location is not found");
                                            locationManager = (LocationManager) requireContext().getSystemService(Context.LOCATION_SERVICE);

                                            if (locationManager != null) {
                                                boolean isGPSEnabled = locationManager
                                                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
// getting network status
                                                boolean isNetworkEnabled = locationManager
                                                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                                                if (isGPSEnabled) {
                                                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);
                                                    Location locationGPS = locationManager
                                                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                                    if (locationGPS != null) {
                                                        checkDistancePointer(locationGPS);
                                                    } else {
                                                        if (isNetworkEnabled) {
                                                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 0, locationListener);
                                                            Location locationNetwork = locationManager
                                                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                                            if (locationNetwork != null) {
                                                                checkDistancePointer(locationNetwork);
                                                            }
                                                        } else {
                                                            ToastUtils.showToast("Your location is not found");
                                                            RealmDTB.getInstance(requireContext()).readLocationRecords();
                                                        }
                                                    }
                                                }


                                            }
                                        }

                                    });
                        } else {
                            detailNotShipVM.handleConfirmBidding(type);
                        }

                        dialogConfirmReceive.dismiss();
                    });
                    dialogConfirmReceive.show(getChildFragmentManager(), null);
                }

                break;
            case R.id.tvEdit:
                if (!isClick) {
                    mDialogConfirm = new Dialog(getString(R.string.confirm_edit_receive), null, getString(R.string.confirm_edit), v1 -> {
                        changeEdit();
                        mDialogConfirm.dismiss();
                    });
                    mDialogConfirm.show(getFragmentManager(), "tag");
                } else {
                    changeEdit();
                }
                break;
            case R.id.btnCancel:
                if (detailNotShipVM.getOrderDetail().getValue() != null){
                    String note = detailNotShipVM.getOrderDetail().getValue().getNote() != null ? detailNotShipVM.getOrderDetail().getValue().getNote() : "";
                    dialogConfirm = new DialogConfirm(getString(R.string.reason_close), getString(R.string.btnConfirm), note, false, v1 -> dialogConfirm.dismiss());
                    dialogConfirm.show(getFragmentManager(), "tag");
                }

                break;
        }
    }

    private void checkDistancePointer(Location location) {
        if (detailNotShipVM.getOrderDetailList().getValue() != null && detailNotShipVM.getOrderDetailList().getValue().size() > 0) {
            FromDepot fromDepot = detailNotShipVM.getOrderDetailList().getValue().get(0).getFromDepot();
            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
            LatLng destination = new LatLng(fromDepot.getLatitude(), fromDepot.getLongitude());
            detailNotShipVM.caculateDistance("driving", "less_driving", origin, destination, API_GOOGLE_MAP_DESTINATION);
        }
    }


    private void changeEdit() {
        //thay đổi trạng thái chỉnh sửa
        isClick = !isClick;
        detailNotShipVM.getVisibilityQR().setValue(isClick);
        detailNotShipVM.getIsEditMode().setValue(isClick);

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            if (data != null) {
                isClick = true;
                detailNotShipVM.getIsEditMode().setValue(isClick);
                String dataQr = data.getStringExtra("data");
                Toast.makeText(getContext(), getString(R.string.scan_qr_success) + dataQr, Toast.LENGTH_LONG).show();
                detailNotShipVM.getVisibilityQR().setValue(true);
//                detailNotShipVM.handleReturnQR(dataQr);
                detailNotShipVM.handleReturnQRHashMap(dataQr);
                currentCargoTypePosition = detailNotShipVM.getPositionCargoTypeByQRCode(dataQr);
                if (currentCargoTypePosition != -1) {
                    List<CargoTypes> cargoTypesList = detailNotShipVM.getCargoTypeList().getValue();
                    if (cargoTypesList != null) {
                        mBinding.iclReceiveGoods.rvTabLayout.scrollToPosition(currentCargoTypePosition);
                        detailNotShipVM.setCargoTypeData(cargoTypesList, currentCargoTypePosition);
                        itemTabCargoTypeAdapter.notifyDataSetChanged();
                    }
                }

            }

        }
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(getActivity(), QrScanActivity.class);
            intent.putExtra(Constants.LIST_CARGO_COMPARE, DetailNotShipVM.convertObjectToJson(detailNotShipVM.getListCargoTypeCompare()));
            intent.putExtra(Constants.LIST_CARGO_VALIDATE, DetailNotShipVM.convertObjectToJson(detailNotShipVM.getGetListCargoTypeValidate().getValue()));
            startActivityForResult(intent, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        Intent intent = new Intent(getActivity(), mClss);
                        intent.putExtra(Constants.LIST_CARGO_COMPARE, detailNotShipVM.getListCargoTypeCompare());
                        intent.putExtra(Constants.LIST_CARGO_VALIDATE, (Serializable) detailNotShipVM.getGetListCargoTypeValidate().getValue());
                        startActivityForResult(intent, 1);
                    }
                } else {
                    Toast.makeText(getActivity(), "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initView();
        subscribeUI();
        return v;
    }

    private void initView() {
        mBinding = (FragmentDetailNotShipBinding) binding;
        detailNotShipVM = (DetailNotShipVM) viewModel;
        getDataBundle();
        detailNotShipVM.getOrderStatus().setValue(type);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("SetTextI18n")
    private void subscribeUI() {
        //chưa vận chuyển kiểm tra distance check point
        detailNotShipVM.getDistanceStatus().observe(getViewLifecycleOwner(), s -> {
            if (EnumBidding.EnumStatus.SUCCESS.equals(s)) {
                detailNotShipVM.handleConfirmBidding(type);
            } else {
                ToastUtils.showToast(s);
            }
        });
        //trạng thái chỉnh sửa
        detailNotShipVM.getIsEditMode().observe(getViewLifecycleOwner(), mode -> {
            if (mode) {
                mBinding.iclReceiveGoods.tvEdit.setText(getString(R.string.button_edit));
                mBinding.toolbar.imgQr.setVisibility(View.VISIBLE);
            } else {
                detailNotShipVM.cloneOrderDetailList(currentCargoTypePosition);
                mBinding.iclReceiveGoods.tvEdit.setText(getString(R.string.edit_detail));
                mBinding.toolbar.imgQr.setVisibility(View.GONE);
            }
        });
        //visibility button remove item qr
        detailNotShipVM.getVisibilityQR().observe(getViewLifecycleOwner(), visibility -> {
            if (qrCodeListAdapter != null) {
                qrCodeListAdapter.setVisibilityRemove(visibility);
            }
        });
        detailNotShipVM.getOrderStatus().observe(getViewLifecycleOwner(), orderStatus -> {
                    BindingAdapterUtils.setTitleToolBarDetail(mBinding.toolbar.tvTitleDetail, orderStatus + "");
                }
        );
        detailNotShipVM.getDistance().observe(getViewLifecycleOwner(), distance -> {
            mBinding.toolbar.tvKm.setText("(" + distance + " km)");
        });
        //records
        detailNotShipVM.getOrderDetailList().observe(getViewLifecycleOwner(), orderDetail -> {
            BindingAdapterUtils.setDescription(mBinding.icl.tvDescription, orderDetail.get(0));
            BindingAdapterUtils.setFromTo(mBinding.icl.tvDescriptionShipTo, orderDetail.get(0));
            receiveGoodsAdapter = new BaseAdapterV3(R.layout.item_receive_goods, orderDetail,
                    this);
            orderDetail.get(0).setOrderType(type);
            mBinding.iclReceiveGoods.rvReceiveGoodsList.setAdapter(receiveGoodsAdapter);
            type = detailNotShipVM.getStatusOrder(orderDetail.get(0));
            if (type == EnumBidding.EnumStatusOrder.CANCEL_ORDER ) {
                mBinding.icl.btnCancel.setVisibility(View.VISIBLE);
            }
            detailNotShipVM.getOrderStatus().setValue(type);
            //case đang vận chuyển hiện qr khi trong trạng thái chỉnh sửa
            if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                if (!isClick) {
                    mBinding.toolbar.imgQr.setVisibility(View.GONE);
                } else {
                    mBinding.toolbar.imgQr.setVisibility(View.VISIBLE);
                }
                mBinding.iclReceiveGoods.tvEdit.setVisibility(View.VISIBLE);
            } else if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
                mBinding.toolbar.imgQr.setVisibility(View.VISIBLE);
                mBinding.iclReceiveGoods.tvEdit.setVisibility(View.GONE);
            } else {
                mBinding.toolbar.imgQr.setVisibility(View.GONE);
                mBinding.iclReceiveGoods.tvEdit.setVisibility(View.GONE);
            }
            detailNotShipVM.getCargoTypeList().setValue(orderDetail.get(0).getBiddingVehicles().getCargo_types());
            if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED || type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                mBinding.btnReceiveGoods.setVisibility(View.VISIBLE);
            } else {
                mBinding.btnReceiveGoods.setVisibility(View.GONE);
            }
            detailNotShipVM.updateDataQRValidate(orderDetail.get(0));

        });
        //cargotype of bidding vehicle
        detailNotShipVM.getCargoType().observe(getViewLifecycleOwner(), cargoTypes -> {
            if (cargoTypes == null) {
                mBinding.iclReceiveGoods.clDetailCargoType.setVisibility(View.GONE);
            } else {
                detailNotShipVM.setQrCodeList(cargoTypes);
                mBinding.iclReceiveGoods.clDetailCargoType.setVisibility(View.VISIBLE);
                mBinding.iclReceiveGoods.tvTotalCargo.setText(MessageFormat.format("{0} cargo", cargoTypes.getCargo_quantity()));
                mBinding.iclReceiveGoods.tvSize.setText(MessageFormat.format("{0} x {1} x {2} m", cargoTypes.getLength(), cargoTypes.getHeight(), cargoTypes.getWidth()));
            }
        });
        //List qr code of cargo in bidding vehicle
        detailNotShipVM.getQrCodeList().observe(getViewLifecycleOwner(), qrCodeList -> {
            if (qrCodeListAdapter == null) {
                qrCodeListAdapter = new QRCodeAdapter(R.layout.item_qr, qrCodeList, this, detailNotShipVM);
                mBinding.iclReceiveGoods.rvQRList.setAdapter(qrCodeListAdapter);
                if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
                    detailNotShipVM.getVisibilityQR().setValue(true);
                } else if (type == EnumBidding.EnumStatusOrder.SHIPPING_IS_SUCCESSFUL || type == EnumBidding.EnumStatusOrder.CANCEL_ORDER) {
                    detailNotShipVM.getVisibilityQR().setValue(false);
                    mBinding.btnReceiveGoods.setVisibility(View.GONE);
                }

            } else {
                qrCodeListAdapter.setList(qrCodeList);
            }
            if (qrCodeList != null) {
                mBinding.iclReceiveGoods.tvActualNumberCargoReceive.setText(MessageFormat.format("{0} cargo", qrCodeList.size()));
            }

            if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
                mBinding.btnReceiveGoods.setVisibility(View.VISIBLE);
                if (detailNotShipVM.isEnableButtonReceiveGoods()) {
                    detailNotShipVM.getVisibilityQR().setValue(true);
                    BindingAdapterUtils.setEnableReceiveButton(mBinding.btnReceiveGoods, false);
                } else {
                    BindingAdapterUtils.setEnableReceiveButton(mBinding.btnReceiveGoods, true);
                }
            } else if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                mBinding.btnReceiveGoods.setVisibility(View.VISIBLE);
                //đang vận chuyển trong trạng thái chỉnh sửa
                if (isClick && qrCodeList != null && qrCodeList.size() > 0 || isClick && qrCodeList != null) {
                    boolean isEnable = detailNotShipVM.isSimilarOrderDetail() || !detailNotShipVM.isEnableButtonReceiveGoods();
                    BindingAdapterUtils.setEnableReceiveButton(mBinding.btnReceiveGoods, isEnable);

                } else {
                    BindingAdapterUtils.setEnableReceiveButton(mBinding.btnReceiveGoods, true);
                    detailNotShipVM.getVisibilityQR().setValue(false);
                }

            }

            if (detailNotShipVM.isEnableButtonReceiveGoods()) {
                mBinding.iclReceiveGoods.tvQRCode.setVisibility(View.VISIBLE);
            } else {
                mBinding.iclReceiveGoods.tvQRCode.setVisibility(View.GONE);
            }

        });
        //list cargo type of bidding vehicle
        detailNotShipVM.getCargoTypeList().observe(getViewLifecycleOwner(), cargoTypes -> {
            if (cargoTypes != null) {
                itemTabCargoTypeAdapter = new XBaseAdapter(R.layout.item_tab_cargo_type, cargoTypes, this);
                mBinding.iclReceiveGoods.rvTabLayout.setAdapter(itemTabCargoTypeAdapter);
            }
            if (cargoTypes == null || cargoTypes.size() == 0) {
                mBinding.iclReceiveGoods.clDetailCargoType.setVisibility(View.GONE);
            }

        });
        //confirm bidding response status
        detailNotShipVM.getConfirmBiddingStatus().observe(getViewLifecycleOwner(), s -> {
            if (s.equals(EnumBidding.EnumStatus.SUCCESS)) {
                if (isClick)
                    isClick = false;
                mBinding.iclReceiveGoods.tvEdit.setText(getString(R.string.edit_detail));
                detailNotShipVM.getVisibilityQR().setValue(isClick);
                detailNotShipVM.getIsEditMode().setValue(isClick);
                detailNotShipVM.getOrderDetailList(biddingId);

                ToastUtils.showToast(getString(R.string.confirm_status_success));
            } else {
                ToastUtils.showToast(s);

            }
        });
        // status get list order detail
        detailNotShipVM.getStatus().observe(getViewLifecycleOwner(), state -> {
            if (state.equals(EnumBidding.EnumStatus.LOADING)) {
                mBinding.pbLoading.setVisibility(View.VISIBLE);
            } else {
                List<CargoTypes> cargoTypesList = detailNotShipVM.getCargoTypeList().getValue();
                if (cargoTypesList != null) {
                    detailNotShipVM.setCargoTypeData(cargoTypesList, currentCargoTypePosition);
                    if (currentCargoTypePosition != -1) {
                        mBinding.iclReceiveGoods.rvTabLayout.scrollToPosition(currentCargoTypePosition);
                    }
                    if (itemTabCargoTypeAdapter != null) {
                        itemTabCargoTypeAdapter.notifyDataSetChanged();
                    }
                    if (qrCodeListAdapter != null) {
                        qrCodeListAdapter.notifyDataSetChanged();

                    }
                }
                mBinding.pbLoading.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        switch (id) {
            case R.id.tvTab:
                List<CargoTypes> cargoTypesList = detailNotShipVM.getCargoTypeList().getValue();
                CargoTypes cargoType = (CargoTypes) o;
                if (cargoTypesList != null) {
                    currentCargoTypePosition = cargoTypesList.indexOf(cargoType);
                    detailNotShipVM.setCargoTypeData(cargoTypesList, currentCargoTypePosition);
                    itemTabCargoTypeAdapter.notifyDataSetChanged();
                    qrCodeListAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.btnRemove:
                QRCode qrCode = (QRCode) o;
                Spannable wordToSpan = new SpannableString(qrCode.getCode());
                wordToSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorText)), 0, wordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordToSpan.setSpan(new ForegroundColorSpan(android.graphics.Typeface.BOLD), 0, wordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                dialogConfirmRemove = new DialogConfirm("Xác nhận xóa mã " + wordToSpan, null, getString(R.string.btnConfirm), view -> {
                    handleRemoveQRCode(qrCode);
                    dialogConfirmRemove.dismiss();
                });
                dialogConfirmRemove.show(getChildFragmentManager(), null);
                detailNotShipVM.getIsEditMode().setValue(true);
                break;
        }
    }

    private void handleRemoveQRCode(QRCode qrCode) {
        List<QRCode> qrList = detailNotShipVM.getQrCodeList().getValue();
        int indexClickRemove = qrList.indexOf(qrCode);

        qrList.remove(indexClickRemove);
        qrCodeListAdapter.notifyItemRemoved(indexClickRemove);
        detailNotShipVM.handleRemoveQRCode(qrCode, qrList);

        mBinding.iclReceiveGoods.tvActualNumberCargoReceive.setText(MessageFormat.format("{0} cargo", qrList.size()));
    }


    private void getDataBundle() {
        Intent intent = getActivity().getIntent();
        if (intent.getExtras() != null) {
            type = intent.getExtras().getInt(Constants.KEY_PAGE);
            biddingId = intent.getExtras().getInt(OBJECT_SERIALIZABLE);
            detailNotShipVM.getOrderDetailList(biddingId);
        }
    }

}

