package com.longhaul.driver.base;

public interface IModel {
    String getModelName();
}
