package com.longhaul.driver.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.longhaul.driver.R;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.FragmentMapsBinding;
import com.longhaul.driver.model.OrderDetail;
import com.longhaul.driver.utils.BindingAdapterUtils;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.utils.ImageUtils;
import com.longhaul.driver.utils.ToastUtils;
import com.longhaul.driver.utils.Utils;
import com.longhaul.driver.viewmodel.DetailNotShipVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.longhaul.driver.utils.StringUtils.API_GOOGLE_MAP_DESTINATION;
import static com.longhaul.driver.utils.StringUtils.API_GOOGLE_MAP_SEARCH;

public class MapsFragment extends BaseActivity<FragmentMapsBinding> implements RoutingListener {
    FragmentMapsBinding fragmentMapsBinding;
    DetailNotShipVM detailNotShipVM;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private List<Polyline> mPolylines = null;
    private MarkerOptions mMarkerOptionsReceive, mMarkerOptionsReturns, driverMarkerOptions;
    private Marker receive, returns, driver;
    private GoogleMap mMap;
    private int type = 0;
    private OrderDetail orderDetail;
    private FusedLocationProviderClient fusedLocationClient;
    private LatLng currentDriverLocation, fromLatLng, toLatLng;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private String googleApiKey;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;

            if (ActivityCompat.checkSelfPermission(MapsFragment.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsFragment.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(MapsFragment.this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Drawable iconDrawable = getResources().getDrawable(R.drawable.ic_map_marker_driver);
                            BitmapDescriptor icon = ImageUtils.getMarkerIconFromDrawable(iconDrawable);
                            currentDriverLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            driverMarkerOptions = new MarkerOptions().position(currentDriverLocation).icon(icon);
                            driver = mMap.addMarker(driverMarkerOptions);
                            detailNotShipVM.getCurrentDriverLocation().setValue(currentDriverLocation);
                            if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED && fromLatLng != null) {
                                Findroutes(driverMarkerOptions.getPosition(), mMarkerOptionsReceive.getPosition());
                                detailNotShipVM.getDrivingDistanceAndTime("driving", "less_driving", currentDriverLocation, fromLatLng, googleApiKey);
                            } else if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED && toLatLng != null) {
                                Findroutes(driverMarkerOptions.getPosition(), mMarkerOptionsReturns.getPosition());
                                detailNotShipVM.getDrivingDistanceAndTime("driving", "less_driving", currentDriverLocation, toLatLng, googleApiKey);
                            }
                        }
                    });
            detailNotShipVM.getOrderDetail().observe(MapsFragment.this, orderDetail -> {
                Drawable iconDrawable = getResources().getDrawable(R.drawable.location);
                BitmapDescriptor icon = ImageUtils.getMarkerIconFromDrawable(iconDrawable);
                fromLatLng = new LatLng(orderDetail.getFromDepot().getLatitude(), orderDetail.getFromDepot().getLongitude());
                toLatLng = new LatLng(orderDetail.getToDepot().getLatitude(), orderDetail.getToDepot().getLongitude());
                mMarkerOptionsReceive = new MarkerOptions().position(fromLatLng).title(orderDetail.getFromDepot().getAddress()).icon(icon);
                mMarkerOptionsReturns = new MarkerOptions().position(toLatLng).title(orderDetail.getToDepot().getAddress());
                receive = googleMap.addMarker(mMarkerOptionsReceive);
                returns = googleMap.addMarker(mMarkerOptionsReturns);
                mMap.setOnCameraIdleListener(() -> {
//                    LatLng center = mMap.getCameraPosition().target;
//                    getAddressFromLocation(MapsFragment.this, center.latitude, center.longitude);
                });
                if (type != EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
                    Findroutes(mMarkerOptionsReceive.getPosition(), mMarkerOptionsReturns.getPosition());
                }

                //zom vừa 2 điểm receive và returns Vào google map;
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(receive.getPosition());
                builder.include(returns.getPosition());
                LatLngBounds bounds = builder.build();
                int padding = 300; // offset from edges of the map in pixels
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mMap.moveCamera(cu);
                    }
                });
            });


        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        fragmentMapsBinding = (FragmentMapsBinding) binding;
        detailNotShipVM = (DetailNotShipVM) viewModel;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        googleApiKey = AppController.getInstance().getSharePre().getString(Constants.GOOGLE_API_KEY_GEOCODE, "");
        getDataBundle();
        initGoogleMap();
        initGooglePlaceAutoComplete();
        subscribeUI();
        initLocationRequest();
    }

    private void initGoogleMap() {
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
        View mapView = (View) mapFragment.getView();
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, Utils.getScreenHeight() / 3);
            locationButton.setLayoutParams(layoutParams);

            int ZoomControl_id = 0x1;
            int MyLocation_button_id = 0x2;

            // Find ZoomControl view
            View zoomControls = mapFragment.getView().findViewById(ZoomControl_id);

            if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                // ZoomControl is inside of RelativeLayout
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();

                // Align it to - parent top|left
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                params.setMargins(0, 0, 30, Utils.getScreenHeight() / 5);
                zoomControls.setLayoutParams(params);
            }
        }
    }

    private void initLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                Location location = locationResult.getLastLocation();
                currentDriverLocation = new LatLng(location.getLatitude(), location.getLongitude());
                detailNotShipVM.getCurrentDriverLocation().setValue(currentDriverLocation);
            }
        };
    }

    private void initGooglePlaceAutoComplete() {
        if (!Places.isInitialized()) {
            Places.initialize(this, API_GOOGLE_MAP_SEARCH, Locale.getDefault());
        }
        PlacesClient placesClient = Places.createClient(this);

    }

    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String address = place.getAddress();
                    fragmentMapsBinding.tvResultSearch.setText(address);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16);
                    mMap.animateCamera(cameraUpdate);
                    mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getAddress()));
                }
                // do query with address
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Toast.makeText(this, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    // hàm tìm đường
    public void Findroutes(LatLng receive, LatLng returns) {
        if (receive == null || returns == null) {
            ToastUtils.showToast("Unable to get location");
        } else {
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(receive, returns)
                    .key(googleApiKey) // key tìm đường do google cấp riêng.
                    .build();
            routing.execute();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_maps;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailNotShipVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if(e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (mPolylines != null) {
            mPolylines.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        mPolylines = new ArrayList<>();
        // vẽ tuyến đường vào bản đồ bằng polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.colorAccent_v2));
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                mPolylines.add(polyline);
            }
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            case R.id.btnSearch:
            case R.id.tvResultSearch:
                onSearchCalled();
                break;
        }

    }

    private void getDataBundle() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            type = intent.getExtras().getInt(Constants.KEY_PAGE);
            String jsonString = intent.getExtras().getString(Constants.OBJECT_SERIALIZABLE);
            Gson gson = new GsonBuilder().create();
            orderDetail = gson.fromJson(jsonString, OrderDetail.class);
            detailNotShipVM.setOrderDetail(orderDetail);
            detailNotShipVM.getOrderStatus().setValue(type);
        }
    }

    private void subscribeUI() {
        detailNotShipVM.getOrderStatus().observe(this, orderStatus ->
                BindingAdapterUtils.setTitleToolBarDetail(fragmentMapsBinding.tvMapTitle, orderStatus + ""));
        detailNotShipVM.getCurrentDriverLocation().observe(this, location -> {


        });

        detailNotShipVM.getLeg().observe(this, leg -> {
            if (driver != null) {
                driver.remove();
            }
            if (currentDriverLocation != null) {
                Drawable iconDrawable = getResources().getDrawable(R.drawable.ic_map_marker_driver);
                BitmapDescriptor icon = ImageUtils.getMarkerIconFromDrawable(iconDrawable);
                driverMarkerOptions = new MarkerOptions().position(currentDriverLocation).title(leg.getDuration().getText() + " - " + leg.getDistance().getText()).icon(icon);
                driver = mMap.addMarker(driverMarkerOptions);
//                if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
//                    Findroutes(driverMarkerOptions.getPosition(), mMarkerOptionsReceive.getPosition());
//                } else if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
//                    Findroutes(driverMarkerOptions.getPosition(), mMarkerOptionsReturns.getPosition());
//                }

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
//        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
//        fusedLocationClient.requestLocationUpdates(
//                locationRequest,
//                locationCallback,
//                Looper.myLooper()
//        );
    }
}