package com.longhaul.driver.base;


import com.longhaul.driver.model.GeoCodedWayPoints;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SOService {
    @GET("json")
    Call<GeoCodedWayPoints> getPoints(@Query("mode") String mode,@Query("transit_routing_preference") String transit_routing_preference,@Query("origin") String origin,@Query("destination") String destination,@Query("key") String key);
}
