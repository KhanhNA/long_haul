package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

public class BiddingDetail extends TsBaseModel {
    private Integer id;
    private String create_date_bidding;
    private DriverInfo driver_info;
    private Double price;
    private Double total_weight;
    private Integer quantity;
    private Double distance;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private SizeCargo size_cargo;
    private String actual_time;

    public String getActual_time() {
        return actual_time;
    }

    public Integer getId() {
        return id;
    }

    public String getCreate_date_bidding() {
        return create_date_bidding;
    }

    public DriverInfo getDriver_info() {
        return driver_info;
    }

    public Double getPrice() {
        return price;
    }

    public Double getTotal_weight() {
        return total_weight;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Double getDistance() {
        return distance;
    }

    public FromDepot getFrom_depot() {
        return from_depot;
    }

    public ToDepot getTo_depot() {
        return to_depot;
    }

    public SizeCargo getSize_cargo() {
        return size_cargo;
    }

    public class ToDepot {

        private String name_to_depot;
        private Double to_latitude;
        private Double to_longitude;
        private String to_receive_time;

        public String getName_to_depot() {
            return name_to_depot;
        }

        public Double getTo_latitude() {
            return to_latitude;
        }

        public Double getTo_longitude() {
            return to_longitude;
        }

        public String getTo_receive_time() {
            return to_receive_time;
        }
    }

    public class DriverInfo {

        private Integer id;
        private String driver_name;
        private String driver_phone_number;
        private String lisence_plate;
        private String idCard;

        public Integer getId() {
            return id;
        }

        public String getDriver_name() {
            return driver_name;
        }

        public String getDriver_phone_number() {
            return driver_phone_number;
        }

        public String getLisence_plate() {
            return lisence_plate;
        }

        public String getIdCard() {
            return idCard;
        }
    }

    public class FromDepot {

        private String name_from_depot;
        private Double from_latitude;
        private Double from_longitude;
        private String from_receive_time;

        public String getName_from_depot() {
            return name_from_depot;
        }

        public Double getFrom_latitude() {
            return from_latitude;
        }

        public Double getFrom_longitude() {
            return from_longitude;
        }

        public String getFrom_receive_time() {
            return from_receive_time;
        }
    }

    public class SizeCargo {

        private Double length;
        private Double width;
        private Double height;
        private String long_unit;
        private Object weight_unit;

        public Double getLength() {
            return length;
        }

        public Double getWidth() {
            return width;
        }

        public Double getHeight() {
            return height;
        }

        public String getLong_unit() {
            return long_unit;
        }

        public Object getWeight_unit() {
            return weight_unit;
        }
    }
}
