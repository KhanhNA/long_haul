package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingDetail extends TsBaseModel {
    private Integer id;
    private String type;// 0 trả, 1 nhận - ngược với kho.
    private String order_type; // 0 cố định, 1 phát sinh.
    private String insurance_name;
    private List<BillService> service_name;
    private String routing_plan_detail_code;
    List<BillPackage> list_bill_package_import;
    List<BillPackage> list_bill_package_export;

}
