package com.longhaul.driver.ui.viewcommon;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;


import com.longhaul.driver.R;
import com.longhaul.driver.base.Constants;
import com.tsolution.base.BR;

public class FilterCargoDialog extends Dialog {
    ViewDataBinding binding;
    private Context mContext;
    private BiddingBottomSheet.OnclickSortListBidding mOnclickSortListBidding;

    public FilterCargoDialog(@NonNull Context context, BiddingBottomSheet.OnclickSortListBidding onclickSortListBidding) {
        super(context);
        mContext = context;
        mOnclickSortListBidding = onclickSortListBidding;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.filter_cargo_dialog, null, false);
        setContentView(binding.getRoot());
        RadioButton radioButton = binding.getRoot().findViewById(Constants.getSortCargo());
        radioButton.setChecked(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);
        this.getWindow().setGravity(Gravity.CENTER);
        int windowsScale = this.getContext().getResources().getDisplayMetrics().widthPixels;
        windowsScale = (int) ((float) windowsScale * 1f);
        this.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initListener();

    }

    private void initListener() {
        binding.setVariable(BR.listener, this);
        RadioGroup radioGroup = binding.getRoot().findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.btnAllCargo:
                   Constants.setSortCargo(R.id.btnAllCargo);
                    mOnclickSortListBidding.onSortListBidding(R.id.btnAllCargo);
                    dismiss();
                    break;

                case R.id.btnNoBidding:
                    Constants.setSortCargo(R.id.btnNoBidding);
                    mOnclickSortListBidding.onSortListBidding(R.id.btnNoBidding);
                    dismiss();
                    break;


                case R.id.btnBidding:
                    Constants.setSortCargo(R.id.btnBidding);
                    mOnclickSortListBidding.onSortListBidding(R.id.btnBidding);
                    dismiss();
                    break;
            }
        });
    }
}
