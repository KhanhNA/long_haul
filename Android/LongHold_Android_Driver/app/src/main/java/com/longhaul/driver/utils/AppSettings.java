package com.longhaul.driver.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import java.util.Locale;

public class AppSettings {
    private static final String TAG = "APP_SETTINGS";

    public static void setLang(Context context, String lang) {
        lang = lang.equalsIgnoreCase("ar") ? "ar" : "en";
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("txt", lang).apply();
        Configuration newConfig = new Configuration(context.getResources().getConfiguration());
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        newConfig.locale = locale;
        newConfig.setLayoutDirection(locale);
        context.getResources().updateConfiguration(newConfig, null);
    }

    public static String getLang(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("txt", "ar");
    }
}