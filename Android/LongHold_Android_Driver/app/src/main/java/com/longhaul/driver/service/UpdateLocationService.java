package com.longhaul.driver.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.Nullable;


import java.util.Timer;
import java.util.TimerTask;

public class UpdateLocationService extends android.app.Service {
    private Timer timer = new Timer();
    Context context;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        doSomethingRepeatedly();
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @SuppressLint("MissingPermission")
    private void doSomethingRepeatedly() {
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
//                        RealmDTB.getInstance(context).readLocationRecords();
                    }
                });

            }
        }, 0, 12000);
    }

}
