package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingCargo extends TsBaseModel {
  private Integer id;
  private String cargo_number;
  private String status;
  private Integer bidding_order_id;
  private String confirm_time;
  private String time_countdown;
}