package com.longhaul.driver.ui.adapter;

import android.widget.ImageButton;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.longhaul.driver.BR;
import com.longhaul.driver.R;
import com.longhaul.driver.utils.BindingAdapterUtils;
import com.longhaul.driver.viewmodel.DetailNotShipVM;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

public class QRCodeAdapter<T> extends BaseQuickAdapter<T, BaseDataBindingHolder> implements LoadMoreModule {
    private AdapterListener listenerAdapter;
    private List<T> data;
    private DetailNotShipVM detailNotShipVM;
    private ImageButton btnRemove;

    public QRCodeAdapter(@LayoutRes int itemLayoutId) {
        super(itemLayoutId);
    }

    public QRCodeAdapter(@LayoutRes int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public QRCodeAdapter(@LayoutRes int layoutResId, @Nullable List<T> data, AdapterListener listener, DetailNotShipVM detailNotShipVM) {
        super(layoutResId, data);
        this.listenerAdapter = listener;
        this.data = data;
        this.detailNotShipVM = detailNotShipVM;
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, T o) {
        //  Binding
        ViewDataBinding binding = holder.getDataBinding();
        if (binding != null) {
            binding.setVariable(BR.viewHolder, o);
            if (listenerAdapter != null) {
                binding.setVariable(BR.listener, this.listenerAdapter);
            }
            binding.setVariable(BR.viewModel, detailNotShipVM);
            binding.executePendingBindings();
            btnRemove = holder.getView(R.id.btnRemove);
        }
    }

    public void setVisibilityRemove(boolean check){
        BindingAdapterUtils.setVisibilityButtonRemoveQR(btnRemove, check);
        notifyDataSetChanged();
    }
}
