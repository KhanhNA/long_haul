package com.longhaul.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Location extends RealmObject {
    public static final String PROPERTY_NAME = "isCheckPush";
    String van_id;
    String latitude;
    String longitude;
    @PrimaryKey
    @Required
    String create_time;
    boolean isCheckPush = false;

    @Override
    public String toString() {
        return "Location{" +
                "van_id='" + van_id + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", create_time='" + create_time + '\'' +
                ", isCheckPush=" + isCheckPush +
                '}';
    }
}
