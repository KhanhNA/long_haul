package com.longhaul.driver.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.longhaul.driver.R;
import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.databinding.ActivityMainBinding;
import com.longhaul.driver.fragmentnavigation.FragmentNavigation;
import com.longhaul.driver.fragmentnavigation.FragmentNavigationImp;
import com.longhaul.driver.fragmentnavigation.FragmentNavigationProvider;
import com.longhaul.driver.model.BiddingShipping;
import com.longhaul.driver.model.NotificationModel;
import com.longhaul.driver.service.SaveLocationService;
import com.longhaul.driver.service.UpdateLocationService;
import com.longhaul.driver.ui.fragment.DetailNotShipFragment;
import com.longhaul.driver.ui.fragment.HomeFragment;
import com.longhaul.driver.ui.fragment.HistoryFragment;
import com.longhaul.driver.ui.fragment.NotificationFragment;
import com.longhaul.driver.ui.viewcommon.NonSwipeableViewPager;
import com.longhaul.driver.ui.viewcommon.PagerAdapter;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.utils.StringUtils;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import static com.longhaul.driver.base.Constants.DATA_PASS_FRAGMENT;
import static com.longhaul.driver.base.Constants.KEY_PAGE;
import static com.longhaul.driver.base.Constants.OBJECT_SERIALIZABLE;
import static com.longhaul.driver.base.Constants.REQUEST_UPDATE;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements FragmentNavigationProvider {

    public FragmentNavigation mFragmentNavigation;
    private ActivityMainBinding activityMainBinding;
    private int mIndexPage = 0;
    private NotificationModel notificationModel = new NotificationModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = binding;
        provideNavigation();

        BottomNavigationView navigation = findViewById(R.id.navigationHome);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        startIntentS();

        getTokenID();
        initViewPagerTablayout();
        actionNotification();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    // sự kiện bottom Home
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                if (mIndexPage != 0)
                    activityMainBinding.viewPager.setCurrentItem(0, false);
                mIndexPage = 0;
                return true;
            case R.id.navigation_history:
                if (mIndexPage != 1)
                    activityMainBinding.viewPager.setCurrentItem(1, false);
                mIndexPage = 1;
                return true;
            case R.id.navigation_notif:
                if (mIndexPage != 2)
                    activityMainBinding.viewPager.setCurrentItem(2, false);
                mIndexPage = 2;
                return true;
            case R.id.navigation_acc:

                return true;

        }
        return false;
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public FragmentNavigation provideNavigation() {
        mFragmentNavigation = new FragmentNavigationImp(this, R.id.frameHome);
        return mFragmentNavigation;
    }

    private void startIntentS() {
        Intent saveLocationService = new Intent(this, SaveLocationService.class);
        this.startService(saveLocationService);
        Intent upLocationService = new Intent(this, UpdateLocationService.class);
        this.startService(upLocationService);
    }

    private void getTokenID() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    Log.e("duongnk", "getTokenID: " + token);
                    // Luu token
                    AppController.getInstance().getEditor().putString("token_fb", token).apply();

                    DriverApi.updateToken(token, new IResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            Log.e("duongnk", "onSuccess: updateToken");
                        }

                        @Override
                        public void onFail(Throwable error) {

                        }
                    });
                });
    }

    private void initViewPagerTablayout() {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment());
        adapter.addFragment(new HistoryFragment());
        adapter.addFragment(new NotificationFragment());
        activityMainBinding.viewPager.setAdapter(adapter);
        activityMainBinding.viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.NONE);
        activityMainBinding.viewPager.setOffscreenPageLimit(4);

    }

    private void actionNotification() {
        int action = getIntent().getIntExtra(Constants.INTENT_NOTIFICATION, 0);
        if (action == 0) return;
        if (action == EnumBidding.EnumActionNotification.ACTION_DETAIL) {
            notificationModel = (NotificationModel) getIntent().getSerializableExtra(Constants.EXTRA_DATA);
            confirmRedNotification(notificationModel.getId());
            Bundle args = new Bundle();
            Intent intent = new Intent(this, CommonActivity.class);
            intent.putExtra(Constants.FRAGMENT, DetailNotShipFragment.class);
            intent.putExtra(DATA_PASS_FRAGMENT, args);
            intent.putExtra(KEY_PAGE, 2);
            intent.putExtra(OBJECT_SERIALIZABLE, notificationModel.getItem_id());
            startActivity(intent);
        }

    }

    public void confirmRedNotification(int id) {
        DriverApi.confirmRedNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}