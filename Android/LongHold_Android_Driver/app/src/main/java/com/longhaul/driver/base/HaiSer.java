package com.longhaul.driver.base;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.longhaul.driver.utils.StringUtils;
import com.ns.odoolib_retrofit.adapter.MySerializer;
import com.ns.odoolib_retrofit.model.OdooRelType;

import java.lang.reflect.Type;

public class HaiSer<T> extends MySerializer<T> {
    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        if (typeOfSrc == OdooRelType.class) {
            OdooRelType odooRelType = (OdooRelType)src;
            if (odooRelType != null && odooRelType.size() >= 1) {
                if(StringUtils.isNumeric(odooRelType.get(0).toString())){
                    Long id = odooRelType.getId();
                    return new JsonPrimitive(id);
                }
            }
            return null;
        } else {
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(src, typeOfSrc);
            return element;
        }
    }
}
