package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Company extends TsBaseModel {
    private Integer id;
    private String name;
    private String tax_id;
    private String phone;
    private String address;

}
