package com.longhaul.driver.model;



import com.tsolution.base.TsBaseModel;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductType extends TsBaseModel {
    private Integer id;
    private String name_seq;
    private String name;
    private BigDecimal net_weight;
    private String description;
    private String status;

    public Integer getId() {
        return id;
    }

    public String getName_seq() {
        return name_seq;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getNet_weight() {
        return net_weight;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }
}
