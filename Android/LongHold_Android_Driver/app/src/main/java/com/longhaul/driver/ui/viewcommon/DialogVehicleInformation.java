package com.longhaul.driver.ui.viewcommon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;


import com.longhaul.driver.R;
import com.longhaul.driver.databinding.LayoutDialogVehicleInformationBinding;

public class DialogVehicleInformation extends DialogFragment {
    private String title, name,phone,idCargo,actionTitleNO,actionTitleOK;
    private String CMT;
    private boolean isHideCancelButton = true;
    private View.OnClickListener onClickListener;


    public DialogVehicleInformation(String title, String name, String phone, String idCargo, String CMT, String actionTitleNO, String actionTitleOK, boolean isHideCancelButton, View.OnClickListener onClickListener) {
        this.title = title;
        this.name = name;
        this.phone = phone;
        this.idCargo = idCargo;
        this.CMT = CMT;
        this.actionTitleNO = actionTitleNO;
        this.actionTitleOK = actionTitleOK;
        this.isHideCancelButton = isHideCancelButton;
        this.onClickListener = onClickListener;
    }

    public DialogVehicleInformation(String title, String name, String phone, String idCargo, String CMT, String actionTitleNO, String actionTitleOK, View.OnClickListener onClickListener) {
        this.title = title;
        this.name = name;
        this.phone = phone;
        this.idCargo = idCargo;
        this.CMT = CMT;
        this.actionTitleNO = actionTitleNO;
        this.actionTitleOK = actionTitleOK;
        this.onClickListener = onClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LayoutDialogVehicleInformationBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_dialog_vehicle_information, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.custom_dialog_confirm_bidding));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.dialogCancel.setText(actionTitleNO);
        binding.dialogCancel.setOnClickListener(v -> this.dismiss());
        binding.txtTitle.setText(title);
        binding.dialogEdt.setOnClickListener(onClickListener);
        if (isHideCancelButton==false){
            binding.dialogCancel.setVisibility(View.GONE);
        }else { binding.dialogCancel.setVisibility(View.VISIBLE);}
        binding.dialogEdtName.setText(name);
        binding.dialogEdtPhone.setText(phone);
        binding.dialogEdtIDCargo.setText(idCargo);
        binding.dialogEdtCMT.setText(CMT+"");
        binding.dialogCancel.setText(actionTitleNO);
        binding.dialogEdt.setText(actionTitleOK);
        return binding.getRoot();
    }

}
