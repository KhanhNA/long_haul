package com.longhaul.driver.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.longhaul.driver.BR;
import com.tsolution.base.TsBaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

public class XBaseAdapterBid extends RecyclerView.Adapter<XBaseAdapterBid.ViewHolder> implements AdapterListener {
    private AdapterListener adapterListener;
    private List datas;
    private int layoutItem;
    ViewDataBinding binding;
    private Context mContext;

    public void setUpDateBid(List datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }


    public XBaseAdapterBid(@LayoutRes int item, List lst, AdapterListener listener) {
        this.layoutItem = item;
        this.datas = lst;
        this.adapterListener = listener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), this.layoutItem, viewGroup, false);
        return new ViewHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        TsBaseModel bm = (TsBaseModel) (this.datas).get(position);
        bm.index = position + 1;
        viewHolder.bind(bm);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding itemBidBinding;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.itemBidBinding = view;
            view.getRoot().setOnLongClickListener(v -> {
                adapterListener.onItemLongClick(v, datas.get(getAdapterPosition()));
                return true;
            });

            view.getRoot().setOnClickListener(v -> {
                adapterListener.onItemClick(v, datas.get(getAdapterPosition()));
            });
        }
        public void bind(Object obj) {
            itemBidBinding.setVariable(BR.viewHolder, obj);
            itemBidBinding.setVariable(BR.listener, adapterListener);
            itemBidBinding.executePendingBindings();
        }

    }


    @Override
    public int getItemCount() {
        return this.datas == null ? 0 : (this.datas).size();
    }

    public void onItemClick(View v, Object o) {
        if (adapterListener != null) {
            adapterListener.onItemClick(v, o);
        }
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }
}
