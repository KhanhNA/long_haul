package com.longhaul.driver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.model.BiddingInformation;
import com.longhaul.driver.model.BiddingShipping;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class HistoryVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<List<BiddingInformation>>> mHistory = new MutableLiveData<>();
    private OdooResultDto<List<BiddingInformation>> mHistoryUpdate;
    private MutableLiveData<List<BiddingShipping>> mHistoryList = new MutableLiveData<>();




    public HistoryVM(@NonNull Application application) {
        super(application);
        mHistoryUpdate = new OdooResultDto<>();
        mHistory.setValue(mHistoryUpdate);
    }

    public MutableLiveData<List<BiddingShipping>> getmHistoryList() {
        return mHistoryList;
    }

    public MutableLiveData<OdooResultDto<List<BiddingInformation>>> getHistory() {
        return mHistory;
    }

    // TODO: 8/23/2020 getList history
    public void listhistory(String fromDate, String toDate, String txtSearch, Integer offset, RunUi runUi) {
        DriverApi.getHistory(fromDate, toDate, txtSearch, offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {
//                if (o == null) {
//                    runUi.run(Constants.FAIL_API);
//                    return;
//                }
                OdooResultDto<List<BiddingInformation>> response = (OdooResultDto<List<BiddingInformation>>) o;


                if (mHistory.getValue() != null && mHistory.getValue().getRecords() != null) {
                    mHistory.getValue().getRecords().addAll(response.getRecords());
                    mHistory.getValue().setLength(response.getLength());
                } else mHistory.setValue((OdooResultDto<List<BiddingInformation>>) o);
                /**

                 */
                ArrayList<BiddingShipping> shippingArrayList = new ArrayList<>();
                // lấy ra các list bid trong Records response
                for (int i = 0; i < mHistory.getValue().getRecords().size(); i++) {
                    // lấy ra các bid của list con trong Records response
                    for (int j = 0; j < mHistory.getValue().getRecords().get(i).size(); j++) {
                        // tạo ra các biddingShipping
                        BiddingShipping history = new BiddingShipping();
                        BiddingInformation biddingInformation = mHistory.getValue().getRecords().get(i).get(j);
                        // set toDepot vs FromDepot cho bid Shipping
                        if (j == 0)
                            history.setToDepotFromDepot(biddingInformation.getFrom_depot().getDepot_code() + "-" + biddingInformation.getTo_depot().getDepot_code());
                        history.setTypeBidding(1);
                        history.setBiddingInformation(biddingInformation);
                        shippingArrayList.add(history);
                    }
                    mHistoryList.setValue(shippingArrayList);
                }

                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.GET_HISTORY_ORDER_ERROR);
            }
        });
    }

    public void clearData() {
        getmHistoryList().setValue(null);
        mHistoryList.setValue(null);
        mHistory.setValue(null);
    }
}
