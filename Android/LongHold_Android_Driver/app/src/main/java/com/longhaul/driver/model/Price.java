package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

public class Price  extends TsBaseModel {

    private Integer id;

    private Integer cargoId;

    private Integer price;

    private String status;

    public Integer getId() {
        return id;
    }

    public Integer getCargoId() {
        return cargoId;
    }

    public Integer getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }
}