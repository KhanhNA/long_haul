package com.longhaul.driver.base;



import com.longhaul.driver.R;
import com.longhaul.driver.utils.EnumBidding;

public class Constants {
    public static final String FRAGMENT = "FRAGMENT";
    public static final String UPDATE_ADAPTER = "update_adapter";
    public static final String USER_NAME = "USER_NAME";
    public static final String MK = "MK";
    public static final String DISTANCE_CHECK_POINT = "DISTANCE_CHECK_POINT";
    public static final String GOOGLE_API_KEY_GEOCODE = "google_api_key_geocode";
    public static final String KEY_PAGE = "key_page";
    public static final String STATUS_NOTIFICATION = "status_notification";
    public static final String LIST_CARGO_COMPARE ="list_cargo_compare" ;
    public static final String LIST_CARGO_VALIDATE ="list_cargo_validate" ;
    public static final String GET_HISTORY_ORDER_SUCCES ="get_history_order_success" ;
    public static final String GET_HISTORY_ORDER_ERROR ="get_history_order_error" ;
    public static int STATUS_SORT = R.id.btnAllCargo;
    public static final String SUCCESS_API = "200";
    public static final String FAIL_API = "-1";
    public static final String DATA_PASS_FRAGMENT = "da_pass_fragment";
    public static final String OBJECT_SERIALIZABLE = "object_serializable";
    public static final int KEY_START_ACTIVITY_FOR_RESULTS = 101;
    public static int STATIC_SELECT_SORT = EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING;
    public static final String SUCCESS_PRICE = "success_price";
    public static final int KEY_EDIT_INFO = 404;
    public static int SORT_CARGO = R.id.btnAllCargo;
    public static final String DELETE_CARGO = "deleteCargo";
    public static final String EDIT_CARGO = "editCargo";
    public static final String VIEW_INFOR_CARGO = "viewCargo";
    public static final int REQUEST_UPDATE = 1;
    public static final String INTENT_NOTIFICATION = "intern_notification";
    public static final String EXTRA_DATA = "extra_data";


    public enum CODE {
        SUCCESS(200), NOT_FOUND(404), ERROR_INTERNAL(500), AUTHEN_FAIL(401), USER_DEFIEND(-200), FATAL_ERROR(-1);

        private Integer code;

        CODE(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }

    public final static String FIRST_USE = "FIRST_USE";

    public final static String SPACE = " ";

    public static final int LANG_EN = 2;

    public static int getStatusSort() {
        return STATUS_SORT;
    }

    public static void setStatusSort(int statusSort) {
        STATUS_SORT = statusSort;
    }

    public static int getSortCargo() {
        return SORT_CARGO;
    }

    public static void setSortCargo(int sortCargo) {
        SORT_CARGO = sortCargo;
    }
}
