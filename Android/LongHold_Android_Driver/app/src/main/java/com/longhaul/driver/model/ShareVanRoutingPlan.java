package com.longhaul.driver.model;

import com.longhaul.driver.base.AppController;

import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShareVanRoutingPlan extends TsBaseModel {
    private Integer id;
    private String routing_plan_day_code;
    private Integer order_number;

    private String address;
    private String warehouse_name;
    private String phone;
    /*
    [('0','Chưa xác nhận'),
    ('1','Lái xe xác nhận'),
    ('2','Đã xác nhận')]
    */
    private Integer status;

    private OdooDateTime expected_from_time;
    private OdooDateTime expected_to_time;
    private OdooDate date_plan;

    private Float latitude;
    private Float longitude;

    private Vehicle vehicle;

    private List<RoutingDetail> routing_plan_day_detail;


    public String getDatePlanStr(){
        if(this.date_plan == null){
            return "";
        }
        return AppController.formatDate.format(date_plan);
    }

    public String getExpectedFromTimeStr(){
        if(this.expected_from_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_from_time);
    }

    public String getExpectedToTimeStr(){
        if(this.expected_to_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_to_time);
    }

}
