package com.longhaul.driver.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.longhaul.driver.R;
import com.longhaul.driver.base.BaseAdapterV3;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.FragmentDetailCargoTypeBinding;
import com.longhaul.driver.model.CargoTypes;
import com.longhaul.driver.model.MessageEvent;
import com.longhaul.driver.model.QRCode;
import com.longhaul.driver.viewmodel.DetailNotShipVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class DetailCargoTypeFragment extends BaseFragment implements ActionsListener, AdapterListener {
    private static DetailCargoTypeFragment detailCargoTypeFragment;
    DetailNotShipVM detailNotShipVM;
    FragmentDetailCargoTypeBinding detailCargoTypeBinding;
    private BaseAdapterV3 qrCodeListAdapter;
    private SharedData sharedData;

    public static DetailCargoTypeFragment getInstance() {
        if (detailCargoTypeFragment == null) {
            detailCargoTypeFragment = new DetailCargoTypeFragment();
        }
        return detailCargoTypeFragment;
    }

    public void setSharedData(SharedData data) {
        sharedData = data;
    }

    public BaseAdapterV3 getQrCodeListAdapter() {
        return qrCodeListAdapter;
    }

    @Override
    public void action(Object... objects) {

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail_cargo_type;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailNotShipVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initView();
        subscribeUI();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void initView() {
        detailNotShipVM = (DetailNotShipVM) viewModel;
        detailCargoTypeBinding = (FragmentDetailCargoTypeBinding) binding;
        getDataBundle();
        detailCargoTypeFragment = this;
        // key các page trong viewpager Bidding
        //init adapter bidding

    }

    private void subscribeUI() {
        detailNotShipVM.getQrCodeList().observe(getViewLifecycleOwner(), qrCodeList -> {
            qrCodeListAdapter = new BaseAdapterV3(R.layout.item_qr, qrCodeList, this);
            detailCargoTypeBinding.rvQRList.setAdapter(qrCodeListAdapter);
        });
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        switch (id) {
            case R.id.btnRemove:
                QRCode qrCode = (QRCode) o;
                List<QRCode> qrList = detailNotShipVM.getQrCodeList().getValue();
                int indexClick = qrList.indexOf(qrCode);
                qrList.remove(indexClick);
                qrCodeListAdapter.notifyItemRemoved(indexClick);
                detailCargoTypeBinding.tvActualNumberCargoReceive.setText(qrList.size() + " cargo");
                if (qrList.isEmpty()) {
                    detailCargoTypeBinding.tvQRCode.setVisibility(View.GONE);
                    sharedData.clickDelete(false, detailNotShipVM.getCargoType().getValue());
                } else {
                    sharedData.clickDelete(true, detailNotShipVM.getCargoType().getValue());
                }
                break;
        }
    }

    private void getDataBundle() {
        if (getArguments() != null) {
            CargoTypes cargoType = (CargoTypes) getArguments().getSerializable(Constants.KEY_PAGE);
            if (cargoType != null) {
                updateData(cargoType);
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        updateData(event.getCargoTypes());
    }

    public void updateData(CargoTypes cargoType) {
        detailNotShipVM.getCargoType().setValue(cargoType);
        detailNotShipVM.setQrCodeList(cargoType);
        List<QRCode> qrList = detailNotShipVM.getQrCodeList().getValue();
        detailCargoTypeBinding.tvActualNumberCargoReceive.setText(qrList.size() + " cargo");
        if (qrList.isEmpty()) {
            detailCargoTypeBinding.tvQRCode.setVisibility(View.GONE);
        }
    }

    interface SharedData {
        void clickDelete(boolean isChange, CargoTypes cargoTypes);
    }

}