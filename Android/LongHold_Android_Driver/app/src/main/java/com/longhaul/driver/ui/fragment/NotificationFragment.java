package com.longhaul.driver.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.longhaul.driver.R;
import com.longhaul.driver.base.BaseAdapterV3;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.NotificationFragmentBinding;
import com.longhaul.driver.model.NotificationModel;
import com.longhaul.driver.viewmodel.CustomLoadMoreView;

import com.longhaul.driver.viewmodel.NotificationVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

public class NotificationFragment extends BaseFragment {


    private NotificationVM mNotificationVM;
    private NotificationFragmentBinding mNotificationFragmentBinding;
    private int mOffset = 0;
    private BaseAdapterV3 mNotificationAdapter;
    private LinearLayoutManager linearLayoutManager;
    private boolean isRefresh;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh)
            refreshData();
    }


    @Override
    public int getLayoutRes() {
        return R.layout.notification_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mNotificationVM = (NotificationVM) viewModel;
        mNotificationFragmentBinding = (NotificationFragmentBinding) binding;
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        return view;
    }

    private void initView() {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mNotificationFragmentBinding.recyclerBidding.setLayoutManager(linearLayoutManager);
        mNotificationAdapter = new BaseAdapterV3(R.layout.item_notifi, mNotificationVM.getNotification().getValue().getRecords(), this);
        mNotificationFragmentBinding.recyclerBidding.setAdapter(mNotificationAdapter);
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mNotificationAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mNotificationAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mNotificationAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mNotificationFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mNotificationFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void offRefreshing() {
        mNotificationFragmentBinding.swipeLayout.setRefreshing(false);
        mNotificationAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void refreshData() {
        mNotificationVM.clearData();
        mOffset = 0;
        getData();
    }

    private void loadMore() {
        int totalRecord = mNotificationVM.getNotification().getValue().getTotal_record();
        if (mOffset < totalRecord) {
            getData();

        } else {
            //thông báo không còn gì để load
            mNotificationAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void getData() {
        mNotificationVM.requestGetNotification(mOffset, this::runUi);
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        NotificationModel notificationModel = (NotificationModel) o;
        switch (v.getId()) {
            case R.id.viewGroup:
                isRefresh = true;

                break;
        }
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                mOffset += 10;
                mNotificationAdapter.setList(Objects.requireNonNull(mNotificationVM.getNotification().getValue()).getRecords());
                mNotificationAdapter.notifyDataSetChanged();
                emptyData(mNotificationVM.getNotification().getValue().getRecords());
                offRefreshing();
                break;
            case Constants.FAIL_API:
                offRefreshing();
                if (mNotificationVM.getNotification().getValue() == null) return;
                emptyData(mNotificationVM.getNotification().getValue().getRecords());

                break;
        }
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mNotificationFragmentBinding.emptyData.setVisibility(View.GONE);
            mNotificationFragmentBinding.recyclerBidding.setVisibility(View.VISIBLE);
        } else {
            mNotificationFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            mNotificationFragmentBinding.recyclerBidding.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NotificationModel notificationModel) {
        mNotificationAdapter.addData(0, notificationModel);
        mNotificationFragmentBinding.recyclerBidding.scrollToPosition(0);
    }
}
