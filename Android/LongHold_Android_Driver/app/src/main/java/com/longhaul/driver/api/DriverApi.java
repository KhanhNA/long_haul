package com.longhaul.driver.api;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import com.longhaul.database.LocationApi;
import com.longhaul.database.RealmDTB;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.SOService;
import com.longhaul.driver.base.StaticData;
import com.longhaul.driver.model.BiddingInformation;
import com.longhaul.driver.model.ConfirmBidding;
import com.longhaul.driver.model.ConfirmBiddingResponse;
import com.longhaul.driver.model.Driver;
import com.longhaul.driver.model.GeoCodedWayPoints;
import com.longhaul.driver.model.NotificationModel;
import com.longhaul.driver.model.OrderDetail;
import com.longhaul.driver.model.PhoneLogin;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.BaseClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverApi extends BaseApi {

    public static void updateToken(String fcmToken, IResponse result) {
        StaticData.sessionCookie = mOdoo.getSessionCokie();
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("fcm_token", fcmToken);

            mOdoo.callRoute("/server/save_token", params, new SharingOdooResponse<OdooResultDto<String>>() {
                @Override
                public void onSuccess(OdooResultDto<String> obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * uId param
     * response :listener Callback Api
     *
     * @param uId
     * @param response
     */
    public static void getDriverInfo(Integer uId, IResponse response) {
        JSONObject params;
        try {
            // chuyền param
            params = new JSONObject();
            params.put("uid", 20);
            //  "/server/save_token" -> đường dẫn Api
            mOdoo.callRoute("/fleet/driver", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
                @Override
                public void onSuccess(OdooResultDto<Driver> obj) {
                    response.onSuccess(obj.getRecords().get(0));
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void UpdateDataLocation(ArrayList<LocationApi> action_logs) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("action_logs", new JSONArray(mGson.toJson(action_logs)));
            mOdoo.callRoute("/bidding/create_action_log", params, new SharingOdooResponse<Boolean>() {

                @Override
                public void onSuccess(Boolean aBoolean) {
                    Log.e("duongnk", "onSuccess: UpdateDataLocation");
                    if (aBoolean != null) {
                        if (AppController.getAppContext() != null)
                            Toast.makeText(AppController.getAppContext(), "Định vị vị trí thành công", Toast.LENGTH_SHORT).show();
                        RealmDTB.getInstance(null).updateEmployeeRecords();
                    } else {
                        if (AppController.getAppContext() != null)
                            Toast.makeText(AppController.getAppContext(), "Định vị vị trí thất bại", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    Log.e("duongnk", "onFail:UpdateDataLocation " + error.toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get_bidding_vehicle_information_for_login (API SHARING VAN MOBILE)
    public static void getBiddingVehicleInforLogin(String license_plate, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("license_plate", license_plate);

            mOdoo.callRoute("/bidding/get_bidding_vehicle_information_for_login", params, new SharingOdooResponse<OdooResultDto<PhoneLogin>>() {
                @Override
                public void onSuccess(OdooResultDto<PhoneLogin> mPhoneLogin) {
                    response.onSuccess(mPhoneLogin);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getListBiddingOrderShipping(String txt_search, int offset, int status, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("offset", offset);
            params.put("limit", 10);
            params.put("status", status);
            params.put("txt_search", txt_search);

            mOdoo.callRoute("/bidding/get_list_bidding_order_shipping", params, new SharingOdooResponse<OdooResultDto<List<BiddingInformation>>>() {
                @Override
                public void onSuccess(OdooResultDto<List<BiddingInformation>> listOdooResultDto) {
                    response.onSuccess(listOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmBidding(String bidding_order_id, String cargo_ids, String type, String confirm_time, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", bidding_order_id);
            params.put("cargo_ids", cargo_ids);
            params.put("type", type);
            params.put("confirm_time", confirm_time);
            mOdoo.callRoute("/bidding/driver_confirm_cargo_quantity", params, new SharingOdooResponse<OdooResultDto<ConfirmBiddingResponse>>() {
                @Override
                public void onSuccess(OdooResultDto<ConfirmBiddingResponse> biddingResponse) {
                    response.onSuccess(biddingResponse);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getOrderDetail(Integer id, IResponse response) {
        JSONObject params;
        try {
            // chuyền param
            params = new JSONObject();
            params.put("bidding_order_id", id);
            //  "/server/save_token" -> đường dẫn Api
            mOdoo.callRoute("/bidding/get_bidding_order_detail_by_id", params, new SharingOdooResponse<OdooResultDto<OrderDetail>>() {
                @Override
                public void onSuccess(OdooResultDto<OrderDetail> orderDetailOdooResultDto) {
                    response.onSuccess(orderDetailOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void getDrivingDistanceAndTime(Context context, String mode, String transit_routing_preference, LatLng origin, LatLng destination, String key, IResponse response) {
        BaseClient<SOService> soService  =  new BaseClient(context, AppController.BASE_URL_GOOGLE_GEOCODE, SOService.class);
        JSONObject params;
        try {
            // chuyền param
            params = new JSONObject();
            params.put("mode", mode);
            params.put("transit_routing_preference", transit_routing_preference);
            params.put("origin", origin.latitude + "," + origin.longitude);
            params.put("destination", destination.latitude + "," + destination.longitude);
            params.put("key", key);
//              "/server/save_token" -> đường dẫn Api
//            mOdoo.callRoute("https://maps.googleapis.com/maps/api/directions/json?mode=" + mode + "&transit_routing_preference=" + transit_routing_preference
//                    +"&origin=" + origin.latitude + "," + origin.longitude+"&destination="+destination.latitude + "," + destination.longitude+"&key="+key, null, new SharingOdooResponse<OdooResultDto<GeoCodedWayPoints>>() {
//
//
//                @Override
//                public void onSuccess(GeoCodedWayPoints geoCodedWayPointsOdooResultDto) {
//                    response.onSuccess(geoCodedWayPointsOdooResultDto);
//                    Log.d("TAG", "onSuccess:" + geoCodedWayPointsOdooResultDto);
//                }
//
//                @Override
//                public void onFail(Throwable error) {
//                    response.onFail(error);
//                }
//            });
            soService.getServices().getPoints(mode, transit_routing_preference, origin.latitude + "," + origin.longitude, destination.latitude + "," + destination.longitude, key).enqueue(new Callback<GeoCodedWayPoints>() {
                @Override
                public void onResponse(Call<GeoCodedWayPoints> call, Response<GeoCodedWayPoints> rs) {
                    response.onSuccess(rs.body());
                }

                @Override
                public void onFailure(Call<GeoCodedWayPoints> call, Throwable t) {
                    response.onFail(t);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: 8/22/2020 get history
    public static void getHistory(String fromDate, String toDate, String txtSearch, Integer offset, IResponse iResponse) {
        JSONObject params = new JSONObject();

        try {
            params.put("from_date", fromDate);
            params.put("to_date", toDate);
            params.put("txt_search", txtSearch);
            params.put("offset", offset);
            params.put("limit", 10);

            mOdoo.callRoute("/bidding/get_list_bidding_order_history", params, new SharingOdooResponse<OdooResultDto<List<BiddingInformation>>>() {
                @Override
                public void onSuccess(OdooResultDto<List<BiddingInformation>> orderHistoryResponse) {
                    if (orderHistoryResponse != null) {
                        iResponse.onSuccess(orderHistoryResponse);
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void getNotification(int offset, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("type", "bidding_vehicle");
            params.put("offset", offset);

            mOdoo.callRoute("/bidding/get_bidding_notification_history", params, new SharingOdooResponse<OdooResultDto<NotificationModel>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationModel> notificationModelOdooResultDto) {
                    response.onSuccess(notificationModelOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void confirmRedNotification(int id, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("id", id);
            mOdoo.callRoute("/notification/is_read", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {

                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
