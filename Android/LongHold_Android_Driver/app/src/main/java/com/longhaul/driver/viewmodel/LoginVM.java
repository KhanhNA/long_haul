package com.longhaul.driver.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.longhaul.driver.api.BaseApi;
import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.StaticData;
import com.longhaul.driver.model.Driver;
import com.longhaul.driver.model.UserInfo;
import com.longhaul.driver.utils.ApiResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;
    private SharedPreferences.Editor editor;


    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }


    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        userInfo.setDistanceCheckPoint(sharedPreferences.getLong(Constants.DISTANCE_CHECK_POINT, 0));
        userInfo.setGoogle_api_key_geocode(sharedPreferences.getString(Constants.GOOGLE_API_KEY_GEOCODE, ""));
        model.set(userInfo);
    }


    public void requestLogin() {
        if (model.get() != null) {
            UserInfo userInfo = model.get();
            BaseApi.requestLogin(userInfo.getUserName(), userInfo.getPassWord(), new IOdooResponse<OdooSessionDto>() {
                @Override
                public void onResponse(OdooSessionDto odooSessionDto, Throwable throwable) {
                    onLoginResponse(odooSessionDto, throwable);
                }

            });
        }

    }


    //forget password
    public void sendPassword() {
        // TODO: 21/02/2020 send OTP
        try {
            view.action("sendOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void confirmOTP() {
        // TODO: 21/02/2020 confirm OTP
        try {
            view.action("confirmOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void onLoginResponse(OdooSessionDto odooSessionDto, Throwable volleyError) {
        if (volleyError == null && odooSessionDto != null) {
            StaticData.setOdooSessionDto(odooSessionDto);
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);

            editor.putString(Constants.USER_NAME, getModelE().getUserName());
            editor.putLong(Constants.DISTANCE_CHECK_POINT, odooSessionDto.getDistance_check_point());
            editor.putString(Constants.GOOGLE_API_KEY_GEOCODE, odooSessionDto.getGoogle_api_key_geocode());

            if (userInfo.getIsSave() != null && userInfo.getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", false);
                editor.putString(Constants.MK, "");
            }
            editor.commit();
            try {
                // TODO: 03/08/2020 lấy thông tin user từ thông tin lấy được khi login
//                getUserInfo(odooSessionDto.getUid());
                responseLiveData.postValue(ApiResponse.success(StaticData.getOdooSessionDto()));

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }


    private void getUserInfo(Integer uId) throws Exception {
        DriverApi.getDriverInfo(uId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                responseLiveData.postValue(ApiResponse.success(StaticData.getOdooSessionDto()));
//                runUi.run("avcsd");
                StaticData.driver = (Driver) o;

            }

            @Override
            public void onFail(Throwable error) {
                responseLiveData.postValue(ApiResponse.error(error));
            }
        });

    }
}
