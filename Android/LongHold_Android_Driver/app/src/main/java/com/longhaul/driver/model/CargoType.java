package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoType extends TsBaseModel {
    private Integer id;
    private Integer length;
    private Integer width;
    private Integer height;
    private String long_unit_moved0;
    private String weight_unit_moved0;
    private String type;
    private Integer from_weight;
    private Integer to_weight;
    private String size_standard_seq;
    private String long_unit;
    private String weight_unit;
    private Integer cargo_quantity;
    private Integer total_weight;
}
