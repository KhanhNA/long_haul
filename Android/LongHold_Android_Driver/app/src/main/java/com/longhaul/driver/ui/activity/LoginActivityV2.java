package com.longhaul.driver.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;


import com.longhaul.driver.R;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.ActivityLoginV2Binding;
import com.longhaul.driver.ui.fragment.DrivingConfirmFragment;
import com.longhaul.driver.utils.ApiResponse;
import com.longhaul.driver.utils.ImageUtils;
import com.longhaul.driver.utils.LanguageUtils;
import com.longhaul.driver.utils.NetworkUtils;
import com.longhaul.driver.utils.ToastUtils;
import com.longhaul.driver.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;


public class LoginActivityV2 extends BaseActivity<ActivityLoginV2Binding> implements ActionsListener {
    private LoginVM loginVM;

    private CircularProgressButton btnLogin;
    private AppCompatSpinner spLanguage;
    private int check = 0;
    //_____________________________TESTLOCATION_______________________________________


    protected LocationManager locationManager;
    protected LocationListener locationListener;

    //___________________________________________________________________________

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginVM = (LoginVM) viewModel;
        btnLogin = binding.btnLogin;
        spLanguage = binding.spin;
        loginVM.loginResponse().observe(this, this::consumeResponse);
        spLanguage.setSelection(AppController.getInstance().getSharePre().getInt("current_language", 0));
        //
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++check > 1) {
                    try {
                        LanguageUtils.changeLanguage(LoginActivityV2.this, AppController.LANGUAGE_CODE[i]);
                        AppController.getInstance().getSharePre().edit().putInt("current_language", i).apply();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TextWatcher loginTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userName = binding.txtUserName.getText().toString().trim();
                String password = binding.txtPassword.getText().toString().trim();
                binding.btnLogin.setEnabled(!userName.isEmpty() && !password.isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        binding.txtUserName.addTextChangedListener(loginTextWatcher);
        binding.txtPassword.addTextChangedListener(loginTextWatcher);

    }

    private void consumeResponse(ApiResponse apiResponse) {
        switch (apiResponse.status) {
            case LOADING:
//                progressDialog.show();
                break;

            case SUCCESS:
                binding.txtLoginFail.setVisibility(View.INVISIBLE);
                startActivity(new Intent(LoginActivityV2.this, MainActivity.class));
                finish();
                break;

            case ERROR:
                binding.txtLoginFail.setVisibility(View.VISIBLE);
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;
            case NOT_CONNECT:
                Toast.makeText(this, R.string.not_connect_server, Toast.LENGTH_SHORT).show();
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;

            default:
                break;
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        if (R.id.btnLogin == v.getId()) {
            if (isValid()) {
                if (!NetworkUtils.isNetworkConnected(this)) {
                    Toast.makeText(LoginActivityV2.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    btnLogin.startAnimation();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loginVM.requestLogin();
                        }
                    }, 1000);
                }
            }
            hideKeyboard();
        }
        if (R.id.btnForgetPass == v.getId()){
            //Chuyển qua màn xác nhận,lấy thông tin số điện thoại về.
            Intent intent = new Intent(LoginActivityV2.this, CommonActivity.class);
            intent.putExtra(Constants.FRAGMENT, DrivingConfirmFragment.class);
            startActivity(intent);
        }
    }

    private boolean isValid() {
        if (loginVM.getModelE().getUserName().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_username));
            return false;
        } else if (loginVM.getModelE().getPassWord().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_password));
            return false;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        btnLogin.dispose();
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(binding.getRoot().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
