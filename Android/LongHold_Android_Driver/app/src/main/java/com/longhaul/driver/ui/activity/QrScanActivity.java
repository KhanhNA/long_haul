package com.longhaul.driver.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.Result;
import com.longhaul.driver.R;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.model.Cargo;
import com.longhaul.driver.model.CargoTypes;
import com.longhaul.driver.viewmodel.DetailNotShipVM;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScaner;
    ArrayList<CargoTypes> listCargoTypeCompare = new ArrayList<>();
    ArrayList<CargoTypes> listCargoTypeValidate = new ArrayList<>();
    ArrayList<String> listQRCompare = new ArrayList<>();
    ArrayList<String> validateQrSuccess = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_qr_scaner);
        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mScaner = new ZXingScannerView(this);
        contentFrame.addView(mScaner);
        findViewById(R.id.ivBack).setOnClickListener(view -> { finish(); });
        getData();
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            listCargoTypeCompare = (ArrayList<CargoTypes>) DetailNotShipVM.getObjectList(bundle.getString(Constants.LIST_CARGO_COMPARE), CargoTypes.class);
            listCargoTypeValidate = (ArrayList<CargoTypes>)DetailNotShipVM.getObjectList(bundle.getString(Constants.LIST_CARGO_VALIDATE), CargoTypes.class);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mScaner.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScaner.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScaner.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {
        String qrCode = result.getText();
        compareQrCode(qrCode);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScaner.resumeCameraPreview(QrScanActivity.this);
            }
        }, 2000);
    }

    private void compareQrCode(String qrCode) {
        //lấy danh sách cargo ở cargotype bên ngoài
        if (listCargoTypeCompare != null && listCargoTypeCompare.size() > 0) {
            for (int i = 0; i < listCargoTypeCompare.size(); i++) {
                List<Cargo> cargoList = listCargoTypeCompare.get(i).getCargos();
                addCargo(cargoList);
            }
        } else {
            Toast.makeText(this, getString(R.string.qr_Invalid), Toast.LENGTH_SHORT).show();
            return;
        }

        if (listQRCompare.contains(qrCode)) {
            validateQrCode(qrCode);
        } else {
            Toast.makeText(this, getString(R.string.qr_code_no_available), Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void addCargo(List<Cargo> cargoList) {
        for (Cargo mCargo : cargoList) {
            if (mCargo != null) {
                listQRCompare.add(mCargo.getQrCode());
            }
        }
    }

    private void validateQrCode(String qrCode) {
        // lấy danh sách  cargo trong biddingvehicle
        if (listCargoTypeValidate != null && listCargoTypeValidate.size() > 0) {
            for (int i = 0; i < listCargoTypeValidate.size(); i++) {
                List<Cargo> cargo = listCargoTypeValidate.get(i).getCargos();
                addCargoValidate(cargo);
            }
        }

        if (validateQrSuccess.contains(qrCode)) {
            Toast.makeText(this, getString(R.string.qr_code_is_available), Toast.LENGTH_SHORT).show();
            return;
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent data = new Intent();
                    data.putExtra("data", qrCode);
                    setResult(RESULT_OK, data);
                    finish();
                }
            }, 1000);
        }
    }

    private void addCargoValidate(List<Cargo> cargoList) {
        for (Cargo cargo : cargoList) {
            if (cargo != null) {
                validateQrSuccess.add(cargo.getQrCode());
            }
        }
    }
}

