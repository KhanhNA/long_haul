package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle extends TsBaseModel {
    private Integer id;
    private String name;

    private String image_logo;
    private String vehicle_image;
    private List<String> list_image;


    private String license_plate;
    private String color;
    private Integer model_year;
    private String fuel_type;
    private Integer body_length;
    private Integer body_width;
    private Integer height;
    private Double capacity;
    private Double available_capacity;

    private String vehicle_type;
    private Float engine_size;//

    private Integer vehicle_status;


    public String getVehicle_image(){
        if(list_image == null && list_image.size() == 0){
            return "";
        }
        return list_image.get(0);
    }

    public String getCapacityStr(){
        if(capacity == null) return "";
        return String.format("%.1f", capacity);
    }
    public String getAvailableCapacityStr(){
        if(capacity == null) return "";
        return String.format("%.1f", capacity);
    }

}
