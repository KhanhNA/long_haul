package com.longhaul.driver.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.longhaul.driver.R;
import com.longhaul.driver.base.AppController;
import com.longhaul.driver.base.BaseAdapterV3;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.HistoryFragmentBinding;
import com.longhaul.driver.model.BiddingShipping;
import com.longhaul.driver.ui.viewcommon.DialogConfirm;
import com.longhaul.driver.utils.DatePickerDialogFragment;
import com.longhaul.driver.utils.EnumBidding;
import com.longhaul.driver.utils.ToastUtils;
import com.longhaul.driver.viewmodel.CustomLoadMoreView;
import com.longhaul.driver.viewmodel.HistoryVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.longhaul.driver.base.Constants.DATA_PASS_FRAGMENT;
import static com.longhaul.driver.base.Constants.KEY_PAGE;
import static com.longhaul.driver.base.Constants.OBJECT_SERIALIZABLE;

public class HistoryFragment extends BaseFragment implements ActionsListener {
    HistoryFragmentBinding mBindding;
    HistoryVM historyVM;
    private BaseAdapterV3 adapter;
    private DatePickerDialogFragment datePickerDialogFragment;
    int mOffset = 0;
    private String mFromDate = " ";
    private String mToDate = " ";
    String tvSearch = "";
    private int mMinYear = 0;
    private int mMinMoth = 0;
    private int mMinDay = 0;
    private int mMaxDay;
    private DialogConfirm dialog;

    @Override
    public int getLayoutRes() {
        return R.layout.history_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HistoryVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBindding = (HistoryFragmentBinding) binding;
        historyVM = (HistoryVM) viewModel;
        refreshData();
        iniDateDefault();
        initView();
        initLoadMore();
        eventChangeEdtSearch();
        mBindding.swipeLayout.setOnRefreshListener(() -> refreshData());

        return v;
    }

    private void eventChangeEdtSearch() {
        mBindding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(" ")) {
                    refreshData();
                }
                tvSearch = s.toString().trim();
                refreshData();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            adapter.setList(historyVM.getmHistoryList().getValue());
            adapter.notifyDataSetChanged();
            mBindding.recyclerView.invalidate();
            offRefreshing();
            emptyData(historyVM.getHistory().getValue().getRecords());
        }

    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mBindding.emptyData.setVisibility(View.GONE);
            mBindding.swipeLayout.setVisibility(View.VISIBLE);
        } else {
            mBindding.emptyData.setVisibility(View.VISIBLE);
            mBindding.swipeLayout.setVisibility(View.GONE);
        }
    }

    private void initView() {
        mBindding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new BaseAdapterV3(R.layout.item_history, historyVM.getmHistoryList().getValue(), this);
        mBindding.recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.viewGroup:
                BiddingShipping biddingShipping = (BiddingShipping) o;
                Bundle args = new Bundle();
                Gson gson = new Gson();
                String json = gson.toJson(biddingShipping.getBiddingInformation());
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailNotShipFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                intent.putExtra(OBJECT_SERIALIZABLE, json);
                intent.putExtra(KEY_PAGE, EnumBidding.EnumBiddingBehavior.SHIPPING_DONE);
                startActivity(intent);

                break;
            case R.id.btnCancel:
                String note = (((BiddingShipping) o).getBiddingInformation().getNote() != null ? ((BiddingShipping) o).getBiddingInformation().getNote() : "");
                dialog = new DialogConfirm(getString(R.string.reason_close), getString(R.string.btnConfirm), note, false, v1 -> dialog.dismiss());
                dialog.show(getFragmentManager(), "tag");
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void loadMore() {
        int length = historyVM.getHistory().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            requestOrderHistory(mFromDate, mToDate, tvSearch, mOffset);
        } else {
            adapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void requestOrderHistory(String fromDate, String mToDate, String tvSearch, int mOffset) {
        historyVM.listhistory(fromDate, mToDate, tvSearch, mOffset, this::runUi);
    }

    private void refreshData() {
        // refresh data
        historyVM.clearData();
        mOffset = 0;
        requestOrderHistory(mFromDate, mToDate, tvSearch, mOffset);
    }

    private void iniDateDefault() {
        Date date = new Date();
        mFromDate = AppController.getDate(date, 0);
        mToDate = AppController.getDate(date, 0);
        // giá trị minDate
        mMinDay = date.getDay();
        mMinMoth = date.getMonth();
        mMinYear = date.getYear();

        // tạo giá trị default cho MaxDate
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 0);
        date = c.getTime();
//        mMaxDay = date.getDay();
        mBindding.tvToDate.setText(mFromDate);
        mBindding.tvFromDate.setText(mToDate);
        requestOrderHistory(mFromDate, mToDate, tvSearch, mOffset);
    }

    // thay đổi date của minDate
    private void onDateSelected(int year, int month, int dayOfMonth) {
        mMinYear = year;
        mMinDay = dayOfMonth;
        mMinMoth = month;
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
//        mMaxDay = setMaxDate(dayOfMonth);
        mFromDate = AppController.getDate(date, 0);
        mBindding.tvFromDate.setText(mFromDate);
    }

    // set giá trị max đết khi chọn todate
    public int setMaxDate(int day) {
        Date date = new Date();
        date.setDate(day);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        date = c.getTime();
        return date.getDate();
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        switch (v.getId()) {
            case R.id.btnFromDate:
                datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.setOnDateSelectedListener(this::onDateSelected);
                datePickerDialogFragment.show(getChildFragmentManager(), "");
                break;
            case R.id.btnToDate:
                if (mMinYear == 0) return;
                datePickerDialogFragment = new DatePickerDialogFragment(mMinYear, mMinMoth, mMinDay, mMinYear, mMinMoth, mMaxDay, this::runUi);
                datePickerDialogFragment.setOnDateSelectedListener((int year, int month, int dayOfMonth) -> {
                    Date date = new Date();
                    date.setDate(dayOfMonth);
                    date.setMonth(month);
                    mToDate = AppController.getDate(date, 0);
                    mBindding.tvToDate.setText(mToDate);
                    refreshData();
                });
                datePickerDialogFragment.show(getChildFragmentManager(), "");
                break;
        }
    }

    private void offRefreshing() {
        mBindding.swipeLayout.setRefreshing(false);
        adapter.getLoadMoreModule().setEnableLoadMore(true);
    }
}

