package com.longhaul.driver.api;

import com.longhaul.driver.base.IResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;

public abstract class SharingOdooResponse2<T> implements IResponse<T> {

    public IOdooResponse getResponse(Class<T> clazz){
        return (IOdooResponse<T>) (o, volleyError) -> {
            if(volleyError != null){
                volleyError.printStackTrace();
                return;
            }
            onSuccess(o);
        };
    }

}
