package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

public class SizeStandard extends TsBaseModel {

    private Integer id;
    private Integer length;
    private Integer width;
    private Integer height;
    private String longUnit;

    public Integer getId() {
        return id;
    }

    public Integer getLength() {
        return length;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public String getLongUnit() {
        return longUnit;
    }
}