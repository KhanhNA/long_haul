package com.longhaul.driver.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleCargo {
    int id;
    String lisence_plate;
    String driver_name;
    String driver_phone;
    int tonange;
    String image;
    String latitude;
    String longitude;
    String id_card;
    int is_bidding;
    ArrayList<BiddingInformation> bidding;


}
