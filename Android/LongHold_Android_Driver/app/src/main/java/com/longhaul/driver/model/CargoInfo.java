package com.longhaul.driver.model;

class CargoInfo {

    public Integer id;

    public String cargo_number;

    public Integer price;

    public Integer quantity;

    public String confirm_time;

    public String create_date;

    public String product_type_name;
}
