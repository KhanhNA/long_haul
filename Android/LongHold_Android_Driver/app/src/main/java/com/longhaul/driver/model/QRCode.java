package com.longhaul.driver.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.tsolution.base.TsBaseModel;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QRCode extends TsBaseModel {
    private String code;

    public QRCode(String qrCode) {
        this.code = qrCode;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QRCode qrCode = (QRCode) o;
        return Objects.equals(code, qrCode.code);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
