package com.longhaul.driver.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;


import com.longhaul.driver.api.DriverApi;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.IResponse;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.model.NotificationModel;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;

public class NotificationVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<NotificationModel>> mNotificationLiveData = new MutableLiveData<>();
    private OdooResultDto<NotificationModel> mNotification;

    public NotificationVM(@NonNull Application application) {
        super(application);
        mNotification = new OdooResultDto<>();
        mNotification.setRecords(new ArrayList<>());
        mNotificationLiveData.setValue(mNotification);
    }


    public MutableLiveData<OdooResultDto<NotificationModel>> getNotification() {
        return mNotificationLiveData;
    }

    public void requestGetNotification(int offset, RunUi runUi) {
        DriverApi.getNotification(offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {

                OdooResultDto<NotificationModel> response = (OdooResultDto<NotificationModel>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                mNotification.getRecords().addAll(response.getRecords());
                mNotification.setTotal_record(response.getTotal_record());
                mNotificationLiveData.setValue(mNotification);
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void clearData() {
        mNotification.getRecords().clear();
        mNotificationLiveData.setValue(null);
    }

    public void confirmRedNotification(int id) {
        DriverApi.confirmRedNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                Log.e("duongnk", "onSuccess: confirmRedNotification");
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
