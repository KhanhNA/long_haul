package com.longhaul.driver.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.tsolution.base.TsBaseModel;

import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CargoTypes extends TsBaseModel {
    private Integer id;
    private Integer length;
    private Integer width;
    private String phone;
    private Integer height;
    private String long_unit_moved0;
    private Integer weight_unit_moved0;
    private String type;
    private Integer from_weight;
    private Integer to_weight;
    private String size_standard_seq;
    private Integer cargo_quantity;
    private Integer total_weight;
    private List<Cargo> cargos;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CargoTypes)) return false;
        CargoTypes that = (CargoTypes) o;
        return getId().equals(that.getId());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
