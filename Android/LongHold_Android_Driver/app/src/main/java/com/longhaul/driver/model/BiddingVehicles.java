package com.longhaul.driver.model;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingVehicles extends TsBaseModel {
    private Integer id;
    private String code;
    private String lisence_plate;
    private String driver_phone_number;
    private String driver_name;
    private String expiry_time;
    private Integer company_id;
    private String status;
    private String description;
    private Integer vehicle_type;
    private Integer weight_unit;
}
