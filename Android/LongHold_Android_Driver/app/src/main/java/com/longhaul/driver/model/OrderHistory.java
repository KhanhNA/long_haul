package com.longhaul.driver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.TsBaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderHistory extends TsBaseModel {
    private Integer id;
    private String bidding_order_number;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private Integer total_weight;
    private Integer total_cargo;
    private Integer price;
    private String currency_name;
    private Integer distance;
    private String type;
    private String status;
    private String note;
    private String create_date;
    private String write_date;
    private Integer bidding_package_id;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private String max_confirm_time;
    private BiddingVehicles bidding_vehicles;
    private List<CargoTypes> cargo_types;
    private BiddingOrderReceive bidding_order_receive;
    private BiddingOrderReturn bidding_order_return;
}
