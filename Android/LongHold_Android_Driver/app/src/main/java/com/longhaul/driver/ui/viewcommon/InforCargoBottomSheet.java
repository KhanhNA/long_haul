package com.longhaul.driver.ui.viewcommon;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;


import com.longhaul.driver.BR;
import com.longhaul.driver.R;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.base.RunUi;
import com.longhaul.driver.databinding.InforCargoBottomSheetBinding;


public class InforCargoBottomSheet extends Dialog {
    InforCargoBottomSheetBinding binding;
    private Context mContext;
    private String mBksDriver;
    private RunUi mRunUi;

    public InforCargoBottomSheet(@NonNull Context context, String bksDriver, RunUi runUi) {
        super(context);
        mContext = context;
        mBksDriver = bksDriver;
        mRunUi = runUi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.infor_cargo_bottom_sheet, null, false);
        setContentView(binding.getRoot());
        binding.tvBksDriver.setText("BKS: " + mBksDriver);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);
        this.getWindow().setGravity(Gravity.BOTTOM);
        int windowsScale = this.getContext().getResources().getDisplayMetrics().widthPixels;
        windowsScale = (int) ((float) windowsScale * 1f);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initListener();
    }

    private void initListener() {
        binding.setVariable(BR.listener, this);
        binding.btnDeleteCargo.setOnClickListener(v -> {
            mRunUi.run(Constants.DELETE_CARGO);
            dismiss();
        });
        binding.btnInforBidding.setOnClickListener(v -> {
            mRunUi.run(Constants.VIEW_INFOR_CARGO);
            dismiss();
        });
        binding.btnEditCargo.setOnClickListener(v -> {
            mRunUi.run(Constants.EDIT_CARGO);
            dismiss();
        });
    }
}
