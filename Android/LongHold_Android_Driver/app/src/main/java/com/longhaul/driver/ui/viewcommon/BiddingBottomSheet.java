package com.longhaul.driver.ui.viewcommon;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.longhaul.driver.R;
import com.longhaul.driver.base.Constants;
import com.longhaul.driver.databinding.BiddingBottomsheetBinding;
import com.longhaul.driver.utils.EnumBidding;
import com.tsolution.base.BR;
import com.tsolution.base.listener.ActionsListener;

public class BiddingBottomSheet extends Dialog implements ActionsListener {
    private Context mContext;
    BiddingBottomsheetBinding binding;
    private OnclickSortListBidding mOnclickSortListBidding;

    public BiddingBottomSheet(@NonNull Context context, OnclickSortListBidding onclickSortListBidding) {
        super(context);
        mContext = context;
        mOnclickSortListBidding = onclickSortListBidding;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.bidding_bottomsheet, null, false);
        setContentView(binding.getRoot());
        RadioButton radioButton = binding.getRoot().findViewById(Constants.getStatusSort());
        radioButton.setChecked(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);
        this.getWindow().setGravity(Gravity.BOTTOM);
        //set tỉ lệ dialog so với chiều ngang của màn hình - 90% so với chiều ngang của màn
        int windowsScale = this.getContext().getResources().getDisplayMetrics().widthPixels;
        windowsScale = (int) ((float) windowsScale * 1f);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initListener();
        iniView();
    }

    private void iniView() {
        String titleSort = mContext.getString(R.string.sort);
        binding.btnAllCargo.setText(titleSort+" "+ mContext.getString(R.string.priceAscending));
        binding.radioSortDescendingPrices.setText(titleSort+" "+(mContext.getString(R.string.descendingPrices)));
        binding.radioArrangeAscendingDistance.setText(titleSort+" "+(mContext.getString(R.string.ascendingDistance)));
        binding.radioDescendingDistance.setText(titleSort+" "+(mContext.getString(R.string.descendingDistance)));
        binding.radioEarliest.setText(titleSort+" "+(mContext.getString(R.string.earliest)));
        binding.radioOld.setText(titleSort+" "+(mContext.getString(R.string.sort_old)));

    }

    private void initListener() {
        binding.setVariable(BR.listener, this);
        RadioGroup radioGroup = binding.getRoot().findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.btnAllCargo:
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING);
                    Constants.setStatusSort(R.id.btnAllCargo);
                    dismiss();
                    break;
                case R.id.radioSortDescendingPrices:
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES);
                    Constants.setStatusSort(R.id.radioSortDescendingPrices);
                    dismiss();
                    break;
                case R.id.radioArrangeAscendingDistance:
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE);
                    Constants.setStatusSort(R.id.radioArrangeAscendingDistance);
                    dismiss();
                    break;
                case R.id.radioDescendingDistance:
                    dismiss();
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE);
                    Constants.setStatusSort(R.id.radioDescendingDistance);
                    break;
                case R.id.radioEarliest:
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.EAR_LIEST);
                    Constants.setStatusSort(R.id.radioEarliest);
                    dismiss();
                    break;
                case R.id.radioOld:
                    mOnclickSortListBidding.onSortListBidding(EnumBidding.EnumSortBiddingBehavior.OLD);
                    Constants.setStatusSort(R.id.radioOld);
                    dismiss();
                    break;
            }
        });
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        if (R.id.btnClose == v.getId()) {
            dismiss();
        }
    }

    public interface OnclickSortListBidding {
        void onSortListBidding(int ketSort);
    }
}
