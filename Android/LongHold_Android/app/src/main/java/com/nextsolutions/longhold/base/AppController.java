package com.nextsolutions.longhold.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Pair;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.model.InformationUser;
import com.nextsolutions.longhold.utils.CustomDialogProgress;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class AppController extends Application {
    //    public static final String BASE_URL = "http://192.168.1.69:8070";
    //    public static final String BASE_URL = "http://demo.aggregatoricapaci.com.vn:8070";
    public static final String BASE_URL = "http://192.168.1.95:8070";
    public static final String BASE_URL_IMAGE = BASE_URL + "/images/";
    public static final String HTTP = "http://";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String DATE_PATTERN = "dd/MM/yyyy";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String DATE_TIME_PATTERN = "HH:mm dd/MM/yyyy";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CACHE_USER = "CACHE_USER";
    public static final String DATE_PATTERN_HISTORY = "yyyy-MM-dd";
    public static final String PASSWORD_INPUT_PATTERN = "[a-zA-Z0-9!@#$%*]+";
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(AppController.DATE_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatTime = new SimpleDateFormat(AppController.TIME_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDateTime = new SimpleDateFormat(AppController.DATE_TIME_PATTERN);
    public static String URI_SOCKET = "http://demo.nextsolutions.com.vn:3001/";
    public static String channel_SOCKET = "bidmessage";
    public static Long languageId = 1L;
    public static String[] LANGUAGE_CODE = {"vi", "en", "my"};
    public static CustomDialogProgress mProgressDialog;
    public static InformationUser mInformationUser;
    private static AppController mInstance;
    private static String mTimeMaxDate = "";
    HashMap<String, Object> clientCache;
    private SharedPreferences sharedPreferences;
    private DecimalFormat formatterCurrency;
    private DecimalFormat formatterNumber;

    public static InformationUser getInformationUser() {
        return mInformationUser;
    }

    public static void setInformationUser(InformationUser mInformationUser) {
        AppController.mInformationUser = mInformationUser;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static void showLoading(Activity activity) {
        try {
            mProgressDialog = new CustomDialogProgress(activity);
            mProgressDialog.show();
        } catch (Exception e) {
            Log.e("mProgressDialog", "showLoading: " + e.toString());
        }

    }

    public static void hideLoading() {
        if (mProgressDialog == null) return;
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();

    }

    @SuppressLint("DefaultLocale")
    public static String formatNumberV2(double number) {
        if (number >= 1000000000) {
            return String.format("%.1fT", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.1fB", number / 1000000.0);
        }

        if (number >= 100000) {
            return String.format("%.1fM", number / 100000.0);
        }

        if (number >= 1000) {
            return String.format("%.1fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String convertDate(String input) {
        input = addHour(input, 7);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String outDate = "";
        String dateReplace = "";

        try {
            Date date = format.parse(input);
            System.out.println(date);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
//            Date GetDate = new Date();
            outDate = timeStampFormat.format(date);
            dateReplace = outDate.replace(':', 'h');
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateReplace;
    }

    public static String convertDate(String input, String DATE_PATTERN) {
        input = addHour(input, 7);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String outDate = "";
        String dateReplace = "";

        try {
            Date date = format.parse(input);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
//            Date GetDate = new Date();
            outDate = timeStampFormat.format(date);
//            dateReplace = outDate.replace(':', 'h');
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String addHour(String myTime, int number) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = format.parse(myTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, number);
            String newTime = format.format(cal.getTime());
            return newTime;
        } catch (ParseException e) {
            System.out.println(" Parsing Exception");
        }
        return null;

    }

    public static Long cvDateToTimeMillis(Date dateInput) {

        long millis = dateInput.getTime();
        return millis;
    }

    @SuppressLint("NewApi")
    public static Date formatDate(int year, int month, int dayOfMonth, String pattent) {
        final Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
        date.setYear(year);
        android.icu.text.SimpleDateFormat sdf = new android.icu.text.SimpleDateFormat(pattent);
        String dt = sdf.format(c.getTime());
        return date;
    }

    public static String convertUTCTime(String dateStr) {
        if (dateStr == null) return "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date value = null;
        try {
            value = formatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_TIME_PATTERN);
        dateFormatter.setTimeZone(TimeZone.getDefault());
        String dt = dateFormatter.format(value);

        return dt;
    }

    // convert date vê dạng timezone
    public static String formatDateToString(Date date, String format,
                                            String timeZone) {
        // null check
        if (date == null) return null;
        // create SimpleDateFormat object with input format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // default system timezone if passed null or empty
        if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        // set timezone to SimpleDateFormat
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        // return Date in required format with timezone as String
        return sdf.format(date);
    }

    // get ngày với trạng thái add thêm date
    public static String getDate(Date date, int current) {
        String currentDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, current);
        date = c.getTime();
        currentDate = new SimpleDateFormat(DATE_PATTERN_HISTORY, Locale.getDefault()).format(date);
        return currentDate;
    }

    public static Date convertStringTimeToDate(String strDate) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * doi tu yyyy-MM-dd HH:mm:ss -> HH:mm
     *
     * @param startTime
     * @return
     */
    public static String convertDateToTime(Date startTime) {
        // Get date from string
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Get time from date
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        String displayValue = timeFormatter.format(startTime);
        return displayValue;
    }

    /**
     * get ngay trong String Date
     *
     * @param startDate
     * @return
     */
    public static String getDayInDate(Date startDate) {
        String day = (String) DateFormat.format("dd", startDate); // 20
        return day;
    }

    /**
     * get tháng trong String date
     *
     * @param startDate
     * @return
     */
    public static String getMothInDate(Date startDate) {

        String moth = new SimpleDateFormat("MM").format(startDate); // 20
        return moth;
    }

    /**
     * lấy ra ngày hiện tại
     *
     * @return
     */
    public static String getDayNow() {
        Date date = new Date();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String formattedDate = df.format(c);
        String day = getDayInDate(c);
        return day;
    }

    /**
     * chuyển ngày yyyy-MM-dd HH:mm:ss -=> Milliseconds
     *
     * @param startDate
     * @return
     */
    public static long getDateToMilliseconds(String startDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();
        return millis;
    }

    public static void downTimer(TextView view, long millisUntilFinished) {
        CountDownTimer countDownTimer = null;
        countDownTimer = new CountDownTimer(millisUntilFinished, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                view.setText("");
                view.setText(hmsTimeFormatter(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                view.setText(view.getContext().getString(R.string.expired));
            }
        };
        countDownTimer.start();
    }

    @SuppressLint("DefaultLocale")
    public static String hmsTimeFormatter(long milliSeconds) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
    }

    @SuppressLint("DefaultLocale")
    public static String msTimeFormatter(long milliSeconds) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
    }

    public static String dateToString(Date date) {


        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String dayString = df.format(date);
        return dayString;
    }

    public static void setDigits(EditText view) {
        view.setInputType(InputType.TYPE_CLASS_TEXT);
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence src, int start, int end, Spanned dest, int dstart, int dend) {

                if (src.equals("")) {
                    return src;
                }
                if (src.toString().matches(PASSWORD_INPUT_PATTERN)) {
                    return src;
                }
                return "";
            }
        };

        view.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(12)});
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
        initRealm();
    }

    private void initRealm() {
        Realm.init(getApplicationContext());
        RealmConfiguration config =
                new RealmConfiguration.Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build();
        Realm.setDefaultConfiguration(config);
    }

    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    public String formatCurrency(Double money) {
        if (money == null) {
            return "0 " + StaticData.getOdooSessionDto().getCurrency();
        }
        if (formatterCurrency == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterCurrency = new DecimalFormat("###,###,###", symbols);
        }

        return formatterCurrency.format(money) + " " + StaticData.getOdooSessionDto().getCurrency();
    }

    public String formatCurrency(BigDecimal money) {
        if (money == null) {
            return "0 " + StaticData.getOdooSessionDto().getCurrency();
        }
        if (formatterNumber == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterNumber = new DecimalFormat("###,###,###", symbols);
        }

        return formatterNumber.format(money) + " " + StaticData.getOdooSessionDto().getCurrency();
    }

    public String formatNumber(Integer money) {
        if (money == null) {
            return "0";
        }
        if (formatterCurrency == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterCurrency = new DecimalFormat("###,###,###", symbols);
        }
        return formatterCurrency.format(money);
    }

    public String formatNumber(Double money) {
        if (money == null) {
            return "0";
        }
        if (formatterNumber == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterNumber = new DecimalFormat("###,###,###.##", symbols);
        }

        return formatterNumber.format(money);
    }

    public String formatDistance(Double distance) {
        if (distance != null) {
            distance *= 0.0001;
            return formatNumber(distance);
        }
        return "0";
    }

    public void putCache(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
    }

    public void newActivity(Activity previousActivity, Class fragmentClazz, String extra, Serializable objExtra) {
        Intent intent = new Intent(previousActivity, CommonActivity.class);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (extra != null) {
            intent.putExtra(extra, objExtra);
        }
        previousActivity.startActivity(intent);
    }

    public void newActivity(BaseFragment fragment, Class fragmentClazz, boolean result, int code, Pair<String, Serializable>... objs) {
        Intent intent = new Intent(fragment.getBaseActivity(), CommonActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (objs != null) {
            for (Pair<String, Serializable> obj : objs) {
                intent.putExtra(obj.first, obj.second);
            }
        }
        if (result) {
            fragment.startActivityForResult(intent, code);
        } else {
            fragment.startActivity(intent);
        }
    }
}
