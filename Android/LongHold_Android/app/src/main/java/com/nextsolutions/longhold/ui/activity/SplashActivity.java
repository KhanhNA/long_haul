package com.nextsolutions.longhold.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.android.volley.VolleyError;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.BaseApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.model.UserInfo;
import com.nextsolutions.longhold.utils.ApiResponse;
import com.nextsolutions.longhold.utils.LanguageUtils;
import com.nextsolutions.longhold.utils.NetworkUtils;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.viewmodel.LoginVM;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class SplashActivity extends BaseActivity {
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OdooClient client = new OdooClient(this, AppController.BASE_URL);
        onConnect(client, null);

    }

    private void startLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivityV2.class));
        // close splash activity
        finish();
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case SUCCESS:
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra(Constants.INTENT_NOTIFICATION, getIntent().getIntExtra(Constants.INTENT_NOTIFICATION, 0));
                intent.putExtra(Constants.EXTRA_DATA,getIntent().getSerializableExtra(Constants.EXTRA_DATA));
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                startLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public void onConnect(OdooClient odooV2, VolleyError volleyError) {
        LanguageUtils.loadLocale(SplashActivity.this);
        BaseApi.setOdoo(odooV2);
        LoginVM loginVM = new LoginVM(getApplication());
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        SharedPreferences preferences = AppController.getInstance().getSharePre();
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass) && NetworkUtils.isNetworkConnected(SplashActivity.this)) {
                loginVM.getModel().set(UserInfo.builder().userName(userName).passWord(pass).build());
                loginVM.requestLogin();
            } else {
                startLogin();
            }
        } else {
            startLogin();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);
    }
}
