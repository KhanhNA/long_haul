package com.nextsolutions.longhold.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SizeStandard extends BaseModel {

    private Integer length;
    private Integer width;
    private Integer height;
    private Integer type;
    private Integer price;
    private String from_weight;
    private String to_weight;

}