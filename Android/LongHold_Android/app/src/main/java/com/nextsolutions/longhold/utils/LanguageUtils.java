package com.nextsolutions.longhold.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;


import com.nextsolutions.longhold.base.AppController;

import java.util.Locale;

public class LanguageUtils {

    /**
     * load current locale and change language
     */
    public static void loadLocale(Context context) {
        String code = AppController.LANGUAGE_CODE[AppController.getInstance().getSharePre().getInt("current_language", 0)];
        changeLanguage(context, code);
    }

    /**
     * change app language
     */
    public static void changeLanguage(Context context, String language) {
        if (language.contains("vi")) {
            AppController.languageId = 1L;
        } else if (language.contains("en")) {
            AppController.languageId = 2L;
        } else if (language.contains("my")) {
            AppController.languageId = 3L;
        }

        Locale locale = new Locale(language);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}
