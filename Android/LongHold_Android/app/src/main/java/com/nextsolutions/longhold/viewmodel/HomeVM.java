package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.UserInfo;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeVM extends BaseViewModel {

    public ObservableField<UserInfo> user = new ObservableField<>();

    public HomeVM(@NonNull Application application) {
        super(application);
    }


    public void getBidding(RunUi runUi){
        runUi.run("getSuccess", 3,5);

    }

}
