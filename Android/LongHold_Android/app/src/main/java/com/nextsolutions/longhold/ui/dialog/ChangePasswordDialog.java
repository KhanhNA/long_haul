package com.nextsolutions.longhold.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.databinding.ChangePasswordDialogBinding;
import com.nextsolutions.longhold.utils.ToastUtils;

public class ChangePasswordDialog extends Dialog {
    private Context mContext;


    public ChangePasswordDialog(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChangePasswordDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.change_password_dialog, null, false);
        this.setContentView(binding.getRoot());
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);

        //set tỉ lệ dialog so với chiều ngang của màn hình - 90% so với chiều ngang của màn
        Rect displayRectangle = new Rect();
        Window window = this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        binding.getRoot().setMinimumWidth((int)(displayRectangle.width() * 0.9f));
//        binding.getRoot().setMinimumHeight((int)(displayRectangle.height() * 0.9f));
        this.getWindow().setContentView(binding.getRoot());

        //--- listener View
        binding.btnCancel.setOnClickListener(v -> {
            dismiss();
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPassword = binding.edtOldPass.getText().toString().trim();
                String newPass = binding.edtNewPass.getText().toString().trim();
                String confirm = binding.edtNewPass2.getText().toString().trim();
                if (isValidate(oldPassword, newPass, confirm, binding.tvError)) {
                    DriverApi.changPassWord(oldPassword, newPass, new IResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            ToastUtils.showToast("Thay đổi mật khẩu thành công");
                            dismiss();
                        }

                        @Override
                        public void onFail(Throwable error) {
                            ToastUtils.showToast("Có lỗi trong quá trình xử lý");
                        }
                    });
                }
            }
        });
        //--- configView
        AppController.setDigits(binding.edtNewPass);
        AppController.setDigits(binding.edtNewPass2);
        AppController.setDigits(binding.edtOldPass);


    }

    private boolean isValidate(String oldPassWord, String newPassWord, String newPassWord2, TextView view) {
        if (oldPassWord.isEmpty() || newPassWord.isEmpty() || newPassWord2.isEmpty()) {
            view.setText("Vui lòng không để trống các trường thông tin!");
            return false;
        }
        if (newPassWord.length() < 6) {
            view.setText("Mật khẩu phải lớn hơn 6 kí tự");
            return false;
        }
        if (oldPassWord.equals(newPassWord)) {
            view.setText("Mật khẩu mới không được trùng với mật khẩu cũ");
            return false;
        }
        if (!newPassWord.equals(newPassWord2)) {
            view.setText("Mật khẩu mới chưa trùng khớp với nhau");
            return false;
        }
        view.setText("");
        return true;
    }
}
