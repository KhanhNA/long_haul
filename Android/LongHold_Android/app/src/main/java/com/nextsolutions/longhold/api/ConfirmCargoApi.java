package com.nextsolutions.longhold.api;

import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.enums.VehicleType;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.Vehicle;
import com.nextsolutions.longhold.model.VehicleDriverId;
import com.ns.odoolib_retrofit.model.OdooResultDto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class ConfirmCargoApi extends BaseApi {

    public static void getListDrivers(int offset, String approved_check, String text_search, IResponse response) {
        JSONObject params = new JSONObject();

        try {
            if (text_search == null) {
                text_search = "";
            }
            if (approved_check == null) {
                approved_check = "";
            }
            params.put("offset", offset);
            params.put("limit", 10);
            params.put("text_search", text_search);
            params.put("approved_check", approved_check);
            mOdoo.callRoute("/bidding/get_fleet_driver", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
                @Override
                public void onSuccess(OdooResultDto<Driver> resultDto) {
                    if (resultDto == null)
                        response.onFail(new Throwable());
                    else
                        response.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getListVehicleDrivers(int offset, String approved_check, String text_search, IResponse response) {
        JSONObject params = new JSONObject();

        try {
            if (text_search == null) {
                text_search = "";
            }
            if (approved_check == null) {
                approved_check = "";
            }
            params.put("offset", offset);
            params.put("limit", 10);
            params.put("text_search", text_search);
            params.put("approved_check", approved_check);
            mOdoo.callRoute("/bidding/get_fleet_driver", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
                @Override
                public void onSuccess(OdooResultDto<Driver> resultDto) {
                    if (resultDto == null || resultDto.getRecords() == null)
                        response.onFail(new Throwable());
                    else
                        response.onSuccess(resultDto.getRecords());
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getListVehicles(String type, int offset, String approved_check, String text_search, IResponse response) {
        JSONObject params;
        params = new JSONObject();


        try {
            params.put("type", type);
            params.put("offset", offset);
            params.put("limit", 10);
            if (text_search == null) {
                text_search = "";
            }
            if (approved_check == null) {
                approved_check = "";
            }
            params.put("approved_check", approved_check);
            params.put("text_search", text_search);
            mOdoo.callRoute("/bidding/get_fleet_vehicle", params, new SharingOdooResponse<OdooResultDto<Vehicle>>() {


                @Override
                public void onSuccess(OdooResultDto<Vehicle> resultDto) {
                    if (type.equals(VehicleType.ALL)) {
                        if (resultDto != null) {
                            response.onSuccess(resultDto);
                        } else
                            response.onSuccess(null);
                    } else {
                        if (resultDto == null)
                            response.onFail(new Throwable());
                        else
                            response.onSuccess(resultDto.getRecords());
                    }


                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmVehicleDriverInfo(String bidding_order_id, List<VehicleDriverId> vehicleDrivers, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", bidding_order_id);
            params.put("bidding_vehicle_ids", new JSONArray(mGson.toJson(vehicleDrivers)));

            mOdoo.callRoute("/bidding/confirm_bidding_vehicle_for_bidding_order_origin", params, new SharingOdooResponse<Boolean>() {

                @Override
                public void onSuccess(Boolean resultDto) {
                    response.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//<<<<<<< HEAD
//    public static void confirmVehicleDriverInfo(String bidding_order_id, List<VehicleDriverId> vehicleDrivers, IResponse response) {
//=======
//    public static void getListVehicles(String type,int offset,String approved_check,String text_search,IResponse response) {
//>>>>>>> origin/Aggregatori_Capaci
//        JSONObject params;
//        params = new JSONObject();
//
//
//
//        try {
//<<<<<<< HEAD
//            params.put("bidding_order_id", bidding_order_id);
//            params.put("bidding_vehicle_ids", new JSONArray(mGson.toJson(vehicleDrivers)));
//=======
//            params.put("type", type);
//            params.put("offset", offset);
//            params.put("limit", 10);
//            if(text_search==null){
//                text_search="";
//            }
//            if(approved_check==null){
//                approved_check="";
//            }
//            params.put("approved_check", approved_check);
//            params.put("text_search", text_search);
//            mOdoo.callRoute("/bidding/get_fleet_vehicle", params, new SharingOdooResponse<OdooResultDto<Vehicle>>() {
//>>>>>>> origin/Aggregatori_Capaci
//
//            mOdoo.callRoute("/bidding/confirm_bidding_vehicle_for_bidding_order_origin", params, new SharingOdooResponse<Boolean>() {
//
//                @Override
//<<<<<<< HEAD
//                public void onSuccess(Boolean resultDto) {
//                    response.onSuccess(resultDto);
//=======
//                public void onSuccess(OdooResultDto<Vehicle> resultDto) {
//                    if(type.equals(VehicleType.ALL)){
//                        if (resultDto != null)
//                            response.onSuccess(resultDto);
//                        response.onSuccess(null);
//                    }else{
//                        if (resultDto == null)
//                            response.onFail(new Throwable());
//                        else
//                            response.onSuccess(resultDto.getRecords());
//                    }
//
//
//>>>>>>> origin/Aggregatori_Capaci
//                }
//
//                @Override
//                public void onFail(Throwable error) {
//                    response.onFail(error);
//                }
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
