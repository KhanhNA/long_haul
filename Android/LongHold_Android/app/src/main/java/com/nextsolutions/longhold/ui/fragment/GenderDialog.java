package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.DialogGenderBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

public class GenderDialog extends DialogFragment {
    private OnConfirm onConfirm;
    private String selectedGender;

    public GenderDialog(){
        super();
    }

    public GenderDialog(OnConfirm onConfirm) {
        this.onConfirm = onConfirm;
    }
    public GenderDialog(String selectedGender, OnConfirm onConfirm) {
        this.onConfirm = onConfirm;
        this.selectedGender = selectedGender;
    }


    @SuppressLint("NonConstantResourceId")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogGenderBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_gender, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        if(this.selectedGender != null){
            switch (selectedGender){
                case "male":
                    binding.rgGender.check(R.id.male);
                    break;
                case "female":
                    binding.rgGender.check(R.id.female);
                    break;
                case "other":
                    binding.rgGender.check(R.id.other);
                    break;
            }
        }
        binding.btnDismiss.setOnClickListener(v->this.dismiss());
        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(v -> {
            int selected = binding.rgGender.getCheckedRadioButtonId();
            switch (selected){
                case R.id.male:
                    onConfirm.getSelected("male", getString(R.string.male));
                    dismiss();
                    break;
                case R.id.female:
                    onConfirm.getSelected("female", getString(R.string.female));
                    dismiss();
                    break;
                case R.id.other:
                    onConfirm.getSelected("other", getString(R.string.other));
                    dismiss();
                    break;
            }
        });
        return binding.getRoot();
    }

    public interface OnConfirm {
        void getSelected(String gender, String genderText);
    }
}
