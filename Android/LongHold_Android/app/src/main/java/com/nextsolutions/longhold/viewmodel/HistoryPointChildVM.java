package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableList;

import com.nextsolutions.longhold.api.CustomerAPI;
import com.nextsolutions.longhold.api.SharingOdooResponse;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.HistoryRatingPointDTO;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryPointChildVM extends BaseViewModel {
    int pageData=0;
    int totalRecord=0;
    ObservableBoolean isRefresh= new ObservableBoolean();
    ObservableList<HistoryRatingPointDTO> historyRatingPointDTOS = new ObservableArrayList<>();
    public HistoryPointChildVM(@NonNull Application application) {
        super(application);
        isRefresh.set(false);
    }

    //Handle data before call api
    public void getData(Boolean loadMore, RunUi runUi){
        if(!loadMore){
            historyRatingPointDTOS.clear();
            pageData=0;
            totalRecord=0;
        }
        CustomerAPI.getHistoryRatingPoint(pageData, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isRefresh.set(false);
                getDataSuccess(o,runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    private void getDataSuccess(Object o,RunUi runUi){
        OdooResultDto<HistoryRatingPointDTO> resultDto = (OdooResultDto<HistoryRatingPointDTO>) o;
        if(resultDto!=null && resultDto.getRecords() !=null &&  resultDto.getRecords().size()>0){
            totalRecord = resultDto.getTotal_record();
            historyRatingPointDTOS.addAll(resultDto.getRecords());
            runUi.run(Constants.API_SUCCESS);
        }else{
            runUi.run(Constants.API_FAIL);
        }
    }
    public void loadMore(RunUi runUi){
        pageData += 1;
        if (pageData*10<totalRecord) {
            getData(true,runUi);
        } else {
            runUi.run("noMore");
        }
    }
}
