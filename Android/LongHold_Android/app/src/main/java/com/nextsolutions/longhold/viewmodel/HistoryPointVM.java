package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryPointVM extends BaseViewModel {


    ObservableField<Integer> indexNavigation= new ObservableField<>();
    public HistoryPointVM(@NonNull Application application) {
        super(application);
        indexNavigation.set(0);
    }
}
