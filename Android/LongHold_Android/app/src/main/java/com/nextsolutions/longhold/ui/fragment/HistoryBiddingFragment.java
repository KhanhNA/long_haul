package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.HistoryFragmentBinding;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.ui.viewcommon.BiddingBottomSheet;
import com.nextsolutions.longhold.ui.viewcommon.Dialog;
import com.nextsolutions.longhold.utils.DatePickerDialogFragment;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.HistoryBiddingVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;

public class HistoryBiddingFragment extends BaseFragment implements BiddingBottomSheet.OnclickSortListBidding {

    private HistoryBiddingVM mHistoryBiddingVM;
    private HistoryFragmentBinding mHistoryFragmentBinding;
    //    private XBaseAdapter adapter;
    private int mOffset = 0;
    private DatePickerDialogFragment datePickerDialogFragment;
    private int mMinYear = 0;
    private int mMinMoth = 0;
    private int mMinDay = 0;
    private BaseAdapterV3 adapter;
    //    private String mFromDate;
//    private String mToDate;
    private String mFromDate = "2020-08-01";
    private String mToDate = "2020-09-10";
    private int mMaxDay;
    private Dialog dialog;

    @Override
    public int getLayoutRes() {
        return R.layout.history_fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mHistoryBiddingVM = (HistoryBiddingVM) viewModel;
        mHistoryFragmentBinding = (HistoryFragmentBinding) binding;
        iniDateDefault();
        iniView();
        initLoadMore();
        initRefreshData();
        getData(mFromDate, mToDate, mHistoryFragmentBinding.edtSearch.getText().toString().trim());
        return view;
    }

    private void iniDateDefault() {
        Date date = new Date();
        mFromDate = AppController.getDate(date, 0);
        mToDate = AppController.getDate(date, 3);
        // giá trị minDate
        mMinDay = date.getDay();
        mMinMoth = date.getMonth();
        mMinYear = date.getYear();

        // tạo giá trị default cho MaxDate calendar
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        date = c.getTime();
        mMaxDay = date.getDay();
    }

    private void iniView() {
        adapter = new BaseAdapterV3<>(R.layout.item_bidding, mHistoryBiddingVM.getHistoryBidding().getValue().getRecords(), this);
        mHistoryFragmentBinding.recyclerBidding.setLayoutManager(new LinearLayoutManager(getContext()));
        mHistoryFragmentBinding.recyclerBidding.setAdapter(adapter);
        mHistoryFragmentBinding.tvFromDate.setText(mFromDate);
        mHistoryFragmentBinding.tvToDate.setText(mToDate);
        mHistoryFragmentBinding.btnSort.setText(mHistoryBiddingVM.getTitleSort(getContext()));
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mHistoryFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mHistoryFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());

    }

    private void getData(String fromDate, String toDate, String txt_search) {
        // get Data
        mHistoryBiddingVM.requestHistoryBidding(fromDate, toDate, txt_search, mHistoryBiddingVM.getStatusSelectSort(), mOffset, this::runUi);
    }

    private void refreshData() {
        mOffset = 0;
        mHistoryBiddingVM.clearData();
//        getData("2020-08-01", "2020-09-10");
        getData(mFromDate, mToDate, mHistoryFragmentBinding.edtSearch.getText().toString().trim());
    }

    private void loadMore() {
        int length = mHistoryBiddingVM.getHistoryBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            getData(mFromDate, mToDate, mHistoryFragmentBinding.edtSearch.getText().toString().trim());
        } else {
            //thông báo không còn gì để load
            adapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void offRefreshing() {
        mHistoryFragmentBinding.swipeLayout.setRefreshing(false);
        adapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mHistoryFragmentBinding.emptyData.setVisibility(View.GONE);
            mHistoryFragmentBinding.recyclerBidding.setVisibility(View.VISIBLE);
        } else {
            mHistoryFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            mHistoryFragmentBinding.recyclerBidding.setVisibility(View.GONE);
        }
    }


    // thay đổi date của minDate
    private void onDateSelected(int year, int month, int dayOfMonth) {
        mMinYear = year;
        mMinDay = dayOfMonth;
        mMinMoth = month;
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
        mMaxDay = setMaxDate(dayOfMonth);
        mFromDate = AppController.getDate(date, 0);
        mHistoryFragmentBinding.tvFromDate.setText(mFromDate);
    }

    private void selectTodate(int year, int month, int dayOfMonth) {
        mMinYear = year;
        mMinDay = dayOfMonth;
        mMinMoth = month;
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
        mMaxDay = setMaxDate(dayOfMonth);
        mToDate = AppController.getDate(date, 0);
        mHistoryFragmentBinding.tvToDate.setText(mToDate);
    }

    // set giá trị max đết khi chọn todate
    public int setMaxDate(int day) {
        Date date = new Date();
        date.setDate(day);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 3);
        date = c.getTime();
        return date.getDate();
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.viewGroup:
                Bundle args = new Bundle();
                args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer) ((BiddingInformation) o).getId());
                args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BID_HISTORY);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);
                break;
            case R.id.btnCancel:
                dialog = new Dialog(getString(R.string.reasons_for_canceling_the_package), getString(R.string.btnConfirm), ((BiddingInformation) o).getNote(), true, true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
                break;
        }
    }


    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HistoryBiddingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            if (mHistoryBiddingVM.getHistoryBidding().getValue() != null && mHistoryBiddingVM.getHistoryBidding().getValue().getRecords() != null) {
                adapter.setList(mHistoryBiddingVM.getHistoryBidding().getValue().getRecords());
                adapter.notifyDataSetChanged();
                offRefreshing();
                emptyData(mHistoryBiddingVM.getHistoryBidding().getValue().getRecords());
            }
        }

        if (action.equals(Constants.FAIL_API)) {
            offRefreshing();
            if (mHistoryBiddingVM.getHistoryBidding().getValue() != null && mHistoryBiddingVM.getHistoryBidding().getValue().getRecords() != null) {
                emptyData(mHistoryBiddingVM.getHistoryBidding().getValue().getRecords());
            } else {
                mHistoryFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnSort:
                new BiddingBottomSheet(getActivity(), HistoryBiddingFragment.this).show();
                break;

            case R.id.btnFromDate:
                datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.setOnDateSelectedListener(this::onDateSelected);
                datePickerDialogFragment.show(getChildFragmentManager(), "");
                break;

            case R.id.btnToDate:
//                if (mMinYear == 0) return;
//                datePickerDialogFragment = new DatePickerDialogFragment(mMinYear, mMinMoth, mMinDay, mMinYear, mMinMoth, mMaxDay, this::runUi);
//                datePickerDialogFragment.setOnDateSelectedListener((int year, int month, int dayOfMonth) -> {
//                    Date date = new Date();
//                    date.setDate(dayOfMonth);
//                    date.setMonth(month);
//                    mToDate = AppController.getDate(date, 0);
//                    mHistoryFragmentBinding.tvToDate.setText(mToDate);
//                    refreshData();
//                });
//                datePickerDialogFragment.show(getChildFragmentManager(), "");

                datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.setOnDateSelectedListener(this::selectTodate);
                datePickerDialogFragment.show(getChildFragmentManager(), "");
                refreshData();
                break;

            case R.id.btnSearch:
                refreshData();
                break;
        }
    }

    // Sắp xếp danh sách history
    @Override
    public void onSortListBidding(int ketSort) {
        mOffset = 0;
        mHistoryBiddingVM.setStatusSelectSort(ketSort);
        mHistoryFragmentBinding.btnSort.setText(mHistoryBiddingVM.sortBidding(mFromDate, mToDate,mHistoryFragmentBinding.edtSearch.toString().trim(), ketSort, mOffset, this::runUi, getContext()));
    }
}
