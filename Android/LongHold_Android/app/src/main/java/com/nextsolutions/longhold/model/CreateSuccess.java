package com.nextsolutions.longhold.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateSuccess {
    private String result;
}
