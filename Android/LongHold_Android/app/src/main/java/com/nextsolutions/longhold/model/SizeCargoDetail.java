package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SizeCargoDetail extends BaseModel {
    private float length;
    private float width;
    private float height;
    private String long_unit;
    private String weight_unit;

    public float getLength() {
        return length;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public String getLong_unit() {
        return long_unit;
    }

    public String getWeight_unit() {
        return weight_unit;
    }
}
