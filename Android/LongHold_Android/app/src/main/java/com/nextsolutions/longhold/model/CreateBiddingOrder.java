package com.nextsolutions.longhold.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBiddingOrder {
    private OdooDateTime max_confirm_time;
    private int bidding_order_id;
}
