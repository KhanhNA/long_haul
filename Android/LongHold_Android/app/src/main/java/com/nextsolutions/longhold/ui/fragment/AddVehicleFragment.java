package com.nextsolutions.longhold.ui.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.AddVehicleFragmentBinding;
import com.nextsolutions.longhold.model.ModelVehicle;
import com.nextsolutions.longhold.model.RangeVehicle;
import com.nextsolutions.longhold.model.TonnageVehicle;
import com.nextsolutions.longhold.model.Vehicle;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.utils.GlideEngine;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.utils.ZoomImage;
import com.nextsolutions.longhold.viewmodel.AddVehicleVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.Calendar;
import java.util.List;

public class AddVehicleFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    AddVehicleVM addVehicleVM;
    AddVehicleFragmentBinding mBinding;
    ListDialogFragment listModelVehicle;
    ListDialogFragment listTonnageVehicle;
    ListDialogFragment listCarManufacturer;
    Integer PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 1100;
    int from_type = 0; // trang thai hien thi 0: tạo xe mới, 1: sửa, xóa xe
    Vehicle vehicle;
    DialogConfirm edit_vehicle;
    DialogConfirm delete_vehicle;
    int type_choose_date = 0; //0 ngày đăng kí, 1 ngày đăng kiểm, 2 ngày hết hạn đăng kiểm

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (AddVehicleFragmentBinding) binding;
        addVehicleVM = (AddVehicleVM) viewModel;
        initView();
        return view;
    }

    public void initView() {
        Intent intent = getActivity().getIntent();
        from_type = intent.getIntExtra("FROM_TYPE", 0);
        if (from_type == 1) {
            mBinding.btnDeleteVehicle.setVisibility(View.VISIBLE);
            mBinding.btnSave.setText(getString(R.string.SAVE));
            vehicle = (Vehicle) intent.getSerializableExtra("MODEL");
            addVehicleVM.getVin_sn().set(vehicle.getVin_sn());
            addVehicleVM.getLicense_plate().set(vehicle.getLicense_plate());
            vehicle.getName();
        }
        mBinding.toolbar.setTitle(getString(R.string.add_vehicle));
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        addVehicleVM.getModelVehicle(this::runUi);
        addVehicleVM.getTonnage(this::runUi);
        addVehicleVM.getCarManufacturer(this::runUi);
        resetEditText();

        Calendar cal = Calendar.getInstance();
        mBinding.edtCreateDate.setOnClickListener(v -> showDialogDatePicker(0, null, cal.getTimeInMillis()));
        mBinding.edtDateRegistration.setOnClickListener(v -> showDialogDatePicker(1, null, cal.getTimeInMillis()));
        mBinding.edtRegistrationEndDate.setOnClickListener(v -> showDialogDatePicker(2, cal.getTimeInMillis(), null));
    }

    private void resetEditText() {
        mBinding.edtRegistrationEndDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipRegistrationEndDate.setError(null);
                }
            }
        });
        mBinding.edtCreateDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipCreateDate.setError(null);
                }
            }
        });
        mBinding.edtDateRegistration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipDateRegistration.setError(null);
                }
            }
        });
        mBinding.edtTonnage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipTonnage.setError(null);
                }
            }
        });
        mBinding.edtVinSn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipVinSn.setError(null);
                }
            }
        });
        mBinding.edtLicensePlates.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipLicensePlate.setError(null);
                }
            }
        });
        mBinding.edtVehicleInspection.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipVehicleInspection.setError(null);
                }
            }
        });
        mBinding.edtVehicleRegistration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipVehicleRegistration.setError(null);
                }
            }
        });
        mBinding.edtCarManufacturer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipCarManufacturer.setError(null);
                }
            }
        });
        mBinding.edtModel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBinding.ipModel.setError(null);
                }
            }
        });
    }

    public void previewImage() {
        ZoomImage.zoom(this, addVehicleVM.getUrl_image_vehicle().get());
    }

    private void showDialogDatePicker(int type_choose_date, Long minDate, Long maxDate) {
        this.type_choose_date = type_choose_date;
        Context scene = getContext();
        if (scene != null) {
            Calendar c = Calendar.getInstance();
            Integer year = c.get(Calendar.YEAR);
            Integer month = c.get(Calendar.MONTH);
            Integer day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            if (maxDate != null) {
                datePickerDialog.getDatePicker().setMaxDate(maxDate);
            }
            if (minDate != null) {
                datePickerDialog.getDatePicker().setMinDate(minDate);
            }
            datePickerDialog.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case "ADD_VEHICLE_FAIL":
                ToastUtils.showToast(getActivity(), getString(R.string.create_vehicle_fail), getResources().getDrawable(R.drawable.ic_x));
                break;
            case "EDIT_VEHICLE_FAIL":
                ToastUtils.showToast(getActivity(), getString(R.string.edit_vehicle_fail), getResources().getDrawable(R.drawable.ic_x));
                break;
            case "ADD_VEHICLE_SUCCESS":
                ToastUtils.showToast(getActivity(), getString(R.string.create_vehicle_success), getResources().getDrawable(R.drawable.ic_check));
                setResult();
                break;
            case "EDIT_VEHICLE_SUCCESS":
                ToastUtils.showToast(getActivity(), getString(R.string.Edit_vehicle_success), getResources().getDrawable(R.drawable.ic_check));
                setResult();
                break;
            default:
                break;
        }
    }

    private void setResult() {
        Intent intent = new Intent();
        getActivity().setResult(Constants.RESULT_OK, intent);
        getActivity().onBackPressed();
    }

    public void handleData(Integer from_type) {
        if (from_type == 0) {
            addVehicleVM.addVehicle(this::runUi);
        } else if (from_type == 1) {
            Log.d("Edit_ok", "onClick: ");
        } else if (from_type == 2) {
            Log.d("Delete_ok", "onClick: ");
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btnDeleteVehicle) {
            delete_vehicle = new DialogConfirm(getString(R.string.Delete_vehicle), getString(R.string.do_you_want_delete_vehicle), false, false, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleData(2);
                    delete_vehicle.dismiss();
                }
            });
            delete_vehicle.show(getChildFragmentManager(), "");
        } else if (validate()) {
            if (from_type == 0) {
                edit_vehicle = new DialogConfirm(getString(R.string.Add_vehicle), "", false, false, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleData(0);
                        edit_vehicle.dismiss();
                    }
                });
                edit_vehicle.show(getChildFragmentManager(), "");
            } else {
                edit_vehicle = new DialogConfirm(getString(R.string.Edit_vehicle), "", false, false, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleData(1);
                        edit_vehicle.dismiss();
                    }
                });
                edit_vehicle.show(getChildFragmentManager(), "");
            }
        }
    }

    public Boolean validate() {
        Boolean check = true;
        if (!addVehicleVM.getIsEmptyImage().get()) {
            mBinding.txtErrorEmptyImage.setVisibility(View.VISIBLE);
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getVin_sn().get())) {
            mBinding.ipVinSn.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getLicense_plate().get())) {
            mBinding.ipLicensePlate.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (addVehicleVM.getModel_id() == null) {
            mBinding.ipModel.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (addVehicleVM.getTonnage_id() == null) {
            mBinding.ipTonnage.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getCreateDate().get())) {
            mBinding.ipCreateDate.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getCarManufacturer().get())) {
            mBinding.ipCarManufacturer.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getRegistrationEndDate().get())) {
            mBinding.ipRegistrationEndDate.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getVehicleRegistration().get())) {
            mBinding.ipVehicleRegistration.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getDateRegistration().get())) {
            mBinding.ipDateRegistration.setError(getString(R.string.Not_empty));
            check = false;
        }
        if (StringUtils.isNullOrEmpty(addVehicleVM.getVehicleInspection().get())) {
            mBinding.ipVehicleInspection.setError(getString(R.string.Not_empty));
            check = false;
        }
        return check;
    }

    public void chooseCarManufacturer() {
        listCarManufacturer = new ListDialogFragment(R.layout.choose_range_vehicle, R.string.range_vehicle, "", addVehicleVM.getListCarManufacturer(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                RangeVehicle dto = (RangeVehicle) o;
                addVehicleVM.getCarManufacturer().set(dto.getName());
                addVehicleVM.setVehicle_type(dto.getId());
                listCarManufacturer.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listCarManufacturer.show(getChildFragmentManager(), "ABC");
    }

    public void chooseModelVehicle() {
        listModelVehicle = new ListDialogFragment(R.layout.choose_model_vehicle, R.string.model_vehicle, "", addVehicleVM.getListModelVehicle(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                ModelVehicle dto = (ModelVehicle) o;
                addVehicleVM.setModel_id(dto.getId());
                mBinding.edtModel.setText(dto.getBrand_name() + " - " + dto.getName());
                listModelVehicle.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listModelVehicle.show(getChildFragmentManager(), "ABC");
    }

    public void chooseTonnageVehicle() {
        listTonnageVehicle = new ListDialogFragment(R.layout.choose_tonnage_vehicle, R.string.tonnage, "", addVehicleVM.getListTonnageVehicle(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                TonnageVehicle dto = (TonnageVehicle) o;
                addVehicleVM.setTonnage_id(dto.getId());
                mBinding.edtTonnage.setText(dto.getMax_tonnage() + " kg");
                listTonnageVehicle.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listTonnageVehicle.show(getChildFragmentManager(), "ABC");
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            pickImage();
        }
    }


    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .isSingleDirectReturn(true)
                .maxSelectNum(1)
                .selectionMode(PictureConfig.SINGLE)
                .forResult(new OnResultCallbackListener() {
                    @Override
                    public void onResult(List result) {
                        if (result != null && result.size() > 0) {
                            LocalMedia localMedia = (LocalMedia) result.get(0);
                            addVehicleVM.getUrl_image_vehicle().set(localMedia.getCompressPath());
                            addVehicleVM.getIsEmptyImage().set(true);
                            addVehicleVM.setLstFileSelected(localMedia);
                            mBinding.txtErrorEmptyImage.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.add_vehicle_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AddVehicleVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = year + "-" + (month + 1) + "-" + dayOfMonth;
        if (type_choose_date == 0) {
            addVehicleVM.getCreateDate().set(date);
        } else if (type_choose_date == 1) {
            addVehicleVM.getDateRegistration().set(date);
        } else {
            addVehicleVM.getRegistrationEndDate().set(date);
        }
    }
}
