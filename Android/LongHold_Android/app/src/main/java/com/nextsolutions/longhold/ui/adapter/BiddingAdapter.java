package com.nextsolutions.longhold.ui.adapter;

import android.os.CountDownTimer;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.listener.AdapterListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.nextsolutions.longhold.base.AppController.hmsTimeFormatter;
import static com.nextsolutions.longhold.utils.BindingAdapterUtils.cvDateToTimeMillis;

public class BiddingAdapter extends BaseAdapterV3 {
    private ArrayList<BiddingInformation> arrayList = new ArrayList<>();
    private TextView mTextView;


    public BiddingAdapter(int layoutResId, @Nullable List data, AdapterListener listener) {
        super(layoutResId, data, listener);
        this.arrayList = (ArrayList<BiddingInformation>) data;
    }

    @Override
    public void onBindViewHolder(@NotNull BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        try {
            mTextView = holder.getView(R.id.tvMaxTime);
            if (arrayList.size() > 0) {
                downTimer(mTextView, arrayList.get(position).getMax_confirm_time(), arrayList.get(position));
            }
        }catch (Exception e){

        }
    }

    public static void downTimer(TextView view, OdooDateTime time, BiddingInformation biddingInformation) {
        //type = 2 && = 1 & = -1 không có countDow
        if (biddingInformation.getType().equals("2") || biddingInformation.getType().equals("1") || biddingInformation.getType().equals("-1"))
            return;
        // isDurationCountdow = true -> cowDow đã đc khởi tạo
        if (biddingInformation.isDurationCountdow()) return;
        //getMax_confirm_time null return k chạy countdow
        if (biddingInformation.getMax_confirm_time() == null) return;
        if (isValid(biddingInformation)) {
            biddingInformation.setDurationCountdow(true);
            new CountDownTimer(cvDateToTimeMillis(time), 1000) {
                public void onTick(long millisUntilFinished) {
                    view.setText(view.getContext().getString(R.string.duration_maxdate) + " " + hmsTimeFormatter(millisUntilFinished));
                }

                public void onFinish() {
                    view.setText(view.getContext().getString(R.string.expired));
                    biddingInformation.setCheckDuration(false);
                }
            }.start();
        }
    }

    private static boolean isValid(BiddingInformation biddingInformation) {
        if (biddingInformation.getBidding_type() == null)
            return true;
        if (biddingInformation.getBidding_type().equals("1") && biddingInformation.getCargo_status().equals("1"))
            return false;
        if (biddingInformation.getBidding_type().equals("2") && biddingInformation.getCargo_status().equals("2"))
            return false;
        return true;
    }
}
