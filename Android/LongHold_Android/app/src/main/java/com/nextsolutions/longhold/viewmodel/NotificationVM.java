package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.NotificationModel;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;

public class NotificationVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<NotificationModel>> mNotificationLiveData = new MutableLiveData<>();
    private OdooResultDto<NotificationModel> mNotification;

    public NotificationVM(@NonNull Application application) {
        super(application);
        mNotification = new OdooResultDto<>();
        mNotification.setRecords(new ArrayList<>());
        mNotificationLiveData.setValue(mNotification);
    }


    public MutableLiveData<OdooResultDto<NotificationModel>> getNotification() {
        return mNotificationLiveData;
    }

    public void requestGetNotification(int offset, RunUi runUi) {
        DriverApi.getNotification(offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {

                OdooResultDto<NotificationModel> response = (OdooResultDto<NotificationModel>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                mNotification.getRecords().addAll(response.getRecords());
                mNotification.setTotal_record(response.getTotal_record());
                mNotificationLiveData.setValue(mNotification);
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void clearData() {
        mNotification.getRecords().clear();
        mNotificationLiveData.setValue(null);
    }

    public void confirmRedNotification(int id) {
        DriverApi.confirmRedNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
