package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.nextsolutions.longhold.model.InformationUser;
import com.nextsolutions.longhold.model.UserInfo;
import com.tsolution.base.BaseViewModel;

public class AccountBiddingVM extends BaseViewModel<UserInfo> {



    private final MutableLiveData<InformationUser> mInformation = new MutableLiveData<>();
    private SharedPreferences.Editor editor;
    private UserInfo userInfo;
    public MutableLiveData<InformationUser> getListInformation() {
        return mInformation;
    }

    public AccountBiddingVM(@NonNull Application application) {
        super(application);
        editor = AppController.getInstance().getEditor();
        mInformation.setValue(AppController.getInformationUser());
    }
    public void clearPassword(){
        userInfo = new UserInfo();
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        model.set(userInfo);
    }
}
