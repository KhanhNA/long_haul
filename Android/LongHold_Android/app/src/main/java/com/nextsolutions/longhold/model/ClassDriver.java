package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClassDriver extends BaseModel {
    private Integer id;
    private String name;
    private Double max_tonnage;
}
