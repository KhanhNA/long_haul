package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.BiddingTimePackage;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.model.CreateBiddingOrder;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.STATUS_NOTIFICATION;

public class PackageTimeChildBiddingVM extends BaseViewModel {

    public MutableLiveData<OdooResultDto<BiddingPackage>> mBiddingPackageLive = new MutableLiveData<>();
    private OdooResultDto<BiddingPackage> mBiddingPackage;
    private ArrayList<Long> longArrayList = new ArrayList<>();
    private SharedPreferences preferences;

    public MutableLiveData<OdooResultDto<BiddingPackage>> getCalendarBidding() {
        return mBiddingPackageLive;
    }

    public PackageTimeChildBiddingVM(@NonNull Application application) {
        super(application);
        preferences = AppController.getInstance().getSharePre();
        mBiddingPackage = new OdooResultDto<>();
        mBiddingPackage.setRecords(new ArrayList<>());
        mBiddingPackageLive.setValue(mBiddingPackage);
    }

    public void requestBiddingPackage(String bidding_time, int order_by, int offset, RunUi runUi) {
        DriverApi.getBiddingPackage(bidding_time, order_by, offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<BiddingPackage> response = (OdooResultDto<BiddingPackage>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                mBiddingPackage.getRecords().addAll(response.getRecords());
                mBiddingPackage.setLength(response.getLength());
                mBiddingPackageLive.setValue(mBiddingPackage);
//                if (mBiddingPackageLive.getValue() != null && mBiddingPackageLive.getValue().getRecords() != null) {
//                    mBiddingPackageLive.getValue().getRecords().addAll(response.getRecords());
//                    mBiddingPackageLive.getValue().setLength(response.getLength());
//                } else mBiddingPackageLive.setValue((OdooResultDto<BiddingPackage>) o);

                runUi.run(Constants.SUCCESS_API);
                Log.e("reload", "onSuccess: requestBiddingPackage " );

            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);

            }
        });
    }

    public String getTitleSort(int order_by, Context context) {
        StringBuilder titleSort = new StringBuilder(context.getString(R.string.title_sort_btn) + " ");
        switch (order_by) {
            case EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING:
                titleSort.append(context.getString(R.string.priceAscending));
                break;
            case EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES:
                titleSort.append(context.getString(R.string.descendingPrices));
                break;
            case EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE:

                titleSort.append(context.getString(R.string.ascendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE:

                titleSort.append(context.getString(R.string.descendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.EAR_LIEST:
                titleSort.append(context.getString(R.string.earliest));
                break;
            case EnumBidding.EnumSortBiddingBehavior.OLD:
                titleSort.append(context.getString(R.string.sort_old));
                break;
        }
        return titleSort.toString();
    }

    public String sortBidding(String bidding_time, int order_by, int offset, RunUi runUi) {
        Context context = AppController.getInstance().getApplicationContext();
        StringBuilder titleSort = new StringBuilder(context.getString(R.string.title_sort_btn) + " ");
        switch (order_by) {
            case EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING:
                requestBiddingPackage(bidding_time, 6, offset, runUi);
                titleSort.append(context.getString(R.string.priceAscending));
                break;
            case EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES:
                requestBiddingPackage(bidding_time, 5, offset, runUi);
                titleSort.append(context.getString(R.string.descendingPrices));
                break;
            case EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE:
                requestBiddingPackage(bidding_time, 4, offset, runUi);
                titleSort.append(context.getString(R.string.ascendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE:
                requestBiddingPackage(bidding_time, 3, offset, runUi);
                titleSort.append(context.getString(R.string.descendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.EAR_LIEST:
                requestBiddingPackage(bidding_time, 2, offset, runUi);
                titleSort.append(context.getString(R.string.earliest));
                break;
            case EnumBidding.EnumSortBiddingBehavior.OLD:
                requestBiddingPackage(bidding_time, 1, offset, runUi);
                titleSort.append(context.getString(R.string.sort_old));
                break;
        }
        return titleSort.toString();
    }


    public void clearData() {
        mBiddingPackage.getRecords().clear();
    }

    public int getStatusSelectSort() {
        return Constants.STATIC_SELECT_SORT;
    }


    public void sortDate(ArrayList<BiddingTimePackage> arrayList) {

        for (int i = 0; i < arrayList.size(); i++) {
            longArrayList.add(arrayList.get(i).getCalendar().getTime());
        }
        Collections.sort(longArrayList);
    }


    public ArrayList<Long> getMilliseconds() {
        return longArrayList;
    }

    public void setStatusSelectSort(int order_by) {
        // lưu lại trạng thái order_by
        Constants.STATIC_SELECT_SORT = order_by;
    }

    public void saveStatusNotification(boolean isNotification) {
        preferences.edit().putBoolean(STATUS_NOTIFICATION, isNotification).apply();
    }

    public boolean getStatusNotification() {
        return preferences.getBoolean(STATUS_NOTIFICATION, true);
    }

    public boolean isCheckPackageStatus(ArrayList<Long> mCountDowTimeMillisecond) {
        if (mCountDowTimeMillisecond.size() < 2) {
            long timeBefore = mCountDowTimeMillisecond.get(0);
            long timeNow = System.currentTimeMillis();
            if (timeBefore < timeNow) {
                return true;
            } else {
                return false;
            }
        } else {
            long timeBefore = mCountDowTimeMillisecond.get(0);
            long timeAfter = mCountDowTimeMillisecond.get(1);
            long timeNow = System.currentTimeMillis();
            // case đang diễn ra trả về true và ngược lại
            if (timeNow > timeBefore && timeNow < timeAfter)
                return true;
        }
        return false;
    }

    public void removeBiddingSocket(List<BiddingPackage> lstBiddingPackages, RunUi runUi) {
        for (int i = 0; i < lstBiddingPackages.size(); i++) {
            String idBiddingSocket = lstBiddingPackages.get(i).getBidding_package_number();
            for (int j = 0; j < mBiddingPackage.getRecords().size(); j++) {
                String idBiddingPackage = mBiddingPackage.getRecords().get(j).getBidding_package_number();
                if (idBiddingPackage.equals(idBiddingSocket)) {
                    mBiddingPackage.getRecords().remove(j);
                }
            }

        }

        mBiddingPackageLive.postValue(mBiddingPackage);
        runUi.run(Constants.UPDATE_UI);
    }

    public void changPriceEventSocket(List<BiddingPackage> lstBiddingPackages, RunUi runUi) {
        for (int i = 0; i < lstBiddingPackages.size(); i++) {
            if (lstBiddingPackages.get(i).getStatus().equals("0")) {
                updateItem(lstBiddingPackages.get(i), runUi);
            } else if (lstBiddingPackages.get(i).getStatus().equals("-1")) {
                removeItem(lstBiddingPackages.get(i), runUi);
            }
        }


    }

    // Xác nhận đấu thầu
    public void createBiddingOrder(BiddingPackage biddingPackage, RunUi runUi) {


        DriverApi.createBiddingOrder(biddingPackage.getId() + "", new Date(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o != null){
                    OdooResultDto<CreateBiddingOrder> resultDto = (OdooResultDto<CreateBiddingOrder>) o;
                    if(resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                        biddingPackage.setBidding_order_id(resultDto.getRecords().get(0).getBidding_order_id());
                        biddingPackage.setMax_confirm_time(resultDto.getRecords().get(0).getMax_confirm_time());
                        runUi.run("createBiddingOrderSuccess", biddingPackage);
                    }else {
                        runUi.run("createBiddingOrderFail");
                    }
                }else {
                    runUi.run("createBiddingOrderFail");
                }

            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("createBiddingOrderFail");
                Log.d("TAG", "onFail: "+error.getMessage());
            }
        });

    }

    private void removeItem(BiddingPackage biddingPackage, RunUi runUi) {
        if (mBiddingPackage.getRecords() == null) return;
        for (int i = 0; i < mBiddingPackage.getRecords().size(); i++) {
            String idBiddingPackage = mBiddingPackage.getRecords().get(i).getBidding_package_number();
            if (idBiddingPackage.equals(biddingPackage.getBidding_package_number())) {
                mBiddingPackage.getRecords().remove(i);
            }
        }
        mBiddingPackageLive.postValue(mBiddingPackage);
        runUi.run(Constants.UPDATE_UI);

    }

    private void updateItem(BiddingPackage biddingPackage, RunUi runUi) {
        for (int i = 0; i < mBiddingPackage.getRecords().size(); i++) {
            String idBiddingPackage = mBiddingPackage.getRecords().get(i).getBidding_package_number();
            if (idBiddingPackage.equals(biddingPackage.getBidding_package_number())) {
                mBiddingPackage.getRecords().set(i, biddingPackage);
            }
        }
        mBiddingPackageLive.postValue(mBiddingPackage);
        runUi.run(Constants.UPDATE_UI);
    }

    public void addBiddingSocket(List<BiddingPackage> lstBiddingPackages, RunUi runUi) {
        for (int i = 0; i < lstBiddingPackages.size(); i++) {
            mBiddingPackage.getRecords().add(0, lstBiddingPackages.get(i));
        }
        mBiddingPackageLive.postValue(mBiddingPackage);
        runUi.run(Constants.UPDATE_UI);
    }

    public void createBiddingOrder(String bidding_package_id, Date confirm_time, RunUi runUi) {

        DriverApi.createBiddingOrder(bidding_package_id, confirm_time, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o != null){
                    runUi.run("createBiddingOrderSuccess");
                }else {
                    runUi.run("createBiddingOrderFail");
                }

            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("createBiddingOrderFail");
                Log.d("TAG", "onFail: "+error.getMessage());
            }
        });

    }
}
