package com.nextsolutions.longhold.base;


import com.nextsolutions.longhold.model.ApiResponseModel;
import com.nextsolutions.longhold.model.CreateSuccess;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface SOService {
    @Multipart
    @POST("/bidding/creat_account_driver")
    Call<CreateSuccess> upLoadImage(
            @Header("Cookie") String cookie,
            @Part MultipartBody.Part part,
            @Part("driverInfo") RequestBody requestBody

    );

    @Multipart
    @POST("/code_share/create_vehicle_code_share")
    Call<ResponseBody> addVehicle(
            @Header("Cookie") String cookie,
            @Part("vehicle") RequestBody requestBody,
            @Part MultipartBody.Part files

    );


    @Multipart
    @POST("bidding/update_account_driver")
    Call<CreateSuccess> upDateInfo(
            @Header("Cookie") String cookie,
            @Part MultipartBody.Part part,
            @Part("driverInfo") RequestBody requestBody

    );

    @Multipart
    @POST("bidding/update_account_long_haul")
    Call<CreateSuccess> updateInformationUser(@Header("Cookie") String cookie,
                                              @Part MultipartBody.Part part,
                                              @Part("longHaulInfo") RequestBody requestBody);

    @Multipart
    @POST("code_share/create_driver_code_share")
    Call<ApiResponseModel> saveDriverInfo(@Header("Cookie") String cookie,
                                          @Part MultipartBody.Part[] files,
                                          @Part("driver") RequestBody driver);

    @Multipart
    @POST("code_share/update_deactive_driver")
    Call<ResponseBody> updateDriverInfo(@Header("Cookie") String cookie,
                                          @Part MultipartBody.Part[] files,
                                          @Part("driver") RequestBody requestBody);
}
