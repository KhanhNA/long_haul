package com.nextsolutions.longhold.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RateModel {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bidding_package_number")
    @Expose
    private String biddingPackageNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("confirm_time")
    @Expose
    private String confirmTime;
    @SerializedName("release_time")
    @Expose
    private String releaseTime;
    @SerializedName("bidding_time")
    @Expose
    private String biddingTime;
    @SerializedName("max_count")
    @Expose
    private Integer maxCount;
    @SerializedName("total_weight")
    @Expose
    private Double totalWeight;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("from_latitude")
    @Expose
    private Double fromLatitude;
    @SerializedName("from_longitude")
    @Expose
    private Double fromLongitude;
    @SerializedName("to_latitude")
    @Expose
    private Double toLatitude;
    @SerializedName("to_longitude")
    @Expose
    private Double toLongitude;
    @SerializedName("from_receive_time")
    @Expose
    private String fromReceiveTime;
    @SerializedName("to_receive_time")
    @Expose
    private String toReceiveTime;
    @SerializedName("from_return_time")
    @Expose
    private String fromReturnTime;
    @SerializedName("to_return_time")
    @Expose
    private String toReturnTime;
    @SerializedName("price_origin")
    @Expose
    private Double priceOrigin;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("write_date")
    @Expose
    private Integer writeDate;
    @SerializedName("countdown_time")
    @Expose
    private Integer countdownTime;
    @SerializedName("bidding_order")
    @Expose
    private BiddingOrder biddingOrder;
    private boolean isCheckShowAll = false;

    public boolean isCheckShowAll() {
        return isCheckShowAll;
    }

    public void setCheckShowAll(boolean checkShowAll) {
        isCheckShowAll = checkShowAll;
    }

    @Getter
    @Setter
    public class BiddingOrder extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("company_id")
        @Expose
        private Integer companyId;
        @SerializedName("bidding_order_number")
        @Expose
        private String biddingOrderNumber;
        @SerializedName("from_depot")
        @Expose
        private FromDepot fromDepot;
        @SerializedName("to_depot")
        @Expose
        private ToDepot toDepot;
        @SerializedName("total_weight")
        @Expose
        private Integer totalWeight;
        @SerializedName("total_cargo")
        @Expose
        private Integer totalCargo;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("bidding_order_receive_id")
        @Expose
        private String biddingOrderReceiveId;
        @SerializedName("bidding_order_return_id")
        @Expose
        private String biddingOrderReturnId;
        @SerializedName("create_date")
        @Expose
        private String createDate;
        @SerializedName("write_date")
        @Expose
        private String writeDate;
        @SerializedName("bidding_package_id")
        @Expose
        private Integer biddingPackageId;
        @SerializedName("from_receive_time")
        @Expose
        private String fromReceiveTime;
        @SerializedName("to_receive_time")
        @Expose
        private String toReceiveTime;
        @SerializedName("from_return_time")
        @Expose
        private String fromReturnTime;
        @SerializedName("to_return_time")
        @Expose
        private String toReturnTime;
        @SerializedName("max_confirm_time")
        @Expose
        private String maxConfirmTime;
        @SerializedName("bidding_vehicles")
        @Expose
        private List<BiddingVehicle> biddingVehicles = null;
        @SerializedName("order_rate_avg")
        @Expose
        private Double orderRateAvg;

    }

    @Getter
    @Setter
    public class BiddingVehicle extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("lisence_plate")
        @Expose
        private String lisencePlate;
        @SerializedName("driver_phone_number")
        @Expose
        private String driverPhoneNumber;
        @SerializedName("driver_name")
        @Expose
        private String driverName;
        @SerializedName("expiry_time")
        @Expose
        private String expiryTime;
        @SerializedName("company_id")
        @Expose
        private Integer companyId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("vehicle_type")
        @Expose
        private Integer vehicleType;
        @SerializedName("weight_unit")
        @Expose
        private String weightUnit;
        @SerializedName("store_fname")
        @Expose
        private String storeFname;
        @SerializedName("cargo_types")
        @Expose
        private List<CargoType> cargoTypes = null;
        @SerializedName("rating")
        @Expose
        private Rating rating;



    }

    @Getter
    @Setter
    public class CargoType extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("length")
        @Expose
        private Integer length;
        @SerializedName("width")
        @Expose
        private Integer width;
        @SerializedName("height")
        @Expose
        private Integer height;
        @SerializedName("long_unit_moved0")
        @Expose
        private String longUnitMoved0;
        @SerializedName("weight_unit_moved0")
        @Expose
        private String weightUnitMoved0;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("from_weight")
        @Expose
        private String fromWeight;
        @SerializedName("to_weight")
        @Expose
        private String toWeight;
        @SerializedName("price_id")
        @Expose
        private String priceId;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("size_standard_seq")
        @Expose
        private String sizeStandardSeq;
        @SerializedName("long_unit")
        @Expose
        private Integer longUnit;
        @SerializedName("weight_unit")
        @Expose
        private Integer weightUnit;
        @SerializedName("cargo_quantity")
        @Expose
        private Integer cargoQuantity;
        @SerializedName("total_weight")
        @Expose
        private Integer totalWeight;

    }

    @Getter
    @Setter
    public class FromDepot extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("depot_code")
        @Expose
        private String depotCode;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("street2")
        @Expose
        private String street2;
        @SerializedName("city_name")
        @Expose
        private String cityName;


    }

    @Getter
    @Setter
    public class Rating extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("driver_id")
        @Expose
        private String driverId;
        @SerializedName("num_rating")
        @Expose
        private Integer numRating;
        @SerializedName("employee_id")
        @Expose
        private Integer employeeId;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("create_date")
        @Expose
        private OdooDateTime createDate;
        @SerializedName("bidding_vehicle_id")
        @Expose
        private Integer biddingVehicleId;
        @SerializedName("bidding_order_id")
        @Expose
        private Integer biddingOrderId;
        @SerializedName("rating_badges")
        @Expose
        private List<RatingBadge> ratingBadges = null;
        @SerializedName("images")
        @Expose
        private List<String> images = null;
    }

    @Getter
    @Setter
    public class RatingBadge extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("code_seq")
        @Expose
        private String codeSeq;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("create_date")
        @Expose
        private String createDate;

    }

    @Getter
    @Setter
    public class ToDepot extends BaseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("depot_code")
        @Expose
        private String depotCode;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("street2")
        @Expose
        private String street2;
        @SerializedName("city_name")
        @Expose
        private String cityName;
    }


}
