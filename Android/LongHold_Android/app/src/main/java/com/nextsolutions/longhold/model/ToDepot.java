package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ToDepot extends BaseModel {
    private Integer id;
    private String name;
    private String depotCode;
    private String address;
    private String street;
    private String street2;
    private Object cityName;
}