package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingHomeVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<BiddingInformation>> mInformationBidding = new MutableLiveData<>();
    private OdooResultDto<BiddingInformation> mInformationUpdate;
    private SharedPreferences preferences;


    public BiddingHomeVM(@NonNull Application application) {
        super(application);
        preferences = AppController.getInstance().getSharePre();
        mInformationUpdate = new OdooResultDto<>();
        mInformationUpdate.setRecords(new ArrayList<>());
        mInformationBidding.setValue(mInformationUpdate);
    }

    public MutableLiveData<OdooResultDto<BiddingInformation>> getInformationBidding() {
        return mInformationBidding;
    }

    public void getInformationBidding(String type, String search_text, Integer offset, RunUi runUi) {
        DriverApi.getInformationBidding(type, search_text, offset,
                new IResponse() {
                    @Override
                    public void onSuccess(Object o) {
                        OdooResultDto<BiddingInformation> response = (OdooResultDto<BiddingInformation>) o;
                        if (response == null) {
                            runUi.run(Constants.FAIL_API);
                            return;
                        }

                        if(offset == 0){
                            mInformationUpdate.getRecords().clear();
                        }
                        mInformationUpdate.getRecords().addAll(response.getRecords());
                        mInformationUpdate.setLength(response.getLength());
                        mInformationBidding.setValue(mInformationUpdate);
//                        if (mInformationBidding.getValue() != null && mInformationBidding.getValue().getRecords() != null) {
//                            mInformationBidding.getValue().getRecords().addAll(response.getRecords());
//                            mInformationBidding.getValue().setLength(response.getLength());
//                        } else mInformationBidding.setValue((OdooResultDto<BiddingInformation>) o);
                        runUi.run(Constants.SUCCESS_API);

                    }

                    @Override
                    public void onFail(Throwable error) {
                        runUi.run(Constants.FAIL_API);
                    }
                });
    }

    //khi search_text là rỗng tức là get dữ liệu bình thường
    //khi search_text khác rỗng thì sẽ tìm kiếm theo ký tự
    public void requestDataInformationBidding(int keyPage, RunUi runUi, String search_text, int offset) {
        switch (keyPage) {
            case EnumBidding.EnumBiddingBehavior.BIDDING_HAPPENNING:
                break;
            case EnumBidding.EnumBiddingBehavior.WAITING_FOR_INFORMATION:
                getInformationBidding("0", search_text, offset, runUi);
                break;
            case EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION:
                getInformationBidding("2", search_text, offset, runUi);
                break;
            case EnumBidding.EnumBiddingBehavior.BID_SUCCESS:
                getInformationBidding("3", search_text, offset, runUi);
                break;
        }
    }

    public void clearData() {
        mInformationUpdate.getRecords().clear();
    }

    public void removeBiddingSocket(List<BiddingPackage> lstBiddingPackages,RunUi runUi) {
        for (int i = 0; i < lstBiddingPackages.size(); i++) {
            int idBiddingSocket = lstBiddingPackages.get(i).getId();

            for (int j = 0; j < mInformationUpdate.getRecords().size(); j++) {
                int idBiddingPackage = mInformationUpdate.getRecords().get(j).getId();

                if (idBiddingPackage == idBiddingSocket) {
                    mInformationUpdate.getRecords().remove(j);
                }
            }
        }
        mInformationBidding.setValue(mInformationUpdate);
        runUi.run(Constants.UPDATE_UI);

    }
}
