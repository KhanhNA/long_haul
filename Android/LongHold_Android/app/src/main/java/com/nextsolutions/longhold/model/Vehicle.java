package com.nextsolutions.longhold.model;


import android.annotation.SuppressLint;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle extends BaseModel {
    private Long id;
    private String name;

    private String image_logo;
    private String vehicle_image;
    private List<String> list_image;
    private String vin_sn;

    private String license_plate;
    private String color;
    private Integer model_year;
    private String fuel_type;
    private Integer body_length;
    private Integer body_width;
    private Integer height;
    private Double capacity;
    private Double available_capacity;

    private String vehicle_type;
    private String approved_check;
    private Float engine_size;//

    private Integer vehicle_status;

    private Double latitude;
    private Double longitude;
    private String tonage_name;

    private Integer model_id;
    private String acquisition_date;
    private String inspection_due_date;
    private String vehicle_registration;
    private String vehicle_inspection;

    private List<BiddingOrder> bidding_orders;


    public String getVehicle_image() {
        if (list_image == null || list_image.size() == 0) {
            return "";
        }
        return list_image.get(0);
    }

    @SuppressLint("DefaultLocale")
    public String getCapacityStr() {
        if (capacity == null) return "";
        return String.format("%.1f", capacity);
    }

    @SuppressLint("DefaultLocale")
    public String getAvailableCapacityStr(){
        if(capacity == null) return "";
        return String.format("%.1f", capacity);
    }

}
