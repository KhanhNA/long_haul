package com.nextsolutions.longhold.ui.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.model.RateModel;
import com.tsolution.base.listener.AdapterListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VehiclesRateAdapter extends BaseAdapterV3 {
    RateAdapter rateAdapter;

    public VehiclesRateAdapter(int layoutResId, @Nullable List data, AdapterListener listener) {
        super(layoutResId, data, listener);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
        super.convert(holder, o);
        if (o instanceof RateModel) {
            RateModel rateModel = (RateModel) o;
            rateAdapter = new RateAdapter(R.layout.item_vehicles, rateModel.getBiddingOrder().getBiddingVehicles(), rateModel.isCheckShowAll());
            ((RecyclerView) holder.getView(R.id.recyclerRate)).setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            ((RecyclerView) holder.getView(R.id.recyclerRate)).setAdapter(rateAdapter);
            ((AppCompatTextView) holder.getView(R.id.btnLoadMore)).setVisibility(rateModel.isCheckShowAll() ? View.GONE : View.VISIBLE);
        }
    }
}
