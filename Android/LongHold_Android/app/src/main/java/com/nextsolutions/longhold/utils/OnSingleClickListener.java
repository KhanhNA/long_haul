package com.nextsolutions.longhold.utils;

import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.tsolution.base.listener.AdapterListener;

import androidx.databinding.BindingAdapter;

public abstract class OnSingleClickListener implements View.OnClickListener {


    @BindingAdapter({"listener", "object"})
    public static void setOnSingleClickListener(View v, AdapterListener adapterListener, Object o) {
        v.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                adapterListener.onItemClick(v, o);
            }
        });
    }

    @BindingAdapter("listener")
    public static void setOnSingleClickListener(View v, AdapterListener adapterListener) {
        v.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Log.e("SINGLE_CLICK", "click");
                adapterListener.onItemClick(v, null);
            }
        });
    }

    private static final long MIN_CLICK_INTERVAL = 500;
    private long mLastClickTime;

    public abstract void onSingleClick(View v);

    @Override
    public final void onClick(View v) {
        long currentClickTime = SystemClock.elapsedRealtime();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;
        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;
        onSingleClick(v);
    }

}