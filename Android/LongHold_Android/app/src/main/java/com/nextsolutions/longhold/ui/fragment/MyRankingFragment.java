package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.MyRankingFragmentBinding;
import com.nextsolutions.longhold.model.RankingDTO;
import com.nextsolutions.longhold.ui.viewcommon.PagerAdapter;
import com.nextsolutions.longhold.viewmodel.MyRankingVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

public class MyRankingFragment extends BaseFragment {
    MyRankingFragmentBinding mBinding;
    MyRankingVM rankingVM;
    XBaseAdapter adapterTitle;
    int currentPosition = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        rankingVM = (MyRankingVM) viewModel;
        mBinding = (MyRankingFragmentBinding) binding;
        init();
        getData();
        return view;
    }

    private void init() {
        mBinding.toolbar.setTitle(R.string.my_ranking);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setUpRecycleView();
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.API_SUCCESS)) {
            rankingVM.getListRanking().get(currentPosition).checked = true;
            mBinding.frameContainer.setCurrentItem(rankingVM.getIndex_rank_current());
            mBinding.rcpNextRank.setProgress(rankingVM.getMyRankingCustomer().get().getTotal_point());
            mBinding.rcpNextRank.setMax(rankingVM.getRankCurrent().get().getTo_point());
            mBinding.lbNeedPoint2.setText(" " + (rankingVM.getRankCurrent().get().getTo_point() - rankingVM.getMyRankingCustomer().get().getTotal_point()) + " ");
            mBinding.lbNeedPoint2.setTextColor(getResources().getColor(R.color.green_text));
            setUpViewpager();
            adapterTitle.notifyDataSetChanged();
        }
    }
    private void setUpViewpager() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        for (RankingDTO rankingDTO: rankingVM.getListRanking()) {
            MyRankingChildFragment Bronze = new MyRankingChildFragment(rankingDTO.getList_awards_leve());
            myPagerAdapter.addFragment(Bronze);
        }

        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if(position != currentPosition){
                    rankingVM.getListRanking().get(position).checked = true;
                    rankingVM.getListRanking().get(currentPosition).checked = false;
                    adapterTitle.notifyItemChanged(position);
                    adapterTitle.notifyItemChanged(currentPosition);
                    currentPosition = position;
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void getData() {
        rankingVM.getData(this::runUi);
    }

    private void setUpRecycleView() {
        adapterTitle = new XBaseAdapter(R.layout.my_ranking_title, rankingVM.getListRanking(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((RankingDTO) o).index - 1;
                rankingVM.getListRanking().get(position).checked = true;
                rankingVM.getListRanking().get(currentPosition).checked = false;
                adapterTitle.notifyItemChanged(position);
                adapterTitle.notifyItemChanged(currentPosition);
                mBinding.frameContainer.setCurrentItem(position);
                currentPosition = position;
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcMyRanking.setAdapter(adapterTitle);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false);
        mBinding.rcMyRanking.setLayoutManager(layoutManager);
    }
    public void gotoHistoryPoint(){
        Intent intent= new Intent(getContext(), CommonActivity.class);
        Bundle bundle= new Bundle();
        bundle.putSerializable(Constants.FRAGMENT,HistoryPointFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.my_ranking_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MyRankingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
