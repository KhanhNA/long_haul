package com.nextsolutions.longhold.api;

import com.nextsolutions.longhold.base.IResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;

public abstract class SharingOdooResponse<Result> implements IOdooResponse<Result>, IResponse<Result> {
    @Override
    public void onResponse(Result result, Throwable volleyError) {
        if(volleyError != null){
            volleyError.printStackTrace();
            onFail(volleyError);
            return;
        }
        onSuccess(result);
    }
}
