package com.nextsolutions.longhold.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.nextsolutions.longhold.R;


public class CustomDialogProgress extends Dialog {
    public CustomDialogProgress(Context context) {
        super(context);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();

        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        View view = LayoutInflater.from(context).inflate(
                R.layout.layout_progress, null);
        setContentView(view);
    }


}
