package com.nextsolutions.longhold.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.VehicleManagementFragmentBinding;
import com.nextsolutions.longhold.ui.viewcommon.PagerAdapter;
import com.nextsolutions.longhold.viewmodel.VehicleManagementVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class VehicleManagementFragment extends BaseFragment {
    VehicleManagementVM vehicleManagementVM;
    VehicleManagementFragmentBinding mBinding;
    MenuItem menuItem;
    ListDriverFragment listDriverFragment;
    ListVehicleFragment listVehicleFragment;
    private int currentTap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (VehicleManagementFragmentBinding) binding;
        vehicleManagementVM = (VehicleManagementVM) viewModel;

        initViewPagerTabLayout();

        initOnSearchView();

        mBinding.edtSearch.setHint(R.string.search_list_vehicle);

        return view;
    }

    private void initViewPagerTabLayout() {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        listDriverFragment = new ListDriverFragment();
        listVehicleFragment = new ListVehicleFragment();
        adapter.addFragment(listVehicleFragment);
        adapter.addFragment(listDriverFragment);
        mBinding.frameContainer.setAdapter(adapter);
        mBinding.frameContainer.setOffscreenPageLimit(2);
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mBinding.tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.frameContainer.setCurrentItem(tab.getPosition(), false);

                currentTap = mBinding.tabLayout.getSelectedTabPosition();

                if (currentTap == 0) {
                    mBinding.edtSearch.setHint(R.string.search_list_vehicle);
                    vehicleManagementVM.getTxtSearch().set("");
                    searchInfo();
                } else if (currentTap == 1) {
                    mBinding.edtSearch.setHint(R.string.search_list_driver);
                    vehicleManagementVM.getTxtSearch().set("");
                    searchInfo();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initOnSearchView() {

        mBinding.tilSearch.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Hide keyboard
                v = getActivity().getCurrentFocus();
                if (v != null) {
                    InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }

                searchInfo();
            }
        });

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void searchInfo() {
        if (currentTap == 0) {
            listVehicleFragment.searchInfo(vehicleManagementVM.getTxtSearch().get());
        } else if (currentTap == 1) {
            listDriverFragment.searchInfo(vehicleManagementVM.getTxtSearch().get());
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.vehicle_management_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return VehicleManagementVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
