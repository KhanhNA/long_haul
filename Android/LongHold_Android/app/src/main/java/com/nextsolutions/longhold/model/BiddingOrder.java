package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingOrder extends BaseModel {
    private Integer id;
    private String name;
}
