package com.nextsolutions.longhold.model;

import com.nextsolutions.longhold.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingPackage extends BaseModel {
    private Integer id;
    private Integer bidding_order_id;
    private String bidding_package_number;
    private String status;
    private String confirm_time;
    private String release_time;
    private String bidding_time;
    private Integer max_count;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private Double total_weight;
    private Double distance;
    private Double from_latitude;
    private Double from_longitude;
    private Double to_latitude;
    private Double to_longitude;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private Double price_origin;
    private Double price;
    private String create_date;
    private String write_date;
    private Integer countdown_time;
    private String price_time_change;
    private Integer price_level_change;
    private Integer total_cargos;
    private OdooDateTime max_confirm_time;
    private List<CargoTypes> cargo_types;

    public String getTotal_weight_str(){
        return this.total_weight != null ? (AppController.getInstance().formatNumber(total_weight) + "kg") : "";
    }

    public String getPrice_str(){
        return this.price != null ? AppController.getInstance().formatCurrency(price) : "";
    }

}
