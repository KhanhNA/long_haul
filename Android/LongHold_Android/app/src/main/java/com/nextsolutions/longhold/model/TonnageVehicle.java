package com.nextsolutions.longhold.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TonnageVehicle extends BaseModel {
    Integer id;
    String code;
    String name;
    String status;
    String description;
//    OdooDateTime create_date;
    Long max_tonnage;
}
