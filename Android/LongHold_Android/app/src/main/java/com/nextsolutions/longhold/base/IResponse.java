package com.nextsolutions.longhold.base;


import com.android.volley.VolleyError;

public interface IResponse<Result>{
    void onSuccess(Result result);
    void onFail(Throwable error);
}
