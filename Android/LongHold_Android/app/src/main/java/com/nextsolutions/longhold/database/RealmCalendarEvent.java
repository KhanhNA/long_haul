package com.nextsolutions.longhold.database;

import android.content.Context;

import com.google.android.gms.tasks.Task;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

import static com.nextsolutions.longhold.database.CalendarEvent.ID_EVENT;


public class RealmCalendarEvent {
    private static RealmCalendarEvent realmDTB;
    private static Realm mRealm;
    private static Context mContext;

    public static RealmCalendarEvent getInstance(Context context) {
        if (realmDTB == null) {
            realmDTB = new RealmCalendarEvent();
        }
        mRealm = Realm.getDefaultInstance();
        if (context != null) {
            mContext = context;
        }
        return realmDTB;
    }


    public void addIdEvent(final String mID) {




        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createObject(CalendarEvent.class, UUID.randomUUID().toString())
                        .setMIdEvent(mID);
            }
        });
    }


    public List<CalendarEvent> readCalendar() {
        AtomicReference<RealmResults<CalendarEvent>> results = new AtomicReference<>();
        mRealm.executeTransaction(realm -> {
            results.set(realm.where(CalendarEvent.class).findAll());
        });
        return results.get();
    }

    public void updateEmployeeRecords() {
        mRealm.executeTransaction(realm -> {
            RealmResults<CalendarEvent> calendarEvent = realm.where(CalendarEvent.class).findAll();
            for (int i = 0; i < calendarEvent.size(); i++) {
            }
        });
    }

    public void deleteCalendar(String id) {
        mRealm.executeTransactionAsync(realm -> realm.where(CalendarEvent.class).equalTo(ID_EVENT, id)
                .findFirst()
                .deleteFromRealm());
    }

}
