package com.nextsolutions.longhold.base;

public interface RunUi {
    void run(Object... params);
}
