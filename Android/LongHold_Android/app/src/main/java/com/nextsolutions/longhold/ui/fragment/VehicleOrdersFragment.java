package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.FragmentVehicleOrdersBinding;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.utils.BindingAdapterUtils;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.VehicleOrdersFragmentVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.List;
import java.util.Objects;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;

public class VehicleOrdersFragment extends BaseFragment {
    public BaseAdapterV3 adapter;
    private VehicleOrdersFragmentVM mVehicleOrdersVM;
    private FragmentVehicleOrdersBinding mVehicleOrdersBinding;
    private BiddingVehicles mBiddingVehicle;
    private VehicleCargo mVehicleCargo = new VehicleCargo();
    private static VehicleOrdersFragment biddingFragment;
    private int mOffset = 0;

    public static VehicleOrdersFragment getInstance() {
        if (biddingFragment == null) {
            biddingFragment = new VehicleOrdersFragment();
        }
        return biddingFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_vehicle_orders;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return VehicleOrdersFragmentVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mVehicleOrdersVM = (VehicleOrdersFragmentVM) viewModel;
        mVehicleOrdersBinding = (FragmentVehicleOrdersBinding) binding;
        biddingFragment = this;
        getData();
        iniView();
        initLoadMore();
        initRefreshData();
        return view;
    }

    private void iniView() {
        // init adapter & recyclerView
        adapter = new BaseAdapterV3(R.layout.item_bid, mVehicleOrdersVM.getInformationBidding().getValue().getRecords(), this);
        mVehicleOrdersBinding.recyclerViewBidOr.setLayoutManager(new LinearLayoutManager(getContext()));
        mVehicleOrdersBinding.recyclerViewBidOr.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        mVehicleOrdersBinding.recyclerViewBidOr.invalidate();
        //setInformationCargo
        if (mVehicleCargo != null) {
            mVehicleOrdersBinding.txtLicensePlatesOr.setText(mVehicleCargo.getLisence_plate()!=null?"BKS " + mVehicleCargo.getLisence_plate():"");
            mVehicleOrdersBinding.txtNameOr.setText(mVehicleCargo.getDriver_name());
            mVehicleOrdersBinding.txtPhoneOr.setText(mVehicleCargo.getDriver_phone_number());
            BindingAdapterUtils.loadImage(mVehicleOrdersBinding.ivAvt, mVehicleCargo.getImage_128());
        } else if (mBiddingVehicle != null) {
            setDataView();
        }
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mVehicleOrdersBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mVehicleOrdersBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void refreshData() {
        // refresh data
        mVehicleOrdersVM.clearData();
        mOffset = 0;
        getData();
    }

    private void loadMore() {
        int length = mVehicleOrdersVM.getInformationBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            mVehicleOrdersVM.getBiddingOrderDetail("7", mOffset, this::runUi);
        } else {
            //thông báo không còn gì để load
            adapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void offRefreshing() {
        mVehicleOrdersBinding.swipeLayout.setRefreshing(false);
        adapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void getData() {
        mVehicleCargo = (VehicleCargo) Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(getActivity()).getIntent().getExtras()).getBundle(DATA_PASS_FRAGMENT)).getSerializable(Constants.DATA_SERIALIZABLE);
        mBiddingVehicle = (BiddingVehicles) Objects.requireNonNull(Objects.requireNonNull(getActivity().getIntent().getExtras()).getBundle(DATA_PASS_FRAGMENT)).getSerializable(StringUtils.TO_VEHICLE_ORDES_FRAGMEN);
        int idBid;
        if (mVehicleCargo != null) idBid = mVehicleCargo.getId();
        else idBid = mBiddingVehicle.getId();
        mVehicleOrdersVM.getBiddingOrderDetail(String.valueOf(idBid), mOffset, this::runUi);
        //mVehicleOrdersVM.getBiddingOrderDetail(String.valueOf(26), mOffset, this::runUi);
    }

    private void setDataView() {
        mVehicleOrdersBinding.txtLicensePlatesOr.setText(mBiddingVehicle.getLisence_plate()!=null?"BKS: " + mBiddingVehicle.getLisence_plate():"");
        mVehicleOrdersBinding.txtNameOr.setText(mBiddingVehicle.getDriver_name());
        mVehicleOrdersBinding.txtPhoneOr.setText(mBiddingVehicle.getDriver_phone_number());
        BindingAdapterUtils.loadImage(mVehicleOrdersBinding.ivAvt, mBiddingVehicle.getImage_128());

    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            adapter.setList(mVehicleOrdersVM.getInformationBidding().getValue().getRecords());
            adapter.notifyDataSetChanged();
            mVehicleOrdersBinding.recyclerViewBidOr.invalidate();
            emptyData(mVehicleOrdersVM.getInformationBidding().getValue().getRecords());
            offRefreshing();


        }
        if (action.equals(Constants.FAIL_API)) {
            emptyData(mVehicleOrdersVM.getInformationBidding().getValue().getRecords());
            offRefreshing();
        }
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mVehicleOrdersBinding.hideData.setVisibility(View.GONE);
            mVehicleOrdersBinding.swipeLayout.setVisibility(View.VISIBLE);
        } else {
            mVehicleOrdersBinding.hideData.setVisibility(View.VISIBLE);
            mVehicleOrdersBinding.swipeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        switch (id) {
            case R.id.itemBid:
                Bundle args = new Bundle();
                args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer) ((BiddingInformation) o).getId());
                args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BID_SUCCESS);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.ibtnOnBack:
                getActivity().onBackPressed();
                break;
        }
    }
}
