package com.nextsolutions.longhold.ui.dialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

class SelectCargoAdapter extends BaseAdapterV3 {
    public SelectCargoAdapter(int itemLayoutId) {
        super(itemLayoutId);
    }

    public SelectCargoAdapter(int layoutResId, @Nullable List data) {
        super(layoutResId, data);
    }

    public SelectCargoAdapter(int layoutResId, @Nullable List data, AdapterListener listener) {
        super(layoutResId, data, listener);
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
        super.convert(holder, o);

    }
}
