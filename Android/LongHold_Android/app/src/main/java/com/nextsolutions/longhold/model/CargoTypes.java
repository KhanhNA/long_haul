package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CargoTypes extends BaseModel {
    private Integer id;
    private Integer length;
    private Integer width;
    private Integer height;
    private String long_unit_moved0;
    private Integer weight_unit_moved0;
    private String type;
    private Integer from_weight;
    private Integer to_weight;
    private Integer price_id;
    private Integer price;
    private String size_standard_seq;
    private Integer long_unit;
    private Integer weight_unit;
    private Integer cargo_quantity;
    private Integer total_weight;

}
