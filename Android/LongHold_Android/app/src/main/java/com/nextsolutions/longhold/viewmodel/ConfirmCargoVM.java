package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;

public class ConfirmCargoVM extends BaseViewModel {

    private final MutableLiveData<OdooResultDto<BiddingVehicles>> mDriverCargo = new MutableLiveData<>();

    public MutableLiveData<OdooResultDto<BiddingVehicles>> getListDriverCargo() {
        return mDriverCargo;
    }

    public ConfirmCargoVM(@NonNull Application application) {
        super(application);
    }

    //get list driver cargo
    public void listBiddingVehicle(RunUi runUi) {
        DriverApi.listBiddingVehicle(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<BiddingVehicles> response = (OdooResultDto<BiddingVehicles>) o;
                mDriverCargo.setValue(response);
                runUi.run(Constants.GET_LIST_DRIVER_CARGO);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.GET_LIST_DRIVER_CARGO_ERROR);
            }
        });
    }

    // TODO: 8/14/2020 xác nhận
    public void addDriverCargo(String bidding_order_id, String bidding_vehicle_id, RunUi runUi) {
        DriverApi.addDriverCargo(bidding_order_id, bidding_vehicle_id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if ("true".equalsIgnoreCase(String.valueOf(o))) {
                    runUi.run(Constants.CONFIRM_SUCCESS);
                } else {
                    runUi.run(Constants.CONFIRM_ERROR);
                }
                Log.e("tungnt", "onSuccess: " + o);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.CONFIRM_ERROR);
                Log.e("tungnt", "errrrorrrr: ");
            }
        });
    }

    public void upDateDataListSelect(ArrayList<BiddingVehicles> mListSelect, ArrayList<BiddingVehicles> listIsSelected,RunUi runUi) {
        for (BiddingVehicles biddingVehicles : mListSelect) {
            biddingVehicles.setChecked(biddingVehicles.isSelected());
            if (biddingVehicles.isChecked()) {
                listIsSelected.add(biddingVehicles);
            } else {
                listIsSelected.remove(biddingVehicles);
            }
        }
        runUi.run(Constants.UPDATE_ADAPTER);
    }

    public void deleteCago(BiddingVehicles biddingVehicles, ArrayList<BiddingVehicles> listIsSelected,RunUi runUi) {
        listIsSelected.remove(biddingVehicles);
        biddingVehicles.setChecked(!biddingVehicles.isSelected());
        runUi.run(Constants.UPDATE_ADAPTER);
    }
}
