package com.nextsolutions.longhold.api;

import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.SOService;
import com.nextsolutions.longhold.model.ApiResponseModel;
import com.nextsolutions.longhold.model.ClassDriver;
import com.nextsolutions.longhold.model.Country;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.ModelVehicle;
import com.nextsolutions.longhold.model.RangeVehicle;
import com.nextsolutions.longhold.model.TonnageVehicle;
import com.nextsolutions.longhold.utils.StringUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverVehicleInfoApi extends BaseApi {
    public static final String GET_MODEL_VEHICLE = "/code_share/get_vehicle_model";
    public static final String GET_TONNAGE_VEHICLE = "/bidding/get_vehicle_tonnage";
    public static final String GET_VEHICLE_TYPE = "/bidding/get_vehicle_type";

    public static void addVehicle(LocalMedia imageVehicle, Integer model_id, String license_plate, String vin_sn, Integer tonnage_id, String vehicle_registration, String registration_date, String inspection_date, String inspection_due_date, Integer vehicle_type, String vehicle_inspection, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        String json = "{\"model_id\":" + model_id
                + ", \"license_plate\": \"" + license_plate + "\""
                + ", \"vin_sn\": \"" + vin_sn + "\""
                + ", \"vehicle_registration\": \"" + vehicle_registration + "\""
                + ", \"registration_date\": \"" + registration_date + "\""
                + ", \"inspection_date\": \"" + inspection_date + "\""
                + ", \"inspection_due_date\": \"" + inspection_due_date + "\""
                + ", \"vehicle_inspection\": \"" + vehicle_inspection + "\""
                + ",\"vehicle_type\":" + vehicle_type
                + ", \"tonnage_id\":" + tonnage_id + "}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

        MultipartBody.Part part = null;
        if (imageVehicle != null) {
            File file = new File(imageVehicle.getCompressPath());
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        }

        Call<ResponseBody> call = soService.addVehicle(mOdoo.getSessionCokie(), bodyJson, part);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void getModelVehicle(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_MODEL_VEHICLE, params, new SharingOdooResponse<OdooResultDto<ModelVehicle>>() {
                @Override
                public void onSuccess(OdooResultDto<ModelVehicle> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        result.onSuccess(obj.getRecords());
                    } else {
                        result.onSuccess(null);
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(new Throwable());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getCarManufacturer(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_VEHICLE_TYPE, params, new SharingOdooResponse<OdooResultDto<RangeVehicle>>() {
                @Override
                public void onSuccess(OdooResultDto<RangeVehicle> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        result.onSuccess(obj.getRecords());
                    } else {
                        result.onSuccess(null);
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(new Throwable());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getTonnageVehicle(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_TONNAGE_VEHICLE, params, new SharingOdooResponse<OdooResultDto<TonnageVehicle>>() {
                @Override
                public void onSuccess(OdooResultDto<TonnageVehicle> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        result.onSuccess(obj.getRecords());
                    } else {
                        result.onSuccess(null);

                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(new Throwable());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getCountries(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute("/company/get_country", params, new SharingOdooResponse<OdooResultDto<Country>>() {
                @Override
                public void onSuccess(OdooResultDto<Country> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        result.onSuccess(obj.getRecords());
                    } else {
                        result.onFail(new Throwable());

                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(new Throwable());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getDrivers(Integer country_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("country_id", country_id);
            mOdoo.callRoute("/code_share/get_driver_license", params, new SharingOdooResponse<OdooResultDto<ClassDriver>>() {
                @Override
                public void onSuccess(OdooResultDto<ClassDriver> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        result.onSuccess(obj.getRecords());
                    } else {
                        result.onFail(new Throwable());

                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(new Throwable());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveDriverInfo(Driver driver, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part[] parts;

        String json = "{\"phone\": \"" + driver.getPhone() + "\""
                + ", \"name\": \"" + driver.getName() + "\""
                + ", \"ssn\": \"" + driver.getSsn() + "\""
                + ", \"birth_date\": \"" + StringUtils.dateToString(driver.getBirth_date(), "yyyy-MM-dd") + "\""
                + ", \"address\": \"" + driver.getAddress() + "\""
                + ", \"expires_date\": \"" + driver.getExpires_date() + "\""
                + ", \"driver_license_date\": \"" + driver.getDriver_license_date() + "\""
                + ", \"no\": \"" + driver.getNo() + "\""
                + ", \"gender\": \"" + driver.getGender() + "\""
                + ", \"full_name\": \"" + driver.getFull_name() + "\""
                + ", \"image_1920\": \"" + "" + "\""
                + ", \"image_license_frontsite\": \"" + "" + "\""
                + ", \"image_license_backsite\": \"" + "" + "\""
                + ", \"country_id\":" + driver.getCountry_id()
                + ", \"class_driver\":" + driver.getClass_driver() + "}";

        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

        File fAvatar = new File(driver.getImage_1920());
        RequestBody fileRAvatar = RequestBody.create(MediaType.parse("image/*"), fAvatar);
        MultipartBody.Part partAvatar = MultipartBody.Part.createFormData("files", fAvatar.getName(), fileRAvatar);

        File fFront = new File(driver.getImage_license_frontsite());
        RequestBody fileRFront = RequestBody.create(MediaType.parse("image/*"), fFront);
        MultipartBody.Part partFront = MultipartBody.Part.createFormData("files", fFront.getName(), fileRFront);

        File fBack = new File(driver.getImage_license_backsite());
        RequestBody fileRBack = RequestBody.create(MediaType.parse("image/*"), fBack);
        MultipartBody.Part partBack = MultipartBody.Part.createFormData("files", fBack.getName(), fileRBack);

        parts = new MultipartBody.Part[3];
        parts[0] = partAvatar;
        parts[1] = partFront;
        parts[2] = partBack;

        Call<ApiResponseModel> call = soService.saveDriverInfo(mOdoo.getSessionCokie(), parts, bodyJson);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                if (response.errorBody() != null) {
                    try {
                        ApiResponseModel apiResponseModel = mGson.fromJson(response.errorBody().string(), ApiResponseModel.class);
                        result.onSuccess(apiResponseModel);
                    } catch (Exception e) {
                        result.onFail(e);
                        e.printStackTrace();
                    }
                } else {
                    result.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                result.onFail(t);
            }
        });
    }

    public static void updateInfoDriver(Driver driver, Boolean isChangeAvatar, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part[] parts;

        String json = "{\"phone\": \"" + driver.getPhone() + "\""
                + ", \"name\": \"" + driver.getName() + "\""
                + ", \"ssn\": \"" + driver.getSsn() + "\""
                + ", \"birth_date\": \"" + StringUtils.dateToString(driver.getBirth_date(), "yyyy-MM-dd") + "\""
                + ", \"address\": \"" + driver.getAddress() + "\""
                + ", \"expires_date\": \"" + StringUtils.dateToString(driver.getExpires_date(), "yyyy-MM-dd") + "\""
                + ", \"driver_license_date\": \"" + StringUtils.dateToString(driver.getDriver_license_date(), "yyyy-MM-dd") + "\""
                + ", \"no\": \"" + driver.getNo() + "\""
                + ", \"gender\": \"" + driver.getGender() + "\""
                + ", \"full_name\": \"" + driver.getFull_name() + "\""
                + ", \"status\": \"" + "running" + "\""
                + ", \"id\": \"" + driver.getId() + "\""
                + ", \"image_1920\": \"" + "" + "\""
                + ", \"image_license_frontsite\": \"" + "" + "\""
                + ", \"image_license_backsite\": \"" + "" + "\""
                + ", \"display_name\": \"" + driver.getName() + "\""
                + ", \"country_id\":" + driver.getCountry_id()
                + ", \"class_driver\":" + driver.getClass_driver() + "}";

        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

        MultipartBody.Part partAvatar = null;
        if (isChangeAvatar) {
            File fAvatar = new File(driver.getImage_1920());
            RequestBody fileRAvatar = RequestBody.create(MediaType.parse("image/*"), fAvatar);
            partAvatar = MultipartBody.Part.createFormData("files", fAvatar.getName(), fileRAvatar);
            parts = new MultipartBody.Part[3];

            parts[0] = partAvatar;
            parts[1] = null;
            parts[2] = null;
        } else {
            parts = null;
        }

//        File fFront = new File(driver.getImage_license_frontsite());
//        RequestBody fileRFront = RequestBody.create(MediaType.parse("image/*"), fFront);
//        MultipartBody.Part partFront = MultipartBody.Part.createFormData("files", fFront.getName(), fileRFront);
//
//        File fBack = new File(driver.getImage_license_backsite());
//        RequestBody fileRBack = RequestBody.create(MediaType.parse("image/*"), fBack);
//        MultipartBody.Part partBack = MultipartBody.Part.createFormData("files", fBack.getName(), fileRBack);



        Call<ResponseBody> call = soService.updateDriverInfo(mOdoo.getSessionCokie(), parts, bodyJson);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response == null) return;
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onFail(t);
            }
        });
    }
}
