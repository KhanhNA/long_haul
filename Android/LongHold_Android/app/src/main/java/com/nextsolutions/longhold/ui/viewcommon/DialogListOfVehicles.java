package com.nextsolutions.longhold.ui.viewcommon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.DialogListBiddingVehiclesBinding;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.enums.EnumBidding;

import java.util.List;

public class DialogListOfVehicles extends DialogFragment {
    private String title;
    private List<BiddingVehicles> mList;
    private DialogAdapterVehicles adapter;
    public View.OnClickListener onClickListener;
    private int mKeyPage = 0;

    public DialogListOfVehicles(String title, List<BiddingVehicles> mList, int keyPage, View.OnClickListener onClickListener) {
        this.title = title;
        this.mList = mList;
        this.onClickListener = onClickListener;
        this.mKeyPage = keyPage;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogListBiddingVehiclesBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_list_bidding_vehicles, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.custom_dialog_confirm_bidding));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.btnExdit.setText(mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION ? getString(R.string.edit_cargo) : getString(R.string.confirm));
        binding.textViewTitle.setText(title);
        adapter = new DialogAdapterVehicles(binding.getRoot().getContext(), mKeyPage, mList);
        binding.recyclerDialogVehicles.setAdapter(adapter);
        binding.recyclerDialogVehicles.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
        adapter.setAdapterBiddingVehicles(mList);

        binding.imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.btnExdit.setOnClickListener(onClickListener);

        return binding.getRoot();
    }
}
