package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Company extends BaseModel {
    private Integer id;
    private String name;
    private String tax_id;
    private String phone;
    private String address;

}
