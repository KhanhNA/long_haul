package com.nextsolutions.longhold.ui.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.FragmentEmployeeLocationBinding;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.ui.viewcommon.InfoWindowCustom;
import com.nextsolutions.longhold.utils.ImageUtils;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.EmployeeLocationVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.utils.LatLngUtils.getLatLngLocation;


public class EmployeeLocationFragment extends BaseFragment implements OnMapReadyCallback {

    EmployeeLocationVM mEmployeeLocationVM;
    FragmentEmployeeLocationBinding mBinding;
    private GoogleMap mMap;
    private static EmployeeLocationFragment biddingFragment;
    MarkerOptions mMarkerOptionsReceive;
    SupportMapFragment mapFragment;
    private VehicleCargo mVehicleCargo;

    public static EmployeeLocationFragment getInstance() {
        if (biddingFragment == null) {
            biddingFragment = new EmployeeLocationFragment();
        }
        return biddingFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_employee_location;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return EmployeeLocationVM.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mEmployeeLocationVM = (EmployeeLocationVM) viewModel;
        mBinding = (FragmentEmployeeLocationBinding) binding;
        getData();
        intiView();
        mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.googleMapLocation);
        mapFragment.getMapAsync(this);
        Drawable iconDrawableblue = getResources().getDrawable(R.drawable.ic_order_already_available_blue);
        BitmapDescriptor iconblue = ImageUtils.getMarkerIconFromDrawable(iconDrawableblue);
        Drawable iconDrawablered = getResources().getDrawable(R.drawable.ic_no_orders_yet_red);
        BitmapDescriptor iconred = ImageUtils.getMarkerIconFromDrawable(iconDrawableblue);

        LatLng end = getLatLngLocation(getContext(), "Cầu giấy, Hà nội");
        mMarkerOptionsReceive = new MarkerOptions().position(end).title("BKS: " + mVehicleCargo.getLisence_plate()).snippet("Tài xế: " + mVehicleCargo.getDriver_name() + "\nSĐT: " + mVehicleCargo.getDriver_phone_number()).icon(iconblue);
        return view;
    }

    private void getData() {
        mVehicleCargo = (VehicleCargo) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(StringUtils.TO_VEHICLE_ORDES_FRAGMEN);
        Log.d("lap", mVehicleCargo.getDriver_name());
    }

    private void intiView() {

    }

    // Thêm Marker (điểm) vào bản đồ.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new InfoWindowCustom(getContext()));
        List<Marker> markerList = new ArrayList<>();
        Marker marker = mMap.addMarker(mMarkerOptionsReceive);
        markerList.add(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(mMarkerOptionsReceive.getPosition()));
        marker.showInfoWindow();
        List<String> list = new ArrayList<>();
        list.add("BKS " + mVehicleCargo.getLisence_plate() + " " + mVehicleCargo.getDriver_name());
        String arr[] = list.toArray(new String[0]);

        // Spinner chọn Biển số xe
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.item_spinner, arr);
        arrayAdapter.setDropDownViewResource(R.layout.item_spinner);
        mBinding.spinnerBSX.setAdapter(arrayAdapter);
        mBinding.spinnerBSX.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String a = list.get(position);
                mBinding.txtLocation.setText("(SĐT: " + mVehicleCargo.getDriver_phone_number() + " )");
                markerList.get(0).showInfoWindow();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.onBackEL:
                getActivity().onBackPressed();
                break;

        }
    }


}
