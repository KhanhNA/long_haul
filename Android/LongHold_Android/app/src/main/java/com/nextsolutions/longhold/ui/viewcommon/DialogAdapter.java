package com.nextsolutions.longhold.ui.viewcommon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.model.BiddingPackageDetail.CargosPackage;
import com.nextsolutions.longhold.model.CargoTypes;

import java.util.List;

public class DialogAdapter extends RecyclerView.Adapter<DialogAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<CargoTypes> mCargos;
    public static ClickListener clickListener;
    private int cargoQuantity = 0;


    public DialogAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mCargo;
        private TextView mTotalCargo;
        private TextView mSize;
        private TextView mWeight;

        public ViewHolder(View itemView) {
            super(itemView);
            mCargo = (TextView) itemView.findViewById(R.id.textViewCargo);
            mTotalCargo = (TextView) itemView.findViewById(R.id.txtTotalCargo);
            mSize = (TextView) itemView.findViewById(R.id.txtSize);
            mWeight = (TextView) itemView.findViewById(R.id.txtWeight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(v, getAdapterPosition());
                }
            });

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_list_cargo, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CargoTypes currentData = mCargos.get(position);
        holder.mCargo.setText(currentData.getType());
        holder.mTotalCargo.setText(currentData.getCargo_quantity() + " cargo");
        holder.mSize.setText(currentData.getLength() + " x " + currentData.getHeight() + " x " + currentData.getWidth() + " m");
        holder.mWeight.setText(currentData.getTotal_weight() + " tấn");
    }

    public void setAdapterCargo(List<CargoTypes> list) {
        mCargos = list;
        notifyDataSetChanged();
    }

    public void setCargoQuantity(int cargoQuantity) {
        this.cargoQuantity = cargoQuantity;
    }

    @Override
    public int getItemCount() {
        return mCargos != null ? mCargos.size() : 0;
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        DialogAdapter.clickListener = clickListener;

    }

    public interface ClickListener {
        void onItemClick(View view, int position);
    }


}
