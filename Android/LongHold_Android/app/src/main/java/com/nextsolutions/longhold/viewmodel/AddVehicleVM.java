package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolutions.longhold.api.DriverVehicleInfoApi;
import com.nextsolutions.longhold.api.SharingOdooResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.ModelVehicle;
import com.nextsolutions.longhold.model.TonnageVehicle;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddVehicleVM extends BaseViewModel {
    LocalMedia lstFileSelected = new LocalMedia();
    List<BaseModel> listModelVehicle = new ObservableArrayList<>();
    List<BaseModel> listTonnageVehicle = new ObservableArrayList<>();
    List<BaseModel> listCarManufacturer = new ObservableArrayList<>();
    ObservableBoolean isEmptyImage = new ObservableBoolean(); // check đã thêm ảnh xe
    Integer model_id; // hãng xe
    ObservableField<String> vin_sn = new ObservableField<>(); // Số khung
    ObservableField<String> license_plate = new ObservableField<>(); // biển số xe
    ObservableField<String> url_image_vehicle = new ObservableField<>(); // ảnh xe
    ObservableField<String> createDate = new ObservableField<>(); // ngày đăng kí xe
    ObservableField<String> dateRegistration = new ObservableField<>(); // ngày đăng kiểm
    ObservableField<String> registrationEndDate = new ObservableField<>(); // ngày hết hạn đăng kiểm
    ObservableField<String> carManufacturer = new ObservableField<>(); // loại xe
    ObservableField<String> vehicleRegistration = new ObservableField<>(); // đăng kí xe
    ObservableField<String> vehicleInspection = new ObservableField<>(); // Mã đăng kí
    Integer tonnage_id; // trọng tải
    Integer vehicle_type; // id loại xe

    public AddVehicleVM(@NonNull Application application) {
        super(application);
        isEmptyImage.set(false);
    }

    public void getModelVehicle(RunUi runUi) {
        DriverVehicleInfoApi.getModelVehicle(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    listModelVehicle.addAll((Collection<? extends ModelVehicle>) o);
                } else {
                    runUi.run("CALL_API_FAIL");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getCarManufacturer(RunUi runUi) {
        DriverVehicleInfoApi.getCarManufacturer(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    listCarManufacturer.addAll((Collection<? extends ModelVehicle>) o);
                } else {
                    runUi.run("CALL_API_FAIL");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getTonnage(RunUi runUi) {
        DriverVehicleInfoApi.getTonnageVehicle(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    listTonnageVehicle.addAll((Collection<? extends TonnageVehicle>) o);
                } else {
                    runUi.run("CALL_API_FAIL");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void addVehicle(RunUi runUi) {
        DriverVehicleInfoApi.addVehicle(lstFileSelected,
                model_id,
                license_plate.get(),
                vin_sn.get(),
                tonnage_id,
                vehicleRegistration.get(),
                createDate.get(),
                dateRegistration.get(),
                registrationEndDate.get(),
                vehicle_type,vehicleInspection.get(),
                new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    runUi.run("ADD_VEHICLE_SUCCESS");
                    return;
                }
                runUi.run("ADD_VEHICLE_FAIL");

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
