package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;

import com.nextsolutions.longhold.api.ConfirmCargoApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.Vehicle;
import com.nextsolutions.longhold.model.VehicleDriver;
import com.nextsolutions.longhold.model.VehicleDriverId;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmCargoVMV2 extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableField<BiddingPackage> objBiddingPackage = new ObservableField<>();
    private ObservableList<VehicleDriver> vehicleDrivers = new ObservableArrayList<>();
    private List<VehicleDriverId> vehicleDriverIds;//list id của vehicle - driver
    private HashMap<Long, Driver> selectedDrivers;//lưu lại những driver đã được chọn
    private HashMap<Long, Vehicle> selectedVehicles;//lưu lại những vehicle đã được chọn

    public ConfirmCargoVMV2(@NonNull Application application) {
        super(application);
        vehicleDriverIds = new ArrayList<>();
        selectedDrivers = new HashMap<>();
        selectedVehicles = new HashMap<>();
        objBiddingPackage.set(new BiddingPackage());
    }


    public void confirmVehicleDriverInfo(RunUi runUi) {
        isLoading.set(true);
        ConfirmCargoApi.confirmVehicleDriverInfo(objBiddingPackage.get().getBidding_order_id() + "", vehicleDriverIds, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if ("true".equalsIgnoreCase(String.valueOf(o))) {
                    runUi.run(Constants.CONFIRM_SUCCESS);
                } else {
                    runUi.run(Constants.CONFIRM_ERROR);
                }
                Log.e("haint", "onSuccess: " + o);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run(Constants.CONFIRM_ERROR);
                Log.e("haint", "errrrorrrr: ");
            }
        });
    }
}
