package com.nextsolutions.longhold.model;

public class VehicleDriverId {
    private Long vehicle_id;
    private Long driver_id;

    public VehicleDriverId(Long vehicle_id, Long driver_id) {
        this.vehicle_id = vehicle_id;
        this.driver_id = driver_id;
    }

    public Long getVehicle_id() {
        return vehicle_id;
    }

    public VehicleDriverId setVehicle_id(Long vehicle_id) {
        this.vehicle_id = vehicle_id;
        return this;
    }

    public Long getDriver_id() {
        return driver_id;
    }

    public VehicleDriverId setDriver_id(Long driver_id) {
        this.driver_id = driver_id;
        return this;
    }
}
