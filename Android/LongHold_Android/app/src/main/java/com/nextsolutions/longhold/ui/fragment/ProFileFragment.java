package com.nextsolutions.longhold.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.ProFileFragmentBinding;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.ProfileVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Objects;

import static com.nextsolutions.longhold.base.Constants.GALLERY_PICTURE;

public class ProFileFragment extends BaseFragment {

    private boolean isCheckEdit;
    private ProFileFragmentBinding mProFileFragmentBinding;
    private ProfileVM mProfileVM;
    private String selectedImagePath;
    private Bitmap bitmap;
    private String result;

    @Override
    public int getLayoutRes() {
        return R.layout.pro_file_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProfileVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mProFileFragmentBinding = (ProFileFragmentBinding) binding;
        mProfileVM = (ProfileVM) viewModel;
        configView();
        AppController.setDigits(mProFileFragmentBinding.tvUser);
        return view;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnEdit:
                isCheckEdit = !isCheckEdit;
                configView();
                break;
            case R.id.imgAvatar:
                selectImage();
                break;
            case R.id.btnConfirm:
                String name = mProFileFragmentBinding.tvUser.getText().toString().trim();
                String phone = mProFileFragmentBinding.tvPhone.getText().toString().trim();
                if (isValidateInput(name, phone))
                    mProfileVM.confirmUpdateInformation(name, phone, result, this::runUi);
                break;
            case R.id.btnBack:
                getActivity().finish();
                break;
            case R.id.viewGroupRate:
                Intent intentSt = new Intent(getActivity(), CommonActivity.class);
                intentSt.putExtra(Constants.FRAGMENT, RateFragment.class);
                startActivity(intentSt);

                break;
        }
    }

    private boolean isValidateInput(String name, String phone) {

        if (name.isEmpty()) {
            ToastUtils.showToast(getString(R.string.validate_input));
            return false;
        }
        if (phone.length() != 10 && phone.length() != 11) {
            ToastUtils.showToast(getString(R.string.validate_phone));
            return false;
        }
        return true;

    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        switch (s) {
            case Constants.UPDATE_UI:
                isValidateResult();
                break;
        }
    }

    private void isValidateResult() {
        switch (Objects.requireNonNull(mProfileVM.getResultUpdate().getValue())) {
            case Constants.ERROR_PHONE:
                ToastUtils.showToast(getString(R.string.error_phone));
                break;
            case Constants.UPDATE_SUCCESS:
                ToastUtils.showToast(getString(R.string.update_success_profile));
                isCheckEdit = !isCheckEdit;
                configView();
                break;
            case Constants.ERROR_ACCOUNT:
                ToastUtils.showToast(getString(R.string.account_already));
                break;
        }

    }

    private void configView() {
        mProFileFragmentBinding.tvAddress.setEnabled(isCheckEdit);
        mProFileFragmentBinding.tvPhone.setEnabled(isCheckEdit);
        mProFileFragmentBinding.tvUser.setEnabled(isCheckEdit);

        if (isCheckEdit) mProFileFragmentBinding.btnEdit.setText(getString(R.string.cancel));
        else mProFileFragmentBinding.btnEdit.setText("Sửa");

        mProFileFragmentBinding.btnConfirm.setVisibility(isCheckEdit ? View.VISIBLE : View.INVISIBLE);
        mProFileFragmentBinding.viewGroupRate.setVisibility(isCheckEdit ? View.GONE : View.VISIBLE);
    }

    private void selectImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            openGallery();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_ASK_READ_EXTERNAL_PERMISSIONS);
                return;
            }
            openGallery();
        }
    }

    private void openGallery() {
        Intent pictureActionIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pictureActionIntent, Constants.GALLERY_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_PICTURE) {
            result = data.getStringExtra(Constants.EXTRA_DATA);
            showGalary(data);
        }
    }

    private void showGalary(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            try {
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                selectedImagePath = c.getString(columnIndex);
                c.close();
            } catch (Exception e) {
                ToastUtils.showToast(getString(R.string.error_handling));
                return;
            }

            final BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, option);

            //  Calculate inSampleSize
            option.inSampleSize = calculateInSampleSize(option, 500, 500);
            option.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(selectedImagePath); // load
            // preview image
            if (bitmap != null) {
                bitmap960();
                Glide.with(getContext()).load(selectedImagePath).into(mProFileFragmentBinding.imgAvatar);
            } else {
                ToastUtils.showToast(getString(R.string.error_handling));
            }
        }
    }

    private int calculateInSampleSize(BitmapFactory.Options option, int reqHeight, int reqWidth) {
        // Raw height and width of image
        final int height = option.outHeight;
        final int width = option.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private void bitmap960() {
        final int maxSize = 960;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if (inWidth > inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, true);
    }
}
