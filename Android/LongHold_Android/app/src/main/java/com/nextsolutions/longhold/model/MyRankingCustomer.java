package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRankingCustomer extends BaseModel {
    Long id;
    Integer point=0;
    Integer total_point;
    List<RankingDTO> list_rank;
}
