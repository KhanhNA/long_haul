package com.nextsolutions.longhold.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.FragmentAccountBinding;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.ui.activity.LoginActivityV2;
import com.nextsolutions.longhold.viewmodel.AccountBiddingVM;
import com.nextsolutions.socket_notification.SocketIONavigation;
import com.nextsolutions.socket_notification.SocketIONavigationProvider;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class AccountFragment extends BaseFragment {
    private AccountBiddingVM accountBiddingVM;
    private FragmentAccountBinding accountFragmentBinding;
    private DialogConfirm dialogLogout;
    private DialogConfirm dialogHelpCenter;
    protected SocketIONavigation mSocketIONavigation;
    private String channel = "bidmessage";
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mSocketIONavigation = ((SocketIONavigationProvider) getActivity()).socketNavigation();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        accountBiddingVM = (AccountBiddingVM) viewModel;
        accountFragmentBinding = (FragmentAccountBinding) binding;
        accountBiddingVM.getListInformation().setValue(AppController.getInformationUser());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_account;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AccountBiddingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnLogout:
                dialogLogout = new DialogConfirm(getString(R.string.dialog_confirm_logout), null, getString(R.string.title_logout), v -> {
                    intentLogout();
                    mSocketIONavigation.disconnect(channel);
                    dialogLogout.dismiss();
                });
                if (getFragmentManager() != null) {
                    dialogLogout.show(getFragmentManager(), "tag");
                }
                break;
            case R.id.btnHelpCenter:
                dialogHelpCenter = new DialogConfirm(getString(R.string.dialog_confirm_help_center), null, getString(R.string.title_agree), v -> dialogHelpCenter.dismiss());
                dialogHelpCenter.setHideCancelButton(true);
                if (getFragmentManager() != null) {
                    dialogHelpCenter.show(getFragmentManager(), "tag");
                }
                break;
            case R.id.btnHistory:
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, HistoryBiddingFragment.class);
                startActivity(intent);
                break;
            case R.id.btnSetting:
                Intent intentSt = new Intent(getActivity(), CommonActivity.class);
                intentSt.putExtra(Constants.FRAGMENT, SettingsFragment.class);
                startActivity(intentSt);
                break;
            case R.id.vProfile:
                Intent intentProfile = new Intent(getActivity(), CommonActivity.class);
                intentProfile.putExtra(Constants.FRAGMENT, ProFileFragment.class);
                startActivity(intentProfile);
                break;
            case R.id.btnRewardPoints:
                Intent intentRewardPoints = new Intent(getActivity(), CommonActivity.class);
                intentRewardPoints.putExtra(Constants.FRAGMENT, MyRankingFragment.class);
                startActivity(intentRewardPoints);
                break;
        }
    }

    private void intentLogout() {
        accountBiddingVM.clearPassword();
        Intent intent = new Intent(getActivity(), LoginActivityV2.class);
        startActivity(intent);
        getActivity().finish();

    }

}