package com.nextsolutions.longhold.model;

import com.nextsolutions.longhold.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingOrderDetail extends BaseModel {
    private Integer id;
    private Integer bidding_order_id;
    private String status;
    private String bidding_package_number;
    private String confirm_time;
    private String release_time;
    private String bidding_time;
    private Integer max_count;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private String total_weight;
    private String distance;
    private Double from_latitude;
    private Double from_longitude;
    private Double to_latitude;
    private Double to_longitude;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private String create_date;
    private Integer total_cargo;
    private Double price;
    private BiddingOrder bidding_order;

    private List<Cargos> cargos;
    private List<BiddingVehicles> bidding_vehicles;
    private List<CargoTypes> cargo_types;

    public String getPrice_str(){
        return this.price != null ? AppController.getInstance().formatCurrency(price) : "";
    }

    @Getter
    @Setter
    public class BiddingOrder extends BaseModel {
        private Integer id;

        private Integer company_id;

        private String bidding_order_number;

        private FromDepot from_depot;

        private ToDepot to_depot;

        private Integer total_weight;

        private Integer total_cargo;

        private Integer price;

        private Integer distance;

        private String type;

        private String status;

        private String note;

        private String bidding_order_receive_id;

        private String bidding_order_return_id;

        private String create_date;

        private String write_date;

        private Integer bidding_package_id;

        private String from_receive_time;

        private String to_receive_time;

        private String from_return_time;

        private String to_return_time;

        private String max_confirm_time;

        private List<BiddingVehicles> bidding_vehicles;

        private List<CargoTypes> cargo_types;
    }
}


