package com.nextsolutions.longhold.model;

import com.nextsolutions.longhold.utils.StringUtils;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillPackage extends BaseModel implements Cloneable{
    private Integer id;
    private Integer quantity_import;
    private Double length;
    private Double width;
    private Double height;
    private Double total_weight;
    private Double capacity;
    private String item_name;
    private OdooRelType product_type_id;
    private String note;

    //nếu nằm trong kho xuất.
    private  Integer quantity_export;
    private transient String warehouse_name;
    private transient String address;
    private transient String phone;
    private transient Float latitude;
    private transient Float longitude;

    //thuộc tính dùng cho lúc cập nhật trạng thái
    private boolean isEdited;


    public String getType(){
        if(product_type_id != null && product_type_id.size() >= 2){
            return product_type_id.get(1) + "";
        }
        return "";
    }


    public String getLengthStr() {
        return this.getLength() != null ? this.getLength().toString() : "";
    }

    public void setLengthStr(String lengthStr) {
        if (!StringUtils.isNullOrEmpty(lengthStr)) {
            if (lengthStr.charAt(0) == '.') {
                lengthStr = lengthStr.replace(".", "0.");
            }
            this.setLength(Double.parseDouble(lengthStr));
        }else {
            this.setLength(0d);
        }
    }

    public String getWidthStr() {
        return this.getWidth() != null ? this.getWidth().toString() : "";
    }

    public void setWidthStr(String widthStr) {
        if (!StringUtils.isNullOrEmpty(widthStr)) {
            if (widthStr.charAt(0) == '.') {
                widthStr = widthStr.replace(".", "0.");
            }
            this.setWidth(Double.parseDouble(widthStr));
        }else {
            this.setWidth(0d);
        }
    }

    public String getHeightStr() {
        return this.getHeight() != null ? this.getHeight().toString() : "";
    }

    public void setHeightStr(String heightStr) {
        if (!StringUtils.isNullOrEmpty(heightStr)) {
            if (heightStr.charAt(0) == '.') {
                heightStr = heightStr.replace(".", "0.");
            }
            this.setHeight(Double.parseDouble(heightStr));
        }else {
            this.setHeight(0d);
        }
    }

    public String getWeightStr() {
        return this.total_weight != null ? this.total_weight.toString() : "";
    }

    public void setWeightStr(String weightStr) {
        if (!StringUtils.isNullOrEmpty(weightStr)) {
            if (weightStr.charAt(0) == '.') {
                weightStr = weightStr.replace(".", "0.");
            }
            this.total_weight = (Double.parseDouble(weightStr));
        } else {
            this.total_weight = (0d);
        }
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
            // This should never happen
        }
    }

}
