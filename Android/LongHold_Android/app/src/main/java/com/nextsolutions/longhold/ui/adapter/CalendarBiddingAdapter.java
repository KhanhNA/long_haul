package com.nextsolutions.longhold.ui.adapter;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.database.CalendarEvent;
import com.nextsolutions.longhold.database.RealmCalendarEvent;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.tsolution.base.listener.AdapterListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CalendarBiddingAdapter extends BaseAdapterV3 {

    private boolean ishCheckStatusPackage;

    public CalendarBiddingAdapter(int layoutResId, @Nullable List data, boolean isPackageStatus, AdapterListener listener) {
        super(layoutResId, data, listener);
        this.ishCheckStatusPackage = isPackageStatus;
    }

    @SuppressLint("NewApi")
    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
        super.convert(holder, o);
        // = true -> Bidding
        // = false -> remind
        if (ishCheckStatusPackage)
            ((TextView) holder.getView(R.id.btnBidding)).setText(R.string.bidding);
        else ((TextView) holder.getView(R.id.btnBidding)).setText(R.string.remind);
        ((TextView) holder.getView(R.id.btnBidding)).setTextColor(AppController.getInstance().getColor(R.color.colorApp));
        ((TextView) holder.getView(R.id.btnBidding)).setBackgroundResource(R.drawable.bg_boder_button);

        // = true -> danh sách dang diễn ra đấu thấu lên không có trạng thái hủy nhắc nhở
        if (ishCheckStatusPackage) return;
        List<CalendarEvent> calendarEventList = RealmCalendarEvent.getInstance(AppController.getInstance().getBaseContext()).readCalendar();
        for (int i = 0; i < calendarEventList.size(); i++) {
            Log.e("duongnk", "convert: " + calendarEventList.get(i));
        }
        if (o instanceof BiddingPackage) {
            BiddingPackage biddingPackage = (BiddingPackage) o;
            for (int i = 0; i < calendarEventList.size(); i++) {
                Log.e("duongnk", "convert: " + calendarEventList.get(i).getMIdEvent() + "?" + biddingPackage.getId());
                if (calendarEventList.get(i).getMIdEvent().equals(biddingPackage.getId() + "")) {
                    ((TextView) holder.getView(R.id.btnBidding)).setText(R.string.close_remind);
                    ((TextView) holder.getView(R.id.btnBidding)).setTextColor(AppController.getInstance().getColor(R.color.item_off));
                    ((TextView) holder.getView(R.id.btnBidding)).setBackgroundResource(R.drawable.backgroud_item_off);
                }
            }
        }
    }
}
