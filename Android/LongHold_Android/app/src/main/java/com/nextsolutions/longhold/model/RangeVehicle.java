package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RangeVehicle extends BaseModel {
    Integer id;
    String name;
    String code;
}
