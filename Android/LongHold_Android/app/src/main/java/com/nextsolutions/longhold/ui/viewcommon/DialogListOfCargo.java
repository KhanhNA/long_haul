package com.nextsolutions.longhold.ui.viewcommon;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.nextsolutions.longhold.model.BiddingPackageDetail.CargosPackage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.DialogListOfCargoTypesBinding;
import com.nextsolutions.longhold.model.CargoTypes;

import java.util.List;


public class DialogListOfCargo extends DialogFragment {
    private String title;
    private List<CargoTypes> mList;
    public View.OnClickListener onClickListener;
    public DialogAdapter adapter;
    private int cargoQuantity = 0;

    public DialogListOfCargo(String title, List<CargoTypes> mList, int cargoQuantity) {
        this.title = title;
        this.mList = mList;
        this.cargoQuantity = cargoQuantity;
    }

    public DialogListOfCargo(String title, List<CargoTypes> list, View.OnClickListener onClickListener) {
        this.title = title;
        this.mList = list;
        this.onClickListener = onClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogListOfCargoTypesBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_list_of_cargo_types, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.custom_dialog_confirm_bidding));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.textViewTitle.setText(title);
        adapter = new DialogAdapter(binding.getRoot().getContext());
        binding.recyclerDialogCargo.setAdapter(adapter);
        binding.recyclerDialogCargo.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
        adapter.setAdapterCargo(mList);
        adapter.setCargoQuantity(cargoQuantity);
        adapter.setOnItemClickListener(new DialogAdapter.ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //ToastUtils.showToast(mList.get(position).getCargo());
            }
        });

        binding.imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
//        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
//        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
//        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        Window window = getDialog().getWindow();
        Point size = new Point();

       if (window == null)
           return;
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;
        int height = size.y;

        window.setLayout((int) (width * 0.9), (int) (height * 0.8));
        window.setGravity(Gravity.CENTER);
    }
}
