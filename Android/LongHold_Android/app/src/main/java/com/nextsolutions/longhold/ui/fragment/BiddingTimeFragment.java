package com.nextsolutions.longhold.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.HaiSer;
import com.nextsolutions.longhold.databinding.BiddingFragmentPackageBinding;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.model.BiddingTimePackage;
import com.nextsolutions.longhold.model.SocketModel;
import com.nextsolutions.longhold.ui.activity.MainActivity;
import com.nextsolutions.longhold.ui.adapter.TabAdapter;
import com.nextsolutions.longhold.ui.viewcommon.PagerAdapter;
import com.nextsolutions.longhold.viewmodel.SocketResult;
import com.nextsolutions.longhold.viewmodel.TimePackageBiddingVM;
import com.nextsolutions.socket_notification.SocketIO;
import com.nextsolutions.socket_notification.SocketIONavigation;
import com.nextsolutions.socket_notification.SocketIONavigationProvider;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import static com.nextsolutions.longhold.base.AppController.channel_SOCKET;
import static com.nextsolutions.longhold.base.Constants.STATUS_NOTIFICATION;

public class BiddingTimeFragment extends BaseFragment implements SocketIO.onMessageSocket {
    private BiddingFragmentPackageBinding biddingFragmentPackageBinding;
    private TimePackageBiddingVM mTimePackageBiddingVM;
    private TabAdapter timePackageBiddingAdapter;
    private ArrayList<BiddingTimePackage> arrayList = new ArrayList<>();
    private int indexClick;
    private PagerAdapter mPageAdapter;
    private int mOffset;
    private boolean isHideNotification;
    private SwitchNotificationHomeFragment listener;
    SharedPreferences preferences = AppController.getInstance().getSharePre();
    protected SocketIONavigation mSocketIONavigation;
    private OdooResultDto<SocketModel> mBiddingPackageSocket = new OdooResultDto<>();
    private List<SocketModel> socketModelList = new ArrayList<>();
    private static BiddingTimeFragment mCalendarBiddingFragment;
    private MainActivity mainActivity;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mSocketIONavigation = ((SocketIONavigationProvider) getActivity()).socketNavigation();
        listener = (SwitchNotificationHomeFragment) context;
        mainActivity = (MainActivity) context;
    }

    public static BiddingTimeFragment getInstance() {
        if (mCalendarBiddingFragment == null) {
            mCalendarBiddingFragment = new BiddingTimeFragment();
        }
        return mCalendarBiddingFragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bidding_fragment_package;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return TimePackageBiddingVM.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mTimePackageBiddingVM = (TimePackageBiddingVM) viewModel;
        biddingFragmentPackageBinding = (BiddingFragmentPackageBinding) binding;
        mBiddingPackageSocket.setRecords(socketModelList);
        mSocketIONavigation.connect(AppController.URI_SOCKET, channel_SOCKET, this::messageSocket);
        initView();
        eventHideNotification();
        getData();
        return view;


    }


    private void initViewPage() {
        mPageAdapter = new PagerAdapter(getChildFragmentManager());
        biddingFragmentPackageBinding.viewPagerTimePackage.setAdapter(mPageAdapter);
        biddingFragmentPackageBinding.viewPagerTimePackage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTimePackageBiddingVM.setSelectItemView(position, BiddingTimeFragment.this::runUi);
                biddingFragmentPackageBinding.recyclerViewTabLayout.scrollToPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initView() {
        biddingFragmentPackageBinding.recyclerViewTabLayout.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        timePackageBiddingAdapter = new TabAdapter(R.layout.item_bidding_child, mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords(), this);
        biddingFragmentPackageBinding.recyclerViewTabLayout.setAdapter(timePackageBiddingAdapter);
        timePackageBiddingAdapter.notifyDataSetChanged();
        biddingFragmentPackageBinding.switchNotification.setChecked(preferences.getBoolean(STATUS_NOTIFICATION, true));
        configSocket(preferences.getBoolean(STATUS_NOTIFICATION, true));
    }

    private void getData() {
        mTimePackageBiddingVM.requestBiddingPackageTime(this::runUi);
    }

    private void statusNotification() {
        isHideNotification = !isHideNotification;
        biddingFragmentPackageBinding.lnNotification.setVisibility(isHideNotification ? View.VISIBLE : View.GONE);
        biddingFragmentPackageBinding.btnHideNotifi.setRotation(isHideNotification ? 180 : 360);
    }

    private void refreshData() {
        mTimePackageBiddingVM.clearData();
        getData();
    }

    private void eventHideNotification() {
        biddingFragmentPackageBinding.switchNotification.setOnCheckedChangeListener((buttonView, isChecked) -> {
            preferences.edit().putBoolean(STATUS_NOTIFICATION, isChecked).apply();
//            // on , off socket
            configSocket(isChecked);
            listener.onInputSent();
        });
    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        if (s.equals(Constants.SUCCESS_API)) {
            Log.e("reload fragment", "SUCCESS_API ");
            initViewPage();
            // udapte list time
            timePackageBiddingAdapter.setUpDateDat(mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords());
            int length = mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords().size();
            for (int i = 0; i < length; i++) {
                // time của mỗi khung giờ đc chuyền qua fragment child để sử dụng cho chức năng count dow time
                String timePackage = AppController.formatDateToString(mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords().get(i).getCalendar(), "", "UTC");
                mPageAdapter.addFragment(createFragment(timePackage, i, mTimePackageBiddingVM.getMilliseconds()));
                Log.e("reload fragment", "runUi: " + i + " ------ String date :  " + timePackage);
            }
            mPageAdapter.notifyDataSetChanged();
            biddingFragmentPackageBinding.viewPagerTimePackage.setOffscreenPageLimit(length);
            emptyData(mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords());
        }
        if (s.equals(Constants.FAIL_API)) {
            emptyData(mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords());
        }
        // update lại adapter thời gian đấu thầu khi scroll viewPage
        if (s.equals(Constants.UPDATE_ADAPTER)) {
            timePackageBiddingAdapter.setUpDateDat(mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords());
        }

    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            biddingFragmentPackageBinding.emptyData.setVisibility(View.GONE);
        } else {
            biddingFragmentPackageBinding.emptyData.setVisibility(View.VISIBLE);
        }
    }

    private void configSocket(boolean isChecked) {
        // on , off socket
        if (isChecked) {
            mSocketIONavigation.connect(AppController.URI_SOCKET, channel_SOCKET, this);
        } else {
            mSocketIONavigation.disconnect(channel_SOCKET);
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        BiddingTimePackage biddingTimePackage = (BiddingTimePackage) o;
        switch (id) {
            case R.id.viewGroup:
                indexClick = mTimePackageBiddingVM.getCalendarBidding().getValue().getRecords().indexOf(biddingTimePackage);
                biddingFragmentPackageBinding.viewPagerTimePackage.setCurrentItem(indexClick);
                break;
        }
    }


    @Override
    public int getRecycleResId() {
        return 0;
    }


    private Fragment createFragment(String date, int position, ArrayList<Long> listTimePacage) {
        Fragment fragment;
        if (position == 0) {
            fragment = new BiddingTimeLiveFragment();
        } else {
            fragment = new BiddingTimeRemindFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_PAGE, date);
        bundle.putInt(Constants.POSITION, position);
        bundle.putSerializable(Constants.DATA_PASS_FRAGMENT, listTimePacage);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnHideNotifi:
                statusNotification();
        }
    }

    private SocketResult socketResult;


    @Override
    public void messageSocket(Object... args) {
        socketModelList.clear();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
                .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        Log.e("duongnk", "messageSocket:BiddingTimeFragment " + args[0]);
        for (int j = 0; j < args.length; j++) {
            JSONObject data = (JSONObject) args[j];
            Log.d("duongnk", "call: " + data.toString());
            socketResult = gson.fromJson(data.toString(), SocketResult.class);
            socketModelList.addAll(socketResult.getResult().getRecords());
        }

        BiddingFragment.getInstance().updateSocketData(args);
        BiddingTimeLiveFragment.getInstance().updateSocketData(args);
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBiddingPackageSocket.setRecords(socketModelList);
                for (int i = 0; i < mBiddingPackageSocket.getRecords().size(); i++) {
                    String actionType = mBiddingPackageSocket.getRecords().get(i).getActionType();
                    switch (actionType) {
                        case EnumBidding.EnumSocketType.BiddingTime:
                            // Reload lại màn hình new home vc
                            refreshData();
                            Log.e("duongnk", "home vc: refreshData");
                            break;
                        case EnumBidding.EnumSocketType.BiddingPackage:
                            // xoá object (nếu có) ở danh sách Đang đấu thầu (new home vc)
                            // reload các tab ở danh sách Đã đấu thầu (home vc)
                            break;
                        case EnumBidding.EnumSocketType.ChangePriceAction:
                            // Nếu status = 0 => update data của object tương ứng (new home vc)
                            // Nếu status = -1 => remove object đó khỏi danh sách đấu thầu (new home vc)
                            break;
                        case EnumBidding.EnumSocketType.OvertimeBiddingAction:
                            // thêm object vào danh sách Đang đấu thầu (new home vc)
                            // xoá object (nếu có) ở danh sách Đã đấu thầu (home vc)
                            break;
                    }
                }
            }
        });
    }

    public interface SwitchNotificationHomeFragment {
        void onInputSent();
    }
}
