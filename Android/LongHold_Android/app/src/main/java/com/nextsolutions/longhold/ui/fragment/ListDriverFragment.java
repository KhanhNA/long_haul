package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.ListDriverFragmentBinding;
import com.nextsolutions.longhold.enums.DriverStatus;
import com.nextsolutions.longhold.enums.VehicleStatus;
import com.nextsolutions.longhold.fragmentnavigation.FragmentNavigationImp;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.ui.dialog.DriverInfoDialog;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.ListDriverVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

public class ListDriverFragment extends BaseFragment<ListDriverFragmentBinding> {
    private static final int REQUEST_ADD_DRIVER = 999;
    ListDriverVM listDriverVM;
    private BaseAdapterV3 baseAdapterV3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        listDriverVM = (ListDriverVM) viewModel;

        initView();
        initLoadMore();

        return view;
    }

    private void initLoadMore() {
        baseAdapterV3.getLoadMoreModule().setOnLoadMoreListener(() -> {
            listDriverVM.getMoreDriver(this::runUi);
        });
        baseAdapterV3.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        baseAdapterV3.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initView() {
        setUpRecycleView();
        listDriverVM.getDrivers(false, this::runUi);
        binding.swRefresh.setOnRefreshListener(() -> {
            listDriverVM.getDrivers(false, this::runUi);
        });

        binding.btnDriverStatus.setOnClickListener(v -> openDialogSelectDriverStatus(v));
    }

    @SuppressLint("RestrictedApi")
    private void openDialogSelectDriverStatus(View view) {
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_vehicle_status, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mn_all:
                    listDriverVM.getApproved_check().set(DriverStatus.ALL);
                    binding.btnDriverStatus.setText(getString(R.string.ALL));
                    break;
                case R.id.mn_waiting:
                    listDriverVM.getApproved_check().set(DriverStatus.WAITING);
                    binding.btnDriverStatus.setText(getString(R.string.waiting));
                    break;
                case R.id.mn_Accepted:
                    listDriverVM.getApproved_check().set(DriverStatus.ACCEPTED);
                    binding.btnDriverStatus.setText(getString(R.string.accepted));
                    break;
                case R.id.mn_rejected:
                    listDriverVM.getApproved_check().set(DriverStatus.REJECTED);
                    binding.btnDriverStatus.setText(getString(R.string.rejected));
                    break;
            }

            listDriverVM.getDrivers(false, this::runUi);
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];

        switch (action) {
            case "getDriverSuccess":
                baseAdapterV3.notifyDataSetChanged();
                break;
            case "getDriverFail":
                Log.i("ListDriver", "runUi: ...getDataFail");
                break;
            case Constants.NO_MORE_DATA:
                baseAdapterV3.getLoadMoreModule().loadMoreEnd();
                break;
            default:
                break;
        }
    }

    public void gotoAddDriverFragment() {
        Intent intentSt = new Intent(getActivity(), CommonActivity.class);
        intentSt.putExtra(Constants.FRAGMENT, DriverInfoFragment.class);
        startActivity(intentSt);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_DRIVER && resultCode == Constants.RESULT_OK) {
            listDriverVM.getDrivers(false, this::runUi);
        }
    }

    private void setUpRecycleView() {
        baseAdapterV3 = new BaseAdapterV3(R.layout.layout_item_driver, listDriverVM.getListDriver(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                Driver driver = (Driver) o;

                if (view.getId() == R.id.itemDriver) {
                    DriverInfoDialog driverInfoDialog = new DriverInfoDialog(driver, getBaseActivity());

                    driverInfoDialog.show(getChildFragmentManager(), "DialogVehicleInfo");
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        binding.rcContent.setAdapter(baseAdapterV3);
        binding.rcContent.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_driver_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListDriverVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }

    public void searchInfo(String textSearch) {
        if (textSearch == null) {
            textSearch = "";
        }
        try {
            listDriverVM.getTxtSearch().set(textSearch);
            listDriverVM.getDrivers(false, this::runUi);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
