package com.nextsolutions.socket_notification;

import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class SocketIO implements SocketIONavigation {
    private Socket mSocket;

    public SocketIO() {
    }

    public void connect(String uri, String channel, onMessageSocket onMessageSocket) {
        {
            try {
                mSocket = IO.socket(uri);
            } catch (URISyntaxException e) {
                Log.e("duongnk", "connect: "+e.toString() );
            }
        }
        mSocket.on(channel, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("duongnk", "on: SocketIO");
                onMessageSocket.messageSocket(args);
            }
        });
        Log.e("duongnk", "connect: SocketIO");
        // connect
        mSocket.connect();
    }

    public void disconnect(String channel) {
        Log.e("duongn", "disconnect: ");
        if (mSocket != null)
            mSocket.off(channel);
    }

    public interface onMessageSocket {
        void messageSocket(Object... args);
    }
}
