package com.nextsolutions.longhold.api;

import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.model.HistoryRatingPointDTO;
import com.ns.odoolib_retrofit.model.OdooResultDto;

import org.json.JSONObject;
import org.xml.sax.ErrorHandler;

public class CustomerAPI extends BaseApi {
    public static String GET_HISTORY_RATING_POINT="/sharevan/get_history_rating_point";
    public static void getHistoryRatingPoint(Integer page, IResponse result){
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("page",page);
            params.put("limit",10);

            mOdoo.callRoute(GET_HISTORY_RATING_POINT, params, new SharingOdooResponse<OdooResultDto<HistoryRatingPointDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<HistoryRatingPointDTO> resultDto) {
                    result.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onSuccess(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
