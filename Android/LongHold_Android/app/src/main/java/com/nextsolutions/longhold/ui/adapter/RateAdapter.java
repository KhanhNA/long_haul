package com.nextsolutions.longhold.ui.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.model.RateModel;
import com.nextsolutions.longhold.ui.fragment.LoadMoreImgViewFragment;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.tsolution.base.CommonActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;

public class RateAdapter extends BaseAdapterV3 {
    ArrayList<RateModel.BiddingVehicle> arrayList = new ArrayList<>();
    Boolean isCheckShowAll;

    public RateAdapter(int layoutResId, @Nullable List data, boolean checkShowAll) {
        super(layoutResId, data);
        arrayList = (ArrayList<RateModel.BiddingVehicle>) data;
        isCheckShowAll = checkShowAll;
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
        super.convert(holder, o);
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (!isCheckShowAll) {
            size = 1;
        } else {
            size = arrayList.size();
        }
        return size;
    }

    @Override
    public void onBindViewHolder(@NotNull BaseViewHolder holder, int position, @NotNull List payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.itemView.findViewById(R.id.imgViewGroupRate).setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Bundle args = new Bundle();
                args.putSerializable(Constants.DATA_INFORMATION, arrayList.get(position));
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, LoadMoreImgViewFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                getContext().startActivity(intent);
            }
            return true;
        });
    }

    @Override
    protected void convert(@NotNull BaseDataBindingHolder holder, Object item, @NotNull List payloads) {
        super.convert(holder, item, payloads);
        holder.getView(R.id.imgViewGroupRate).setVisibility(View.GONE);

    }

    @Override
    protected void setOnItemClick(@NotNull View v, int position) {
        super.setOnItemClick(v, position);
        switch (v.getId()) {
            case R.id.imgViewGroupRate:
                ToastUtils.showToast("duongnk");
                Bundle args = new Bundle();
                args.putSerializable(Constants.DATA_INFORMATION, arrayList);
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, LoadMoreImgViewFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                getContext().startActivity(intent);
                break;
        }
    }

}
