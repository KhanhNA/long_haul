package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.NotificationFragmentBinding;
import com.nextsolutions.longhold.model.NotificationModel;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.NotificationVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;

public class NotificationFragment extends BaseFragment {


    private NotificationVM mNotificationVM;
    private NotificationFragmentBinding mNotificationFragmentBinding;
    private int mOffset = 0;
    private BaseAdapterV3 mNotificationAdapter;
    private LinearLayoutManager linearLayoutManager;
    private boolean isRefresh;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh)
            refreshData();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mNotificationVM = (NotificationVM) viewModel;
        mNotificationFragmentBinding = (NotificationFragmentBinding) binding;
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        return view;
    }

    private void initView() {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mNotificationFragmentBinding.recyclerBidding.setLayoutManager(linearLayoutManager);
        mNotificationAdapter = new BaseAdapterV3(R.layout.item_history, mNotificationVM.getNotification().getValue().getRecords(), this);
        mNotificationFragmentBinding.recyclerBidding.setAdapter(mNotificationAdapter);
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mNotificationAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mNotificationAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mNotificationAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mNotificationFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mNotificationFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void offRefreshing() {
        mNotificationFragmentBinding.swipeLayout.setRefreshing(false);
        mNotificationAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void refreshData() {
        mNotificationVM.clearData();
        mOffset = 0;
        getData();
    }

    private void loadMore() {
        int totalRecord = mNotificationVM.getNotification().getValue().getTotal_record();
        if (mOffset < totalRecord) {
            getData();

        } else {
            //thông báo không còn gì để load
            mNotificationAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void getData() {
        mNotificationVM.requestGetNotification(mOffset, this::runUi);
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        NotificationModel notificationModel = (NotificationModel) o;
        switch (v.getId()) {
            case R.id.viewGroup:
                isRefresh = true;
                mNotificationVM.confirmRedNotification(notificationModel.getId());
                Bundle args = new Bundle();
                args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer.parseInt(notificationModel.getItem_id())));
                args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BID_SUCCESS);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);

                break;
        }
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                mOffset += 10;
                mNotificationAdapter.setList(Objects.requireNonNull(mNotificationVM.getNotification().getValue()).getRecords());
                mNotificationAdapter.notifyDataSetChanged();
                emptyData(mNotificationVM.getNotification().getValue().getRecords());
                offRefreshing();
                break;
            case Constants.FAIL_API:
                emptyData(Objects.requireNonNull(mNotificationVM.getNotification().getValue()).getRecords());
                offRefreshing();
                break;
        }
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mNotificationFragmentBinding.emptyData.setVisibility(View.GONE);
            mNotificationFragmentBinding.swipeLayout.setVisibility(View.VISIBLE);
        } else {
            mNotificationFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            mNotificationFragmentBinding.swipeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NotificationModel notificationModel) {
        mNotificationAdapter.addData(0, notificationModel);
        mNotificationFragmentBinding.recyclerBidding.scrollToPosition(0);
    }
}
