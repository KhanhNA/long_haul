package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RankingDTO extends BaseModel {
    private Integer id;
    private String name;
    private Integer from_point;
    private Integer to_point;
    private String current_rank;
    private Integer point_level_up;
    private String uri_path;
    private List<PrivilegesDTO> list_awards_leve;
}
