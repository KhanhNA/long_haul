package com.nextsolutions.longhold.enums;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class EnumBidding {
    private static EnumBidding mInstance = null;

    public static EnumBidding getInstance() {
        if (mInstance == null) {
            mInstance = new EnumBidding();
        }
        return mInstance;
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumBiddingBehavior.BIDDING_HAPPENNING, EnumBiddingBehavior.WAITING_FOR_INFORMATION, EnumBiddingBehavior.WAITING_FOR_CONFIRMATION,
            EnumBiddingBehavior.BID_SUCCESS, EnumBiddingBehavior.BID_HISTORY, EnumBiddingBehavior.BIDDING_UPCOMING})
    public @interface EnumBiddingBehavior {
        int BIDDING_HAPPENNING = 1;
        int WAITING_FOR_INFORMATION = 2;
        int WAITING_FOR_CONFIRMATION = 3;
        int BID_SUCCESS = 4;
        int BID_HISTORY = 5;
        int BIDDING_UPCOMING = 6;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumSortBiddingBehavior.PRICE_ASCENDING, EnumSortBiddingBehavior.SORT_DESCENDING_PRICES, EnumSortBiddingBehavior.DESCENDING_DISTANCE,
            EnumSortBiddingBehavior.EAR_LIEST, EnumSortBiddingBehavior.ASCENDING_DISTANCE, EnumSortBiddingBehavior.OLD})
    public @interface EnumSortBiddingBehavior {
        int PRICE_ASCENDING = 1;
        int SORT_DESCENDING_PRICES = 2;
        int ASCENDING_DISTANCE = 3;
        int DESCENDING_DISTANCE = 4;
        int EAR_LIEST = 5;
        int OLD = 6;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({EnumSocketType.BiddingPackage, EnumSocketType.BiddingTime, EnumSocketType.ChangePriceAction, EnumSocketType.OvertimeBiddingAction})
    public @interface EnumSocketType {
        String BiddingTime = "biddingTime";
        String ChangePriceAction = "changePriceAction";
        String OvertimeBiddingAction = "overtimeBiddingAction";
        String BiddingPackage = "biddingPackage";
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumStatusManagerCargo.GET_ALL, EnumStatusManagerCargo.GET_CARGO_BIDDING, EnumStatusManagerCargo.GET_CARO_NOT_BIDDING})
    public @interface EnumStatusManagerCargo {
        int GET_ALL = 2;
        int GET_CARGO_BIDDING = 1;
        int GET_CARO_NOT_BIDDING = 0;
    }
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({})
    public @interface EnumActionNotification{
        int ACTION_DETAIL = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({EnumStatus.SUCCESS, EnumStatus.FAIL})
    public @interface EnumStatus {
       String SUCCESS = "SUCCESS";
       String FAIL = "FAIL";
    }
}
