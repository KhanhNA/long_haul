package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.ListVehicleFragmentBinding;
import com.nextsolutions.longhold.enums.VehicleStatus;
import com.nextsolutions.longhold.model.Vehicle;
import com.nextsolutions.longhold.ui.dialog.VehicleInfoDialog;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.ListVehicleVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

public class ListVehicleFragment extends BaseFragment {
    ListVehicleFragmentBinding mBinding;
    ListVehicleVM listVehicleVM;
    BaseAdapterV3 adapter;
    Vehicle vehicle_select;
    int REQUEST_ADD_VEHICLE = 1234;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (ListVehicleFragmentBinding) binding;
        listVehicleVM = (ListVehicleVM) viewModel;

        initView();
        initLoadMore();
        return view;
    }

    private void initView() {
        setUpRecycleView();
        listVehicleVM.getVehicles(false, this::runUi);
        mBinding.swRefresh.setOnRefreshListener(() -> {
            listVehicleVM.getVehicles(false, this::runUi);
        });
        mBinding.btnVehicleStatus.setOnClickListener(v -> openDialogSelectVehicleStatus(v));
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getVehicleSuccess":
                adapter.notifyDataSetChanged();
                break;
            case "getVehicleFail":
                Log.i("ListVehicle", "runUi: ...getListVehicleFail");
                break;
            case Constants.NO_MORE_DATA:
                adapter.getLoadMoreModule().loadMoreEnd();
                break;
            default:
                break;
        }

    }

    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            listVehicleVM.onLoadMore(this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    public void gotoAddVehicleFragment(int from_type) {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        intent.putExtra("FROM_TYPE", from_type);
        if (from_type == 1) {
            intent.putExtra("MODEL", vehicle_select);
        }
        intent.putExtra(Constants.FRAGMENT, AddVehicleFragment.class);
        startActivityForResult(intent, REQUEST_ADD_VEHICLE);
    }

    @SuppressLint("RestrictedApi")
    private void openDialogSelectVehicleStatus(View view) {
//Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_vehicle_status, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mn_all:
                    listVehicleVM.getApproved_check().set(VehicleStatus.ALL);
                    mBinding.btnVehicleStatus.setText(getString(R.string.ALL));
                    break;
                case R.id.mn_waiting:
                    listVehicleVM.getApproved_check().set(VehicleStatus.WAITING);
                    mBinding.btnVehicleStatus.setText(getString(R.string.waiting));
                    break;
                case R.id.mn_Accepted:
                    listVehicleVM.getApproved_check().set(VehicleStatus.ACCEPTED);
                    mBinding.btnVehicleStatus.setText(getString(R.string.accepted));
                    break;
                case R.id.mn_rejected:
                    listVehicleVM.getApproved_check().set(VehicleStatus.REJECTED);
                    mBinding.btnVehicleStatus.setText(getString(R.string.rejected));
                    break;
            }

            listVehicleVM.getVehicles(false, this::runUi);
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void setUpRecycleView() {
        adapter = new BaseAdapterV3(R.layout.layout_item_vehicle, listVehicleVM.getListVehicle(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                vehicle_select = (Vehicle) o;
                //gotoAddVehicleFragment(1);

                if (view.getId() == R.id.itemVehicle) {
                    VehicleInfoDialog dialogVehicleInfo = new VehicleInfoDialog(vehicle_select);

                    FragmentManager fragmentManager = getFragmentManager();
                    dialogVehicleInfo.show(fragmentManager, "DialogVehicleInfo");
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcContent.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_VEHICLE && resultCode == Constants.RESULT_OK) {
            listVehicleVM.getVehicles(false, this::runUi);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_vehicle_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListVehicleVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }

    public void searchInfo(String textSearch) {
        if (textSearch == null) {
            textSearch = "";
        }
        try {
            listVehicleVM.getTxtSearch().set(textSearch);
            listVehicleVM.getVehicles(false, this::runUi);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
