package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.tsolution.base.BaseViewModel;

public class SettingsVM extends BaseViewModel {
    public SettingsVM(@NonNull Application application) {
        super(application);
    }

    public void AcceptNotification(String id, RunUi runUi) {
        DriverApi.acceptNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                runUi.run(Constants.SUCCESS_API);
                Log.e("duongnk", "onSuccess: AcceptNotification" );
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
                Log.e("duongnk", "onFail: AcceptNotification" );
            }
        });


    }

}
