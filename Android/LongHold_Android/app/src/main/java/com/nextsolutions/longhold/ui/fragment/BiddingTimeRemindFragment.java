package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.database.CalendarEvent;
import com.nextsolutions.longhold.database.RealmCalendarEvent;
import com.nextsolutions.longhold.databinding.ChildBiddingFragmentBinding;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.ui.adapter.CalendarBiddingAdapter;
import com.nextsolutions.longhold.ui.viewcommon.BiddingBottomSheet;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.HideViewOnScrollListener;
import com.nextsolutions.longhold.utils.ReminderProvide;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.PackageTimeChildBiddingVM;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import java.util.ArrayList;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.TIME;
import static com.nextsolutions.longhold.utils.ReminderProvide.pushAppointmentsToCalender;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;

public class BiddingTimeRemindFragment extends BaseFragment implements ActionsListener, BiddingBottomSheet.OnclickSortListBidding {
    public String mDate;
    private ChildBiddingFragmentBinding biddingFragmentBinding;
    private PackageTimeChildBiddingVM mPackageTimeChildBiddingVM;
    private CalendarBiddingAdapter mBaseAdapter;
    private int offset = 0;
    // = 6 -> default giá tăng dần
    private int mOrderBy = 6;
    private int mKeySort = 0;
    CountDownTimer countDownTimer;
    private int mPosition;
    private ArrayList<Long> mCountDowTimeMillisecond = new ArrayList<>();


    @Override
    public int getLayoutRes() {
        return R.layout.child_bidding_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PackageTimeChildBiddingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        biddingFragmentBinding = (ChildBiddingFragmentBinding) binding;
        mPackageTimeChildBiddingVM = (PackageTimeChildBiddingVM) viewModel;
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initView() {
        biddingFragmentBinding.recyclerBiddingPackage.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        // mặc định luôn luôn là danh sách sắp bắt đầu
        mBaseAdapter = new CalendarBiddingAdapter(R.layout.item_bidding_package, mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords(), false, this);
        biddingFragmentBinding.recyclerBiddingPackage.setAdapter(mBaseAdapter);
        biddingFragmentBinding.tvTitleCountDow.setText(getString(R.string.start_countdow_time));
        biddingFragmentBinding.btnFilter.setText(mPackageTimeChildBiddingVM.getTitleSort(mPackageTimeChildBiddingVM.getStatusSelectSort(), getContext()));
        biddingFragmentBinding.btnFilter.setOnClickListener(v -> new BiddingBottomSheet(getActivity(), BiddingTimeRemindFragment.this).show());

        biddingFragmentBinding.recyclerBiddingPackage.addOnScrollListener(new HideViewOnScrollListener(biddingFragmentBinding.appBarLayout));

    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mBaseAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mBaseAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mBaseAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        biddingFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        biddingFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void getData() {
        mDate = getArguments().getString(Constants.KEY_PAGE);
        mPosition = getArguments().getInt(Constants.POSITION);
        mCountDowTimeMillisecond = (ArrayList<Long>) getArguments().getSerializable(Constants.DATA_PASS_FRAGMENT);
        mPackageTimeChildBiddingVM.requestBiddingPackage(mDate, mOrderBy, offset, this::runUi);

    }

    private void refreshData() {
        mPackageTimeChildBiddingVM.clearData();
        offset = 0;
        mPackageTimeChildBiddingVM.sortBidding(mDate, mPackageTimeChildBiddingVM.getStatusSelectSort(), offset, this::runUi);
    }

    private void loadMore() {
        int length = mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            mPackageTimeChildBiddingVM.sortBidding(mDate, mPackageTimeChildBiddingVM.getStatusSelectSort(), offset, this::runUi);

        } else {
            //thông báo không còn gì để load
            mBaseAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        if (s.equals(Constants.SUCCESS_API)) {
            offset++;
            mBaseAdapter.setList(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
            mBaseAdapter.notifyDataSetChanged();
            offRefreshing();
            countDowTime(mPosition);
            emptyData(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
        }
        if (s.equals(Constants.FAIL_API)) {
            emptyData(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
            offRefreshing();
        }
    }

    private void offRefreshing() {
        biddingFragmentBinding.swipeLayout.setRefreshing(false);
        mBaseAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    /**
     * thêm nhắc nhở vào calendar
     *
     * @param id
     * @param o
     */
    private void addNewEvent(String id, Object o) {
        // kiểm tra sự kiện đã đc thêm ở trước đó chưa (true là chưa có và thực hiện add - false -> đã có và thực hiện xóa)
        if (isCheckIdEventCalendar(id)) {
            BiddingPackage biddingPackage = (BiddingPackage) o;
            RealmCalendarEvent.getInstance(getContext()).addIdEvent(id + "");

            String from = (biddingPackage.getFrom_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getFrom_receive_time()) : "");
            String to = (biddingPackage.getTo_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getTo_receive_time()) : "");
            String content = "[bidding_order_number - " + biddingPackage.getDistance() + "km - " + biddingPackage.getPrice_str() + "]" + "Gói thầu từ " + from + "-" + to;
            pushAppointmentsToCalender(getActivity(), "[LongHaul Bidding]", content, mCountDowTimeMillisecond.get(mPosition), true, 1, 1);
            refreshData();
            Toast.makeText(getActivity(), R.string.add_calendar, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * kiem tra id da dc add vao calendar k
     * nếu đã đc add rồi thì sẽ thực hiện chức năng xóa nhắc nhở
     *
     * @param id
     * @return
     */
    private boolean isCheckIdEventCalendar(String id) {
        boolean isCheck = true;
        List<CalendarEvent> calendarEventList = RealmCalendarEvent.getInstance(AppController.getInstance().getBaseContext()).readCalendar();
        for (int i = 0; i < calendarEventList.size(); i++) {
            if (calendarEventList.get(i).getMIdEvent().equals(id)) {
                RealmCalendarEvent.getInstance(getContext()).deleteCalendar(id);
                ReminderProvide.deleteEventCalendar(id);
                refreshData();
                Toast.makeText(getActivity(), R.string.remove_calendar, Toast.LENGTH_SHORT).show();
                isCheck = false;
                break;
            }
        }
        return isCheck;
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.viewGroup:
                // click item listview
                BiddingPackage mBiddingPackage = (BiddingPackage) o;
                Bundle args = new Bundle();
                args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, mBiddingPackage.getId());
                args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BIDDING_UPCOMING);
                args.putSerializable(TIME, mCountDowTimeMillisecond);
                args.putInt(Constants.POSITION, mPosition);
                args.putSerializable(Constants.DATA_SERIALIZABLE, mBiddingPackage);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);
                break;
            case R.id.btnBidding:
                // click button đấu thầu
                if (biddingFragmentBinding.tvCountDowTime.getText().equals("00:00:00")) {
                    ToastUtils.showToast(getString(R.string.title_notifi_notbidding));
                    return;
                }
                BiddingPackage biddingPackage = null;
                if (o instanceof BiddingPackage)
                    biddingPackage = (BiddingPackage) o;

                if (ReminderProvide.haveCalendarReadWritePermissions(getActivity())) {
                    addNewEvent(biddingPackage.getId() + "", o);
                } else {
                    ReminderProvide.requestCalendarReadWritePermission(getActivity());
                }
                break;
        }
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
        }
    }

    public void countDowTime(int position) {
        Log.e("reload", "countDowTime: 2");
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        long datePosition = mCountDowTimeMillisecond.get(position);
        long myCurrentTimeMillis = System.currentTimeMillis();
        countDownTimer = new CountDownTimer(datePosition - myCurrentTimeMillis, 1000) {
            public void onTick(long millisUntilFinished) {
                biddingFragmentBinding.tvCountDowTime.setText(AppController.hmsTimeFormatter(millisUntilFinished));
            }

            public void onFinish() {
                biddingFragmentBinding.tvCountDowTime.setText("00:00:00");
            }
        }.start();

    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            biddingFragmentBinding.viewNotData.setVisibility(View.GONE);
            biddingFragmentBinding.recyclerBiddingPackage.setVisibility(View.VISIBLE);
        } else {
            biddingFragmentBinding.viewNotData.setVisibility(View.VISIBLE);
            biddingFragmentBinding.recyclerBiddingPackage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSortListBidding(int keySort) {
        offset = 0;
        mPackageTimeChildBiddingVM.clearData();
        biddingFragmentBinding.btnFilter.setText(mPackageTimeChildBiddingVM.sortBidding(mDate, keySort, offset, this::runUi));
    }

    @Override
    public void action(Object... objects) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == ReminderProvide.CALENDARHELPER_PERMISSION_REQUEST_CODE) {
            if (ReminderProvide.haveCalendarReadWritePermissions(getActivity())) {
                Toast.makeText(getActivity(), (String) "Have Calendar Read/Write Permission.",
                        Toast.LENGTH_LONG).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void updateUI() {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshData();
            }
        });
    }

}
