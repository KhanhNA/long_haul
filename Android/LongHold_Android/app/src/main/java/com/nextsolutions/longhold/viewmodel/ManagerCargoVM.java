package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

public class ManagerCargoVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<VehicleCargo>> mVehicleCargo = new MutableLiveData<>();
    private OdooResultDto<VehicleCargo> mVehicle;
    private int mStatusSortCargo = EnumBidding.EnumStatusManagerCargo.GET_ALL;

    public ManagerCargoVM(@NonNull Application application) {
        super(application);
        mVehicle = new OdooResultDto<>();
        mVehicleCargo.setValue(mVehicle);
    }

    public MutableLiveData<OdooResultDto<VehicleCargo>> getVehicleCargo() {
        return mVehicleCargo;
    }

    public void requestVehicle(String text_search, int offset, int status, RunUi runUi) {
        DriverApi.getVehicleCargo(text_search, offset, status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<VehicleCargo> response = (OdooResultDto<VehicleCargo>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                if (mVehicleCargo.getValue() != null && mVehicleCargo.getValue().getRecords() != null) {
                    mVehicleCargo.getValue().getRecords().addAll(response.getRecords());
                    mVehicleCargo.getValue().setLength(response.getLength());
                    getVehicleCargo().setValue(mVehicleCargo.getValue());
                } else {
                    getVehicleCargo().setValue(response);
                }
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public String sortCargo(int statusSortCargo, String text_search, int offset, RunUi runUi) {
        StringBuilder tvSort = new StringBuilder("");
        Context context = AppController.getInstance().getBaseContext();
        switch (statusSortCargo) {
            case R.id.btnAllCargo:
                mStatusSortCargo = EnumBidding.EnumStatusManagerCargo.GET_ALL;
                requestVehicle(text_search, offset, EnumBidding.EnumStatusManagerCargo.GET_ALL, runUi);
                tvSort.append(context.getString(R.string.all_cargo));
                break;
            case R.id.btnNoBidding:
                mStatusSortCargo = EnumBidding.EnumStatusManagerCargo.GET_CARO_NOT_BIDDING;
                requestVehicle(text_search, offset, EnumBidding.EnumStatusManagerCargo.GET_CARO_NOT_BIDDING, runUi);
                tvSort.append(context.getString(R.string.cargo_not_bidding));

                break;
            case R.id.btnBidding:
                mStatusSortCargo = EnumBidding.EnumStatusManagerCargo.GET_CARGO_BIDDING;
                requestVehicle(text_search, offset, EnumBidding.EnumStatusManagerCargo.GET_CARGO_BIDDING, runUi);
                tvSort.append(context.getString(R.string.cargo_bidding));
                break;
        }
        return tvSort.toString();
    }

    public void clearData() {
        mVehicleCargo.setValue(new OdooResultDto<>());
    }

    public int getStatusCargo() {
        return mStatusSortCargo;
    }

    public void removeIndexData(int position) {
        mVehicleCargo.getValue().getRecords().remove(position);
        getVehicleCargo().setValue(mVehicleCargo.getValue());
    }

    public void removeCargo(int id, RunUi runUi) {
        DriverApi.removeVehicleCargo(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                runUi.run(Constants.REMOVE_CARGO);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }
}
