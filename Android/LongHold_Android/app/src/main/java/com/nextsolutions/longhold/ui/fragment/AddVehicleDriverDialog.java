package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.DialogAddVehicleDriverBinding;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.Vehicle;
import com.nextsolutions.longhold.model.VehicleDriver;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.AddVehicleDriverVM;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

import java.util.HashMap;


public class AddVehicleDriverDialog extends BottomSheetDialogFragment implements AdapterListener {
    private static final int REQUEST_ADD_DRIVER = 127;
    private static final int REQUEST_ADD_VEHICLE = 1000;
    private final onConfirm onConfirm;
    public VehicleDriver vehicleDriver;
    DialogAddVehicleDriverBinding binding;
    AddVehicleDriverVM vehicleDriverVM;
    ListDialogFragment listDialogFragment;
    HashMap<Long, Driver> selectedDrivers;
    HashMap<Long, Vehicle> selectedVehicles;
    //    DialogConfirm edit_vehicle;
    DialogConfirm delete_vehicle;
    Vehicle vehicle_select;

    AddVehicleDriverDialog(VehicleDriver vehicleDriver, HashMap<Long, Driver> selectedDrivers, HashMap<Long, Vehicle> selectedVehicles, onConfirm onConfirm) {
        if (vehicleDriver != null) {
            this.vehicleDriver = vehicleDriver;
        } else {
            this.vehicleDriver = new VehicleDriver();
        }
        this.onConfirm = onConfirm;
        this.selectedDrivers = selectedDrivers;
        this.selectedVehicles = selectedVehicles;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_vehicle_driver, container, false);
        vehicleDriverVM = ViewModelProviders.of(this).get(AddVehicleDriverVM.class);
        vehicleDriverVM.getModel().set(vehicleDriver);
        binding.setViewModel(vehicleDriverVM);
        binding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return binding.getRoot();
    }

    private void initView() {
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        binding.btnAddVehicle.setOnClickListener(v -> openAddVehicleFragment(0));
        binding.btnAddDriver.setOnClickListener(v -> openAddDriverFragment());

    }

    private void openAddDriverFragment() {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, DriverInfoFragment.class);
        startActivityForResult(intent, REQUEST_ADD_DRIVER);
    }

    //from_type : 0 tạo xe mới, 1 sửa xe
    private void openAddVehicleFragment(int from_type) {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        intent.putExtra("FROM_TYPE", from_type);
        if (from_type == 1) {
            intent.putExtra("MODEL", vehicle_select);
        }
        intent.putExtra(Constants.FRAGMENT, AddVehicleFragment.class);
        startActivityForResult(intent, REQUEST_ADD_VEHICLE);
    }

    public void selectDriver() {
        vehicleDriverVM.getDrivers(this::runUi);
    }

    public void selectVehicle() {
        vehicleDriverVM.getVehicles(this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getDriverSuccess":
                listDialogFragment = new ListDialogFragment(R.layout.layout_item_driver, R.string.select_driver, "", vehicleDriverVM.getDriverList(), this);
                listDialogFragment.show(getChildFragmentManager(), listDialogFragment.getTag());
                break;
            case "getDriverFail":
                ToastUtils.showToast(getString(R.string.cannot_get_drivers));
                break;
            case "getVehicleSuccess":
                listDialogFragment = new ListDialogFragment(R.layout.layout_item_vehicle, R.string.select_vehicle, "", vehicleDriverVM.getVehicleList(), this);
                listDialogFragment.show(getChildFragmentManager(), listDialogFragment.getTag());
                break;
            case "getVehicleFail":
                ToastUtils.showToast(getString(R.string.cannot_get_vehicles));
                break;
        }

    }

    public void addVehicleDriver() {
        if (vehicleDriverVM.isValid()) {
            onConfirm.onAddVehicleDriver(vehicleDriverVM.getModelE());
            dismiss();
        } else {
            ToastUtils.showToast(getString(R.string.msg_select_vehicle_driver));
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }


    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onItemClick(View view, Object o) {
        if (view.getId() == R.id.itemVehicle) {
            if (selectedVehicles.get(((Vehicle) o).getId()) != null) {
                ToastUtils.showToast(getString(R.string.msg_exist_vehicle));
            } else {
                vehicleDriverVM.getModelE().setVehicle((Vehicle) o);
                vehicleDriverVM.getModel().notifyChange();
                listDialogFragment.dismiss();
            }
        } else if (view.getId() == R.id.itemDriver) {
            if (selectedDrivers.get(((Driver) o).getId()) != null) {
                ToastUtils.showToast(getString(R.string.msg_exist_driver));
            } else {
                vehicleDriverVM.getModelE().setDriver((Driver) o);
                vehicleDriverVM.getModel().notifyChange();
                listDialogFragment.dismiss();

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_VEHICLE && resultCode == Constants.RESULT_OK) {
            vehicleDriverVM.setChangeVehicle(true);
            vehicleDriverVM.getVehicles(this::runUi);
        } else if (requestCode == REQUEST_ADD_DRIVER && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                int action = data.getIntExtra("ACTION", -1);
                switch (action) {
                    case 1://thêm
                        Driver driver = (Driver) data.getSerializableExtra("DRIVER");
                        if (driver != null) {
                            vehicleDriverVM.getModelE().setDriver(driver);
                            vehicleDriverVM.getModel().notifyChange();
                            vehicleDriverVM.setChangeDriver(true);
                        }
                        break;
                    case 2://sửa
                        int position = data.getIntExtra("POSITION", -1);
                        Driver driverUpdate = (Driver) data.getSerializableExtra("DRIVER");
                        if (driverUpdate != null) {
                            vehicleDriverVM.getDriverList().set(position, driverUpdate);
                            vehicleDriverVM.getModelE().setDriver(driverUpdate);
                            vehicleDriverVM.getModel().notifyChange();
                        }
                        break;
                    case 3://xóa
                        int positionDelete = data.getIntExtra("POSITION", -1);
                        if (positionDelete > -1) {
                            vehicleDriverVM.getDriverList().remove(positionDelete);
                            selectDriver();
                        }
                        break;

                }
            }

        }
        if (requestCode == REQUEST_ADD_VEHICLE && resultCode == Constants.RESULT_OK) {
            vehicleDriverVM.setChangeVehicle(true);
            vehicleDriverVM.getVehicles(this::runUi);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }

    public interface onConfirm {
        void onAddVehicleDriver(VehicleDriver vehicleDriver);
    }
}
