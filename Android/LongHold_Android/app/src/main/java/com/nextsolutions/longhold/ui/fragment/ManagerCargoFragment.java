package com.nextsolutions.longhold.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.ManagerCargoFragmentBinding;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.ui.viewcommon.BiddingBottomSheet;
import com.nextsolutions.longhold.ui.viewcommon.FilterCargoDialog;
import com.nextsolutions.longhold.ui.viewcommon.InforCargoBottomSheet;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.ManagerCargoVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.KEY_START_ACTIVITY_FOR_RESULTS;

public class ManagerCargoFragment extends BaseFragment implements BiddingBottomSheet.OnclickSortListBidding {
    private ManagerCargoFragmentBinding mManagerCargoFragmentBinding;
    private ManagerCargoVM mManagerCargoVM;
    private BaseAdapterV3 adapter;
    private int mOffset = 0;
    private String textSearch = "";
    private int mStatusSortCargo;
    private int indexData;
    private int idCargo;
    private VehicleCargo getVehicleCargo;
    private DialogConfirm dialogConfirmRemoveCargo;

    @Override
    public int getLayoutRes() {
        return R.layout.manager_cargo_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ManagerCargoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mManagerCargoFragmentBinding = (ManagerCargoFragmentBinding) binding;
        mManagerCargoVM = (ManagerCargoVM) viewModel;
        initView();
        initLoadMore();
        initRefreshData();
        getData(textSearch);
        eventChangeEdtSearch();
        return v;
    }


    private void initView() {
        // getStatusSortCargo
        mStatusSortCargo = mManagerCargoVM.getStatusCargo();
        //int Adapter
        adapter = new BaseAdapterV3<>(R.layout.item_cargo, mManagerCargoVM.getVehicleCargo().getValue().getRecords(), this);
        mManagerCargoFragmentBinding.recyclerCargo.setLayoutManager(new LinearLayoutManager(getContext()));
        mManagerCargoFragmentBinding.recyclerCargo.setAdapter(adapter);


    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mManagerCargoFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mManagerCargoFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void getData(String textSearch) {
        mManagerCargoVM.requestVehicle(textSearch, mOffset, mManagerCargoVM.getStatusCargo(), this::runUi);
    }

    private void loadMore() {
        int length = mManagerCargoVM.getVehicleCargo().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            getData(textSearch);
        } else {
            //thông báo không còn gì để load
            adapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void refreshData() {
        // refresh data
        mManagerCargoVM.clearData();
        mOffset = 0;
        getData(textSearch);
    }

    private void offRefreshing() {
        mManagerCargoFragmentBinding.swipeLayout.setRefreshing(false);
        adapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void eventChangeEdtSearch() {
        mManagerCargoFragmentBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("") || s.toString().trim().length() == 0) {
                    textSearch = "";
                    eventSearch(textSearch);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void eventSearch(String tvSearch) {
        // clear Data & set Default offset
        mManagerCargoVM.clearData();
        mOffset = 0;
        getData(tvSearch);
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mManagerCargoFragmentBinding.viewHideData.setVisibility(View.GONE);
            mManagerCargoFragmentBinding.swipeLayout.setVisibility(View.VISIBLE);
        } else {
            mManagerCargoFragmentBinding.viewHideData.setVisibility(View.VISIBLE);
            mManagerCargoFragmentBinding.swipeLayout.setVisibility(View.GONE);
        }
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                if (mManagerCargoVM.getVehicleCargo().getValue() != null && mManagerCargoVM.getVehicleCargo().getValue().getRecords() != null) {
                    mOffset++;
                    adapter.setList(mManagerCargoVM.getVehicleCargo().getValue().getRecords());
                    adapter.notifyDataSetChanged();
                    emptyData(mManagerCargoVM.getVehicleCargo().getValue().getRecords());
                    offRefreshing();
                }
                break;

            case Constants.FAIL_API:
                if (mManagerCargoVM.getVehicleCargo().getValue() != null && mManagerCargoVM.getVehicleCargo().getValue().getRecords() != null) {
                    emptyData(mManagerCargoVM.getVehicleCargo().getValue().getRecords());
                } else {
                    mManagerCargoFragmentBinding.viewHideData.setVisibility(View.VISIBLE);
                }
                offRefreshing();
                break;
            case Constants.DELETE_CARGO:
                StringBuilder mTitle = new StringBuilder(getString(R.string.title_remove_cargo_1));
                mTitle.append(" "+mManagerCargoVM.getVehicleCargo().getValue().getRecords().get(indexData).getLisence_plate());
                mTitle.append(" "+getString(R.string.title_remove_cargo_2));
                dialogConfirmRemoveCargo = new DialogConfirm(mTitle.toString(), "", false, false, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mManagerCargoVM.removeCargo(idCargo, ManagerCargoFragment.this::runUi);
                        dialogConfirmRemoveCargo.dismiss();
                    }
                });
                dialogConfirmRemoveCargo.show(getChildFragmentManager(), "");

                break;

            case Constants.EDIT_CARGO:
                // sự kiện chỉnh sủa
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.EDIT_CARGO_DATA, getVehicleCargo);
                bundle.putInt(Constants.KEY_EDIT_INFO_CARGO, 5);
                Intent intent1 = new Intent(getActivity(), CommonActivity.class);
                intent1.putExtra(Constants.FRAGMENT, UpdateCargoFragment.class);
                intent1.putExtra(Constants.DATA_PASS_FRAGMENT, bundle);
                startActivityForResult(intent1, KEY_START_ACTIVITY_FOR_RESULTS);
                break;

            case Constants.VIEW_INFOR_CARGO:
                // sự liện xem thông tin
                Bundle args = new Bundle();
                args.putSerializable(Constants.DATA_SERIALIZABLE, getVehicleCargo);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, VehicleOrdersFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);
                break;
            case Constants.REMOVE_CARGO:
                if (mManagerCargoVM.getVehicleCargo().getValue() != null && mManagerCargoVM.getVehicleCargo().getValue().getRecords() != null) {
                    mManagerCargoVM.removeIndexData(indexData);
                    adapter.setList(mManagerCargoVM.getVehicleCargo().getValue().getRecords());
                    adapter.notifyDataSetChanged();
                }
                break;
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnFilter:
                new FilterCargoDialog(getActivity(), this::onSortListBidding).show();
                break;
            case R.id.btnSearch:
                if (mManagerCargoFragmentBinding.edtSearch.getText().toString().trim().isEmpty())
                    return;
                textSearch = mManagerCargoFragmentBinding.edtSearch.getText().toString().trim();
                eventSearch(textSearch);
                break;
            case R.id.imgAddDriverCargo:
                Bundle args = new Bundle();
                args.putInt(Constants.KEY_CREATE_DRIVER_CARGO, Constants.KEY_ADD);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, UpdateCargoFragment.class);
                intent.putExtra(Constants.DATA_PASS_FRAGMENT, args);
                startActivityForResult(intent, KEY_START_ACTIVITY_FOR_RESULTS);
                break;
        }
    }

    @Override
    public void onSortListBidding(int statusSortCargo) {
        // clear Data & set Default offset
        mManagerCargoVM.clearData();
        mOffset = 0;
        mManagerCargoFragmentBinding.btnFilter.setText(mManagerCargoVM.sortCargo(statusSortCargo, textSearch, mOffset, this::runUi));
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        VehicleCargo vehicleCargo = (VehicleCargo) o;
        indexData = mManagerCargoVM.getVehicleCargo().getValue().getRecords().indexOf(vehicleCargo);
        idCargo = mManagerCargoVM.getVehicleCargo().getValue().getRecords().get(indexData).getId();
        switch (v.getId()) {
            case R.id.viewGroup:
                getVehicleCargo = (VehicleCargo) o;
                new InforCargoBottomSheet(getActivity(), vehicleCargo, this::runUi).show();
                break;
            case R.id.btnMapInformation:
                Bundle args = new Bundle();
                args.putSerializable(StringUtils.TO_VEHICLE_ORDES_FRAGMEN, (Serializable) o);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, EmployeeLocationFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == KEY_START_ACTIVITY_FOR_RESULTS) {
            refreshData();
        }
    }
}
