package com.nextsolutions.longhold.ui.viewcommon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.utils.BindingAdapterUtils;

public class ImageRate extends LinearLayout {

    private ImageView mImageView;
    private TextView texMore;
    private View view;

    public ImageRate(Context context) {
        super(context);
        init();
    }

    private void init() {
        view = LayoutInflater.from(getContext()).inflate(R.layout.item_img_rate, this, true);
        mImageView = view.findViewById(R.id.imgView);
        texMore = view.findViewById(R.id.tvMore);
    }

    public void setmImageView(String url) {
        BindingAdapterUtils.loadImage(mImageView, url);
    }

    public void setTexMore(int size) {
        texMore.setVisibility(VISIBLE);
        texMore.setText("+" + size);
    }

    public ImageView getmImageView() {
        return mImageView;
    }

    public TextView getTexMore() {
        return texMore;
    }
}
