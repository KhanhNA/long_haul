package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.Price;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

public class HistoryBiddingVM extends BaseViewModel {
    private MutableLiveData<OdooResultDto<BiddingInformation>> mHistoryBidding = new MutableLiveData<>();
    private OdooResultDto<BiddingInformation> mHistoryBiddingUpdate;
    private MutableLiveData<OdooResultDto<Price>> mPrice = new MutableLiveData<>();

    public HistoryBiddingVM(@NonNull Application application) {
        super(application);
        mHistoryBiddingUpdate = new OdooResultDto<>();
        mHistoryBidding.setValue(mHistoryBiddingUpdate);
    }

    public MutableLiveData<OdooResultDto<BiddingInformation>> getHistoryBidding() {
        return mHistoryBidding;
    }

    public MutableLiveData<OdooResultDto<Price>> getPrice() {
        return mPrice;
    }

    /**
     * get History Bidding
     *
     * @param fromDate
     * @param toDate
     * @param order_by
     * @param offset
     * @param runUi
     */
    public void requestHistoryBidding(String fromDate, String toDate, String txt_search, int order_by, int offset, RunUi runUi) {
        DriverApi.getHistoryBidding(fromDate, toDate, txt_search, order_by, offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<BiddingInformation> response = (OdooResultDto<BiddingInformation>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                if (mHistoryBidding.getValue() != null && mHistoryBidding.getValue().getRecords() != null) {
                    mHistoryBidding.getValue().getRecords().addAll(response.getRecords());
                    mHistoryBidding.getValue().setLength(response.getLength());
                    mHistoryBidding.setValue(mHistoryBidding.getValue());
                } else {
                    mHistoryBidding.setValue(response);
                }
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    /**
     * get Price History
     *
     * @param fromDate
     * @param toDate
     */
    public void getPriceBiddingHistory(String fromDate, String toDate, RunUi runUi) {
        DriverApi.getPriceHistoryBidding(fromDate, toDate, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                mPrice.setValue((OdooResultDto<Price>) o);
                runUi.run(Constants.SUCCESS_PRICE);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public String sortBidding(String fromDate, String toDate,String txtSearch ,int keySort, int offset, RunUi runUi, Context context) {
        StringBuilder titleSort = new StringBuilder(context.getString(R.string.sort));
        switch (keySort) {
            case EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING:
                sortRequest(fromDate, toDate,txtSearch, EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING, offset, runUi);
                titleSort.append("Giá tăng dần");
                break;
            case EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES:
                sortRequest(fromDate, toDate, txtSearch,EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES, offset, runUi);
                titleSort.append("Giá giảm dần");
                break;
            case EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE:
                sortRequest(fromDate, toDate, txtSearch,EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE, offset, runUi);
                titleSort.append("Quãng đường tăng dần");
                break;
            case EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE:
                sortRequest(fromDate, toDate, txtSearch,EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE, offset, runUi);
                titleSort.append("Quãng đường giảm dần");
                break;
            case EnumBidding.EnumSortBiddingBehavior.EAR_LIEST:
                sortRequest(fromDate, toDate,txtSearch, EnumBidding.EnumSortBiddingBehavior.EAR_LIEST, offset, runUi);
                titleSort.append("Đơn mới nhất");
                break;
            case EnumBidding.EnumSortBiddingBehavior.OLD:
                sortRequest(fromDate, toDate, txtSearch,EnumBidding.EnumSortBiddingBehavior.OLD, offset, runUi);
                titleSort.append("Đơn cũ nhất");
                break;
        }
        return titleSort.toString();
    }

    private void sortRequest(String fromDate, String toDate, String txtSearch,int order_by, int offset, RunUi runUi) {
        clearData();
        requestHistoryBidding(fromDate, toDate,txtSearch, order_by, offset, runUi);
    }

    public void clearData() {
        if (getHistoryBidding().getValue() != null)
            getHistoryBidding().setValue(null);
    }

    public void setStatusSelectSort(int order_by) {
        // lưu lại trạng thái sort dữ liệu
        Constants.STATIC_SELECT_SORT = order_by;
    }

    public int getStatusSelectSort() {
        return Constants.STATIC_SELECT_SORT;
    }

    public String getTitleSort(Context context) {
        int order_by = getStatusSelectSort();
        StringBuilder titleSort = new StringBuilder(context.getString(R.string.title_sort_btn) + " ");
        switch (order_by) {
            case EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING:
                titleSort.append(context.getString(R.string.priceAscending));
                break;
            case EnumBidding.EnumSortBiddingBehavior.SORT_DESCENDING_PRICES:
                titleSort.append(context.getString(R.string.descendingPrices));
                break;
            case EnumBidding.EnumSortBiddingBehavior.ASCENDING_DISTANCE:

                titleSort.append(context.getString(R.string.ascendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.DESCENDING_DISTANCE:

                titleSort.append(context.getString(R.string.descendingDistance));
                break;
            case EnumBidding.EnumSortBiddingBehavior.EAR_LIEST:
                titleSort.append(context.getString(R.string.earliest));
                break;
            case EnumBidding.EnumSortBiddingBehavior.OLD:
                titleSort.append(context.getString(R.string.sort_old));
                break;
        }
        return titleSort.toString();
    }
}
