package com.nextsolutions.longhold.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.HaiSer;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.databinding.BiddingFragmentBinding;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.SocketModel;
import com.nextsolutions.longhold.ui.activity.MainActivity;
import com.nextsolutions.longhold.ui.viewcommon.Dialog;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.BiddingHomeVM;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.SocketResult;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.KEY_START_ACTIVITY_FOR_RESULTS;

public class BiddingFragment extends BaseFragment implements ActionsListener {
    BiddingHomeVM mBiddingHomeVM;
    BiddingFragmentBinding mBiddingFragmentBinding;
    private BaseAdapterV3 adapter;
    public int mKeyPage;
    private int mOffset = 0;
    private static BiddingFragment biddingFragment;
    private Dialog dialog;
    private OdooResultDto<SocketModel> mBiddingPackageSocket = new OdooResultDto<>();

    private MainActivity mainActivity;
    private List<SocketModel> socketModelList = new ArrayList<>();

    public static BiddingFragment getInstance() {
        if (biddingFragment == null) {
            biddingFragment = new BiddingFragment();
        }
        return biddingFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bidding_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BiddingHomeVM.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBiddingHomeVM = (BiddingHomeVM) viewModel;
        mBiddingFragmentBinding = (BiddingFragmentBinding) binding;
        mBiddingPackageSocket.setRecords(socketModelList);
        biddingFragment = this;
        iniView();
        initLoadMore();
        initRefreshData();
        getdata();
        return view;
    }

    private void iniView() {
        // key các page trong viewpager Bidding
        mKeyPage = getArguments().getInt(Constants.KEY_PAGE);
        //init adapter bidding
        adapter = new BaseAdapterV3(R.layout.item_bidding, mBiddingHomeVM.getInformationBidding().getValue().getRecords(), this);
        mBiddingFragmentBinding.recyclerBidding.setLayoutManager(new LinearLayoutManager(getContext()));
        mBiddingFragmentBinding.recyclerBidding.setAdapter(adapter);
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mBiddingFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mBiddingFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    /**
     * get danh sách bidding theo status mKeyPage
     */
    private void getdata() {
        mBiddingHomeVM.requestDataInformationBidding(mKeyPage, this::runUi, "", mOffset);
    }


    private void refreshData() {
        // refresh data
        mBiddingHomeVM.clearData();
        mOffset = 0;
        getdata();
        Log.e("duongnk", "refreshData: BiddingFragment");
    }

    private void loadMore() {
        int length = mBiddingHomeVM.getInformationBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            mBiddingHomeVM.requestDataInformationBidding(mKeyPage, this::runUi, "", mOffset);
        } else {
            //thông báo không còn gì để load
            adapter.getLoadMoreModule().loadMoreEnd();
        }

    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            adapter.setList(mBiddingHomeVM.getInformationBidding().getValue().getRecords());
            adapter.notifyDataSetChanged();
            offRefreshing();
            emptyData(mBiddingHomeVM.getInformationBidding().getValue().getRecords());
        }
        if (action.equals(Constants.FAIL_API)) {
            if (mBiddingHomeVM.getInformationBidding().getValue() != null) {
                emptyData(mBiddingHomeVM.getInformationBidding().getValue().getRecords());
            } else {
                mBiddingFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            }
            offRefreshing();
        }
    }

    private void offRefreshing() {
        mBiddingFragmentBinding.swipeLayout.setRefreshing(false);
        adapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mBiddingFragmentBinding.emptyData.setVisibility(View.GONE);
            mBiddingFragmentBinding.recyclerBidding.setVisibility(View.VISIBLE);
        } else {
            mBiddingFragmentBinding.emptyData.setVisibility(View.VISIBLE);
            mBiddingFragmentBinding.recyclerBidding.setVisibility(View.GONE);
        }
    }


    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        BiddingInformation biddingInformation = (BiddingInformation) o;
        switch (id) {
            case R.id.viewGroup:
                if (!biddingInformation.isCheckDuration()) {
                    ToastUtils.showToast(getContext().getString(R.string.expired));
                    return;
                }
                if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_INFORMATION) {
                    Bundle args = new Bundle();
                    args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer) ((BiddingInformation) o).getId());
                    args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.WAITING_FOR_INFORMATION);
                    Intent intent = new Intent(getActivity(), CommonActivity.class);
                    args.putSerializable(Constants.DATA_INFORMATION, biddingInformation);
                    intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                    intent.putExtra(DATA_PASS_FRAGMENT, args);
                    startActivity(intent);
                }
                if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION) {
                    Bundle args = new Bundle();
                    args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer) ((BiddingInformation) o).getId());
                    args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION);
                    Intent intent = new Intent(getActivity(), CommonActivity.class);
                    args.putSerializable(Constants.DATA_INFORMATION, biddingInformation);
                    intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                    intent.putExtra(DATA_PASS_FRAGMENT, args);
                    startActivity(intent);
                }
                if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_SUCCESS) {
                    Bundle args = new Bundle();
                    args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer) ((BiddingInformation) o).getId());
                    args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BID_SUCCESS);
                    args.putSerializable(Constants.DATA_INFORMATION, biddingInformation);
                    Intent intent = new Intent(getActivity(), CommonActivity.class);
                    intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                    intent.putExtra(DATA_PASS_FRAGMENT, args);
                    startActivity(intent);
                }
                break;

            case R.id.btnEnterInfo:
                if (!biddingInformation.isCheckDuration()) {
                    ToastUtils.showToast(getContext().getString(R.string.expired));
                    return;
                }
                break;
            case R.id.btnCancel:
                // case xem lý do bị hủy
                if (biddingInformation.getType().equals("-1")) {
                    dialog = new Dialog(getString(R.string.reasons_for_canceling_the_package), getString(R.string.btnConfirm), ((BiddingInformation) o).getNote(), true, true, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show(getFragmentManager(), "tag");
                } else {
                    // case điền thông tin xe
                    if (!biddingInformation.isCheckDuration()) {
                        ToastUtils.showToast(getContext().getString(R.string.expired));
                        return;
                    }
                    Intent intent = new Intent(getActivity(), ConfirmCargoFragmentV2.class);
                    intent.putExtra("TOTAL_CARGO", biddingInformation.getTotal_cargo());
                    intent.putExtra("TOTAL_WEIGHT", biddingInformation.getTotal_weight());
                    intent.putExtra("BIDDING_ORDER_ID", biddingInformation.getId());
                    intent.putExtra("MAX_CONFIRM_TIME", biddingInformation.getMax_confirm_time());
                    startActivity(intent);
                }
                break;
        }
    }

    /**
     * Nhận sự kiện cập nhận dữ liệu từ socket để cập nhận dữ liệu
     *
     * @param args
     */
    private SocketResult socketResult;

    public void updateSocketData(Object... args) {
        socketModelList.clear();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
                .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        for (int j = 0; j < args.length; j++) {
            JSONObject data = (JSONObject) args[j];
            Log.d("duongnk", "call: " + data.toString());
            socketResult = gson.fromJson(data.toString(), SocketResult.class);
            socketModelList.addAll(socketResult.getResult().getRecords());
        }

        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBiddingPackageSocket.setRecords(socketModelList);
                for (int i = 0; i < mBiddingPackageSocket.getRecords().size(); i++) {
                    String actionType = mBiddingPackageSocket.getRecords().get(i).getActionType();
                    switch (actionType) {
                        case EnumBidding.EnumSocketType.BiddingTime:
                            // Reload lại màn hình new home vc
                            break;
                        case EnumBidding.EnumSocketType.BiddingPackage:
                            // xoá object (nếu có) ở danh sách Đang đấu thầu (new home vc)
                            // reload các tab ở danh sách Đã đấu thầu (home vc)
                            refreshData();
                            Log.e("duongnk", "home vc: refreshData");
                            break;
                        case EnumBidding.EnumSocketType.ChangePriceAction:
                            refreshData();
                            Log.e("duongnk", "home vc: ChangePriceAction");
                            // Nếu status = 0 => update data của object tương ứng (new home vc)
                            // Nếu status = -1 => remove object đó khỏi danh sách đấu thầu (new home vc)
                            break;
                        case EnumBidding.EnumSocketType.OvertimeBiddingAction:
                            // thêm object vào danh sách Đang đấu thầu (new home vc)
                            // xoá object (nếu có) ở danh sách Đã đấu thầu (home vc)
                            removeBiddingSocket(mBiddingPackageSocket.getRecords().get(i).getLstBiddingPackages(), BiddingFragment.this::runUi);
                            Log.e("duongnk", "home vc: removeBiddingSocket");
                            break;
                    }
                }
            }
        });

    }

    private void removeBiddingSocket(List<BiddingPackage> lstBiddingPackages, RunUi runUi) {
        mBiddingHomeVM.removeBiddingSocket(lstBiddingPackages, runUi);
    }


    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_START_ACTIVITY_FOR_RESULTS && resultCode == getActivity().RESULT_OK) {
            refreshData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//        refreshData();

    }
}
