package com.nextsolutions.longhold.base;


import com.nextsolutions.longhold.model.Driver;
import com.ns.odoolib_retrofit.model.OdooSessionDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class StaticData {

    private static OdooSessionDto odooSessionDto;
    public  static String sessionCookie;

    public static final String FRAGMENT = "FRAGMENT";
    public static String TOKEN_DCOM = "";

    public static OdooSessionDto getOdooSessionDto() {
        return odooSessionDto;
    }

    public static void setOdooSessionDto(OdooSessionDto odooSessionDto) {
        StaticData.odooSessionDto = odooSessionDto;
    }

    public static String getUserLogin() {
        if(odooSessionDto == null){
            return "";
        }
        return odooSessionDto.getUsername();
    }
}
