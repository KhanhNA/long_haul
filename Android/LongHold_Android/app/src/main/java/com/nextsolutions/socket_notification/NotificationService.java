package com.nextsolutions.socket_notification;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.HaiSer;
import com.nextsolutions.longhold.model.NotificationModel;
import com.nextsolutions.longhold.ui.activity.SplashActivity;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;
import java.util.Map;

public class NotificationService extends FirebaseMessagingService {
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        PendingIntent pendingIntent = null;
        NotificationModel notificationModel = new NotificationModel();
        if (remoteMessage.getData() != null)
            notificationModel = senData(remoteMessage.getData());

        //-------------------------- Config Intent Notification
        if (notificationModel != null) {
            Intent intent = null;
            if (notificationModel.getClick_action() != null && notificationModel.getClick_action().equals("enterprise_application_bidding_order_detail")) {
                intent = new Intent(getBaseContext(), SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.INTENT_NOTIFICATION, EnumBidding.EnumActionNotification.ACTION_DETAIL);
                intent.putExtra(Constants.EXTRA_DATA,notificationModel);
            }

            pendingIntent = PendingIntent.getActivity(getBaseContext(), 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        }

        //---------------------------- Config Thông báo hệ thống
        String channel_id = createNotificationChannel(getBaseContext());
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext(), channel_id)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
                /*.setLargeIcon(largeIcon)*/
                .setSmallIcon(R.mipmap.ic_launcher) //needs white icon with transparent BG (For all platforms)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDark))
                .setVibrate(new long[]{1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) /* ID of notification */, notificationBuilder.build());

    }

    @SuppressLint("WrongConstant")
    public static String createNotificationChannel(Context context) {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            String channelId = "notification_LongHaul";

            // The user-visible name of the channel.
            CharSequence channelName = "LongHaul";
            // The user-visible description of the channel.
            String channelDescription = "LongHaul Alert";
            int channelImportance = NotificationManager.IMPORTANCE_MAX;
            boolean channelEnableVibrate = true;
//            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
//            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            // Returns null for pre-O (26) devices.
            return null;
        }
    }

    @SuppressLint("NewApi")
    public NotificationModel senData(Map<String, String> data) {
        NotificationModel notificationModel = new NotificationModel();
        String json = data.get("mess_object");
        if (json != null) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
                    .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                    .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                    .create();
            notificationModel = gson.fromJson(json, NotificationModel.class);

            EventBus.getDefault().post(notificationModel);
        }
        return notificationModel;
    }
}