package com.nextsolutions.longhold.model;

import lombok.Getter;

@Getter
public class ActionLog {

   private String van_id;
   private Float latitude;
   private Float longitude;

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }
}
