package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolutions.longhold.api.ConfirmCargoApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.enums.VehicleType;
import com.nextsolutions.longhold.model.BiddingOrder;
import com.nextsolutions.longhold.model.Vehicle;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListVehicleVM extends BaseViewModel {
    ObservableField<String> txtSearch = new ObservableField<>();
    ObservableBoolean isNotData = new ObservableBoolean();
    ObservableBoolean isLoading = new ObservableBoolean();
    List<Vehicle> listVehicle = new ArrayList<>();
    List<BiddingOrder> biddingOrders = new ArrayList<>();
    ObservableField<String> approved_check = new ObservableField<>();
    Integer offset = 0;
    int total_records;

    public ListVehicleVM(@NonNull Application application) {
        super(application);
        isLoading.set(true);
        isNotData.set(true);
    }

    public void getVehicles(Boolean isLoadMore, RunUi runUi) {
        if (!isLoadMore) {
            offset = 0;
            listVehicle.clear();
        }
        isLoading.set(true);
        ConfirmCargoApi.getListVehicles(VehicleType.ALL, offset, approved_check.get(), txtSearch.get(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                OdooResultDto<Vehicle> resultDto = (OdooResultDto<Vehicle>) o;
                if (resultDto == null) {
                    isNotData.set(true);
                    isLoading.set(false);
                }

                if (resultDto != null && resultDto.getRecords() != null) {
                    total_records = resultDto.getTotal_record();
                    listVehicle.addAll(resultDto.getRecords());
                    isNotData.set(listVehicle.size() <= 0);
                    runUi.run("getVehicleSuccess");
                } else {
                    runUi.run("getVehicleFail");
                    isNotData.set(true);
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                isNotData.set(true);
                runUi.run("getVehicleFail");
            }
        });
    }

    public void onLoadMore(RunUi runUi) {
        if (listVehicle.size() < total_records) {
            offset += 1;
            getVehicles(true, runUi);
        } else
            runUi.run(Constants.NO_MORE_DATA);
    }

}
