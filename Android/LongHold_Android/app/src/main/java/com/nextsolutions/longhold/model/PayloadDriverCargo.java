package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayloadDriverCargo extends BaseModel {
    private Integer id;
    private String code;
    private String name;
    private String status;
    private String description;
    private Integer create_uid;
    private String create_date;
    private Integer write_uid;
    private String write_date;
    private Integer max_tonage;
}
