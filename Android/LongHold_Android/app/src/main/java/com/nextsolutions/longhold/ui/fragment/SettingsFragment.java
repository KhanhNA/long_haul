package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.SettingsFragmentBinding;
import com.nextsolutions.longhold.ui.dialog.ChangePasswordDialog;
import com.nextsolutions.longhold.viewmodel.SettingsVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class SettingsFragment extends BaseFragment {


    private SettingsFragmentBinding mSettingsFragmentBinding;
    private SettingsVM mSettingsVM;
    private boolean mIsCheck;

    @Override
    public int getLayoutRes() {
        return R.layout.settings_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SettingsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mSettingsFragmentBinding = (SettingsFragmentBinding) binding;
        mSettingsVM = (SettingsVM) viewModel;
        listenerView();

        return view;
    }

    private void listenerView() {
        mSettingsFragmentBinding.btnChangePassword.setOnClickListener(v -> new ChangePasswordDialog(getActivity()).show());
        mSettingsFragmentBinding.switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    Log.e("duognk", "onCheckedChanged: listenerView");
                    mSettingsVM.AcceptNotification("1", SettingsFragment.this::runUi);
                } else {
                    mSettingsVM.AcceptNotification("0", SettingsFragment.this::runUi);
                }
                mIsCheck = isChecked;
            }
        });
        mSettingsFragmentBinding.btnBack.setOnClickListener(v -> {
            getActivity().finish();
        });
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            Log.e("duognk", "onCheckedChanged: SUCCESS_API");
            mSettingsFragmentBinding.switchNotification.setChecked(mIsCheck);
        }
        if (action.equals(Constants.FAIL_API)) {

        }
    }

}
