package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cargos extends BaseModel {

    private int id;
    private String cargo_number;
    private int from_depot_id;
    private int to_depot_id;
    private int distance;
    private int size_id;
    private String create_date;
    private String write_date;
    private int weight;
    private String description;
    private SizeStandard size_standard;

}