package com.nextsolutions.longhold.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.DialogVehicleInfoBinding;
import com.nextsolutions.longhold.model.Vehicle;

public class VehicleInfoDialog extends DialogFragment {
    DialogVehicleInfoBinding mBinding;

    private Vehicle vehicle;

    public VehicleInfoDialog(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_vehicle_info, container, false);

        mBinding.ivBack.setOnClickListener(v -> {
            dismiss();
        });

        mBinding.setViewHolder(vehicle);
        return mBinding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.fullScreenDialog);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
