package com.nextsolutions.longhold.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.text.Spannable;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asp.fliptimerviewlibrary.CountDownClock;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.enums.VehicleStatus;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingOrder;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.BiddingTimePackage;
import com.nextsolutions.longhold.model.CargoTypes;
import com.nextsolutions.longhold.model.NotificationModel;
import com.nextsolutions.longhold.model.RateModel;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.ui.viewcommon.ImageRate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.listener.AdapterListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.iwgang.countdownview.CountdownView;


public class BindingAdapterUtils {

    @BindingAdapter(value = "imageDrawableRes")
    public static void showImage(ImageView view, String image) {
        RequestOptions options = new RequestOptions().skipMemoryCache(true).override(200, 200).placeholder(R.drawable.ic_avt);
        Glide.with(view.getContext())
                .load(image)
                .apply(options)
                .into(view);
    }

    static CountDownTimer countDownTimer;

    /**
     * set CountDowTime cho biddingItem
     *
     * @param view
     * @param time
     * @param biddingInformation
     */
    @BindingAdapter({"downTime", "biddingInformation"})
    public static void downTimer(CountdownView view, OdooDateTime time, BiddingInformation biddingInformation) {
        //type = 2 && = 1 & = -1 không có countDow
        if (biddingInformation.getType().equals("2") || biddingInformation.getType().equals("1") || biddingInformation.getType().equals("-1")) {
            view.setVisibility(View.GONE);
            return;
        }
        // isDurationCountdow = true -> cowDow đã đc khởi tạo
        if (biddingInformation.isDurationCountdow()) {
            view.setVisibility(View.GONE);
            return;
        }
        //getMax_confirm_time null return k chạy countdow
        if (biddingInformation.getMax_confirm_time() == null) {
            view.setVisibility(View.GONE);
            return;
        }
        if (isValid(biddingInformation)) {
            view.setVisibility(View.VISIBLE);
//            biddingInformation.setDurationCountdow(true);
//            if (countDownTimer != null)
//                countDownTimer.cancel();
//            countDownTimer = new CountDownTimer(cvDateToTimeMillis(time), 1000) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                    view.setText(view.getContext().getString(R.string.duration_maxdate) + " " + hmsTimeFormatter(millisUntilFinished));
//                }
//
//                @Override
//                public void onFinish() {
//                    view.setText(view.getContext().getString(R.string.expired));
//                    biddingInformation.setCheckDuration(false);
//                }
//            };
//            countDownTimer.onTick(cvDateToTimeMillis(time));
//            countDownTimer.start();
            view.start(cvDateToTimeMillis(time)); // Millisecond
            view.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
                @Override
                public void onEnd(CountdownView cv) {
                    cv.allShowZero();
                }
            });

        }
    }

    @BindingAdapter({"setViewDowTime", "biddingInformation"})
    public static void setViewDowTime(TextView view, OdooDateTime time, BiddingInformation biddingInformation) {
        //type = 2 && = 1 & = -1 không có countDow
        if (biddingInformation.getType().equals("2") || biddingInformation.getType().equals("1") || biddingInformation.getType().equals("-1")) {
            view.setVisibility(View.GONE);
            return;
        }
        // isDurationCountdow = true -> cowDow đã đc khởi tạo
        if (biddingInformation.isDurationCountdow()) {
            view.setVisibility(View.GONE);
            return;
        }
        //getMax_confirm_time null return k chạy countdow
        if (biddingInformation.getMax_confirm_time() == null) {
            view.setVisibility(View.GONE);
            return;
        }
        if (isValid(biddingInformation)) {
            view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * set toDate->FromDate cho item Bidding
     *
     * @param view
     * @param fromTime
     * @param toTime
     */
    @BindingAdapter({"setTimeBidding", "toTime"})
    public static void setTimeBidding(TextView view, OdooDateTime fromTime, OdooDateTime toTime) {
        if (fromTime == null && toTime != null)
            view.setText(AppController.formatDateTime.format(toTime));
        if (fromTime != null && toTime == null)
            view.setText(AppController.formatDateTime.format(fromTime));
        if (fromTime != null && toTime != null)
            view.setText(AppController.formatDateTime.format(fromTime) + " - " + AppController.formatDateTime.format(toTime));
        if (fromTime == null && toTime == null)
            view.setText("");
    }

    @BindingAdapter({"status_vehicle"})
    public static void status_vehicle(TextView view, String approved_check) {
        if (VehicleStatus.ACCEPTED.equals(approved_check)) {
            view.setText(view.getResources().getString(R.string.accepted));
        } else if (VehicleStatus.REJECTED.equals(approved_check)) {
            view.setText(view.getResources().getString(R.string.rejected));
        } else {
            view.setText(view.getResources().getString(R.string.waiting));
        }
    }

    @SuppressLint("ResourceAsColor")
    @BindingAdapter("setStatusBidding")
    public static void setStatusBidding(TextView view, BiddingInformation biddingInformation) {
        if (biddingInformation == null) return;
        if (biddingInformation.getType().equals("1")) {
            if (biddingInformation.getType().equals("-1")) {
                view.setVisibility(View.VISIBLE);
                view.setTextColor(Color.parseColor("red"));
                view.setText(R.string.status_cancel);
            } else {
                if (biddingInformation.getStatus().equals("0")) {
                    view.setText((R.string.the_car_is_not_in_stock));
                } else if (biddingInformation.getStatus().equals("1")) {
                    view.setText((R.string.received_the_goods));
                } else if (biddingInformation.getStatus().equals("2")) {
                    view.setText((R.string.has_returned_the_goods));
                }


            }
            if (biddingInformation.getStatus() == null) {
                view.setText("");
            }
        } else {
            view.setVisibility(View.GONE);
        }

    }

    @BindingAdapter("setStatusBiddings")
    public static void setStatusBiddings(TextView view, BiddingInformation biddingInformation) {
        if (biddingInformation == null) return;
        if (biddingInformation.getType().equals("-1") || biddingInformation.getType().equals("0")) {
            view.setVisibility(View.VISIBLE);
            view.setText(biddingInformation.getType().equals("-1") ? view.getContext().getString(R.string.close_bidding) : view.getContext().getString(R.string.fill_in_vehicle_information));
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("setHideViewPriceBiddingClose")
    public static void setHideViewPriceBiddingClose(TextView view, BiddingInformation biddingInformation) {
        if (biddingInformation == null) return;
        if (biddingInformation.getType().equals("-1")) {
            view.setVisibility(View.VISIBLE);
            view.setText(AppController.getInstance().formatCurrency(biddingInformation.getPrice()));
        } else {
            view.setVisibility(View.GONE);
        }

    }

    public static Long cvDateToTimeMillis(Date maxdate) {
        long millis = maxdate.getTime();
        Calendar calendar = Calendar.getInstance();
        long timeNow = calendar.getTimeInMillis();
        long maxDate = millis - timeNow;
        return maxDate;
    }

    @SuppressLint("DefaultLocale")
    private static String hmsTimeFormatter(long milliSeconds) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
    }

    private static boolean isValid(BiddingInformation biddingInformation) {
        if (biddingInformation.getBidding_type() == null)
            return true;
        if (biddingInformation.getBidding_type().equals("1") && biddingInformation.getCargo_status().equals("1"))
            return false;
        if (biddingInformation.getBidding_type().equals("2") && biddingInformation.getCargo_status().equals("2"))
            return false;
        return true;
    }

    @SuppressLint("NewApi")
    @BindingAdapter("setSelectItem")
    public static void setSelectItem(TextView view, BiddingTimePackage biddingTimePackage) {
        switch (view.getId()) {
            case R.id.tvTime:
                (view).setText(AppController.convertDateToTime(biddingTimePackage.getCalendar()));
                if (biddingTimePackage.isSelect())
                    view.setTextColor(view.getContext().getColor(R.color.colorApp));
                else view.setTextColor(view.getContext().getColor(R.color.color_normal_text));
                break;
            case R.id.tvStatus:
                if (biddingTimePackage.isSelect())
                    view.setTextColor(view.getContext().getColor(R.color.colorApp));
                else view.setTextColor(view.getContext().getColor(R.color.color_normal_text));
                if (AppController.getDayInDate(biddingTimePackage.getCalendar()).equals(AppController.getDayNow())) {
                    // position = 0 & calendar nhỏ nhất sẽ là case đang diễn ra-> Đang diễn ra
                    // còn lại sẽ là case sắp diễn ra
                    if (biddingTimePackage.isFirst()) {
                        long timeNow = System.currentTimeMillis();
                        long timeBid = AppController.cvDateToTimeMillis(biddingTimePackage.getCalendar());
                        // position = 0 & time now < time của bid thì sẽ là sắp bắt đầu
                        //--------------- time now > time của bid thì sẽ là đang diễn ra
                        if (timeNow < timeBid) (view).setText(R.string.upcoming);
                        else (view).setText(R.string.happenning);
                    } else (view).setText(R.string.upcoming);
                } else {
                    // case calendar lớn hơn ngày hiện tại set status bằng ngày và tháng của calendar response
                    String dayMoth = AppController.getDayInDate(biddingTimePackage.getCalendar()) + "-" + AppController.getMothInDate(biddingTimePackage.getCalendar());
                    (view).setText(dayMoth);
                }
                break;
            case R.id.tvView:
                if (biddingTimePackage.isSelect())
                    view.setBackgroundColor(view.getContext().getColor(R.color.colorApp));
                else view.setBackgroundColor(view.getContext().getColor(R.color.coloBgApp));
                break;
        }
    }

    @SuppressLint("NewApi")
    @BindingAdapter("setLicensePlatesCargo")
    public static void setLicensePlatesCargo(TextView tv, VehicleCargo vehicleCargo) {
        if (vehicleCargo.getLisence_plate().isEmpty()) return;

        String total_cargo = vehicleCargo.getTonnage() != null ? " | " + vehicleCargo.getTonnage() + " tấn" : "";
        String fulltext = tv.getContext().getString(R.string.license_plates) + " " + vehicleCargo.getLisence_plate() + " " + total_cargo;
        setSpanStringColor(tv, fulltext, total_cargo, tv.getContext().getColor(R.color.item_off), 1);

    }

    /**
     * set listIDdCargo adpter
     *
     * @param linearLayout
     * @param
     */
    @BindingAdapter("setListIdCargo")
    public static void setListIdCargo(LinearLayout linearLayout, Object o) {
        linearLayout.removeAllViews();

        // set ListId của item Cargo
        if (o instanceof VehicleCargo) {
            VehicleCargo vehicleCargo = (VehicleCargo) o;
            if (vehicleCargo.getList_bidding_order() == null) {
                linearLayout.setVisibility(View.GONE);
                return;
            }
            for (int i = 0; i < vehicleCargo.getList_bidding_order().size(); i++) {

                addItemChill(linearLayout, vehicleCargo.getList_bidding_order().get(i));
            }
        }
        // set số lượng quality và type của bidding
        if (o instanceof BiddingInformation) {
            BiddingInformation biddingInformation = (BiddingInformation) o;
            // case bidding bị hủy sẽ k có thông tin quality và type của bidding
            if (biddingInformation.getType().equals("-1")) return;
            for (int i = 0; i < biddingInformation.getBidding_vehicles().size(); i++) {
                for (int j = 0; j < biddingInformation.getBidding_vehicles().get(i).getCargo_types().size(); j++) {
                    CargoTypes cargoType = biddingInformation.getBidding_vehicles().get(i).getCargo_types().get(j);
                    addItemChill(linearLayout, cargoType);
                }
            }
        }
        if (o instanceof BiddingPackage) {
            BiddingPackage biddingPackage = (BiddingPackage) o;
            if (biddingPackage.getCargo_types() == null) return;
            for (CargoTypes cargoTypes : biddingPackage.getCargo_types()) {
                addItemChill(linearLayout, cargoTypes);
            }
        }
        // set số

    }

    /**
     * set date và trạng thái itemBidding của list danh sách Cargo
     *
     * @param textView
     * @param viewModel
     */
    @SuppressLint("NewApi")
    @BindingAdapter({"setDateSatatusBiddingCargo"})
    public static void setDateStatusBiddingCargo(TextView textView, BiddingInformation viewModel) {
        if (viewModel == null) return;
        StringBuilder stringBuilder = new StringBuilder((textView.getContext().getString(R.string.bidding)));
        stringBuilder.append(" " + AppController.formatDateTime.format(viewModel.getCreate_date()));

        if (viewModel.getType().equals("-1")) {
            stringBuilder.append(textView.getContext().getString(R.string.cancelled_bidding));
            setSpanStringColor(textView, stringBuilder.toString(), textView.getContext().getString(R.string.cancelled_bidding), textView.getContext().getColor(R.color.colorRed), 0);
            return;
        } else if (viewModel.getType().equals("1")) {
            if (viewModel.getStatus().equals("0"))
                stringBuilder.append(textView.getContext().getString(R.string.not_yet_shipped));
            if (viewModel.getStatus().equals("1"))
                stringBuilder.append(textView.getContext().getString(R.string.being_transported));
            if (viewModel.getStatus().equals("2"))
                stringBuilder.append(textView.getContext().getString(R.string.bidding_done));
        }

        textView.setText(stringBuilder.toString());
    }

    @SuppressLint("NewApi")
    @BindingAdapter({"setStatusNotification"})
    public static void setStatusNotification(TextView textView, NotificationModel notificationModel) {
        if (notificationModel.is_read())
            textView.setTextColor(textView.getContext().getColor(R.color.hint_text_color));
        else textView.setTextColor(textView.getContext().getColor(R.color.color_normal_text));
    }

    @SuppressLint("NewApi")
    @BindingAdapter({"setBackgronudNotification"})
    public static void setBackgronudNotification(ConstraintLayout view, NotificationModel notificationModel) {
        if (notificationModel.is_read())
            view.setBackgroundColor(view.getContext().getColor(R.color.colorWhite));
        else view.setBackgroundColor(view.getContext().getColor(R.color.backgroud_notifi));
    }

    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, OdooDateTime time) {
        if (time != null) {
            if ((System.currentTimeMillis() - time.getTime()) < 60 * 1000) {
                view.setText(R.string.now);
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        time.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        }
    }


    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.not_img));
            return;
        }
        loadImage(view, imageUrl, view.getContext());
    }

    @BindingAdapter({"imageUrl", "hintIcon"})
    public static void loadImage(ImageView view, String imageUrl, Drawable hintIcon) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(hintIcon);
            return;
        }
        loadImage(view, imageUrl, view.getContext());
    }

    private static void loadImage(ImageView imgReward, String icon, Context context) {
//        GlideUrl url = new GlideUrl(AppController.HTTP + icon, new LazyHeaders.Builder()
//                .addHeader("Cookie", StaticData.sessionCookie)
//                .build());
        Glide.with(context)
                .load(icon)
                .error(R.drawable.not_img)
                .into(imgReward);
    }

    private static void addItemChill(LinearLayout linearLayout, Object o) {
        linearLayout.setVisibility(View.VISIBLE);
        String value = "";
        LayoutInflater mInflater = LayoutInflater.from(linearLayout.getContext());
        View newsListRow = mInflater.inflate(R.layout.item_total_cargo_info, null, false);
        TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);
        if (o instanceof VehicleCargo.BiddingOrder) {
            VehicleCargo.BiddingOrder biddingOrder = (VehicleCargo.BiddingOrder) o;
            value = "ID: " + biddingOrder.getBidding_order_number();
        }
        if (o instanceof CargoTypes) {
            CargoTypes cargoTypes = (CargoTypes) o;
            String quality = cargoTypes.getCargo_quantity() + " cargo";
            String type = cargoTypes.getType();
            value = quality + " " + type;
        }
        textView.setText(value);
        linearLayout.addView(newsListRow);
    }

    private static void setSpanStringColor(TextView view, String fulltext, String subtext, int color, int size) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE);
        Spannable str = (Spannable) view.getText();
        int i = fulltext.indexOf(subtext);
        str.setSpan(new ForegroundColorSpan(color), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (size != 0)
            str.setSpan(new RelativeSizeSpan(0.8f), i, i + subtext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }
    //--------------------------------------------------------- RATE ADAPTER -----------------------------------------------------------

    @SuppressLint("NewApi")
    @BindingAdapter({"setIdVehiclesRate"})
    public static void setIdVehiclesRate(TextView textView, RateModel rateModel) {
        StringBuilder id = new StringBuilder("ID: ");
        if (rateModel == null) return;
        String numberOder = (rateModel.getBiddingOrder().getBiddingOrderNumber() != null ? rateModel.getBiddingOrder().getBiddingOrderNumber() : "");
        String distance = (rateModel.getDistance() != null ? rateModel.getDistance() : 0.0) + " km";
        String price = AppController.getInstance().formatCurrency(rateModel.getPrice());
        id.append(numberOder).append(" - ").append(distance).append("- ").append(price);
        setSpanStringColor(textView, id.toString(), price, textView.getContext().getColor(R.color.colorApp), 0);
    }

    @BindingAdapter({"setNameDriver"})
    public static void setNameDriver(TextView textView, RateModel.BiddingVehicle biddingVehicle) {
        StringBuilder name = new StringBuilder("BKS: ");
        if (biddingVehicle == null) return;
        String bks = (biddingVehicle.getLisencePlate() != null ? biddingVehicle.getLisencePlate() : "");
        String diverName = (biddingVehicle.getDriverName() != null ? biddingVehicle.getDriverName() : "");
        name.append(bks).append(" - ").append(diverName);
        textView.setText(name.toString());
    }

    @BindingAdapter({"setListCargo"})
    public static void setListCargo(LinearLayout linearLayout, RateModel.BiddingVehicle biddingVehicle) {
        linearLayout.removeAllViews();
        if (biddingVehicle.getCargoTypes() == null || biddingVehicle.getCargoTypes().size() == 0) {
            linearLayout.setVisibility(View.GONE);
            return;
        }
        for (int i = 0; i < biddingVehicle.getCargoTypes().size(); i++) {
            addItemChillRate(linearLayout, biddingVehicle.getCargoTypes().get(i));
        }
    }

    @BindingAdapter({"setImg"})
    public static void setImg(LinearLayout linearLayout, RateModel.BiddingVehicle biddingVehicle) {
        linearLayout.removeAllViews();
        if (biddingVehicle.getRating() == null || biddingVehicle.getRating().getImages().size() == 0) {
            linearLayout.setVisibility(View.GONE);
            return;
        }
        for (int i = 0; i < biddingVehicle.getRating().getImages().size(); i++) {
            if (i < 3)
                addItemImg(linearLayout, biddingVehicle.getRating().getImages().get(i), i, biddingVehicle.getRating().getImages().size());
        }
    }

    @BindingAdapter({"setListComment"})
    public static void setListComment(LinearLayout linearLayout, RateModel.BiddingVehicle biddingVehicle) {
        linearLayout.removeAllViews();
        if (biddingVehicle.getRating() == null || biddingVehicle.getRating().getRatingBadges() == null)
            return;
        for (int i = 0; i < biddingVehicle.getRating().getRatingBadges().size(); i++) {
            addItemChillComment(linearLayout, biddingVehicle.getRating().getRatingBadges().get(i));
        }
    }

    private static void addItemChillComment(LinearLayout linearLayout, RateModel.RatingBadge o) {
        String value = "";
        LayoutInflater mInflater = LayoutInflater.from(linearLayout.getContext());
        // xử lý tạo list cargo

        View newsListRow = mInflater.inflate(R.layout.item_comment_rate, null, false);
        TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);

        value = o.getName();

        textView.setText(value);
        linearLayout.addView(newsListRow);
    }

    private static void addItemImg(LinearLayout linearLayout, String url, int position, int size) {
        ImageRate imageRate = new ImageRate(linearLayout.getContext());
        loadImage(imageRate.getmImageView(), url);
        if (position == 2)
            imageRate.setTexMore((size - 2));
        linearLayout.addView(imageRate);
    }

    private static void addItemChillRate(LinearLayout linearLayout, Object o) {
        String value = "";
        LayoutInflater mInflater = LayoutInflater.from(linearLayout.getContext());
        // xử lý tạo list cargo
        if (o instanceof RateModel.CargoType) {

            View newsListRow = mInflater.inflate(R.layout.item_total_cargo_info, null, false);
            TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);

            RateModel.CargoType biddingOrder = (RateModel.CargoType) o;
            String quanitity = (biddingOrder.getCargoQuantity() != null ? String.valueOf(biddingOrder.getCargoQuantity()) : "");
            String type = (biddingOrder.getType() != null ? biddingOrder.getType() : "");
            value = quanitity + " " + type;

            textView.setText(value);
            linearLayout.addView(newsListRow);
        }
    }

    @BindingAdapter("countdownTimer")
    public static void countdownTimer(CountDownClock countDownClock, OdooDateTime odooDateTime) {
        if (odooDateTime != null) {
            long durationMillisecond = odooDateTime.getTime() - System.currentTimeMillis();
            countDownClock.startCountDown(durationMillisecond);
            countDownClock.setCountdownListener(new CountDownClock.CountdownCallBack() {
                @Override
                public void countdownAboutToFinish() {

                }

                @Override
                public void countdownFinished() {

                }
            });
        }
    }

    @BindingAdapter("child_recycleView")
    public static void child_recycleView(RecyclerView view, List<BiddingOrder> list) {
        if (list != null) {
            XBaseAdapter xBaseAdapter = new XBaseAdapter(R.layout.item_cargo_name, list, new AdapterListener() {
                @Override
                public void onItemClick(View view, Object o) {

                }

                @Override
                public void onItemLongClick(View view, Object o) {

                }
            });
            view.setAdapter(xBaseAdapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.HORIZONTAL, false);
            view.setLayoutManager(layoutManager);
        }
    }
}
