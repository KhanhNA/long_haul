package com.nextsolutions.longhold.ui.fragment;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.viewmodel.ManagerVehicleVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class ManagerVehicleFragment extends BaseFragment {




    @Override
    public int getLayoutRes() {
        return R.layout.managet_vehicle_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ManagerVehicleVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
