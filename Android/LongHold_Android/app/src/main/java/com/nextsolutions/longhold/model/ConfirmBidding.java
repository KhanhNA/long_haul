package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmBidding extends BaseModel {
    private Integer id;
    private String cargo_number;
    private String status;
    private Integer bidding_order_id;
    private String confirm_time;
    private String time_countdown;
}
