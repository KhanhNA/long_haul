package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingVehicles extends BaseModel {
    private int id;
    private String driver_name;
    private String code;
    private String lisence_plate;
    private String driver_phone_number;
    private String expiry_time;
    private Integer vehicle_type;
    private String status;
    private String description;
    private String tonnage;
    private String image_128;
    private String id_card;
    private List<CargoTypes> cargo_types;
    private boolean isSelected = false;

    public boolean isChecked() {
        return isSelected;
    }

    public void setChecked(boolean checked) {
        isSelected = checked;
    }

    private boolean isSelectAll = false;

    public boolean isCheckedAll() {
        return isSelectAll;
    }

    public void setCheckedAll(boolean checkedAll) {
        isSelectAll = checkedAll;
    }


}
