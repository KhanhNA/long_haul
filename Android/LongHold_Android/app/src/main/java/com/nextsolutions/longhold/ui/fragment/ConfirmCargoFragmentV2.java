package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.ConfirmCargoFragmentV2Binding;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.VehicleDriver;
import com.nextsolutions.longhold.model.VehicleDriverId;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.ConfirmCargoVMV2;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

public class ConfirmCargoFragmentV2 extends BaseActivity<ConfirmCargoFragmentV2Binding> {
    private BiddingPackage biddingPackage;

    private ConfirmCargoVMV2 confirmCargoVM;
    private XBaseAdapter adapter;
    private DialogConfirm dialogDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        confirmCargoVM = (ConfirmCargoVMV2) viewModel;
        getData();
        initView();
    }

    private void initView() {
        adapter = new XBaseAdapter(R.layout.item_vehicle_driver, confirmCargoVM.getVehicleDrivers(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                showOptionVehicleDriver(view, (VehicleDriver) o);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        binding.rcVehicleDriver.setAdapter(adapter);

        binding.btnAddNewItem.setOnClickListener(v -> addVehicleDriver());
        binding.addVehicle.setOnClickListener(v -> addVehicleDriver());

        binding.btnConfirm.setOnClickListener(v -> {
            confirmCargoVM.confirmVehicleDriverInfo(this::runUi);
        });

        binding.toolbar.setNavigationOnClickListener(v -> onBackPressed());

        binding.btnChooseLater.setOnClickListener(v -> onBackPressed());

    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.CONFIRM_ERROR:
                ToastUtils.showToast(this, getString(R.string.something_wrong), getResources().getDrawable(R.drawable.ic_clear));
                break;
            case Constants.CONFIRM_SUCCESS:
                ToastUtils.showToast(this, getString(R.string.bidding_success), getResources().getDrawable(R.drawable.ic_check));
                finish();
                break;
        }
    }


    public void addVehicleDriver() {
        AddVehicleDriverDialog addVehicleDriverDialog = new AddVehicleDriverDialog(null
                , confirmCargoVM.getSelectedDrivers()
                , confirmCargoVM.getSelectedVehicles()
                , vehicleDriver -> {
            confirmCargoVM.getVehicleDrivers().add(0, vehicleDriver);
            confirmCargoVM.getVehicleDriverIds().add(0, new VehicleDriverId(vehicleDriver.getVehicle().getId(), vehicleDriver.getDriver().getId()));
            adapter.notifyItemInserted(0);
            confirmCargoVM.getSelectedDrivers().put(vehicleDriver.getDriver().getId(), vehicleDriver.getDriver());
            confirmCargoVM.getSelectedVehicles().put(vehicleDriver.getVehicle().getId(), vehicleDriver.getVehicle());
        });
        addVehicleDriverDialog.show(getSupportFragmentManager(), addVehicleDriverDialog.getTag());
    }

    @SuppressLint("RestrictedApi")
    private void showOptionVehicleDriver(View view, VehicleDriver o) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_add_vehicle, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(this, (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    editVehicleDriver(o);
                    break;
                case R.id.action_del:
                    showDialogDelete(o);
                    break;
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void showDialogDelete(VehicleDriver o) {
        dialogDelete = new DialogConfirm(getString(R.string.confirm_delete), "", v -> {
            confirmCargoVM.getVehicleDrivers().remove(o.index - 1);
            confirmCargoVM.getVehicleDriverIds().remove(o.index - 1);
            adapter.notifyItemRemoved(o.index - 1);
            //xóa xe và tài xế đã chọn
            confirmCargoVM.getSelectedDrivers().remove(o.getDriver().getId());
            confirmCargoVM.getSelectedVehicles().remove(o.getVehicle().getId());

            dialogDelete.dismiss();
        });
        dialogDelete.show(getSupportFragmentManager(), dialogDelete.getTag());
    }

    private void editVehicleDriver(VehicleDriver o) {
        AddVehicleDriverDialog addVehicleDriverDialog = new AddVehicleDriverDialog(o
                , confirmCargoVM.getSelectedDrivers()
                , confirmCargoVM.getSelectedVehicles()
                , vehicleDriver -> {
            confirmCargoVM.getVehicleDrivers().set(o.index - 1, vehicleDriver);
            confirmCargoVM.getVehicleDriverIds().get(o.index - 1)
                    .setDriver_id(vehicleDriver.getDriver().getId())
                    .setVehicle_id(vehicleDriver.getVehicle().getId());

            adapter.notifyItemChanged(o.index - 1);
            //xóa xe và tài xế đã chọn
            confirmCargoVM.getSelectedDrivers().remove(o.getDriver().getId());
            confirmCargoVM.getSelectedVehicles().remove(o.getVehicle().getId());
            //thêm xe và tài xế mới sửa
            confirmCargoVM.getSelectedDrivers().put(vehicleDriver.getDriver().getId(), vehicleDriver.getDriver());
            confirmCargoVM.getSelectedVehicles().put(vehicleDriver.getVehicle().getId(), vehicleDriver.getVehicle());
        });
        addVehicleDriverDialog.show(getSupportFragmentManager(), addVehicleDriverDialog.getTag());
    }

    private void getData() {
        biddingPackage = (BiddingPackage) getIntent().getSerializableExtra("BIDDING_PACKAGE");
        if (biddingPackage == null) {
            biddingPackage = new BiddingPackage();
            int totalCargo = getIntent().getIntExtra("TOTAL_CARGO", 0);
            double totalWeight = getIntent().getDoubleExtra("TOTAL_WEIGHT", 0);
            int biddingOrderId = getIntent().getIntExtra("BIDDING_ORDER_ID", -1);
            OdooDateTime maxConfirmTime = (OdooDateTime) getIntent().getSerializableExtra("MAX_CONFIRM_TIME");
            biddingPackage.setTotal_cargos(totalCargo);
            biddingPackage.setTotal_weight(totalWeight);
            biddingPackage.setBidding_order_id(biddingOrderId);
            biddingPackage.setMax_confirm_time(maxConfirmTime);
        }
        confirmCargoVM.getObjBiddingPackage().set(biddingPackage);

    }

    @Override
    public int getLayoutRes() {
        return R.layout.confirm_cargo_fragment_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmCargoVMV2.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
