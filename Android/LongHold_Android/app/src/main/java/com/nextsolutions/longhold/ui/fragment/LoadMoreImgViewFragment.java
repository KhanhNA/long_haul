package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.LoadMoreFragmentBinding;
import com.nextsolutions.longhold.model.RateModel;
import com.nextsolutions.longhold.ui.viewcommon.PagerAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;

import static com.nextsolutions.longhold.base.Constants.DATA_INFORMATION;
import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.TIME;

public class LoadMoreImgViewFragment extends BaseFragment {
    private LoadMoreFragmentBinding moreFragmentBinding;
    private RateModel.BiddingVehicle biddingVehicle;
    private PagerAdapter mPageAdapter;

    @Override
    public int getLayoutRes() {
        return R.layout.load_more_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        moreFragmentBinding = (LoadMoreFragmentBinding) binding;
        moreFragmentBinding.btnBack.setOnClickListener(v -> getActivity().finish());
        getData();
        initViewPage();
        listenerView();
        return view;
    }

    private void listenerView() {
        moreFragmentBinding.btnBack.setOnClickListener(v -> getActivity().finish());
        int size = biddingVehicle.getRating().getImages().size();
        moreFragmentBinding.tvCurrent.setText("1/"+size);

    }


    private void getData() {
        biddingVehicle = (RateModel.BiddingVehicle) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(DATA_INFORMATION);
    }

    private void initViewPage() {
        mPageAdapter = new PagerAdapter(getChildFragmentManager());
        int size = biddingVehicle.getRating().getImages().size();
        for (int i = 0; i < size; i++) {
            String img = biddingVehicle.getRating().getImages().get(i);
            mPageAdapter.addFragment(createFragment(img));
        }
        moreFragmentBinding.viewPagerTimePackage.setAdapter(mPageAdapter);
        mPageAdapter.notifyDataSetChanged();
        moreFragmentBinding.viewPagerTimePackage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                moreFragmentBinding.tvCurrent.setText((position+1)+"/"+size);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private Fragment createFragment(String url) {
        ImageFragment imageFragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_PAGE, url);
        imageFragment.setArguments(bundle);
        return imageFragment;

    }
}
