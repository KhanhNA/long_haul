package com.nextsolutions.longhold.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformationUser {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("zip")
    @Expose
    private Object zip;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("date")
    @Expose
    private Object date;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("tz")
    @Expose
    private String tz;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("employee")
    @Expose
    private Object employee;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("state_id")
    @Expose
    private Object stateId;
    @SerializedName("country_id")
    @Expose
    private Object countryId;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("mobile")
    @Expose
    private Object mobile;
    @SerializedName("color")
    @Expose
    private Integer color;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("staff_type")
    @Expose
    private Object staffType;
    @SerializedName("city_name")
    @Expose
    private Object cityName;
    @SerializedName("store_fname")
    @Expose
    private String storeFname;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    private int company_num_rate;
    private String uri_path;


}