package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.ConfirmCargoFragmentBinding;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.ui.dialog.DialogConfirm;
import com.nextsolutions.longhold.ui.dialog.DialogSelectCargo;
import com.nextsolutions.longhold.ui.viewcommon.CustomToast;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.ConfirmCargoVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import static com.nextsolutions.longhold.base.Constants.BIDDING_ID;
import static com.nextsolutions.longhold.base.Constants.BIDDING_VEHICLES;
import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.TIME;

public class ConfirmCargoFragment extends BaseFragment implements ActionsListener, AdapterListener {

    private DialogSelectCargo mDialogSelectCargo;
    private ConfirmCargoFragmentBinding mBinding;
    private ConfirmCargoVM confirmCargoVM;
    private XBaseAdapter adapter;
    private DialogConfirm mDialogConfirm;
    private CountDownTimer countDownTimer;
    private String bidingId;
    private int biddingID;
    private boolean check;
    private int mPosition;
    private BiddingInformation mBiddingInformation;
    private ArrayList<Long> mCountDowTimeMillisecond = new ArrayList<>();
    private ArrayList<BiddingVehicles> listIsSelected = new ArrayList<>();
    private ArrayList<BiddingVehicles> listDriverCargo = new ArrayList<>();

    @Override
    public int getLayoutRes() {
        return R.layout.confirm_cargo_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmCargoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (ConfirmCargoFragmentBinding) binding;
        confirmCargoVM = (ConfirmCargoVM) viewModel;
        confirmCargoVM.listBiddingVehicle(this::runUI);
        getData();
        showListCargoIsSelected();
        initView();


        return v;
    }

    private void initView() {
        if (mBiddingInformation != null) {
            mBinding.tvId.setText("ID : " + mBiddingInformation.getBidding_order_number() + " - " + mBiddingInformation.getDistance() + " km");
            mBinding.tvId.setVisibility(View.VISIBLE);
            mBinding.imgView.setVisibility(View.GONE);
            mBinding.txtBidSuccess.setVisibility(View.GONE);
        }

    }

    private void getData() {
        biddingID = getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getInt(BIDDING_ID);
        mCountDowTimeMillisecond = (ArrayList<Long>) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(TIME);
        mPosition = getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getInt(Constants.POSITION);
        mBiddingInformation = (BiddingInformation) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(Constants.DATA_INFORMATION);
        listIsSelected = (ArrayList<BiddingVehicles>) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(BIDDING_VEHICLES);

        hideRecyclerView();
        countDowTime(mPosition);


    }

    private void hideRecyclerView() {
        if (listIsSelected != null && listIsSelected.size() > 0) {
            mBinding.rvSelectDriverCargo.setVisibility(View.VISIBLE);
            mBinding.ctrAddCargo.setVisibility(View.GONE);
            for (BiddingVehicles biddingVehicles : listIsSelected) {
                biddingVehicles.setChecked(!biddingVehicles.isSelected());
                listDriverCargo.add(biddingVehicles);
            }
        } else {
            listIsSelected = new ArrayList<>();
            mBinding.rvSelectDriverCargo.setVisibility(View.GONE);
            mBinding.ctrAddCargo.setVisibility(View.VISIBLE);
        }
    }

    /**
     * hiển thị listview danh sách lái xe được chọn
     */
    private void showListCargoIsSelected() {
        adapter = new XBaseAdapter(R.layout.item_delete_cargo, listIsSelected, this);
        mBinding.rvSelectDriverCargo.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.rvSelectDriverCargo.setAdapter(adapter);
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        switch (v.getId()) {
            case R.id.btnAddCargo:
            case R.id.imgAddCargo:
                addDialog(listDriverCargo);
                break;
            case R.id.btnCancel:
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.btnConfirm:
                if (listIsSelected.size() > 0) {
                    callApiSuccess();
                } else {
                    ToastUtils.showToast(getString(R.string.no_select_driver_cargo));
                }
                break;
        }
    }

    private void callApiSuccess() {
        StringBuilder builder = new StringBuilder();

        for (BiddingVehicles biddingVehicles : listIsSelected) {
            builder.append(biddingVehicles.getId() + ",");
        }

        builder.deleteCharAt(builder.lastIndexOf(","));
        String idCar = builder.toString();
        if ("".equals(idCar)) {
            ToastUtils.showToast(getString(R.string.no_select_driver_cargo));
        } else {
            if (mBiddingInformation != null) {
                confirmCargoVM.addDriverCargo(String.valueOf(mBiddingInformation.getId()), idCar, this::runUI);
            } else {
                confirmCargoVM.addDriverCargo(String.valueOf(biddingID), idCar, this::runUI);
            }
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.itemClick:
                deleteCargo((BiddingVehicles) o);
                break;
        }
    }

    private void deleteCargo(BiddingVehicles biddingVehicles) {
        mDialogConfirm = new DialogConfirm(Html.fromHtml(getString(R.string.dialog_confirm_title1)) + " " + biddingVehicles.getLisence_plate() + " " + getString(R.string.dialog_confirm_title2), null, "Xác nhận", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCargoVM.deleteCago(biddingVehicles,listIsSelected,ConfirmCargoFragment.this::runUI);
                mDialogConfirm.dismiss();
            }
        });
        mDialogConfirm.show(getFragmentManager(), "tag");
    }

    // TODO: 8/19/2020 show dialog chonj taif xe
    private void addDialog(ArrayList<BiddingVehicles> listDriverCargo) {
        mDialogSelectCargo = new DialogSelectCargo(getActivity(), getContext(), listDriverCargo, new DialogSelectCargo.OnButtonClicked() {
            @Override
            public void onClickButtonSelectAccount(ArrayList<BiddingVehicles> mListSelect) {
                showDataRecycleView();
                confirmCargoVM.upDateDataListSelect(mListSelect,listIsSelected,ConfirmCargoFragment.this::runUI);
                mDialogSelectCargo.dismiss();
            }
        });
        mDialogSelectCargo.setCloseCallback(new DialogSelectCargo.IDialogSelectErrorCallback() {
            @Override
            public void close() {
                mDialogSelectCargo.dismiss();
            }
        });
        mDialogSelectCargo.show(getChildFragmentManager(), "");
    }

    private void showDataRecycleView() {
        mBinding.rvSelectDriverCargo.setVisibility(View.VISIBLE);
        mBinding.ctrAddCargo.setVisibility(View.GONE);
    }

    private void runUI(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_LIST_DRIVER_CARGO:
                for (BiddingVehicles cg : confirmCargoVM.getListDriverCargo().getValue().getRecords()) {
                    listDriverCargo.add(cg);
                }

            case Constants.GET_LIST_DRIVER_CARGO_ERROR:
                break;

            case Constants.CONFIRM_SUCCESS:
                confirmCargoSuccess();
                break;
            case Constants.CONFIRM_ERROR:
                ToastUtils.showToast("Lỗi ");
                break;

            case Constants.UPDATE_ADAPTER:
                adapter.setUpDateDat(listIsSelected);
                break;
        }
    }

    private void confirmCargoSuccess() {
        CustomToast.makeText(getContext(), getString(R.string.bidding_success), CustomToast.LENGTH_LONG, CustomToast.SUCCESS, true).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().onBackPressed();
            }
        }, 3000);
    }

    public void countDowTime(int position) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (mCountDowTimeMillisecond != null) {
            long dateNow = mCountDowTimeMillisecond.get(position);
            long dateNext = (position == mCountDowTimeMillisecond.size() - 1 ? 0 : mCountDowTimeMillisecond.get(position + 1));
            countDownTimer = new CountDownTimer(dateNext - dateNow, 1000) {
                public void onTick(long millisUntilFinished) {
                    mBinding.tvInfo.setVisibility(View.VISIBLE);
                    mBinding.tvCountDownTime.setText(AppController.msTimeFormatter(millisUntilFinished) + " s");
                }

                public void onFinish() {
                    mBinding.tvInfo.setVisibility(View.VISIBLE);
                    mBinding.tvCountDownTime.setText("00:00:00" + " s");
                }
            }.start();
        }
    }

}
