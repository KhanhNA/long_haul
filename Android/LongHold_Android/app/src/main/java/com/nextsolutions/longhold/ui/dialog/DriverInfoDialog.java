package com.nextsolutions.longhold.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputLayout;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.UpdateDriverInfoBinding;
import com.nextsolutions.longhold.model.Country;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.ui.fragment.GenderDialog;
import com.nextsolutions.longhold.utils.GlideEngine;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.UpdateDriverInfoVM;

import java.util.List;

public class DriverInfoDialog extends DialogFragment {
    UpdateDriverInfoBinding mBinding;
    private boolean isCheckEdit;

    private Driver mDriver;
    private Context mContext;
    private UpdateDriverInfoVM mUpdateDriverInfoVM;

    public DriverInfoDialog(Driver mDriver, Context mContext) {
        this.mDriver = mDriver;
        this.mContext = mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.update_driver_info, container, false);
        mUpdateDriverInfoVM = ViewModelProviders.of(this).get(UpdateDriverInfoVM.class);
        mBinding.setViewModel(mUpdateDriverInfoVM);
        mBinding.setViewHolder(mDriver);
        mBinding.setListener(DriverInfoDialog.this);

        mBinding.ivBack.setOnClickListener(v -> dismiss());

        mBinding.btnSaveInfo.setOnClickListener(v -> updateInfoDriver());
        mBinding.tvUpdate.setOnClickListener(v -> clickChangeText());
        mBinding.imgAvatar.setOnClickListener(v -> pickImage((ImageView) v));
        mBinding.etGender.setOnClickListener(v -> showSelectGenderDialog());

        mUpdateDriverInfoVM.getInfoDriver().set(mDriver);

        mUpdateDriverInfoVM.getCountries(this::runUi);

        configView();

        return mBinding.getRoot();
    }

    private void showSelectGenderDialog() {
        GenderDialog dialog = new GenderDialog(mUpdateDriverInfoVM.getInfoDriver().get().getGender(), (gender, genderName) -> {
            mUpdateDriverInfoVM.getInfoDriver().get().setGender(gender);
            mBinding.etGender.setText(genderName);
            mBinding.etGender.setError(null);
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    @SuppressLint("NonConstantResourceId")
    private void pickImage(ImageView imageView) {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.SINGLE)
                .forResult(new OnResultCallbackListener() {
                    @Override
                    public void onResult(List result) {
                        if (result != null && result.size() > 0) {
                            LocalMedia localMedia = ((LocalMedia) result.get(0));
                            switch (imageView.getId()) {
                                case R.id.imgAvatar:
                                    mUpdateDriverInfoVM.getIsChangeAvatar().set(true);
                                    mUpdateDriverInfoVM.getInfoDriver().get().setImage_1920(localMedia.getCompressPath());
                                    break;
                            }
                            mUpdateDriverInfoVM.getInfoDriver().notifyChange();
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    private void clickChangeText() {
        isCheckEdit = !isCheckEdit;
        configView();
    }

    private void updateInfoDriver() {
        mUpdateDriverInfoVM.updateInfoDriver(this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.UPDATE_INFO_DRIVER_SUCCESS:
                ToastUtils.showToast(Constants.UPDATE_INFO_DRIVER_SUCCESS);
                break;
            case Constants.UPDATE_INFO_DRIVER_FAILED:
                ToastUtils.showToast(Constants.UPDATE_INFO_DRIVER_FAILED);
                break;
            case "getCountriesSuccess":
                List<Country> countryList = mUpdateDriverInfoVM.getCountries();
                Country country = countryList.get(mDriver.getCountry_id());

                mBinding.etCountry.setText(country.getName());
                break;
            case "getCountriesFail":
                Log.i(DriverInfoDialog.class.getName(), "runUi: ... get Country Failed");
                break;
            default:
                break;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.fullScreenDialog);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private void configView() {
        mBinding.etFullName.setEnabled(isCheckEdit);
        mBinding.etGender.setEnabled(isCheckEdit);
        mBinding.imgAvatar.setEnabled(isCheckEdit);
        if (isCheckEdit) {
            mBinding.txtFullName.setEndIconMode(TextInputLayout.END_ICON_CLEAR_TEXT);
            mBinding.txtAddress.setEndIconMode(TextInputLayout.END_ICON_CLEAR_TEXT);
            mBinding.etGender.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_down), null);
            mBinding.tvUpdate.setText(getString(R.string.cancel));
        } else {
            mBinding.txtFullName.setEndIconMode(TextInputLayout.END_ICON_NONE);
            mBinding.txtAddress.setEndIconMode(TextInputLayout.END_ICON_NONE);
            mBinding.etGender.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            mBinding.tvUpdate.setText("Edit info driver");
        }

        mBinding.btnSaveInfo.setVisibility(isCheckEdit ? View.VISIBLE : View.INVISIBLE);
    }
}
