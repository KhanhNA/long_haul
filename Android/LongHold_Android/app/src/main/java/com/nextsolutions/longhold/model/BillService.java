package com.nextsolutions.longhold.model;




import com.tsolution.base.BaseModel;

import androidx.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillService extends BaseModel {
    private Integer id;
    private String name;
    private String display_name;
//    private Integer service_type;
    private Double price;
    private String description;
    private String status;
    private String service_code;

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null)
            return false;
        return this.id.equals(((BillService) obj).id);
    }
}
