package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.HistoryPointChildFragmentBinding;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.HistoryPointChildVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import static com.nextsolutions.longhold.base.Constants.*;


public class HistoryPointChildFragment extends BaseFragment {
    BaseAdapterV3 adapter;
    HistoryPointChildFragmentBinding mBinding;
    HistoryPointChildVM historyPointChildVM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (HistoryPointChildFragmentBinding) binding;
        historyPointChildVM = (HistoryPointChildVM) viewModel;
        setUpRecycleView();
        initView();
        getData();
        initLoadMore();
        return view;
    }
    private void initView(){
        mBinding.swRefresh.setOnRefreshListener(() -> {
            onRefresh();
        });
    }
    public void onRefresh(){
        historyPointChildVM.getIsRefresh().set(true);
        historyPointChildVM.getData(false,this::runUi);
    }
    private void setUpRecycleView(){
        adapter = new BaseAdapterV3(R.layout.history_point_child_item,historyPointChildVM.getHistoryRatingPointDTOS(),this);
        mBinding.rcContent.setAdapter(adapter);
    }
    private void getData(){
        historyPointChildVM.getData(false,this::runUi);
    }
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action){
            case API_SUCCESS :
                adapter.getLoadMoreModule().loadMoreComplete();
                adapter.notifyDataSetChanged();
                break;
            case "noMore":
                adapter.getLoadMoreModule().loadMoreEnd();
                adapter.notifyDataSetChanged();
                break;
        }
    }
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            historyPointChildVM.loadMore(this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.history_point_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HistoryPointChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }
}
