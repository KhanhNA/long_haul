package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.ImageFragmentBinding;
import com.nextsolutions.longhold.utils.BindingAdapterUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class ImageFragment extends BaseFragment {
    ImageFragmentBinding mImageFragmentBinding;

    @Override
    public int getLayoutRes() {
        return R.layout.image_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mImageFragmentBinding = (ImageFragmentBinding) binding;
        String img = getArguments().getString(Constants.KEY_PAGE);
        BindingAdapterUtils.loadImage(mImageFragmentBinding.imgSet, img);
        return view;
    }
}
