package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.BaseAdapterV3;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.FragmentIncentivePackingBinding;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListDialogFragment extends BottomSheetDialogFragment {
    private final int itemLayout;
    private final AdapterListener listener;
    private final int title;
    private final String description;
    private final List<BaseModel> lstData;

    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, String description, List<BaseModel> lstData, AdapterListener adapterListener) {
        this.lstData = lstData;
        this.itemLayout = itemLayout;
        this.listener = adapterListener;
        this.title = title;
        this.description = description;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentIncentivePackingBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_incentive_packing, container, false);
        binding.txtTitle.setText(getString(title));
        if (description != null) {
            binding.txtMsg.setText(description);
        } else {
            binding.txtMsg.setVisibility(View.GONE);
        }

        XBaseAdapter adapter = new XBaseAdapter(itemLayout, lstData, listener);
        RecyclerView rcIncentive = binding.rcIncentiveProduct;
        rcIncentive.setAdapter(adapter);
        rcIncentive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinimumHeight(height / 2);

        return binding.getRoot();
    }

}