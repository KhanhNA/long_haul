package com.nextsolutions.longhold.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.RateFragmentBinding;
import com.nextsolutions.longhold.model.RateModel;
import com.nextsolutions.longhold.ui.adapter.VehiclesRateAdapter;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.VehiclesRateVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

import java.util.List;

public class RateFragment extends BaseFragment implements ActionsListener {
    private VehiclesRateVM mVehiclesRateVM;
    private RateFragmentBinding mRateFragmentBinding;
    private VehiclesRateAdapter mVehiclesRateAdapter;
    private int mOffset = 0;

    @Override
    public int getLayoutRes() {
        return R.layout.rate_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return VehiclesRateVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mVehiclesRateVM = (VehiclesRateVM) viewModel;
        mRateFragmentBinding = (RateFragmentBinding) binding;
        initView();
        getData();
        initRefreshData();
        initLoadMore();
        return view;
    }

    private void getData() {
        mVehiclesRateVM.getVehiclesRate(mOffset, this::runUi);
    }


    private void initView() {
        mRateFragmentBinding.recyclerVehicles.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mVehiclesRateAdapter = new VehiclesRateAdapter(R.layout.item_rate, mVehiclesRateVM.getListVehiclesRates().getValue().getRecords(), this);
        mRateFragmentBinding.recyclerVehicles.setAdapter(mVehiclesRateAdapter);
        if (AppController.getInformationUser() != null){
            mRateFragmentBinding.tvRate.setText(AppController.getInformationUser().getCompany_num_rate() + "/5");
        }
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mVehiclesRateAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mVehiclesRateAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mVehiclesRateAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mRateFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mRateFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void refreshData() {
        mVehiclesRateVM.clearData();
        mOffset = 0;
        getData();
    }

    private void loadMore() {
        int length = mVehiclesRateVM.getListVehiclesRates().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            getData();

        } else {
            //thông báo không còn gì để load
            mVehiclesRateAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void offRefreshing() {
        mRateFragmentBinding.swipeLayout.setRefreshing(false);
        mVehiclesRateAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        switch (s) {
            case Constants.SUCCESS_API:
                mOffset++;
                mVehiclesRateAdapter.setList(mVehiclesRateVM.getListVehiclesRates().getValue().getRecords());
                offRefreshing();
                break;
            case Constants.FAIL_API:
                offRefreshing();
                break;
        }

    }
    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mRateFragmentBinding.viewNotData.setVisibility(View.GONE);
            mRateFragmentBinding.recyclerVehicles.setVisibility(View.VISIBLE);
        } else {
            mRateFragmentBinding.viewNotData.setVisibility(View.VISIBLE);
            mRateFragmentBinding.recyclerVehicles.setVisibility(View.GONE);
        }
    }
    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        int id = v.getId();
        RateModel rateModel = (RateModel) o;
        int index = mVehiclesRateVM.getListVehiclesRates().getValue().getRecords().indexOf(rateModel);
        switch (id) {
            case R.id.btnLoadMore:
                mVehiclesRateVM.getListVehiclesRates().getValue().getRecords().get(index).setCheckShowAll(true);
                mVehiclesRateAdapter.setList(mVehiclesRateVM.getListVehiclesRates().getValue().getRecords());
                mVehiclesRateAdapter.notifyDataSetChanged();
                mRateFragmentBinding.recyclerVehicles.scrollToPosition(index);
                break;

            case R.id.viewGroup:
                mVehiclesRateVM.getListVehiclesRates().getValue().getRecords().get(index).setCheckShowAll(false);
                mVehiclesRateAdapter.setList(mVehiclesRateVM.getListVehiclesRates().getValue().getRecords());
                mVehiclesRateAdapter.notifyDataSetChanged();
                break;
        }
    }


    @Override
    public void action(Object... objects) {

    }
}
