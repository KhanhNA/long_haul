package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolutions.longhold.api.ConfirmCargoApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.Driver;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListDriverVM extends BaseViewModel {

    ObservableField<String> txtSearch = new ObservableField<>();
    ObservableBoolean isNotData = new ObservableBoolean();
    ObservableBoolean isLoading = new ObservableBoolean();
    List<Driver> listDriver = new ArrayList<>();
    ObservableField<String> approved_check = new ObservableField<>();
    Integer offset = 0;
    int total_records;

    public ListDriverVM(@NonNull Application application) {
        super(application);
        isLoading.set(true);
        isNotData.set(true);
    }

    public void getDrivers(boolean isLoadMore, RunUi runUi) {
        if (!isLoadMore) {
            offset = 0;
            listDriver.clear();
        }
        isLoading.set(true);
        ConfirmCargoApi.getListDrivers(offset, approved_check.get(), txtSearch.get(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                OdooResultDto<Driver> resultDto = (OdooResultDto<Driver>) o;
                if (resultDto != null && resultDto.getRecords() != null) {
                    total_records = resultDto.getTotal_record();
                    listDriver.addAll(resultDto.getRecords());
                    if (listDriver.size() > 0) {
                        isNotData.set(false);
                    } else {
                        isNotData.set(true);
                    }
                    runUi.run("getDriverSuccess");
                } else {
                    runUi.run("getDriverFail");
                    isNotData.set(true);
                }

            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                isNotData.set(true);
                runUi.run("getDriverFail");
            }
        });
    }

    public void getMoreDriver(RunUi runUi) {
        if (listDriver.size() < total_records) {
            offset += 1;
            getDrivers(true, runUi);
        } else {
            runUi.run(Constants.NO_MORE_DATA);
        }
    }
}
