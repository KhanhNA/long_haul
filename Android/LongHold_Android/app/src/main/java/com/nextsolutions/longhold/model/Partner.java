package com.nextsolutions.longhold.model;


import com.ns.odoolib_retrofit.model.OdooRelType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Partner {
    private Long id;
    private String name;
    private String display_name;
    private Boolean date;
    private String title;
    private String parent_id;
    private String parent_name;

    private String ref;
    private String lang;
    private Double active_lang_count;
    private String tz;
    private String tz_offset;
    private String vat;
    private String same_vat_partner_id;

    private String website;
    private String comment;

    private Double credit_limit;
    private boolean active;
    private String employee;
    private String
            function;
    private String type;
    private String street;
    private String street2;
    private String zip;
    private String city;
    private OdooRelType state_id;
    private OdooRelType country_id;
    private Double partner_latitude;
    private Double partner_longitude;
    private String email;
    private String email_formatted;
    private String phone;
    private String mobile;
    private String is_company;
//    private Integer industry_id;
    private String company_type;
    private OdooRelType company_id;
    private Double color;

    private String partner_share;
    private String contact_address;
    private String commercial_company_name;
    private String company_name;

    private String name_seq;
    private String im_status;
    private String date_localization;

    private String activity_state;
    private Long activity_user_id;
    private Long activity_type_id;
    private String activity_date_deadline;
    private String activity_summary;
    private String activity_exception_decoration;
    private String activity_exception_icon;
    private String message_is_follower;
    private String message_unread;
    private Double message_unread_counter;
    private String message_needaction;
    private Double message_needaction_counter;
    private String message_has_error;
    private Double message_has_error_counter;
    private Double message_attachment_count;
    private Long message_main_attachment_id;
    private String email_normalized;
    private boolean is_blacklisted;
    private Double message_bounce;

    private Long user_id;
    private String signup_token;
    private String signup_type;
    private String signup_expiration;
    private String signup_valid;
    private String signup_url;
    private Long partner_gid;
    private String additional_info;
    private String phone_sanitized;
    private String phone_blacklisted;
    private String message_has_sms_error;
    private String image_1920;
    private String image_1024;
    private String image_512;
    private String image_256;
    private String image_128;

    private String walletAcount;
    private Long walletId;//client id

}