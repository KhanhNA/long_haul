package com.nextsolutions.longhold.model;

import com.nextsolutions.longhold.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingInformation extends BaseModel {

    private Integer id;
    private String cargo_number;
    private String cargo_status;
    private String confirm_time;
    private OdooDateTime max_date;
    private String from_depot_id;
    private String to_depot_id;
    private String distance;
    private String size_id;
    private String quantity;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private OdooDateTime create_date;
    private String product_type_id;
    private String biding_id;
    private String company_id;
    private String driver_name;
    private String phone;
    private String van_id;
    private Double total_weight;
    private Integer total_cargo;
    private SizeStandard size_standard;
    private Double price;
    private ProductType product_type;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private String bidding_status;
    private String bidding_type;
    private String type;
    private String status;
    private boolean isCheckDuration = true;
    private String bidding_order_number;
    private Integer create_uid;
    private String driver_id;
    private Integer cargo_id;
    private String note;
    private String bidding_order_receive_id;
    private String bidding_order_return_id;
    private Integer bidding_vehicle_id;
    private Integer price_bidding_order;
    private boolean isDurationCountdow = false;
    private String write_date;
    private int bidding_package_id;
    private OdooDateTime max_confirm_time;
    private List<BiddingVehicles> bidding_vehicles;


    public String getPrice_str(){
        return this.price != null ? AppController.getInstance().formatCurrency(price) : "";
    }


}
