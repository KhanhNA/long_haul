package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleDriver extends BaseModel {
    private Driver driver;
    private Vehicle vehicle;
}
