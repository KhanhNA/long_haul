package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Country extends BaseModel {
    private Integer id;
    private String name;
    private String code;
    private String uri_path;
}
