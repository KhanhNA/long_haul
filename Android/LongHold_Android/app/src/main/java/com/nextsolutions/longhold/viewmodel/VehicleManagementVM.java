package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VehicleManagementVM extends BaseViewModel {

    ObservableField<String> txtSearch = new ObservableField<>();

    public VehicleManagementVM(@NonNull Application application) {
        super(application);
    }
}
