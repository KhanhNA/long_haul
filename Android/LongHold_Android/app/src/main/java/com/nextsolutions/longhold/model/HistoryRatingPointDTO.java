package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryRatingPointDTO extends BaseModel {
    Long id;
    Long rating_place_id;
    Integer rating;
    String create_date;
}
