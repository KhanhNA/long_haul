package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverCargo extends BaseModel {
    private Integer id;
    private String code;
    private Integer res_user_id;
    private String lisence_plate;
    private String driver_phone_number;
    private String driver_name;
    private String to_char;
    private Integer company_id;
    private String status;
    private String description;
    private String store_fname;
    private Integer create_uid;
    private Integer write_uid;
    private String id_card;
    private String name_seq_self_sharevan_bidding_vehicle;
    private Integer res_partner_id;
    private Integer tonnage;
    private String vehicle_type;


}
