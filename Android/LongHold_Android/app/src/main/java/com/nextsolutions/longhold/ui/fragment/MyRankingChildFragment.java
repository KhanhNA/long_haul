package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.MyRankingChildFragmentBinding;
import com.nextsolutions.longhold.model.PrivilegesDTO;
import com.nextsolutions.longhold.viewmodel.MyRankingChildVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.List;

public class MyRankingChildFragment extends BaseFragment {
    MyRankingChildFragmentBinding mBinding;
    MyRankingChildVM rankingChildVM;
    XBaseAdapter adapter;
    List<PrivilegesDTO> privilegesDTOS;
    public MyRankingChildFragment(List<PrivilegesDTO> privilegesDTOS) {
        this.privilegesDTOS = privilegesDTOS;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        rankingChildVM = (MyRankingChildVM) viewModel;
        mBinding = (MyRankingChildFragmentBinding) binding;
        setUpRecycleView();
        getData();
        return view;
    }

    private void getData(){
        rankingChildVM.getData(privilegesDTOS,this::runUi);
    }
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.API_SUCCESS)) {
            adapter.notifyDataSetChanged();
        }
    }

    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.my_ranking_content, rankingChildVM.getPrivilegesDTOS(), this);
        mBinding.rcPri.setAdapter(adapter);
        mBinding.rcPri.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));

    }



    @Override
    public int getLayoutRes() {
        return R.layout.my_ranking_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MyRankingChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcPri;
    }
}
