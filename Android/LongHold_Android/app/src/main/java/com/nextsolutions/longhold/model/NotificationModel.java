package com.nextsolutions.longhold.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationModel extends BaseModel {

    int id;
    int notification_id;
    boolean is_read;
    int create_uid;
    OdooDateTime create_date;
    String sent_date;
    String title;
    String content;
    String type;
    String click_action;
    String message_type;
    String item_id;
    String object_status;
    String image_256;
}
