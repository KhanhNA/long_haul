package com.nextsolutions.longhold.ui.viewcommon;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.nextsolutions.longhold.BR;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.databinding.InforCargoBottomSheetBinding;
import com.nextsolutions.longhold.model.VehicleCargo;


public class InforCargoBottomSheet extends Dialog {
    InforCargoBottomSheetBinding binding;
    private Context mContext;
    private VehicleCargo mVehicleCargo;
    private RunUi mRunUi;

    public InforCargoBottomSheet(@NonNull Context context, VehicleCargo vehicleCargo , RunUi runUi) {
        super(context);
        mContext = context;
        mVehicleCargo = vehicleCargo;
        mRunUi = runUi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.infor_cargo_bottom_sheet, null, false);
        setContentView(binding.getRoot());
        binding.tvBksDriver.setText("BKS: " + mVehicleCargo.getLisence_plate());
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);
        this.getWindow().setGravity(Gravity.BOTTOM);
        int windowsScale = this.getContext().getResources().getDisplayMetrics().widthPixels;
        windowsScale = (int) ((float) windowsScale * 1f);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        initListener();
        configView();
    }

    /**
     * set hide button view bidding
     */
    private void configView() {
        binding.btnInforBidding.setVisibility(mVehicleCargo.getList_bidding_order()!=null?View.VISIBLE:View.GONE);
    }

    private void initListener() {
        binding.setVariable(BR.listener, this);
        binding.btnDeleteCargo.setOnClickListener(v -> {
            mRunUi.run(Constants.DELETE_CARGO);
            dismiss();
        });
        binding.btnInforBidding.setOnClickListener(v -> {
            mRunUi.run(Constants.VIEW_INFOR_CARGO);
            dismiss();
        });
        binding.btnEditCargo.setOnClickListener(v -> {
            mRunUi.run(Constants.EDIT_CARGO);
            dismiss();
        });
    }
}
