package com.nextsolutions.longhold.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.databinding.ActivityMainBinding;
import com.nextsolutions.longhold.fragmentnavigation.FragmentNavigation;
import com.nextsolutions.longhold.fragmentnavigation.FragmentNavigationImp;
import com.nextsolutions.longhold.fragmentnavigation.FragmentNavigationProvider;
import com.nextsolutions.longhold.model.NotificationModel;
import com.nextsolutions.longhold.ui.fragment.AccountFragment;
import com.nextsolutions.longhold.ui.fragment.BiddingTimeFragment;
import com.nextsolutions.longhold.ui.fragment.BiddingTimeLiveFragment;
import com.nextsolutions.longhold.ui.fragment.DetailBiddingFragment;
import com.nextsolutions.longhold.ui.fragment.HomeFragment;
import com.nextsolutions.longhold.ui.fragment.ManagerCargoFragment;
import com.nextsolutions.longhold.ui.fragment.NotificationFragment;
import com.nextsolutions.longhold.ui.fragment.VehicleManagementFragment;
import com.nextsolutions.longhold.ui.viewcommon.NonSwipeableViewPager;
import com.nextsolutions.longhold.ui.viewcommon.PagerAdapter;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.socket_notification.SocketIO;
import com.nextsolutions.socket_notification.SocketIONavigation;
import com.nextsolutions.socket_notification.SocketIONavigationProvider;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import static com.nextsolutions.longhold.base.AppController.channel_SOCKET;
import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;


public class MainActivity extends BaseActivity<ActivityMainBinding> implements FragmentNavigationProvider,
        BiddingTimeFragment.SwitchNotificationHomeFragment, SocketIO.onMessageSocket, SocketIONavigationProvider {

    public FragmentNavigation mFragmentNavigation;
    public SocketIONavigation mSocketIONavigation;

    private int mIndexPage = 0;
    private ActivityMainBinding activityMainBinding;
    private NotificationModel notificationModel = new NotificationModel();
    // sự kiện bottom Home
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        switch (item.getItemId()) {
            case R.id.calendar_bidding:
                if (mIndexPage != 0)
                    activityMainBinding.viewPager.setCurrentItem(0, false);
                mIndexPage = 0;
                return true;
            case R.id.navigation_home:
                if (mIndexPage != 1)
                    activityMainBinding.viewPager.setCurrentItem(1, false);
                mIndexPage = 1;
                return true;
            case R.id.navigation_manager:
                if (mIndexPage != 2)
                    activityMainBinding.viewPager.setCurrentItem(2, false);
                mIndexPage = 2;
                return true;
            case R.id.navigation_notif:
                if (mIndexPage != 3)
                    activityMainBinding.viewPager.setCurrentItem(3, false);
                mIndexPage = 3;
                return true;
            case R.id.navigation_account:
                if (mIndexPage != 4)
                    activityMainBinding.viewPager.setCurrentItem(4, false);
                mIndexPage = 4;
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = binding;
        provideNavigation();
        socketNavigation();
        if (mSocketIONavigation != null)
            mSocketIONavigation.connect(AppController.URI_SOCKET, channel_SOCKET, this::messageSocket);
        initViewPagerTablayout();

        BottomNavigationView navigation = findViewById(R.id.navigationHome);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // get ID tocken của device
        getTokenID();

        // sự kiện Action Notification
        actionNotification();
    }

    private void actionNotification() {
        int action = getIntent().getIntExtra(Constants.INTENT_NOTIFICATION, 0);
        if (action == 0) return;
        if (action == EnumBidding.EnumActionNotification.ACTION_DETAIL) {
            notificationModel = (NotificationModel) getIntent().getSerializableExtra(Constants.EXTRA_DATA);
            confirmRedNotification(notificationModel.getId());
            Bundle args = new Bundle();
            args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, (Integer.parseInt(notificationModel.getItem_id())));
            args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BID_SUCCESS);
            Intent intent = new Intent(this, CommonActivity.class);
            intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
            intent.putExtra(DATA_PASS_FRAGMENT, args);
            startActivity(intent);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public FragmentNavigation provideNavigation() {
        mFragmentNavigation = new FragmentNavigationImp(this, R.id.frameHome);
        return mFragmentNavigation;
    }

    @Override
    public SocketIONavigation socketNavigation() {
        mSocketIONavigation = new SocketIO();
        return mSocketIONavigation;
    }

    // sự kiện hide notification
    @Override
    public void onInputSent() {
        BiddingTimeLiveFragment.getInstance().upDateNotification();
    }

    @Override
    public void messageSocket(Object... args) {

    }

    private void getTokenID() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    Log.e("duongnk", "getTokenID: " + token);
                    // Luu token
                    AppController.getInstance().getEditor().putString("token_fb", token).apply();
                    DriverApi.updateToken(token, new IResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            Log.e("duongnk", "onSuccess: updateToken");
                        }

                        @Override
                        public void onFail(Throwable error) {

                        }
                    });
                });
    }


    private void initViewPagerTablayout() {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BiddingTimeFragment());
        adapter.addFragment(new HomeFragment());
        adapter.addFragment(new VehicleManagementFragment());
        adapter.addFragment(new NotificationFragment());
        adapter.addFragment(new AccountFragment());
        activityMainBinding.viewPager.setAdapter(adapter);
        activityMainBinding.viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.NONE);
        activityMainBinding.viewPager.setOffscreenPageLimit(5);
    }

    public void confirmRedNotification(int id) {
        DriverApi.confirmRedNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocketIONavigation.disconnect(channel_SOCKET);
    }
}