package com.nextsolutions.longhold.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SocketModel extends BaseModel {
    @SerializedName("actionType")
    @Expose
    private String actionType;
    @SerializedName("lstBiddingPackages")
    @Expose
    private List<BiddingPackage> lstBiddingPackages = null;
    private String bidding_package_id;
}
