package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GestureDetectorCompat;
import androidx.fragment.app.DialogFragment;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.database.RealmCalendarEvent;
import com.nextsolutions.longhold.databinding.FragmentDetailBiddingBinding;
import com.nextsolutions.longhold.model.ActionLog;
import com.nextsolutions.longhold.model.BiddingOrderDetail;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.BiddingPackageDetail;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.ui.viewcommon.CustomToast;
import com.nextsolutions.longhold.ui.viewcommon.Dialog;
import com.nextsolutions.longhold.ui.viewcommon.DialogListOfCargo;
import com.nextsolutions.longhold.ui.viewcommon.DialogListOfVehicles;
import com.nextsolutions.longhold.ui.viewcommon.InfoWindowCustom;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.ImageUtils;
import com.nextsolutions.longhold.utils.ReminderProvide;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.DetailBiddingFragmentVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.nextsolutions.longhold.base.Constants.DATA_INFORMATION;
import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.DATA_SERIALIZABLE;
import static com.nextsolutions.longhold.base.Constants.GOOGLE_API_KEY_GEOCODE;
import static com.nextsolutions.longhold.base.Constants.KEY_START_ACTIVITY_FOR_RESULTS;
import static com.nextsolutions.longhold.base.Constants.TIME;
import static com.nextsolutions.longhold.utils.ReminderProvide.pushAppointmentsToCalender;

public class DetailBiddingFragment extends BaseFragment implements OnMapReadyCallback, RoutingListener {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private GoogleMap mMap;
    private MarkerOptions mMarkerOptionsReceive, mMarkerOptionsReturns;
    private List<Polyline> mPolylines = null;
    private DetailBiddingFragmentVM mDetailBiddingFragmentVM;
    private int mKeyPage;
    private FragmentDetailBiddingBinding mBinding;
    private Dialog mDialog;
    private DialogListOfCargo mDialogListOfCargo;
    private DialogListOfVehicles mDialogListOfVehicles;
    private int bidding_id;
    private Marker receive, returns;
    private int mPosition;
    private ArrayList<Long> mCountDowTimeMillisecond = new ArrayList<>();
    private BiddingPackage mBiddingPackage = new BiddingPackage();
    private GestureDetectorCompat detector;
    private BottomSheetBehavior<ConstraintLayout> bottomSheetBehavior;
    private BiddingInformation mBiddingInformation;
    private String googleMapKey;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail_bidding;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailBiddingFragmentVM.class;
    }

    private void getData() {
        googleMapKey = AppController.getInstance().getSharePre().getString(GOOGLE_API_KEY_GEOCODE, "");
        mKeyPage = Objects.requireNonNull(Objects.requireNonNull(getActivity()).getIntent().getExtras()).getBundle(DATA_PASS_FRAGMENT).getInt(StringUtils.KEYPAGE);
        bidding_id = Objects.requireNonNull(Objects.requireNonNull(getActivity().getIntent().getExtras()).getBundle(DATA_PASS_FRAGMENT)).getInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O);
        mCountDowTimeMillisecond = (ArrayList<Long>) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(TIME);
        mPosition = Objects.requireNonNull(getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT)).getInt(Constants.POSITION);
        mBiddingPackage = (BiddingPackage) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(DATA_SERIALIZABLE);
        mBiddingInformation = (BiddingInformation) getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getSerializable(DATA_INFORMATION);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mDetailBiddingFragmentVM = (DetailBiddingFragmentVM) viewModel;
        mBinding = (FragmentDetailBiddingBinding) binding;
        mBinding.loading.setVisibility(View.VISIBLE);
        mBinding.txtPriceLinesRed.setVisibility(View.GONE);
        getData();

        switch (mKeyPage) {
            case EnumBidding.EnumBiddingBehavior.BIDDING_HAPPENNING:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mDetailBiddingFragmentVM.getBiddingPackage(String.valueOf(bidding_id), this::runUi);
                mBinding.iclBottomSheet.txtDateBidding.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReturnsReality.setVisibility(View.GONE);
                mBinding.txtStatus.setText(getString(R.string.going_on));
                break;
            case EnumBidding.EnumBiddingBehavior.BIDDING_UPCOMING:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mDetailBiddingFragmentVM.getBiddingPackage(String.valueOf(bidding_id), this::runUi);
                mBinding.iclBottomSheet.txtDateBidding.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReturnsReality.setVisibility(View.GONE);
                mBinding.txtStatus.setText(getString(R.string.coming_soon));
                if (mDetailBiddingFragmentVM.isCheckIdEventCalendar(String.valueOf(mBiddingPackage.getId()))) {
                    mBinding.btnBidding.setText(getString(R.string.remind_me));
                } else {
                    mBinding.btnBidding.setText(R.string.close_remind);
                }
                break;
            case EnumBidding.EnumBiddingBehavior.WAITING_FOR_INFORMATION:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mBinding.btnBidding.setText(getString(R.string.fill_in_vehicle_information));
                mBinding.btnBidding.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReturnsReality.setVisibility(View.GONE);
                mBinding.txtStatus.setText(getString(R.string.lack_of_driving_information));
                break;
            case EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mBinding.btnBidding.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReceiveReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.GONE);
                mBinding.iclBottomSheet.textViewReturnsReality.setVisibility(View.GONE);
                mBinding.txtStatus.setText(getString(R.string.wait_for_confirmation));
                mBinding.btnBidding.setText(getString(R.string.wait_for_vehicle_information));
                break;
            case EnumBidding.EnumBiddingBehavior.BID_SUCCESS:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mBinding.iclBottomSheet.txtLicensePlates.setVisibility(View.VISIBLE);
                mBinding.btnBidding.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.textViewReceiveReality.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.textViewReturnsReality.setVisibility(View.VISIBLE);

                break;
            case EnumBidding.EnumBiddingBehavior.BID_HISTORY:
                mDetailBiddingFragmentVM.getNewBiddingDetail(String.valueOf(bidding_id), this::runUi);
                mBinding.iclBottomSheet.txtReceiveReality.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtReturnsReality.setVisibility(View.VISIBLE);
                mBinding.iclBottomSheet.txtLicensePlates.setVisibility(View.VISIBLE);
                mBinding.btnBidding.setVisibility(View.INVISIBLE);
                break;
        }
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.googleMap);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomSheetBehavior = BottomSheetBehavior.from(mBinding.iclBottomSheet.viewInfo);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        detector = new GestureDetectorCompat(getContext(), new MyGestureListener());
        mBinding.clFooter.setOnTouchListener(this::onTouch);

    }



    private boolean onTouch(View view1, MotionEvent motionEvent) {
        if (detector.onTouchEvent(motionEvent)) {
            return true;
        }
        return false;
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onDown(MotionEvent event) {
//            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            Log.d(DEBUG_TAG, "onDown: " + event.toString());
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
//            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return super.onSingleTapUp(e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals("Fail")) {
            ToastUtils.showToast("Có lỗi xảy ra");
        }
        if (action.equals("createBiddingOrderSuccess")) {
            //click button đấu thầu
            CustomToast.makeText(getContext(), getString(R.string.participate_in_successful_bidding), CustomToast.LENGTH_LONG, 1, true).show();
            Bundle args = new Bundle();
            args.putInt(Constants.KEY_BIDDING_ID, bidding_id);
            args.putInt(Constants.BIDDING_ID, bidding_id);
            args.putSerializable(TIME, mCountDowTimeMillisecond);
            args.putInt(Constants.POSITION, mPosition);
            Intent intent = new Intent(getActivity(), CommonActivity.class);
            intent.putExtra(Constants.FRAGMENT, ConfirmCargoFragment.class);
            intent.putExtra(DATA_PASS_FRAGMENT, args);
            startActivityForResult(intent, KEY_START_ACTIVITY_FOR_RESULTS);
        } else if (action.equals("createBiddingOrderFail")) {
           ToastUtils.showToast("Lỗi");
        }
        if (action.equals("getNewBiddingSuccess")) {
            getDataOrderView();
        }
        if (action.equals("getBiddingFail")) {
            ToastUtils.showToast("Không có dữ liệu gói thầu : " + bidding_id);
        }
        if (action.equals("getBiddingPackageSuccess")) {
            getDataPackageView();
        }
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getDataOrderView() {
        BiddingOrderDetail biddingOrderDetail = mDetailBiddingFragmentVM.mBiddingOrder.getValue();
        if (biddingOrderDetail != null) {
            mDetailBiddingFragmentVM.getBiddingPackage(String.valueOf(biddingOrderDetail.getBidding_order().getBidding_package_id()), this::runUi);
            mBinding.txtprice.setText(biddingOrderDetail.getPrice_str());
            mBinding.txtDistance.setText(biddingOrderDetail.getDistance() + " km");
            mBinding.iclBottomSheet.txtCargoSize.setText(biddingOrderDetail.getTotal_weight() + " kg");
            if (biddingOrderDetail.getBidding_order().getFrom_depot() != null) {
                DetailBiddingFragmentVM.setTimeBidding(mBinding.txtReceive, biddingOrderDetail.getFrom_receive_time(),
                        biddingOrderDetail.getTo_receive_time(),
                        biddingOrderDetail.getBidding_order().getFrom_depot().getAddress());
            }
            if (biddingOrderDetail.getBidding_order().getTo_depot() != null) {
                DetailBiddingFragmentVM.setTimeBidding(mBinding.txtReturns, biddingOrderDetail.getFrom_return_time(),
                        biddingOrderDetail.getTo_return_time(),
                        biddingOrderDetail.getBidding_order().getTo_depot().getAddress());
            }

            mBinding.iclBottomSheet.txtId.setText("ID: " + biddingOrderDetail.getBidding_package_number());
            mBinding.iclBottomSheet.txtNumberOfCargo.setText(biddingOrderDetail.getBidding_order().getTotal_cargo() + " cargo");
            if (biddingOrderDetail.getCreate_date() != null) {
                mBinding.iclBottomSheet.txtDateBidding.setText(getString(R.string.tvbidding) + AppController.convertDate(biddingOrderDetail.getCreate_date()));
            }
            if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_HISTORY) {
                mBinding.iclBottomSheet.txtReceiveReality.setText(AppController.convertDate(biddingOrderDetail.getBidding_order().getFrom_receive_time(), "HH:mm dd/MM/yyyy"));
                mBinding.iclBottomSheet.txtReturnsReality.setText(AppController.convertDate(biddingOrderDetail.getBidding_order().getTo_return_time(), "HH:mm dd/MM/yyyy"));
                setType();
            }
            if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_SUCCESS) {
                mBinding.iclBottomSheet.txtReceiveReality.setText(AppController.convertDate(biddingOrderDetail.getBidding_order().getFrom_receive_time(), "HH:mm dd/MM/yyyy"));
                mBinding.iclBottomSheet.txtReturnsReality.setText(AppController.convertDate(biddingOrderDetail.getBidding_order().getTo_return_time(), "HH:mm dd/MM/yyyy"));
                setType();
            }

            Drawable iconDrawable = getResources().getDrawable(R.drawable.location);
            BitmapDescriptor icon = ImageUtils.getMarkerIconFromDrawable(iconDrawable);
            if (biddingOrderDetail.getFrom_latitude() != null && biddingOrderDetail.getFrom_latitude() != null) {
//                LatLng start = getLatLngLocation(getContext(), biddingOrderDetail.getBidding_order().getFrom_depot().getAddress());
                LatLng start = new LatLng(biddingOrderDetail.getFrom_latitude(), biddingOrderDetail.getFrom_longitude());
                mMarkerOptionsReceive = new MarkerOptions().position(start)
                        .title(biddingOrderDetail.getBidding_order().getFrom_depot().getAddress())
                        .icon(icon);
                if (mMap != null)
                    receive = mMap.addMarker(mMarkerOptionsReceive);
            }
            if (biddingOrderDetail.getTo_latitude() != null && biddingOrderDetail.getTo_longitude() != null) {
//                LatLng end = getLatLngLocation(getContext(), biddingOrderDetail.getBidding_order().getTo_depot().getAddress());
                LatLng end = new LatLng(biddingOrderDetail.getTo_latitude(), biddingOrderDetail.getTo_longitude());
                mMarkerOptionsReturns = new MarkerOptions().position(end)
                        .title(biddingOrderDetail.getBidding_order().getTo_depot().getAddress());
                returns = mMap.addMarker(mMarkerOptionsReturns);

            }
            if (mMarkerOptionsReceive != null && mMarkerOptionsReturns != null && mMap != null) {
                // Tìm đường vẽ đường
                Findroutes(mMarkerOptionsReceive.getPosition(), mMarkerOptionsReturns.getPosition());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(mMarkerOptionsReturns.getPosition()));
                //Hiển thị 2 các điểm vừa màn hình
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(receive.getPosition());
                builder.include(returns.getPosition());
                LatLngBounds bounds = builder.build();
                int padding = 300; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.moveCamera(cu);
            }


        }
        mBinding.loading.setVisibility(View.GONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getDataPackageView() {
        mBinding.txtprice.setText(mDetailBiddingFragmentVM.mBiddingPackage.getValue().getPrice_str());
        mBinding.txtDistance.setText(mDetailBiddingFragmentVM.mBiddingPackage.getValue().getDistance() + " km");
        mBinding.iclBottomSheet.txtCargoSize.setText(mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTotal_weight() + " kg");
        if (mDetailBiddingFragmentVM.mBiddingPackage.getValue().getFrom_receive_time() != null) {
            DetailBiddingFragmentVM.setTimeBidding(mBinding.txtReceive, mDetailBiddingFragmentVM.mBiddingPackage.getValue().getFrom_receive_time(),
                    mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTo_receive_time(),
                    mDetailBiddingFragmentVM.mBiddingPackage.getValue().getFrom_depot().getAddress());
        }
        if (mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTo_depot() != null) {
            DetailBiddingFragmentVM.setTimeBidding(mBinding.txtReturns, mDetailBiddingFragmentVM.mBiddingPackage.getValue().getFrom_return_time(),
                    mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTo_return_time(),
                    mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTo_depot().getAddress());
        }
        mBinding.iclBottomSheet.txtId.setText("ID: " + mDetailBiddingFragmentVM.mBiddingPackage.getValue().getBidding_package_number());
        mBinding.iclBottomSheet.txtNumberOfCargo.setText(mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTotal_cargo() + " cargo");
        if (mDetailBiddingFragmentVM.mBiddingPackage.getValue().getCreate_date() != null) {
            mBinding.iclBottomSheet.txtDateBidding.setText(getString(R.string.tvbidding) + AppController.convertDate(mDetailBiddingFragmentVM.mBiddingPackage.getValue().getCreate_date()));
        }
        Drawable iconDrawableblue = getResources().getDrawable(R.drawable.ic_order_already_available_blue);
        BitmapDescriptor iconblue = ImageUtils.getMarkerIconFromDrawable(iconDrawableblue);
        Drawable iconDrawablered = getResources().getDrawable(R.drawable.ic_no_orders_yet_red);
        BitmapDescriptor iconred = ImageUtils.getMarkerIconFromDrawable(iconDrawablered);
        // Thêm tài xế vào bản đồ qua vòng for này ( dùng getAction_log() để thêm, hiện tại trong API chưa có LatLng )
        List<BiddingPackageDetail.BiddingVehiclePackage> biddingVehiclesList = mDetailBiddingFragmentVM.mBiddingVehiclesPackage.getValue();
        if (biddingVehiclesList != null && biddingVehiclesList.size() > 0) {
            Log.d("TAG", "getDataPackageView: " + biddingVehiclesList.size());
            for (BiddingPackageDetail.BiddingVehiclePackage biddingVehicles : biddingVehiclesList) {
                if (biddingVehicles.getAction_log() != null && biddingVehicles.getAction_log().size() > 0) {
                    ActionLog actionLog = biddingVehicles.getAction_log().get(0);
                    if (actionLog.getVan_id().equals(biddingVehicles.getId() + "")) {
                        LatLng lnl = new LatLng(actionLog.getLatitude(), actionLog.getLongitude());
                        //if 1 đã có đơn else chưa có đơn
                        if (mMap != null) {
                            Log.d("TAG", "getDataPackageViewActionLog: " + actionLog.getLatitude() + " " + actionLog.getLongitude());
                            MarkerOptions driverMarker = new MarkerOptions().position(lnl).title(biddingVehicles.getLisence_plate())
                                    .snippet("Tài xế: " + biddingVehicles.getDriver_name() +
                                            "\nSĐT: " + biddingVehicles.getDriver_phone_number());
                            if ("1".equals(biddingVehicles.getStatus())) {
                                driverMarker.icon(iconblue);
                            } else {
                                driverMarker.icon(iconred);
                            }
                            mMap.addMarker(driverMarker);
                        }
                    }

                } else {
                    Log.d("TAG", "getDataPackageViewId: " + biddingVehicles.getId() + " Null");
                }
            }

        }
        mBinding.loading.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new InfoWindowCustom(getContext()));
        //Click vào Info maker
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // So sánh marker title là BKS với BKS trong list mBiddingVehicles
                List<BiddingVehicles> biddingVehicles = mDetailBiddingFragmentVM.mBiddingVehicles.getValue();
                if (biddingVehicles != null){
                    for (BiddingVehicles data : biddingVehicles) {
                        if (marker.getTitle().equals(data.getLisence_plate())) {
                            // chuyền đối tượng ứng với maker sang màn VehicleOrdersFragment
                            Bundle args = new Bundle();
                            args.putSerializable(StringUtils.TO_VEHICLE_ORDES_FRAGMEN, data);
                            Intent intent = new Intent(getActivity(), CommonActivity.class);
                            intent.putExtra(Constants.FRAGMENT, VehicleOrdersFragment.class);
                            intent.putExtra(Constants.DATA_PASS_FRAGMENT, args);
                            startActivity(intent);
                            break;
                        }
                    }
                }

            }
        });
    }

    // hàm tìm đường
    public void Findroutes(LatLng receive, LatLng returns) {
        if (receive == null || returns == null && googleMapKey.isEmpty()) {
            ToastUtils.showToast("Unable to get location");
        } else {
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(receive, returns)
                    .key(googleMapKey) // key tìm đường do google cấp riêng.
                    .build();
            routing.execute();
        }
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (mPolylines != null) {
            mPolylines.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        mPolylines = new ArrayList<>();
        // vẽ tuyến đường vào bản đồ bằng polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.colorAccent_v2));
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                mPolylines.add(polyline);
            }
        }
    }

    @Override
    public void onRoutingCancelled() {
        Findroutes(mMarkerOptionsReceive.getPosition(), mMarkerOptionsReturns.getPosition());
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Findroutes(mMarkerOptionsReceive.getPosition(), mMarkerOptionsReturns.getPosition());
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btn_Bidding:
                //Đang diễn ra
                if (mKeyPage == EnumBidding.EnumBiddingBehavior.BIDDING_HAPPENNING) {
                    mDialog = new Dialog(getString(R.string.confirm_participation_in_bidding_package), null, getString(R.string.btnConfirm), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dateFormatter.setLenient(false);
                            Date today = new Date();
//                            String mData = dateFormatter.format(today);
                            mDetailBiddingFragmentVM.createBiddingOrder(String.valueOf(bidding_id), today, DetailBiddingFragment.this::runUi);

                            mDialog.dismiss();
                        }
                    });
                    mDialog.show(getChildFragmentManager(), "tag");
                    //Thiếu thông tin xe
                } else if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_INFORMATION) {
                    Bundle args = new Bundle();
                    args.putInt(Constants.KEY_BIDDING_ID, bidding_id);
//                    args.putSerializable(Constants.BIDDING_ID, mDetailBiddingFragmentVM.mBiddingPackage.getValue());
//                    args.putSerializable(Constants.BIDDING_ID, mDetailBiddingFragmentVM.mBiddingPackage.getValue());
                    args.putSerializable(TIME, mCountDowTimeMillisecond);
                    args.putInt(Constants.POSITION, mPosition);
                    args.putSerializable(Constants.DATA_INFORMATION,mBiddingInformation);
                    Intent intent = new Intent(getActivity(), CommonActivity.class);
                    intent.putExtra(Constants.FRAGMENT, ConfirmCargoFragment.class);
                    intent.putExtra(DATA_PASS_FRAGMENT, args);
                    startActivityForResult(intent, KEY_START_ACTIVITY_FOR_RESULTS);

                } else if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION) {
                    mDialogListOfVehicles = new DialogListOfVehicles(getString(R.string.cargo_vehicle_information), mDetailBiddingFragmentVM.mBiddingVehicles.getValue(), mKeyPage, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            eventClickEditInfoCargo(mKeyPage, mDetailBiddingFragmentVM.mBiddingVehicles.getValue(), mDialogListOfVehicles);
                        }
                    });
                    mDialogListOfVehicles.show(getChildFragmentManager(), "rer");

                } else if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_HISTORY) {
                    mDialogListOfVehicles = new DialogListOfVehicles(getString(R.string.btnConfirm), mDetailBiddingFragmentVM.mBiddingVehicles.getValue(), mKeyPage, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialogListOfVehicles.dismiss();
                        }
                    });
                    mDialogListOfVehicles.show(getChildFragmentManager(), "tag");

                } else if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_SUCCESS) {
                    if (mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getType().equals("-1")) {
                        mDialog = new Dialog(getString(R.string.reasons_for_canceling_the_package), getString(R.string.btnConfirm),
                                mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getNote(), false, true,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                });
                        mDialog.show(getChildFragmentManager(), "tag");
                    } else {
                        mDialogListOfVehicles = new DialogListOfVehicles(getString(R.string.cargo_vehicle_information), mDetailBiddingFragmentVM.mBiddingVehicles.getValue(), mKeyPage, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialogListOfVehicles.dismiss();
                            }
                        });
                        mDialogListOfVehicles.show(getChildFragmentManager(), "rer");
                    }


                } else if (mKeyPage == EnumBidding.EnumBiddingBehavior.BIDDING_UPCOMING) {
                    if (ReminderProvide.haveCalendarReadWritePermissions(getActivity())) {
                        addNewEvent(mBiddingPackage.getId() + "", mBiddingPackage);
                    } else {
                        ReminderProvide.requestCalendarReadWritePermission(getActivity());
                    }
                }
                break;
            case R.id.onBack:
                getActivity().onBackPressed();
                break;
            case R.id.txtLicensePlates:
                if (mDetailBiddingFragmentVM.mCargos.getValue() != null) {
                    mDialogListOfCargo = new DialogListOfCargo(getString(R.string.list_of_cargo_types), mDetailBiddingFragmentVM.mCargos.getValue(), mDetailBiddingFragmentVM.mBiddingPackage.getValue().getTotal_cargo());
                    mDialogListOfCargo.show(getChildFragmentManager(), "tag");
                }else{
                    ToastUtils.showToast(getString(R.string.text_data_not_found));
                }
                break;
        }
    }

    private void eventClickEditInfoCargo(int mKeyPage, List<BiddingVehicles> mBiddingVehicles, DialogFragment dialog) {
        if (mKeyPage == EnumBidding.EnumBiddingBehavior.BID_SUCCESS)
            dialog.dismiss();
        if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION) {
            intentConfirmCargoFragment(mBiddingVehicles);
            dialog.dismiss();
        }
    }

    private void intentConfirmCargoFragment(List<BiddingVehicles> mBiddingVehicles) {
        Bundle args = new Bundle();
        args.putInt(Constants.KEY_BIDDING_ID, bidding_id);
        args.putSerializable(TIME, mCountDowTimeMillisecond);
        args.putInt(Constants.POSITION, mPosition);
//        args.putSerializable(Constants.BIDDING_ID, mDetailBiddingFragmentVM.mBiddingPackage.getValue());
        args.putSerializable(Constants.BIDDING_VEHICLES, (Serializable) mBiddingVehicles);
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        args.putSerializable(Constants.DATA_INFORMATION,mBiddingInformation);
        intent.putExtra(Constants.FRAGMENT, ConfirmCargoFragment.class);
        intent.putExtra(DATA_PASS_FRAGMENT, args);
        startActivityForResult(intent, KEY_START_ACTIVITY_FOR_RESULTS);
    }

    //Hiển thị Toast "Điền thông tin Thành công" khi quay lại màn này.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_START_ACTIVITY_FOR_RESULTS && resultCode == getActivity().RESULT_OK) {
            CustomToast.makeText(getContext(), getString(R.string.fill_in_vehicle_information), CustomToast.LENGTH_LONG, 1, true).show();
        }
    }

    /**
     * thêm nhắc nhở vào calendar
     *
     * @param id
     * @param o
     */
    private void addNewEvent(String id, Object o) {
        // kiểm tra sự kiện đã đc thêm ở trước đó chưa (true là chưa có và thực hiện add - false -> đã có và thực hiện xóa)
        if (mDetailBiddingFragmentVM.isCheckIdEventCalendar(String.valueOf(id))) {
            BiddingPackage biddingPackage = (BiddingPackage) o;
            RealmCalendarEvent.getInstance(getContext()).addIdEvent(id + "");
            String from = (biddingPackage.getFrom_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getFrom_receive_time()) : "");
            String to = (biddingPackage.getTo_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getTo_receive_time()) : "");
            String content = "[bidding_order_number - " + biddingPackage.getDistance() + "km - " + biddingPackage.getPrice_str() + "]" + "Gói thầu từ " + from + "-" + to;

            pushAppointmentsToCalender(requireActivity(), "[LongHaul Bidding]", content, mCountDowTimeMillisecond.get(mPosition), true, 1, 1);
            mBinding.btnBidding.setText(R.string.close_remind);
            Toast.makeText(getActivity(), R.string.add_calendar, Toast.LENGTH_SHORT).show();
        } else {
            RealmCalendarEvent.getInstance(getContext()).deleteCalendar(id);
            ReminderProvide.deleteEventCalendar(id);
            mBinding.btnBidding.setText(R.string.remind);
            Toast.makeText(getActivity(), R.string.remove_calendar, Toast.LENGTH_SHORT).show();
        }
    }

    private void setType() {
        if (mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getType().equals("-1")) {
            mBinding.txtPriceLinesRed.setVisibility(View.VISIBLE);
            mBinding.txtStatus.setText(getString(R.string.canceled_status));
            mBinding.txtStatus.setTextColor(getContext().getResources().getColor(R.color.red));
            mBinding.btnBidding.setText(getString(R.string.reason_for_cancellation));
        } else {
            mBinding.btnBidding.setText(getString(R.string.cargo_vehicle_information));
            if (mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getStatus().equals("0")) {
                mBinding.txtStatus.setText(getContext().getResources().getString(R.string.bidding_success_in_transition) + getContext().getResources().getString(R.string.not_yet_shipped) + ")");
            } else if (mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getStatus().equals("1")) {
                mBinding.txtStatus.setText(getContext().getResources().getString(R.string.bidding_success_in_transition) + getContext().getResources().getString(R.string.received_the_goods) + ")");
            } else if (mDetailBiddingFragmentVM.mBiddingOrder.getValue().getBidding_order().getStatus().equals("2")) {
                mBinding.txtStatus.setText(getContext().getResources().getString(R.string.bidding_success_in_transition) + getContext().getResources().getString(R.string.bidding_done) + ")");
            }
        }
    }

}

