package com.nextsolutions.longhold.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.DialogSelectCargoBinding;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.tsolution.base.listener.AdapterListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class DialogSelectCargo extends DialogFragment implements AdapterListener, View.OnClickListener {


    DialogSelectCargoBinding mBinding;
    private Activity mActivity;
    private Context mContext;
    private OnButtonClicked onButtonClicked;
    private ArrayList<BiddingVehicles> mListDriverCargo = new ArrayList<>();
    private ArrayList<BiddingVehicles> mListSelect = new ArrayList<>();
    private IDialogSelectErrorCallback mCloseCallback;
    private XBaseAdapter adapter;


    //chuyen key page phan biet case thie thong tin, cho xac nhan , bid thanh cong
    public DialogSelectCargo(@NotNull Activity activity,
                             @NotNull Context context,
                             @NotNull ArrayList<BiddingVehicles> listSelect,
                             @NotNull OnButtonClicked onSelectAccount) {
        this.mActivity = activity;
        this.mContext = context;
        this.onButtonClicked = onSelectAccount;
        this.mListDriverCargo = listSelect;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_cargo, container, false);
        initRecyclerView();
        mListSelect.clear();
        mBinding.imgBack.setOnClickListener(view1 -> close());
        mBinding.btnConfirm.setOnClickListener(this::onClick);
        mBinding.imgSelect.setOnClickListener(this::onClick);
        mBinding.imgSuccess.setOnClickListener(this::onClick);
        showBtnSelectAll();

        return mBinding.getRoot();
    }

    private void showBtnSelectAll() {
        mBinding.imgSuccess.setVisibility(View.GONE);
        for (BiddingVehicles biddingVehicles : mListDriverCargo) {
            if (biddingVehicles.isCheckedAll()) {
                mBinding.imgSelect.setVisibility(View.GONE);
                mBinding.imgSuccess.setVisibility(View.VISIBLE);
            } else {
                mBinding.imgSelect.setVisibility(View.VISIBLE);
                mBinding.imgSuccess.setVisibility(View.GONE);
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    private void initRecyclerView() {
        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        adapter = new XBaseAdapter(R.layout.item_select_cargo, mListDriverCargo, this);
        mBinding.rvSelectDriverCargo.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.rvSelectDriverCargo.setAdapter(adapter);
    }

    private void filter(String textSearch) {
        ArrayList<BiddingVehicles> filteredList = new ArrayList<>();
        if (!textSearch.isEmpty()) {
            for (BiddingVehicles row : mListDriverCargo) {
                if (
                        row.getDriver_name().toLowerCase().contains(textSearch.toLowerCase())
                                || row.getDriver_phone_number().contains(textSearch)
                                || row.getLisence_plate().contains(textSearch)
                ) {
                    filteredList.add(row);
                }
            }
            adapter.setUpDateDat(filteredList);
        } else {
            adapter.setUpDateDat(mListDriverCargo);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int maxWidth = displayMetrics.widthPixels;
        int maxHeight = displayMetrics.heightPixels;
        int scaledWidth = (int) (maxWidth * 0.90);
        int scaledHeight = (int) (scaledWidth * 500 / 320);
        if (dialog != null) {
            if (scaledHeight > maxHeight) {
                dialog.getWindow().setLayout(scaledWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
            } else {
                dialog.getWindow().setLayout(scaledWidth, scaledHeight);
            }
        }
    }

    private void close() {
        mCloseCallback.close();
        dismiss();
    }

    public void setCloseCallback(IDialogSelectErrorCallback callback) {
        mCloseCallback = callback;
    }

    @Override
    public void onItemClick(View v, Object o) {
        BiddingVehicles biddingVehicle = (BiddingVehicles) o;
        int id = v.getId();
        switch (id) {
            case R.id.itemClick:
                biddingVehicle.setChecked(!biddingVehicle.isSelected());
                if (biddingVehicle.isChecked()) {
                    biddingVehicle.setChecked(biddingVehicle.isChecked());
                    mListSelect.add(biddingVehicle);
                    adapter.notifyDataSetChanged();
                } else {
                    biddingVehicle.setChecked(biddingVehicle.isSelected());
                    mListSelect.remove(biddingVehicle);
                    adapter.notifyDataSetChanged();
                }
                showButtonAll();
                break;
        }
    }

    private void showButtonAll() {
        if (mListSelect.size() == mListDriverCargo.size()) {
            mBinding.imgSuccess.setVisibility(View.VISIBLE);
            mBinding.imgSelect.setVisibility(View.GONE);
        } else {
            mBinding.imgSelect.setVisibility(View.VISIBLE);
            mBinding.imgSuccess.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirm:
                onButtonClicked.onClickButtonSelectAccount(mListSelect);
                break;

            case R.id.imgSelect:
                mListSelect.clear();
                mBinding.imgSuccess.setVisibility(View.VISIBLE);
                mBinding.imgSelect.setVisibility(View.GONE);
                for (BiddingVehicles biddingVehicles : mListDriverCargo) {
                    biddingVehicles.setChecked(!biddingVehicles.isSelected());
                    if (!biddingVehicles.isChecked()) {
                        biddingVehicles.setChecked(!biddingVehicles.isSelected());
                        biddingVehicles.setCheckedAll(biddingVehicles.isCheckedAll());
                    } else if (biddingVehicles.isChecked()) {
                        biddingVehicles.setCheckedAll(!biddingVehicles.isCheckedAll());
                        biddingVehicles.setChecked(biddingVehicles.isSelected());
                        mListSelect.add(biddingVehicles);
                        adapter.notifyDataSetChanged();
                    }
                    adapter.setUpDateDat(mListDriverCargo);
                }
                break;
            case R.id.imgSuccess:
                mListSelect.clear();
                mBinding.imgSuccess.setVisibility(View.GONE);
                mBinding.imgSelect.setVisibility(View.VISIBLE);
                for (BiddingVehicles biddingVehicles : mListDriverCargo) {
                    biddingVehicles.setChecked(!biddingVehicles.isSelected());
                    biddingVehicles.setCheckedAll(!biddingVehicles.isCheckedAll());
                    if (!biddingVehicles.isChecked()) {
                        biddingVehicles.setCheckedAll(biddingVehicles.isCheckedAll());
                        biddingVehicles.setChecked(biddingVehicles.isChecked());
                    }
                    mListSelect.add(biddingVehicles);
                    adapter.setUpDateDat(mListDriverCargo);
                }
                break;
        }
    }

    public interface IDialogSelectErrorCallback {
        void close();
    }

    public interface OnButtonClicked {
        void onClickButtonSelectAccount(ArrayList<BiddingVehicles> mListSelect);
    }
}

