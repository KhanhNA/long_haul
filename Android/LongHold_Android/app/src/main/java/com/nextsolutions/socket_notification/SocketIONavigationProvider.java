package com.nextsolutions.socket_notification;

public interface SocketIONavigationProvider {
    SocketIONavigation socketNavigation();
}
