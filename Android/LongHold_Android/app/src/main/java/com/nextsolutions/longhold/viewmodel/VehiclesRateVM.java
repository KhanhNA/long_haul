package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.RateModel;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;

public class VehiclesRateVM extends BaseViewModel {


    private MutableLiveData<OdooResultDto<RateModel>> mListVehiclesRates = new MutableLiveData<>();
    private OdooResultDto<RateModel> mVehiclesRate = new OdooResultDto<>();


    public VehiclesRateVM(@NonNull Application application) {
        super(application);
        mVehiclesRate.setRecords(new ArrayList<>());
        mListVehiclesRates.setValue(mVehiclesRate);
    }

    public MutableLiveData<OdooResultDto<RateModel>> getListVehiclesRates() {
        return mListVehiclesRates;
    }

    public void getVehiclesRate(int offset, RunUi runUi) {
        DriverApi.getListVehiclesRate(offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<RateModel> response = (OdooResultDto<RateModel>) o;
                mVehiclesRate.getRecords().addAll(response.getRecords());
                mVehiclesRate.setLength(response.getLength());
                mListVehiclesRates.setValue(mVehiclesRate);
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }
    public void clearData(){
       if(mVehiclesRate.getRecords()!=null)
           mVehiclesRate.getRecords().clear();
    }
}
