package com.nextsolutions.longhold.base;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.BR;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.utils.GlideEngine;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class ImageBaseAdapter extends RecyclerView.Adapter<ImageBaseAdapter.ViewHolder> {
    private List<String> datas;
    private int layoutItem;
    private List<LocalMedia> localMedia;
    private Activity activity;
    public ImageBaseAdapter(Activity activity, @LayoutRes int item, List<String> lst) {
        this.layoutItem = item;
        this.datas = lst;
        this.activity = activity;
    }
    public ImageBaseAdapter(Context context, @LayoutRes int item, List<String> lst) {
        this.layoutItem = item;
        this.datas = lst;
        this.activity = (Activity) context;
    }

    public void notifyDataSetChange(){
        if(datas == null){
            return;
        }
        localMedia = new ArrayList<>();
        for (String url : datas) {
            LocalMedia urlImage = new LocalMedia();
            urlImage.setPath(url);
            localMedia.add(urlImage);
        }
        super.notifyDataSetChanged();
    }

    public void notifyDataSetChange(List<String> list){
        this.datas = list;
        notifyDataSetChange();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), this.layoutItem, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        viewHolder.bind(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return this.datas == null ? 0 : (this.datas).size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding itemProductBinding;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.itemProductBinding = view;
            view.getRoot().setOnClickListener(v -> {
                previewImage(getAdapterPosition());
            });
        }

        public void bind(Object obj) {
            itemProductBinding.setVariable(BR.viewHolder, obj);
            itemProductBinding.executePendingBindings();
        }

    }
    private void previewImage(int position) {
        try {
            PictureSelector.create(activity)
                    .themeStyle(R.style.picture_WeChat_style)
                    .isWeChatStyle(true)
                    .loadImageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(position, localMedia);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
