package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.List;

public class VehicleOrdersFragmentVM extends BaseViewModel {


    public MutableLiveData<OdooResultDto<BiddingInformation>> mInformationBidding = new MutableLiveData<>();
    public MutableLiveData<BiddingVehicles> mBiddingVehicle = new MutableLiveData<>();
    private OdooResultDto<BiddingInformation> mInformationUpdate;

    public VehicleOrdersFragmentVM(@NonNull Application application) {
        super(application);
        mInformationUpdate = new OdooResultDto<>();
        mInformationBidding.setValue(mInformationUpdate);
    }

    public MutableLiveData<OdooResultDto<BiddingInformation>> getInformationBidding() {
        return mInformationBidding;
    }


    public void getBiddingOrderDetail(String bidding_package_id, int offset, RunUi runUi) {
        DriverApi.requestGetBiddingOrderDetail(bidding_package_id, offset, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<BiddingInformation> response = (OdooResultDto<BiddingInformation>) o;
                if (response == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                if (mInformationBidding.getValue() != null && mInformationBidding.getValue().getRecords() != null) {
                    mInformationBidding.getValue().getRecords().addAll(response.getRecords());
                    mInformationBidding.getValue().setLength(response.getLength());
                    if(response.getRecords().get(0).getBidding_vehicles() != null){
                        mBiddingVehicle.setValue(response.getRecords().get(0).getBidding_vehicles().get(0));
                    }
                } else mInformationBidding.setValue((OdooResultDto<BiddingInformation>) o);
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void clearData() {
        mInformationBidding.setValue(null);
    }
}
