package com.nextsolutions.longhold.ui.viewcommon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.model.CargoTypes;
import com.nextsolutions.longhold.enums.EnumBidding;

import java.util.List;

public class DialogAdapterVehicles extends RecyclerView.Adapter<DialogAdapterVehicles.ViewHolder> {
    private final LayoutInflater mInflater;
    private List<BiddingVehicles> mVehicles;
    private int mKeyPage = 0;
    private List<BiddingVehicles> mList;
    Context mContext;

    public DialogAdapterVehicles(Context context, int keyPage, List<BiddingVehicles> list) {
        mInflater = LayoutInflater.from(context);
        this.mKeyPage = keyPage;
        this.mList = list;
        mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mLisencePlate;
        private TextView mTonnage;
        private TextView mDriverName;
        private TextView mPhone;
        private TextView mIdCard;
        private ImageView mAvt;
        private LinearLayout lnViewScrCargo;

        public ViewHolder(View itemView) {
            super(itemView);
            mAvt = (ImageView) itemView.findViewById(R.id.ivAvtDialog);
            mLisencePlate = (TextView) itemView.findViewById(R.id.txtBXSDialog);
            mTonnage = (TextView) itemView.findViewById(R.id.txtTonnageDialog);
            mDriverName = (TextView) itemView.findViewById(R.id.txtNameDialog);
            mPhone = (TextView) itemView.findViewById(R.id.txtPhoneDialog);
            mIdCard = (TextView) itemView.findViewById(R.id.txtCmtDialog);
            lnViewScrCargo = itemView.findViewById(R.id.lnViewScrCargo);

        }
    }

    @NonNull
    @Override
    public DialogAdapterVehicles.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_bidding_vehicle, parent, false);
        return new DialogAdapterVehicles.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DialogAdapterVehicles.ViewHolder holder, int position) {
        BiddingVehicles currentData = mVehicles.get(position);
        holder.mLisencePlate.setText("BKS: " + currentData.getLisence_plate());
        //holder.mTonnage.setText(" | " + currentData.getCargo_types().get(0).getTotal_weight() + " tấn");
        holder.mDriverName.setText("Tài xê :" +currentData.getDriver_name());
        holder.mPhone.setText("Số điện thoại : " + currentData.getDriver_phone_number());
        holder.lnViewScrCargo.removeAllViews();
        // holder.mAvt.setImageResource();

        // tạo list quality + type cyar thông tin xe
        if (mKeyPage == EnumBidding.EnumBiddingBehavior.WAITING_FOR_CONFIRMATION) return;
        for (int i = 0; i < mList.get(position).getCargo_types().size(); i++) {
            addItemChill(holder.lnViewScrCargo, mList.get(position).getCargo_types().get(i));
        }
    }

    public void setAdapterBiddingVehicles(List<BiddingVehicles> data) {
        mVehicles = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mVehicles != null ? mVehicles.size() : 0;
    }

    public void addItemChill(LinearLayout linearLayout, CargoTypes cargo_types) {
        View newsListRow = mInflater.inflate(R.layout.item_total_cargo_info, null, false);
        TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);
        int cargo_quantity = cargo_types.getCargo_quantity();
        String type = cargo_types.getType();
        textView.setText(cargo_quantity + " " + type);
        linearLayout.addView(newsListRow);
    }
}
