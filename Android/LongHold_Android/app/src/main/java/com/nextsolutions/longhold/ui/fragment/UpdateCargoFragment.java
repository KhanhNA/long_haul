package com.nextsolutions.longhold.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.databinding.FragmentEditInfoCargoBinding;
import com.nextsolutions.longhold.model.PayloadDriverCargo;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.UpdateCargoVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

import java.util.ArrayList;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.GALLERY_PICTURE;

public class UpdateCargoFragment extends BaseFragment implements ActionsListener {

    private FragmentEditInfoCargoBinding mBinding;
    private UpdateCargoVM updateCargoVM;
    private String selectedImagePath;
    private Bitmap mImageAvarta;
    private Bitmap bitmap;
    private boolean isChangePicture = false;
    VehicleCargo vehicleCargo;
    private String result;
    private int KEY_ADD_UPDATE_CARGO;
    private int KEY_EDIT_INFO_CARGO;
    private ArrayList<String> listPayload = new ArrayList<>();
    private ArrayList<PayloadDriverCargo> driverCargoArrayList = new ArrayList<>();
    private String tonnage;
    private int idTonnage;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_edit_info_cargo;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return UpdateCargoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (FragmentEditInfoCargoBinding) binding;
        updateCargoVM = (UpdateCargoVM) viewModel;
        updateCargoVM.getListPayload(this::runUi);
        getData();
        initView();
        checkValueDate();
        return v;
    }

    private void checkValueDate() {
        mBinding.edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 10) {
                    ToastUtils.showToast(getString(R.string.input_number_phone));
                }
            }
        });
        mBinding.edtCMT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 10)
                    ToastUtils.showToast(getString(R.string.input_cmt));
            }
        });
    }

    private void initView() {
        if (KEY_ADD_UPDATE_CARGO == Constants.KEY_ADD) {
            createInfoDriver();
            setSpinner();
        }

        if (KEY_EDIT_INFO_CARGO == 5) {
            editInfoCargo();
            setSpinner();
        }
    }

    private void editInfoCargo() {
        listPayload.add(getString(R.string.pay_load));
        mBinding.tvTitleInfo.setText(R.string.update_cargo_edit_info);
        mBinding.edtName.setText(vehicleCargo.getDriver_name());
        mBinding.edtPhone.setText(vehicleCargo.getDriver_phone_number());
        mBinding.edtIDCargo.setText(vehicleCargo.getLisence_plate());
        mBinding.edtCMT.setText(vehicleCargo.getId_card());
        mBinding.tvPayLoad.setText(vehicleCargo.getTonnage());
        mBinding.btnEditSuccess.setVisibility(View.VISIBLE);
        mBinding.edtIDCargo.setEnabled(false);
        mBinding.tvClick.setVisibility(View.VISIBLE);
        loadImage();
    }

    private void loadImage() {
        if (vehicleCargo != null) {
            Glide.with(this)
                    .asBitmap()
                    .load("http://" + vehicleCargo.getImage_128())
                    .error(R.drawable.ic_avatar_account)
                    .centerCrop()
                    .into(mBinding.imgAvatar);
        }
    }


    private void setSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listPayload);
        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        mBinding.spPayLoad.setAdapter(adapter);
        mBinding.spPayLoad.setOnItemSelectedListener(new MyProcessEvent());
    }


    private void createInfoDriver() {
        listPayload.add(getString(R.string.pay_load));
        mBinding.tvTitleInfo.setText(R.string.update_info_cargo_create_new);
        mBinding.spPayLoad.setVisibility(View.VISIBLE);
        mBinding.imgDown.setVisibility(View.VISIBLE);
        mBinding.edtKg.setVisibility(View.GONE);
        mBinding.edtIDCargo.setEnabled(true);
        mBinding.tvClick.setVisibility(View.GONE);

    }

    private void getData() {
        vehicleCargo = getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getParcelable(Constants.EDIT_CARGO_DATA);
        KEY_ADD_UPDATE_CARGO = getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getInt(Constants.KEY_CREATE_DRIVER_CARGO);
        KEY_EDIT_INFO_CARGO = getActivity().getIntent().getExtras().getBundle(DATA_PASS_FRAGMENT).getInt(Constants.KEY_EDIT_INFO_CARGO);
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        switch (v.getId()) {
            case R.id.imgAvatar:
                selectImage();
                break;
            case R.id.imgBack:
            case R.id.btnCancel:
                getActivity().onBackPressed();
                break;

            case R.id.btnEditSuccess:
                if (KEY_ADD_UPDATE_CARGO == Constants.KEY_ADD) {
                    if (mBinding.edtPhone.getText().toString().length() > 10) {
                        ToastUtils.showToast(getString(R.string.input_number_phone));
                        return;
                    }
                    if (checkEmpty()) {
                        callApiCreate();
                    }
                }

                if (KEY_EDIT_INFO_CARGO == 5) {
                    if (mBinding.edtPhone.getText().toString().length() > 10) {
                        ToastUtils.showToast(getString(R.string.input_number_phone));
                        return;
                    }
                    if (checkEmpty()) {
                        callApiUpdate();
                    }
                }

                break;
            case R.id.tvClick:
                ToastUtils.showToast(getString(R.string.no_change_id_cargo));
                break;
        }
    }

    private void callApiUpdate() {
        for (int i =0; i<driverCargoArrayList.size();i++){
            String abc =mBinding.tvPayLoad.getText().toString();
            if (abc.equals(String.valueOf(driverCargoArrayList.get(i).getMax_tonage()))){
                idTonnage = driverCargoArrayList.get(i).getId();
            }
        }
            updateCargoVM.editInfoCargo(mBinding.edtPhone.getText().toString(), mBinding.edtName.getText().toString(), vehicleCargo.getId(), mBinding.edtCMT.getText().toString(), idTonnage, result, this::runUi);
    }

    private void callApiCreate() {
        updateCargoVM.createDriverCargo(mBinding.edtPhone.getText().toString(), mBinding.edtIDCargo.getText().toString(), idTonnage, mBinding.edtName.getText().toString(), mBinding.edtCMT.getText().toString(), result, this::runUi);
    }

    private boolean checkEmpty() {
        if (StringUtils.isNullOrEmpty(mBinding.edtName.getText().toString().trim())) {
            ToastUtils.showToast(getString(R.string.input_name));
            return false;
        } else if (StringUtils.isNullOrEmpty(mBinding.edtPhone.getText().toString().trim())) {
            ToastUtils.showToast(getString(R.string.input_phone));
            return false;
        } else if (StringUtils.isNullOrEmpty(mBinding.edtIDCargo.getText().toString())) {
            ToastUtils.showToast(getString(R.string.input_id_cargo));
            return false;
        } else if (StringUtils.isNullOrEmpty(mBinding.edtCMT.getText().toString())) {
            ToastUtils.showToast(getString(R.string.input_number_id));
            return false;
        }
        return true;
    }

    private void selectImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            openGallery();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_ASK_READ_EXTERNAL_PERMISSIONS);
                return;
            }
            openGallery();
        }
    }

    private void openGallery() {
        Intent pictureActionIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pictureActionIntent, Constants.GALLERY_PICTURE);
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_LIST_PAYLOAD_SUCCESS:
                for (PayloadDriverCargo pl : updateCargoVM.getListPayLoad().getValue().getRecords()) {
                    listPayload.add(String.valueOf(pl.getMax_tonage()) + " " + pl.getCode());
                    driverCargoArrayList.add(pl);
                }
                break;
            case Constants.GET_LIST_DRIVER_CARGO_ERROR:
                break;

            case Constants.UPDATE_DRIVER_CARGO_SUCCESS:
            case Constants.EDIT_CARGO_SUCCESS:
                final Intent data = new Intent();
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
                break;
            case Constants.EDIT_CARGO_ERROR:
                break;

            case Constants.UPDATE_DRIVER_CARGO_ERROR:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_PICTURE) {
            showGalary(data);
        }
    }

    private void showGalary(Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            try {
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                selectedImagePath = c.getString(columnIndex);
                c.close();
            } catch (Exception e) {
                ToastUtils.showToast("Load looix");
                return;
            }

            final BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, option);

            //  Calculate inSampleSize
            option.inSampleSize = calculateInSampleSize(option, 500, 500);
            option.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(selectedImagePath); // load
            // preview image
            if (bitmap != null) {
                bitmap960();

                mImageAvarta = bitmap;
                isChangePicture = true;
                Glide.with(getContext()).load(selectedImagePath).into(mBinding.imgAvatar);
                result = selectedImagePath;
//                mBinding.imgAvatar.setImageBitmap(mImageAvarta);
            } else {
                ToastUtils.showToast("xay ra loi");
            }
        }
    }

    private void bitmap960() {
        final int maxSize = 960;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if (inWidth > inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, true);
    }

    private int calculateInSampleSize(BitmapFactory.Options option, int reqHeight, int reqWidth) {
        // Raw height and width of image
        final int height = option.outHeight;
        final int width = option.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    private class MyProcessEvent implements
            AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            mBinding.tvPayLoad.setVisibility(View.GONE);
            if (position == 0) {
                mBinding.tvPayLoad.setVisibility(View.VISIBLE);
            } else {

                mBinding.tvPayLoad.setText(listPayload.get(position));
                String[] arr = mBinding.tvPayLoad.getText().toString().split(" ");

                for (int i = 0; i < arr.length; i++) {
                    tonnage =arr[0];
                }
                 getIdTonnage();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    private void getIdTonnage() {
        for (int i =0;i<driverCargoArrayList.size();i++){
            if (tonnage.equals(String.valueOf(driverCargoArrayList.get(i).getMax_tonage()))){
                idTonnage = driverCargoArrayList.get(i).getId();
            }
        }
    }
}
