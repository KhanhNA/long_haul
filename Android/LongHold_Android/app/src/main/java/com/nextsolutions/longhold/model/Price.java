package com.nextsolutions.longhold.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

public class Price  extends BaseModel {

    private Integer id;

    private Integer cargoId;

    private Integer price;

    private String status;

    public Integer getId() {
        return id;
    }

    public Integer getCargoId() {
        return cargoId;
    }

    public Integer getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }
}