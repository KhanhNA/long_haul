package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.nextsolutions.longhold.model.InformationUser;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

public class ProfileVM extends BaseViewModel {

    private final MutableLiveData<InformationUser> mInformation = new MutableLiveData<>();

    public MutableLiveData<InformationUser> getListInformation() {
        return mInformation;
    }


    private final MutableLiveData<String> mResultUpdate = new MutableLiveData<>();

    public MutableLiveData<String> getResultUpdate() {
        return mResultUpdate;
    }


    public ProfileVM(@NonNull Application application) {
        super(application);
        mInformation.setValue(AppController.getInformationUser());
    }

    public void confirmUpdateInformation(String name, String phone, String img, RunUi runUi) {
        DriverApi.updateInformationUer(name, phone, img, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o == null) {
                    return;
                }
                mResultUpdate.setValue((String) o);
                runUi.run(Constants.UPDATE_UI);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

}
