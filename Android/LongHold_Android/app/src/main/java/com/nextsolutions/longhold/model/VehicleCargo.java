package com.nextsolutions.longhold.model;

import android.os.Parcel;
import android.os.Parcelable;


import com.tsolution.base.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleCargo extends BaseModel implements Parcelable {
    int id;
    String code;
    String driver_name;
    String id_card;
    String lisence_plate;
    String driver_phone_number;
    String tonnage;
    String image_128;
    ArrayList<BiddingOrder> list_bidding_order;
    double latitude;
    double longitude;

    public VehicleCargo(Parcel in) {
        id = in.readInt();
        code = in.readString();
        driver_name = in.readString();
        id_card = in.readString();
        lisence_plate = in.readString();
        driver_phone_number = in.readString();
        tonnage = in.readString();
        image_128 = in.readString();
    }

    public static final Creator<VehicleCargo> CREATOR = new Creator<VehicleCargo>() {
        @Override
        public VehicleCargo createFromParcel(Parcel in) {
            return new VehicleCargo(in);
        }

        @Override
        public VehicleCargo[] newArray(int size) {
            return new VehicleCargo[size];
        }
    };

    public VehicleCargo() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeString(driver_name);
        dest.writeString(id_card);
        dest.writeString(lisence_plate);
        dest.writeString(driver_phone_number);
        dest.writeString(tonnage);
        dest.writeString(image_128);
    }

    @Getter
    @Setter
    public class BiddingOrder extends BaseModel {
        int id;
        String bidding_order_number;
    }
}
