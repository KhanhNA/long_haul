package com.nextsolutions.longhold.base;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.enums.EnumBidding;

public class Constants {
    public static final String FRAGMENT = "FRAGMENT";

    public static final String USER_NAME = "USER_NAME";
    public static final String MK = "MK";
    public static final String KEY_PAGE = "key_page";
    public static final String STATUS_NOTIFICATION = "status_notification";
    public static final String TIME = "time";
    public static final String BIDDING_ID = "bidding_order_id";
    public static final String GET_LIST_DRIVER_CARGO = "get_list_driver_cargo";
    public static final String GET_LIST_DRIVER_CARGO_ERROR = "get_list_driver_cargo_error";
    public static final String CONFIRM_SUCCESS = "confirm_success";
    public static final String CONFIRM_ERROR = "confirm_error";
    public static final String KEY_CREATE_DRIVER_CARGO = "create_driver_cargo";
    public static final int KEY_ADD = 10001;
    public static final String GET_LIST_PAYLOAD_SUCCESS = "get_list_payload_success";
    public static final String GET_LIST_PAYLOAD_ERROR = "get_list_payload_error";
    public static final String KEY_ORDER_ID = "key_order_id";
    public static final String KEY_EDIT_INFO_CARGO = "key_edit_info_cargo";
    public static final String EDIT_CARGO_SUCCESS = "editCargoSuccess";
    public static final String EDIT_CARGO_ERROR = "editCargoError";
    public static final String KEY_BIDDING_ID = "key_bidding_id";
    public static final String KEY_CREATE_DRIVER_CARGO_ERROR = "key_create_driver_error";
    public static final String EDIT_CARGO_DATA = "edit_cargo_data";
    public static final String KEY_ADD_EDIT_CARGO = "key_edit_info";
    public static final String UPDATE_DRIVER_CARGO_SUCCESS = "update_driver_cargo_success";
    public static final String UPDATE_DRIVER_CARGO_ERROR = "update_driver_cargo_error";
    public static final String DATA_INFORMATION = "data_information";
    public static final String GOOGLE_API_KEY_GEOCODE = "google_api_key_geocode";
    public static final String API_SUCCESS = "API_SUCCESS";
    public static final String API_FAIL = "API_FAIL";
    public static final String UPDATE_INFO_DRIVER_SUCCESS = "UPDATE_INFO_DRIVER_SUCCESS";
    public static final String UPDATE_INFO_DRIVER_FAILED = "UPDATE_INFO_DRIVER_FAILED";
    public static final String SUCCESS_API = "200";
    public static final String FAIL_API = "-1";
    public static final String DATA_PASS_FRAGMENT = "da_pass_fragment";
    public static final int KEY_START_ACTIVITY_FOR_RESULTS = 101;
    public static final int GALLERY_PICTURE = 2001;
    public static final String SUCCESS_PRICE = "success_price";
    public static final int KEY_EDIT_INFO = 404;
    public static final String DELETE_CARGO = "deleteCargo";
    public static final String EDIT_CARGO = "editCargo";
    public static final String VIEW_INFOR_CARGO = "viewCargo";
    public static final int REQUEST_CODE_ASK_READ_EXTERNAL_PERMISSIONS = 124;
    public static final int PERMISSION_REQUEST_CODE_EXTERNAL_STORAGE = 111;
    public static final String EXTRA_DATA = "extra_data";
    public static final String POSITION = "position";
    public static final String UPDATE_ADAPTER = "update_adapter";
    public static final String BIDDING_VEHICLES = "BiddingVehicles";
    public static final String DATA_SERIALIZABLE = "Data_serializable";
    public static final String REMOVE_CARGO = "remove_cargo";
    public static final String UPDATE_UI = "udapte_ui";
    public static final String ERROR_PHONE = "error_driver_phone_number";
    public static final String UPDATE_SUCCESS = "Update success";
    public static final String ERROR_ACCOUNT = "error: Account does not exist";
    public static final String INTENT_NOTIFICATION = "intern_notification";
    public static final String NO_MORE_DATA = "NO_MORE_DATA";
    public static final int RESULT_OK = 200;
    public final static String FIRST_USE = "FIRST_USE";
    public final static String SPACE = " ";
    public static final int LANG_EN = 2;
    public static int STATUS_SORT = R.id.btnAllCargo;
    public static int STATIC_SELECT_SORT = EnumBidding.EnumSortBiddingBehavior.PRICE_ASCENDING;
    public static int SORT_CARGO = R.id.btnAllCargo;

    public static int getStatusSort() {
        return STATUS_SORT;
    }

    public static void setStatusSort(int statusSort) {
        STATUS_SORT = statusSort;
    }

    public static int getSortCargo() {
        return SORT_CARGO;
    }

    public static void setSortCargo(int sortCargo) {
        SORT_CARGO = sortCargo;
    }

    public enum CODE {
        SUCCESS(200), NOT_FOUND(404), ERROR_INTERNAL(500), AUTHEN_FAIL(401), USER_DEFIEND(-200), FATAL_ERROR(-1);

        private Integer code;

        CODE(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }
}
