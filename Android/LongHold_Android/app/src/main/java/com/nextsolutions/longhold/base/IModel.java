package com.nextsolutions.longhold.base;

public interface IModel {
    String getModelName();
}
