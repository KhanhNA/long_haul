package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolutions.longhold.api.DriverVehicleInfoApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.Country;
import com.nextsolutions.longhold.model.Driver;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateDriverInfoVM extends BaseViewModel {
    ObservableField<Driver> infoDriver = new ObservableField<>();
    private List<Country> countries;
    private List<BaseModel> classDrivers;
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableBoolean isChangeAvatar = new ObservableBoolean();

    public UpdateDriverInfoVM(@NonNull Application application) {
        super(application);
        countries = new ArrayList<>();
        isChangeAvatar.set(false);
    }

    public void updateInfoDriver(RunUi runUi) {
        DriverVehicleInfoApi.updateInfoDriver(infoDriver.get(), isChangeAvatar.get(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                runUi.run(Constants.UPDATE_INFO_DRIVER_SUCCESS);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.UPDATE_INFO_DRIVER_FAILED);
            }
        });
    }

    public void getCountries(RunUi runUi) {
        isLoading.set(true);
        DriverVehicleInfoApi.getCountries(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                countries.clear();
                countries.addAll((Collection<? extends Country>) o);
                runUi.run("getCountriesSuccess");
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getCountriesFail");
                isLoading.set(false);
            }
        });
    }
}
