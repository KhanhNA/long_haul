package com.nextsolutions.longhold.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelVehicle extends BaseModel {
    Integer id;
    String name;
    String brand_name;
}
