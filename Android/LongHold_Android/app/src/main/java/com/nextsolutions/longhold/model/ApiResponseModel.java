package com.nextsolutions.longhold.model;

public class ApiResponseModel {
    public Integer id;
    public Integer status;
    public String message;

    public ApiResponseModel() {
    }

    public ApiResponseModel(Integer status, String message) {
        this.status = status;
        this.message = message;
    }
}
