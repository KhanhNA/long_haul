package com.nextsolutions.longhold.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalendarEvent extends RealmObject {
    public static final String ID_EVENT = "mIdEvent";
    @PrimaryKey
    @Required
    String id;
    String mIdEvent;
}
