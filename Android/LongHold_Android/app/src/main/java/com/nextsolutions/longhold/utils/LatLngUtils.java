package com.nextsolutions.longhold.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.directions.route.AbstractRouting;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import static com.nextsolutions.longhold.utils.StringUtils.API_GOOGLE_MAP_DESTINATION;

public class LatLngUtils {
    private static String mLocation;
    private static List<Address> addressList = null;
    private static Address address;
    private static Context mContext;

    public static LatLng getLatLngLocation(Context context, String location) {
        mLocation = location;
        mContext = context;
//        do {
            getGeocoder();
//        } while (addressList == null);
        if (address != null){
            return new LatLng(address.getLatitude(), address.getLongitude());

        }else {
            return new LatLng(0, 0);
        }
    }

    private static void getGeocoder() {
        Geocoder geocoder = new Geocoder(mContext);
        try {
            addressList = geocoder.getFromLocationName(mLocation, 5);
            if (addressList != null) {
                address = addressList.get(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
