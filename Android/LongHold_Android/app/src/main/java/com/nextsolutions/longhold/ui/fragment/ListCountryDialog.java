package com.nextsolutions.longhold.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.XBaseAdapter;
import com.nextsolutions.longhold.databinding.FragmentIncentivePackingBinding;
import com.nextsolutions.longhold.databinding.ListCountryDialogBinding;
import com.nextsolutions.longhold.model.Country;
import com.nextsolutions.longhold.utils.StringUtils;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListCountryDialog extends BottomSheetDialogFragment {
    private final List<Country> lstData;
    private Handler handler;
    private OnSelectedCountry onSelectedCountry;
    public ListCountryDialog(List<Country> lstData, OnSelectedCountry onSelectedCountry) {
        this.lstData = lstData;
        this.onSelectedCountry = onSelectedCountry;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ListCountryDialogBinding binding = DataBindingUtil.inflate(inflater, R.layout.list_country_dialog, container, false);

        XBaseAdapter adapter = new XBaseAdapter(R.layout.item_country, lstData, new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                onSelectedCountry.onSelectedCountry((Country) o);
                dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        RecyclerView rcIncentive = binding.rcIncentiveProduct;
        rcIncentive.setAdapter(adapter);
        rcIncentive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinimumHeight(height / 2);

        handler = new Handler();
        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(() -> adapter.setUpDateDat(search(s.toString())), 350);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return binding.getRoot();
    }

    public List<Country> search(String key) {
        if (StringUtils.isNullOrEmpty(key))
            return this.lstData;

        List<Country> rs = new ArrayList<>();
        for (Country temp : lstData) {
            if (temp.getName().toUpperCase().contains(key.toUpperCase())) {
                rs.add(temp);
            }
        }
        return rs;
    }

    public interface OnSelectedCountry{
        void onSelectedCountry(Country country);
    }
}