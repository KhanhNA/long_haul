package com.nextsolutions.longhold.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.databinding.DriverInfoFragmentBinding;
import com.nextsolutions.longhold.enums.CardType;
import com.nextsolutions.longhold.model.ClassDriver;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.utils.GlideEngine;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.DriverInfoVM;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.Calendar;
import java.util.List;

public class DriverInfoFragment extends BaseFragment implements View.OnClickListener {
    DriverInfoFragmentBinding mBinding;
    DriverInfoVM driverInfoVM;
    ListDialogFragment listDialogFragment;
    private int action;//1.Thêm, 2.Sửa, 3.Xóa
    private int position;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (DriverInfoFragmentBinding) binding;
        driverInfoVM = (DriverInfoVM) viewModel;

        initToolBar();
        getIntentData();
        initView();
        driverInfoVM.getCountries(this::runUi, false);
        return view;
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();
        Driver driver = (Driver) intent.getSerializableExtra("DRIVER");
        position = intent.getIntExtra("POSITION", -1);
        if (driver != null) {
            action = 2;
            driverInfoVM.getModel().set(driver);
            mBinding.toolbar.setTitle(R.string.edit_driver);
        } else {
            action = 1;
        }
    }

    private void initToolBar() {
        mBinding.toolbar.setTitle(R.string.driver_information);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getCountriesSuccess":
                boolean isShow = (boolean) objects[1];
                if (isShow) {
                    showSelectCountryDialog();
                }
                ToastUtils.showToast(getActivity(), "Get countries success", getResources().getDrawable(R.drawable.ic_clear));
                break;
            case "getCountriesFail":
                ToastUtils.showToast(getActivity(), getString(R.string.fail_to_get_countries), getResources().getDrawable(R.drawable.ic_clear));
                break;
            case "getClassDriverSuccess":
                ToastUtils.showToast(getActivity(), "Get class driver success", getResources().getDrawable(R.drawable.ic_clear));
                showSelectClassDriver();
                break;
            case "getClassDriverFail":
                ToastUtils.showToast(getActivity(), getString(R.string.fail_to_get_class_driver), getResources().getDrawable(R.drawable.ic_clear));
                break;
            case "saveDriverSuccess":
                if (this.action == 2) {
                    ToastUtils.showToast(getString(R.string.update_success));
                } else {
                    ToastUtils.showToast(getString(R.string.create_driver_success));
                }
                Intent intent = new Intent();
                intent.putExtra("DRIVER", driverInfoVM.getModelE());
                intent.putExtra("POSITION", position);
                intent.putExtra("ACTION", this.action);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                break;
            case "exitsDriver":
                ToastUtils.showToast(getActivity(), getString(R.string.msg_exits_driver_create), getResources().getDrawable(R.drawable.ic_clear));
                mBinding.etPhone.setError(getString(R.string.msg_exits_driver_create));
                break;
            case "saveDriverFail":
                ToastUtils.showToast(getActivity(), getString(R.string.msg_create_fail), getResources().getDrawable(R.drawable.ic_clear));
                break;
        }
    }

    private void showSelectClassDriver() {
        listDialogFragment = new ListDialogFragment(R.layout.item_class_driver, R.string.class_driver, getString(R.string.msg_select_class_driver)
                , driverInfoVM.getClassDrivers(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                ClassDriver classDriver = (ClassDriver) o;
                driverInfoVM.getModelE().setClass_driver(classDriver.getId());
                mBinding.etClassDriver.setText(classDriver.getName());
                listDialogFragment.dismiss();
                mBinding.etClassDriver.setError(null);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listDialogFragment.show(getChildFragmentManager(), listDialogFragment.getTag());
    }

    private void setMessage() {
        driverInfoVM.setMsg_invalid_field(getString(R.string.INVALID_FIELD));
        driverInfoVM.setMsg_over_18_age(getString(R.string.msg_over_18_age));
    }

    private void initView() {
        setMessage();

        mBinding.etGender.setOnClickListener(this);
        mBinding.etCountry.setOnClickListener(this);
        mBinding.etClassDriver.setOnClickListener(this);
        mBinding.etCardType.setOnClickListener(this);
        mBinding.etBirthDay.setOnClickListener(this);

        mBinding.imgAvatar.setOnClickListener(this);
        mBinding.imgFrontCard.setOnClickListener(this);
        mBinding.imgBackCard.setOnClickListener(this);

        mBinding.btnConfirm.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etGender:
                showSelectGenderDialog();
                break;
            case R.id.etCountry:
                showSelectCountryDialog();
                break;
            case R.id.etClassDriver:
                selectClassDriver();
                break;
            case R.id.etCardType:
                selectCardType(v);
                break;
            case R.id.etBirthDay:
                selectBirthDay();
                break;
            case R.id.imgAvatar:
                pickImage((ImageView) v, mBinding.lbAvatar);
                break;
            case R.id.imgFrontCard:
                pickImage((ImageView) v, mBinding.lbFrontCard);
                break;
            case R.id.imgBackCard:
                pickImage((ImageView) v, mBinding.lbBackCard);
                break;
            case R.id.btnConfirm:
                saveDriver();
                break;
        }
    }

    private void saveDriver() {
        driverInfoVM.saveDriverInfo(this::runUi);
    }

    @SuppressLint("NonConstantResourceId")
    private void pickImage(ImageView imageView, TextView label) {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.SINGLE)
                .forResult(new OnResultCallbackListener() {
                    @Override
                    public void onResult(List result) {
                        if (result != null && result.size() > 0) {
                            LocalMedia localMedia = ((LocalMedia) result.get(0));
                            switch (imageView.getId()) {
                                case R.id.imgAvatar:
                                    driverInfoVM.getModelE().setImage_1920(localMedia.getCompressPath());
                                    break;
                                case R.id.imgFrontCard:
                                    driverInfoVM.getModelE().setImage_license_frontsite(localMedia.getCompressPath());
                                    break;
                                case R.id.imgBackCard:
                                    driverInfoVM.getModelE().setImage_license_backsite(localMedia.getCompressPath());
                                    break;
                            }
                            driverInfoVM.getModel().notifyChange();
                            label.setError(null);

                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    private void selectBirthDay() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year1, month1, dayOfMonth);
            driverInfoVM.getModelE().setBirth_date(new OdooDate(calendar.getTime().getTime()));
            driverInfoVM.getModel().notifyChange();
            mBinding.etBirthDay.setError(null);

        }, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(c.getTime().getTime());
        datePickerDialog.show();
    }

    @SuppressLint({"RestrictedApi", "NonConstantResourceId"})
    private void selectCardType(View view) {
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_card_type, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_card:
                    driverInfoVM.getModelE().setCard_type(CardType.CARD);
                    mBinding.etCardType.setText(R.string.identity_card);
                    mBinding.ssn.setHint(getString(R.string.identity_card));
                    break;
                case R.id.action_passport:
                    driverInfoVM.getModelE().setCard_type(CardType.PASSPORT);
                    mBinding.etCardType.setText(R.string.passport);
                    mBinding.ssn.setHint(getString(R.string.passport));
                    break;
            }
            mBinding.etCardType.setError(null);
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void selectClassDriver() {
        if (driverInfoVM.getModelE().getCountry_id() == null) {
            ToastUtils.showToast(getString(R.string.msg_select_countries));
        } else {
            driverInfoVM.getClassDrivers(this::runUi);
        }
    }

    private void showSelectGenderDialog() {
        GenderDialog dialog = new GenderDialog(driverInfoVM.getModelE().getGender(), (gender, genderName) -> {
            driverInfoVM.getModelE().setGender(gender);
            mBinding.etGender.setText(genderName);
            mBinding.etGender.setError(null);
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    private void showSelectCountryDialog() {
        if (driverInfoVM.getCountries().size() > 0) {
            ListCountryDialog listDialogFragment = new ListCountryDialog(driverInfoVM.getCountries(), country -> {
                driverInfoVM.getModelE().setCountry_id(country.getId());
                driverInfoVM.getModelE().setClass_driver(null);
                mBinding.etCountry.setText(country.getName());
                mBinding.etCountry.setError(null);
            });
            listDialogFragment.show(getChildFragmentManager(), listDialogFragment.getTag());
        } else {
            driverInfoVM.getCountries(this::runUi, true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.driver_info_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DriverInfoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
