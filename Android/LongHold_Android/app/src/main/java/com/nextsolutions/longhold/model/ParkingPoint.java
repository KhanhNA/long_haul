package com.nextsolutions.longhold.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingPoint extends BaseModel {
    private Integer id;
    private String name;
    private String address;
    private String phone;
    private Float latitude;
    private Float longitude;

}
