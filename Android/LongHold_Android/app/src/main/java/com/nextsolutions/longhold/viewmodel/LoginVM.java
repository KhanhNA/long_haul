package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nextsolutions.longhold.api.BaseApi;
import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.StaticData;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.UserInfo;
import com.nextsolutions.longhold.utils.ApiResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;
    private SharedPreferences.Editor editor;


    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }


    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        userInfo.setGoogle_api_key_geocode(sharedPreferences.getString(Constants.GOOGLE_API_KEY_GEOCODE, ""));
        model.set(userInfo);
    }


    public void requestLogin() {
        if (model.get() != null) {
            UserInfo userInfo = model.get();
//            HttpHelper.requestApolloLogin(appException, userInfo.getUserName(), userInfo.getPassWord(), this::loginSuccess);
            BaseApi.requestLogin(userInfo.getUserName(), userInfo.getPassWord(), new IOdooResponse<OdooSessionDto>() {
                @Override
                public void onResponse(OdooSessionDto odooSessionDto, Throwable throwable) {
                    onLoginResponse(odooSessionDto, throwable);

                }

            });
        }

    }


    //forget password
    public void sendPassword() {
        // TODO: 21/02/2020 send OTP
        try {
            view.action("sendOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void confirmOTP() {
        // TODO: 21/02/2020 confirm OTP
        try {
            view.action("confirmOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void onLoginResponse(OdooSessionDto odooSessionDto, Throwable volleyError) {
        if (volleyError == null && odooSessionDto != null) {
            StaticData.setOdooSessionDto(odooSessionDto);
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);
            //login thành công -> lưu luôn account mặc định lần thứ 2 luôn luôn đăng nhập
            editor.putBoolean("chkSave", true);
            editor.putString(Constants.USER_NAME, getModelE().getUserName());
            editor.putString(Constants.MK, getModelE().getPassWord());
            if (odooSessionDto != null){
                editor.putString(Constants.GOOGLE_API_KEY_GEOCODE, odooSessionDto.getGoogle_api_key_geocode());
            }
            editor.commit();
            try {
                // TODO: 03/08/2020 lấy thông tin user từ thông tin lấy được khi login
                getUserInfo(odooSessionDto.getUid());
                responseLiveData.postValue(ApiResponse.success(StaticData.getOdooSessionDto()));
                // lấy thông tin User
//                DriverApi.getInformationUser();

            } catch (Exception e) {
                e.printStackTrace();
                responseLiveData.postValue(ApiResponse.error(new Throwable()));
            }

        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }


    private void getUserInfo(Integer uId) throws Exception {
        DriverApi.getDriverInfo(uId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                responseLiveData.postValue(ApiResponse.success(StaticData.getOdooSessionDto()));

            }

            @Override
            public void onFail(Throwable error) {
                responseLiveData.postValue(ApiResponse.error(error));
            }
        });

    }
}
