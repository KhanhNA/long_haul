package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.PrivilegesDTO;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRankingChildVM extends BaseViewModel {
    ObservableList<PrivilegesDTO> privilegesDTOS = new ObservableArrayList<>();
    public MyRankingChildVM(@NonNull Application application) {
        super(application);
    }
    public void getData(List<PrivilegesDTO> privilegesDTOS, RunUi runUi){
        this.privilegesDTOS.addAll(privilegesDTOS);
        runUi.run(Constants.API_SUCCESS);
    }

}
