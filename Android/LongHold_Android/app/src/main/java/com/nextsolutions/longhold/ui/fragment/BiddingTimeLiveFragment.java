package com.nextsolutions.longhold.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.HaiSer;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.database.CalendarEvent;
import com.nextsolutions.longhold.database.RealmCalendarEvent;
import com.nextsolutions.longhold.databinding.ChildBiddingFragmentBinding;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.CreateBiddingOrder;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.nextsolutions.longhold.model.SocketModel;
import com.nextsolutions.longhold.ui.activity.MainActivity;
import com.nextsolutions.longhold.ui.adapter.CalendarBiddingAdapter;
import com.nextsolutions.longhold.ui.viewcommon.CustomToast;
import com.nextsolutions.longhold.utils.HideViewOnScrollListener;
import com.nextsolutions.longhold.ui.viewcommon.BiddingBottomSheet;
import com.nextsolutions.longhold.ui.viewcommon.Dialog;
import com.nextsolutions.longhold.enums.EnumBidding;
import com.nextsolutions.longhold.utils.ReminderProvide;
import com.nextsolutions.longhold.utils.StringUtils;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.nextsolutions.longhold.viewmodel.PackageTimeChildBiddingVM;
import com.nextsolutions.longhold.viewmodel.CustomLoadMoreView;
import com.nextsolutions.longhold.viewmodel.SocketResult;
import com.nextsolutions.socket_notification.SocketIONavigation;
import com.nextsolutions.socket_notification.SocketIONavigationProvider;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.nextsolutions.longhold.base.Constants.DATA_PASS_FRAGMENT;
import static com.nextsolutions.longhold.base.Constants.KEY_START_ACTIVITY_FOR_RESULTS;
import static com.nextsolutions.longhold.base.Constants.TIME;
import static com.nextsolutions.longhold.utils.ReminderProvide.pushAppointmentsToCalender;

public class BiddingTimeLiveFragment extends BaseFragment implements BiddingBottomSheet.OnclickSortListBidding, AppBarLayout.OnOffsetChangedListener {

    private static BiddingTimeLiveFragment mCalendarHappenningBiddingFragment;
    public SocketIONavigation mSocketIONavigation;
    CountDownTimer countDownTimer;
    private String mDate;
    private ChildBiddingFragmentBinding childBiddingFragmentBinding;
    private PackageTimeChildBiddingVM mPackageTimeChildBiddingVM;
    private CalendarBiddingAdapter mBaseAdapter;
    private int offset = 0;
    // = 6 -> default giá tăng dần
    private int mOrderBy = 6;
    private int mPosition;
    private ArrayList<Long> mCountDowTimeMillisecond = new ArrayList<>();
    private boolean mIsCheckStatusPackage;
    private OdooResultDto<SocketModel> mBiddingPackageSocket = new OdooResultDto<>();

    private Dialog mDialogConfirm;

    private MainActivity mainActivity;
    private List<SocketModel> socketModelList = new ArrayList<>();
    private BiddingPackage biddingPackage;
    /**
     * Nhận sự kiện cập nhận dữ liệu từ socket để cập nhận dữ liệu
     *
     * @param args
     */
    private SocketResult socketResult;

    public static BiddingTimeLiveFragment getInstance() {
        if (mCalendarHappenningBiddingFragment == null) {
            mCalendarHappenningBiddingFragment = new BiddingTimeLiveFragment();
        }
        return mCalendarHappenningBiddingFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mSocketIONavigation = ((SocketIONavigationProvider) getActivity()).socketNavigation();
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
//        mCalendarBiddingFragmentBinding.swipeLayout.setEnabled(i == 0);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        upDateNotification();
//        refreshData();
//    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCalendarHappenningBiddingFragment = this;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.child_bidding_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PackageTimeChildBiddingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        childBiddingFragmentBinding = (ChildBiddingFragmentBinding) binding;
        mPackageTimeChildBiddingVM = (PackageTimeChildBiddingVM) viewModel;
        mBiddingPackageSocket.setRecords(socketModelList);
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        return view;
    }

    private void getData() {
        assert getArguments() != null;
        if (getArguments().getSerializable(Constants.DATA_PASS_FRAGMENT) != null) {
            mDate = getArguments().getString(Constants.KEY_PAGE);
            mPosition = getArguments().getInt(Constants.POSITION);
            mCountDowTimeMillisecond = (ArrayList<Long>) getArguments().getSerializable(Constants.DATA_PASS_FRAGMENT);
            mPackageTimeChildBiddingVM.requestBiddingPackage(mDate, mOrderBy, offset, this::runUi);
        }
    }

    private void initView() {

        setmIsCheckStatusPackage();
        // init recycler & adapter
        childBiddingFragmentBinding.recyclerBiddingPackage.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mBaseAdapter = new CalendarBiddingAdapter(R.layout.item_bidding_package, mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords(), mIsCheckStatusPackage, this);
        childBiddingFragmentBinding.recyclerBiddingPackage.setAdapter(mBaseAdapter);
        // set title countDow
        if (mIsCheckStatusPackage)
            childBiddingFragmentBinding.tvTitleCountDow.setText(getString(R.string.shut_dow_countdow_time));
        else
            childBiddingFragmentBinding.tvTitleCountDow.setText(getString(R.string.start_countdow_time));

        childBiddingFragmentBinding.btnFilter.setText(mPackageTimeChildBiddingVM.getTitleSort(mPackageTimeChildBiddingVM.getStatusSelectSort(), getContext()));
        childBiddingFragmentBinding.btnFilter.setOnClickListener(v -> new BiddingBottomSheet(getActivity(), BiddingTimeLiveFragment.this).show());

    }

    private void setmIsCheckStatusPackage() {
        //isCheckStatusPackage = true list thuốc danh sách đang đấu thầu
        //-------------------- = false list thuộc danh sách sắp bắt đầu
        if (mCountDowTimeMillisecond != null)
            mIsCheckStatusPackage = mPackageTimeChildBiddingVM.isCheckPackageStatus(mCountDowTimeMillisecond);

    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mBaseAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mBaseAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mBaseAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        childBiddingFragmentBinding.swipeLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        childBiddingFragmentBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }

    private void initCountDowView() {
        Log.e("reload", "initCountDowView: 1");
        if (mCountDowTimeMillisecond.size() > 1) {
            childBiddingFragmentBinding.tvCountDowTime.setVisibility(View.VISIBLE);
            countDowTime(mPosition);
        } else {
            childBiddingFragmentBinding.tvCountDowTime.setVisibility(View.GONE);
            childBiddingFragmentBinding.tvTitleCountDow.setVisibility(View.GONE);
        }
    }

    private void refreshData() {
        // refresh data
        mPackageTimeChildBiddingVM.clearData();
        offset = 0;
        mPackageTimeChildBiddingVM.sortBidding(mDate, mPackageTimeChildBiddingVM.getStatusSelectSort(), offset, this::runUi);
        Log.e("reload", "refreshData: BiddingTimeHappenningFragment");
    }

    private void loadMore() {
        int length = mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getLength();
        int limit = 10;
        if (length == limit) {
            mPackageTimeChildBiddingVM.sortBidding(mDate, mPackageTimeChildBiddingVM.getStatusSelectSort(), offset, this::runUi);

        } else {
            //thông báo không còn gì để load
            mBaseAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    private void offRefreshing() {
        childBiddingFragmentBinding.swipeLayout.setRefreshing(false);
        mBaseAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        switch (s) {
            case Constants.SUCCESS_API:
                offset++;
                mBaseAdapter.setList(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
                mBaseAdapter.notifyDataSetChanged();
                initCountDowView();
                emptyData(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
                offRefreshing();
                break;
            case Constants.FAIL_API:
                emptyData(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
                offRefreshing();
                break;
            case Constants.UPDATE_UI:
                mBaseAdapter.setList(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
                mBaseAdapter.notifyDataSetChanged();
                emptyData(mPackageTimeChildBiddingVM.getCalendarBidding().getValue().getRecords());
                break;
            case "createBiddingOrderSuccess":
                BiddingPackage biddingPackage = (BiddingPackage) objects[1];
                Intent intent = new Intent(getActivity(),ConfirmCargoFragmentV2.class);
                intent.putExtra("BIDDING_PACKAGE", biddingPackage);
                startActivity(intent);
                AppController.hideLoading();
                break;
            case "createBiddingOrderFail":
                AppController.hideLoading();
                ToastUtils.showToast(getString(R.string.something_wrong));
                break;
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        biddingPackage = (BiddingPackage) o;
        switch (v.getId()) {
            case R.id.viewGroup:
                Bundle args = new Bundle();
                args.putInt(StringUtils.TO_DETAIL_BIDDING_FRAGMENT_O, biddingPackage.getId());
                if (mIsCheckStatusPackage)
                    args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BIDDING_HAPPENNING);
                else
                    args.putInt(StringUtils.KEYPAGE, EnumBidding.EnumBiddingBehavior.BIDDING_UPCOMING);
                args.putSerializable(TIME, mCountDowTimeMillisecond);
                args.putInt(Constants.POSITION, mPosition);
                args.putSerializable(Constants.DATA_SERIALIZABLE, biddingPackage);
                Intent intent = new Intent(getActivity(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, DetailBiddingFragment.class);
                intent.putExtra(DATA_PASS_FRAGMENT, args);
                startActivity(intent);
                break;
            case R.id.btnBidding:
                // trạng thái đang diễn ra
                if (mIsCheckStatusPackage) {
                    if (childBiddingFragmentBinding.tvCountDowTime.getText().equals("00:00:00")) {
                        ToastUtils.showToast(getString(R.string.time_up_bidding));
                        return;
                    }
                    int bidding_id = biddingPackage.getId();
                    Date today = new Date();

                    mDialogConfirm = new Dialog(getString(R.string.confirm_bidding_title), null, (getString(R.string.confirm_cargo_truck_confirm)), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mPackageTimeChildBiddingVM.createBiddingOrder(biddingPackage, BiddingTimeLiveFragment.this::runUi);
                            AppController.showLoading(getActivity());
                            mDialogConfirm.dismiss();
                        }
                    });
                    mDialogConfirm.show(getChildFragmentManager(), "dialogConfirm");


                } else {
                    // trạng thái sắp bắt đầu
                    if (childBiddingFragmentBinding.tvCountDowTime.getText().equals("00:00:00")) {
                        ToastUtils.showToast(getString(R.string.title_notifi_notbidding));
                        return;
                    }
                    if (biddingPackage != null)
                        if (ReminderProvide.haveCalendarReadWritePermissions(getActivity())) {
                            addNewEvent(biddingPackage.getId() + "", biddingPackage);
                        } else {
                            ReminderProvide.requestCalendarReadWritePermission(getActivity());
                        }
                    break;
                }
                break;
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
    }

    public void countDowTime(int position) {
        Log.e("duongnk", "countDowTime: 1");

        long datePosition;
        long myCurrentTimeMillis = System.currentTimeMillis();
        // dang dien ra
        if (mIsCheckStatusPackage) {
            datePosition = mCountDowTimeMillisecond.get(position + 1);
        } else {
            // sap dien ra
            datePosition = mCountDowTimeMillisecond.get(position);
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(datePosition - myCurrentTimeMillis, 1000) {
            public void onTick(long millisUntilFinished) {
                childBiddingFragmentBinding.tvCountDowTime.setText(AppController.hmsTimeFormatter(millisUntilFinished));
            }

            public void onFinish() {
                childBiddingFragmentBinding.tvCountDowTime.setText("00:00:00");
            }
        }.start();

    }

    @Override
    public void onSortListBidding(int ketSort) {
        offset = 0;
        mPackageTimeChildBiddingVM.clearData();
        childBiddingFragmentBinding.btnFilter.setText(mPackageTimeChildBiddingVM.sortBidding(mDate, ketSort, offset, this::runUi));
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            childBiddingFragmentBinding.viewNotData.setVisibility(View.GONE);
            childBiddingFragmentBinding.recyclerBiddingPackage.setVisibility(mPackageTimeChildBiddingVM.getStatusNotification() ? View.VISIBLE : View.GONE);
        } else {
            childBiddingFragmentBinding.viewNotData.setVisibility(View.VISIBLE);
            childBiddingFragmentBinding.recyclerBiddingPackage.setVisibility(mPackageTimeChildBiddingVM.getStatusNotification() ? View.VISIBLE : View.GONE);
        }
    }

    public void upDateNotification() {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mPackageTimeChildBiddingVM.getStatusNotification())
                    childBiddingFragmentBinding.recyclerBiddingPackage.addOnScrollListener(new HideViewOnScrollListener(childBiddingFragmentBinding.appBarLayout));
                childBiddingFragmentBinding.recyclerBiddingPackage.setVisibility(mPackageTimeChildBiddingVM.getStatusNotification() ? View.VISIBLE : View.GONE);
                childBiddingFragmentBinding.appBarLayout.setVisibility((mPackageTimeChildBiddingVM.getStatusNotification() ? View.VISIBLE : View.GONE));
                childBiddingFragmentBinding.tvNotification.setVisibility((mPackageTimeChildBiddingVM.getStatusNotification() ? View.GONE : View.VISIBLE));
            }
        });

    }

    public void updateSocketData(Object... args) {
        socketModelList.clear();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
                .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        Log.e("duongnk", "messageSocket: " + args[0]);
        for (int j = 0; j < args.length; j++) {
            JSONObject data = (JSONObject) args[j];
            Log.d("duongnk", "call: " + data.toString());
            socketResult = gson.fromJson(data.toString(), SocketResult.class);
            socketModelList.addAll(socketResult.getResult().getRecords());
        }
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBiddingPackageSocket.setRecords(socketModelList);
                for (int i = 0; i < mBiddingPackageSocket.getRecords().size(); i++) {
                    String actionType = mBiddingPackageSocket.getRecords().get(i).getActionType();
                    switch (actionType) {
                        case EnumBidding.EnumSocketType.BiddingTime:
                            // Reload lại màn hình new home vc
                            break;
                        case EnumBidding.EnumSocketType.BiddingPackage:
                            // comment code chức nắng xóa bidding
                            // xoá object (nếu có) ở danh sách Đang đấu thầu (new home vc)
                            removeObjectEventSocket(mBiddingPackageSocket.getRecords().get(i).getLstBiddingPackages());
                            Log.e("duongnk", "new home vc: removeObjectEventSocket");
                            // reload các tab ở danh sách Đã đấu thầu (home vc) - thực hiện bên màn hình đã đấu thầu
                            break;
                        case EnumBidding.EnumSocketType.ChangePriceAction:
                            // Nếu status = 0 => update data của object tương ứng (new home vc)
                            // Nếu status = -1 => remove object đó khỏi danh sách đấu thầu (new home vc)
                            changePriceEventSocket(mBiddingPackageSocket.getRecords().get(i).getLstBiddingPackages());
                            Log.e("duongnk", "new home vc: changePriceEventSocket");
                            break;
                        case EnumBidding.EnumSocketType.OvertimeBiddingAction:
                            // thêm object vào danh sách Đang đấu thầu (new home vc)
                            addBiddingEventSocket(mBiddingPackageSocket.getRecords().get(i).getLstBiddingPackages(), BiddingTimeLiveFragment.this::runUi);
                            Log.e("duongnk", "new home vc: addBiddingEventSocket");
                            // xoá object (nếu có) ở danh sách Đã đấu thầu (home vc)
                            break;
                    }
                }
            }
        });
    }

    private void addBiddingEventSocket(List<BiddingPackage> lstBiddingPackages, RunUi runUi) {
        mPackageTimeChildBiddingVM.addBiddingSocket(lstBiddingPackages, runUi);
    }

    private void changePriceEventSocket(List<BiddingPackage> lstBiddingPackages) {
        mPackageTimeChildBiddingVM.changPriceEventSocket(lstBiddingPackages, this::runUi);
    }

    private void removeObjectEventSocket(List<BiddingPackage> lstBiddingPackages) {
        mPackageTimeChildBiddingVM.removeBiddingSocket(lstBiddingPackages, this::runUi);
    }

    /**
     * thêm nhắc nhở vào calendar
     *
     * @param id
     * @param o
     */
    private void addNewEvent(String id, Object o) {
        // kiểm tra sự kiện đã đc thêm ở trước đó chưa (true là chưa có và thực hiện add - false -> đã có và thực hiện xóa)
        if (isCheckIdEventCalendar(String.valueOf(id))) {
            BiddingPackage biddingPackage = (BiddingPackage) o;

            RealmCalendarEvent.getInstance(getContext()).addIdEvent(id);

            String from = (biddingPackage.getFrom_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getFrom_receive_time()) : "");
            String to = (biddingPackage.getTo_receive_time() != null ? AppController.formatDateTime.format(biddingPackage.getTo_receive_time()) : "");
            String price = (biddingPackage.getPrice() != null ? AppController.getInstance().formatCurrency(biddingPackage.getPrice()) : "");
            String content = "[bidding_order_number - " + biddingPackage.getDistance() + "km - " + price + "]" + "Gói thầu từ " + from + "-" + to;

            pushAppointmentsToCalender(getActivity(), "[LongHaul Bidding]", content, mCountDowTimeMillisecond.get(mPosition), true, 1, 1);
            refreshData();
            Toast.makeText(getActivity(), R.string.add_calendar, Toast.LENGTH_SHORT).show();
        } else {
            RealmCalendarEvent.getInstance(getContext()).deleteCalendar(id);
            ReminderProvide.deleteEventCalendar(id);
            refreshData();
            Toast.makeText(getActivity(), R.string.remove_calendar, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * kiem tra id da dc add vao calendar k
     * nếu đã đc add rồi thì sẽ thực hiện chức năng xóa nhắc nhở
     *
     * @param id
     * @return
     */
    private boolean isCheckIdEventCalendar(String id) {
        boolean isCheck = true;
        List<CalendarEvent> calendarEventList = RealmCalendarEvent.getInstance(AppController.getInstance().getBaseContext()).readCalendar();
        for (int i = 0; i < calendarEventList.size(); i++) {
            if (calendarEventList.get(i).getMIdEvent().equals(id)) {
                isCheck = false;
                break;
            }
        }
        return isCheck;
    }

    public void updateUI() {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                refreshData();
            }
        });

    }

}

