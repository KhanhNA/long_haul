package com.nextsolutions.longhold.fragmentnavigation;

public interface FragmentNavigationProvider {
    FragmentNavigation provideNavigation();
}
