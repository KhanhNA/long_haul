package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.base.StaticData;
import com.nextsolutions.longhold.model.MyRankingCustomer;
import com.nextsolutions.longhold.model.Partner;
import com.nextsolutions.longhold.model.RankingDTO;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRankingVM extends BaseViewModel {
    ObservableField<MyRankingCustomer> myRankingCustomer= new ObservableField<>();
    ObservableField<Integer> totalPointNextRank= new ObservableField<>();
    ObservableField<RankingDTO> rankCurrent= new ObservableField<>();
    ObservableList<RankingDTO> listRanking = new ObservableArrayList<>();
//    ObservableField<Partner> infoCustomer = new ObservableField<>();
    int index_rank_current=0;
    public MyRankingVM(@NonNull Application application) {
        super(application);
//        infoCustomer.set(StaticData.getOdooSessionDto().getP);
    }
    public void getData(RunUi runUi){
//        CustomerApi.getRankingCustomer(new SharingOdooResponse() {
//            @Override
//            public void onSuccess(Object o) {
//                getDataSuccess(o,runUi);
//            }
//        });

    }
    private void getDataSuccess(Object o,RunUi runUi){
        MyRankingCustomer myRankingCustomer= (MyRankingCustomer) o;
        if(myRankingCustomer!=null && myRankingCustomer.getList_rank()!=null && myRankingCustomer.getList_rank().size()>0){
            this.myRankingCustomer.set(myRankingCustomer);
            listRanking.addAll(myRankingCustomer.getList_rank());
            handleData(myRankingCustomer);
            runUi.run(Constants.API_SUCCESS);
        }
        else{
            runUi.run(Constants.API_FAIL);
        }
    }

    //getCurrent_rank()=="1" .rank hien tai
    private void handleData(MyRankingCustomer myRankingCustomer){
        int check_rank=0;
        for(RankingDTO item: myRankingCustomer.getList_rank()){
            if(item.getCurrent_rank().equals("1")){
                totalPointNextRank.set(item.getTo_point());
                index_rank_current = check_rank;
                rankCurrent.set(item);
                return;
            }
            check_rank++;
        }
    }

}
