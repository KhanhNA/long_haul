package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.database.CalendarEvent;
import com.nextsolutions.longhold.database.RealmCalendarEvent;
import com.nextsolutions.longhold.model.BiddingOrderDetail;
import com.nextsolutions.longhold.model.BiddingPackageDetail;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.model.CargoTypes;
import com.nextsolutions.longhold.model.BiddingPackageDetail.CargosPackage;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseViewModel;

import java.util.Date;
import java.util.List;

public class DetailBiddingFragmentVM extends BaseViewModel {
    //
    public MutableLiveData<BiddingOrderDetail> mBiddingOrder = new MutableLiveData<>();
    //V2
    public MutableLiveData<BiddingPackageDetail> mBiddingPackage = new MutableLiveData<>();

    public MutableLiveData<List<BiddingVehicles>> mBiddingVehicles = new MutableLiveData<>();

    public MutableLiveData<List<BiddingPackageDetail.BiddingVehiclePackage>> mBiddingVehiclesPackage = new MutableLiveData<>();
    public MutableLiveData<List<CargoTypes>> mCargos = new MutableLiveData<>();
    public MutableLiveData<List<CargoTypes>> mCargoTypes = new MutableLiveData<>();;


    public DetailBiddingFragmentVM(@NonNull Application application) {
        super(application);

    }

    //Lấy thông tin get_bidding_package_detail
    public void getBiddingPackage(String bidding_package_id, RunUi runUi) {
        DriverApi.getBiddingPackageDetail(bidding_package_id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o!=null){
                    OdooResultDto<BiddingPackageDetail> response = (OdooResultDto<BiddingPackageDetail>) o;
                    mBiddingPackage.setValue(response.getRecords().get(0));
                    mBiddingVehiclesPackage.setValue(response.getRecords().get(0).getBidding_vehicles());
                    runUi.run("getBiddingPackageSuccess");
                }else {
                    runUi.run("getBiddingFail");
                }
                Log.d("TAG", "onSuccess: ");
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getBiddingFail");
                Log.d("TAG", "onFail: "+error.getMessage());
            }
        });
    }


    public void getNewBiddingDetail(String biddingID, RunUi runUi) {
        DriverApi.getNewBiddingDetail(biddingID, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    OdooResultDto<BiddingOrderDetail> response = (OdooResultDto<BiddingOrderDetail>) o;
                    BiddingOrderDetail biddingOrderDetail =response.getRecords().get(0);
                    mBiddingOrder.setValue(biddingOrderDetail);
                    List<BiddingVehicles> biddingVehicles = response.getRecords().get(0).getBidding_order().getBidding_vehicles();
                    if (biddingVehicles != null) {
                        mBiddingVehicles.setValue(biddingVehicles);
                        if (biddingVehicles.size() > 0){
                            mCargoTypes.setValue(biddingVehicles.get(0).getCargo_types());
                        }
                    }
                    if (biddingOrderDetail.getBidding_order().getCargo_types() != null){
                        mCargos.setValue(biddingOrderDetail.getBidding_order().getCargo_types());
                    }
                    runUi.run("getNewBiddingSuccess");
                } else {
                    runUi.run("getBiddingFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getBiddingFail");
                Log.d("TAG", "onFail: "+error.getMessage());
            }
        });
    }


    // Xác nhận đấu thầu
    public void createBiddingOrder(String bidding_package_id, Date confirm_time, RunUi runUi) {

        DriverApi.createBiddingOrder(bidding_package_id, confirm_time, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o != null){
                    runUi.run("createBiddingOrderSuccess");
                }else {
                    runUi.run("createBiddingOrderFail");
                }
                
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("createBiddingOrderFail");
                Log.d("TAG", "onFail: "+error.getMessage());
            }
        });

    }

    public static void setTimeBidding(TextView view, OdooDateTime fromTime, OdooDateTime toTime, String address) {
        if (fromTime == null && toTime != null && address != null)
            view.setText(AppController.formatDateTime.format(toTime) + "\n" + address);
        if (fromTime != null && toTime == null && address != null)
            view.setText(AppController.formatDateTime.format(fromTime) + "\n" + address);
        if (fromTime != null && toTime != null && address != null)
            view.setText(AppController.formatDateTime.format(fromTime) + " - " + AppController.formatDateTime.format(toTime) + "\n" + address);
        if (fromTime == null && toTime == null && address == null)
            view.setText("");
        if (fromTime == null && toTime != null && address == null)
            view.setText(AppController.formatDateTime.format(toTime));
        if (fromTime != null && toTime == null && address == null)
            view.setText(AppController.formatDateTime.format(fromTime));
        if (fromTime != null && toTime != null && address == null)
            view.setText(AppController.formatDateTime.format(fromTime) + " - " + AppController.formatDateTime.format(toTime));
        if (fromTime == null && toTime == null && address != null)
            view.setText("\n"+address);
    }
    /**
     * kiem tra id da dc add vao calendar k
     * nếu đã đc add rồi thì sẽ thực hiện chức năng xóa nhắc nhở
     *
     * @param id
     * @return
     */
    public boolean isCheckIdEventCalendar(String id) {
        boolean isCheck = true;
        List<CalendarEvent> calendarEventList = RealmCalendarEvent.getInstance(AppController.getInstance().getBaseContext()).readCalendar();
        for (int i = 0; i < calendarEventList.size(); i++) {
            if (calendarEventList.get(i).getMIdEvent().equals(id)) {
                isCheck = false;
                break;
            }
        }
        return isCheck;
    }

}
