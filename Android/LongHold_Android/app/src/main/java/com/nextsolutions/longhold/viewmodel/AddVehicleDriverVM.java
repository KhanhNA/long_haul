package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.api.ConfirmCargoApi;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.enums.DriverStatus;
import com.nextsolutions.longhold.enums.VehicleType;
import com.nextsolutions.longhold.model.Driver;
import com.nextsolutions.longhold.model.VehicleDriver;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddVehicleDriverVM extends BaseViewModel<VehicleDriver> {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private boolean isChangeVehicle;//biến kiểm tra có thêm xe nào ko
    private boolean isChangeDriver;//biến kiếm tra có thêm tài xế nào ko
    private List<BaseModel> driverList;
    private List<BaseModel> vehicleList;

    public AddVehicleDriverVM(@NonNull Application application) {
        super(application);
    }


    public void getVehicles(RunUi runUi){
        if(vehicleList != null && !isChangeVehicle){
            runUi.run("getVehicleSuccess");
            return;
        }
        isLoading.set(true);
        ConfirmCargoApi.getListVehicles(VehicleType.ACTIVE,0,null,null,new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                List<BaseModel> vehicles = (List<BaseModel>) o;
                if(vehicles != null && vehicles.size() > 0){
                    vehicleList = vehicles;
                    runUi.run("getVehicleSuccess");
                }else {
                    runUi.run("getVehicleFail");
                    isLoading.set(true);
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getVehicleFail");
            }
        });
    }

    public void getDrivers(RunUi runUi){
        if(driverList != null && !isChangeDriver){
            runUi.run("getDriverSuccess");
            return;
        }
        isLoading.set(true);
        ConfirmCargoApi.getListVehicleDrivers(0, DriverStatus.ACCEPTED, null, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isChangeDriver = false;
                isLoading.set(false);
                List<BaseModel> drivers = (List<BaseModel>) o;
                if (drivers != null && drivers.size() > 0) {
                    driverList = drivers;
                    runUi.run("getDriverSuccess");
                } else {
                    runUi.run("getDriverFail");
                    isLoading.set(true);
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getDriverFail");
            }
        });
    }

    public boolean isValid() {
        boolean isValid = true;
        if(getModelE().getDriver() == null){
            addError("driver", R.string.driver, true);
            isValid = false;
        }else {
            clearErro("driver");
        }

        if(getModelE().getVehicle() == null){
            addError("vehicle", R.string.vehicle, true);
            isValid = false;
        }else {
            clearErro("vehicle");
        }
        return isValid;
    }
}
