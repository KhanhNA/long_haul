package com.nextsolutions.longhold.enums;

public class DriverStatus {
    public static final String ALL = "";
    public static final String WAITING = "waiting";
    public static final String ACCEPTED = "accepted";
    public static final String REJECTED = "rejected";
}
