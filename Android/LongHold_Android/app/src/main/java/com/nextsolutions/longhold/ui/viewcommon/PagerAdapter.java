package com.nextsolutions.longhold.ui.viewcommon;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nextsolutions.longhold.ui.fragment.BiddingTimeLiveFragment;
import com.nextsolutions.longhold.ui.fragment.BiddingTimeRemindFragment;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager, BEHAVIOR_SET_USER_VISIBLE_HINT);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);

    }


    public void clearFragment() {
        mFragmentList.clear();
    }


    @Override
    public int getItemPosition(Object object) {


        if (object instanceof BiddingTimeLiveFragment) {
            BiddingTimeLiveFragment f = (BiddingTimeLiveFragment) object;
            f.updateUI();

        } else if (object instanceof BiddingTimeRemindFragment) {
            BiddingTimeRemindFragment biddingTimeRemindFragment = (BiddingTimeRemindFragment) object;
            biddingTimeRemindFragment.updateUI();
        }
        return super.getItemPosition(object);

    }
}