package com.nextsolutions.longhold.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignationLog extends BaseModel {
    private Integer id;
    private String assignation_log_code;
    private OdooDateTime date_start;
    private OdooDateTime date_end;
}
