package com.nextsolutions.longhold.api;


import android.util.Log;

import com.nextsolutions.longhold.R;
import com.nextsolutions.longhold.base.AppController;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.SOService;
import com.nextsolutions.longhold.base.StaticData;
import com.nextsolutions.longhold.enums.EnumOrderBy;
import com.nextsolutions.longhold.model.BiddingDetail;
import com.nextsolutions.longhold.model.BiddingInformation;
import com.nextsolutions.longhold.model.BiddingOrderDetail;
import com.nextsolutions.longhold.model.BiddingPackage;
import com.nextsolutions.longhold.model.BiddingPackageDetail;
import com.nextsolutions.longhold.model.BiddingVehicles;
import com.nextsolutions.longhold.model.ConfirmBidding;
import com.nextsolutions.longhold.model.CreateBiddingOrder;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.nextsolutions.longhold.model.DriverCargo;
import com.nextsolutions.longhold.model.InformationUser;
import com.nextsolutions.longhold.model.NotificationModel;
import com.nextsolutions.longhold.model.PayloadDriverCargo;
import com.nextsolutions.longhold.model.Price;
import com.nextsolutions.longhold.model.RateModel;
import com.nextsolutions.longhold.model.VehicleCargo;
import com.nextsolutions.longhold.utils.ToastUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverApi extends BaseApi {

    public static void updateToken(String fcmToken, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("fcm_token", fcmToken);

            mOdoo.callRoute("/server/save_token", params, new SharingOdooResponse<OdooResultDto<String>>() {
                @Override
                public void onSuccess(OdooResultDto<String> obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * uId param
     * response :listener Callback Api
     *
     * @param uId
     * @param response
     */
    public static void getDriverInfo(Integer uId, IResponse response) {
        StaticData.sessionCookie = mOdoo.getSessionCokie();
//        JSONObject params;
//        try {
//            // chuyền param
//            params = new JSONObject();
//            params.put("uid", 20);
//            //  "/server/save_token" -> đường dẫn Api
//            mOdoo.callRoute("/fleet/driver", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
//                @Override
//                public void onSuccess(OdooResultDto<Driver> obj) {
//                    response.onSuccess(obj.getRecords().get(0));
//                }
//
//                @Override
//                public void onFail(Throwable error) {
//
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void getInformationBidding(String type, String search_text, Integer offset, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("type", type);
            params.put("search_code", search_text);
            params.put("offset", offset);
            params.put("order", EnumOrderBy.NEWEST);
            params.put("limit", 10);
            mOdoo.callRoute("/bidding/get_bidding_order_bidded", params, new SharingOdooResponse<OdooResultDto<BiddingInformation>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingInformation> biddingInformationOdooResultDto) {
                    response.onSuccess(biddingInformationOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                    Log.e("acv", "onFail: " + error.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // TODO: 8/13/2020 get list drivercargo
    public static void listBiddingVehicle(IResponse iResponse) {
        JSONObject params = new JSONObject();
        try {
//            params.put("status", uid);
            mOdoo.callRoute("/bidding/list_bidding_vehicle", params, new SharingOdooResponse<OdooResultDto<BiddingVehicles>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingVehicles> resultDto) {
                    iResponse.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // TODO: 8/14/2020 thêm tài xế vào đơn tungnt
    public static void addDriverCargo(String bidding_order_id, String bidding_vehicle_ids, IResponse iResponse) {
        JSONObject params = new JSONObject();
        try {
            params.put("bidding_order_id", bidding_order_id);
            params.put("bidding_vehicle_ids", bidding_vehicle_ids);

            mOdoo.callRoute("/bidding/confirm_bidding_vehicle_for_bidding_order", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    iResponse.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: 8/17/2020 Create thông tin lái xe 


    // TODO: 8/17/2020 lấy danh sách trọng tải
    public static void getListPayload(IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();
        try {
            mOdoo.callRoute("/bidding/get_vehicle_tonnage", params, new SharingOdooResponse<OdooResultDto<PayloadDriverCargo>>() {
                @Override
                public void onSuccess(OdooResultDto<PayloadDriverCargo> response) {
                    iResponse.onSuccess(response);
                }

                @Override
                public void onFail(Throwable error) {
                    {
                        iResponse.onFail(error);

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void confirmBidding(String bidding_order_id, String
            bidding_vehicle_id, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", bidding_order_id);
            params.put("bidding_vehicle_id", bidding_vehicle_id);

            mOdoo.callRoute("/bidding/confirm_bidding_vehicle", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean odooResultDto) {
                    iResponse.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //call api ConfirmBidding
    public static void setConfirmBidding(int cargoId, int useId, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("cargo_id", cargoId);
            params.put("use_id", useId);
            mOdoo.callRoute("/bidding/bidding_cargo", params, new SharingOdooResponse<OdooResultDto<ConfirmBidding>>() {
                @Override
                public void onSuccess(OdooResultDto<ConfirmBidding> mConfirmBidding) {
                    response.onSuccess(mConfirmBidding);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void editBiddingInformation(String bidding_vehicle_id, String
            bidding_id, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_vehicle_id", bidding_vehicle_id);
            params.put("bidding_order_id", bidding_id);

            mOdoo.callRoute("/bidding/edit_bidding_information", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean odooResultDto) {
                    iResponse.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //lấy về thông tin  bidding detail
    public static void getBiddingDetail(String bidding_order_id, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", bidding_order_id);

            mOdoo.callRoute("/bidding/get_bidding_detail", params, new SharingOdooResponse<OdooResultDto<BiddingDetail>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingDetail> mBiddingDetail) {
                    response.onSuccess(mBiddingDetail);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get history Bidding
     */

    public static void getHistoryBidding(String fromDate, String toDate,String txt_search, int order_by,
                                         int offset, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("from_date", fromDate);
            params.put("to_date", toDate);
            params.put("order_by", order_by);
            params.put("txt_search", txt_search);
            params.put("offset", offset);
            params.put("limit", 10);
            mOdoo.callRoute("/bidding/get_list_bidding_order_history_enterprise", params, new SharingOdooResponse<OdooResultDto<BiddingInformation>>() {

                @Override
                public void onSuccess(OdooResultDto<BiddingInformation> biddingInformationOdooResultDto) {
                    iResponse.onSuccess(biddingInformationOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * get price history Bidding
     */

    public static void getPriceHistoryBidding(String fromDate, String toDate, IResponse
            iResponse) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("fromDate", fromDate);
            params.put("toDate", toDate);
            params.put("status", 0);
            mOdoo.callRoute("/bidding/get_history_bidding_price", params, new SharingOdooResponse<OdooResultDto<Price>>() {
                @Override
                public void onSuccess(OdooResultDto<Price> priceOdooResultDto) {
                    iResponse.onSuccess(priceOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (
                JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * get list cargo
     *
     * @param text_search
     * @param offset
     * @param
     * @param status
     * @param iResponse
     */
    public static void getVehicleCargo(String text_search, int offset, int status, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("txt_search", text_search);
            params.put("offset", offset);
            params.put("limit", 10);
            params.put("status", status);
            mOdoo.callRoute("/bidding/list_manager_bidding_vehicle", params, new SharingOdooResponse<OdooResultDto<VehicleCargo>>() {
                @Override
                public void onSuccess(OdooResultDto<VehicleCargo> vehicleCargoOdooResultDto) {
                    iResponse.onSuccess(vehicleCargoOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * xacs nhanaj edit thong tin
     */
    public static void editInfoCargo(String phoneNumber, String linsence_plate, Integer
            tonnage, String number_name, String id_card, IResponse iResponse) {
        JSONObject driverInfo = new JSONObject();
        try {
            driverInfo.put("driver_phone_number", phoneNumber);
            driverInfo.put("lisence_plate", linsence_plate);
            driverInfo.put("id_tonnage", tonnage);
            driverInfo.put("driver_name", number_name);
            driverInfo.put("id_card", id_card);

            mOdoo.callRoute("bidding/update_account_driver", driverInfo, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    Log.e("tungnt", "onSuccess: ");
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Xóa thông tin xe
     *
     * @param vehicle_id
     */
    public static void removeVehicleCargo(int vehicle_id, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("vehicle_id", vehicle_id);
            mOdoo.callRoute("/bidding/delete_account_driver", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    response.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestGetBiddingOrderDetail(String bidding_package_id, int offset, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_vehicle_id", bidding_package_id);
            params.put("offset", offset);
            params.put("limit", 10);
            mOdoo.callRoute("/bidding/get_list_bidding_order_by_bidding_vehicle_id", params, new SharingOdooResponse<OdooResultDto<BiddingInformation>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingInformation> biddingInformationOdooResultDto) {
                    iResponse.onSuccess(biddingInformationOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getBiddingPackageTime(IResponse response) {
        JSONObject params = new JSONObject();

        try {
            mOdoo.callRoute("/bidding/bidding_package_time", params, new SharingOdooResponse<OdooResultDto<OdooDateTime>>() {
                @Override
                public void onSuccess(OdooResultDto<OdooDateTime> calendarBiddingOdooResultDto) {
                    response.onSuccess(calendarBiddingOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getBiddingPackage(String bidding_time, int order_by,
                                         int offset, IResponse iResponse) {
        JSONObject params = new JSONObject();
        try {
            params.put("bidding_time", bidding_time);
            params.put("order_by", order_by);
            params.put("offset", offset);
            params.put("limit", 10);
            mOdoo.callRoute("/bidding/get_bidding_package_information", params, new SharingOdooResponse<OdooResultDto<BiddingPackage>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingPackage> biddingPackageOdooResultDto) {
                    iResponse.onSuccess(biddingPackageOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Tạo tài khoản thông tin xe
    public static void creatAccountDriver(String driver_phone_number, String lisence_plate, int id_tonnage, String driver_name, String id_card, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("driver_phone_number", driver_phone_number);
            params.put("lisence_plate", lisence_plate);
            params.put("id_tonnage", id_tonnage);
            params.put("driver_name", driver_name);
            params.put("id_card", id_card);

            mOdoo.callRoute("/bidding/creat_account_driver", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean odooResultDto) {
                    iResponse.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getBiddingPackageDetail(String bidding_package_id, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_package_id", bidding_package_id);

            mOdoo.callRoute("/bidding/get_bidding_package_detail", params, new SharingOdooResponse<OdooResultDto<BiddingPackageDetail>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingPackageDetail> odooResultDto) {
                    iResponse.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getNewBiddingDetail(String biddingID, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", biddingID);
            mOdoo.callRoute("/bidding/get_bidding_order_detail", params, new SharingOdooResponse<OdooResultDto<BiddingOrderDetail>>() {

                @Override
                public void onSuccess(OdooResultDto<BiddingOrderDetail> biddingPackageV2OdooResultDto) {
                    Log.e("duongnk", "onSuccess: bidding new");
                    iResponse.onSuccess(biddingPackageV2OdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void createBiddingOrder(String bidding_package_id, Date confirm_time, IResponse iResponse) {
        JSONObject params;
        params = new JSONObject();


        try {
            params.put("bidding_package_id", bidding_package_id);
            params.put("confirm_time", AppController.formatDateToString(confirm_time, "", "UTC"));

            mOdoo.callRoute("/bidding/create_bidding_order", params, new SharingOdooResponse<OdooResultDto<CreateBiddingOrder>>() {
                @Override
                public void onSuccess(OdooResultDto<CreateBiddingOrder> odooResultDto) {
                    iResponse.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    iResponse.onFail(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDriverCargo(String driver_phone_number, String lisence_plate, int id_tonnage, String driver_name, String id_card, String listPicture, IResponse iResponse) {
        SOService soService = (SOService) mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part part = null;
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("driver_phone_number", driver_phone_number);
            params.put("lisence_plate", lisence_plate);
            params.put("id_tonnage", id_tonnage);
            params.put("driver_name", driver_name);
            params.put("id_card", id_card);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), String.valueOf(params));
        if (listPicture != null) {
            File file = new File(listPicture);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        }
        Call<CreateSuccess> call = soService.upLoadImage(mOdoo.getSessionCokie(), part, bodyJson);
        call.enqueue(new Callback<CreateSuccess>() {
            @Override
            public void onResponse(Call<CreateSuccess> call, Response<CreateSuccess> response) {
                String data = response.body().getResult();
                if (response != null) {
                    showResult(data);
                }
                if (data.equals("Create success")) {
                    iResponse.onSuccess(data);
                }
            }

            @Override
            public void onFailure(Call<CreateSuccess> call, Throwable t) {
                iResponse.onFail(t);
            }
        });
    }

    private static void showResult(String data) {
        switch (data) {
            case "Update success":
                ToastUtils.showToast(R.string.update_success);
                break;
            case "Create success":
                ToastUtils.showToast(R.string.create_account_success);
                break;
            case "error_lisence_plate":
                ToastUtils.showToast(R.string.account_is_exist);
                break;
            case "error_id_tonnage":
                ToastUtils.showToast(R.string.payload_is_exist);
                break;
            case "error_driver_phone_number":
                ToastUtils.showToast(R.string.number_phone_is_exist);
                break;
            case "error_id_card":
                ToastUtils.showToast(R.string.number_id_is_exist);
                break;
            case "error : You do not have permission to create":
                ToastUtils.showToast(R.string.account_is_exist);
                break;
            case "error : You do not have permission to update":
                ToastUtils.showToast(R.string.no_update);
                break;
            case "The phone number is wrong format.(eg. 0123456789)":
                ToastUtils.showToast(R.string.input_number_format);
                break;
            case "The phone number must be equals 10 numbers":
                ToastUtils.showToast(R.string.phone_number_enter_10);
                break;
        }
    }

    // TODO: 8/18/2020 edit cargo
    public static void editInfoCargo(String driver_phone_number, String driver_name, int id_vehicle, String id_card, int id_tonnage, String picture, IResponse iResponse) {
        SOService soService = (SOService) mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part part = null;
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("driver_phone_number", driver_phone_number);
            params.put("driver_name", driver_name);
            params.put("id_vehicle", id_vehicle);
            params.put("id_card", id_card);
            params.put("id_tonnage", id_tonnage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), String.valueOf(params));
        if (picture != null) {
            File file = new File(picture);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        }
        Call<CreateSuccess> call = soService.upDateInfo(mOdoo.getSessionCokie(), part, bodyJson);
        call.enqueue(new Callback<CreateSuccess>() {
            @Override
            public void onResponse(Call<CreateSuccess> call, Response<CreateSuccess> response) {
                String data = response.body().getResult();
                if (response != null) {
                    showResult(data);
                }
                if (data.equals("Update success")) {
                    iResponse.onSuccess(data);
                }
            }

            @Override
            public void onFailure(Call<CreateSuccess> call, Throwable t) {
                iResponse.onFail(t);
            }
        });
    }

    public static void getNotification(int offset, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("type", "bidding_order");
            params.put("offset", offset);

            mOdoo.callRoute("/bidding/get_bidding_notification_history", params, new SharingOdooResponse<OdooResultDto<NotificationModel>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationModel> notificationModelOdooResultDto) {
                    response.onSuccess(notificationModelOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmRedNotification(int id, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("id", id);
            mOdoo.callRoute("/notification/is_read", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    response.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changPassWord(String oldPass, String newPass, IResponse response) {
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("old_password", oldPass);
            params.put("new_password", newPass);
            mOdoo.callRoute("/server/change_password", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    response.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void acceptNotification(String accept_firebase, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("accept_firebase.", accept_firebase);
            mOdoo.callRoute("/notification/accept_firebase_notification", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    response.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void getInformationUser() {
        JSONObject params;
        params = new JSONObject();
        try {
            mOdoo.callRoute("/share_van_order/get_customer_information", params, new SharingOdooResponse<OdooResultDto<InformationUser>>() {


                @Override
                public void onSuccess(OdooResultDto<InformationUser> informationUserOdooResultDto) {
                    //check null
                    if (informationUserOdooResultDto != null && informationUserOdooResultDto.getRecords() != null) {
                        AppController.setInformationUser(informationUserOdooResultDto.getRecords().get(0));
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateInformationUer(String name, String phone, String img, IResponse iResponse) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part part = null;
        JSONObject params;
        params = new JSONObject();
        try {
            params.put("name", name);
            params.put("phone", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), String.valueOf(params));
        if (img != null) {
            File file = new File(img);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        }
        Call<CreateSuccess> call = soService.updateInformationUser(mOdoo.getSessionCokie(), part, bodyJson);
        call.enqueue(new Callback<CreateSuccess>() {
            @Override
            public void onResponse(Call<CreateSuccess> call, Response<CreateSuccess> response) {
                if (response == null) return;
                String data = response.body().getResult();
                if (data.equals("Update success")) {
                    iResponse.onSuccess(data);
                }
            }

            @Override
            public void onFailure(Call<CreateSuccess> call, Throwable t) {
                iResponse.onFail(t);
            }
        });
    }

    public static void getListVehiclesRate(int offset, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("offset", offset);
            params.put("limit", 10);
            mOdoo.callRoute("/bidding/get_all_bidding_order_rating", params, new SharingOdooResponse<OdooResultDto<RateModel>>() {


                @Override
                public void onSuccess(OdooResultDto<RateModel> rateModelOdooResultDto) {
                    if (rateModelOdooResultDto == null) return;
                    response.onSuccess(rateModelOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}




