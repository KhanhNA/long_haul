package com.nextsolutions.longhold.model;

import com.nextsolutions.longhold.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingPackageDetail extends BaseModel {
    private Integer id;
    private Integer bidding_order_id;
    private String bidding_package_number;
    private String status;
    private String confirm_time;
    private String release_time;
    private String bidding_time;
    private Integer max_count;
    private FromDepot from_depot;
    private ToDepot to_depot;
    private Float total_weight;
    private Float distance;
    private Float from_latitude;
    private Float from_longitude;
    private Float to_latitude;
    private Float to_longitude;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private Double price;
    private String create_date;
    private String write_date;
    private Integer total_cargo;
    private List<CargosPackage> cargos;
    private List<BiddingVehiclePackage> bidding_vehicles;
    private List<CargoTypes> cargo_types;


    public String getPrice_str(){
        return this.price != null ? AppController.getInstance().formatCurrency(price) : "";
    }

    @Getter
    @Setter
    public class BiddingVehiclePackage extends BaseModel {
        private Integer id;
        private String status;
        private String lisence_plate;
        private String driver_phone_number;
        private String driver_name;
        private String create_date;
        private String write_date;
        private String id_card;
        private Integer tonnage;
        private List<ActionLog> action_log;
        private String bidding_vehicle_seq;
    }
    @Getter
    @Setter
    public class CargosPackage extends BaseModel {
        private int id;
        private String cargo_number;
        private int from_depot_id;
        private int to_depot_id;
        private int distance;
        private int size_id;
        private String create_date;
        private String write_date;
        private int weight;
        private String description;
        private SizeStandard size_standard;

    }
}
