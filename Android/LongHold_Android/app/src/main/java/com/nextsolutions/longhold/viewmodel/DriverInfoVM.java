package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.nextsolutions.longhold.api.DriverVehicleInfoApi;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.base.StaticData;
import com.nextsolutions.longhold.model.ApiResponseModel;
import com.nextsolutions.longhold.model.Country;
import com.nextsolutions.longhold.model.Driver;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverInfoVM extends BaseViewModel<Driver> {
    private String msg_invalid_field;
    private String msg_over_18_age;

    private List<Country> countries;
    private List<BaseModel> classDrivers;
    private ObservableBoolean isLoading = new ObservableBoolean();


    public DriverInfoVM(@NonNull Application application) {
        super(application);
        countries = new ArrayList<>();
        classDrivers = new ArrayList<>();
        model.set(new Driver());
    }

    public void getCountries(RunUi runUi, boolean isShowDialog) {
        isLoading.set(true);
        DriverVehicleInfoApi.getCountries(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                countries.clear();
                countries.addAll((Collection<? extends Country>) o);
                runUi.run("getCountriesSuccess", isShowDialog);
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getCountriesFail");
                isLoading.set(false);
            }
        });
    }

    public void getClassDrivers(RunUi runUi) {
        isLoading.set(true);
        DriverVehicleInfoApi.getDrivers(getModelE().getCountry_id(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                classDrivers.clear();
                classDrivers.addAll((Collection<? extends BaseModel>) o);
                runUi.run("getClassDriverSuccess");
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getClassDriverFail");
                isLoading.set(false);
            }
        });
    }

    public void saveDriverInfo(RunUi runUi) {
        if (!isValid()) {
            return;
        }
        isLoading.set(true);
        DriverVehicleInfoApi.saveDriverInfo(getModelE(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                ApiResponseModel apiResponseModel = (ApiResponseModel) o;
                if (apiResponseModel == null) {
                    runUi.run("saveDriverFail");
                    return;
                }
                if (apiResponseModel.status == 200) {
                    runUi.run("saveDriverSuccess");
                } else if (apiResponseModel.status == 1002) {
                    runUi.run("exitsDriver");
                } else {
                    runUi.run("saveDriverFail");

                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("saveDriverFail");
                isLoading.set(false);
            }
        });
    }

    private boolean isValid() {
        boolean isValid = true;
        Driver driver = getModelE();
        if (driver.getFull_name() == null || driver.getPhone().trim().length() == 0) {
            isValid = false;
            addError("name", msg_invalid_field, true);
        } else {
            clearErro("name");
        }

        if (driver.getBirth_date() == null) {
            isValid = false;
            addError("birthDay", "", true);
        } else if (calculateAge(Calendar.getInstance().getTime(), driver.getBirth_date()) < 18) {
            isValid = false;
            addError("birthDay", msg_over_18_age, true);
        } else {
            clearErro("birthDay");
        }

        if (driver.getPhone() == null || driver.getPhone().trim().length() == 0) {
            isValid = false;
            addError("phone", msg_invalid_field, true);
        } else {
            clearErro("phone");
        }

        if (driver.getGender() == null) {
            isValid = false;
            addError("gender", "", true);
        } else {
            clearErro("gender");
        }

        if (driver.getAddress() == null || driver.getAddress().trim().length() == 0) {
            isValid = false;
            addError("address", msg_invalid_field, true);
        } else {
            clearErro("address");
        }

        if (driver.getCountry_id() == null) {
            isValid = false;
            addError("national", "", true);
        } else {
            clearErro("national");
        }

        if (driver.getClass_driver() == null) {
            isValid = false;
            addError("classDriver", "", true);
        } else {
            clearErro("classDriver");
        }

        if (driver.getNo() == null || driver.getNo().trim().length() == 0) {
            isValid = false;
            addError("no", msg_invalid_field, true);
        } else {
            clearErro("no");
        }

        if (driver.getCard_type() == null) {
            isValid = false;
            addError("cardType", "", true);
        } else {
            clearErro("cardType");
        }

        if (driver.getSsn() == null || driver.getSsn().trim().length() == 0) {
            isValid = false;
            addError("ssn", msg_invalid_field, true);
        } else {
            clearErro("ssn");
        }

        if (driver.getImage_1920() == null) {
            isValid = false;
            addError("avatar", msg_invalid_field, true);
        } else {
            clearErro("avatar");
        }

        if (driver.getImage_license_frontsite() == null) {
            isValid = false;
            addError("front", msg_invalid_field, true);
        } else {
            clearErro("front");
        }

        if (driver.getImage_license_backsite() == null) {
            isValid = false;
            addError("back", msg_invalid_field, true);
        } else {
            clearErro("back");
        }

        return isValid;
    }

    private int calculateAge(Date time, OdooDate birth_date) {
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        now.setTime(time);
        dob.setTime(birth_date);

        int year1 = now.get(Calendar.YEAR);
        int year2 = dob.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dob.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dob.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            } else if (day2 < day1) {
                age++;
            }
        }

        return age;
    }
}
