package com.nextsolutions.longhold.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingTimePackage extends BaseModel {
    OdooDateTime calendar;
    boolean isSelect = false;
    boolean isFirst = false;
}
