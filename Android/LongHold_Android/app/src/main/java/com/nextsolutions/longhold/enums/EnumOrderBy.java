package com.nextsolutions.longhold.enums;

public class EnumOrderBy {
    public static String PRICE_ASC = "1"; //Giá tăng dần
    public static String PRICE_DESC = "2"; //Giá giảm dần
    public static String DISTANCE_ASC = "3"; //Quãng đường tăng dần
    public static String DISTANCE_DESC = "4"; //Quãng đường giảm dần
    public static String OLDEST = "5"; //Đơn cũ nhất
    public static String NEWEST = "6"; //Đơn mới nhất
}
