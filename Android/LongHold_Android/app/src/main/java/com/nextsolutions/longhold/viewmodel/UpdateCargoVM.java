package com.nextsolutions.longhold.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.CreateSuccess;
import com.nextsolutions.longhold.model.PayloadDriverCargo;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

public class UpdateCargoVM extends BaseViewModel {
    public MutableLiveData<OdooResultDto<PayloadDriverCargo>> mListPayLoad = new MutableLiveData<>();
    public MutableLiveData<CreateSuccess> mCreate = new MutableLiveData<>();

    public MutableLiveData<CreateSuccess> addCreateSuccess() {
        return mCreate;
    }

    public MutableLiveData<OdooResultDto<PayloadDriverCargo>> getListPayLoad() {
        return mListPayLoad;
    }


    public UpdateCargoVM(@NonNull Application application) {
        super(application);
    }

    // TODO: 8/17/2020 get list
    public void getListPayload(RunUi runUi) {
        DriverApi.getListPayload(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<PayloadDriverCargo> response = (OdooResultDto<PayloadDriverCargo>) o;
                mListPayLoad.setValue(response);
                runUi.run(Constants.GET_LIST_PAYLOAD_SUCCESS);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.GET_LIST_DRIVER_CARGO_ERROR);

            }
        });
    }

    public void createDriverCargo(String driver_phone_number, String lisence_plate, int id_tonnage, String driver_name, String id_card, String listPicture, RunUi runUi) {
        DriverApi.createDriverCargo(driver_phone_number, lisence_plate, id_tonnage, driver_name, id_card, listPicture, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                runUi.run(Constants.EDIT_CARGO_SUCCESS);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.EDIT_CARGO_ERROR);
            }
        });
    }

    public void editInfoCargo(String driver_phone_number, String driver_name, int id_vehicle, String id_card, int id_tonnage, String picture, RunUi runUi) {
        DriverApi.editInfoCargo(driver_phone_number, driver_name, id_vehicle, id_card, id_tonnage, picture, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                runUi.run(Constants.UPDATE_DRIVER_CARGO_SUCCESS);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.UPDATE_DRIVER_CARGO_ERROR);

            }
        });
    }
}
