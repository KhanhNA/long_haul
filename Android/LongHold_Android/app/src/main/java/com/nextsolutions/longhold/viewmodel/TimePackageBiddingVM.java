package com.nextsolutions.longhold.viewmodel;

import android.app.Application;
import android.util.Log;

import com.nextsolutions.longhold.api.DriverApi;
import com.nextsolutions.longhold.base.Constants;
import com.nextsolutions.longhold.base.IResponse;
import com.nextsolutions.longhold.base.RunUi;
import com.nextsolutions.longhold.model.BiddingTimePackage;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

public class TimePackageBiddingVM extends BaseViewModel {

    public MutableLiveData<OdooResultDto<BiddingTimePackage>> mCalendarBidding = new MutableLiveData<>();
    private OdooResultDto<BiddingTimePackage> mCalendar;
    private ArrayList<Long> longArrayList = new ArrayList<>();
    private ArrayList<OdooDateTime> mDateTimeBiddingSort = new ArrayList<>();


    public MutableLiveData<OdooResultDto<BiddingTimePackage>> getCalendarBidding() {
        return mCalendarBidding;
    }

    public TimePackageBiddingVM(@NonNull Application application) {
        super(application);
        mCalendar = new OdooResultDto<>();
        mCalendarBidding.setValue(mCalendar);
    }

    public void requestBiddingPackageTime(@NotNull RunUi runUi) {
        ArrayList<BiddingTimePackage> CalendarArrayList = new ArrayList<>();
        DriverApi.getBiddingPackageTime(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<OdooDateTime> resultDto = (OdooResultDto<OdooDateTime>) o;
                if (resultDto == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }
                mDateTimeBiddingSort.clear();
                mDateTimeBiddingSort.addAll(sortDateOdo(resultDto.getRecords()));
                for (int i = 0; i < mDateTimeBiddingSort.size(); i++) {
                    BiddingTimePackage biddingTimePackage = new BiddingTimePackage();
                    if (i == 0) {
                        biddingTimePackage.setSelect(true);
                        biddingTimePackage.setFirst(true);
                    }
                    biddingTimePackage.setCalendar(mDateTimeBiddingSort.get(i));
                    CalendarArrayList.add(biddingTimePackage);

                }
                mCalendar.setRecords(CalendarArrayList);
                mCalendarBidding.setValue(mCalendar);
                Log.e("reload", "onSuccess: mCalendarBidding"+mCalendarBidding.getValue().getRecords().size() );
                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });


    }

    public void setSelectItemView(int position, RunUi runUi) {
        for (int i = 0; i < getCalendarBidding().getValue().getRecords().size(); i++) {
            getCalendarBidding().getValue().getRecords().get(i).setSelect(false);
        }
        getCalendarBidding().getValue().getRecords().get(position).setSelect(true);
        getCalendarBidding().setValue(getCalendarBidding().getValue());
        runUi.run(Constants.UPDATE_ADAPTER);
    }

    /**
     * Sort date bidding theo dang milliseconds
     *
     * @param arrayList
     */
    public ArrayList<OdooDateTime> sortDateOdo(List<OdooDateTime> arrayList) {
        longArrayList.clear();
        for (int i = 0; i < arrayList.size(); i++) {
            longArrayList.add(arrayList.get(i).getTime());
        }
        Collections.sort(longArrayList);

        ArrayList<OdooDateTime> odooDateTimes = new ArrayList<>();
        for (int i = 0; i < longArrayList.size(); i++) {
            OdooDateTime odooDateTime = new OdooDateTime();
            odooDateTime.setTime(longArrayList.get(i));
            odooDateTimes.add(odooDateTime);
        }
        return odooDateTimes;
    }


    public ArrayList<Long> getMilliseconds() {
        return longArrayList;
    }

    public void clearData() {
        mDateTimeBiddingSort.clear();
        longArrayList.clear();
        if (mCalendar.getRecords() != null)
            mCalendar.getRecords().clear();
        mCalendarBidding.setValue(mCalendar);
    }
}
