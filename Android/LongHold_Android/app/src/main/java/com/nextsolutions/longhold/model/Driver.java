package com.nextsolutions.longhold.model;

import android.annotation.SuppressLint;

import com.nextsolutions.longhold.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver extends BaseModel {
    private Long id;
    private String driver_code;
    private String name;
    private String display_name;
    private String full_name;
    private String address;
    private String phone;
    private String tz;
    private String lang;
    private Integer nationality;
    private String user_driver;
    private String ssn;
    private OdooDate birth_date;
    private OdooDate hire_date;
    private OdooDate leave_date;
    private Float average_rating;
    private Integer point;

    private String image_1920;
    private String image_license_frontsite;
    private String image_license_backsite;

    private Integer class_driver;
    private OdooDate expires_date;
    private String no;
    private OdooDate driver_license_date;

    private AssignationLog assignation_log;
    private Vehicle vehicle;

    private Integer country_id;
    private Company company;
    private String gender;
    private String card_type;

    private String approved_check;
    private String max_tonnage;
    private String street;
    private String email;
    private String name_seq;
    private String class_name;

    public String getHire_date_str() {
        if (this.hire_date != null) {
            return AppController.formatDate.format(this.hire_date);
        }
        return "";
    }

    public String getBirth_date_str() {
        if (this.birth_date != null) {
            return AppController.formatDate.format(this.birth_date);
        }
        return "";
    }

    @SuppressLint("DefaultLocale")
    public String getAverage_rating_str() {
        if (this.average_rating != null) {
            return String.format("%.1f", average_rating);
        }
        return "";
    }

}