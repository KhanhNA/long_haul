//
//  NSMutableAttributedStringExtension.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/17/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    func addStrikethrough() {
        addAttribute(NSAttributedString.Key.strikethroughStyle,
                     value: NSUnderlineStyle.single.rawValue,
                     range: NSMakeRange(0, length))
    }
}
