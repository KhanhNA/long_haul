//
//  Enum+Extension.swift
//  mStore
//
//  Created by dong luong on 3/23/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
enum TypeSearch {
    case typeHistory, typeApi
    func value() -> Int? {
        switch self {
        case .typeHistory:
            return 0
        case .typeApi:
            return 1
        }
    }
    
    func valueString() -> String? {
        switch self {
        case .typeHistory:
            return "Search1"
        case .typeApi:
            return "Search2"
            
        }
    }
}

enum TypeStatusSearch {
    case typeRunning, typeDraft,typeDelete
    func value() -> Int? {
        switch self {
        case .typeRunning:
            return 0
        case .typeDraft:
            return 1
        case .typeDelete:
            return 2
        }
    }
    
    func valueString() -> String? {
        switch self {
        case .typeRunning:
            return "running"
        case .typeDraft:
            return "draft"
        case .typeDelete:
            return "delete"
            
        }
    }
}

enum TypeProvince {
    case typeProvince, typeDistrict, typeVillage
    func value() -> Int? {
        switch self {
        case .typeProvince:
            return 0
        case .typeDistrict:
            return 1
        case .typeVillage:
            return 2
        }
    }
    
    func valueString() -> String? {
        switch self {
        case .typeProvince:
            return "txt_City".localized()
        case .typeDistrict:
            return "txt_Distrist".localized()
        case .typeVillage:
            return "txt_Village".localized()
            
        }
    }
}

enum TypeDetailOrder {
    case typeMyOrder, typeOrderLV2, typeOrderTemp
    func value() -> Int? {
        switch self {
        case .typeMyOrder:
            return 0
        case .typeOrderLV2:
            return 1
        case .typeOrderTemp:
            return 2
        }
    }
}

enum TypeBottomSheet {
    case typeService, typeInsurance, typeSubcribe
    func value() -> Int? {
        switch self {
        case .typeService:
            return 0
        case .typeInsurance:
            return 1
        case .typeSubcribe:
            return 2
        }
    }
}

enum TypePopUpPayment {
    case typeNoMoney, typeConfirmMoney, typeConfirmOrder
    func value() -> Int? {
        switch self {
        case .typeNoMoney:
            return 0
        case .typeConfirmMoney:
            return 1
        case .typeConfirmOrder:
            return 2
        }
    }
}

enum TypeIncentive {
    case typeGroup, typeProduct, typeCoupon, typeFlashSale
    func value() -> Int? {
        switch self {
        case .typeGroup:
            return 1
        case .typeProduct:
            return 2
        case .typeCoupon:
            return 3
        case .typeFlashSale:
            return 4
        }
    }
    
}

enum TypeNotification {
    case typeNewWestNotification, typePromotionNotification, typeOrderNotification, typeDifferenceNotification
    func value() -> Int? {
        switch self {
        case .typeNewWestNotification:
            return 0
        case .typePromotionNotification:
            return 1
        case .typeOrderNotification:
            return 2
        case .typeDifferenceNotification:
            return 3
        }
    }
}

enum TypeStatusNotification {
    case typeNotificationOrder, typeNotificationOrderApprove, typeNotificationOrderReject, typeNotificationInvite,typeNotificationPromotion,typeNotificationOrderSuccess
    func value() -> Int? {
        switch self {
        case .typeNotificationOrder:
            return 1
        case .typeNotificationOrderApprove:
            return 2
        case .typeNotificationOrderReject:
            return 3
        case .typeNotificationInvite:
            return 4
        case .typeNotificationPromotion:
            return 5
        case .typeNotificationOrderSuccess:
            return 6
        }
    }
}

enum TypePopup {
    case typeLogout, typeDeleteAddress
    func value() -> Int? {
        switch self {
        case .typeLogout:
            return 0
        case .typeDeleteAddress:
            return 1
        }
    }
}

enum TypeConfirmStatus {
    case typeUnconfimred, typeDriverConfirm, typeConfirmedCompleted
    func value() -> Int? {
        switch self {
        case .typeUnconfimred:
            return 0
        case .typeDriverConfirm:
            return 1
        case .typeConfirmedCompleted:
            return 2
        }
    }
    
    func valueString() -> String? {
        switch self {
        case .typeUnconfimred:
            return "lbl_status_unconfirm".localized()
        case .typeDriverConfirm:
            return "lbl_status_driver_confirm".localized()
        case .typeConfirmedCompleted:
            return "lbl_status_confirmed".localized()
            
        }
    }
    
}



