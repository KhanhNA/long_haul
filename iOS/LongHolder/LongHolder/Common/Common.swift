
//
//  Common.swift
//  mStore
//
//  Created by TheLightLove on 17/12/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
import UIKit

class Const {
    //http://192.168.1.69
    //http://dev.nextsolutions.com.vn
    static let PROJECT_NAME                 = "mStore"
    
   // static let HOST_NAME                    = "http://cloud.nextsolutions.com.vn"
    static let HOST_NAME                    = "http://dev.nextsolutions.com.vn"
    static let BASE_URL                     = HOST_NAME + ":9999"
    static let URL_GETDATA                  = HOST_NAME + ":8234"
    static let URL_GETDATA8888              = HOST_NAME + ":8888"
    static let URL_GETDATA41019             = HOST_NAME + ":41019"
    static let URL_GETDATA9191              = HOST_NAME + ":9191"

    
    static let GETTOKEN                     = "http://Dcommerce:A31b4c24l3kj35d4AKJQ@dev.nextsolutions.com.vn:9999/oauth/token"
//     static let GETTOKEN                     = "http://Dcommerce:A31b4c24l3kj35d4AKJQ@cloud.nextsolutions.com.vn:9999/oauth/token"
//    static let GETTOKEN                     = "http://Dcommerce:A31b4c24l3kj35d4AKJQ@192.168.1.69:9999/oauth/token"
    static let IMAGEFILES                   =  HOST_NAME + ":8234/api/v1/files"
    static let USER_DEFAULT_PEOPLE          = "USER_DEFAULT_PEOPLE"
    static let IMAGEFILESNEWFEED                   =  "https://newfeedd7ae5f4f39bf4299ab3e389acff1706f165400-android.s3-ap-southeast-1.amazonaws.com/"
    static let accessKey = "AKIAIGE4MILHWHYNRL2A"
    static let secretKey = "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"
    static let S3BucketName = "newfeedd7ae5f4f39bf4299ab3e389acff1706f165400-android"
    static let poolId = "ap-southeast-1:ac2151fc-940f-4400-bf60-6d48900e71b8"
    
    
//    static func listFont() -> [ModelFont] {
//        var listFont: [ModelFont] = [ModelFont]()
//        listFont.append(ModelFont(strFont: "SFUIText-Bold.ttf", isClick: false))
//        listFont.append(ModelFont(strFont: "arial.ttf", isClick: false))
//        listFont.append(ModelFont(strFont: "Bangers-Regular.ttf", isClick: false))
//        listFont.append(ModelFont(strFont: "DancingScript-Regular.ttf", isClick: false))
//        listFont.append(ModelFont(strFont:"lmmonoproplt10-regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Marker Felt.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Pacifico-Regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "PaytoneOne-Regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SRoboto-Bold.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Roboto-Italic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Roboto-Light.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Roboto-Medium.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Roboto-Regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "Roboto-Thin.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "UVNChimBien_R.TTF",isClick: false))
//        listFont.append(ModelFont(strFont: "UVNDaLat_R.TTF",isClick: false))
//        listFont.append(ModelFont(strFont: "UVNTinTucHep.TTF",isClick: false))
//        listFont.append(ModelFont(strFont: "VT323-Regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-BoldItalic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-Heavy.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-HeavyItalic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-Light.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-LightItalic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-Medium.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-MediumItalic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-Regular.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-RegularItalic.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-Semibold.ttf",isClick: false))
//        listFont.append(ModelFont(strFont: "SFUIText-SemiboldItalic.ttf",isClick: false))
//        return listFont
//    }
    
//    static func listColor() -> [ModelColor] {
//        var listColor: [ModelColor] = [ModelColor]()
//        listColor.append(ModelColor(strColor: "#000000", isClick: false))
//        listColor.append(ModelColor(strColor: "#424242", isClick: false))
//        listColor.append(ModelColor(strColor: "#636363", isClick: false))
//        listColor.append(ModelColor(strColor: "#9C9C94", isClick: false))
//        listColor.append(ModelColor(strColor: "#CEC6CE", isClick: false))
//        listColor.append(ModelColor(strColor: "#EFEFEF", isClick: false))
//        listColor.append(ModelColor(strColor: "#F7F7F7", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFFFFF", isClick: false))
//        listColor.append(ModelColor(strColor: "#FF0000", isClick: false))
//        listColor.append(ModelColor(strColor: "#FF9C00", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFFF00", isClick: false))
//        listColor.append(ModelColor(strColor: "#00FF00", isClick: false))
//        listColor.append(ModelColor(strColor: "#00FFFF", isClick: false))
//        listColor.append(ModelColor(strColor: "#0000FF", isClick: false))
//        listColor.append(ModelColor(strColor: "#9C00FF", isClick: false))
//        listColor.append(ModelColor(strColor: "#FF00FF", isClick: false))
//        listColor.append(ModelColor(strColor: "#F7C6CE", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFE7CE", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFEFC6", isClick: false))
//        listColor.append(ModelColor(strColor: "#D6EFD6", isClick: false))
//        listColor.append(ModelColor(strColor: "#CEDEE7", isClick: false))
//        listColor.append(ModelColor(strColor: "#CEE7F7", isClick: false))
//        listColor.append(ModelColor(strColor: "#D6D6E7", isClick: false))
//        listColor.append(ModelColor(strColor: "#E7D6DE", isClick: false))
//        listColor.append(ModelColor(strColor: "#E79C9C", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFC69C", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFE79C", isClick: false))
//        listColor.append(ModelColor(strColor: "#E79C9C", isClick: false))
//        listColor.append(ModelColor(strColor: "#B5D6A5", isClick: false))
//        listColor.append(ModelColor(strColor: "#A5C6CE", isClick: false))
//        listColor.append(ModelColor(strColor: "#9CC6EF", isClick: false))
//        listColor.append(ModelColor(strColor: "#B5A5D6", isClick: false))
//        listColor.append(ModelColor(strColor: "#D6A5BD", isClick: false))
//        listColor.append(ModelColor(strColor: "#E76363", isClick: false))
//        listColor.append(ModelColor(strColor: "#F7AD6B", isClick: false))
//        listColor.append(ModelColor(strColor: "#FFD663", isClick: false))
//        listColor.append(ModelColor(strColor: "#F7AD6B", isClick: false))
//        listColor.append(ModelColor(strColor: "#94BD7B", isClick: false))
//        listColor.append(ModelColor(strColor: "#73A5AD", isClick: false))
//        listColor.append(ModelColor(strColor: "#6BADDE", isClick: false))
//        listColor.append(ModelColor(strColor: "#8C7BC6", isClick: false))
//        listColor.append(ModelColor(strColor: "#C67BA5", isClick: false))
//        listColor.append(ModelColor(strColor: "#CE0000", isClick: false))
//        listColor.append(ModelColor(strColor: "#E79439", isClick: false))
//        listColor.append(ModelColor(strColor: "#EFC631", isClick: false))
//        listColor.append(ModelColor(strColor: "#6BA54A", isClick: false))
//        listColor.append(ModelColor(strColor: "#4A7B8C", isClick: false))
//        listColor.append(ModelColor(strColor: "#3984C6", isClick: false))
//        listColor.append(ModelColor(strColor: "#634AA5", isClick: false))
//        listColor.append(ModelColor(strColor: "#4A7B8C", isClick: false))
//        listColor.append(ModelColor(strColor: "#A54A7B", isClick: false))
//        listColor.append(ModelColor(strColor: "#9C0000", isClick: false))
//        listColor.append(ModelColor(strColor: "#B56308", isClick: false))
//        listColor.append(ModelColor(strColor: "#BD9400", isClick: false))
//        listColor.append(ModelColor(strColor: "#397B21", isClick: false))
//        listColor.append(ModelColor(strColor: "#104A5A", isClick: false))
//        listColor.append(ModelColor(strColor: "#085294", isClick: false))
//        listColor.append(ModelColor(strColor: "#311873", isClick: false))
//        listColor.append(ModelColor(strColor: "#731842", isClick: false))
//        listColor.append(ModelColor(strColor: "#630000", isClick: false))
//        listColor.append(ModelColor(strColor: "#7B3900", isClick: false))
//        listColor.append(ModelColor(strColor: "#846300", isClick: false))
//        listColor.append(ModelColor(strColor: "#295218", isClick: false))
//        listColor.append(ModelColor(strColor: "#083139", isClick: false))
//        listColor.append(ModelColor(strColor: "#003163", isClick: false))
//        listColor.append(ModelColor(strColor: "#21104A", isClick: false))
//        listColor.append(ModelColor(strColor: "#4A1031", isClick: false))
//        return listColor
//    }
    
}
// MARK: - APIPath
struct APIPath {
    static let login                        = "/user/me"
    static let categories                   = "/api/v1/categories/tree"
    static let trandeMake                   = "/api/v1/manufacturers"
    static let allProduct                   = "/api/v1/products/getAll"
    static let getOrderL2                   = "/api/v1/merchants/getOrderL2"
    static let flashSale                    = "/api/v1/incentive/getFlashSale"
    static let flashSaleByTime              = "/api/v1/incentive/getFlashSaleByTime"
    static let grouponHome                  = "/api/v1/products/supperSale"
    static var SelectItem                   = ""
    static var SelectItemGroupon            = ""
    static let ProductsCategory             = "/api/v1/products/category"
    static let orders                       = "/api/v1/orderV2/get-orders"
    static let ordersdetail                 = "/api/v1/orders/detail"
    static let mechant                      = "/api/v1/merchants"
    static let commonlist                   = "/api/v1/common/listAll"
    static let ordercals                    = "/api/v1/orderV2/cals"
    static let merchantAddress              = "/api/v1/merchantAddress"
    static let transportMethod              = "/api/v1/common/list"
    static let newAddress                   = "/api/v1/newAddress"
    static let deleteAddress                = "/api/v1/merchant-address/delete"
    static let updateAddress                = "/api/v1/merchant-address/update"
    static let PAYPAL_CLIENT_ID_SANDBOX    = "Ac3xQXeFX9YuF1jQ30HyovSByTqTKbiXRNax_0NfwsI_hGfH_LimVVKhquKDc1cw-84YYvQgRCG81IP2"
    static let PAYPAL_CLIENT_ID_PRODUCT    = "AYPa1ZZuS6e9s1J7AHOiDWQkzsm2o0YCT7eAYIPoz0tk6qtg7_vHQu_UZIYg7qNcAenZOOWPxaVs-NCp"
    static let apitest                      = "/api/v1/mechants/test"
    static let inputCode                    = "/api/v1/merchants/inputCode"
    static let createMerchant               = "/api/v1/merchants/createMerchantL2"
    static let KpiMonth                     = "/api/v1/kpi-month"
    static let GroupPonList                 = "/api/v1/groupon/listRegistry"
    static let GroupPon                     = "/api/v1/groupon/registerStatus"
    static let Banner                       = "/api/v1/incentive/getBanner"
    static let changePassword               = "/api/v1/merchants/changePass"
    static let ContactOverviewL2            = "/api/v1/merchants/overviewL2"
    static let ContactOverview              = "/api/v1/merchants/overview"
    static let ChangeOrder                  = "/api/v1/orders/changeOrder"
    static let SearchHome                   = "/api/v1/products/search"
    static let SearchKeyHome                = "/api/v1/products/suggestion"
    static let listGrouponProgram           = "/api/v1/groupon/listGrouponProgram"
    static let notification                 = "/api/v1/notification/getList"
    static let updateNotification           = "/api/v1/notification/update"
    static let registerGroupon              = "/api/v1/groupon/register"
    static let checkGroupon                 = "/api/v1/products/incentive/"
    static let inventory                    = "/merchant-orders/get-inventory"
    static let incentiveSale                = "/api/v1/incentive/from-id"
    static let loginWallet                  = "/payment-gateway/api/v1/authenticate"
    static let getOderTranfer               = "/api/v1/get-order-transfer"
    static let saveOder                     = "/api/v1/orders/save-template"
    static let getInforSaveOrder            = "/api/v1/orderV2/get-template"
    static let getPrediction                = "/api/v1/products/prediction"
    static let getProductofmerchant         = "/api/v1/products/all-not-in"
    static let editTeamPlate                = "/api/v1/orders/edit-template"
    static let delTeamPlate                 = "/api/v1/orders/del-template"
    static let recommendSearch              = "/api/v1/recommend-search/merchant"
    static let getListAddress               = "/api/v1/common/listAll"
    static let updateProfile                = "/api/v1/merchants/l1.update"
    static let getPromotion                 = "/api/v1/incentive/getList"
    static let getMerchantByUser            = "/api/v1/merchant/user"
    static let getIncentiveCounpon          = "/api/v1/incentive/getCouponCode"
    static var detailProduct                = "/api/v1/productsV2/detail"
    static var currentTimeFlashSale         = "/api/v1/incentive/flashSale/currentTime"
    static var getProductManufacturer       = "/api/v1/products/manufacturer"
    static var updateToken                  = "/api/v1/merchants/update/token"
    static var kpiMerchant                  = "/api/v1/kpi-merchant"
}

// MARK: - APIParamKeys
struct APIParamKeys {
    static let phoneNumber                  = "phoneNumber"
    static let userId                       = "userId"
    static let verifyCode                   = "verifyCode"
    static let userName                     = "userName"
    static let email                        = "email"
    static let userPassword                 = "userPassword"
    static let avatarPath                   = "avatarPath"
    static let regionId                     = "regionId"
    static let schoolId                     = "schoolId"
    static let password                     = "password"
    static let searchKey                    = "searchKey"
    static let newPassword                  = "newPassword"
    static let learnClass                   = "class"
    static let unitId                       = "unitId"
    static let lessonId                     = "lessonId"
    static let examTypeId                   = "examTypeId"
    static let examId                       = "examId"
    static let successAnswer                = "successAnswer"
    static let wrongAnswer                  = "wrongAnswer"
    static let type                         = "type"
}

struct AppFormatters {
    static let debugConsoleDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd HH:mm:ss.SSS"
        formatter.timeZone = TimeZone(identifier: "UTC")!
        return formatter
    }()
    
    static let baseDateFormate = "HH:mm:ss"
    
    static let ISO8601                  = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
    static let newsFormat               = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
    static let mainFormat               = "yyyy-MM-dd'T'HH:mm:ss.SSSSS"
    static let timeSaleFormat           = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    static let JPYMDWTimeFormat         = "yyyy年MM月dd日(EEEEE) HH:mm"
    static let dateTimeSale             = "配信期間yyyy.MM.dd HH:mmまで"
    static let convertSeverDateFormater = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let timeSaleServerFormater   = "yyyy-MM-dd'T'HH:mm:ss"
    static let JPShortWithTimeFormat    = "MM月dd日(EEEEE) HH:mm"
    static let fullFormat               = "dd/MM/yyyy HH:mm:ss"
    static let dateNews                 = "yyyy年MM月dd日 HH:mm"
    static let dateTimeFormat           = "dd/MM/yyyy HH:mm"
    static let dateTimeSaleCoupon       = "yyyy年MM月末口まで"
    static let JPShortYearFormat        = "yyyy年MM月dd日(E)"
    static let JPMDHS                   = "MM月dd日 HH:mm"
    static let dateRegisterFormat       = "yyyy年MM月dd日"
    static let tireImageDateFormat      = "yyyyMMddHHmm"
    static let dateDetailNews           = "MM月dd日昼刊"
    static let JPShortFormat            = "MM月dd日(E)"
    static let nomarlFormat             = "dd/MM/yyyy"
    static let dateSeverFormat          = "yyyy-MM-dd"
    static let shortFormat              = "yyyyMMdd"
    static let JPdateMonth              = "MM月dd日"
    static let dateMonth                = "MM/dd"
    static let hourMinuteFormat         = "HH:mm"
    static let nomarlFormatYear         = "yyyy/MM/dd"
}

enum MessageType: String {
    case warnning = "警報"
    case success  = "成功"
    case error    = "エーラー"
    
    var key: String {
        return String(describing: self)
    }
    
    static func icon(with str: String) -> String {
        switch str {
        case "warnning"   :   return "warn"
        case "success"    :   return "success"
        case "error"      :   return "error"
        default:
            return ""
        }
    }
}


// HTTP status code
enum ResultCode: Int, Codable {
    case httpSuccess = 200
    case httpCreated = 201
    case httpAccepted = 202
    case httpNoContent = 204
    case unknow = 999
    case httpInternalError = 500
    case httpBadRequest = 400
    case httpNotFound = 404
    case httpForbidden = 403
    case httpUnauthorized = 401
    case success = 1
    case error = 2
    case warning = 3
    case notFoundObj = 4
    case bodyInvalid = 5
    case paramInvalid = 6
    case noData = 7
    case codeExist = 20
    case emailExist = 21
    case phoneExist = 22
    case passwordWrong = 23
    case empApprovedOrder = 24
    case empBusy = 25
    
    var message: String {
        switch self {
        case .httpBadRequest, .httpNotFound:
            return "No Connection"
        default:
            return ""
        }
    }
    
    var isApiSuccess: Bool {
        return rawValue >= 200 && rawValue < 300
    }
}

struct Screen {
    static var height: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    static var width: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    static let hIP4: CGFloat = 480
    static let hIP5E: CGFloat = 568
    static let hIP6: CGFloat = 667
    static let hIPPlus: CGFloat = 736
    static let hIPX: CGFloat = 812
    static let hIPXR: CGFloat = 896
    static let hIPXS: CGFloat = 812
    static let hIPXSMax: CGFloat = 896
    static let hIPadMini: CGFloat = 1024
    static let hIPadAir: CGFloat = 1024
    static let hIPadPro105: CGFloat = 1112
    static let hIPadPro129: CGFloat = 1336
    
    static var isIphoneX: Bool {
        return height == hIPX
    }
    
    static var isGreatThanIphoneX: Bool {
        return height >= hIPX && height <= hIPXSMax
    }
    
    static var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
}

struct ColorHex {
    static let f2a66a                  = "f2a66a"
    static let e76b85                  = "e76b85"
    static let ea7184                  = "ea7184"
    static let ea6b88                  = "ea6b88"
    static let ea9847                  = "ea9847"
    static let cd853f                  = "cd853f"
    static let cc3232                  = "cc3232"
    static let hex32a54a               = "32a54a"
    static let bmi59d3df               = "59d3df"
    static let bmi5cb775               = "5cb775"
    static let bmiebda4e               = "ebda4e"
    static let bmiee5a30               = "ee5a30"
    static let bmieb5684               = "eb5684"
    static let bmic2372d               = "c2372d"
}
struct typeScreen {
    static let nextTarger              = "nextTarger"
    static let perfectTager            = "perfectTager"
}
struct BMIMessage {
    static let bmiMessge1              = "現在の肥満度は"
    static let bmiMessge2              = "目標の肥満度は"
}
struct BMiContent {
    static let bmiType0                = "「低体重」です。"
    static let bmiType1                = "「普通体重」です。"
    static let bmiType2                = "「肥満（1度）」です。"
    static let bmiType3                = "「肥満（2度）」です。"
    static let bmiType4                = "「肥満（3度）」です。"
    static let bmiType5                = "「肥満（4度）」です。"
}
struct BMIType {
    static let bmiType0                = "現在の肥満度は「低体重」です。"
    static let bmiType1                = "現在の肥満度は「普通体重」です。"
    static let bmiType2                = "現在の肥満度は「肥満（1度）」です。"
    static let bmiType3                = "現在の肥満度は「肥満（2度）」です。"
    static let bmiType4                = "現在の肥満度は「肥満（3度）」です。"
    static let bmiType5                = "現在の肥満度は「肥満（4度）」です。"
}

struct MessageError {
    static let sexEmpty                = "性別を選択してください。"
    static let userEmpty               = "名前を入力してください。"
    static let furiganaEmpty           = "フリガナを入力してください。"
    static let characterPassword       = "新パスワードは６文字以上にしてください。"
    static let confirmPassword         = "新パスワード確認が違います。"
    static let agreeTeam               = "「ご利用規約」および「プライバシーポリシー」への同意が必要です。内容をご確認の上、ご同意いただける場合、チェックを入れて、次の画面へお進みください。"
    static let heightEmpty             = "身長を入力してください。"
    static let weightEmpty             = "体重を入力してください。"
}

struct WebViewURL {
    static let privacy                  = "https://tavenal.com/privacy"
    static let tos                      = "https://smart-me.jp/tos"
}

struct Key {
    static let appName                  = "appName"
    static let nameCurrentUser          = "nameCurrentUser"
    static let deviceToken              = "deviceToken"
    static let fcmToken                 = "fcmToken"
    static let sessionId                = "sessionId"
    static let titleHistoryBooking      = "titleHistoryBooking"
    static let roomCurrent              = "roomCurrent"
    static let isFirstLaunchApp         = "isFirstLaunchApp"
    static let currentHashPassword      = "currentHashPassword"
    static let firstRunInstalled        = "1stRunInstalled"
    static let titleHistorySearchAds    = "titleHistorySearchAds"
}

enum DeviceType {
    case iPhone5 // 5, 5s, 5c
    case iPhone8 // 6, 6s, 7, 8
    case iPhone8Plus // 6+, 6s+, 7+, 8+
    case iPhoneX // x, xs
    case iPhoneXR // xr
    case iPhoneXSMax // xs max
    case unknown
    case iPad
}

struct DateFormat  {
    static let birthDay                 =          "yyyy-MM-dd"
}

struct Regex {
    static let emailRegEx    = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,}"
    static let phoneRegEx    = "^[0]{1}[1-9]{1}[0-9]{8,9}$" // VietName PhoneNumber
}
