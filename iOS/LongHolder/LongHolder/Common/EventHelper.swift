//
//  EventHelper.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/15/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import EventKit
import RealmSwift

class EventModel: Object {
    @objc dynamic var eventId = ""
    @objc dynamic var id = ""
    
    static func getEKEvent(id: String) -> (EventModel?, EKEvent?)? {
        let eventModel = EventModel.getAll(Type: EventModel.self, filter: "id = '\(id)'").first
        
        if eventModel != nil {
            
            let eventStore = EKEventStore()
            let event = eventStore.event(withIdentifier: eventModel!.eventId)
            
            return (eventModel, event)
            
        }
        
        return (nil, nil)
    }
}

class EventHelper: NSObject {
    static func removeEvent(_ event: EKEvent) {
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }
            
            guard granted else { return }
            
            do {
                
                try eventStore.remove(event, span: .thisEvent, commit: true)
                
            } catch let error {
                
                print(error.localizedDescription)
                
            }
        }
    }
    
    static func addEventToCalendar(id: String,
                                   title: String,
                                   notes: String,
                                   startDate: Date,
                                   announceTimeBefore: Int,
                                   completion: @escaping (_ error: Error?) -> ()) {
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }
            
            guard granted else { return }
            
            let event = EKEvent(eventStore: eventStore)
            
            event.title = title
            event.notes = notes
            event.startDate = startDate
            event.endDate = startDate.adding(minutes: 1)
            
            event.calendar = eventStore.defaultCalendarForNewEvents
            
            let alarm = EKAlarm(relativeOffset: TimeInterval(-announceTimeBefore * 60))
            event.alarms = [alarm]
            
            do {
                
                try eventStore.save(event, span: .thisEvent)
                
                if let (eventModel, eventFromRealm) = EventModel.getEKEvent(id: id),
                    eventModel != nil, eventFromRealm != nil {
                    
                    eventModel?.update {
                        eventModel?.eventId = event.eventIdentifier
                    }
                    
                } else {
                    
                    let eventModel = EventModel()
                    eventModel.eventId = event.eventIdentifier
                    eventModel.id = id
                    
                    eventModel.save()
                }
                
                completion(nil)
                
            } catch let error {
                
                print(error.localizedDescription)
                
                completion(error)
            }
        }
    }
}
