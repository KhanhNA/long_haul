//
//  Helper.swift
//  mStore
//
//  Created by TheLightLove on 17/12/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
import UIKit

func appDelegate () -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

// Define log function - start
func DEBUGLog(_ message: String,
              file: String = #file,
              line: Int = #line,
              function: String = #function) {
    #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("DEBUG: \(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
    #endif
    // Nothing to do if not debugging
}

func ERRORLog(_ message: String,
              file: String = #file,
              line: Int = #line,
              function: String = #function) {
    #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("ERROR: \(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
    #endif
    // Nothing to do if not debugging
}

public func LS(_ key: String,
               tableName: String? = nil,
               bundle: Bundle = Bundle.main,
               value: String = "",
               comment: String = "") -> String {
    return NSLocalizedString(key, tableName: tableName, bundle: bundle, value: value, comment: value)
}

class Dispatch: NSObject {
    static func async(_ closure: @escaping () -> Void) {
        DispatchQueue.main.async {
            closure()
        }
    }
    
    static func asyncAfter(_ timeDelay: Double, _ closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + timeDelay, execute: {
            closure()
        })
    }
}
