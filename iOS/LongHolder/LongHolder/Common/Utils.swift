

import Foundation
import UIKit
import Toast
import MaterialComponents.MaterialActionSheet
import CoreLocation
import RealmSwift

class Utils {
    
    static func isValid(text: String, pattern: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            if regex.firstMatch(in: text, options: [], range: NSMakeRange(0, text.utf16.count)) != nil {
                return true
            } else {
                return false
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return false
    }
    
    static func getHoursMinutesSeconds(hours: Int, minutes: Int, seconds: Int) -> String {
        var time = ""
        time += (hours < 10 ? "0" : "") + String(hours)
        time += ":"
        time += (minutes < 10 ? "0" : "") + String(minutes)
        time += ":"
        time += (seconds < 10 ? "0" : "") + String(seconds)
        return time
    }
    
    static func secondsToHoursMinutesSeconds(seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func formatNumber(number: Double?,
                             maximumFractionDigits: Int = 2,
                             minimumFractionDigits: Int = 0) -> String {
        // convert to number
        let number = NSNumber(value: number ?? 0)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = false
        numberFormatter.numberStyle = .decimal
        numberFormatter.roundingMode = .down
        numberFormatter.maximumFractionDigits = maximumFractionDigits
        numberFormatter.minimumFractionDigits = minimumFractionDigits
        
        let numberToString = numberFormatter.string(from: number) ?? ""
        
        return numberToString.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    static func formatMoney(number: Double?,
                            currency: String? = nil,
                            maximumFractionDigits: Int = 2,
                            minimumFractionDigits: Int = 0) -> String {
        let currency = currency?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        // convert to number
        let number = NSNumber(value: number ?? 0)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.roundingMode = .down
        numberFormatter.maximumFractionDigits = maximumFractionDigits
        numberFormatter.minimumFractionDigits = minimumFractionDigits
        
        let numberToString = numberFormatter.string(from: number) ?? ""
        let money = numberToString + " " + currency
        
        return money.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    static func fromtoDateHistory(dateString:String) -> String {
        if dateString == "" {
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateFromInputString = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.string(from: dateFromInputString!)
        }
    }
    
    
    static func currentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return  formatter.string(from: date)
       }
    
    static func addMonthDate(numberMonth: Int) -> String {
           let date = Calendar.current.date(byAdding: .month, value: numberMonth, to: Date()) ?? Date()
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           return  formatter.string(from: date)
          }
    
    static func fromtoDateTT(dateString:String) -> String{
        if dateString == "" {
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            let dateFromInputString = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.string(from: dateFromInputString!)
        }
    }
    
    static func fromtoDateTTOrder(dateString:String) -> String{
          if dateString == "" {
              return ""
          }else{
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
              let dateFromInputString = dateFormatter.date(from: dateString)
              dateFormatter.dateFormat = "yyyy-MM-dd"
              return dateFormatter.string(from: dateFromInputString!)
          }
      }
    
    static func fromtoTimeServerTT(dateString:String) -> String{
           if dateString == "" {
               return ""
           }else{
               let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "dd-MM-yyyy"
               let dateFromInputString = dateFormatter.date(from: dateString)
               dateFormatter.dateFormat = "yyyy-MM-dd"
               return dateFormatter.string(from: dateFromInputString!)
           }
       }
    
    static func fromtoComfirmServerTT(dateString:String) -> String{
            if dateString == "" {
                return ""
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let dateFromInputString = dateFormatter.date(from: dateString)
                dateFormatter.dateFormat = "yyyy-MM-dd"
                return dateFormatter.string(from: dateFromInputString!)
            }
        }
    
    static func fromtoDateNotifaction(dateString:String) -> String{
        if dateString == "" {
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateFromInputString = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            return dateFormatter.string(from: dateFromInputString!)
        }
    }
    
    static func fromtoDateWallet(dateString:String) -> String{
        if dateString == "" {
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateFromInputString = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.string(from: dateFromInputString!)
        }
    }

    
    static func decodeBase64(toImage strEncodeData: String!) -> UIImage {
        if strEncodeData == "" {
            
        }else{
            if let decData = Data(base64Encoded: strEncodeData, options: .ignoreUnknownCharacters) {
                return UIImage(data: decData)!
            }
        }
        return UIImage()
    }
    
    static func setfileImage(stringUrl:String) -> String{
        var imgStr:String = ""
        imgStr = imgStr + Const.IMAGEFILES + stringUrl
        return imgStr
    }
    
    
    
   static  func formatPercent(number: Double) -> String {
           var numberResult = ""
           if ("\(number )".contains(".")) {
               let number2 = "\(number )".split(".")[1]
               if Int(number2) == 0 {
                   let disCount = NSString(format: "%.0f", number ) as String
                   numberResult = disCount + "%"
                 print(disCount.last ?? "")
               } else {
                   let disCount = NSString(format: "%.2f", number ) as String
                  numberResult = disCount + "%"
                if disCount.last == "0" {
                    let disCount = NSString(format: "%.1f", number ) as String
                    numberResult = disCount + "%"
                }
                 //print(disCount.last ?? "")
               }
           } else {
               let disCount = NSString(format: "%.2f", number ) as String
            print(disCount.last ?? "")
           numberResult = disCount + "%"
           }
           return numberResult
       }
    
    
    static func addActionSheet(_ actionSheet:MDCActionSheetController){
        let actionOne = MDCActionSheetAction(title: "",
                                                     image: UIImage(named: ""))
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
        actionSheet.addAction(actionOne)
    }
    

    
    static func getmaxDayToWait(_ dateInt:Int) -> String {
        let dateTime = TimeInterval(Int(dateInt*24*60*60))
        let timestamp = NSDate().timeIntervalSince1970
        let myTimeInterval = TimeInterval(timestamp)
        let dateOrder = dateTime + myTimeInterval
        let date = Date(timeIntervalSince1970: dateOrder)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    

    class func showAlertInView(_ view: UIViewController, title: String, message: String, cancel: @escaping () -> ()){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default) { (action) in
            cancel()
        }
        
        alertController.addAction(cancelAction)
        
        view.present(alertController, animated: true) { }
    }

    
    static func dismisactionSheet(_ view:UIViewController) {
        view.dismiss(animated: true) {
            return
        }
    }
    
    static func onMinus(_ number:Int) -> String {
        var numberPakage = number
        if numberPakage <=  1{
            numberPakage = 1
            return String(numberPakage)
        }else{
            numberPakage -= 1
        }
        return String(numberPakage)
    }
    

    
    static func openApp() {
        let application = UIApplication.shared
        let secondAppPath = "second://"
        let appUrl = URL(string: secondAppPath)!
        let websiteUrl = URL(string: "https://www.google.com.vn/")!
        if application.canOpenURL(appUrl) {
            application.open(appUrl, options: [:], completionHandler: nil)
        } else {
            application.canOpenURL(websiteUrl)
        }
    }
    
    static func setBorderColor(_ view:UIView,_ intBorder:Double,_ intRadius:Int) {
        view.layer.masksToBounds = true
        view.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.layer.borderWidth = CGFloat(intBorder)
        view.layer.cornerRadius = CGFloat(intRadius)
    }
    
    static func settButton(_ btn:UIButton,_ sizeTextButton:CGFloat,_ backGround:UIColor,_ fontButton:String,_ textColor:UIColor,_ radius:CGFloat) {
        btn.titleLabel?.font =  UIFont(name: fontButton, size: sizeTextButton)
        btn.backgroundColor = backGround
        btn.layer.cornerRadius = radius
        btn.layer.masksToBounds = true
        btn.setTitleColor(textColor, for: .normal)
    }

}

func df2so2(_ price: Double) -> String{
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = "."
    numberFormatter.groupingSize = 3
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = "."
    numberFormatter.numberStyle = .decimal
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter.string(from: price as NSNumber)! + "name_money".localized()
}

func formatKPI(_ price: Double) -> String{
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = "."
    numberFormatter.groupingSize = 3
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = "."
    numberFormatter.numberStyle = .decimal
    numberFormatter.maximumFractionDigits = 2
    return numberFormatter.string(from: price as NSNumber)!
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UIViewController {
    func shadowViewControll(_ shadowView:UIView) -> UIView {
        shadowView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOffset = CGSize(width: 0.0 , height: 2.0)
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 1.0
        return shadowView
    }
}

extension UIViewController {
    func shadowViewButton(_ globeButton:UIButton) -> UIButton {
        globeButton.backgroundColor = UIColor(red: 171/255, green: 178/255, blue: 186/255, alpha: 1.0)
        // Shadow and Radius
        globeButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        globeButton.layer.shadowOffset = CGSize(width: 0.3, height: 2.0)
        globeButton.layer.shadowOpacity = 1.0
        globeButton.layer.shadowRadius = 0.0
        globeButton.layer.masksToBounds = false
        globeButton.layer.cornerRadius = 6
        return globeButton
    }
}


extension String {
    
    func toDouble() -> Double {
        if self == nil{
            return 0
        }
        if (self as NSString).doubleValue > 0{
            return (self as NSString).doubleValue
        }
        return 0
    }
    func toInt() -> Int {
        if (self as NSString).integerValue > 0{
            return (self as NSString).integerValue
        }
        return 0
    }
    
    func toUInt() -> UInt? {
        if contains("-") {
            return nil
        }
        return self.withCString { cptr -> UInt? in
            var endPtr : UnsafeMutablePointer<Int8>? = nil
            errno = 0
            let result = strtoul(cptr, &endPtr, 10)
            if errno != 0 || endPtr?.pointee != 0 {
                return nil
            } else {
                return result
            }
        }
    }
    
    func toInt32() -> Int32 {
        if (self as NSString).integerValue > 0{
            return (self as NSString).intValue
        }
        return 0
    }
    func toFloat() -> Float {
        if (self as NSString).floatValue > 0{
            return (self as NSString).floatValue
        }
        return 0
    }
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

}

