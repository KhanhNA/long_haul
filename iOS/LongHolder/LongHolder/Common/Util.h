//
//  Util.h
//  mStore
//
//  Created by TheLightLove on 17/12/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface Util : NSObject

+ (NSString *)convertObjectToJSON:(id)obj;
+ (id)objectForKey:(NSString *)key;
+ (void)setObject:(id)obj forKey:(NSString *)key;
+ (NSDictionary*)convertStringToDictionary:(NSString*)string;
+ (void)removeObjectForKey:(NSString *)key;
+ (void)arestar;
+ (void)callApp1;
@end
