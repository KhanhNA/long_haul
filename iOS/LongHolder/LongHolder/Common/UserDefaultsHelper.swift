//
//  UserDefaultsHelper.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/24/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class UserDefaultsHelper: NSObject {
    static func setIsOnNotification(isOn: Bool) {
        UserDefaults.standard.set(isOn, forKey: "setIsOnNotification")
        UserDefaults.standard.synchronize()
    }
    
    static func getIsOnNotification() -> Bool {
        return UserDefaults.standard.value(forKey: "setIsOnNotification") as? Bool ?? true
    }
    
    static func setAcceptFirebase(acceptFirebase: Bool) {
        UserDefaults.standard.set(acceptFirebase, forKey: "setAcceptFirebase")
        UserDefaults.standard.synchronize()
    }
    
    static func getAcceptFirebase() -> Bool {
        return UserDefaults.standard.value(forKey: "setAcceptFirebase") as? Bool ?? true
    }
}
