//
//  SocketIOManager.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/24/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    static let shared = SocketIOManager()
    
    var bidmessage: ((_ data: [Any], _ ack: SocketAckEmitter) -> ())?
    
    private lazy var manager: SocketManager? = {
        guard let url = URL(string: socketUrlString) else { return nil }
        return SocketManager(socketURL: url)
    }()
    
    func setOnBidmessage() {
        manager?.defaultSocket.on("bidmessage") { [weak self] data, ack in
            print("==================================================")
            print("Socket connected bidmessage")
            self?.bidmessage?(data, ack)
        }
    }
    
    func setOffBidmessage() {
        manager?.defaultSocket.off("bidmessage")
    }
    
    func addHandlers() {
        manager?.defaultSocket.on(clientEvent: .connect) { data, ack in
            print("==================================================")
            print("Socket connected")
        }
        
        setOnBidmessage()
    }
    
    func connect() {
        manager?.defaultSocket.connect()
    }
    
    func disconnect() {
        manager?.defaultSocket.disconnect()
    }
}
