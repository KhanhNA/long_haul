//
//  RealmExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RealmSwift

extension Object {
    func save() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func delete() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(self)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func update(action: () -> ()) {
        do {
            let realm = try Realm()
            try realm.write {
                action()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    static func getAll<T: Object>() -> [T] {
        do {
            let realm = try Realm()
            let objects = realm.objects(T.self)
            return Array(objects)
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
    
    static func getAll<T: Object>(Type: T.Type, filter: String) -> [T] {
        do {
            let realm = try Realm()
            let objects = realm.objects(T.self)
            return Array(objects.filter(filter))
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
}
