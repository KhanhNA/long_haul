//
//  CustomDisablePasteUITextField.swift
//  mStore
//
//  Created by dong luong on 4/22/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation

import UIKit  // don't forget this

class CustomDisablePasteUITextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    

}
