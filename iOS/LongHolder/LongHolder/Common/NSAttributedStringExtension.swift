//
//  NSAttributedStringExtension.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/7/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

public extension NSAttributedString {
    func add(_ attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString(attributedString: self)
        let range = NSRange(location: 0, length: string.count)
        mutableAttributedString.addAttributes(attributes, range: range)
        return mutableAttributedString
    }
    
    func font(_ font: UIFont) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.font: font]
        return add(attributes)
    }
    
    func color(_ color: UIColor) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.foregroundColor: color]
        return add(attributes)
    }
}
