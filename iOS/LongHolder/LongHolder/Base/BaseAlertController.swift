//
//  BaseAlertController.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class BaseAlertController: NSObject {
    static func show(message: String?,
                     confirmTitle: String?,
                     confirmAction: (() -> ())?,
                     cancelTitle: String?,
                     cancelAction: (() -> ())?,
                     from viewController: UIViewController) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let confirmAlertAction = UIAlertAction(title: confirmTitle, style: .default) { (_) in
            confirmAction?()
        }
        let cancelAlertAction = UIAlertAction(title: cancelTitle, style: .default) { (_) in
            cancelAction?()
        }
        if confirmTitle != nil && confirmTitle?.isEmpty == false {
            alertController.addAction(confirmAlertAction)
        }
        alertController.addAction(cancelAlertAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
