//
//  BaseDialogView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/5/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class BaseDialogView: UIView {
    public init() {
        super.init(frame: UIScreen.main.bounds)
        setupNib()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: UIScreen.main.bounds)
        setupNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        setupNib()
    }
    
    func show(completion: @escaping () -> ()) {
        alpha = 0
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.addSubview(self)
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.alpha = 1
            
            completion()
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.alpha = 0
        }) { [weak self] (_) in
            self?.removeFromSuperview()
        }
    }
}
