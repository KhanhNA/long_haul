//
//  ToastDialogView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/5/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class SuccessDialogView: BaseDialogView {
    @IBOutlet weak var messageLabel: UILabel!
    
    static func show(message: String, dismissClosure: (() -> ())?) {
        let view = SuccessDialogView()
        view.show {
            view.messageLabel.text = message
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                view.dismiss()
                dismissClosure?()
            }
        }
    }
}
