//
//  APIDefinition.swift
//  OdooCustomer
//
//  Created by dong luong on 7/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

private let hostURL = URL(string: "http://demo.nextsolutions.com.vn:8070/")!
private let hostURLMap = URL(string: "https://maps.googleapis.com/maps/api/place/")!
let hostLinkImage = "http://demo.nextsolutions.com.vn:8070/images/"
let socketUrlString = "http://demo.nextsolutions.com.vn:3001/"

//private let hostURL = URL(string: "http://demo.nextsolutions.com.vn:8070/")!
//private let hostURLMap = URL(string: "https://maps.googleapis.com/maps/api/place/")!
//let hostLinkImage = "http://demo.nextsolutions.com.vn:8070/images/"

enum apiURL: String {
    
    // HOME VC
    // Danh sách đã đấu thầu
    case getBiddingOrderBidded = "bidding/get_bidding_order_bidded"
    
    // Danh sách xe add bidding
    case getListBiddingVehicle = "bidding/list_bidding_vehicle"
    
    // Bidding đơn 
    case biddingCargo = "bidding/bidding_cargo"
    
    // Xác nhận bidding
    case confirmBiddingVehicle = "bidding/confirm_bidding_vehicle"
    
    // Chi tiết đơn hàng
    case getBiddingDetail = "bidding/get_bidding_detail"
    
    // HISTORY VC
    // Lịch sử bid(đối với đơn đã vận chuyển thành công)
    case getHistoryBidding = "bidding/get_list_bidding_order_history_enterprise"
    case getHistoryBiddingPrice = "bidding/get_history_bidding_price"
    
    // VAN VC
    // Danh sách xe quản lý
    case listManagerBiddingVehicle = "bidding/list_manager_bidding_vehicle"
    // Xóa thông tin xe 
    case deleteAccountDriver = "bidding/delete_account_driver"
    // Update thông tin xe
    case updateAccountDriver = "bidding/update_account_driver"
    case createAccountDriver = "bidding/creat_account_driver"
    // Danh sách trọng tải xe
    case getVehicleTonnage = "bidding/get_vehicle_tonnage"
    
    // NEW HOME VC
    // LIST thời gian đấu thầu
    case biddingPackageTime = "bidding/bidding_package_time"
    // Danh sách đấu thầu
    case getBiddingPackageInformation = "bidding/get_bidding_package_information"
    // Bidding gói thầu
    case createBiddingOrder = "bidding/create_bidding_order"
    // Chi tiết gói thầu
    case getBiddingPackageDetail = "bidding/get_bidding_package_detail"
    case getBiddingOrderDetail = "bidding/get_bidding_order_detail"
    // Add lái xe vào đơn thầu
    case comfirmBiddingVehicleForBiddingOrder = "bidding/confirm_bidding_vehicle_for_bidding_order"
    // Chi tiết đơn hàng lái xe
    case getListBiddingOrderByBiddingVehicleId = "bidding/get_list_bidding_order_by_bidding_vehicle_id"
    
    // NOTIFICATION VC
    // Lịch sử thông báo
    case getBiddingNotificationHistory = "bidding/get_bidding_notification_history"
    case isRead = "notification/is_read"
    
    // PROFILE VC
    // Đổi mật khẩu
    case profileChangePassword = "server/change_password"
    // Cấu hình nhận hoặc không nhận Firebase
    case acceptFirebaseNotification = "notification/accept_firebase_notification"
    // Get thông tin employee
    // case getCustomerInformation = "share_van_order/get_customer_information"
    // Update thông tin employee
    case updateAccountLongHaul = "bidding/update_account_long_haul"
    // get information employee
//    case getInformationEmployee = "bidding/get_information_employee"
    case getInformationEmployee = "bidding/get_all_bidding_order_rating"

    case saveToken = "server/save_token"
    case logout = "mobile/logout"
    
    case loginOdoo = "web/session/authenticate"
    case searchReadOdoo = "web/dataset/search_read"
    case getWareHouseOdoo = "share_van_order/get_warehouse"
    case checkWareHouseOdoo = "share_van_order/check_warehouse_info"
    case searchTextMapOdoo = "textsearch/json"
    case detailPlaceID   = "details/json"
    case listServiceOdoo = "share_van_order/service/list_active"
    case listProductTypeOdoo = "share_van_order/product_type/list_active"
    case listInsuranceOdoo = "share_van_order/insurance/list_active"
    case listSubcribeOdoo = "share_van_order/subscribe/list"
    case listBillLadingHistory = "share_van_order/bill_lading_history"
    case getInforLogin = "share_van_order/get_customer_information"
    case updateProfileLogin = "share_van_order/edit_customer_information"
    case getRoutingPlanDay = "share_van_order/routing_plan_day_by_employeeid"
    case getRoutingPlanDayDetail = "share_van_order/routing_plan_day/detail"
    case getRoutingDetailId = "share_van_order/get_detail_routing_plan_customer"
    case creatOrder = "share_van_order/create_order"
    
    
    
    case notifications = "notification/getList"
    case notificationUpdate = "notification/update"
    case notificationDetail = "notification/detail"
    case registerEmail = "users"
    case updatePass = "users/password"
    case checkRegisterFacebook = "users/check/is_sign_up"
    case loginFacebook = "users/login/facebook"
    case getPostList = "posts"
    case getListFollowPost = "followPost"
    case getListPostRecently = "posts/getPost/Recently"
    case getListFollow = "follower"
    case getListMyPost = "posts/created_user"
    case comment
    case review_posts
    case bank
    case getListBillBuy = "bill/buy"
    case buyProduct = "bill"
    case getListFoodBill = "posts/lists"
    case postManagerOrder = "bill/sell"
    case postUpdateBill = "bill/update/status"
    case postToken = "device"
    case upLoadImageBienLai = "bill/update/receipt"
    case detailProduct = "posts/getOne"

//    //Call post recipe
//       case postRecipe = "recipe/post-recipe"

    // Call when start app
    case startUp = "common/start-up"
    // Call when get device token
    case syncPushToken = "common/sync-push-token"
    // Call when login email
    case loginEmail = "account/login"

    // Call when register account with facebook, google
    case socialLogin = "account/social-login"
    // Call when verify phone
    case verifyPhone = "account/verify-phone"
    // Call when forgot password
    case forgotPassword = "account/forgot-pass"
    // Call get list recipe
    case getRecipes = "recipe/get-recipes"
    // Call get recipe detail
    case getRecipeDetail = "recipe/recipe-detail"
    // Call favorite
    case favoriteRecipe = "recipe/favorite-recipe"
    // Call when searcg recipes
    case searchRecipe = "recipe/search-recipes"
    // Call post recipe
    case postRecipe = "recipe/post-recipe"
    // Call rate recipe
    case rateRecipe = "recipe/rate-recipe"
    // Call post image pratice
    case postImagePractice = "recipe/post-image-practice"
    // Call get recipe popular
    case getRecipePopular = "recipe/get-recipe-popular"
    // Call search order food
    case orderFood = "order/search-foods"
    // Call find cooker by coordinate
    case findCookerByCoordinate = "order/get-cooker-by-coordinate"
    // Get list Chef by Condition
    case getListChefByCondition = "order/get-cooker-by-condition"
    // Create Order
    case createOrderFood = "order/make-order"
    // Get Food of Chef
    case getFoodsOfCooker = "order/get-food-of-cooker"
    // Get Order Detail
    case getOrderDetail = "order/order-detail"
    // get favorite cooker
    case getFavorCooker = "order/get-cooker-favorite"
    // Call change order status
    case changeOrderStatus = "order/change-order-status"
    // Call get account
    case getAccount = "account/get"
    // Call change password
    case changePassword = "account/change-pass"
    // Call register cooker
    case registerCooker = "account/register-cooker"
    // Call update account
    case updateAccount = "account/update"
    // Call get history order
    case historyOrder = "order/history-order"
    // Call get list share food
    case listShareFood = "post/get-posts"
    // Call get detail share post
    case detailShareFood = "post/post-detail"
    // Call get comment share post
    case getComment = "post/get-comments"
    // Call like unlike post
    case likePost = "post/like-post"
    // Call like cooker
    case likeCooker = "account/favorite-cooker"
    // Call post comment
    case postComment = "post/post-comment"
    // Call create post
    case createPost = "post/create-post"
    // Call add food
    case addFood = "food/add-food"
    // Call del food
    case delFood = "food/delete-food"
    // Call get own food
    case getOwnRecipe = "recipe/get-own-recipes"
    // Call get favor food
    case getFavorRecipe = "recipe/get-favorite-recipes"
    // Call share recipe
    case shareRecipe = "recipe/share-recipe"
    // Call share post
    case sharePost = "post/share-post"
    // Call get recipe practice
    case getRecipePractice = "recipe/get-image-practices"
    // Call get suggest food
    case getSuggestFood = "food/food-suggestion"
    var url: String {
        return (hostURL.appendingPathComponent(rawValue).absoluteString)
    }
    
    var urlMap: String {
           return (hostURLMap.appendingPathComponent(rawValue).absoluteString)
       }
}

typealias ApiResultHandler = (_ result: ApiResult) -> Void

enum ApiResult {
    case success([String: Any])
    case failure(ApiError)
}

enum ApiError: Error {
    case api(String)
    case http
    case offline

    var message: String {
        switch self {
        case .api(let message):
            return message
        case .http:
            return "connection is too weak . try again"
        case .offline:
            return "Không thể kết nối đến server!"
        }
    }
}

enum TypeUser: Int {
    case normal = 1
    case saler = 2
    case wait = 3
}

enum Provider: String {
    case facebook
    case google
}

enum Gender: Int {
    case unknown = 0
    case male = 1
    case female = 2
}

enum CookerAvailable: Int {
    case online = 1
    case offline = 2
}

enum OrderStatus: Int {
    case userOrder = 10
    case cookerAccept = 15
    case ship = 20
    case userReceive = 30
    case cookerCancel = 40
    case userCancel = 50
}

enum NotificationType: String {
    case normal = "0"
    case cooker = "1"
    case user = "2"
    case cancelOrder = "3"
    case userCancelOrder = "4"
}

enum CookerFavorite: Int {
    case favorite = 1
    case unfavorite = 2
}
