//
//  CargoListView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class CargoListView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var list: [CargoType]!
    
    static func show(list: [CargoType]) {
        let view = CargoListView()
        view.show {
            view.list = list
            view.titleLabel.text = "Danh sách loại cargo".localized()
            
            view.tableView.dataSource = view
            view.tableView.delegate = view
            
            view.tableView.register(cellNibName: CargoTableViewCell.className)
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss()
    }
}

extension CargoListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(CargoTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(cargo: list[indexPath.row])
        
        return cell
    }
}
