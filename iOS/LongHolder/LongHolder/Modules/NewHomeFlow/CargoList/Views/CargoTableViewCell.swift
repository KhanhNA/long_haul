//
//  CargoTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class CargoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var totalCargoLabel: UILabel!
    @IBOutlet weak var totalCargoValueLabel: UILabel!
    
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sizeValueLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weightValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        totalCargoLabel.text = "Số lượng cargo".localized()
        sizeLabel.text = "Kích thước(L * H * W)".localized()
        weightLabel.text = "Cân nặng".localized()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(cargo: CargoType) {
        titleLabel.text = cargo.type
        totalCargoValueLabel.text = cargo.getCargoQuantity()
        sizeValueLabel.text = cargo.getLHW()
        weightValueLabel.text = cargo.getWeight()
    }
}
