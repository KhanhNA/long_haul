//
//  NewConfirmVanVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class NewConfirmVanVC: BaseVC {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet weak var noteWarningLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var emptyTitleLabel: UILabel!
    @IBOutlet weak var addVanButton: UIButton!
    
    private let viewModel = NewConfirmVanViewModel()
    
    var maxConfirmTime: String?
    var biddingOrderId: String!
    
    var getListBiddingVehicleRecords: [BiddingVehicle]?
    
    // kiểm tra có đang ở flow đấu thầu
    // hay đang ở flow điền thông tin xe, chỉnh sửa
    // để xử lý show hide titleLabel và skipButton
    var isBiddingFlow = false
    
    var timer: Timer!
    
    var subtitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        viewModel.maxConfirmTime = maxConfirmTime
        viewModel.biddingOrderId = biddingOrderId
        viewModel.getListBiddingVehicleRecords.accept(getListBiddingVehicleRecords ?? [])
        
        setupNavigationController()
        setupViews()
        setupTableView()
        
        handleSubscribe()
        
        if maxConfirmTime != nil {
            startCountdown()
            noteWarningLabel.superview?.isHidden = false
        } else {
            noteWarningLabel.superview?.isHidden = true
        }
    }
    
    private func setupViews() {
        titleLabel.text = "Đấu giá thành công gói thầu".localized()
        emptyTitleLabel.text = "Thêm thông tin lái xe để hoàn thành xác nhận gói thầu".localized()
        
        addVanButton.setTitle("Thêm xe".localized(), for: .normal)
        skipButton.setTitle("Để sau".localized(), for: .normal)
        confirmButton.setTitle("Xác nhận".localized(), for: .normal)
        
        titleLabel.superview?.superview?.superview?.isHidden = !isBiddingFlow
    }
    
    private func setupTableView() {
        tableView.register(cellNibName: ChooseVanTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý dữ liệu khi chọn Van từ ChooseVanView
        viewModel.getListBiddingVehicleRecords.subscribe(onNext: { [weak self] (getListBiddingVehicleRecords) in
            guard let self = self else { return }
            
            self.isShowEmpty = true
            self.tableView.reloadData()
            
            self.emptyView.isHidden = (getListBiddingVehicleRecords.count != 0)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý dữ liệu khi call api comfirmBiddingVehicleForBiddingOrder
        viewModel.comfirmBiddingVehicleForBiddingOrder.subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value, value else { return }
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.homeContainerVC?.reloadData()
            
            for viewController in self.navigationController?.viewControllers ?? []
                where (viewController is NewHomeContainerVC
                    || viewController is HomeContainerVC) {
                        
                        self.navigationController?.popToViewController(viewController, animated: true)
                        
                        var message = ""
                        if self.isBiddingFlow {
                            message = "Chúc mừng bạn\nĐấu giá thành công gói thầu!".localized()
                        } else {
                            message = "Điền thông tin thành công!".localized()
                        }
                        
                        SuccessDialogView.show(message: message) { }
            }
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func skipAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        if isBiddingFlow {
            let message = "Chúc mừng bạn\nĐấu giá thành công gói thầu!".localized()
            SuccessDialogView.show(message: message) { }
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        if viewModel.isTimeOut {
            showToastTimeOut()
            return
        }
        
        guard viewModel.checkIsValidAndShowMessageIfNeed() else { return }
        
        ConfirmDialogView.show(title: "Xác nhận hoàn tất danh sách xe chở hàng gói thầu!".localized(),
                               yesTitle: "Xác nhận".localized(),
                               noTitle: "Huỷ bỏ".localized(),
                               yesAction: { [weak self] in
                                
                                self?.viewModel.callApi()
                                
            }, noAction: { })
    }
    
    @IBAction func addVanAction(_ sender: Any) {
        handleAdd()
    }
}

extension NewConfirmVanVC {
    private func setupNavigationController() {
        setupCustomNavigationController(title: "Xác nhận xe chở hàng".localized(),
                                        subtitle: subtitle)
        
        let image = UIImage(named: "new_confirm_van_vc_add")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(handleAdd))
    }
    
    @objc private func handleAdd() {
        if viewModel.isTimeOut {
            showToastTimeOut()
            return
        }
        
        let getListBiddingVehicleRecords = viewModel.getListBiddingVehicleRecords.value
        
        ChooseVanView.show(selectedGetListBiddingVehicleRecords: getListBiddingVehicleRecords) { [weak self] (selectedGetListBiddingVehicleRecords) in
            self?.viewModel.getListBiddingVehicleRecords.accept(selectedGetListBiddingVehicleRecords)
        }
    }
}

extension NewConfirmVanVC {
    private func startCountdown() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(handleStartCountdown),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc private func handleStartCountdown() {
        
        guard let maxConfirmTimeDate = maxConfirmTime?.toDate() else { return }
        
        let startDate = Date()
        
        let differenceInSeconds = Int(maxConfirmTimeDate.timeIntervalSince(startDate))
        
        let (hours, minutes, seconds) = Utils.secondsToHoursMinutesSeconds(seconds: differenceInSeconds)
        
        if hours <= 0 && minutes <= 0 && seconds <= 0 {
            
            viewModel.isTimeOut = true
            noteWarningLabel.text = "Đã hết hạn".localized()
            timer?.invalidate()
            
        } else {
            
            let time = Utils.getHoursMinutesSeconds(hours: hours, minutes: minutes, seconds: seconds)
            noteWarningLabel.text = String(format: "Thiếu thông tin xe, gói thầu hủy trong %@s?".localized(), time)
            
        }
    }
    
    private func showToastTimeOut() {
        viewModel.errorMessage.accept("Đã hết hạn".localized())
    }
}

extension NewConfirmVanVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getListBiddingVehicleRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(ChooseVanTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.showDeleteButton()
        
        let getListBiddingVehicleRecord = viewModel.getListBiddingVehicleRecords.value[indexPath.row]
        cell.setup(getListBiddingVehicleRecord: getListBiddingVehicleRecord)
        
        cell.deleteClosure = { [weak self] in
            if self?.viewModel.isTimeOut == true {
                self?.showToastTimeOut()
                return
            }
            
            let title = String(format: "Bạn muốn hủy xe %@ chở gói thấu?".localized(),
                               getListBiddingVehicleRecord.lisencePlate ?? "")
            
            ConfirmDialogView.show(title: title,
                                   yesTitle: "Xác nhận".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { [weak self] in
                                    self?.viewModel.removeGetListBiddingVehicleRecord(getListBiddingVehicleRecord)
                }, noAction: { })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
