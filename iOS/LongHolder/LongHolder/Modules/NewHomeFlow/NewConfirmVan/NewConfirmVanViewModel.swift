//
//  NewConfirmVanViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NewConfirmVanViewModel: BaseViewModel {
    // danh sách bidding_vehicle trên table view
    let getListBiddingVehicleRecords = BehaviorRelay<[BiddingVehicle]>(value: [])
    
    // xử lý dữ liệu khi call api comfirmBiddingVehicleForBiddingOrder
    let comfirmBiddingVehicleForBiddingOrder = BehaviorRelay<Bool?>(value: nil)
    
    var maxConfirmTime: String?
    var biddingOrderId: String!
    
    var isTimeOut = false
    
    func removeGetListBiddingVehicleRecord(_ getListBiddingVehicleRecord: BiddingVehicle) {
        var getListBiddingVehicleRecords = self.getListBiddingVehicleRecords.value
        getListBiddingVehicleRecords.removeAll(where: { $0.id == getListBiddingVehicleRecord.id })
        
        self.getListBiddingVehicleRecords.accept(getListBiddingVehicleRecords)
    }
    
    func checkIsValidAndShowMessageIfNeed() -> Bool {
        guard let _ = biddingOrderId,
            getListBiddingVehicleRecords.value.count > 0 else {
                
                errorMessage.accept("Bạn chưa điền thông tin tài xế!".localized())
                
                return false
        }
        
        return true
    }
    
    func callApi() {
        
        guard checkIsValidAndShowMessageIfNeed() else { return }
        
        isShowLoading.accept(true)
        
        let biddingVehicleIds = getListBiddingVehicleRecords.value
            .compactMap({ $0.id })
            .compactMap({ String($0) })
            .joined(separator: ",")
        
        NewHomeApi.comfirmBiddingVehicleForBiddingOrder(biddingOrderId: biddingOrderId, biddingVehicleIds: biddingVehicleIds) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard data?.boolResult == true else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            self.comfirmBiddingVehicleForBiddingOrder.accept(true)
        }
    }
}
