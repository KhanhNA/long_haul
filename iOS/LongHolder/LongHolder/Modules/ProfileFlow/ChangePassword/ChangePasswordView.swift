//
//  ChangePasswordView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/31/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class ChangePasswordView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmNewPasswordTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var confirmNewPasswordButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    // xử lý api lỗi
    let apiError = BehaviorRelay<Error?>(value: nil)
    let errorMessage = BehaviorRelay<String?>(value: nil)
    let isShowLoading = BehaviorRelay<Bool>(value: false)
    
    static func show() {
        let view = ChangePasswordView()
        view.show {
            view.titleLabel.text = "Đổi mật khẩu".localized()
            
            view.passwordTextField.delegate = view
            view.newPasswordTextField.delegate = view
            view.confirmNewPasswordTextField.delegate = view
            
            view.passwordTextField.placeholder = "Mật khẩu hiện tại".localized()
            view.newPasswordTextField.placeholder = "Mật khẩu mới".localized()
            view.confirmNewPasswordTextField.placeholder = "Nhập lại mật khẩu mới".localized()
            
            view.cancelButton.setTitle("Huỷ bỏ".localized(), for: .normal)
            view.saveButton.setTitle("Lưu".localized(), for: .normal)
            
            view.handleSubscribe()
        }
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: disposeBag)
        
        
        // xử lý khi gọi api lỗi
        apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            //            self.makeToast(error.localizedDescription,
            //                           duration: 1,
            //                           position: CSToastPositionCenter,
            //                           style: nil)
            
            self.errorMessageLabel.isHidden = false
            self.errorMessageLabel.text = error.localizedDescription
            self.apiError.accept(nil)
            
        }).disposed(by: disposeBag)
        
        
        errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            //            self.makeToast(value,
            //                           duration: 1,
            //                           position: CSToastPositionCenter,
            //                           style: nil)
            
            self.errorMessageLabel.isHidden = false
            self.errorMessageLabel.text = value
            self.errorMessage.accept(nil)
            
        }).disposed(by: disposeBag)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        guard let password = passwordTextField.text,
            let newPassword = newPasswordTextField.text,
            let confirmNewPassword = confirmNewPasswordTextField.text,
            !password.isEmpty,
            !newPassword.isEmpty,
            !confirmNewPassword.isEmpty else {
                errorMessage.accept("Vui lòng không để trống các trường thông tin!".localized())
                return
        }
        
        if newPassword.count < 6 {
            errorMessage.accept("Mật khẩu phải lớn hơn 6 kí tự".localized())
            return
        }
        
        if password == newPassword {
            errorMessage.accept("Mật khẩu mới không được trùng với mật khẩu cũ".localized())
            return
        }
        
        if newPassword != confirmNewPassword {
            errorMessage.accept("Mật khẩu mới chưa trùng khớp với nhau".localized())
            return
        }
        
        callApi()
    }
    
    @IBAction func showHidePasswordAction(_ sender: Any) {
        passwordButton.isSelected = !passwordButton.isSelected
        passwordTextField.isSecureTextEntry = !passwordButton.isSelected
    }
    
    @IBAction func showHideNewPasswordAction(_ sender: Any) {
        newPasswordButton.isSelected = !newPasswordButton.isSelected
        newPasswordTextField.isSecureTextEntry = !newPasswordButton.isSelected
    }
    
    @IBAction func showHideConfirmNewPasswordAction(_ sender: Any) {
        confirmNewPasswordButton.isSelected = !confirmNewPasswordButton.isSelected
        confirmNewPasswordTextField.isSecureTextEntry = !confirmNewPasswordButton.isSelected
    }
}

extension ChangePasswordView {
    func callApi() {
        guard let oldPassword = passwordTextField.text,
            let newPassword = newPasswordTextField.text,
            !oldPassword.isEmpty,
            !newPassword.isEmpty
            else { return }
        
        isShowLoading.accept(true)
        
        ProfileApi.changePassword(oldPassword: oldPassword, newPassword: newPassword) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            if data?.boolResult == true {
                SuccessDialogView.show(message: "Đổi mật khẩu thành công!".localized()) { [weak self] in
                    
                    self?.dismiss()
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.profileTableVC?.handleLogout()
                    
                }
            } else {
                self.errorMessage.accept("Sai mật khẩu hiện tại".localized())
            }
        }
    }
}

extension ChangePasswordView: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        errorMessageLabel.isHidden = true
        
        if string.isEmpty {
            return true
        }
        if string == " " {
            return true
        }
        return Utils.isValid(text: string, pattern: "[a-zA-Z0-9]")
    }
}
