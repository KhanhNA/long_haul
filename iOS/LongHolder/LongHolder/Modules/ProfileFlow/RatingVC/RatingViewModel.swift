//
//  RatingViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RatingViewModel: BaseViewModel {

    let getBiddingDetailRecords = BehaviorRelay<[GetBiddingDetailRecord]>(value: [])
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        isShowEmpty.accept(false)
        
        offset = 0
        callApi(isShowLoading: isShowLoading)
    }
    
    func callApi(isShowLoading: Bool = true) {
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        ProfileApi.getInformationEmployee(offset: offset, limit: limit) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.getBiddingDetailRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.getBiddingDetailRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.getBiddingDetailRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                let fakeData = GetBiddingDetailRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.getBiddingDetailRecords.accept(oldRecords)
        }
    }
}
