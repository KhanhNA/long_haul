//
//  RatingTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {
    @IBOutlet weak var lisencePlateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descriptionStringLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var imageView3BlurView: UIView!
    @IBOutlet weak var imageView3CountLabel: UILabel!
    
    @IBOutlet weak var createDateLabel: UILabel!
    
    @IBOutlet var starsImageView: [UIImageView]!
    
    var showImagesClosure: (() -> ())?
    var cargoTypes: [CargoType]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: VanCollectionViewCell.className)
        collectionView.contentInset = .zero
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 100, height: 25)
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(biddingVehicle: BiddingVehicle) {
        cargoTypes = biddingVehicle.cargoTypes ?? []
        
        collectionView.isHidden = (cargoTypes?.isEmpty == true)
        collectionView.reloadData()
        
        let lisencePlate = biddingVehicle.lisencePlate ?? ""
        let driverName = biddingVehicle.driverName ?? ""
        lisencePlateLabel.text = String(format: "%@: %@ - %@", "BKS".localized(),
                                        lisencePlate,
                                        driverName)
        
        descriptionStringLabel.text = biddingVehicle.rating?.note   
        
        handleRating(biddingVehicle: biddingVehicle)
        
        handleImage(biddingVehicle: biddingVehicle)
        
        handleStar(biddingVehicle: biddingVehicle)
        
        createDateLabel.text = biddingVehicle.rating?.getCreateDate()
    }
    
    func handleImage(biddingVehicle: BiddingVehicle) {
        imageView1.superview?.isHidden = true
        imageView2.superview?.isHidden = true
        imageView3.superview?.isHidden = true
        
        imageView3BlurView.isHidden = true
        imageView3CountLabel.isHidden = true

        let images = biddingVehicle.rating?.images ?? []
        
        imageView1.superview?.isHidden = images.isEmpty
        
        if images.count >= 3 {
            
            imageView1.setImage(urlString: images[0], placeholder: nil)
            imageView2.setImage(urlString: images[1], placeholder: nil)
            imageView3.setImage(urlString: images[2], placeholder: nil)
            
            imageView1.superview?.isHidden = false
            imageView2.superview?.isHidden = false
            imageView3.superview?.isHidden = false
            
            if images.count >= 4 {
                
                imageView3CountLabel.text = String(format: "+%@", String(images.count - 2))
                
                imageView3BlurView.isHidden = false
                imageView3CountLabel.isHidden = false
                
            } else {
                
                imageView3CountLabel.text = ""
                
                imageView3BlurView.isHidden = true
                imageView3CountLabel.isHidden = true
                
            }
            
        } else if images.count >= 2 {
            
            imageView1.setImage(urlString: images[0], placeholder: nil)
            imageView2.setImage(urlString: images[1], placeholder: nil)
            imageView3.setImage(urlString: nil, placeholder: nil)
            
            imageView1.superview?.isHidden = false
            imageView2.superview?.isHidden = false
            imageView3.superview?.isHidden = true
            
        } else if images.count >= 1 {
            
            imageView1.setImage(urlString: images[0], placeholder: nil)
            imageView2.setImage(urlString: nil, placeholder: nil)
            imageView3.setImage(urlString: nil, placeholder: nil)
            
            imageView1.superview?.isHidden = false
            imageView2.superview?.isHidden = true
            imageView3.superview?.isHidden = true
        }
    }
    
    func handleRating(biddingVehicle: BiddingVehicle) {
        ratingLabel.isHidden = true
        
        for view in ratingStackView.arrangedSubviews where view != ratingLabel {
            view.removeFromSuperview()
        }
        
        let ratingBadges = biddingVehicle.rating?.ratingBadges ?? []
        
        ratingStackView.isHidden = ratingBadges.isEmpty
        
        for ratingBadge in ratingBadges {
            let label = UILabel()
            
            label.cornerRadius = ratingLabel.cornerRadius
            label.font = ratingLabel.font
            label.textColor = ratingLabel.textColor
            label.backgroundColor = ratingLabel.backgroundColor

            label.text = String(format: "     %@     ", ratingBadge.name ?? "")
            
            label.translatesAutoresizingMaskIntoConstraints = false
            ratingStackView.addArrangedSubview(label)
            
            label.heightAnchor.constraint(equalToConstant: 25).isActive = true
        }
    }
    
    func handleStar(biddingVehicle: BiddingVehicle) {
        let numRating = biddingVehicle.rating?.numRating ?? 0
        let maxIndexShowed = numRating - 1
        
        for index in 0..<5 {
            starsImageView[index].isHidden = (maxIndexShowed < index)
        }
    }
    
    @IBAction func showImagesAction(_ sender: Any) {
        showImagesClosure?()
    }
}

extension RatingTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cargoTypes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(VanCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }

        if let cargoTypes = cargoTypes {
            let item = cargoTypes[indexPath.item]
            cell.idLabel.text = String(format: "%@ %@",
                                       String(item.cargoQuantity ?? 0),
                                       String(item.type ?? ""))
        } else {
            cell.idLabel.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
