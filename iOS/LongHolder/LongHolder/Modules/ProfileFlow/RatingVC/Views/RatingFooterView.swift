//
//  RatingFooterView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class RatingFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    var actionClosure: (() -> ())?
    
    func setup(actionClosure: (() -> ())?) {
        self.actionClosure = actionClosure
        titleLabel.text = "Xem thêm".localized()
    }
    
    @IBAction func action(_ sender: Any) {
        actionClosure?()
    }
}
