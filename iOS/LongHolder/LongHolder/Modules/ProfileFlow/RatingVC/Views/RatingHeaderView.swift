
//
//  RatingHeaderView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class RatingHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var depot1Label: UILabel!
    @IBOutlet weak var depot2Label: UILabel!
    
    func setup(getBiddingDetailRecord: GetBiddingDetailRecord) {
        
        guard let biddingOrder = getBiddingDetailRecord.biddingOrder else {
            
            idLabel.text = ""
            depot1Label.text = ""
            depot2Label.text = ""
            ratingLabel.text = ""
            
            return
        }
        
        let id = biddingOrder.getId()
        let distance = biddingOrder.getDistance()
        let price = biddingOrder.getPrice()
        
        let idMutableAttributedString = NSMutableAttributedString()
        
        idMutableAttributedString.append((id + " - " + distance + " - ")
            .attributedString
            .color(AppColor.hex24282C))
        
        idMutableAttributedString.append(price
            .attributedString
            .color(AppColor.hex00A359))
        
        idLabel.attributedText = idMutableAttributedString
        
        
        depot1Label.text = biddingOrder.fromDepot?.address
        depot2Label.text = biddingOrder.toDepot?.address
        
        ratingLabel.text = biddingOrder.getOrderRateAvgString()
    }
}
