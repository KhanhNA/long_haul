//
//  RatingVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class RatingVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    private var stackView: UIStackView!
    private var rateLabel: UILabel!
    private let viewModel = RatingViewModel()
    
    var star = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        setupTableView()
        handleSubscribe()
        viewModel.reloadData(isShowLoading: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationController(rate: star)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stackView.removeFromSuperview()
    }
    
    func setupNavigationController(rate: String) {
        setupNavigationController(title: "Đánh giá".localized())
        setupRightBarButtonItem(rate: rate)
    }
    
    func setupRightBarButtonItem(rate: String) {
        if stackView == nil {
            if rateLabel == nil {
                rateLabel = UILabel()
                rateLabel.textAlignment = .right
                rateLabel.textColor = AppColor.hexD46C59
                rateLabel.font = .systemFont(ofSize: 16)
            }
            
            let startImageView = UIImageView(image: UIImage(named: "profile_star"))
            startImageView.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
            
            stackView = UIStackView(arrangedSubviews: [rateLabel, startImageView])
            stackView.axis = .horizontal
            stackView.spacing = 5
            
            stackView.translatesAutoresizingMaskIntoConstraints = false
        }
        
        rateLabel.text = rate
        
        guard let navigationBar = navigationController?.navigationBar else { return }
        navigationBar.addSubview(stackView)
        
        stackView.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -10).isActive = true
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getBiddingDetailRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
}

extension RatingVC: UITableViewDataSource, UITableViewDelegate {
    private func setupTableView() {
        tableView.register(headerNibName: RatingHeaderView.className)
        tableView.register(headerNibName: RatingFooterView.className)
        tableView.register(cellNibName: RatingTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 10, right: 0)
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getBiddingDetailRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
        let getBiddingDetailRecord = getBiddingDetailRecords[section]
        let biddingVehicles = getBiddingDetailRecord.biddingOrder?.biddingVehicles ?? []
        
        if biddingVehicles.count == 0 {
            return 0
        }
        
        if getBiddingDetailRecord.isShowAll {
            return biddingVehicles.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
        let getBiddingDetailRecord = getBiddingDetailRecords[indexPath.section]
        
        if indexPath.section == getBiddingDetailRecords.count - 1
            && getBiddingDetailRecord.isLoadMore {
            
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(RatingTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        let biddingVehicles = getBiddingDetailRecord.biddingOrder?.biddingVehicles ?? []
        let biddingVehicle = biddingVehicles[indexPath.row]
        
        cell.setup(biddingVehicle: biddingVehicle)
        
        cell.showImagesClosure = { [weak self] in
            let images = biddingVehicle.rating?.images ?? []
            if images.isEmpty {
                return
            }
            
            self?.mainCoordinator?.goToPreviewVC(images: images)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RatingHeaderView.className)
        
        if let headerView = headerView as? RatingHeaderView {
            
            let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
            let getBiddingDetailRecord = getBiddingDetailRecords[section]
            headerView.setup(getBiddingDetailRecord: getBiddingDetailRecord)
            
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
        let getBiddingDetailRecord = getBiddingDetailRecords[section]
        
        if getBiddingDetailRecord.isShowAll {
            return nil
        }
        
        let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RatingFooterView.className)
        
        if let footerView = footerView as? RatingFooterView {
            footerView.setup {
                getBiddingDetailRecord.isShowAll = !getBiddingDetailRecord.isShowAll
                tableView.reloadData()
            }
        }
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
        let getBiddingDetailRecord = getBiddingDetailRecords[section]
        
        return (getBiddingDetailRecord.isShowAll ? 0.01 : 35)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getBiddingDetailRecords = viewModel.getBiddingDetailRecords.value
        let getBiddingDetailRecord = getBiddingDetailRecords[indexPath.section]
        
        if getBiddingDetailRecord.isShowAll {
            getBiddingDetailRecord.isShowAll = false
            tableView.reloadData()
        }
    }
}
