//
//  SettingsVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/29/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class SettingsVC: BaseVC {
    @IBOutlet weak var acceptFirebaseSwitch: UISwitch!
    
    let disposeBag = DisposeBag()
    
    // xử lý api lỗi
    let apiError = BehaviorRelay<Error?>(value: nil)
    let errorMessage = BehaviorRelay<String?>(value: nil)
    let isShowLoading = BehaviorRelay<Bool>(value: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        acceptFirebaseSwitch.isOn = UserDefaultsHelper.getAcceptFirebase()
        setupNavigationController(title: "Cài đặt".localized())
        
        handleSubscribe()
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: disposeBag)
        
        
        // xử lý khi gọi api lỗi
        apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.apiError.accept(nil)
            
        }).disposed(by: disposeBag)
        
        
        errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.errorMessage.accept(nil)
            
        }).disposed(by: disposeBag)
    }
    
    @IBAction func acceptFirebaseSwitchValueChanged(_ sender: Any) {
        let acceptFirebase = (acceptFirebaseSwitch.isOn ? "1" : "0")
        
        isShowLoading.accept(true)
        
        ProfileApi.acceptFirebaseNotification(acceptFirebase: acceptFirebase) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            if data?.boolResult == true {
                UserDefaultsHelper.setAcceptFirebase(acceptFirebase: self.acceptFirebaseSwitch.isOn)
            } else {
                self.acceptFirebaseSwitch.isOn = !self.acceptFirebaseSwitch.isOn
                self.errorMessage.accept("Có lỗi xảy ra".localized())
            }
        }
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        ChangePasswordView.show()
    }
}
