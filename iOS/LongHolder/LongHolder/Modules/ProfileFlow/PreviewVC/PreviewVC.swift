//
//  PreviewVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class PreviewVC: BaseVC {
    @IBOutlet weak var collectionView: UICollectionView!
    var images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: ImageCollectionViewCell.className)
        collectionView.contentInset = .zero
        
        update(index: 0)
    }
    
    func update(index: Int) {
        setupNavigationController(title: String(format: "%@/%@",
                                                String(index + 1),
                                                String(images.count)))
    }
}

extension PreviewVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(ImageCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }
        
        cell.imageView.setImage(urlString: images[indexPath.item], placeholder: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension PreviewVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = collectionView.contentOffset.x / collectionView.frame.size.width
        update(index: Int(index))
    }
}
