//
//  MyProfileVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/31/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class MyProfileVC: BaseVC {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var rateValueLabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    
    let viewModel = MyProfileViewModel()
    var profileRecord: ProfileRecord?
    
    private let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.profileRecord = profileRecord
        setupViews()
        handleSubscribe()
        setupData()
    }
    
    func setupViews() {
        isShowNavigationSeparator = true
        setupNavigationController(title: "Hồ sơ của tôi".localized())
        
        nameLabel.text = "Người đại diện".localized()
        phoneLabel.text = "Số điện thoại liên lạc".localized()
        addressLabel.text = "Địa chỉ công ty".localized()
        
        rateLabel.text = "ĐÁNH GIÁ".localized()
        acceptButton.setTitle("Xác nhận".localized(), for: .normal)
        
        nameTextField.delegate = self
        phoneTextField.delegate = self
        addressTextField.delegate = self
        
        handleEdit()
    }
    
    func setupData() {
        if profileRecord?.currentImage != nil {
            avatarImageView.image = profileRecord?.currentImage
        } else {
            avatarImageView.setImage(urlString: profileRecord?.uriPath,
                                     placeholder: UIImage(named: "confirm_van_vc_avatar"))
        }
        
        companyLabel.text = profileRecord?.companyName
        nameTextField.text = profileRecord?.name
        phoneTextField.text = profileRecord?.phone
        rateValueLabel.text = profileRecord?.getOrderRateAvgString()
        
        let street = profileRecord?.street ?? ""
        let city = profileRecord?.city ?? ""
        
        var address = street
        if !street.isEmpty && !city.isEmpty {
            address += ", "
        }
        address += city
        
        addressTextField.text = address
    }
    
    @objc private func edit() {
        viewModel.isEdit.accept(!viewModel.isEdit.value)
        
        if !viewModel.isEdit.value {
            setupData()
        }
    }
    
    func handleEdit() {
        let isEdit = viewModel.isEdit.value
        let title = (isEdit ? "Huỷ" : "Sửa").localized()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(edit))
        navigationItem.rightBarButtonItem?.tintColor = AppColor.hex00A359
        
        nameTextField.isUserInteractionEnabled = isEdit
        phoneTextField.isUserInteractionEnabled = isEdit
        addressTextField.isUserInteractionEnabled = false
        
        acceptButton.superview?.isHidden = !isEdit
        
        rateLabel.superview?.isHidden = isEdit
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        viewModel.callApi(name: nameTextField.text, phone: phoneTextField.text)
    }
    
    @IBAction func changeAvatarAction(_ sender: Any) {
        if !viewModel.isEdit.value {
            return
        }
        
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else { return }
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.allowsEditing = false
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func rateAction(_ sender: Any) {
        mainCoordinator?.goToRatingVC(star: profileRecord?.getOrderRateAvgString() ?? "")
    }
}

extension MyProfileVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        
        viewModel.currentImage = image
        
        avatarImageView.image = image
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}

extension MyProfileVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case nameTextField:
            if string.isEmpty {
                return true
            }
            if string == " " {
                return true
            }
            return Utils.isValid(text: string, pattern: "[a-zA-Z]")
            
        case phoneTextField:
            if let text = phoneTextField.text,
                text.count >= 11,
                !string.isEmpty {
                return false
            }
            return true
            
        default:
            return true
        }
    }
}

extension MyProfileVC {
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.isEdit.subscribe(onNext: { [weak self] (_) in
            self?.handleEdit()
        }).disposed(by: viewModel.disposeBag)
    }
}
