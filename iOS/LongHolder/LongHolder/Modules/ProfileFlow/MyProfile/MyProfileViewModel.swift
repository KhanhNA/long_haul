//
//  MyProfileViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 9/1/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class MyProfileViewModel: BaseViewModel {
    var currentImage: UIImage?
    var profileRecord: ProfileRecord?
    
    let isEdit = BehaviorRelay<Bool>(value: false)
    
    func callApi(name: String?, phone: String?) {
        guard let name = name,
            let phone = phone,
            !name.isEmpty,
            !phone.isEmpty else {
                
                errorMessage.accept("Vui lòng không để trống các trường thông tin!".localized())
                
                return
        }
        
        if phone.count != 10 && phone.count != 11 {
            
            errorMessage.accept("Số điện thoại chỉ được nhập từ 10-11 kí tự!".localized())
            return
        }
        
        isShowLoading.accept(true)
        
        let parameter = UpdateAccountLongHaulParameter()
        parameter.name = name
        parameter.phone = phone
        
        ProfileApi.updateAccountLongHaul(image: currentImage, parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let stringResult = data?.stringResult, !stringResult.isEmpty else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            switch stringResult {
            case "error_driver_phone_number":
                self.errorMessage.accept("Số điện thoại đã tồn tại".localized())
                
            case "Update success":
                SuccessDialogView.show(message: "Cập nhật thành công".localized()) { [weak self] in
                    guard let self = self else { return }
                    
                    self.profileRecord?.name = name
                    self.profileRecord?.phone = phone
                    self.profileRecord?.currentImage = self.currentImage
                    
                    self.isEdit.accept(false)
                }
                
            default:
                self.errorMessage.accept("Có lỗi xảy ra".localized())
            }
        }
    }
}
