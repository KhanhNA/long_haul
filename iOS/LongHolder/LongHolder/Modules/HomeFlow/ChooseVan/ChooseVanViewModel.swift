//
//  ChooseVanViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/5/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChooseVanViewModel: BaseViewModel {
    // dữ liệu truyền từ màn hình NewConfirmVanVC
    // dùng để init isSelected của getListBiddingVehicleRecords
    var selectedGetListBiddingVehicleRecords: [BiddingVehicle]!
    
    // danh sách bidding_vehicle trên table view
    let getListBiddingVehicleRecords = BehaviorRelay<[BiddingVehicle]>(value: [])
    
    var isSelectAll: Bool {
        return getListBiddingVehicleRecords.value
            .filter({ $0.isShowing })
            .first(where: { !$0.isSelected }) == nil
    }
    
    func handleSelectAll() {
        let getListBiddingVehicleRecords = self.getListBiddingVehicleRecords.value
        if isSelectAll {
            getListBiddingVehicleRecords.filter({ $0.isShowing })
                .forEach{( $0.isSelected = false )}
        } else {
            getListBiddingVehicleRecords.filter({ $0.isShowing })
                .forEach{( $0.isSelected = true )}
        }
        self.getListBiddingVehicleRecords.accept(getListBiddingVehicleRecords)
    }
    
    func callApi() {
        let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
        guard let uid = loginModel?.uid else { return }
        
        isShowLoading.accept(true)
        
        HomeApi.getListBiddingVehicle(uID: uid) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            let records = data?.result?.records
            records?.forEach({ (getListBiddingVehicleRecord) in
                
                let selectedGetListBiddingVehicleRecord = self.selectedGetListBiddingVehicleRecords?.first(where: {
                    $0.id == getListBiddingVehicleRecord.id
                })
                getListBiddingVehicleRecord.isSelected = (selectedGetListBiddingVehicleRecord != nil)
                
            })
            
            self.getListBiddingVehicleRecords.accept(records ?? [])
        }
    }
    
    func isKeywordExistInRecord(getListBiddingVehicleRecord: BiddingVehicle, keyword: String) -> Bool {
        let keywordForSearch = keyword.stringForSearch()
        
        return (getListBiddingVehicleRecord.lisencePlate?.contains(keywordForSearch) == true
            || (getListBiddingVehicleRecord.driverName
                ?? getListBiddingVehicleRecord.name
                ?? "")?.stringForSearch().contains(keywordForSearch) == true
            || getListBiddingVehicleRecord.driverPhoneNumber?.contains(keywordForSearch) == true
            || getListBiddingVehicleRecord.idCard?.contains(keywordForSearch) == true)
    }
}
