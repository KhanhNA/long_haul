//
//  ChooseVanTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ChooseVanTableViewCell: UITableViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var lisencePlateLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverPhoneNumberLabel: UILabel!
    @IBOutlet weak var idCardLabel: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var line1View: UIView!
    @IBOutlet weak var line2View: UIView!
    
    var cargoTypes: [CargoType]?
    var deleteClosure: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.isHidden = false
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: VanCollectionViewCell.className)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 150, height: 20)
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(getListBiddingVehicleRecord: BiddingVehicle) {

        line1View.alpha = 1
        line2View.alpha = 0
        collectionView.superview?.isHidden = true
        
        let lisencePlate = String(format: "%@: %@", "BKS".localized(),
                                  getListBiddingVehicleRecord.lisencePlate ?? "")
        var tonnage = ""
        if let value = getListBiddingVehicleRecord.tonnage {
            tonnage = String(value) + " " + "tấn".localized()
        }
        
        let lisencePlateAndTonnage = NSMutableAttributedString()
        lisencePlateAndTonnage.append(lisencePlate.attributedString)
        
        if !tonnage.isEmpty {
            lisencePlateAndTonnage.append(" | ".attributedString)
            lisencePlateAndTonnage.append(tonnage.attributedString)
        }
        
        lisencePlateLabel.attributedText = lisencePlateAndTonnage
        
        driverNameLabel.text = String(format: "%@: %@", "Tài xế".localized(),
                                      getListBiddingVehicleRecord.driverName
                                        ?? getListBiddingVehicleRecord.name
                                        ?? "")
        
        driverPhoneNumberLabel.text = String(format: "%@: %@", "Số điện thoại".localized(),
                                             getListBiddingVehicleRecord.driverPhoneNumber ?? "")
        
        idCardLabel.text = String(format: "%@: %@", "CMT".localized(),
                                  getListBiddingVehicleRecord.idCard ?? "")
        
        checkButton.isSelected = getListBiddingVehicleRecord.isSelected
        
        avatarImageView.setImage(urlString: getListBiddingVehicleRecord.image128,
                                 placeholder: UIImage(named: "home_vc_avatar"))
    }
    
    // xem thông tin xe chở hàng
    // từ màn hình detail
    func setup(biddingVehicle: BiddingVehicle) {
        
        self.cargoTypes = biddingVehicle.cargoTypes ?? []
        
        line1View.alpha = 0
        line2View.alpha = 1
        collectionView.superview?.isHidden = false
        collectionView.reloadData()
        
        let lisencePlate = String(format: "%@: %@", "BKS".localized(),
                                  biddingVehicle.lisencePlate ?? "")
        var tonnage = ""
        if let value = biddingVehicle.tonnage {
            tonnage = String(value) + " " + "tấn".localized()
        }
        
        let lisencePlateAndTonnage = NSMutableAttributedString()
        lisencePlateAndTonnage.append(lisencePlate.attributedString)
       
        if !tonnage.isEmpty {
            lisencePlateAndTonnage.append(" | ".attributedString)
            lisencePlateAndTonnage.append(tonnage.attributedString)
        }
        
        lisencePlateLabel.attributedText = lisencePlateAndTonnage
        
        driverNameLabel.text = String(format: "%@: %@", "Tài xế".localized(),
                                      biddingVehicle.driverName ?? "")
        
        driverPhoneNumberLabel.text = String(format: "%@: %@", "Số điện thoại".localized(),
                                             biddingVehicle.driverPhoneNumber ?? "")
        
        idCardLabel.text = String(format: "%@: %@", "CMT".localized(),
                                  biddingVehicle.idCard ?? "")
        
        avatarImageView.setImage(urlString: biddingVehicle.image128,
                                 placeholder: UIImage(named: "home_vc_avatar"))
    }
    
    func hideRightView() {
        deleteButton.isHidden = true
        checkButton.isHidden = true
    }
    
    func showCheckButton() {
        deleteButton.isHidden = true
        checkButton.isHidden = false
    }
    
    func showDeleteButton() {
        deleteButton.isHidden = false
        checkButton.isHidden = true
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        deleteClosure?()
    }
}

extension ChooseVanTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cargoTypes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(VanCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }
        
        if let cargoTypes = cargoTypes {
            let item = cargoTypes[indexPath.item]
            cell.idLabel.text = String(format: "%@ %@",
                                       String(item.cargoQuantity ?? 0),
                                       String(item.type ?? ""))
        } else {
            cell.idLabel.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
