//
//  ChooseVanView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class ChooseVanView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    private let viewModel = ChooseVanViewModel()
    private var timer: Timer?
    var completion: ((_ getListBiddingVehicleRecords: [BiddingVehicle]) -> ())?
    
    static func show(selectedGetListBiddingVehicleRecords: [BiddingVehicle],
                     completion: ((_ getListBiddingVehicleRecords: [BiddingVehicle]) -> ())?) {
        let view = ChooseVanView()
        view.show {
            view.completion = completion
            
            view.tableView.register(cellNibName: ChooseVanTableViewCell.className)
            
            view.tableView.dataSource = view
            view.tableView.delegate = view
            
            view.titleLabel.text = "Chọn xe chở hàng".localized()
            view.searchTextField.placeholder = "Tìm kiếm biển số xe, tên tài xế, số điện thoại xe".localized()
            
            view.confirmButton.setTitle("Xác nhận".localized(), for: .normal)
            
            view.updateCheckButton()
            
            view.handleSubscribe()
            
            view.viewModel.selectedGetListBiddingVehicleRecords = selectedGetListBiddingVehicleRecords
            view.viewModel.callApi()
        }
    }
    
    private func handleSubscribe() {
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.makeToast(error.localizedDescription,
                           duration: 1,
                           position: CSToastPositionCenter,
                           style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.makeToast(value,
                           duration: 1,
                           position: CSToastPositionCenter,
                           style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getListBiddingVehicleRecords.subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.updateCheckButton()
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @objc private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    private func updateCheckButton() {
        let isSelectAll = (viewModel.getListBiddingVehicleRecords.value
            .filter({ $0.isShowing })
            .first(where: { !$0.isSelected }) == nil)
        let named = (isSelectAll ? "home_vc_check" : "home_vc_uncheck")
        checkButton.setImage(UIImage(named: named), for: .normal)
    }
    
    @IBAction func searchTextFieldEditingChanged(_ sender: Any) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5,
                                     target: self,
                                     selector: #selector(reloadData),
                                     userInfo: nil,
                                     repeats: false)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func checkAction(_ sender: Any) {
        viewModel.handleSelectAll()
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        completion?(viewModel.getListBiddingVehicleRecords.value.filter({ $0.isSelected }))
        dismiss()
    }
}

extension ChooseVanView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getListBiddingVehicleRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(ChooseVanTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.showCheckButton()
        
        let getListBiddingVehicleRecord = viewModel.getListBiddingVehicleRecords.value[indexPath.row]
        cell.setup(getListBiddingVehicleRecord: getListBiddingVehicleRecord)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getListBiddingVehicleRecord = viewModel.getListBiddingVehicleRecords.value[indexPath.row]
        getListBiddingVehicleRecord.isSelected = !getListBiddingVehicleRecord.isSelected
        
        tableView.reloadData()
        
        updateCheckButton()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let keyword = searchTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let getListBiddingVehicleRecord = viewModel.getListBiddingVehicleRecords.value[indexPath.row]
        
        let isExist = viewModel.isKeywordExistInRecord(getListBiddingVehicleRecord: getListBiddingVehicleRecord, keyword: keyword)
        let isShowCell = (keyword.isEmpty || isExist)
        
        getListBiddingVehicleRecord.isShowing = isShowCell
        
        return (isShowCell ? UITableView.automaticDimension : 0.001)
    }
}
