//
//  DetailBiddingViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/7/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class DetailBiddingViewModel: BaseViewModel {
    // dữ liệu để gọi api detail
    var id: String!
    
    // dự liệu truyền từ new home vc
    // kiểm tra trạng thái của bidding (Đang diễn ra || Sắp diễn ra)
    var biddingPackageTimeModel: BiddingPackageTimeModel?
    
    // dữ liệu khi gọi api detail trả về
    // dành cho màn hình new home
    let getBiddingPackageDetailRecord = BehaviorRelay<GetBiddingPackageDetailRecord?>(value: nil)
    
    // dữ liệu khi gọi api detail trả về
    // dành cho màn hình home
    let getBiddingDetailRecord = BehaviorRelay<GetBiddingDetailRecord?>(value: nil)
    
    func getDetail() {
        if biddingPackageTimeModel != nil {
            getDetailFromNewHome()
        } else {
            getDetailFromHome()
        }
    }
    
    func getDetailFromNewHome() {
        
        guard let id = id else { return }
        
        isShowLoading.accept(true)
        
        NewHomeApi.getBiddingPackageDetail(id: id) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result, let getBiddingPackageDetailRecord = result.records?.first else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            self.getBiddingPackageDetailRecord.accept(getBiddingPackageDetailRecord)
        }
    }
    
    func getDetailFromHome() {
        
        guard let id = id else { return }
        
        isShowLoading.accept(true)
        
        NewHomeApi.getBiddingOrderDetail(id: id) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result, let getBiddingDetailRecord = result.records?.first else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            self.getBiddingDetailRecord.accept(getBiddingDetailRecord)
        }
    }
}
