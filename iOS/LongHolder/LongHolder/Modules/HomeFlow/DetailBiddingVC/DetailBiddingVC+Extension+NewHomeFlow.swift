//
//  DetailBiddingVC+Extension.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/18/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

// xử lý dữ liệu từ luồng new home
// GetBiddingPackageDetailRecord
extension DetailBiddingVC {
    func subscribeGetBiddingPackageDetailRecord() {
        // dữ liệu khi gọi api detail trả về
        viewModel.getBiddingPackageDetailRecord.subscribe(onNext: { [weak self] (getBiddingPackageDetailRecord) in
            
            guard let self = self,
                let getBiddingPackageDetailRecord = getBiddingPackageDetailRecord,
                let fromLatitude = getBiddingPackageDetailRecord.fromLatitude,
                let fromLongitude = getBiddingPackageDetailRecord.fromLongitude,
                let toLatitude = getBiddingPackageDetailRecord.toLatitude,
                let toLongitude = getBiddingPackageDetailRecord.toLongitude else { return }
            
            self.handleMap(fromLatitude: fromLatitude,
                           fromLongitude: fromLongitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude)
            
            let fromAddress = getBiddingPackageDetailRecord.fromDepot?.address ?? ""
            let toAddress = getBiddingPackageDetailRecord.toDepot?.address ?? ""
            
            _ = self.mapView.addMarker(latitude: fromLatitude,
                                 longitude: fromLongitude,
                                 imageName: "home_vc_map_from",
                                 title: fromAddress,
                                 snippet: "")
            
            _ = self.mapView.addMarker(latitude: toLatitude,
                                 longitude: toLongitude,
                                 imageName: "home_vc_map_to",
                                 title: toAddress,
                                 snippet: "")
            
            self.addMakers(biddingVehicles: getBiddingPackageDetailRecord.biddingVehicles)
            
            self.setup(getBiddingPackageDetailRecord: getBiddingPackageDetailRecord)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    func setup(getBiddingPackageDetailRecord: GetBiddingPackageDetailRecord) {
        
        // top view
        fromDepotAddressLabel.text = getBiddingPackageDetailRecord.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getBiddingPackageDetailRecord.getFromReceiveTimeToReceiveTime()
        toDepotAddressLabel.text = getBiddingPackageDetailRecord.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getBiddingPackageDetailRecord.getFromReturnTimeToReturnTime()
        distanceLabel.text = getBiddingPackageDetailRecord.getDistance()
        
        // bottom view
        createDateLabel.isHidden = (biddingPackageTimeModel != nil)
        createDateLabel.text = getBiddingPackageDetailRecord.getCreateDate()
        idLabel.text = getBiddingPackageDetailRecord.getId()
        totalCargoValueLabel.text = getBiddingPackageDetailRecord.getTotalCargo()
        totalWeightValueLabel.text = getBiddingPackageDetailRecord.getTotalWeight()
        
        let price: NSMutableAttributedString = NSMutableAttributedString(string: getBiddingPackageDetailRecord.getPrice(currency: "đ"))
        if getBiddingPackageDetailRecord.isCancel {
            price.addStrikethrough()
        }
        
        priceLabel.attributedText = price.color(priceLabel.textColor)
        
        updateBiddingButtonGetBiddingPackageDetailRecord()
    }
    
    func updateBiddingButtonGetBiddingPackageDetailRecord() {
        if biddingPackageTimeModel?.type == .happenning {
            
            biddingButton.setTitle("ĐẤU THẦU".localized(), for: .normal)
            
        } else {
            if let id = id,
                let (eventModel, event) = EventModel.getEKEvent(id: String(id)),
                eventModel != nil, event != nil {
                
                biddingButton.setTitle("Hủy nhắc nhở".localized(), for: .normal)
                biddingButton.setTitleColor(AppColor.hex919395, for: .normal)
                biddingButton.borderColor = AppColor.hex919395
                
            } else {
                
                biddingButton.setTitle("Nhắc nhở tôi".localized(), for: .normal)
                biddingButton.setTitleColor(AppColor.hex00A359, for: .normal)
                biddingButton.borderColor = AppColor.hex00A359
                
            }
        }
    }
    
    func addEvent() {
        let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self,
                                                        fromKey: "loginModel")
        
        let announceTimeBefore = loginModel?.announce_time_before ?? 1
        
        let getBiddingPackageDetailRecord = viewModel.getBiddingPackageDetailRecord.value
        
        let title = String(format: "[LongHaul Bidding][%@ - %@ - %@]",
                           getBiddingPackageDetailRecord?.biddingPackageNumber ?? "",
                           getBiddingPackageDetailRecord?.getDistance() ?? "",
                           getBiddingPackageDetailRecord?.getPrice(currency: "đ") ?? "")
        
        let notes = String(format: "Gói thầu từ %@ tới %@ sẽ bắt đầu lúc %@".localized(),
                           getBiddingPackageDetailRecord?.fromDepot?.address ?? "",
                           getBiddingPackageDetailRecord?.toDepot?.address ?? "",
                           biddingPackageTimeModel?.date.toString(with: "HH:mm") ?? "")
        
        let completion: (_ error: Error?) -> () = { [weak self] error in
            guard let self = self else { return }
            
            if let error = error {
                DispatchQueue.main.async { [weak self] in
                    self?.viewModel.errorMessage.accept(error.localizedDescription)
                }
                return
            }
            
            let message = String(format: "Nhắc nhở sẽ được gửi đến bạn %@ phút trước khi gói thầu bắt đầu".localized(),
                                 String(announceTimeBefore))
            
            DispatchQueue.main.async {
                SuccessDialogView.show(message: message) { [weak self] in
                    self?.updateBiddingButtonGetBiddingPackageDetailRecord()
                }
            }
        }
        
        EventHelper.addEventToCalendar(id: String(id),
                                       title: title,
                                       notes: notes,
                                       startDate: biddingPackageTimeModel?.date ?? Date(),
                                       announceTimeBefore: announceTimeBefore,
                                       completion: completion)
    }
    
    // xử lý ĐẤU THẦU hoặc Nhắc nhở tôi
    func biddingGetBiddingPackageDetailRecord() {
        if biddingPackageTimeModel?.type == .happenning {
            
            // ĐẤU THẦU
            
            ConfirmDialogView.show(title: "Xác nhận tham gia đấu thầu!".localized(), yesTitle: "Xác nhận".localized(), noTitle: "Huỷ bỏ".localized(), yesAction: { [weak self] in
                
                self?.biddingViewModel.biddingCargo()
                
                }, noAction: { })
            
        } else {
            
            // Nhắc nhở tôi
            
            guard let id = id else { return }
            
            if let (eventModel, event) = EventModel.getEKEvent(id: String(id)),
                let _ = eventModel,
                let _ = event {
                
                EventHelper.removeEvent(event!)
                
                eventModel!.delete()
                
                updateBiddingButtonGetBiddingPackageDetailRecord()
                
                
            } else {
                
                addEvent()
            }
        }
    }
    
    func showCargoDetailGetBiddingPackageDetailRecord() {
        CargoListView.show(list: viewModel.getBiddingPackageDetailRecord.value?.cargoTypes ?? [])
    }
}
