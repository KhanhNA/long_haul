//
//  DetailBiddingVC+Extension2.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/18/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

// xử lý dữ liệu từ luồng home
// GetBiddingDetailRecord
extension DetailBiddingVC {
    func subscribeGetBiddingDetailRecord() {
        // dữ liệu khi gọi api detail trả về
        viewModel.getBiddingDetailRecord.subscribe(onNext: { [weak self] (getBiddingDetailRecord) in
            
            guard let self = self,
                let getBiddingDetailRecord = getBiddingDetailRecord,
                let fromLatitude = getBiddingDetailRecord.fromLatitude,
                let fromLongitude = getBiddingDetailRecord.fromLongitude,
                let toLatitude = getBiddingDetailRecord.toLatitude,
                let toLongitude = getBiddingDetailRecord.toLongitude else { return }
            
            self.handleMap(fromLatitude: fromLatitude,
                           fromLongitude: fromLongitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude)
            
            let fromAddress = getBiddingDetailRecord.biddingOrder?.fromDepot?.address ?? ""
            let toAddress = getBiddingDetailRecord.biddingOrder?.toDepot?.address ?? ""
            
            _ = self.mapView.addMarker(latitude: fromLatitude,
                                 longitude: fromLongitude,
                                 imageName: "home_vc_map_from",
                                 title: fromAddress,
                                 snippet: "")
            
            _ = self.mapView.addMarker(latitude: toLatitude,
                                 longitude: toLongitude,
                                 imageName: "home_vc_map_to",
                                 title: toAddress,
                                 snippet: "")
            
            self.addMakers(biddingVehicles: getBiddingDetailRecord.biddingOrder?.biddingVehicles)
            
            self.setup(getBiddingDetailRecord: getBiddingDetailRecord)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    func setup(getBiddingDetailRecord: GetBiddingDetailRecord) {
        
        // top view
        fromDepotAddressLabel.text = getBiddingDetailRecord.biddingOrder?.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getBiddingDetailRecord.biddingOrder?.getFromReceiveTimeToReceiveTime()
        toDepotAddressLabel.text = getBiddingDetailRecord.biddingOrder?.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getBiddingDetailRecord.biddingOrder?.getFromReturnTimeToReturnTime()
        distanceLabel.text = getBiddingDetailRecord.biddingOrder?.getDistance()
        
        // bottom view
        createDateLabel.isHidden = (biddingPackageTimeModel != nil)
        createDateLabel.text = getBiddingDetailRecord.biddingOrder?.getCreateDate()
        idLabel.text = getBiddingDetailRecord.biddingOrder?.getId()
        totalCargoValueLabel.text = getBiddingDetailRecord.biddingOrder?.getTotalCargo()
        totalWeightValueLabel.text = getBiddingDetailRecord.biddingOrder?.getTotalWeight()
        
        let price: NSMutableAttributedString = NSMutableAttributedString(string: getBiddingDetailRecord.biddingOrder?.getPrice(currency: "đ") ?? "")
        if getBiddingDetailRecord.biddingOrder?.isCancel == true {
            price.addStrikethrough()
        }
        
        priceLabel.attributedText = price.color(priceLabel.textColor)
        
        updateBiddingButtonGetBiddingDetailRecord(getBiddingDetailRecord: getBiddingDetailRecord)
    }
    
    func updateBiddingButtonGetBiddingDetailRecord(getBiddingDetailRecord: GetBiddingDetailRecord) {
        
        var subtitle = ""
        var subtitleColor = AppColor.hex919395
        
        if getBiddingDetailRecord.biddingOrder?.homeType == .success {
            
            if getBiddingDetailRecord.biddingOrder?.isCancel == true {
                subtitle = String(format: "(%@)", "Đã huỷ".localized())
                subtitleColor = AppColor.hexFF0C20
                
            } else {
                
                if getBiddingDetailRecord.biddingOrder?.status == "0" {
                    // Chưa vận chuyển
                    subtitle = String(format: "(%@ - %@)",
                                      "Đấu thầu thành công".localized(),
                                      "Chưa vận chuyển".localized())
                    
                } else if getBiddingDetailRecord.biddingOrder?.status == "1" {
                    // Đang vận chuyển
                    subtitle = String(format: "(%@ - %@)",
                                      "Đấu thầu thành công".localized(),
                                      "Đang vận chuyển".localized())
                    
                } else if getBiddingDetailRecord.biddingOrder?.status == "2" {
                    // Đã trả hàng
                    subtitle = String(format: "(%@ - %@)",
                                      "Đấu thầu thành công".localized(),
                                      "Đã trả hàng".localized())
                }
            }
            
        } else if getBiddingDetailRecord.biddingOrder?.homeType == .missing {
            
            subtitle = String(format: "(%@)", "Thiếu thông tin lái xe".localized())
            
        } else {
            
            subtitle = String(format: "(%@)", getBiddingDetailRecord.biddingOrder?.homeType?.getTitle() ?? "")
            
        }
        
        setupCustomNavigationController(title: "Chi tiết gói thầu".localized(),
                                        subtitle: subtitle,
                                        subtitleColor: subtitleColor)
        
        if getBiddingDetailRecord.biddingOrder?.homeType == .missing {
            
            biddingButton.setTitle("Điền thông tin xe".localized(), for: .normal)
            
        } else if getBiddingDetailRecord.biddingOrder?.homeType == .wait {
            
            biddingButton.setTitle("Thông tin xe chở hàng".localized(), for: .normal)
            
        } else {
            
            if getBiddingDetailRecord.biddingOrder?.isCancel == true {
                biddingButton.setTitle("Lý do bị hủy".localized(), for: .normal)
            } else {
                biddingButton.setTitle("Thông tin xe chở hàng".localized(), for: .normal)
            }
        }
    }
    
    // xử lý Điền thông tin xe
    // Hoặc Thông tin xe chở hàng
    // Hoặc Lý do bị huỷ
    func biddingGetBiddingDetailRecord() {
        if viewModel.getBiddingDetailRecord.value?.biddingOrder?.homeType == .missing {
            
            if let maxConfirmTime = viewModel
                .getBiddingDetailRecord.value?
                .biddingOrder?
                .maxConfirmTime?.toString(),
                
                let biddingOrderId = viewModel
                    .getBiddingDetailRecord.value?
                    .biddingOrder?.id {
                
                // Điền thông tin xe
                mainCoordinator?
                    .goToNewConfirmVanVC(maxConfirmTime: maxConfirmTime,
                                         biddingOrderId: String(biddingOrderId),
                                         getListBiddingVehicleRecords: nil,
                                         subtitle: viewModel
                                            .getBiddingDetailRecord.value?
                                            .biddingOrder?
                                            .getSubtitleForNewConfirmVanVC() ?? "",
                                         isBiddingFlow: false)
            }
            
        } else if viewModel.getBiddingDetailRecord.value?.biddingOrder?.homeType == .wait {
            
            // Thông tin xe chở hàng
            // cho phép chỉnh sửa
            
            if let biddingOrderId = viewModel
                .getBiddingDetailRecord.value?
                .biddingOrder?.id {
                
                let biddingVehicles = viewModel.getBiddingDetailRecord.value?.biddingOrder?.biddingVehicles
                
                VanListView.show(list: biddingVehicles ?? []) { [weak self] in
                    
                    // chỉnh sửa
                    self?.mainCoordinator?
                        .goToNewConfirmVanVC(maxConfirmTime: nil,
                                             biddingOrderId: String(biddingOrderId),
                                             getListBiddingVehicleRecords: biddingVehicles,
                                             subtitle: self?.viewModel
                                                .getBiddingDetailRecord.value?
                                                .biddingOrder?
                                                .getSubtitleForNewConfirmVanVC() ?? "",
                                             isBiddingFlow: false)
                }
            }
            
        } else {
            
            if viewModel.getBiddingDetailRecord.value?.biddingOrder?.isCancel == true {
                
                // Lý do bị huỷ
                ReasonView.show(title: "Lý do hủy gói thầu".localized(),
                                detail: viewModel.getBiddingDetailRecord.value?.biddingOrder?.note ?? "")
                
            } else {
                // chỉ cho phép xem
                
                let biddingVehicles = viewModel.getBiddingDetailRecord.value?.biddingOrder?.biddingVehicles
                
                VanListView.show(list: biddingVehicles ?? [], editClosure: nil)
            }
        }
    }
    
    func showCargoDetailGetBiddingDetailRecord() {
        let cargoTypes = viewModel.getBiddingDetailRecord.value?.cargoTypes ?? []
        CargoListView.show(list: cargoTypes)
    }
}
