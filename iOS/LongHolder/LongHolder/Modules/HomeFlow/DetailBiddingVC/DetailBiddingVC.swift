//
//  DetailBiddingVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import GoogleMaps
import FittedSheets
import MarqueeLabel

class DetailBiddingVC: BaseVC {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var fromDepotAddressLabel: MarqueeLabel!
    @IBOutlet weak var fromReceiveTimeToReceiveTimeLabel: UILabel!
    @IBOutlet weak var toDepotAddressLabel: MarqueeLabel!
    @IBOutlet weak var fromReturnTimeToReturnTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var infoBottomView: UIStackView!
    
    @IBOutlet weak var createDateLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var totalCargoLabel: UILabel!
    @IBOutlet weak var totalCargoValueLabel: UILabel!
    @IBOutlet weak var totalWeightLabel: UILabel!
    @IBOutlet weak var totalWeightValueLabel: UILabel!
    
    @IBOutlet weak var cargoDetailButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var receiveActualTimeLabel: UILabel!
    @IBOutlet weak var receiveActualTimeValueLabel: UILabel!
    @IBOutlet weak var returnActualTimeLabel: UILabel!
    @IBOutlet weak var returnActualTimeValueLabel: UILabel!
    
    @IBOutlet weak var biddingButton: UIButton!
    
    @IBOutlet weak var mapContainerView: UIView!
    
    var mapView: GMSMapView!
    
    let viewModel = DetailBiddingViewModel()
    let biddingViewModel = BiddingViewModel()
    
    // nếu đi từ new home: bidding_package_id
    // nếu đi từ home: bidding_order_id
    var id: String!
    
    // kiểm tra trạng thái của bidding (Đang diễn ra || Sắp diễn ra)
    var biddingPackageTimeModel: BiddingPackageTimeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.id = id
        viewModel.biddingPackageTimeModel = biddingPackageTimeModel
        
        biddingViewModel.biddingPackageId = id
        
        setupNavigationController()
        
        setupViews()
        
        setupBottomView()
        
        handleSubscribe()
        
        viewModel.getDetail()
    }
    
    func setupViews() {
        totalCargoLabel.text = "Tổng số lượng cargo".localized()
        totalWeightLabel.text = "Tổng cân nặng".localized()
        cargoDetailButton.setTitle("Chi tiết loại cargo".localized(), for: .normal)
        receiveActualTimeLabel.text = "Nhận hàng thực tế".localized()
        returnActualTimeLabel.text = "Trả hàng thực tế".localized()
        
        receiveActualTimeLabel.superview?.isHidden = true
        returnActualTimeLabel.superview?.isHidden = true
        
        topView.dropShadow(cornerRadius: 12)
        bottomView.dropShadow(cornerRadius: 24)
        
        fromDepotAddressLabel.setupMarqueeLabel()
        toDepotAddressLabel.setupMarqueeLabel()
    }
    
    func setupNavigationController() {
        if let biddingPackageTimeModel = biddingPackageTimeModel {
            let subtitle = String(format: "(%@)", biddingPackageTimeModel.getTitle())
            setupCustomNavigationController(title: "Chi tiết gói thầu".localized(),
                                            subtitle: subtitle)
        } else {
            setupNavigationController(title: "Chi tiết gói thầu".localized())
        }
    }
    
    func handleSubscribe() {
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        biddingViewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        biddingViewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        biddingViewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // dữ liệu khi gọi api createBiddingOrder trả về
        biddingViewModel.createBiddingOrderRecord.subscribe(onNext: { [weak self] (createBiddingOrderRecord) in
            
            guard let self = self,
                let maxConfirmTime = createBiddingOrderRecord?.maxConfirmTime?.toString(),
                let biddingOrderID = createBiddingOrderRecord?.biddingOrderID
                else { return }
            
            let getBiddingPackageDetailRecord = self.viewModel
                .getBiddingPackageDetailRecord.value
            
            self.mainCoordinator?
                .goToNewConfirmVanVC(maxConfirmTime: maxConfirmTime,
                                     biddingOrderId: String(biddingOrderID),
                                     getListBiddingVehicleRecords: nil,
                                     subtitle: getBiddingPackageDetailRecord?
                                        .getSubtitleForNewConfirmVanVC() ?? "",
                                     isBiddingFlow: true)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // ---------------------------------- //
        
        subscribeGetBiddingPackageDetailRecord()
        
        subscribeGetBiddingDetailRecord()
    }
    
    @IBAction func cargoDetailAction(_ sender: Any) {
        
        if biddingPackageTimeModel != nil {
            showCargoDetailGetBiddingPackageDetailRecord()
        } else {
            showCargoDetailGetBiddingDetailRecord()
        }
    }
    
    @IBAction func biddingAction(_ sender: Any) {
        
        if biddingPackageTimeModel != nil {
            
            // xử lý ĐẤU THẦU hoặc Nhắc nhở tôi
            
            biddingGetBiddingPackageDetailRecord()
            
        } else {
            
            // xử lý Điền thông tin xe
            // Hoặc Thông tin xe chở hàng
            // Hoặc Lý do bị huỷ
            
            biddingGetBiddingDetailRecord()
        }
    }
}

extension DetailBiddingVC {
    func setupBottomView() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        swipeDown.direction = .down
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        swipeUp.direction = .up
        
        bottomView.addGestureRecognizer(swipeDown)
        bottomView.addGestureRecognizer(swipeUp)
        
        infoBottomView.isHidden = false
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        if sender.direction == .down {
            
            if infoBottomView.isHidden {
                return
            }
            
            infoBottomView.alpha = 0
            infoBottomView.hideAnimated(in: bottomStackView, completion: { })
            
        } else if sender.direction == .up {
            
            if !infoBottomView.isHidden {
                return
            }
            
            infoBottomView.showAnimated(in: bottomStackView, completion: { })
            infoBottomView.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.infoBottomView.alpha = 1
            }
        }
    }
}

extension DetailBiddingVC {
    func handleMap(fromLatitude: Double?,
                   fromLongitude: Double?,
                   toLatitude: Double?,
                   toLongitude: Double?) {
        
        guard let fromLatitude = fromLatitude,
            let fromLongitude = fromLongitude,
            let toLatitude = toLatitude,
            let toLongitude = toLongitude else { return }
        
        let edgeInsets = UIEdgeInsets(top: topView.frame.height + 75,
                                      left: 20,
                                      bottom: bottomView.frame.height,
                                      right: 20)
        
        if mapView == nil {
            mapView = MapsHelper.addMap(in: mapContainerView,
                                        fromLatitude: fromLatitude,
                                        fromLongitude: fromLongitude,
                                        toLatitude: toLatitude,
                                        toLongitude: toLongitude,
                                        edgeInsets: edgeInsets)
        } else {
            mapView.update(fromLatitude: fromLatitude,
                           fromLongitude: fromLongitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude,
                           edgeInsets: edgeInsets)
        }
    }
    
    
    func addMakers(biddingVehicles: [BiddingVehicle]?) {
        for item in biddingVehicles ?? [] {
            
            if let latitude = item.actionLog?.first?.latitude,
                let longitude = item.actionLog?.first?.longitude {
                
                let imageName = (item.status == "1" ? "van_vc_has_order" : "van_vc_no_order")
                
                let title = item.lisencePlate ?? ""
                
                let snippet = String(format: "%@: %@\n%@: %@",
                                     "Tài xế".localized(), item.driverName ?? "",
                                     "Sđt".localized(), item.driverPhoneNumber ?? "")
                
                _ = mapView.addMarker(latitude: latitude,
                                     longitude: longitude,
                                     imageName: imageName,
                                     title: title,
                                     snippet: snippet)
            }
        }
    }
}
