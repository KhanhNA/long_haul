//
//  BiddingViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BiddingViewModel: BaseViewModel {
    // dữ liệu để gọi api detail
    var biddingPackageId: String!
    
    // dữ liệu khi gọi api createBiddingOrder trả về
    let createBiddingOrderRecord = BehaviorRelay<CreateBiddingOrderRecord?>(value: nil)
    
    func biddingCargo() {
        
        guard let biddingPackageId = biddingPackageId else { return }
        
        isShowLoading.accept(true)
        
        NewHomeApi.createBiddingOrder(biddingPackageId: biddingPackageId) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result,
                let createBiddingOrderRecord = result.records?.first else {
                    self.errorMessage.accept("Có lỗi xảy ra".localized())
                    return
            }
            
            self.createBiddingOrderRecord.accept(createBiddingOrderRecord)
        }
    }
}
