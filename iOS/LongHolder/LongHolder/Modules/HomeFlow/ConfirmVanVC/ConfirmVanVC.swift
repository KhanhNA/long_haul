//
//  ConfirmVanVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import DropDown

enum ConfirmVanParentScreen {
    case home
    case detail
    case van
}

class ConfirmVanVC: BaseVC {
    @IBOutlet weak var driverNameTextField: UITextField!
    @IBOutlet weak var driverPhoneNumberTextField: UITextField!
    @IBOutlet weak var lisencePlateTextField: UITextField!
    @IBOutlet weak var idCardTextField: UITextField!
    @IBOutlet weak var grossTonTextField: UITextField!
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var driverNameArrowButton: UIButton!
    @IBOutlet weak var driverPhoneNumberArrowButton: UIButton!
    @IBOutlet weak var lisencePlateArrowButton: UIButton!
    @IBOutlet weak var idCardArrowButton: UIButton!
    @IBOutlet weak var lisencePlateButton: UIButton!
    
    private let imagePickerController = UIImagePickerController()
    
    private let viewModel = ConfirmVanViewModel()
    
    // dùng với trường hợp update
    var getListVehicleRecord: GetListVehicleRecord?
    
    var reloadTableViewVanVC: (() -> ())?
    var reloadApiVanVC: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        viewModel.getListVehicleRecord = getListVehicleRecord
        
        setupViews()
        
        handleMode()
        
        handleSubscribe()
        
        viewModel.callApiGetVehicleTonnage()
    }
    
    private func setupViews() {
        driverNameTextField.placeholder = "Họ tên tài xế".localized()
        driverPhoneNumberTextField.placeholder = "Số điện thoại tài xế".localized()
        lisencePlateTextField.placeholder = "Biển số xe".localized()
        idCardTextField.placeholder = "Số chứng minh thư".localized()
        grossTonTextField.placeholder = "Trọng tải".localized()
        
        confirmButton.setTitle("Xác nhận".localized(), for: .normal)
        skipButton.setTitle("Huỷ".localized(), for: .normal)
        
        driverNameTextField.delegate = self
        driverPhoneNumberTextField.delegate = self
        lisencePlateTextField.delegate = self
        idCardTextField.delegate = self
    }
    
    private func handleMode() {
        if getListVehicleRecord != nil {
            
            // update
            setupNavigationController(title: "Chỉnh sửa thông tin xe".localized())
            lisencePlateButton.isHidden = false
            
            driverNameTextField.text = getListVehicleRecord?.driverName
            driverPhoneNumberTextField.text = getListVehicleRecord?.driverPhoneNumber
            lisencePlateTextField.text = getListVehicleRecord?.lisencePlate
            idCardTextField.text = getListVehicleRecord?.idCard
            
            let selectedGetVehicleTonnageRecord = GetVehicleTonnageRecord()
            selectedGetVehicleTonnageRecord.maxTonage = getListVehicleRecord?.tonnage
            viewModel.selectedGetVehicleTonnageRecord.accept(selectedGetVehicleTonnageRecord)
            
            avatarImageView.setImage(urlString: getListVehicleRecord?.image128,
                                     placeholder: UIImage(named: "confirm_van_vc_avatar"))
        } else {
            
            // create
            
            setupNavigationController(title: "Tạo mới thông tin xe".localized())
            lisencePlateButton.isHidden = true
        }
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // Danh sách trọng tải xe
        viewModel.getVehicleTonnageRecords.subscribe(onNext: { (getVehicleTonnageRecords) in
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.selectedGetVehicleTonnageRecord.subscribe(onNext: { [weak self] (getVehicleTonnageRecord) in
            
            if let maxTonage = getVehicleTonnageRecord?.maxTonage {
                self?.grossTonTextField.text = String(maxTonage) + " " + "tấn".localized()
            } else {
                self?.grossTonTextField.text = ""
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.popVC.subscribe(onNext: { [weak self] (value) in
            
            guard let self = self, value else { return }
            
            if self.getListVehicleRecord?.id != nil {
                self.reloadTableViewVanVC?()
            } else {
                self.reloadApiVanVC?()
            }
            
            self.navigationController?.popViewController(animated: true)
            
            }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func chooseVanAction(_ sender: Any) {
        guard confirmButton.isEnabled else { return }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        
        guard viewModel.checkValidDataAndShowMessageIfNeed(driverName: driverNameTextField.text,
                                                           driverPhone: driverPhoneNumberTextField.text,
                                                           lisencePlate: lisencePlateTextField.text,
                                                           idCard: idCardTextField.text) else { return }
        
        viewModel.createOrUpdateAccountDriver(driverPhone: driverPhoneNumberTextField.text,
                                              lisencePlate: lisencePlateTextField.text,
                                              idTonnage: viewModel.selectedGetVehicleTonnageRecord.value?.id,
                                              driverName: driverNameTextField.text,
                                              idCard: idCardTextField.text)
    }
    
    @IBAction func changeAvatarAction(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else { return }
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.allowsEditing = false
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func changeGrossTonAction(_ sender: Any) {
        let dropDown = DropDown()
        
        dropDown.anchorView = sender as? UIButton
        
        dropDown.dataSource = viewModel.getVehicleTonnageRecords.value.compactMap({
            if let maxTonage = $0.maxTonage {
                return String(maxTonage)
            }
            return ""
        })
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else { return }
            self.viewModel.selectedGetVehicleTonnageRecord.accept(self.viewModel.getVehicleTonnageRecords.value[index])
        }
        
        dropDown.width = (sender as? UIButton)?.frame.width
        
        dropDown.dismissMode = .onTap
        
        dropDown.show()
    }
    
    @IBAction func lisencePlateAction(_ sender: Any) {
        viewModel.errorMessage.accept("Không được chỉnh sửa biểm kiểm soát xe!".localized())
    }
}

extension ConfirmVanVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        
        viewModel.currentImage = image
        
        avatarImageView.image = image
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}

extension ConfirmVanVC: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        switch textField {
        case driverNameTextField:
            if string.isEmpty {
                return true
            }
            if string == " " {
                return true
            }
            return Utils.isValid(text: string, pattern: "[a-zA-Z]")
            
        case driverPhoneNumberTextField:
            if let text = driverPhoneNumberTextField.text,
                text.count >= 11, !string.isEmpty {
                return false
            }
            return true
            
        case lisencePlateTextField:
            return true
            
        case idCardTextField:
            if let text = idCardTextField.text,
                text.count >= 10, !string.isEmpty {
                return false
            }
            return true
            
        default:
            return true
        }
    }
}
