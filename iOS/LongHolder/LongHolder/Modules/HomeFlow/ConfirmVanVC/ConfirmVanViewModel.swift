//
//  ConfirmVanViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ConfirmVanViewModel: BaseViewModel {
    var popVC = BehaviorRelay<Bool>(value: false)
    
    var currentImage: UIImage? // avatar chọn từ thư viện, update qua api
    
    // dùng với trường hợp update
    var getListVehicleRecord: GetListVehicleRecord?
    
    // Danh sách trọng tải xe
    let getVehicleTonnageRecords = BehaviorRelay<[GetVehicleTonnageRecord]>(value: [])
    let selectedGetVehicleTonnageRecord = BehaviorRelay<GetVehicleTonnageRecord?>(value: nil)
    
    func createOrUpdateAccountDriver(driverPhone: String?,
                                     lisencePlate: String?,
                                     idTonnage: Int?,
                                     driverName: String?,
                                     idCard: String?) {
        
        let parameter = AccountDriverParameter()
        parameter.driverPhoneNumber = driverPhone
        parameter.lisencePlate = lisencePlate
        parameter.idTonnage = idTonnage
        parameter.driverName = driverName
        parameter.idCard = idCard
        parameter.id = getListVehicleRecord?.id
        
        let updateGetListVehicleRecord: () -> () = { [weak self] in
            guard let self = self else { return }
            
            self.getListVehicleRecord?.driverPhoneNumber = driverPhone
            self.getListVehicleRecord?.lisencePlate = lisencePlate
            self.getListVehicleRecord?.tonnage = self.selectedGetVehicleTonnageRecord.value?.maxTonage
            self.getListVehicleRecord?.driverName = driverName
            self.getListVehicleRecord?.idCard = idCard
        }
        
        isShowLoading.accept(true)
        
        let handleResponse: UpdateAccountDriverCompletion = { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                self.errorMessage.accept(error.localizedDescription)
                return
            }
            
            guard let stringResult = data?.stringResult, !stringResult.isEmpty else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            switch stringResult {
            case "error_lisence_plate":
                self.errorMessage.accept("Tài khoản đã tồn tại".localized())
                
            case "error_id_tonnage":
                self.errorMessage.accept("Trọng tải xe không tồn tại".localized())
                
            case "error_driver_phone_number":
                self.errorMessage.accept("Số điện thoại đã tồn tại".localized())
                
            case "error_id_card":
                self.errorMessage.accept("Chứng minh thư đã tồn tại".localized())
                
            case "Update success":
                updateGetListVehicleRecord()
                self.popVC.accept(true)
                
            case "Create success":
                updateGetListVehicleRecord()
                self.popVC.accept(true)
                
            default:
                self.errorMessage.accept("Có lỗi xảy ra".localized())
            }
        }
        
        if getListVehicleRecord != nil {
            VanApi.updateAccountDriver(image: currentImage, parameter: parameter, completion: handleResponse)
        } else {
            VanApi.createAccountDriver(image: currentImage, parameter: parameter, completion: handleResponse)
        }
    }
    
    func checkValidDataAndShowMessageIfNeed(driverName: String?,
                                            driverPhone: String?,
                                            lisencePlate: String?,
                                            idCard: String?) -> Bool {
        
        guard driverName?.isEmpty == false
            && driverPhone?.isEmpty == false
            && lisencePlate?.isEmpty == false
            && idCard?.isEmpty == false
            && selectedGetVehicleTonnageRecord.value != nil else {
                
                errorMessage.accept("Bạn chưa điền thông tin tài xế!".localized())
                
                return false
        }
        
        if let driverPhone = driverPhone, (driverPhone.count != 10 && driverPhone.count != 11) {
            
            errorMessage.accept("Số điện thoại chỉ được nhập từ 10-11 kí tự!".localized())
            return false
        }
        
        if let idCard = idCard, idCard.count != 10 {
            
            errorMessage.accept("Chứng minh thư chỉ được nhập 10 kí tự!".localized())
            return false
        }
        
        return true
    }
    
    func callApiGetVehicleTonnage() {
        isShowLoading.accept(true)
        
        VanApi.getVehicleTonnage { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                return
            }
            
            self.getVehicleTonnageRecords.accept(records)
        }
    }
}
