//
//  HomeSortVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import FittedSheets

enum HomeSortType: String {
    case priceAsc = "1"
    case priceDesc = "2"
    
    case distanceAsc = "3"
    case distanceDesc = "4"
    
    case orderNewest = "5"
    case olderOldest = "6"
    
    func getTitle() -> String {
        switch self {
        case .priceAsc:
            return "Giá tăng dần".localized()
        case .priceDesc:
            return "Giá giảm dần".localized()
            
        case .distanceAsc:
            return "Quãng đường tăng dần".localized()
        case .distanceDesc:
            return "Quãng đường giảm dần".localized()
            
        case .orderNewest:
            return "Đơn mới nhất".localized()
        case .olderOldest:
            return "Đơn cũ nhất".localized()
        }
    }
}

class HomeSortVC: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var priceAscLabel: UILabel!
    @IBOutlet weak var priceDescLabel: UILabel!
    @IBOutlet weak var distanceAscLabel: UILabel!
    @IBOutlet weak var distanceDescLabel: UILabel!
    @IBOutlet weak var orderNewestLabel: UILabel!
    @IBOutlet weak var olderOldestLabel: UILabel!
    
    @IBOutlet weak var priceAscButton: UIButton!
    @IBOutlet weak var priceDescButton: UIButton!
    @IBOutlet weak var distanceAscButton: UIButton!
    @IBOutlet weak var distanceDescButton: UIButton!
    @IBOutlet weak var orderNewestButton: UIButton!
    @IBOutlet weak var orderOldestButton: UIButton!
    
    var type: HomeSortType!
    var completion: ((_ type: HomeSortType) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        setSelectedButton(type: type)
    }
    
    private func setupViews() {
        titleLabel.text = String(format: "%@:", "Chọn cách sắp xếp".localized())
        
        let sortTitle = "Sắp xếp theo".localized()
        priceAscLabel.text = sortTitle + " " + HomeSortType.priceAsc.getTitle().lowercased()
        priceDescLabel.text = sortTitle + " " + HomeSortType.priceDesc.getTitle().lowercased()
        distanceAscLabel.text = sortTitle + " " + HomeSortType.distanceAsc.getTitle().lowercased()
        distanceDescLabel.text = sortTitle + " " + HomeSortType.distanceDesc.getTitle().lowercased()
        orderNewestLabel.text = sortTitle + " " + HomeSortType.orderNewest.getTitle().lowercased()
        olderOldestLabel.text = sortTitle + " " + HomeSortType.olderOldest.getTitle().lowercased()
    }
    
    private func setSelectedButton(type: HomeSortType) {
        self.type = type
        
        priceAscButton.isSelected = false
        priceDescButton.isSelected = false
        distanceAscButton.isSelected = false
        distanceDescButton.isSelected = false
        orderNewestButton.isSelected = false
        orderOldestButton.isSelected = false
        
        switch type {
        case .priceAsc:
            priceAscButton.isSelected = true
        case .priceDesc:
            priceDescButton.isSelected = true
            
        case .distanceAsc:
            distanceAscButton.isSelected = true
        case .distanceDesc:
            distanceDescButton.isSelected = true
            
        case .orderNewest:
            orderNewestButton.isSelected = true
        case .olderOldest:
            orderOldestButton.isSelected = true
        }
    }
    
    func closeSheet(delay: TimeInterval = 0.3) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            self?.sheetViewController?.closeSheet()
        }
    }
    
    @IBAction func priceAscAction(_ sender: Any) {
        setSelectedButton(type: .priceAsc)
        completion?(.priceAsc)
        
        closeSheet()
    }
    
    @IBAction func priceDescAction(_ sender: Any) {
        setSelectedButton(type: .priceDesc)
        completion?(.priceDesc)
        
        closeSheet()
    }
    
    @IBAction func distanceAscAction(_ sender: Any) {
        setSelectedButton(type: .distanceAsc)
        completion?(.distanceAsc)
        
        closeSheet()
    }
    
    @IBAction func distanceDescAction(_ sender: Any) {
        setSelectedButton(type: .distanceDesc)
        completion?(.distanceDesc)
        
        closeSheet()
    }
    
    @IBAction func orderNewestAction(_ sender: Any) {
        setSelectedButton(type: .orderNewest)
        completion?(.orderNewest)
        
        closeSheet()
    }
    
    @IBAction func orderOldestAction(_ sender: Any) {
        setSelectedButton(type: .olderOldest)
        completion?(.olderOldest)
        
        closeSheet()
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        sheetViewController?.closeSheet()
    }
}
