//
//  VanListView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/17/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class VanListView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIButton!
    
    var list: [BiddingVehicle]!
    var editClosure: (() -> ())?
    
    static func show(list: [BiddingVehicle], editClosure: (() -> ())?) {
        let view = VanListView()
        view.show {
            view.list = list
            view.editClosure = editClosure
            
            view.titleLabel.text = "Thông tin xe chở hàng".localized()
            
            if view.editClosure == nil {
                view.editButton.setTitle("Xác nhận".localized(), for: .normal)
            } else {
                view.editButton.setTitle("Chỉnh sửa".localized(), for: .normal)
            }
            
            view.tableView.dataSource = view
            view.tableView.delegate = view
            
            view.tableView.register(cellNibName: ChooseVanTableViewCell.className)
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func editAction(_ sender: Any) {
        editClosure?()
        dismiss()
    }
}

extension VanListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(ChooseVanTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.hideRightView()
        cell.setup(biddingVehicle: list[indexPath.row])
        
        return cell
    }
}
