//
//  ConfirmVanView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/6/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ConfirmVanView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var driverNameTextField: UITextField!
    @IBOutlet weak var driverPhoneNumberTextField: UITextField!
    @IBOutlet weak var lisencePlateTextField: UITextField!
    @IBOutlet weak var idCardTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    private var editClosure: (() -> ())?
    
    static func show(fromHomeType: HomeType,
                     getListBiddingVehicleRecord: BiddingVehicle,
                     editClosure: (() -> ())?) {
        let view = ConfirmVanView()
        view.show {
            view.editClosure = editClosure
            
            if fromHomeType == .wait {
                
                view.updateButton.isHidden  = true
                
            } else if fromHomeType == .success {
                
                view.updateButton.isHidden  = false
            }
            
            view.titleLabel.text = "Thông tin xe".localized()
            view.cancelButton.setTitle("Huỷ".localized(), for: .normal)
            view.editButton.setTitle("Chỉnh sửa".localized(), for: .normal)
            
            view.setup(getListBiddingVehicleRecord: getListBiddingVehicleRecord)
        }
    }
    
    private func setup(getListBiddingVehicleRecord: BiddingVehicle?) {
        guard let getListBiddingVehicleRecord = getListBiddingVehicleRecord else { return }
        
        driverNameTextField.text        = getListBiddingVehicleRecord.driverName ?? getListBiddingVehicleRecord.name
        driverPhoneNumberTextField.text = getListBiddingVehicleRecord.driverPhoneNumber
        lisencePlateTextField.text      = getListBiddingVehicleRecord.lisencePlate
        idCardTextField.text            = getListBiddingVehicleRecord.idCard
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func editAction(_ sender: Any) {
        editClosure?()
        dismiss()
    }
    
    @IBAction func updateAction(_ sender: Any) {
        dismiss()
    }
}
