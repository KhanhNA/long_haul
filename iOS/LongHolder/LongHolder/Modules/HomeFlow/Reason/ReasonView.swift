//
//  ReasonView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/17/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ReasonView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    static func show(title: String, detail: String) {
        let view = ReasonView()
        view.show {
            view.titleLabel.text = title
            view.detailLabel.text = detail
            view.confirmButton.setTitle("Xác nhận".localized(), for: .normal)
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        dismiss()
    }
}
