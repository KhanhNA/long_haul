//
//  GetHistoryBiddingPriceResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetHistoryBiddingPriceRecord: NSObject, Mappable {
    var price: Double?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        price <- map["price"]
    }
}
