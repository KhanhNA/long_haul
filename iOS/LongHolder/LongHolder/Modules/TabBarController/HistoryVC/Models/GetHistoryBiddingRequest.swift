//
//  GetHistoryBiddingRequest.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class GetHistoryBiddingRequest: NSObject {
    var fromDate: Date?
    var toDate: Date?
    var offset: Int?
    var limit: Int?
    var orderBy: HomeSortType?
    var txtSearch: String?
    
    enum CodingKeys: String, CodingKey {
        case fromDate = "from_date"
        case toDate = "to_date"
        case offset = "offset"
        case limit = "limit"
        case orderBy = "order_by"
        case txtSearch = "txt_search"
    }
    
    override init() {
        super.init()
    }
    
    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        let format = "yyyy-MM-dd"
        json[CodingKeys.fromDate.rawValue] = fromDate?.toString(with: format) ?? ""
        json[CodingKeys.toDate.rawValue] = toDate?.toString(with: format) ?? ""
        json[CodingKeys.offset.rawValue] = offset ?? 0
        json[CodingKeys.limit.rawValue] = limit ?? 10
        json[CodingKeys.orderBy.rawValue] = orderBy?.rawValue ?? ""
        json[CodingKeys.txtSearch.rawValue] = txtSearch ?? ""
        
        return json
    }
}
