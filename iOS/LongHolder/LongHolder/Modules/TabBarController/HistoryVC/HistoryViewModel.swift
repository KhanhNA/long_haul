//
//  HistoryViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HistoryViewModel: BaseViewModel {
    // trạng thái sắp xếp
    let sortType = BehaviorRelay<HomeSortType>(value: .priceAsc)
    
    // danh sách bidding trên table view
    let getBiddingInformationRecords = BehaviorRelay<[GetBiddingInformationRecord]>(value: [])
    
    let fromDate = BehaviorRelay<Date?>(value: nil)
    let toDate = BehaviorRelay<Date?>(value: nil)
    
    let textSearch = BehaviorRelay<String?>(value: nil)
    
    var oldTextSearch: String?
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    func addDefaultDateAndCallApi() {
        let now = Date()
        toDate.accept(now)
        fromDate.accept(now.adding(days: -3))
        reloadData()
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        guard let _ = fromDate.value, let _ = toDate.value else { return }
        
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        isShowEmpty.accept(false)
        
        offset = 0
        callApi()
    }
    
    private func getHistoryBiddingRequest(isGetPrice: Bool = false) -> GetHistoryBiddingRequest {
        let parameter = GetHistoryBiddingRequest()
        parameter.fromDate = fromDate.value
        parameter.toDate = toDate.value
        
        if !isGetPrice {
            parameter.offset = offset
            parameter.limit = limit
        }
        
        parameter.orderBy = sortType.value
        
        parameter.txtSearch = textSearch.value
        
        return parameter
    }
    
    func callApi() {
        guard let _ = fromDate.value, let _ = toDate.value else { return }
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        let parameter = getHistoryBiddingRequest()
        
        HistoryApi.getHistoryBidding(parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.getBiddingInformationRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.getBiddingInformationRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.getBiddingInformationRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                let fakeData = GetBiddingInformationRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.getBiddingInformationRecords.accept(oldRecords)
        }
    }
}
