//
//  HistoryVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets

class HistoryVC: BaseVC {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    private let viewModel = HistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initMainCoordinator()
        setupNavigationController()
        
        searchTextField.placeholder = "Tìm kiếm mã đơn hàng, địa chỉ nhận, trả hàng".localized()
        
        setupTableView()
        handleSubscribe()
        
        viewModel.addDefaultDateAndCallApi()
    }
    
    private func setupTableView() {
        tableView.tableHeaderView = headerView
        
        tableView.register(headerNibName: HomeTableViewHeaderFooterView.className)
        tableView.register(cellNibName: HomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    private func handleSubscribe() {
        
        // cập nhật dữ liệu textSearch từ searchTextField
        searchTextField.rx.text.bind(to: viewModel.textSearch)
            .disposed(by: viewModel.disposeBag)
        
        
        searchTextField.rx.controlEvent([.editingDidEnd]).asObservable().subscribe(onNext: { [weak self] (_) in
            guard let self = self,
                self.viewModel.oldTextSearch != self.viewModel.textSearch.value  else { return }
            
            self.viewModel.reloadData(isShowLoading: true)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // cập nhật trạng thái khi thay đổi sort
        viewModel.sortType.subscribe(onNext: { [weak self] (value) in
            guard let self = self else { return }
            
            let sortTitle = "Sắp xếp theo".localized()
            self.sortButton.setTitle(sortTitle + ": " + value.getTitle(), for: .normal)
            
            self.viewModel.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getBiddingInformationRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi thay đổi date
        viewModel.fromDate.subscribe(onNext: { [weak self] (value) in
            
            let format = "dd/MM/yyyy"
            let title = (value == nil ? "Từ ngày".localized() : value?.toString(with: format))
            self?.fromLabel.text = title
            
        }).disposed(by: viewModel.disposeBag)
        
        viewModel.toDate.subscribe(onNext: { [weak self] (value) in
            
            let format = "dd/MM/yyyy"
            let title = (value == nil ? "Từ ngày".localized() : value?.toString(with: format))
            self?.toLabel.text = title
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let controller = HomeSortVC(nibName: HomeSortVC.className, bundle: nil)
        controller.type = viewModel.sortType.value 
        controller.completion = { [weak self] type in
            self?.viewModel.sortType.accept(type)
        }
        
        let sheet = SheetViewController(controller: controller, sizes: [.fixed(375)])
        sheet.handleColor = .clear
        sheet.topCornersRadius = 20
        sheet.extendBackgroundBehindHandle = false
        sheet.overlayColor = AppColor.hex80C4C4C4
        present(sheet, animated: false, completion: nil)
    }
    
    @IBAction func fromAction(_ sender: Any) {
        HistoryDateView.show(minimumDate: nil, maximumDate: viewModel.toDate.value, date: viewModel.fromDate.value, didSelect: { [weak self] (date) in
            self?.viewModel.fromDate.accept(date)
            }, dismissClosure: { [weak self] in
                self?.viewModel.reloadData()
        })
    }
    
    @IBAction func toAction(_ sender: Any) {
        HistoryDateView.show(minimumDate: viewModel.fromDate.value,
                             maximumDate: nil,
                             date: viewModel.toDate.value,
                             didSelect: { [weak self] (date) in
                                self?.viewModel.toDate.accept(date)
            }, dismissClosure: { [weak self] in
                self?.viewModel.reloadData()
        })
    }
}

extension HistoryVC {
    private func setupNavigationController() {
        setupNavigationController(title: "")
        updateTitleAndSubtitle()
    }
    
    private func updateTitleAndSubtitle() {
        // let price = Utils.formatMoney(number: viewModel.price.value)
        let title = "Lịch sử gói thầu".localized()
        // let subtitle = String(format: "(%@: %@ VND)", "Tổng".localized(), price)
        // setupCustomNavigationController(title: title, subtitle: subtitle)
        setupNavigationController(title: title)
    }
}

extension HistoryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getBiddingInformationRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        if indexPath.row == viewModel.getBiddingInformationRecords.value.count - 1 && getBiddingInformationRecord.isLoadMore {
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(HomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getBiddingInformationRecord: getBiddingInformationRecord, homeType: .success)
        
        cell.showReason = {
            ReasonView.show(title: "Lý do hủy gói thầu".localized(),
                            detail: getBiddingInformationRecord.note ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        if getBiddingInformationRecord.homeType == .missing && getBiddingInformationRecord.isTimeout() {
            viewModel.errorMessage.accept("Đã hết hạn".localized())
            return
        }
        
        guard let id = getBiddingInformationRecord.id else { return }
        
        mainCoordinator?.goToDetailBiddingVC(id: id, biddingPackageTimeModel: nil)
    }
}
