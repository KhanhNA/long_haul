//
//  MainCoordinator.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject {
    var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
}

extension MainCoordinator {
    // nếu đi từ new home: bidding_package_id
    // nếu đi từ home: bidding_order_id
    func goToDetailBiddingVC(id: Int, biddingPackageTimeModel: BiddingPackageTimeModel?) {
        let viewController = DetailBiddingVC(nibName: DetailBiddingVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.id = String(id)
        
        // kiểm tra trạng thái của bidding (Đang diễn ra || Sắp diễn ra)
        viewController.biddingPackageTimeModel = biddingPackageTimeModel
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToNewConfirmVanVC(maxConfirmTime: String?,
                             biddingOrderId: String,
                             getListBiddingVehicleRecords: [BiddingVehicle]?,
                             subtitle: String,
                             isBiddingFlow: Bool) {
        let viewController = NewConfirmVanVC(nibName: NewConfirmVanVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.maxConfirmTime = maxConfirmTime
        viewController.biddingOrderId = biddingOrderId
        viewController.getListBiddingVehicleRecords = getListBiddingVehicleRecords
        viewController.subtitle = subtitle
        viewController.isBiddingFlow = isBiddingFlow
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToConfirmVanVC(getListVehicleRecord: GetListVehicleRecord?,
                          reloadTableViewVanVC: (() -> ())?,
                          reloadApiVanVC: (() -> ())?) {
        let viewController = ConfirmVanVC(nibName: ConfirmVanVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.getListVehicleRecord = getListVehicleRecord
        
        viewController.reloadTableViewVanVC = reloadTableViewVanVC
        viewController.reloadApiVanVC = reloadApiVanVC
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MainCoordinator {
    func goToVanDetailVC(getListVehicleRecord: GetListVehicleRecord?) {
        let viewController = VanDetailVC(nibName: VanDetailVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        // thông tin tài xế
        viewController.getListVehicleRecord = getListVehicleRecord
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToVanMapsVC() {
        let viewController = VanMapsVC(nibName: VanMapsVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MainCoordinator {
    func goToHistoryVC() {
        let storyboard = UIStoryboard(name: TabBarController.className, bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: HistoryVC.className) as? HistoryVC else { return }
        
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToSettingsVC() {
        let viewController = SettingsVC(nibName: SettingsVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMyProfileVC(profileRecord: ProfileRecord?) {
        let viewController = MyProfileVC(nibName: MyProfileVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.profileRecord = profileRecord
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToRatingVC(star: String) {
        let viewController = RatingVC(nibName: RatingVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.star = star
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToPreviewVC(images: [String]) {
        let viewController = PreviewVC(nibName: PreviewVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        viewController.images = images
        navigationController?.pushViewController(viewController, animated: true)
    }
}
