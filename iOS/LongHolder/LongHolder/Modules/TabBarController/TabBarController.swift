//
//  TabBarController.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = AppColor.hex00A359
        
        setupData()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let fcmToken = appDelegate.fcmToken {
            updateServerSaveToken(fcmToken: fcmToken) { (_, _) in }
        }
    }
    
    func setupData() {
        UserDefaultsHelper.setIsOnNotification(isOn: true)
        
        let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
        UserDefaultsHelper.setAcceptFirebase(acceptFirebase: (loginModel?.accept_firebase == 1))
    }
    
    func updateServerSaveToken(fcmToken: String,
                               completion: @escaping (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()) {
        BaseClient.shared.requestAPIWithMappable(apiURL.saveToken.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["fcm_token": fcmToken]],
                                                 headers: nil,
                                                 completion: completion)
    }
}
