//
//  ProfileTableViewController.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/18/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class ProfileTableViewController: UITableViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var loyaltyLabel: UILabel!
    @IBOutlet weak var loyaltyDetailLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var logoutLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    
    var mainCoordinator: MainCoordinator?
    var profileRecord: ProfileRecord?
    
    let disposeBag = DisposeBag()
    
    // xử lý api lỗi
    let apiError = BehaviorRelay<Error?>(value: nil)
    let errorMessage = BehaviorRelay<String?>(value: nil)
    let isShowLoading = BehaviorRelay<Bool>(value: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.profileTableVC = self
        
        handleSubscribe()
        
        initMainCoordinator()
        setupNavigationController()
        setupViews()
        setupTableView()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
        callApiIfNeed()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            // profile
            mainCoordinator?.goToMyProfileVC(profileRecord: profileRecord)
            
        case 1:
            // Điểm thưởng
            break
            
        case 2:
            // Lịch sử đấu thầu
            mainCoordinator?.goToHistoryVC()
            
        case 3:
            // Cài đặt
            mainCoordinator?.goToSettingsVC()
            
        case 4:
            // Trung tâm trợ giúp
            ConfirmDialogView.show(title: "Vui lòng goị 19008198 để được trợc giúp!".localized(),
                                   yesTitle: "Gọi".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { }, noAction: { })
            
        case 5:
            // Đăng xuất
            ConfirmDialogView.show(title: "Bạn muốn đăng xuất?".localized(),
                                   yesTitle: "Đăng xuất".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { [weak self] in
                                    
                                    self?.handleLogout()
                                    
                }, noAction: { })
            
        default:
            break
        }
    }
}

extension ProfileTableViewController {
    func removeAll() {
        UserDefaults.standard.save(customObject: ResultResponceLogin(), inKey: "loginModel")
        
        let rootViewController = RootViewController(nibName: RootViewController.className,
                                                    bundle: nil)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = rootViewController
        appDelegate?.window?.makeKeyAndVisible()
    }
    
    func handleLogout() {
        logout { [weak self] (data, error) in
            guard let self = self else { return }
            
            if let error = error {
                print(error.localizedDescription)
                self.showMessage(error.localizedDescription)
                return
            }
            
            SocketIOManager.shared.disconnect()
            self.removeAll()
        }
    }
    
    private func logout(completion: @escaping (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()) {
        BaseClient.shared.requestAPIWithMappable(apiURL.logout.url,
                                                 method: .post,
                                                 parameters: nil,
                                                 headers: nil,
                                                 completion: completion)
    }
}

extension ProfileTableViewController {
    func initMainCoordinator() {
        mainCoordinator = MainCoordinator(navigationController: navigationController)
    }
    
    func setupNavigationController() {
        navigationItem.backBarButtonItem = UIBarButtonItem()
        
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = AppColor.hex151522
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func setupViews() {
        loyaltyLabel.text = "Điểm thưởng".localized()
        loyaltyDetailLabel.text = String(format: "(Còn %@ điểm để trở thành thành viên bạc)".localized(), "xxx")
        historyLabel.text = "Lịch sử đấu thầu".localized()
        settingsLabel.text = "Cài đặt".localized()
        helpLabel.text = "Trung tâm trợ giúp".localized()
        logoutLabel.text = "Đăng xuất".localized()
    }
    
    func setupTableView() {
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
    }
    
    func setupData() {
        if profileRecord?.currentImage != nil {
            profileImageView.image = profileRecord?.currentImage
        } else {
            profileImageView.setImage(urlString: profileRecord?.uriPath,
                                      placeholder: UIImage(named: "confirm_van_vc_avatar"))
        }
        
        nameLabel.text = profileRecord?.name
        companyLabel.text = profileRecord?.companyName
        starLabel.text = profileRecord?.getOrderRateAvgString()
    }
}

extension ProfileTableViewController {
    func showMessage(_ message: String) {
        view.makeToast(message,
                       duration: 1,
                       position: CSToastPositionCenter,
                       style: nil)
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: disposeBag)
        
        
        // xử lý khi gọi api lỗi
        apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.apiError.accept(nil)
            
        }).disposed(by: disposeBag)
        
        
        errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            
            self.errorMessage.accept(nil)
            
        }).disposed(by: disposeBag)
    }
    
    func callApiIfNeed() {
        if profileRecord != nil {
            setupData()
            return
        }
        
        isShowLoading.accept(true)
        
        ProfileApi.getCustomerInformation { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.showMessage(error.localizedDescription)
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.showMessage("Có lỗi xảy ra".localized())
                
                return
            }
            
            self.profileRecord = records.first
            self.setupData()
        }
    }
}
