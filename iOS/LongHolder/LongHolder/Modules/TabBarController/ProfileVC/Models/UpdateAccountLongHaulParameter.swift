//
//  UpdateAccountLongHaulRequest.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/31/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class UpdateAccountLongHaulParameter: NSObject, Mappable {
    var name: String?
    var phone: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        phone <- map["phone"]
    }
}
