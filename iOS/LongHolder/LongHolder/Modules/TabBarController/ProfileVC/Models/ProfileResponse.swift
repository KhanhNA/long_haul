//
//  ProfileResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/31/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class ProfileRecord: NSObject, Mappable {
    var id: Int?
    var name: String?
    var phone: String?
    var city: String?
    var street: String?
    var zip: String?
    var companyId: Int?
    var displayName: String?
    var date: String?
    var title: String?
    var parentId: String?
    var lang: String?
    var tz: String?
    var userId: Int?
    var employee: String?
    var type: String?
    var stateId: String?
    var countryId: String?
    var email: String?
    var mobile: String?
    var color: Int?
    var code: String?
    var staffType: String?
    var cityName: String?
    var storeFname: String?
    var point: Int?
    var companyName: String?
    var uriPath: String?
    var companyNumRate: Double?
    
    var currentImage: UIImage?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        city <- map["city"]
        street <- map["street"]
        zip <- map["zip"]
        companyId <- map["company_id"]
        displayName <- map["display_name"]
        date <- map["date"]
        title <- map["title"]
        parentId <- map["parent_id"]
        lang <- map["lang"]
        tz <- map["tz"]
        userId <- map["user_id"]
        employee <- map["employee"]
        type <- map["type"]
        stateId <- map["state_id"]
        countryId <- map["country_id"]
        email <- map["email"]
        mobile <- map["mobile"]
        color <- map["color"]
        code <- map["code"]
        staffType <- map["staff_type"]
        cityName <- map["city_name"]
        storeFname <- map["store_fname"]
        point <- map["point"]
        companyName <- map["company_name"]
        uriPath <- map["uriPath"]
        companyNumRate <- map["company_num_rate"]
    }
    
    func getOrderRateAvgString() -> String {
        let max = "5"
        let orderRateAvgNumberString = Utils.formatNumber(number: companyNumRate,
                                                          maximumFractionDigits: 2,
                                                          minimumFractionDigits: 0)
        return String(format: "%@/%@", orderRateAvgNumberString, max)
    }
}
