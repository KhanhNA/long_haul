//
//  ProfileApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/31/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ProfileApi: NSObject {
    // Đổi mật khẩu
    typealias ChangePasswordResult = BaseMappableResultModel<BaseMappableModel>
    typealias ChangePasswordResponseCompletion = BaseMappableResponseModel<ChangePasswordResult>
    typealias ChangePasswordCompletion = (ChangePasswordResponseCompletion?, Error?) -> ()
    
    static func changePassword(oldPassword: String,
                               newPassword: String,
                               completion: @escaping ChangePasswordCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.profileChangePassword.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["old_password": oldPassword,
                                                                         "new_password": newPassword]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Cấu hình nhận hoặc không nhận Firebase
    typealias AcceptFirebaseNotificationResult = BaseMappableResultModel<BaseMappableModel>
    typealias AcceptFirebaseNotificationResponseCompletion = BaseMappableResponseModel<AcceptFirebaseNotificationResult>
    typealias AcceptFirebaseNotificationCompletion = (AcceptFirebaseNotificationResponseCompletion?, Error?) -> ()
    
    static func acceptFirebaseNotification(acceptFirebase: String,
                                           completion: @escaping AcceptFirebaseNotificationCompletion) {
        
        /*
         accept_firebase = 1 : có nhận thông báo.
         accept_firebase = 0 : không nhận thông báo.
         */
        
        BaseClient.shared.requestAPIWithMappable(apiURL.acceptFirebaseNotification.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["accept_firebase": acceptFirebase]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Get thông tin employee
    typealias GetCustomerInformationResult = BaseMappableResultModel<ProfileRecord>
    typealias GetCustomerInformationResponseCompletion = BaseMappableResponseModel<GetCustomerInformationResult>
    typealias GetCustomerInformationCompletion = (GetCustomerInformationResponseCompletion?, Error?) -> ()
    
    static func getCustomerInformation(completion: @escaping GetCustomerInformationCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getInforLogin.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": [:]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Update thông tin employee
    typealias UpdateAccountLongHaulResult = BaseMappableResultModel<BaseMappableModel>
    typealias UpdateAccountLongHaulResponseCompletion = BaseMappableResponseModel<UpdateAccountLongHaulResult>
    typealias UpdateAccountLongHaulCompletion = (UpdateAccountLongHaulResponseCompletion?, Error?) -> ()
    
    static func updateAccountLongHaul(image: UIImage?,
                                      parameter: UpdateAccountLongHaulParameter,
                                      completion: @escaping UpdateAccountLongHaulCompletion) {
        
        var images = [UIImage]()
        if let image = image {
            images.append(image)
        }
        
        BaseClient.shared.callPostApiMultipleImageWithMappable(api: apiURL.updateAccountLongHaul.url,
                                                               params: ["longHaulInfo": parameter.toJSONString() ?? ""],
                                                               arr_images: images,
                                                               Name: String(Date().timeIntervalSince1970),
                                                               completion: completion)
    }
    
    // getInformationEmployee
    typealias GetInformationEmployeeResult = BaseMappableResultModel<GetBiddingDetailRecord>
    typealias GetInformationEmployeeResponseCompletion = BaseMappableResponseModel<GetInformationEmployeeResult>
    typealias GetInformationEmployeeCompletion = (GetInformationEmployeeResponseCompletion?, Error?) -> ()
    
    static func getInformationEmployee(offset: Int, limit: Int,
                                       completion: @escaping GetInformationEmployeeCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getInformationEmployee.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["offset": offset, "limit": limit]],
                                                 headers: nil,
                                                 completion: completion)
    }
}
