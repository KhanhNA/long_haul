//
//  BiddingCargoResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/7/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class BiddingCargoRecord: NSObject, Mappable {
    var id: Int?
    var cargoNumber: String?
    var status: String?
    var biddingOrderID: Int?
    
    private var _confirmTime: String?
    var confirmTime: Date?
    
    var timeCountdown: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        cargoNumber <- map["cargo_number"]
        status <- map["status"]
        biddingOrderID <- map["bidding_order_id"]
        
        _confirmTime <- map["confirm_time"]
        // convert về date (+ múi giờ)
        if let _confirmTime = _confirmTime {
            confirmTime = Date.UTCToLocal(date: _confirmTime)?.toDate()
        }
        
        timeCountdown <- map["time_countdown"]
    }
}
