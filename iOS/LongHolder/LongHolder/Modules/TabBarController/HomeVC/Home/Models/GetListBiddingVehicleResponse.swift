////
////  GetListBiddingVehicleResponse.swift
////  LongHolder
////
////  Created by Hieu Dinh on 8/5/20.
////  Copyright © 2020 dong luong. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//// MARK: - Record
//class GetListBiddingVehicleRecord: NSObject, Mappable {
//    var id: Int?
//    var code: String?
//    var name: String?
//    var idCard: String?
//    var lisencePlate: String?
//    var driverPhoneNumber: String?
//    var tonnage: Int?
//    var image128: String?
//    
//    var isSelected = false
//    var isShowing = true // check item có đang show trên table view hay không
//    
//    override init() {
//        super.init()
//    }
//    
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        id <- map["id"]
//        code <- map["code"]
//        name <- map["name"]
//        idCard <- map["id_card"]
//        lisencePlate <- map["lisence_plate"]
//        driverPhoneNumber <- map["driver_phone_number"]
//        tonnage <- map["tonnage"]
//        image128 <- map["image_128"]
//    }
//}
