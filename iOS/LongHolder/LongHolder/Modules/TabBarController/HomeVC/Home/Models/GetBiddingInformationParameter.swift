//
//  GetBiddingInformationParameter.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class GetBiddingInformationParameter: NSObject {
    var type: HomeType?
    var offset: Int?
    var limit: Int?
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case offset = "offset"
        case limit = "limit"
    }

    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        json[CodingKeys.type.rawValue] = type?.rawValue ?? ""
        json[CodingKeys.offset.rawValue] = offset ?? 0
        json[CodingKeys.limit.rawValue] = limit ?? 10
        
        return json
    }
}
