//
//  GetBiddingDetailResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/7/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

// MARK: - Record
class GetBiddingDetailRecord: NSObject, Mappable {
    var id: Int?
    var biddingPackageNumber: String?
    var status: String?
    var confirmTime: String?
    var releaseTime: String?
    var biddingTime: String?
    var maxCount: Int?
    var totalWeight: Int?
    var distance: Double?
    var fromLatitude: Double?
    var fromLongitude: Double?
    var toLatitude: Double?
    var toLongitude: Double?
    var fromReceiveTime: String?
    var toReceiveTime: String?
    var fromReturnTime: String?
    var toReturnTime: String?
    var priceOrigin: Int?
    var price: Int?
    var createDate: String?
    var writeDate: String?
    var countdownTime: Int?
    var priceTimeChange: String?
    var priceLevelChange: Int?
    var biddingOrder: BiddingOrder?
    
    var cargoTypes: [CargoType]?
    
    var isLoadMore = false
    
    var isShowAll = false
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        biddingPackageNumber <- map["bidding_package_number"]
        status <- map["status"]
        confirmTime <- map["confirm_time"]
        releaseTime <- map["release_time"]
        biddingTime <- map["bidding_time"]
        maxCount <- map["max_count"]
        totalWeight <- map["total_weight"]
        distance <- map["distance"]
        fromLatitude <- map["from_latitude"]
        fromLongitude <- map["from_longitude"]
        toLatitude <- map["to_latitude"]
        toLongitude <- map["to_longitude"]
        fromReceiveTime <- map["from_receive_time"]
        toReceiveTime <- map["to_receive_time"]
        fromReturnTime <- map["from_return_time"]
        toReturnTime <- map["to_return_time"]
        priceOrigin <- map["price_origin"]
        price <- map["price"]
        createDate <- map["create_date"]
        writeDate <- map["write_date"]
        countdownTime <- map["countdown_time"]
        priceTimeChange <- map["price_time_change"]
        priceLevelChange <- map["price_level_change"]
        biddingOrder <- map["bidding_order"]
        
        cargoTypes <- map["cargo_types"]
    }
}

// MARK: - BiddingOrder
class BiddingOrder: NSObject, Mappable {
    var id: Int?
    var companyID: Int?
    var biddingOrderNumber: String?
    var fromDepot: Depot?
    var toDepot: Depot?
    var totalWeight: Int?
    var totalCargo: Int?
    var price: Double?
    var distance: Double?
    
    var type: String?
    
    var homeType: HomeType? {
        if let type = type {
            if type == "1" || type == "-1" {
                return .success
            }
            return HomeType(rawValue: type)
        }
        return nil
    }
    
    var isCancel: Bool {
        return type == "-1"
    }
    
    var status: String?
    var note: String?
    var biddingOrderReceiveID: String?
    var biddingOrderReturnID: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    var writeDate: String?
    var biddingPackageID: Int?
    
    private var _fromReceiveTime: String?
    private var _toReceiveTime: String?
    private var _fromReturnTime: String?
    private var _toReturnTime: String?
    var fromReceiveTime: Date?
    var toReceiveTime: Date?
    var fromReturnTime: Date?
    var toReturnTime: Date?
    
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    
    var biddingVehicles: [BiddingVehicle]?
    var orderRateAvg: Double?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        companyID <- map["company_id"]
        biddingOrderNumber <- map["bidding_order_number"]
        fromDepot <- map["from_depot"]
        toDepot <- map["to_depot"]
        totalWeight <- map["total_weight"]
        totalCargo <- map["total_cargo"]
        price <- map["price"]
        distance <- map["distance"]
        type <- map["type"]
        status <- map["status"]
        note <- map["note"]
        biddingOrderReceiveID <- map["bidding_order_receive_id"]
        biddingOrderReturnID <- map["bidding_order_return_id"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        writeDate <- map["write_date"]
        biddingPackageID <- map["bidding_package_id"]
        
        _fromReceiveTime <- map["from_receive_time"]
        _toReceiveTime <- map["to_receive_time"]
        _fromReturnTime <- map["from_return_time"]
        _toReturnTime <- map["to_return_time"]
        
        // convert về date (+ múi giờ)
        if let _fromReceiveTime = _fromReceiveTime {
            fromReceiveTime = Date.UTCToLocal(date: _fromReceiveTime)?.toDate()
        }
        if let _toReceiveTime = _toReceiveTime {
            toReceiveTime = Date.UTCToLocal(date: _toReceiveTime)?.toDate()
        }
        if let _fromReturnTime = _fromReturnTime {
            fromReturnTime = Date.UTCToLocal(date: _fromReturnTime)?.toDate()
        }
        if let _toReturnTime = _toReturnTime {
            toReturnTime = Date.UTCToLocal(date: _toReturnTime)?.toDate()
        }
        
        maxConfirmTime <- map["max_confirm_time"]
        biddingVehicles <- map["bidding_vehicles"]
        
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        orderRateAvg <- map["order_rate_avg"]
    }
    
    func getFromReceiveTimeToReceiveTime() -> String {
        let fromReceiveTimeString = fromReceiveTime?.toString(with: Global.localFormat)
        let toReceiveTimeString = toReceiveTime?.toString(with: Global.localFormat)
        
        if fromReceiveTimeString == nil || fromReceiveTimeString?.isEmpty == true {
            return toReceiveTimeString ?? ""
        }
        
        if toReceiveTimeString == nil || toReceiveTimeString?.isEmpty == true {
            return fromReceiveTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReceiveTimeString ?? "",
                      toReceiveTimeString ?? "")
    }
    
    func getFromReturnTimeToReturnTime() -> String {
        let fromReturnTimeString = fromReturnTime?.toString(with: Global.localFormat)
        let toReturnTimeString = toReturnTime?.toString(with: Global.localFormat)
        
        if fromReturnTimeString == nil || fromReturnTimeString?.isEmpty == true {
            return toReturnTimeString ?? ""
        }
        
        if toReturnTimeString == nil || toReturnTimeString?.isEmpty == true {
            return fromReturnTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReturnTimeString ?? "",
                      toReturnTimeString ?? "")
    }
    
    func getPrice(currency: String = " VND") -> String {
        return String(format: "%@%@", Utils.formatMoney(number: price ?? 0), currency)
    }
    
    func getDistance() -> String {
        return String(format: "%@ km", Utils.formatNumber(number: distance ?? 0))
    }
    
    func getId() -> String {
        return String(format: "ID: %@", biddingOrderNumber ?? "")
    }
    
    func getSubtitleForNewConfirmVanVC() -> String {
        return String(format: "(%@ - %@)", getId(), getDistance())
    }
    
    func getTotalCargo() -> String {
        return String(format: "%@ cargo", String(totalCargo ?? 0))
    }
    
    func getTotalWeight() -> String {
        return String(format: "%@ kg", String(totalWeight ?? 0))
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        
        return String(format: "%@ %@", "Đấu thầu".localized(), createDateString ?? "")
    }
    
    func getOrderRateAvgString() -> String {
        let max = "5"
        let orderRateAvgNumberString = Utils.formatNumber(number: orderRateAvg,
                                                          maximumFractionDigits: 2,
                                                          minimumFractionDigits: 0)
        return String(format: "%@/%@", orderRateAvgNumberString, max)
    }
}

// MARK: - SizeCargo
class SizeCargo: NSObject, Mappable {
    var length: Int?
    var width: Int?
    var height: Int?
    var longUnit: String?
    var weightUnit: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        longUnit <- map["long_unit"]
        weightUnit <- map["weight_unit"]
    }
}
