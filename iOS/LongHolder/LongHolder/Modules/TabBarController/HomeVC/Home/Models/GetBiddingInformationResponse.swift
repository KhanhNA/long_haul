//
//  GetBiddingInformationResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetBiddingInformationRecord: NSObject, Mappable {
    var id: Int?
    var companyID: Int?
    var biddingOrderNumber: String?
    var fromDepot: Depot?
    var toDepot: Depot?
    var totalWeight: Int?
    var totalCargo: Int?
    var price: Double?
    var distance: Double?
    var type: String?
    var status: String?
    var note: String?
    var biddingOrderReceiveID: String?
    var biddingOrderReturnID: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    var writeDate: String?
    var biddingPackageID: Int?
    
    private var _fromReceiveTime: String?
    private var _toReceiveTime: String?
    private var _fromReturnTime: String?
    private var _toReturnTime: String?
    var fromReceiveTime: Date?
    var toReceiveTime: Date?
    var fromReturnTime: Date?
    var toReturnTime: Date?
    
    var biddingVehicles: [BiddingVehicle]?
    
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    
    var cargoTypes: [CargoType]?
    
    var isLoadMore = false
    
    var isCancel: Bool {
        return type == "-1"
    }
    
    var homeType: HomeType? {
        if let type = type {
            if type == "1" || type == "-1" {
                return .success
            }
            return HomeType(rawValue: type)
        }
        return nil
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        companyID <- map["company_id"]
        biddingOrderNumber <- map["bidding_order_number"]
        fromDepot <- map["from_depot"]
        toDepot <- map["to_depot"]
        totalWeight <- map["total_weight"]
        totalCargo <- map["total_cargo"]
        price <- map["price"]
        distance <- map["distance"]
        type <- map["type"]
        status <- map["status"]
        note <- map["note"]
        biddingOrderReceiveID <- map["bidding_order_receive_id"]
        biddingOrderReturnID <- map["bidding_order_return_id"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        writeDate <- map["write_date"]
        biddingPackageID <- map["bidding_package_id"]
        
        _fromReceiveTime <- map["from_receive_time"]
        _toReceiveTime <- map["to_receive_time"]
        _fromReturnTime <- map["from_return_time"]
        _toReturnTime <- map["to_return_time"]
        
        // convert về date (+ múi giờ)
        if let _fromReceiveTime = _fromReceiveTime {
            fromReceiveTime = Date.UTCToLocal(date: _fromReceiveTime)?.toDate()
        }
        if let _toReceiveTime = _toReceiveTime {
            toReceiveTime = Date.UTCToLocal(date: _toReceiveTime)?.toDate()
        }
        if let _fromReturnTime = _fromReturnTime {
            fromReturnTime = Date.UTCToLocal(date: _fromReturnTime)?.toDate()
        }
        if let _toReturnTime = _toReturnTime {
            toReturnTime = Date.UTCToLocal(date: _toReturnTime)?.toDate()
        }
        
        biddingVehicles <- map["bidding_vehicles"]
        
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        cargoTypes <- map["cargo_types"]
    }
    
    func getFromReceiveTimeToReceiveTime() -> String {
        let fromReceiveTimeString = fromReceiveTime?.toString(with: Global.localFormat)
        let toReceiveTimeString = toReceiveTime?.toString(with: Global.localFormat)
        
        if fromReceiveTimeString == nil || fromReceiveTimeString?.isEmpty == true {
            return toReceiveTimeString ?? ""
        }
        
        if toReceiveTimeString == nil || toReceiveTimeString?.isEmpty == true {
            return fromReceiveTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReceiveTimeString ?? "",
                      toReceiveTimeString ?? "")
    }
    
    func getFromReturnTimeToReturnTime() -> String {
        let fromReturnTimeString = fromReturnTime?.toString(with: Global.localFormat)
        let toReturnTimeString = toReturnTime?.toString(with: Global.localFormat)
        
        if fromReturnTimeString == nil || fromReturnTimeString?.isEmpty == true {
            return toReturnTimeString ?? ""
        }
        
        if toReturnTimeString == nil || toReturnTimeString?.isEmpty == true {
            return fromReturnTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReturnTimeString ?? "",
                      toReturnTimeString ?? "")
    }
    
    func getPrice(currency: String = " VND") -> String {
        return String(format: "%@%@", Utils.formatMoney(number: price ?? 0), currency)
    }
    
    func getDistance() -> String {
        return String(format: "%@ km", Utils.formatNumber(number: distance ?? 0))
    }
    
    func getId() -> String {
        return String(format: "ID: %@", biddingOrderNumber ?? "")
    }
    
    func getSubtitleForNewConfirmVanVC() -> String {
        return String(format: "(%@ - %@)", getId(), getDistance())
    }
    
    func getTotalCargo() -> String {
        return String(format: "%@ cargo", String(totalCargo ?? 0))
    }
    
    func getTotalWeight() -> String {
        return String(format: "%@ kg", String(totalWeight ?? 0))
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        return createDateString ?? ""
    }
    
    func getHMS() -> (hours: Int, minutes: Int, seconds: Int) {
        guard let endDate = maxConfirmTime else {
            return (0, 0, 0)
        }
        
        let startDate = Date()
        
        let differenceInSeconds = Int(endDate.timeIntervalSince(startDate))
        return Utils.secondsToHoursMinutesSeconds(seconds: differenceInSeconds)
    }
    
    func isTimeout() -> Bool {
        let (hours, minutes, seconds) = getHMS()
        return hours <= 0 && minutes <= 0 && seconds <= 0
    }
}

// MARK: - Depot
class Depot: NSObject, Mappable {
    var id: Int?
    var name: String?
    var depotCode: String?
    var address: String?
    var street: String?
    var street2: String?
    var cityName: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        depotCode <- map["depot_code"]
        address <- map["address"]
        street <- map["street"]
        street2 <- map["street2"]
        cityName <- map["city_name"]
    }
}

// MARK: - Price
class Price: NSObject, Mappable {
    var id: Int?
    var cargoID: Int?
    var price: Double?
    var status: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        cargoID <- map["cargo_id"]
        price <- map["price"]
        status <- map["status"]
    }
}

// MARK: - ProductType
class ProductType: NSObject, Mappable {
    var id: Int?
    var nameSeq: String?
    var netWeight: Int?
    var name: String?
    var productTypeDescription: String?
    var status: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nameSeq <- map["name_seq"]
        netWeight <- map["net_weight"]
        name <- map["name"]
        productTypeDescription <- map["description"]
        status <- map["status"]
    }
}

// MARK: - SizeStandard
class SizeStandard: NSObject, Mappable {
    var id: Int?
    var length: Int?
    var width: Int?
    var height: Int?
    var longUnit: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        longUnit <- map["long_unit"]
    }
}

// MARK: - BiddingVehicle
class BiddingVehicle: NSObject, Mappable {
    var id: Int?
    var weightUnit: Int?
    var lisencePlate: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    var resUserID: Int?
    var companyID: Int?
    var idCard: String?
    var code: String?
    var driverPhoneNumber: String?
    var driverName: String?
    var writeDate: String?
    var resPartnerID: Int?
    var tonnage: Int?
    var expiryTime: String?
    var biddingVehicleSeq: String?
    var status: String?
    var actionLog: [ActionLog]?
    var cargoTypes: [CargoType]?
    
    var name: String?
    var image128: String?
    
    var isSelected = false
    var isShowing = true // check item có đang show trên table view hay không
    
    var rating: Rating?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        weightUnit <- map["weight_unit"]
        lisencePlate <- map["lisence_plate"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        resUserID <- map["res_user_id"]
        companyID <- map["company_id"]
        idCard <- map["id_card"]
        code <- map["code"]
        driverPhoneNumber <- map["driver_phone_number"]
        driverName <- map["driver_name"]
        writeDate <- map["write_date"]
        resPartnerID <- map["res_partner_id"]
        tonnage <- map["tonnage"]
        expiryTime <- map["expiry_time"]
        biddingVehicleSeq <- map["bidding_vehicle_seq"]
        status <- map["status"]
        actionLog <- map["action_log"]
        cargoTypes <- map["cargo_types"]
        
        name <- map["name"]
        image128 <- map["image128"]
        
        if let driverName = driverName, driverName.isEmpty {
            name = driverName
        } else if let name = name, name.isEmpty {
            driverName = name
        }
        
        rating <- map["rating"]
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        return createDateString ?? ""
    }
}

// MARK: - ActionLog
class ActionLog: NSObject, Mappable {
    var latitude: Double?
    var longitude: Double?
    var vanID: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        vanID <- map["van_id"]
    }
}


class Rating: NSObject, Mappable {
    var id: Int?
    var driverId: Int?
    var numRating: Int?
    var employeeId: Int?
    var note: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    var biddingVehicleId: Int?
    var biddingOrderId: Int?
    var ratingBadges: [RatingBadge]?
    var images: [String]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        driverId <- map["driver_id"]
        numRating <- map["num_rating"]
        employeeId <- map["employee_id"]
        note <- map["note"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        biddingVehicleId <- map["bidding_vehicle_id"]
        biddingOrderId <- map["bidding_order_id"]
        ratingBadges <- map["rating_badges"]
        images <- map["images"]
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        return createDateString ?? ""
    }
}

class RatingBadge: NSObject, Mappable {
    var id: Int?
    var name: String?
    var codeSeq: String?
    var code: String?
    var ratingBadgeDescription: String?
    var status: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        codeSeq <- map["code_seq"]
        code <- map["code"]
        ratingBadgeDescription <- map["description"]
        status <- map["status"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        return createDateString ?? ""
    }
}
