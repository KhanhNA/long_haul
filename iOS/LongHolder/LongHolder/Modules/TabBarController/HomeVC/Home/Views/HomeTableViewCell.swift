//
//  HomeTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var createDateLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var totalWeightLabel: UILabel!
    
    @IBOutlet weak var fromDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReceiveTimeToReceiveTimeLabel: UILabel!
    @IBOutlet weak var toDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReturnTimeToReturnTimeLabel: UILabel!
    
    @IBOutlet weak var maxDateLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var inputVanButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var getBiddingInformationRecord: GetBiddingInformationRecord!
    private var cargoTypes: [CargoType]?
    private var timer: Timer?
    
    var inputVan: ((_ getBiddingInformationRecord: GetBiddingInformationRecord) -> ())?
    var showReason: (() -> ())?
    var homeType: HomeType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inputVanButton.setTitle("Điền thông tin lái xe".localized(), for: .normal)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: VanCollectionViewCell.className)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 100, height: 20)
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(getBiddingInformationRecord: GetBiddingInformationRecord, homeType: HomeType) {
        self.getBiddingInformationRecord = getBiddingInformationRecord
        self.homeType = homeType
        
        cargoTypes = []
        if let biddingVehicles = getBiddingInformationRecord.biddingVehicles {
            for biddingVehicle in biddingVehicles {
                cargoTypes?.append(contentsOf: biddingVehicle.cargoTypes ?? [])
            }
        }
        collectionView.isHidden = true
        
        let price: NSMutableAttributedString = NSMutableAttributedString(string: getBiddingInformationRecord.getPrice())
        
        if homeType == .missing {
            
            createDateLabel.text = "Đấu thầu".localized() + " " + getBiddingInformationRecord.getCreateDate()
            
            inputVanButton.setTitle("Điền thông tin lái xe".localized(), for: .normal)
            inputVanButton.superview?.isHidden = false
            inputVanButton.isHidden = false
            
            maxDateLabel.isHidden = false
            
            timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(getCountdownTime),
                                         userInfo: nil,
                                         repeats: true)
            timer?.fire()
            
        } else if homeType == .wait {
            
            createDateLabel.text = "Đấu thầu".localized() + " " + getBiddingInformationRecord.getCreateDate()
            
            inputVanButton.superview?.isHidden = true
            maxDateLabel.isHidden = true
            
        } else {
            
            if getBiddingInformationRecord.isCancel {
                // đã huỷ
                
                let createDate = NSMutableAttributedString()
                createDate.append(getBiddingInformationRecord
                    .getCreateDate()
                    .attributedString
                    .color(AppColor.hex919395))
                
                createDate.append(" - ".attributedString.color(AppColor.hex919395))
                createDate.append("Đã huỷ".localized().attributedString.color(AppColor.hexFF0C20))
                
                createDateLabel.attributedText = createDate
                
                inputVanButton.setTitle("Lý do bị hủy".localized(), for: .normal)
                inputVanButton.superview?.isHidden = false
                inputVanButton.isHidden = false
                
                maxDateLabel.isHidden = true
                
                price.addStrikethrough()
                
            } else {
                
                if getBiddingInformationRecord.status == "0" {
                    // Chưa vận chuyển
                    createDateLabel.text = String(format: "%@ %@ - %@",
                                                  "Đấu thầu".localized(),
                                                  getBiddingInformationRecord.getCreateDate(),
                                                  "Chưa vận chuyển".localized())
                    
                } else if getBiddingInformationRecord.status == "1" {
                    // Đang vận chuyển
                    createDateLabel.text = String(format: "%@ %@ - %@",
                                                  "Đấu thầu".localized(),
                                                  getBiddingInformationRecord.getCreateDate(),
                                                  "Đang vận chuyển".localized())
                    
                } else if getBiddingInformationRecord.status == "2" {
                    // Đã trả hàng
                    createDateLabel.text = String(format: "%@ %@ - %@",
                                                  "Đấu thầu".localized(),
                                                  getBiddingInformationRecord.getCreateDate(),
                                                  "Đã trả hàng".localized())
                }
                
                collectionView.isHidden = false
                collectionView.reloadData()
                
                inputVanButton.superview?.isHidden = (cargoTypes == nil || cargoTypes?.count == 0)
                inputVanButton.isHidden = true
                
                maxDateLabel.isHidden = true
            }
        }
        
        priceLabel.attributedText = price.color(priceLabel.textColor)
        
        distanceLabel.text = getBiddingInformationRecord.getDistance()
        quantityLabel.text = getBiddingInformationRecord.getTotalCargo()
        totalWeightLabel.text = getBiddingInformationRecord.getTotalWeight()
        
        fromDepotAddressLabel.text = getBiddingInformationRecord.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getBiddingInformationRecord.getFromReceiveTimeToReceiveTime()
        
        toDepotAddressLabel.text = getBiddingInformationRecord.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getBiddingInformationRecord.getFromReturnTimeToReturnTime()
        
        idLabel.text = getBiddingInformationRecord.getId()
    }
    
    @objc private func getCountdownTime() {
        let (hours, minutes, seconds) = getBiddingInformationRecord.getHMS()
        
        if hours <= 0 && minutes <= 0 && seconds <= 0 {
            
            maxDateLabel.text = "Đã hết hạn".localized()
            timer?.invalidate()
            
        } else {
            
            let time = Utils.getHoursMinutesSeconds(hours: hours, minutes: minutes, seconds: seconds)
            maxDateLabel.text = String(format: "%@ %@", "Còn".localized(), time)
        }
    }
    
    @IBAction func inputVanAction(_ sender: Any) {
        if homeType == .missing {
            
            inputVan?(getBiddingInformationRecord)
            
        } else {
            
            if getBiddingInformationRecord.isCancel {
                showReason?()
            }
        }
    }
}

extension HomeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cargoTypes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(VanCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }
        
        if let cargoTypes = cargoTypes {
            let item = cargoTypes[indexPath.item]
            cell.idLabel.text = String(format: "%@ %@",
                                       String(item.cargoQuantity ?? 0),
                                       String(item.type ?? ""))
        } else {
            cell.idLabel.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
