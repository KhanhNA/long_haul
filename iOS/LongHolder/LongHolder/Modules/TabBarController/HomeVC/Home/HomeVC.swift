//
//  HomeVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets

enum HomeType: String {
    case missing    = "0"
    case wait       = "2"
    case success    = "3" // (gồm case = "-1" (bidding thất bại) hoặc "1" (bidding thành công))
    
    func getTitle() -> String {
        switch self {
        case .missing:
            return "Thiếu thông tin".localized()
        case .wait:
            return "Chờ xác nhận".localized()
        case .success:
            return "Đã xác nhận".localized()
        }
    }
}

class HomeVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = HomeViewModel()
    
    // thứ tự các màn hình danh sách bidding
    // 0: mới nhất
    // 1: chờ thông tin
    // 2: chờ xác nhận
    // 3: bid thành công
    var homeType = HomeType.missing
    
    // check gọi api duy nhất 1 lần ở viewDidAppear
    // tránh bị lag khi scroll đổi page
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.homeType = homeType
        
        setupTableView()
        handleSubscribe()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad {
            isFirstLoad = false
            viewModel.callApi()
        }
    }
    
    private func setupTableView() {
        tableView.register(headerNibName: HomeTableViewHeaderFooterView.className)
        tableView.register(cellNibName: HomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getBiddingInformationRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getBiddingInformationRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        if indexPath.row == viewModel.getBiddingInformationRecords.value.count - 1 && getBiddingInformationRecord.isLoadMore {
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(HomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getBiddingInformationRecord: getBiddingInformationRecord, homeType: homeType)
        
        cell.inputVan = { [weak self] getBiddingInformationRecord in
            guard let self = self, let id = getBiddingInformationRecord.id else { return }
            
            if self.homeType == .missing && getBiddingInformationRecord.isTimeout() {
                self.viewModel.errorMessage.accept("Đã hết hạn".localized())
                return
            }
            
            if let maxConfirmTime = getBiddingInformationRecord.maxConfirmTime?.toString() {
                // Điền thông tin xe
                self.mainCoordinator?
                    .goToNewConfirmVanVC(maxConfirmTime: maxConfirmTime,
                                         biddingOrderId: String(id),
                                         getListBiddingVehicleRecords: nil,
                                         subtitle: getBiddingInformationRecord
                                            .getSubtitleForNewConfirmVanVC(),
                                         isBiddingFlow: false)
            }
            
        }
        
        cell.showReason = {
            ReasonView.show(title: "Lý do hủy gói thầu".localized(),
                            detail: getBiddingInformationRecord.note ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        if homeType == .missing && getBiddingInformationRecord.isTimeout() {
            viewModel.errorMessage.accept("Đã hết hạn".localized())
            return
        }
        
        guard let id = getBiddingInformationRecord.id else { return }
        
        mainCoordinator?.goToDetailBiddingVC(id: id, biddingPackageTimeModel: nil)
    }
}
