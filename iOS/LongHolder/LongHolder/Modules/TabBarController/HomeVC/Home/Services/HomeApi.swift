//
//  HomeApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

// Danh sách bidding
typealias GetBiddingInformationResult = BaseMappableResultModel<GetBiddingInformationRecord>
typealias GetBiddingInformationResponseCompletion = BaseMappableResponseModel<GetBiddingInformationResult>
typealias GetBiddingInformationCompletion = (GetBiddingInformationResponseCompletion?, Error?) -> ()

class HomeApi {
    
    static func getBiddingOrderBidded(parameter: GetBiddingInformationParameter,
                                      completion: @escaping GetBiddingInformationCompletion) {
        /*
         # order by = 1 : Sắp xếp theo giá tăng giần
         # order by = 2 : Sắp xếp theo giá giảm giần
         # order by = 3 : Sắp xếp theo quãng đường tăng dần
         # order by = 4 : Sắp xếp theo quãng đường giảm dần
         # order by = 5 : Sắp xếp đơn mới nhất
         # order by = 6 : Sắp xếp đơn cũ nhất
         */
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingOrderBidded.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": parameter.getJson()],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Danh sách xe add bidding
    typealias GetListBiddingVehicleResult = BaseMappableResultModel<BiddingVehicle>
    typealias GetListBiddingVehicleResponseCompletion = BaseMappableResponseModel<GetListBiddingVehicleResult>
    typealias GetListBiddingVehicleCompletion = (GetListBiddingVehicleResponseCompletion?, Error?) -> ()
    
    static func getListBiddingVehicle(uID: Int, completion: @escaping GetListBiddingVehicleCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getListBiddingVehicle.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["uID": uID]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Bidding đơn
    typealias BiddingCargoResult = BaseMappableResultModel<BiddingCargoRecord>
    typealias BiddingCargoResponseCompletion = BaseMappableResponseModel<BiddingCargoResult>
    typealias BiddingCargoCompletion = (BiddingCargoResponseCompletion?, Error?) -> ()
    
    static func biddingCargo(uID: Int,
                             cargoId: Int,
                             completion: @escaping BiddingCargoCompletion) {
        let parameters: [String : Any] = ["jsonrpc": "2.0",
                                          "params": ["user_id": uID,
                                                     "cargo_id": cargoId]]
        BaseClient.shared.requestAPIWithMappable(apiURL.biddingCargo.url,
                                                 method: .post,
                                                 parameters: parameters,
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Xác nhận bidding
    typealias ConfirmBiddingVehicleCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()
    
    static func confirmBiddingVehicle(biddingOrderId: Int,
                                      biddingVehicleId: Int,
                                      completion: @escaping ConfirmBiddingVehicleCompletion) {
        let parameters: [String : Any] = ["jsonrpc": "2.0",
                                          "params": ["bidding_order_id": biddingOrderId,
                                                     "bidding_vehicle_id": biddingVehicleId]]
        BaseClient.shared.requestAPIWithMappable(apiURL.confirmBiddingVehicle.url,
                                                 method: .post,
                                                 parameters: parameters,
                                                 headers: nil,
                                                 completion: completion)
    }
}
