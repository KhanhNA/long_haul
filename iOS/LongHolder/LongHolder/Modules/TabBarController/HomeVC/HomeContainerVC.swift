//
//  HomeContainerVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import CarbonKit

class HomeContainerVC: BaseVC {
    private var controllerArray: [HomeVC] = []
    
    let homeVC2 = HomeVC(nibName: HomeVC.className, bundle: nil) // Thiếu thông tin
    private let homeVC3 = HomeVC(nibName: HomeVC.className, bundle: nil) // Chờ xác nhận
    private let homeVC4 = HomeVC(nibName: HomeVC.className, bundle: nil) // Đã xác nhận
    
    private var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        setupNavigationController()
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.homeContainerVC = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupPageMenu()
    }
    
    // xoá tất cả dữ liệu
    // và gọi lại api
    func reloadData() {
        controllerArray.forEach({ $0.viewModel.reloadData() })
    }
}

extension HomeContainerVC {
    private func setupNavigationController() {
        setupNavigationController(title: "Danh sách gói đã đấu thầu".localized())
    }
}

extension HomeContainerVC {
    private func setupPageMenu() {
        guard isFirstLoad else { return }
        isFirstLoad = false
        
        setupControllerArray()
        
        let items = [
            HomeType.missing.getTitle(),    // Thiếu thông tin
            HomeType.wait.getTitle(),       // Chờ xác nhận
            HomeType.success.getTitle()     // Đã xác nhận
        ]
        
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.toolbar.barTintColor = .white
        carbonTabSwipeNavigation.toolbar.clipsToBounds = true
        carbonTabSwipeNavigation.currentTabIndex = 0
        
        carbonTabSwipeNavigation.setIndicatorColor(AppColor.hex00A359)
        carbonTabSwipeNavigation.setSelectedColor(AppColor.hex00A359, font: .boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setNormalColor(AppColor.hex3A3E41, font: .boldSystemFont(ofSize: 14))
        
        carbonTabSwipeNavigation.setTabExtraWidth(0)
        carbonTabSwipeNavigation.setTabBarHeight(40)
        
        for index in 0..<items.count {
            carbonTabSwipeNavigation.carbonSegmentedControl?
                .setWidth(view.frame.width / CGFloat(items.count),
                          forSegmentAt: index)
        }
    }
    
    private func setupControllerArray() {
        homeVC2.mainCoordinator = mainCoordinator
        homeVC3.mainCoordinator = mainCoordinator
        homeVC4.mainCoordinator = mainCoordinator
        
        homeVC2.homeType = .missing
        homeVC3.homeType = .wait
        homeVC4.homeType = .success
        
        controllerArray.append(contentsOf: [
            homeVC2,
            homeVC3,
            homeVC4
        ])
    }
}

extension HomeContainerVC: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation,
                                  viewControllerAt index: UInt) -> UIViewController {
        return controllerArray[Int(index)]
    }
}
