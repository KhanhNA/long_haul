//
//  VanVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class VanVC: BaseVC {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var filterButton: UIButton!
    
    let viewModel = VanViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        setupNavigationController()
        
        setupViews()
        setupTableView()
        handleSubscribe()
        
        viewModel.reloadData()
    }
    
    private func setupViews() {
        searchTextField.placeholder = "Tìm kiếm biển số xe, tên tài xế, số điện thoại xe".localized()
    }
    
    private func setupNavigationController() {
        setupNavigationController(title: "Quản lý xe".localized())
        let image = UIImage(named: "new_confirm_van_vc_add")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(handleAdd))
    }
    
    @objc private func handleAdd() {
        mainCoordinator?.goToConfirmVanVC(getListVehicleRecord: nil, reloadTableViewVanVC: { [weak self] in
            
            self?.tableView.reloadData()
            
        }, reloadApiVanVC: { [weak self] in
            
            self?.viewModel.reloadData(isShowLoading: true)
            
        })
    }
    
    private func setupTableView() {
        tableView.tableHeaderView = headerView
        
        tableView.register(cellNibName: VanTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    private func handleSubscribe() {
        
        // cập nhật dữ liệu textSearch từ searchTextField
        searchTextField.rx.text.bind(to: viewModel.textSearch)
            .disposed(by: viewModel.disposeBag)
        
        
        searchTextField.rx.controlEvent([.editingDidEnd]).asObservable().subscribe(onNext: { [weak self] (_) in
            guard let self = self,
                self.viewModel.oldTextSearch != self.viewModel.textSearch.value  else { return }
            
            self.viewModel.reloadData(isShowLoading: true)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // cập nhật trạng thái khi thay đổi filter
        viewModel.type.subscribe(onNext: { [weak self] (value) in
            guard let self = self else { return }
            
            self.filterButton.setTitle(value.getTitle(), for: .normal)
            self.viewModel.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getListVehicleRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    func update(getListVehicleRecord: GetListVehicleRecord) {
        viewModel.update(getListVehicleRecord: getListVehicleRecord)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        VanFilterView.show(type: viewModel.type.value, completion: { [weak self] type in
            self?.viewModel.type.accept(type)
        })
    }
}

extension VanVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getListVehicleRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getListVehicleRecord = viewModel.getListVehicleRecords.value[indexPath.row]
        
        if indexPath.row == viewModel.getListVehicleRecords.value.count - 1 && getListVehicleRecord.isLoadMore {
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(VanTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getListVehicleRecord: getListVehicleRecord)
        cell.gotToMap = { [weak self] in
            guard let self = self else { return }
            self.mainCoordinator?.goToVanMapsVC()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getListVehicleRecord = viewModel.getListVehicleRecords.value[indexPath.row]
        
        let title = "BKS".localized() + " " + (getListVehicleRecord.lisencePlate ?? "")
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        if getListVehicleRecord.isBidding {
            
            let goToDetailAlertAction = UIAlertAction(title: "Xem thông tin đơn hàng".localized(), style: .default) { [weak self] (_) in
                guard let self = self else { return }
                self.mainCoordinator?.goToVanDetailVC(getListVehicleRecord: getListVehicleRecord)
            }
            
            alertController.addAction(goToDetailAlertAction)
        }
        
        let editAlertAction = UIAlertAction(title: "Sửa thông tin xe".localized(), style: .default) { [weak self] (_) in
            guard let self = self else { return }
            
            self.mainCoordinator?.goToConfirmVanVC(getListVehicleRecord: getListVehicleRecord, reloadTableViewVanVC: {

                tableView.reloadData()
                
            }, reloadApiVanVC: { [weak self] in
                     
                self?.viewModel.reloadData(isShowLoading: true)
                
            })
        }
        
        
        let deleteAlertAction = UIAlertAction(title: "Xóa xe".localized(), style: .default) { [weak self] (_) in
            guard let self = self else { return }
            
            let confirmAction: () -> () = { [weak self] in
                self?.viewModel.callApiDeleteAccountDriver(vehicleId: getListVehicleRecord.id)
            }
            let message = String(format: "%@ %@", "Bạn có muốn xóa tài khoản xe BKS".localized(),
                                 getListVehicleRecord.lisencePlate ?? "")
            BaseAlertController.show(message: message,
                                     confirmTitle: "Đồng ý".localized(),
                                     confirmAction: confirmAction,
                                     cancelTitle: "Không".localized(),
                                     cancelAction: { },
                                     from: self)
        }
        
        
        let cancelAlertAction = UIAlertAction(title: "Đóng".localized(), style: .cancel) { (_) in }
        
        alertController.addAction(editAlertAction)
        alertController.addAction(deleteAlertAction)
        alertController.addAction(cancelAlertAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
