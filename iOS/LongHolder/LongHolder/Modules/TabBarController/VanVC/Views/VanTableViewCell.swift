//
//  VanCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class VanTableViewCell: UITableViewCell {
    @IBOutlet weak var lisencePlateLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverPhoneNumberLabel: UILabel!
    @IBOutlet weak var idCardLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var gotToMap: (() -> ())?
    var getListVehicleRecord: GetListVehicleRecord!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: VanCollectionViewCell.className)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 100, height: 20)
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(getListVehicleRecord: GetListVehicleRecord) {
        self.getListVehicleRecord = getListVehicleRecord
        
        if getListVehicleRecord.isBidding {
            collectionView.isHidden = false
            collectionView.reloadData()
        } else {
            collectionView.isHidden = true
        }
        
        let lisencePlate = String(format: "%@: %@", "BKS".localized(),
                                  getListVehicleRecord.lisencePlate ?? "")
        var tonnage = ""
        if let value = getListVehicleRecord.tonnage {
            tonnage = String(value) + " " + "tấn".localized()
        }
        
        let lisencePlateAndTonnage = NSMutableAttributedString()
        lisencePlateAndTonnage.append(lisencePlate.attributedString)
        
        if !tonnage.isEmpty {
            lisencePlateAndTonnage.append(" | ".attributedString)
            lisencePlateAndTonnage.append(tonnage.attributedString.color(AppColor.hex919395))
        }
        
        lisencePlateLabel.attributedText = lisencePlateAndTonnage
        
        driverNameLabel.text = String(format: "%@: %@", "Tài xế".localized(),
                                      getListVehicleRecord.driverName ?? "")
        
        driverPhoneNumberLabel.text = String(format: "%@: %@", "Số điện thoại".localized(),
                                             getListVehicleRecord.driverPhoneNumber ?? "")
        
        idCardLabel.text = String(format: "%@: %@", "CMT".localized(),
                                  getListVehicleRecord.idCard ?? "")
        
        avatarImageView.setImage(urlString: getListVehicleRecord.image128,
                                 placeholder: UIImage(named: "home_vc_avatar"))
    }
    
    @IBAction func mapAction(_ sender: Any) {
        gotToMap?()
    }
}

extension VanTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getListVehicleRecord.listBiddingOrder?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(VanCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }
        
        if let listBiddingOrder = getListVehicleRecord.listBiddingOrder {
            let item = listBiddingOrder[indexPath.item]
            cell.idLabel.text = String(format: "ID: %@", item.biddingOrderNumber ?? "")
        } else {
            cell.idLabel.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
