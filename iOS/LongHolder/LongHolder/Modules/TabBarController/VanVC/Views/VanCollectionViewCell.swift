//
//  VanCollectionViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class VanCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var idLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        idLabel.superview?.cornerRadius = 8
    }
}
