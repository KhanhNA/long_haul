//
//  VanApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

// update_account_driver
typealias UpdateAccountDriverCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()

class VanApi: NSObject {
    // Danh sách xe quản lý
    typealias GetListVehicleResult = BaseMappableResultModel<GetListVehicleRecord>
    typealias GetListVehicleResponseCompletion = BaseMappableResponseModel<GetListVehicleResult>
    typealias GetListVehicleCompletion = (GetListVehicleResponseCompletion?, Error?) -> ()
    
    static func getListVehicle(parameter: GetListVehicleRequest,
                               completion: @escaping GetListVehicleCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.listManagerBiddingVehicle.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": parameter.getJson()],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Xóa thông tin xe
    typealias DeleteAccountDriverCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()
    
    static func deleteAccountDriver(vehicleId: Int, completion: @escaping DeleteAccountDriverCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.deleteAccountDriver.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["vehicle_id": vehicleId]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    static func updateAccountDriver(image: UIImage?,
                                    parameter: AccountDriverParameter,
                                    completion: @escaping UpdateAccountDriverCompletion) {
        var images = [UIImage]()
        if let image = image {
            images.append(image)
        }
        
        BaseClient.shared.callPostApiMultipleImageWithMappable(api: apiURL.updateAccountDriver.url,
                                                               params: ["driverInfo": parameter.toJSONString() ?? ""],
                                                               arr_images: images,
                                                               Name: String(Date().timeIntervalSince1970),
                                                               completion: completion)
    }
    
    static func createAccountDriver(image: UIImage?,
                                    parameter: AccountDriverParameter,
                                    completion: @escaping UpdateAccountDriverCompletion) {
        var images = [UIImage]()
        if let image = image {
            images.append(image)
        }
        
        BaseClient.shared.callPostApiMultipleImageWithMappable(api: apiURL.createAccountDriver.url,
                                                               params: ["driverInfo": parameter.toJSONString() ?? ""],
                                                               arr_images: images,
                                                               Name: String(Date().timeIntervalSince1970),
                                                               completion: completion)
    }
    
    // Danh sách trọng tải xe
    typealias GetVehicleTonnageResult = BaseMappableResultModel<GetVehicleTonnageRecord>
    typealias GetVehicleTonnageResponseCompletion = BaseMappableResponseModel<GetVehicleTonnageResult>
    typealias GetVehicleTonnageCompletion = (GetVehicleTonnageResponseCompletion?, Error?) -> ()
    
    static func getVehicleTonnage(completion: @escaping GetVehicleTonnageCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getVehicleTonnage.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": [:]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Chi tiết đơn hàng lái xe
    static func getListBiddingOrderByBiddingVehicleId(biddingVehicleId: Int,
                                                      offset: Int,
                                                      limit: Int,
                                                      completion: @escaping GetBiddingInformationCompletion) {
        
        let params = ["bidding_vehicle_id": biddingVehicleId,
                      "offset": offset,
                      "limit": limit]
        
        BaseClient.shared.requestAPIWithMappable(apiURL.getListBiddingOrderByBiddingVehicleId.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": params],
                                                 headers: nil,
                                                 completion: completion)
    }
}
