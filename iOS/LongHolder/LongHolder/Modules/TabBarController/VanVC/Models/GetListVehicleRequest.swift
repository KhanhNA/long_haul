//
//  GetListVehicleParameter.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class GetListVehicleRequest: NSObject {
    var textSearch: String?
    var offset: Int?
    var limit: Int?
    var status: VanFilterType?
    
    enum CodingKeys: String, CodingKey {
        case textSearch = "txt_search"
        case offset = "offset"
        case limit = "limit"
        case status = "status"
    }
    
    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        json[CodingKeys.textSearch.rawValue] = textSearch ?? ""
        json[CodingKeys.offset.rawValue] = offset ?? 0
        json[CodingKeys.limit.rawValue] = limit ?? 10
        json[CodingKeys.status.rawValue] = status?.rawValue ?? 0
        
        return json
    }
}
