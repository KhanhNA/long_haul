//
//  GetVehicleTonnageResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/17/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetVehicleTonnageRecord: NSObject, Mappable {
    var id: Int?
    var code: String?
    var name: String?
    var maxTonage: Int?
    var status: String?
    var typeUnit: Bool?
    var recordDescription: Bool?
    var displayName: String?
    var createUid: [String]?
    var createDate: String?
    var writeUid: [String]?
    var writeDate: String?
    var lastUpdate: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        maxTonage <- map["max_tonage"]
        status <- map["status"]
        typeUnit <- map["type_unit"]
        recordDescription <- map["description"]
        displayName <- map["display_name"]
        createUid <- map["create_uid"]
        createDate <- map["create_date"]
        writeUid <- map["write_uid"]
        writeDate <- map["write_date"]
        lastUpdate <- map["__last_update"]
    }
}
