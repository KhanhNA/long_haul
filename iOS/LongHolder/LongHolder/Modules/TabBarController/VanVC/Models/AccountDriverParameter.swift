//
//  AccountDriverParameter.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/18/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class AccountDriverParameter: NSObject, Mappable {
    var driverPhoneNumber: String?
    var lisencePlate: String?
    var idTonnage: Int?
    var driverName: String?
    var idCard: String?
    var id: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        driverPhoneNumber <- map["driver_phone_number"]
        lisencePlate <- map["lisence_plate"]
        idTonnage <- map["id_tonnage"]
        driverName <- map["driver_name"]
        idCard <- map["id_card"]
        id <- map["id_vehicle"]
    }
}
