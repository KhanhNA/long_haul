//
//  GetListVehicleResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetListVehicleRecord: NSObject, Mappable {
    var listBiddingOrder: [BiddingOrder]?
    var driverPhoneNumber: String?
    var idCard: String?
    var id: Int?
    var code: String?
    var longitude: Double?
    var image128: String?
    var latitude: Double?
    var tonnage: Int?
    var lisencePlate: String?
    var driverName: String?
    
    var isLoadMore = false
    var isBidding: Bool {
        return listBiddingOrder != nil
            && listBiddingOrder?.count != nil
            && listBiddingOrder?.count != 0
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        listBiddingOrder <- map["list_bidding_order"]
        driverPhoneNumber <- map["driver_phone_number"]
        idCard <- map["id_card"]
        id <- map["id"]
        code <- map["code"]
        longitude <- map["longitude"]
        image128 <- map["image_128"]
        latitude <- map["latitude"]
        tonnage <- map["tonnage"]
        lisencePlate <- map["lisence_plate"]
        driverName <- map["driver_name"]
    }
    
    func update(from item: GetListVehicleRecord) {
        listBiddingOrder = item.listBiddingOrder
        driverPhoneNumber = item.driverPhoneNumber
        idCard = item.idCard
        id = item.id
        code = item.code
        longitude = item.longitude
        image128 = item.image128
        latitude = item.latitude
        tonnage = item.tonnage
        lisencePlate = item.lisencePlate
        driverName = item.driverName
    }
}
