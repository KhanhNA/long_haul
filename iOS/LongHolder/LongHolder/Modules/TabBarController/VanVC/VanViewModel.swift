//
//  VanViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VanViewModel: BaseViewModel {
    let type = BehaviorRelay<VanFilterType>(value: .all)
    
    // danh sách tài xế
    let getListVehicleRecords = BehaviorRelay<[GetListVehicleRecord]>(value: [])
    
    let textSearch = BehaviorRelay<String?>(value: nil)
    
    var oldTextSearch: String?
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        isShowEmpty.accept(false)
        
        offset = 0
        callApi()
    }
    
    func callApi() {
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        let parameter = GetListVehicleRequest()
        parameter.textSearch = textSearch.value ?? ""
        parameter.offset = offset
        parameter.limit = limit
        parameter.status = type.value
        
        VanApi.getListVehicle(parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.oldTextSearch = parameter.textSearch
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.getListVehicleRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.getListVehicleRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.getListVehicleRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                let fakeData = GetListVehicleRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.getListVehicleRecords.accept(oldRecords)
        }
    }
    
    func callApiDeleteAccountDriver(vehicleId: Int?) {
        guard let vehicleId = vehicleId else { return }
        
        isShowLoading.accept(true)
        
        VanApi.deleteAccountDriver(vehicleId: vehicleId) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            if data?.boolResult == true {
                
                var oldRecords = self.getListVehicleRecords.value
                oldRecords.removeAll(where: { $0.id == vehicleId })
                self.getListVehicleRecords.accept(oldRecords)
                
            } else {
                
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                
            }
        }
    }
    
    func update(getListVehicleRecord: GetListVehicleRecord) {
        let getListVehicleRecords = self.getListVehicleRecords.value
        getListVehicleRecords.first(where: { $0.id == getListVehicleRecord.id })?
            .update(from: getListVehicleRecord)
    }
}
