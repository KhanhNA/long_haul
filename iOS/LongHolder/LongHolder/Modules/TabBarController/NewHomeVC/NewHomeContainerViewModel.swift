//
//  NewHomeContainerViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ObjectMapper

class NewHomeContainerViewModel: BaseViewModel {
    // danh sách view controllers trong page
    let controllerArray = BehaviorRelay<[NewHomeVC]>(value: [])
    
    // index của view controller trong page / controllerArray
    let currentTabIndex = BehaviorRelay<Int>(value: 0)
    
    // Danh sách thời gian đấu thầu
    // Status (đang/sắp đấu thầu)
    let biddingPackageTimeModels = BehaviorRelay<[BiddingPackageTimeModel]>(value: [])
    
    func callApiGetBiddingPackageTime(completion: @escaping () -> () = { }) {
        isShowLoading.accept(true)
        
        NewHomeApi.getBiddingPackageTime { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result,
                let records = result.records?
                    .compactMap({ $0.toDate() ?? Date() })
                    .sorted(by: { $0 < $1 }) else {
                        return
            }
            
            
            var biddingPackageTimeModels = [BiddingPackageTimeModel]()
            
            let dateFormat = "yyyy-MM-dd"
            
            let now = Date()
            
            for (index, _date) in records.enumerated() {
                
                // convert về date (+ múi giờ)
                guard let date = Date.UTCToLocal(date: _date.toString())?.toDate() else {
                    continue
                }
                
                let item = BiddingPackageTimeModel()
                item.date = date
                item.dateString = date.toString()
                
                let checkTypeUpcomingOrTomorrow: (_ item: BiddingPackageTimeModel) -> () = { item in
                    
                    if date.toString(with: dateFormat) == now.toString(with: dateFormat) {
                        item.type = .upcoming
                    } else {
                        item.type = .tomorrow
                    }
                    
                }
                
                if records.count == 1 {
                    
                    // server trả về list chỉ có 1 item
                    // nếu thời gian hiện tại lớn hơn item đó
                    // -> Đang diễn ra
                    
                    if now > date {
                        item.type = .happenning
                    } else {
                        checkTypeUpcomingOrTomorrow(item)
                    }
                    
                } else if records.count >= 2 && index == 0 {
                    
                    // server trả về list chỉ có nhiều hơn 2 items
                    // nếu thời gian hiện tại nằm trong khoảng thời gian của item đầu tiên và thứ hai
                    // -> Đang diễn ra
                    
                    let first = date
                    let _second = records[1]
                    
                    // convert về date (+ múi giờ)
                    guard let second = Date.UTCToLocal(date: _second.toString())?.toDate() else {
                        continue
                    }
                    
                    if first < now && now < second {
                        item.type = .happenning
                    } else {
                        checkTypeUpcomingOrTomorrow(item)
                    }
                    
                } else {
                    
                    checkTypeUpcomingOrTomorrow(item)
                }
                
                biddingPackageTimeModels.append(item)
            }
            
            self.biddingPackageTimeModels.accept(biddingPackageTimeModels)
            completion()
        }
    }
}

extension NewHomeContainerViewModel {
    // xoá tất cả dữ liệu thời gian bidding time
    // gọi lại api bidding time
    // xoá tất cả dữ liệu controller array
    // gọi lại api
    func reloadData() {
        callApiGetBiddingPackageTime {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                self?.controllerArray.value.forEach({
                    if $0.biddingPackageTimeModel != nil
                        && $0.biddingPackageTimeModel.date != nil {
                        $0.viewModel.reloadData()
                    }
                })
            }
        }
    }
}

extension NewHomeContainerViewModel {
    typealias BidmessageRecordResult = BaseMappableResultModel<BidmessageRecord>
    typealias BidmessageResponse = BaseMappableResponseModel<BidmessageRecordResult>
    
    func handleBidmessage(json: [String: Any]?) {
        guard let json = json,
            let response = Mapper<BidmessageResponse>().map(JSON: json),
            let records = response.result?.records else {
                return
        }
        
        print("==================================================")
        if let jsonData = try? JSONSerialization.data(withJSONObject: response.toJSON(),
                                                      options: JSONSerialization.WritingOptions.prettyPrinted) {
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
            print("Response ->", jsonString ?? "")
        } else {
            print("Response ->", response.toJSON())
        }
        
        for record in records {
            if let actionType = record.actionType {
                
                switch actionType {
                case .biddingTime:
                    // Reload lại màn hình new home vc
                    handleBiddingTime()
                    
                case .changePriceAction:
                    // Nếu status = 0 => update data của object tương ứng (new home vc)
                    // Nếu status = -1 => remove object đó khỏi danh sách đấu thầu (new home vc)
                    handleChangePriceAction(record: record)
                    
                case .overtimeBiddingAction:
                    // thêm object vào danh sách Đang đấu thầu (new home vc)
                    // xoá object (nếu có) ở danh sách Đã đấu thầu (home vc)
                    handleOvertimeBiddingAction(record: record)
                    
                case .biddingPackage:
                    // xoá object (nếu có) ở danh sách Đang đấu thầu (new home vc)
                    // reload các tab ở danh sách Đã đấu thầu (home vc)
                    handleBiddingPackage(record: record)
                }
            }
        }
    }
    
    func handleBiddingTime() {
        // Reload lại màn hình new home vc
        reloadData()
    }
    
    func handleChangePriceAction(record: BidmessageRecord) {
        // Nếu status = 0 => update data của object tương ứng (new home vc)
        // Nếu status = -1 => remove object đó khỏi danh sách đấu thầu (new home vc)
        
        for item in record.lstBiddingPackages ?? [] {
            for viewController in controllerArray.value
                where viewController.biddingPackageTimeModel.type == .happenning {
                    
                    let viewModel = viewController.viewModel
                    var viewModelRecords = viewModel.getBiddingPackageInformationRecords.value
                    
                    if item.status == "0" {
                        
                        viewModelRecords.first(where: { $0.id == item.id })?.update(from: item)
                        
                    } else if item.status == "-1" {
                        
                        viewModelRecords.removeAll(where: { $0.id == item.id })
                        
                    }
                    
                    viewModel.getBiddingPackageInformationRecords.accept(viewModelRecords)
            }
        }
    }
    
    func handleOvertimeBiddingAction(record: BidmessageRecord) {
        // thêm object vào danh sách Đang đấu thầu (new home vc)
        // xoá object (nếu có) ở danh sách Đã đấu thầu (home vc)
        
        for item in record.lstBiddingPackages ?? [] {
            for viewController in controllerArray.value
                where viewController.biddingPackageTimeModel.type == .happenning {
                    
                    let viewModel = viewController.viewModel
                    var viewModelRecords = viewModel.getBiddingPackageInformationRecords.value
                    
                    viewModelRecords.insert(item, at: 0)
                    viewModel.getBiddingPackageInformationRecords.accept(viewModelRecords)
            }
            
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            if let homeContainerVC = appDelegate?.homeContainerVC {
                
                let viewModel = homeContainerVC.homeVC2.viewModel
                var viewModelRecords = viewModel.getBiddingInformationRecords.value
                
                if let id = item.id,
                    viewModelRecords.compactMap({ $0.biddingPackageID }).contains(id) {
                    
                    viewModelRecords.removeAll(where: { $0.biddingPackageID == item.id })
                    viewModel.getBiddingInformationRecords.accept(viewModelRecords)
                }
            }
        }
    }
    
    func handleBiddingPackage(record: BidmessageRecord) {
        // xoá object (nếu có) ở danh sách Đang đấu thầu (new home vc)
        // reload các tab ở danh sách Đã đấu thầu (home vc)
        
        for item in record.lstBiddingPackages ?? [] {
            for viewController in controllerArray.value
                where viewController.biddingPackageTimeModel.type == .happenning {
                    
                    let viewModel = viewController.viewModel
                    var viewModelRecords = viewModel.getBiddingPackageInformationRecords.value
                    
                    viewModelRecords.removeAll(where: { $0.id == item.id })
                    viewModel.getBiddingPackageInformationRecords.accept(viewModelRecords)
            }
        }
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.homeContainerVC?.reloadData()
    }

    func handleSubscribeSocket() {
        SocketIOManager.shared.bidmessage = { [weak self] (data, ack) in
            self?.handleBidmessage(json: data.first as? [String: Any])
        }
        
        SocketIOManager.shared.addHandlers()
        SocketIOManager.shared.connect()
        
        handleOnOffBidmessage()
    }
    
    func handleOnOffBidmessage() {
        if UserDefaultsHelper.getIsOnNotification() {
            SocketIOManager.shared.setOnBidmessage()
        } else {
            SocketIOManager.shared.setOffBidmessage()
        }
    }
}
