//
//  BidmessageResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/25/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

enum ActionType: String {
    case biddingTime            = "biddingTime"
    case changePriceAction      = "changePriceAction"
    case overtimeBiddingAction  = "overtimeBiddingAction"
    case biddingPackage         = "biddingPackage"
}

class BidmessageRecord: NSObject, Mappable {
    var actionType: ActionType?
    var lstBiddingPackages: [GetBiddingPackageInformationRecord]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        actionType <- map["actionType"]
        lstBiddingPackages <- map["lstBiddingPackages"]
    }
}
