//
//  GetBiddingPackageDetailResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetBiddingPackageDetailRecord: NSObject, Mappable {
    var id: Int?
    var biddingOrderID: Int?
    var biddingPackageNumber: String?
    var status: String?
    var confirmTime: String?
    var releaseTime: String?
    var biddingTime: String?
    var maxCount: Int?
    var fromDepot: Depot?
    var toDepot: Depot?
    var totalWeight: Int?
    var distance: Double?
    var fromLatitude: Double?
    var fromLongitude: Double?
    var toLatitude: Double?
    var toLongitude: Double?
    
    private var _fromReceiveTime: String?
    private var _toReceiveTime: String?
    private var _fromReturnTime: String?
    private var _toReturnTime: String?
    var fromReceiveTime: Date?
    var toReceiveTime: Date?
    var fromReturnTime: Date?
    var toReturnTime: Date?
    
    var priceOrigin: Double?
    var price: Double?
    
    private var _createDate: String?
    var createDate: Date?
    
    var writeDate: String?
    var countdownTime: Int?
    var priceTimeChange: String?
    var priceLevelChange: Int?
    var totalCargo: Int?
    // var cargos: [Cargo]?
    var cargoTypes: [CargoType]?
    var biddingVehicles: [BiddingVehicle]?
    
    var type: String?
    
    var homeType: HomeType? {
        if let type = type {
            if type == "1" || type == "-1" {
                return .success
            }
            return HomeType(rawValue: type)
        }
        return nil
    }
    
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    var note: String?
    
    var isCancel: Bool {
        return type == "-1"
    }
    
    var isLoadMore = false
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        biddingOrderID <- map["bidding_order_id"]
        biddingPackageNumber <- map["bidding_package_number"]
        status <- map["status"]
        confirmTime <- map["confirm_time"]
        releaseTime <- map["release_time"]
        biddingTime <- map["bidding_time"]
        maxCount <- map["max_count"]
        fromDepot <- map["from_depot"]
        toDepot <- map["to_depot"]
        totalWeight <- map["total_weight"]
        distance <- map["distance"]
        fromLatitude <- map["from_latitude"]
        fromLongitude <- map["from_longitude"]
        toLatitude <- map["to_latitude"]
        toLongitude <- map["to_longitude"]
        
        _fromReceiveTime <- map["from_receive_time"]
        _toReceiveTime <- map["to_receive_time"]
        _fromReturnTime <- map["from_return_time"]
        _toReturnTime <- map["to_return_time"]
        
        // convert về date (+ múi giờ)
        if let _fromReceiveTime = _fromReceiveTime {
            fromReceiveTime = Date.UTCToLocal(date: _fromReceiveTime)?.toDate()
        }
        if let _toReceiveTime = _toReceiveTime {
            toReceiveTime = Date.UTCToLocal(date: _toReceiveTime)?.toDate()
        }
        if let _fromReturnTime = _fromReturnTime {
            fromReturnTime = Date.UTCToLocal(date: _fromReturnTime)?.toDate()
        }
        if let _toReturnTime = _toReturnTime {
            toReturnTime = Date.UTCToLocal(date: _toReturnTime)?.toDate()
        }
        
        priceOrigin <- map["price_origin"]
        price <- map["price"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        writeDate <- map["write_date"]
        countdownTime <- map["countdown_time"]
        priceTimeChange <- map["price_time_change"]
        priceLevelChange <- map["price_level_change"]
        totalCargo <- map["total_cargo"]
        // cargos <- map["cargos"]
        cargoTypes <- map["cargo_types"]
        biddingVehicles <- map["bidding_vehicles"]
        
        type <- map["type"]
        
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        note <- map["note"]
    }
    
    func getFromReceiveTimeToReceiveTime() -> String {
        let fromReceiveTimeString = fromReceiveTime?.toString(with: Global.localFormat)
        let toReceiveTimeString = toReceiveTime?.toString(with: Global.localFormat)
        
        if fromReceiveTimeString == nil || fromReceiveTimeString?.isEmpty == true {
            return toReceiveTimeString ?? ""
        }
        
        if toReceiveTimeString == nil || toReceiveTimeString?.isEmpty == true {
            return fromReceiveTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReceiveTimeString ?? "",
                      toReceiveTimeString ?? "")
    }
    
    func getFromReturnTimeToReturnTime() -> String {
        let fromReturnTimeString = fromReturnTime?.toString(with: Global.localFormat)
        let toReturnTimeString = toReturnTime?.toString(with: Global.localFormat)
        
        if fromReturnTimeString == nil || fromReturnTimeString?.isEmpty == true {
            return toReturnTimeString ?? ""
        }
        
        if toReturnTimeString == nil || toReturnTimeString?.isEmpty == true {
            return fromReturnTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReturnTimeString ?? "",
                      toReturnTimeString ?? "")
    }
    
    func getPrice(currency: String = " VND") -> String {
        return String(format: "%@%@", Utils.formatMoney(number: price ?? 0), currency)
    }
    
    func getDistance() -> String {
        return String(format: "%@ km", Utils.formatNumber(number: distance ?? 0))
    }
    
    func getId() -> String {
        return String(format: "ID: %@", biddingPackageNumber ?? "")
    }
    
    func getSubtitleForNewConfirmVanVC() -> String {
        return String(format: "(%@ - %@)", getId(), getDistance())
    }
    
    func getTotalCargo() -> String {
        return String(format: "%@ cargo", String(totalCargo ?? 0))
    }
    
    func getTotalWeight() -> String {
        return String(format: "%@ kg", String(totalWeight ?? 0))
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        
        return String(format: "%@ %@", "Đấu thầu".localized(), createDateString ?? "")
    }
}

/*
class Cargo: NSObject, Mappable {
    var id: Int?
    var cargoNumber: String?
    var fromDepotID: Int?
    var toDepotID: Int?
    var distance: Int?
    var sizeID: Int?
    var weight: Int?
    var cargoDescription: String?
    var price: Double?
    var fromLatitude: Double?
    var toLatitude: Double?
    var biddingPackageID: Int?
    var fromLongitude: Double?
    var toLongitude: Double?
    var sizeStandard: SizeStandard?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        cargoNumber <- map["cargo_number"]
        fromDepotID <- map["from_depot_id"]
        toDepotID <- map["to_depot_id"]
        distance <- map["distance"]
        sizeID <- map["size_id"]
        weight <- map["weight"]
        cargoDescription <- map["description"]
        price <- map["price"]
        fromLatitude <- map["from_latitude"]
        toLatitude <- map["to_latitude"]
        biddingPackageID <- map["bidding_package_id"]
        fromLongitude <- map["from_longitude"]
        toLongitude <- map["to_longitude"]
        sizeStandard <- map["size_standard"]
    }
    
    func getWeight() -> String {
        return String(format: "%@ %@", String(weight ?? 0), "tấn".localized())
    }
    
    func getLHW() -> String {
        return String(format: "%@ x %@ x %@ m",
                      String(sizeStandard?.length ?? 0),
                      String(sizeStandard?.height ?? 0),
                      String(sizeStandard?.width ?? 0))
    }
}
*/

class CargoType: NSObject, Mappable {
    var id: Int?
    var length: Int?
    var width: Int?
    var height: Int?
    var longUnitMoved0: String?
    var weightUnitMoved0: String?
    var type: String?
    var fromWeight: String?
    var toWeight: String?
    var priceID: String?
    var price: String?
    var sizeStandardSeq: String?
    var longUnit: String?
    var weightUnit: String?
    var cargoQuantity: Int?
    var totalWeight: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        longUnitMoved0 <- map["long_unit_moved0"]
        weightUnitMoved0 <- map["weight_unit_moved0"]
        type <- map["type"]
        fromWeight <- map["from_weight"]
        toWeight <- map["to_weight"]
        priceID <- map["price_id"]
        price <- map["price"]
        sizeStandardSeq <- map["size_standard_seq"]
        longUnit <- map["long_unit"]
        weightUnit <- map["weight_unit"]
        cargoQuantity <- map["cargo_quantity"]
        totalWeight <- map["total_weight"]
    }
    
    func getCargoQuantity() -> String {
        return String(format: "%@ %@", String(cargoQuantity ?? 0), "cargo".localized())
    }
    
    func getWeight() -> String {
        return String(format: "%@ %@", String(totalWeight ?? 0), "tấn".localized())
    }
    
    func getLHW() -> String {
        return String(format: "%@ x %@ x %@ m",
                      String(length ?? 0),
                      String(height ?? 0),
                      String(width ?? 0))
    }
}
