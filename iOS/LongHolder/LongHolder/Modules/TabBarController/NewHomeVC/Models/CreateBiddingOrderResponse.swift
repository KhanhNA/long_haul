//
//  CreateBiddingOrderResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class CreateBiddingOrderRecord: NSObject, Mappable {
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    
    var biddingOrderID: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        biddingOrderID <- map["bidding_order_id"]
    }
}
