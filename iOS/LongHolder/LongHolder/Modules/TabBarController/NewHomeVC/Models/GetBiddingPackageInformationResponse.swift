//
//  GetBiddingPackageInformationResponse.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetBiddingPackageInformationRecord: NSObject, Mappable {
    var id: Int?
    var biddingOrderID: Int?
    var biddingPackageNumber: String?
    var status: String?
    
    var confirmTime: String?
    var releaseTime: String?
    var biddingTime: String?
    
    var maxCount: Int?
    var fromDepot: Depot?
    var toDepot: Depot?
    var totalWeight: Int?
    var distance: Double?
    var fromLatitude: Double?
    var fromLongitude: Double?
    var toLatitude: Double?
    var toLongitude: Double?
    
    private var _fromReceiveTime: String?
    private var _toReceiveTime: String?
    private var _fromReturnTime: String?
    private var _toReturnTime: String?
    var fromReceiveTime: Date?
    var toReceiveTime: Date?
    var fromReturnTime: Date?
    var toReturnTime: Date?
    
    var priceOrigin: Int?
    var price: Double?
    var createDate: Int?
    var writeDate: String?
    var countdownTime: Int?
    var priceTimeChange: String?
    var priceLevelChange: Int?
    var totalCargos: Int?
    
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    
    var biddingVehicles: [BiddingVehicle]?
    var note: String?
    
    var isLoadMore = false
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        biddingOrderID <- map["bidding_order_id"]
        biddingPackageNumber <- map["bidding_package_number"]
        status <- map["status"]
        confirmTime <- map["confirm_time"]
        releaseTime <- map["release_time"]
        biddingTime <- map["bidding_time"]
        maxCount <- map["max_count"]
        fromDepot <- map["from_depot"]
        toDepot <- map["to_depot"]
        totalWeight <- map["total_weight"]
        distance <- map["distance"]
        fromLatitude <- map["from_latitude"]
        fromLongitude <- map["from_longitude"]
        toLatitude <- map["to_latitude"]
        toLongitude <- map["to_longitude"]
        
        _fromReceiveTime <- map["from_receive_time"]
        _toReceiveTime <- map["to_receive_time"]
        _fromReturnTime <- map["from_return_time"]
        _toReturnTime <- map["to_return_time"]
        
        // convert về date (+ múi giờ)
        if let _fromReceiveTime = _fromReceiveTime {
            fromReceiveTime = Date.UTCToLocal(date: _fromReceiveTime)?.toDate()
        }
        if let _toReceiveTime = _toReceiveTime {
            toReceiveTime = Date.UTCToLocal(date: _toReceiveTime)?.toDate()
        }
        if let _fromReturnTime = _fromReturnTime {
            fromReturnTime = Date.UTCToLocal(date: _fromReturnTime)?.toDate()
        }
        if let _toReturnTime = _toReturnTime {
            toReturnTime = Date.UTCToLocal(date: _toReturnTime)?.toDate()
        }
        
        priceOrigin <- map["price_origin"]
        price <- map["price"]
        createDate <- map["create_date"]
        writeDate <- map["write_date"]
        countdownTime <- map["countdown_time"]
        priceTimeChange <- map["price_time_change"]
        priceLevelChange <- map["price_level_change"]
        totalCargos <- map["total_cargos"]
        
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        biddingVehicles <- map["bidding_vehicles"]
        note <- map["note"]
    }
    
    func getFromReceiveTimeToReceiveTime() -> String {
        let fromReceiveTimeString = fromReceiveTime?.toString(with: Global.localFormat)
        let toReceiveTimeString = toReceiveTime?.toString(with: Global.localFormat)
        
        if fromReceiveTimeString == nil || fromReceiveTimeString?.isEmpty == true {
            return toReceiveTimeString ?? ""
        }
        
        if toReceiveTimeString == nil || toReceiveTimeString?.isEmpty == true {
            return fromReceiveTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReceiveTimeString ?? "",
                      toReceiveTimeString ?? "")
    }
    
    func getFromReturnTimeToReturnTime() -> String {
        let fromReturnTimeString = fromReturnTime?.toString(with: Global.localFormat)
        let toReturnTimeString = toReturnTime?.toString(with: Global.localFormat)
        
        if fromReturnTimeString == nil || fromReturnTimeString?.isEmpty == true {
            return toReturnTimeString ?? ""
        }
        
        if toReturnTimeString == nil || toReturnTimeString?.isEmpty == true {
            return fromReturnTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReturnTimeString ?? "",
                      toReturnTimeString ?? "")
    }
    
    func getPrice(currency: String = " VND") -> String {
        return String(format: "%@%@", Utils.formatMoney(number: price ?? 0), currency)
    }
    
    func getDistance() -> String {
        return String(format: "%@ km", Utils.formatNumber(number: distance ?? 0))
    }
    
    func getId() -> String {
        return String(format: "ID: %@", biddingPackageNumber ?? "")
    }
    
    func getSubtitleForNewConfirmVanVC() -> String {
        return String(format: "(%@ - %@)", getId(), getDistance())
    }
    
    func getTotalCargosAndTotalWeight() -> String {
        return String(format: "%@ cargo %@ kg",
                      String(totalCargos ?? 0),
                      String(totalWeight ?? 0))
    }
    
    func update(from item: GetBiddingPackageInformationRecord?) {
        guard let item = item else { return }
        mapping(map: Map(mappingType: .fromJSON, JSON: item.toJSON()))
    }
}
