//
//  BiddingPackageTimeModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class BiddingPackageTimeModel: NSObject {
    var date: Date!
    var dateString: String!
    var type: NewHomeType!
    
    func getTitle() -> String {
        let format = "yyyy-MM-dd"
        if let date1 = Date().adding(days: 2)?.toString(with: format).toDate(with: format),
            let date2 = date.toString(with: format).toDate(with: format),
            date1.timeIntervalSince1970 <= date2.timeIntervalSince1970 {
            
            return date.toString(with: "dd/MM")
            
        } else {
            return type.getTitle()
        }
    }
}
