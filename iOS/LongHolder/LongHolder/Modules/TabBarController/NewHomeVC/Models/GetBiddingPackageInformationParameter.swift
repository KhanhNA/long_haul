//
//  GetBiddingPackageInformationRequest.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class GetBiddingPackageInformationParameter: NSObject {
    var biddingTime: String?
    var orderBy: HomeSortType?
    var offset: Int?
    var limit: Int?
    
    enum CodingKeys: String, CodingKey {
        case biddingTime = "bidding_time"
        case orderBy = "order_by"
        case offset = "offset"
        case limit = "limit"
    }
    
    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        if let biddingTime = biddingTime {
            json[CodingKeys.biddingTime.rawValue] = Date.localToUTC(date: biddingTime) ?? ""
        }
        json[CodingKeys.orderBy.rawValue] = orderBy?.rawValue ?? ""
        json[CodingKeys.offset.rawValue] = offset ?? 0
        json[CodingKeys.limit.rawValue] = limit ?? 10
        
        return json
    }
}
