//
//  NewHomeVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import CarbonKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class NewHomeContainerVC: BaseVC {
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var bidmessageSwitch: UISwitch!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var childView: UIView!
    
    let viewModel = NewHomeContainerViewModel()
    private let layout = UICollectionViewFlowLayout()
    private var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        setupNavigationController()
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.newHomeContainerVC = self
        
        setupCollectionView()
        setupNotification()
        handleSubscribe()
        
        viewModel.handleSubscribeSocket()
        viewModel.callApiGetBiddingPackageTime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        handleShowHideEmptyView()
    }
    
    private func setupNotification() {
        notificationLabel.text = "Thông báo".localized()
        bidmessageSwitch.isOn = UserDefaultsHelper.getIsOnNotification()
    }
    
    private func handleSubscribe() {
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý thêm các view controllers
        viewModel.controllerArray.subscribe(onNext: { [weak self] (value) in
            guard let self = self else { return }
            
            if value.count == 0 {
                self.collectionView.superview?.isHidden = true
                return
            }
            
            self.collectionView.superview?.isHidden = false
            
            for viewController in value {
                
                viewController.mainCoordinator = self.mainCoordinator
                
                viewController.handleHideSwitchView = { [weak self] in
                    guard let self = self else { return }
                    
                    // nếu switchView đang show mà scroll các page
                    // ẩn switchView
                    if !self.switchView.isHidden {
                        self.handleNotification()
                    }
                }
            }
            
            self.setupCarbonTabSwipeNavigation()
            self.updateCollectionViewLayout()
            
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi thay đổi tab
        viewModel.currentTabIndex.subscribe(onNext: { [weak self] (value) in
            guard let self = self, self.viewModel.controllerArray.value.count > 0 else { return }
            
            self.setupNavigationController(title: "Danh sách đấu thầu".localized())
            
            self.carbonTabSwipeNavigation?.setCurrentTabIndex(UInt(value), withAnimation: true)
            
            guard value < self.viewModel.biddingPackageTimeModels.value.count else { return }
            
            self.collectionView.reloadData()
            
            self.collectionView.scrollToItem(at: IndexPath(item: value, section: 0),
                                             at: .centeredHorizontally,
                                             animated: true)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // Danh sách thời gian đấu thầu
        // Status (đang/sắp đấu thầu)
        viewModel.biddingPackageTimeModels.subscribe(onNext: { [weak self] (value) in
            guard let self = self else { return }
            
            var controllerArray = [NewHomeVC]()
            
            for (index, item) in value.enumerated() {
                
                let viewController = NewHomeVC(nibName: NewHomeVC.className, bundle: nil)
                viewController.biddingPackageTimeModel = item
                
                if (index + 1) < value.count {
                    viewController.nextBiddingPackageTimeModel = value[index + 1]
                }
                
                controllerArray.append(viewController)
            }
            
            self.viewModel.controllerArray.accept(controllerArray)
            
            self.collectionView.reloadData()
            
            self.handleShowHideEmptyView()
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func switchAction(_ sender: Any) {
        handleShowHideEmptyView()
        
        UserDefaultsHelper.setIsOnNotification(isOn: bidmessageSwitch.isOn)
        
        viewModel.handleOnOffBidmessage()
    }
}

extension NewHomeContainerVC {
    private func setupNavigationController() {
        setupNavigationController(title: "Danh sách đấu thầu".localized())
        setLeftBarButtonItem(isArrowUp: true)
    }
    
    private func setLeftBarButtonItem(isArrowUp: Bool) {
        let imageNamed = (isArrowUp ? "home_vc_arrow_up" : "home_vc_arrow_down")
        let image = UIImage(named: imageNamed)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(handleNotification))
    }
}

extension NewHomeContainerVC {
    private func setupCarbonTabSwipeNavigation() {
        let items = viewModel.controllerArray.value.compactMap({ _ in return "" })
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: childView)
        
        carbonTabSwipeNavigation.toolbar.clipsToBounds = true
        carbonTabSwipeNavigation.currentTabIndex = 0
        
        carbonTabSwipeNavigation.setTabExtraWidth(0)
        carbonTabSwipeNavigation.setTabBarHeight(0)
    }
}

extension NewHomeContainerVC {
    @objc private func handleNotification() {
        switchView.isHidden = !switchView.isHidden
        view.addSubview(switchView)
        setLeftBarButtonItem(isArrowUp: switchView.isHidden)
    }
    
    private func handleShowHideEmptyView() {
        guard let viewController = viewModel.controllerArray.value.first else { return }
        viewController.emptyView?.isHidden = bidmessageSwitch.isOn
    }
}

extension NewHomeContainerVC: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation,
                                  viewControllerAt index: UInt) -> UIViewController {
        return viewModel.controllerArray.value[Int(index)]
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation,
                                  didMoveAt index: UInt) {
        viewModel.currentTabIndex.accept(Int(index))
    }
}

extension NewHomeContainerVC {
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(cellNibName: NewHomeContainerCollectionViewCell.className)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        collectionView.superview?.backgroundColor = .white
        
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        
        updateCollectionViewLayout()
    }
    
    private func updateCollectionViewLayout() {
        guard viewModel.controllerArray.value.count > 0 else { return }
        
        if viewModel.controllerArray.value.count <= 4 {
            
            let width = UIScreen.main.bounds.width / CGFloat(viewModel.controllerArray.value.count)
            layout.itemSize = CGSize(width: width, height: 45)
            
        } else {
            
            layout.itemSize = CGSize(width: 100, height: 45)
            
        }
        
        collectionView.collectionViewLayout = layout
        
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0),
                                    at: .centeredVertically, animated: true)
    }
}

extension NewHomeContainerVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.biddingPackageTimeModels.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(NewHomeContainerCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }
        
        cell.setup(biddingPackageTimeModel: viewModel.biddingPackageTimeModels.value[indexPath.item],
                   isSelected: (indexPath.item == viewModel.currentTabIndex.value))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.currentTabIndex.accept(indexPath.item)
    }
}
