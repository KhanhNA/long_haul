//
//  NewHomeTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class NewHomeTableViewCell: UITableViewCell {
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityAndTotalWeightLabel: UILabel!
    @IBOutlet weak var fromDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReceiveTimeToReceiveTimeLabel: UILabel!
    @IBOutlet weak var toDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReturnTimeToReturnTimeLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var biddingButton: UIButton!
    
    private var getBiddingPackageInformationRecord: GetBiddingPackageInformationRecord!
    var biddingClosure: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(getBiddingPackageInformationRecord: GetBiddingPackageInformationRecord,
               biddingPackageTimeModel: BiddingPackageTimeModel) {
        
        self.getBiddingPackageInformationRecord = getBiddingPackageInformationRecord
        
        priceLabel.text = getBiddingPackageInformationRecord.getPrice()
        
        distanceLabel.text = getBiddingPackageInformationRecord.getDistance()
        
        quantityAndTotalWeightLabel.text = getBiddingPackageInformationRecord.getTotalCargosAndTotalWeight()
        
        fromDepotAddressLabel.text = getBiddingPackageInformationRecord.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getBiddingPackageInformationRecord.getFromReceiveTimeToReceiveTime()
        
        toDepotAddressLabel.text = getBiddingPackageInformationRecord.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getBiddingPackageInformationRecord.getFromReturnTimeToReturnTime()
        
        idLabel.text = getBiddingPackageInformationRecord.getId()
        
        if biddingPackageTimeModel.type == .happenning {
            
            biddingButton.setTitle("Đấu thầu".localized(), for: .normal)
            
        } else {
            
            if let id = getBiddingPackageInformationRecord.id,
                let (eventModel, event) = EventModel.getEKEvent(id: String(id)),
                let _ = eventModel,
                let _ = event {
                
                biddingButton.setTitle("Hủy nhắc nhở".localized(), for: .normal)
                biddingButton.setTitleColor(AppColor.hex919395, for: .normal)
                biddingButton.borderColor = AppColor.hex919395
                
            } else {
                
                biddingButton.setTitle("Nhắc nhở tôi".localized(), for: .normal)
                biddingButton.setTitleColor(AppColor.hex00A359, for: .normal)
                biddingButton.borderColor = AppColor.hex00A359
                
            }
        }
    }
    
    @IBAction func biddingAction(_ sender: Any) {
        biddingClosure?()
    }
}
