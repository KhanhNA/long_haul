//
//  NewHomeVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets

enum NewHomeType: Int {
    case happenning = 0
    case upcoming = 1
    case tomorrow = 2
    
    func getTitle() -> String {
        switch self {
        case .happenning:
            return "Đang diễn ra".localized()
        case .upcoming:
            return "Sắp diễn ra".localized()
        case .tomorrow:
            return "Ngày mai".localized()
        }
    }
}

class NewHomeVC: BaseVC {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var countdownTitleLabel: UILabel!
    @IBOutlet weak var countdownValueLabel: UILabel!
    
    let viewModel = NewHomeViewModel()
    let biddingViewModel = BiddingViewModel()
    
    var handleHideSwitchView: (() -> ())?
    
    var biddingPackageTimeModel: BiddingPackageTimeModel!
    var nextBiddingPackageTimeModel: BiddingPackageTimeModel?
    
    var timer: Timer?
    
    // check gọi api duy nhất 1 lần ở viewDidAppear
    // tránh bị lag khi scroll đổi page
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.biddingPackageTimeModel = biddingPackageTimeModel
        viewModel.nextBiddingPackageTimeModel = nextBiddingPackageTimeModel
        
        emptyLabel.text = "Vui lòng bật thông báo để nhận các gói thấu mới!".localized()
        
        if biddingPackageTimeModel.type == .happenning {
            countdownTitleLabel.text = "Kết thúc trong".localized()
        } else {
            countdownTitleLabel.text = "Bắt đầu trong".localized()
        }
        
        if biddingPackageTimeModel.type == .happenning
            && nextBiddingPackageTimeModel == nil {
            
            // case chỉ có item Đang đấu thầu
            // không có Sắp diễn ra
            // không có count down Đã hết hạn
            
            countdownTitleLabel.text = " "
            countdownValueLabel.text = " "
            
        } else {
            startCountdown()
        }
        
        setupTableView()
        handleSubscribe()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleHideSwitchView?()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad {
            isFirstLoad = false
            viewModel.callApi()
        }
    }
    
    private func setupTableView() {
        tableView.tableHeaderView = headerView
        
        tableView.register(headerNibName: HomeTableViewHeaderFooterView.className)
        tableView.register(cellNibName: NewHomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // cập nhật trạng thái khi thay đổi sort
        viewModel.sortType.subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            let sortTitle = "Sắp xếp theo".localized()
            self.sortButton.setTitle(sortTitle + ": " + value.getTitle(), for: .normal)
            
            self.viewModel.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getBiddingPackageInformationRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        handleSubscribeBiddingViewModel()
    }
    
    func handleSubscribeBiddingViewModel() {
        // xử lý show / hide loading
        biddingViewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        biddingViewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        biddingViewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // dữ liệu khi gọi api createBiddingOrder trả về
        biddingViewModel.createBiddingOrderRecord.subscribe(onNext: { [weak self] (createBiddingOrderRecord) in
            
            guard let self = self,
                let maxConfirmTime = createBiddingOrderRecord?.maxConfirmTime?.toString(),
                let biddingOrderID = createBiddingOrderRecord?.biddingOrderID
                else { return }
            
            let getBiddingPackageInformationRecord = self.viewModel
                .getBiddingPackageInformationRecords.value
                .first(where: { $0.id == biddingOrderID })
            
            self.mainCoordinator?
                .goToNewConfirmVanVC(maxConfirmTime: maxConfirmTime,
                                     biddingOrderId: String(biddingOrderID),
                                     getListBiddingVehicleRecords: nil,
                                     subtitle: getBiddingPackageInformationRecord?
                                        .getSubtitleForNewConfirmVanVC() ?? "",
                                     isBiddingFlow: true)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let controller = HomeSortVC(nibName: HomeSortVC.className, bundle: nil)
        controller.type = viewModel.sortType.value ?? .priceAsc
        controller.completion = { [weak self] type in
            self?.viewModel.sortType.accept(type)
        }
        
        let sheet = SheetViewController(controller: controller, sizes: [.fixed(375)])
        sheet.handleColor = .clear
        sheet.topCornersRadius = 20
        sheet.extendBackgroundBehindHandle = false
        sheet.overlayColor = AppColor.hex80C4C4C4
        present(sheet, animated: false, completion: nil)
    }
}

extension NewHomeVC {
    func startCountdown() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(handleCountdown),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    private func getHMS() -> (hours: Int, minutes: Int, seconds: Int) {
        if biddingPackageTimeModel.type == .happenning {
            
            guard let nextBiddingPackageTimeModel = nextBiddingPackageTimeModel else {
                return (0, 0, 0)
            }
            
            let startDate = Date()
            
            let differenceInSeconds = Int(nextBiddingPackageTimeModel.date.timeIntervalSince(startDate))
            
            return Utils.secondsToHoursMinutesSeconds(seconds: differenceInSeconds)
            
        } else {
            
            let startDate = Date()
            
            let differenceInSeconds = Int(biddingPackageTimeModel.date.timeIntervalSince(startDate))
            
            return Utils.secondsToHoursMinutesSeconds(seconds: differenceInSeconds)
            
        }
    }
    
    @objc private func handleCountdown() {
        let (hours, minutes, seconds) = getHMS()
        
        if hours <= 0 && minutes <= 0 && seconds <= 0 {
            
            if biddingPackageTimeModel.type == .happenning {
                
                countdownValueLabel.text = "Đã hết hạn".localized()
                timer?.invalidate()
                
            } else {
                
                countdownValueLabel.text = "Đã bắt đầu".localized()
                timer?.invalidate()
                
            }
            
        } else {
            
            let time = Utils.getHoursMinutesSeconds(hours: hours, minutes: minutes, seconds: seconds)
            countdownValueLabel.text = time
        }
    }
}

extension NewHomeVC {
    private func addEvent(getBiddingPackageInformationRecord: GetBiddingPackageInformationRecord) {
        guard let id = getBiddingPackageInformationRecord.id else { return }
        
        if let (eventModel, event) = EventModel.getEKEvent(id: String(id)),
            let _ = eventModel,
            let _ = event {
            
            EventHelper.removeEvent(event!)
            
            eventModel!.delete()
            
            tableView.reloadData()
            
        } else {
            
            let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self,
                                                            fromKey: "loginModel")
            
            let announceTimeBefore = loginModel?.announce_time_before ?? 1
            
            let title = String(format: "[LongHaul Bidding][%@ - %@ - %@]",
                               getBiddingPackageInformationRecord.biddingPackageNumber ?? "",
                               getBiddingPackageInformationRecord.getDistance(),
                               getBiddingPackageInformationRecord.getPrice())
            
            let notes = String(format: "Gói thầu từ %@ tới %@ sẽ bắt đầu lúc %@".localized(),
                               getBiddingPackageInformationRecord.fromDepot?.address ?? "",
                               getBiddingPackageInformationRecord.toDepot?.address ?? "",
                               biddingPackageTimeModel.date.toString(with: "HH:mm"))
            
            let completion: (_ error: Error?) -> () = { [weak self] error in
                guard let self = self else { return }
                
                if let error = error {
                    DispatchQueue.main.async { [weak self] in
                        self?.viewModel.errorMessage.accept(error.localizedDescription)
                    }
                    return
                }
                
                let message = String(format: "Nhắc nhở sẽ được gửi đến bạn %@ phút trước khi gói thầu bắt đầu".localized(),
                                     String(announceTimeBefore))
                
                DispatchQueue.main.async {
                    SuccessDialogView.show(message: message) { [weak self] in
                        self?.tableView.reloadData()
                    }
                }
            }
            
            EventHelper.addEventToCalendar(id: String(id),
                                           title: title,
                                           notes: notes,
                                           startDate: biddingPackageTimeModel.date,
                                           announceTimeBefore: announceTimeBefore,
                                           completion: completion)
        }
    }
    
    private func handleBidding(getBiddingPackageInformationRecord: GetBiddingPackageInformationRecord) {
        if biddingPackageTimeModel.type == .happenning {
            
            guard let id = getBiddingPackageInformationRecord.id else { return }
            
            // ĐẤU THẦU
            
            ConfirmDialogView.show(title: "Xác nhận tham gia đấu thầu!".localized(),
                                   yesTitle: "Xác nhận".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { [weak self] in
                                    
                                    guard let self = self else { return }
                                    self.biddingViewModel.biddingPackageId = String(id)
                                    self.biddingViewModel.biddingCargo()
                                    
                }, noAction: { })
            
        } else {
            
            // Nhắc nhở tôi
            
            addEvent(getBiddingPackageInformationRecord: getBiddingPackageInformationRecord)
            
        }
    }
}

extension NewHomeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getBiddingPackageInformationRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getBiddingPackageInformationRecord = viewModel.getBiddingPackageInformationRecords.value[indexPath.row]
        
        if indexPath.row == viewModel.getBiddingPackageInformationRecords.value.count - 1
            && getBiddingPackageInformationRecord.isLoadMore {
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(NewHomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getBiddingPackageInformationRecord: getBiddingPackageInformationRecord,
                   biddingPackageTimeModel: biddingPackageTimeModel)
        
        cell.biddingClosure = { [weak self] in
            guard let self = self else { return }
            
            let (hours, minutes, seconds) = self.getHMS()
            
            if hours <= 0 && minutes <= 0 && seconds <= 0 {
                
                let isHappenning = (self.biddingPackageTimeModel.type == .happenning)
                
                var message = ""
                if isHappenning {
                    message = "Đã hết thời gian đấu thầu".localized()
                } else {
                    message = "Đã bắt đầu thời gian đấu thầu".localized()
                }
                
                self.viewModel.errorMessage.accept(message)
                
                return
            }
            
            
            // xử lý ĐẤU THẦU hoặc Nhắc nhở tôi
            
            self.handleBidding(getBiddingPackageInformationRecord: getBiddingPackageInformationRecord)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getBiddingPackageInformationRecord = viewModel.getBiddingPackageInformationRecords.value[indexPath.row]
        guard let id = getBiddingPackageInformationRecord.id else { return }
        
        mainCoordinator?.goToDetailBiddingVC(id: id, biddingPackageTimeModel: viewModel.biddingPackageTimeModel)
    }
}
