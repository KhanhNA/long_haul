//
//  NewHomeViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NewHomeViewModel: BaseViewModel {
    // trạng thái sắp xếp
    let sortType = BehaviorRelay<HomeSortType?>(value: nil)
    
    // danh sách bidding trên table view
    let getBiddingPackageInformationRecords = BehaviorRelay<[GetBiddingPackageInformationRecord]>(value: [])
    
    var biddingPackageTimeModel: BiddingPackageTimeModel!
    var nextBiddingPackageTimeModel: BiddingPackageTimeModel?
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        isShowEmpty.accept(false)
        
        offset = 0
        callApi(isShowLoading: isShowLoading)
    }
    
    func callApi(isShowLoading: Bool = true) {
        guard let biddingTime = biddingPackageTimeModel?.dateString else { return }
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        let parameter = GetBiddingPackageInformationParameter()
        parameter.biddingTime = biddingTime
        parameter.orderBy = sortType.value ?? .priceAsc
        parameter.offset = offset
        parameter.limit = limit
        
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        NewHomeApi.getBiddingPackageInformation(parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.getBiddingPackageInformationRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.getBiddingPackageInformationRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.getBiddingPackageInformationRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                let fakeData = GetBiddingPackageInformationRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.getBiddingPackageInformationRecords.accept(oldRecords)
        }
    }
}
