//
//  NewHomeContainerCollectionViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class NewHomeContainerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
    var isSelectedView: Bool = false {
        didSet {
            let selectedColor = AppColor.hex00A359
            let grayColor = AppColor.hex919395
            
            selectedView.backgroundColor    = (isSelectedView ? selectedColor : .clear)
            titleLabel.textColor            = (isSelectedView ? selectedColor : .black)
            subtitleLabel.textColor         = (isSelectedView ? selectedColor : grayColor)
            
            let titleLabelFontPointSize     = titleLabel.font.pointSize
            let subtitleLabelFontPointSize  = subtitleLabel.font.pointSize
            
            if isSelectedView {
                
                titleLabel.font = .boldSystemFont(ofSize: titleLabelFontPointSize)
                subtitleLabel.font = .boldSystemFont(ofSize: subtitleLabelFontPointSize)
                
            } else {
                
                titleLabel.font = .systemFont(ofSize: titleLabelFontPointSize)
                subtitleLabel.font = .systemFont(ofSize: subtitleLabelFontPointSize)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(biddingPackageTimeModel: BiddingPackageTimeModel,
               isSelected: Bool) {
        
        titleLabel.text = biddingPackageTimeModel.date.toString(with: "HH:mm")
        subtitleLabel.text = biddingPackageTimeModel.getTitle()
        
        isSelectedView = isSelected
    }
}
