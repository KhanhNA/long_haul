//
//  NewHomeApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/14/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

enum GetBiddingPackageDetailFromScreen {
    case newHome
    case home
}

class NewHomeApi: NSObject {
    // Danh sách thời gian đấu thầu
    // Status (đang/sắp đấu thầu)
    typealias GetBiddingPackageTimeRecordResult = BaseMappableSimpleResultModel<String>
    typealias GetBiddingPackageTimeResponseCompletion = BaseMappableResponseModel<GetBiddingPackageTimeRecordResult>
    typealias GetBiddingPackageTimeCompletion = (GetBiddingPackageTimeResponseCompletion?, Error?) -> ()
    
    static func getBiddingPackageTime(completion: @escaping GetBiddingPackageTimeCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.biddingPackageTime.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": [:]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Danh sách đấu thầu
    typealias GetBiddingPackageInformationResult = BaseMappableResultModel<GetBiddingPackageInformationRecord>
    typealias GetBiddingPackageInformationResponseCompletion = BaseMappableResponseModel<GetBiddingPackageInformationResult>
    typealias GetBiddingPackageInformationCompletion = (GetBiddingPackageInformationResponseCompletion?, Error?) -> ()
    
    static func getBiddingPackageInformation(parameter: GetBiddingPackageInformationParameter,
                                             completion: @escaping GetBiddingPackageInformationCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingPackageInformation.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": parameter.getJson()],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Bidding gói thầu
    typealias CreateBiddingOrderResult = BaseMappableResultModel<CreateBiddingOrderRecord>
    typealias CreateBiddingOrderResponseCompletion = BaseMappableResponseModel<CreateBiddingOrderResult>
    typealias CreateBiddingOrderCompletion = (CreateBiddingOrderResponseCompletion?, Error?) -> ()
    
    static func createBiddingOrder(biddingPackageId: String,
                                   completion: @escaping CreateBiddingOrderCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.createBiddingOrder.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["bidding_package_id": biddingPackageId,
                                                                         "confirm_time": Date.localToUTC(date: Date().toString())]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Add lái xe vào đơn thầu
    typealias ComfirmBiddingVehicleForBiddingOrderCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()
    
    static func comfirmBiddingVehicleForBiddingOrder(biddingOrderId: String,
                                                     biddingVehicleIds: String,
                                                     completion: @escaping ComfirmBiddingVehicleForBiddingOrderCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.comfirmBiddingVehicleForBiddingOrder.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["bidding_order_id": biddingOrderId,
                                                                         "bidding_vehicle_ids": biddingVehicleIds]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    // Chi tiết gói thầu
    typealias GetBiddingPackageDetailResult = BaseMappableResultModel<GetBiddingPackageDetailRecord>
    typealias GetBiddingPackageDetailResponseCompletion = BaseMappableResponseModel<GetBiddingPackageDetailResult>
    typealias GetBiddingPackageDetailCompletion = (GetBiddingPackageDetailResponseCompletion?, Error?) -> ()
    
    static func getBiddingPackageDetail(id: String,
                                        completion: @escaping GetBiddingPackageDetailCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingPackageDetail.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["bidding_package_id": id]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    typealias GetBiddingDetailResult = BaseMappableResultModel<GetBiddingDetailRecord>
    typealias GetBiddingDetailResponseCompletion = BaseMappableResponseModel<GetBiddingDetailResult>
    typealias GetBiddingDetailCompletion = (GetBiddingDetailResponseCompletion?, Error?) -> ()
    
    static func getBiddingOrderDetail(id: String,
                                      completion: @escaping GetBiddingDetailCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingOrderDetail.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["bidding_order_id": id]],
                                                 headers: nil,
                                                 completion: completion)
    }
}
