//
//  NotificationVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/26/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class NotificationVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    let viewModel = NotificationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        initMainCoordinator()
        setupNavigationController(title: "Thông báo".localized())
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.notificationVC = self
        
        setupTableView()
        handleSubscribe()
        
        viewModel.reloadData()
    }
    
    private func setupTableView() {
        tableView.register(cellNibName: NotificationTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.notificationRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
}

extension NotificationVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.notificationRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationRecord = viewModel.notificationRecords.value[indexPath.row]

        if indexPath.row == viewModel.notificationRecords.value.count - 1 && notificationRecord.isLoadMore {
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }

            cell.start()

            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }

            return cell
        }
        
        
        guard let cell = tableView.dequeue(NotificationTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }

        cell.setup(notificationRecord: notificationRecord)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let notificationRecord = viewModel.notificationRecords.value[indexPath.row]
        
        guard let type = notificationRecord.type else { return }
        
        switch type {
        case .detail:
            guard let itemID = notificationRecord.itemID,
                let intItemID = Int(itemID) else { return }
            mainCoordinator?.goToDetailBiddingVC(id: intItemID, biddingPackageTimeModel: nil)
        }
        
        viewModel.setIsRead(id: notificationRecord.id)
    }
}
