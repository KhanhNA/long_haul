//
//  NotificationModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/26/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

enum NotificationRecordType: String {
    case detail = "enterprise_application_bidding_order_detail"
}

class NotificationRecord: NSObject, Mappable {
    var id: Int?
    var notificationID: Int?
    var isRead: Bool?
    var createUid: Int?
    
    private var _createDate: String?
    var createDate: Date?
    
    private var _sentDate: String?
    var sentDate: Date?
    
    var title: String?
    var content: String?
    var type: NotificationRecordType?
    var clickAction: String?
    var messageType: String?
    var itemID: String?
    var objectStatus: String?
    var image256: String?
    
    var isLoadMore = false
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        notificationID <- map["notification_id"]
        isRead <- map["is_read"]
        createUid <- map["create_uid"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        _createDate <- map["sent_date"]
        // convert về date (+ múi giờ)
        if let _sentDate = _sentDate {
            sentDate = Date.UTCToLocal(date: _sentDate)?.toDate()
        }
        
        title <- map["title"]
        content <- map["content"]
        type <- map["type"]
        clickAction <- map["click_action"]
        messageType <- map["message_type"]
        itemID <- map["item_id"]
        objectStatus <- map["object_status"]
        image256 <- map["image_256"]
    }
    
    func getCreateDate() -> String {
        guard let createDate = createDate else { return "" }
        
        let date = Date()
        let differenceInSeconds = Int(date.timeIntervalSince(createDate))
        let differenceInMinutes = Int(differenceInSeconds / 60)
        let differenceInHours = Int(differenceInSeconds / 60)
        
        if differenceInMinutes < 60 {
            let minutesString = "phút trước".localized()
            
            if differenceInMinutes <= 1 {
                return String(format: "1 %@", minutesString)
            }
            
            return String(format: "%@ %@", String(differenceInMinutes), minutesString)
            
        } else if differenceInHours < 24 {
            
            let hoursString = "giờ trước".localized()
            
            if differenceInHours <= 1 {
                return String(format: "1 %@", hoursString)
            }
            
            return String(format: "%@ %@", String(differenceInHours), hoursString)
            
        } else {
            
            return createDate.toString(with: Global.localFormat)
        }
    }
}
