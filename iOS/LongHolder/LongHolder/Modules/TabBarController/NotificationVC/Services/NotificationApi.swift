//
//  NotificationApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/26/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class NotificationApi: NSObject {
    // Lịch sử thông báo
    typealias NotificationResult = BaseMappableResultModel<NotificationRecord>
    typealias NotificationResponseCompletion = BaseMappableResponseModel<NotificationResult>
    typealias NotificationCompletion = (NotificationResponseCompletion?, Error?) -> ()
    
    static func getBiddingNotificationHistory(offset: String,
                                              completion: @escaping NotificationCompletion) {
        /*
         type = "bidding_order". App doanh nghiệp
         type = "bidding_vehicle". App driver
         */
        
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingNotificationHistory.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["type": "bidding_order", "offset": offset]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    typealias IsReadCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()
    static func setIsRead(id: Int, completion: @escaping IsReadCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.isRead.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["id": id]],
                                                 headers: nil,
                                                 completion: completion)
    }
}
