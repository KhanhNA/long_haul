//
//  VanDetailViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/19/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VanDetailViewModel: BaseViewModel {
    // danh sách bidding trên table view
    let getBiddingInformationRecords = BehaviorRelay<[GetBiddingInformationRecord]>(value: [])
    
    var getListVehicleRecord: GetListVehicleRecord?
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        isShowEmpty.accept(false)
        
        offset = 0
        callApi(isShowLoading: isShowLoading)
    }
    
    func callApi(isShowLoading: Bool = true) {
        guard let biddingVehicleId = getListVehicleRecord?.id else { return }
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        VanApi.getListBiddingOrderByBiddingVehicleId(biddingVehicleId: biddingVehicleId, offset: offset, limit: limit) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.getBiddingInformationRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.getBiddingInformationRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.getBiddingInformationRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                let fakeData = GetBiddingInformationRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.getBiddingInformationRecords.accept(oldRecords)
        }
    }
}
