//
//  VanDetailVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class VanDetailVC: BaseVC {
    @IBOutlet weak var lisencePlateLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverPhoneNumberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    private let viewModel = VanDetailViewModel()
    var getListVehicleRecord: GetListVehicleRecord?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getListVehicleRecord = getListVehicleRecord
        
        setupViews()
        
        setupTableView()
        
        handleSubscribe()
        
        viewModel.callApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupViews() {
        lisencePlateLabel.text = String(format: "%@: %@", "BKS".localized(),
                                        getListVehicleRecord?.lisencePlate ?? "")
        
        driverNameLabel.text = String(format: "%@: %@", "Tài xế".localized(),
                                      getListVehicleRecord?.driverName ?? "")
        
        driverPhoneNumberLabel.text = String(format: "%@: %@", "Số điện thoại".localized(),
                                             getListVehicleRecord?.driverPhoneNumber ?? "")
        
        avatarImageView.setImage(urlString: getListVehicleRecord?.image128,
                                 placeholder: UIImage(named: "confirm_van_vc_avatar"))
    }
    
    private func setupTableView() {
        tableView.register(cellNibName: HomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setEmptyDataSet(for: tableView)
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getBiddingInformationRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension VanDetailVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getBiddingInformationRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        if indexPath.row == viewModel.getBiddingInformationRecords.value.count - 1
            && getBiddingInformationRecord.isLoadMore {
            
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(HomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getBiddingInformationRecord: getBiddingInformationRecord, homeType: .success)
        
        cell.showReason = {
            ReasonView.show(title: "Lý do hủy gói thầu".localized(),
                            detail: getBiddingInformationRecord.note ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getBiddingInformationRecord = viewModel.getBiddingInformationRecords.value[indexPath.row]
        
        guard let id = getBiddingInformationRecord.id else { return }
        
        mainCoordinator?.goToDetailBiddingVC(id: id, biddingPackageTimeModel: nil)
    }
}

