//
//  VanFilterView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

enum VanFilterType: Int {
    case all = 2
    case hasOrder = 1
    case noOrder = 0
    
    func getTitle() -> String {
        switch self {
        case .all:
            return "Tất cả xe".localized()
        case .hasOrder:
            return "Xe đã có đơn hàng".localized()
        case .noOrder:
            return "Xe chưa có đơn hàng".localized()
        }
    }
}

class VanFilterView: BaseDialogView {
    @IBOutlet weak var allLabel: UILabel!
    @IBOutlet weak var noOrderLabel: UILabel!
    @IBOutlet weak var hasOrderLabel: UILabel!
    
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var noOrderButton: UIButton!
    @IBOutlet weak var hasOrderButton: UIButton!
    
    var completion: ((_ type: VanFilterType) -> ())?
    
    static func show(type: VanFilterType, completion: ((_ type: VanFilterType) -> ())?) {
        let view = VanFilterView()
        view.show {
            view.allLabel.text = VanFilterType.all.getTitle()
            view.noOrderLabel.text = VanFilterType.noOrder.getTitle()
            view.hasOrderLabel.text = VanFilterType.hasOrder.getTitle()
            
            view.completion = completion
            view.setType(type)
        }
    }
    
    func setType(_ type: VanFilterType) {
        allButton.isSelected = false
        noOrderButton.isSelected = false
        hasOrderButton.isSelected = false
        
        switch type {
        case .all:
            allButton.isSelected = true
        case .noOrder:
            noOrderButton.isSelected = true
        case .hasOrder:
            hasOrderButton.isSelected = true
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func allAction(_ sender: Any) {
        completion?(.all)
        dismiss()
    }
    
    @IBAction func noOrderAction(_ sender: Any) {
        completion?(.noOrder)
        dismiss()
    }
    
    @IBAction func hasOrderAction(_ sender: Any) {
        completion?(.hasOrder)
        dismiss()
    }
}
