//
//  VanMapsViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VanMapsViewModel: BaseViewModel {
    let getListVehicleRecords = BehaviorRelay<[GetListVehicleRecord]>(value: [])
    
    func callApi() {
        isShowLoading.accept(true)
        
        let parameter = GetListVehicleRequest()
        parameter.textSearch = ""
        parameter.offset = offset
        parameter.limit = limit
        parameter.status = .all
        
        VanApi.getListVehicle(parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                return
            }
            
            self.getListVehicleRecords.accept(records)
        }
    }
}
