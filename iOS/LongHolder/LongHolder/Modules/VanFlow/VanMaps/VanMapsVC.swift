//
//  VanMapsVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import GoogleMaps
import DropDown
import CoreLocation

class VanMapsVC: BaseVC {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var hasOrderLabel: UILabel!
    @IBOutlet weak var noOrderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    private var mapView: GMSMapView!
    
    private let viewModel = VanMapsViewModel()
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        handleSubscribe()
        
        viewModel.callApi()
        
        requestAlwaysAuthorization()
    }
    
    private func setupViews() {
        setupNavigationController(title: "Vị trí nhân viên".localized())
        
        hasOrderLabel.text = "Đã có đơn hàng".localized()
        noOrderLabel.text = "Chưa có đơn hàng".localized()
        
        titleLabel.text = ""
        subtitleLabel.text = ""
        
        topView.dropShadow(cornerRadius: 12)
        bottomView.dropShadow(cornerRadius: 12)
    }
    
    private func setTitleAndSubtitle(item: GetListVehicleRecord) {
        titleLabel.text = String(format: "%@ %@ %@",
                                 "BKS".localized(),
                                 item.lisencePlate ?? "",
                                 item.driverName ?? "")
        
        if let _ = item.latitude, let _ = item.longitude {
            subtitleLabel.isHidden = true
        } else {
            subtitleLabel.isHidden = false
            subtitleLabel.text = "(Không lấy được vị trí hiện tại)".localized()
        }
    }
    
    func handleLocation() {
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    private func handleSubscribe() {
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.getListVehicleRecords.subscribe(onNext: { [weak self] (_) in
            
            self?.addMakers()
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func changeDriverAction(_ sender: Any) {
        let dropDown = DropDown()
        
        dropDown.anchorView = topView
        
        dropDown.dataSource = viewModel.getListVehicleRecords.value.compactMap({
            String(format: "%@ %@ %@",
                   "BKS".localized(),
                   $0.lisencePlate ?? "",
                   $0.driverName ?? "")
        })
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else { return }
            self.focus(to: self.viewModel.getListVehicleRecords.value[index])
        }
        
        dropDown.width = topView.frame.width
        
        dropDown.dismissMode = .onTap
        
        dropDown.show()
    }
}

extension VanMapsVC {
    private func focus(to item: GetListVehicleRecord) {
        setTitleAndSubtitle(item: item)
        
        guard let latitude = item.latitude, let longitude = item.longitude else { return }
        
        let camera = MapsHelper.getGMSCameraPosition(latitude: latitude, longitude: longitude)
        mapView.camera = camera
    }
    
    private func addMakers() {
        if viewModel.getListVehicleRecords.value.isEmpty {
            handleLocation()
            return
        }
        
        for item in viewModel.getListVehicleRecords.value {
            
            if let latitude = item.latitude, let longitude = item.longitude {
                
                if mapView == nil {
                    mapView = MapsHelper.addMap(in: mapContainerView, latitude: latitude, longitude: longitude)
                    focus(to: item)
                }
                
                let imageName = (item.isBidding ? "van_vc_has_order" : "van_vc_no_order")
                
                let title = item.lisencePlate ?? ""
                
                let snippet = String(format: "%@: %@\n%@: %@",
                                     "Tài xế".localized(), item.driverName ?? "",
                                     "Sđt".localized(), item.driverPhoneNumber ?? "")
                
                _ = mapView.addMarker(latitude: latitude,
                                     longitude: longitude,
                                     imageName: imageName,
                                     title: title,
                                     snippet: snippet)
            }
        }
    }
}

extension VanMapsVC: CLLocationManagerDelegate {
    func requestAlwaysAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        locationManager.stopUpdatingLocation()

        if mapView == nil {
            mapView = MapsHelper.addMap(in: mapContainerView,
                                        latitude: location.coordinate.latitude,
                                        longitude: location.coordinate.longitude)
        }
    }
}
