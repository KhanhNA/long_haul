//
//  BaseViewController.swift
//  OdooCustomer
//
//  Created by dong luong on 7/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    let defaults = UserDefaults.standard
     var keyboardMan = KeyboardMan()
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if navigationController?.topViewController != self {
            navigationController?.navigationBar.isHidden = false
        }
        super.viewWillDisappear(animated)
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
           
           var returnValue = true
           let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
           
           do {
               let regex = try NSRegularExpression(pattern: emailRegEx)
               let nsString = emailAddressString as NSString
               let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
               
               if results.count == 0
               {
                   returnValue = false
               }
               
           } catch let error as NSError {
               print("invalid regex: \(error.localizedDescription)")
               returnValue = false
           }
           
           return  returnValue
       }
    
   
    func openAppChat()  {
//        let  pass = Util.object(forKey: "token") as? String ?? ""
//       // let name = Util.object(forKey: "username") as? String ?? ""
//        let name = Global.mechantsObj.userName ?? ""
//        let customURL = String(format: "App2Open://?name=%@&pass=%@", name,pass)
//              if let url = URL(string: customURL) {
//                  if UIApplication.shared.canOpenURL(url) {
//                      UIApplication.shared.open(url)
//                  } else {
//                      openAppStore()
//                      print("App scheme not found")
//                  }
//              }
    }
    
    func openAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1083446067"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
    
    func keyboardCallback(_ info: ((_ keyboardHeight: CGFloat, _ action: KeyboardMan.KeyboardInfo.Action) -> Void)?) {
           self.keyboardMan.postKeyboardInfo = { _, keyboardInfo in
               // TODO:
               switch keyboardInfo.action {
               case .show:
                   let value: CGFloat = UIApplication.shared.statusBarFrame.height < 44 ? 0 : -34
                   info!(keyboardInfo.height + value, keyboardInfo.action)
               case .hide:
                   info!(0, keyboardInfo.action)
               }
           }
       }
    
     func callNumber(phoneNumber:String) {

      if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {

        let application:UIApplication = UIApplication.shared
        if (application.canOpenURL(phoneCallURL)) {
            application.open(phoneCallURL, options: [:], completionHandler: nil)
        }
      }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        super.viewWillAppear(animated)
    }
    
    func showPopUp(title: String, message: String, titleBtnOK: String, isCancel: Bool, actionOK: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: titleBtnOK, style: .default, handler: { action in
            if action.style == .default {
                actionOK()
            }
        }))
        if isCancel {
            alert.addAction(UIAlertAction(title: "Bỏ qua", style: .cancel, handler: { _ in
                
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
  
    
}
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    func maskCircle(anyImage: UIImage) {
        self.contentMode = UIView.ContentMode.scaleAspectFill
       self.layer.cornerRadius = self.frame.height / 2
       self.layer.masksToBounds = false
       self.clipsToBounds = true
       self.image = anyImage
      }

}

extension UINavigationController {
    func popToViewControllerCustom(_ ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}

//extension BaseViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    func uploadImageAction() {
//        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
//
//        let photoAction = UIAlertAction(title: "Thư viện ảnh", style: .default) { [weak self] (_: UIAlertAction!) in
//            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
//                let imagePicker = UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = .photoLibrary
//                imagePicker.allowsEditing = false
//                self?.present(imagePicker, animated: true, completion: nil)
//            }
//        }
//        alert.addAction(photoAction)
//
//        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (_: UIAlertAction!) in
//            if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                let imagePicker = UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = .camera
//                imagePicker.allowsEditing = false
//                self?.present(imagePicker, animated: true, completion: nil)
//            }
//        }
//        alert.addAction(cameraAction)
//
//        let cancelAction = UIAlertAction(title: "Bỏ qua", style: .cancel, handler: nil)
//        alert.addAction(cancelAction)
//
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {}
//}

extension BaseViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UINavigationController: UIGestureRecognizerDelegate {
    open override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}




