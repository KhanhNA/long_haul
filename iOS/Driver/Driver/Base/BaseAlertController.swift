//
//  BaseAlertController.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import SDCAlertView

class BaseAlertController: NSObject {
    static func show(title: String?,
                     confirmTitle: String?,
                     confirmAction: (() -> ())?,
                     cancelTitle: String?,
                     cancelAction: (() -> ())?) {
        let alertController = AlertController(title: title, message: nil, preferredStyle: .alert)

        if confirmTitle != nil && confirmTitle?.isEmpty == false {
            let confirmAlertAction = AlertAction(title: confirmTitle, style: .preferred) { (_) in confirmAction?() }
            alertController.addAction(confirmAlertAction)
        }

        let cancelAlertAction = AlertAction(title: cancelTitle, style: .preferred) { (_) in cancelAction?() }
        alertController.addAction(cancelAlertAction)

        alertController.present()
    }
    
    static func showAndDontAllowDismiss(message: String?,
                                        title: String?,
                                        action: (() -> ())?) -> AlertController {
        let alertController = AlertController(title: message, message: nil, preferredStyle: .alert)
        
        let cancelAlertAction = AlertAction(title: title, style: .preferred)
        alertController.addAction(cancelAlertAction)
        
        alertController.shouldDismissHandler = { _ in
            action?()
            return false
        }
        
        alertController.present()
        
        return alertController
    }
}
