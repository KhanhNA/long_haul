//
//  BaseViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewModel: NSObject {
    let disposeBag = DisposeBag()
    
    // show hide empty on table view
    var isShowEmpty = BehaviorRelay<Bool>(value: false)
    
    // xử lý api lỗi
    let apiError = BehaviorRelay<Error?>(value: nil)
    let errorMessage = BehaviorRelay<String?>(value: nil)
    let isShowLoading = BehaviorRelay<Bool>(value: false)
    
    // xử lý load more
    var isLoadMore = BehaviorRelay<Bool>(value: false)
    var offset = 0
    let limit = 10
}
