//
//  ConfirmDialogView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ConfirmDialogView: BaseDialogView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yes2Button: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    
    var title: String!
    var yesTitle: String!
    var noTitle: String!
    var yesAction: (() -> ())?
    var noAction: (() -> ())?
    
    static func show(title: String,
                     yesTitle: String,
                     noTitle: String,
                     yesAction: (() -> ())?,
                     noAction: (() -> ())?) {
        let view = ConfirmDialogView()
        view.show {
            view.title = title
            view.yesTitle = yesTitle
            view.noTitle = noTitle
            view.yesAction = yesAction
            view.noAction = noAction
            
            view.titleLabel.text = view.title
            view.yesButton.setTitle(view.yesTitle, for: .normal)
            view.yes2Button.setTitle(view.yesTitle, for: .normal)
            
            if noTitle.isEmpty {
                view.yes2Button.isHidden = false
                
                view.noButton.superview?.isHidden = true
            } else {
                view.yes2Button.isHidden = true
                
                view.noButton.superview?.isHidden = false
                view.noButton.setTitle(view.noTitle, for: .normal)
            }
        }
    }
    
    @IBAction func yesAction(_ sender: Any) {
        yesAction?()
        dismiss()
    }
    
    @IBAction func noAction(_ sender: Any) {
        noAction?()
        dismiss()
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss()
    }
}
