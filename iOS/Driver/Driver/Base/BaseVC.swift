//
//  BaseVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class BaseVC: UIViewController {
    
    // show hide empty on table view
    var isShowEmpty = false
    
    // xử lý pull to refresh
    var refreshControl: UIRefreshControl?
    private var refreshControlAction: (() -> ())?
    
    var mainCoordinator: MainCoordinator?
    
    var isShowNavigationSeparator = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addSeparatorView()
        if let separatorView = navigationController?.navigationBar.viewWithTag(100) {
            separatorView.isHidden = !isShowNavigationSeparator
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let separatorView = navigationController?.navigationBar.viewWithTag(100) {
            separatorView.isHidden = true
        }
    }
}

// navigationController
extension BaseVC {
    func initMainCoordinator() {
        mainCoordinator = MainCoordinator(navigationController: navigationController)
    }
    
    func addSeparatorView() {
        guard navigationController?.navigationBar.viewWithTag(100) == nil else { return }
        
        let separatorView = UIView()
        separatorView.tag = 100
        separatorView.backgroundColor = AppColor.hex919395.withAlphaComponent(0.05)
        navigationController?.navigationBar.addSubview(separatorView)
        
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        if let navigationBar = navigationController?.navigationBar {
            separatorView.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor).isActive = true
            separatorView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor).isActive = true
            separatorView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
            separatorView.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
        }
        
        separatorView.isHidden = !isShowNavigationSeparator
    }
    
    func setupNavigationController(title: String, barTintColor: UIColor = .white) {
        navigationItem.backBarButtonItem = UIBarButtonItem()
        
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = AppColor.hex151522
        
        navigationItem.title = title
    }
    
    func setupCustomNavigationController(title: String, subtitle: String) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = .boldSystemFont(ofSize: 18)
        
        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.textColor = AppColor.hex919395
        subtitleLabel.textAlignment = .center
        subtitleLabel.font = .systemFont(ofSize: 14)
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.axis = .vertical
        
        navigationItem.titleView = stackView
    }
}

// pull to refresh
extension BaseVC {
    func addRefreshControl(for tableView: UITableView, action: (() -> ())?) {
        self.refreshControlAction = action
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @objc private func handleRefreshControl() {
        refreshControlAction?()
    }
}

// empty on table view
extension BaseVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func setEmptyDataSet(for tableView: UITableView) {
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        if !isShowEmpty {
            return "".attributedString
        }
        
        let font = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
        return "Không có dữ liệu".localized().attributedString.add(font)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        if !isShowEmpty {
            return UIImage()
        }
        
        return UIImage(named: "home_vc_folder")
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -25
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
