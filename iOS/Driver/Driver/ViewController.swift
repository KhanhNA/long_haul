//
//  ViewController.swift
//  Driver
//
//  Created by dong luong on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(startViewPager), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (navigationController?.topViewController != self) {
            navigationController?.navigationBar.isHidden = false
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        super.viewWillAppear(animated)
    }
    
    
    @objc func startViewPager() {
        let player = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
        if player == nil {
            let vc = RootViewController(nibName: RootViewController.className, bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
//            let mainViewController = CreatNewOrderViewController(nibName: CreatNewOrderViewController.className, bundle: nil)
//            let leftViewController = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
//            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//            let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
//            UIApplication.shared.delegate?.window??.rootViewController = slideMenuController
            
            goToTabBarController()
            
        }
        
    }
    
    private func goToTabBarController() {
        let tabBarStoryboard = UIStoryboard(name: TabBarController.className, bundle: nil)
        let tabBarController = tabBarStoryboard.instantiateViewController(withIdentifier: TabBarController.className)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = tabBarController
        appDelegate?.window?.makeKeyAndVisible()
    }
}

