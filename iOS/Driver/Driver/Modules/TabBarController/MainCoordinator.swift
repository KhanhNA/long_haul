//
//  MainCoordinator.swift
//  Driver
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject {
    var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
}

extension MainCoordinator {
    func goToInputOtpVC() {
        let viewController = InputOtpVC(nibName: InputOtpVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToConfirmOtpVC() {
        let viewController = ConfirmOtpVC(nibName: ConfirmOtpVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MainCoordinator {
    func goToDetailVC(id: Int, from homeType: HomeType?) {
        let viewController = DetailVC(nibName: DetailVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        viewController.id = id
        viewController.fromHomeType = homeType
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToScannerVC(totalQrArray: [String],
                       existQrArray: [String],
                       completion: ((_ code: String) -> ())?) {
        let viewController = ScannerVC(nibName: ScannerVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        // các qr hợp lệ (khi quét mã có kết quả phải check contains trong list này)
        viewController.totalQrArray = totalQrArray
        // các qr đã được thêm (nếu contains thì báo lỗi)
        viewController.existQrArray = existQrArray
        viewController.completion = completion
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToVanMapsVC(getListBiddingOrderShippingRecord: GetListBiddingOrderShippingRecord?,
                       fromHomeType: HomeType) {
        let viewController = VanMapsVC(nibName: VanMapsVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        viewController.getListBiddingOrderShippingRecord = getListBiddingOrderShippingRecord
        viewController.fromHomeType = fromHomeType
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToSettingsVC() {
        let viewController = SettingsVC(nibName: SettingsVC.className, bundle: nil)
        viewController.hidesBottomBarWhenPushed = true
        
        viewController.mainCoordinator = self
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}
