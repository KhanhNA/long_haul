//
//  HistoryVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets

class HistoryVC: BaseVC {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    
    private let viewModel = HistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        setupNavigationController()
        
        setupTableView()
        handleSubscribe()
        
        viewModel.addDefaultDateAndCallApi()
    }
    
    private func handleSubscribe() {
        
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi có dữ liệu trả về, cập nhật lại danh sách
        viewModel.output.getListBiddingOrderShippingRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // khi thay đổi date
        viewModel.input.fromDate.subscribe(onNext: { [weak self] (value) in
            
            let format = "dd/MM/yyyy"
            let title = (value == nil ? "Từ ngày".localized() : value?.toString(with: format))
            self?.fromLabel.text = title
            
        }).disposed(by: viewModel.disposeBag)
        
        viewModel.input.toDate.subscribe(onNext: { [weak self] (value) in
            
            let format = "dd/MM/yyyy"
            let title = (value == nil ? "Từ ngày".localized() : value?.toString(with: format))
            self?.toLabel.text = title
            
        }).disposed(by: viewModel.disposeBag)
        
        // cập nhật dữ liệu textSearch từ searchTextField
        searchTextField.rx.text.bind(to: viewModel.input.textSearch)
            .disposed(by: viewModel.disposeBag)
        
        searchTextField.rx.controlEvent([.editingDidEnd]).asObservable().subscribe(onNext: { [weak self] (_) in
            
            guard let self = self,
                self.viewModel.local.oldTextSearch != self.viewModel.input.textSearch.value
                else { return }
            
            self.viewModel.reloadData(isShowLoading: true)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func fromAction(_ sender: Any) {
        HistoryDateView.show(minimumDate: nil, maximumDate: viewModel.input.toDate.value, date: viewModel.input.fromDate.value, didSelect: { [weak self] (date) in
            self?.viewModel.input.fromDate.accept(date)
            }, dismissClosure: { [weak self] in
                self?.viewModel.reloadData()
        })
    }
    
    @IBAction func toAction(_ sender: Any) {
        HistoryDateView.show(minimumDate: viewModel.input.fromDate.value,
                             maximumDate: nil,
                             date: viewModel.input.toDate.value,
                             didSelect: { [weak self] (date) in
                                self?.viewModel.input.toDate.accept(date)
            }, dismissClosure: { [weak self] in
                self?.viewModel.reloadData()
        })
    }
}

extension HistoryVC {
    private func setupNavigationController() {
        setupNavigationController(title: "Lịch sử vận chuyển".localized())
    }
}

extension HistoryVC: UITableViewDataSource, UITableViewDelegate {
    private func setupTableView() {
        tableView.register(headerNibName: HomeTableViewHeaderFooterView.className)
        tableView.register(cellNibName: HomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        tableView.tableHeaderView = headerView
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.output.getListBiddingOrderShippingRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.getListBiddingOrderShippingRecords.value[section].count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableViewHeaderFooterView.className)
        if let headerView = headerView as? HomeTableViewHeaderFooterView {
            
            let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
            let getListBiddingOrderShippingRecord = getListBiddingOrderShippingRecords[section].first
            headerView.titleLabel.text = getListBiddingOrderShippingRecord?.getCode()
            
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
        let getListBiddingOrderShippingRecord = (getListBiddingOrderShippingRecords[indexPath.section])[indexPath.row]
        
        if indexPath.section == getListBiddingOrderShippingRecords.count - 1
            && indexPath.row == getListBiddingOrderShippingRecords[indexPath.section].count - 1
            && getListBiddingOrderShippingRecord.isLoadMore {
            
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(HomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getListBiddingOrderShippingRecord: getListBiddingOrderShippingRecord,
                   homeType: .return)
        
        cell.showReason = {
            ReasonView.show(title: "Lý do hủy".localized(),
                            detail: getListBiddingOrderShippingRecord.note ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
        let getListBiddingOrderShippingRecord = (getListBiddingOrderShippingRecords[indexPath.section])[indexPath.row]
        
        guard let id = getListBiddingOrderShippingRecord.id else { return }
        mainCoordinator?.goToDetailVC(id: id, from: .return)
    }
}
