//
//  HistoryApi.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import Alamofire

class HistoryApi: NSObject {
    // Lịch sử vận chuyển
    typealias GetListBiddingOrderHistoryResult = BaseMappableResultModel<GetListBiddingOrderShippingRecord>
    typealias GetListBiddingOrderHistoryResponseCompletion = BaseMappableResponseModel<GetListBiddingOrderHistoryResult>
    typealias GetListBiddingOrderHistoryCompletion = (GetListBiddingOrderHistoryResponseCompletion?, Error?) -> ()
    
    static func getListBiddingOrderHistory(parameter: GetHistoryBiddingRequest,
                                           completion: @escaping GetListBiddingOrderHistoryCompletion) {
        /*
         # order by = 1 : Sắp xếp theo giá tăng giần
         # order by = 2 : Sắp xếp theo giá giảm giần
         # order by = 3 : Sắp xếp theo quãng đường tăng dần
         # order by = 4 : Sắp xếp theo quãng đường giảm dần
         # order by = 5 : Sắp xếp đơn mới nhất
         # order by = 6 : Sắp xếp đơn cũ nhất
         */
        BaseClient.shared.requestAPIWithMappable(apiURL.getListBiddingOrderHistory.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": parameter.getJson()],
                                                 headers: nil,
                                                 completion: completion)
    }
}
