//
//  HistoryDateView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class HistoryDateView: BaseDialogView {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var didSelect: ((_ date: Date) -> ())?
    var dismissClosure: (() -> ())?
    
    static func show(minimumDate: Date?,
                     maximumDate: Date?,
                     date: Date?,
                     didSelect: ((_ date: Date) -> ())?,
                     dismissClosure: (() -> ())?) {
        let view = HistoryDateView()
        view.show {
            view.datePicker.minimumDate = minimumDate
            view.datePicker.maximumDate = maximumDate
            view.datePicker.date = date ?? Date()
            view.datePicker.datePickerMode = .date
            
            view.didSelect = didSelect
            view.dismissClosure = dismissClosure
            
            view.didSelect?(date ?? Date())
        }
    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        didSelect?(datePicker.date)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismissClosure?()
        dismiss()
    }
}
