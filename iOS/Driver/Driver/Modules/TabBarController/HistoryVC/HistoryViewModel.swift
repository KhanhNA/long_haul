//
//  HistoryViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/8/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HistoryViewModel: BaseViewModel {
    
    struct Input {
        let fromDate = BehaviorRelay<Date?>(value: nil)
        let toDate = BehaviorRelay<Date?>(value: nil)
        let textSearch = BehaviorRelay<String?>(value: nil)
    }
    
    struct Output {
        // danh sách bidding trên table view
        let getListBiddingOrderShippingRecords = BehaviorRelay<[[GetListBiddingOrderShippingRecord]]>(value: [])
    }
    
    struct Local {
        var isCallingApi = false
        var oldTextSearch: String?
    }
    
    // ---------------------------- //
    
    private (set) var input  = Input()
    private (set) var output = Output()
    private (set) var local  = Local()
    
    // ---------------------------- //
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    func addDefaultDateAndCallApi() {
        let now = Date()
        input.toDate.accept(now)
        input.fromDate.accept(now.adding(days: -3))
        reloadData()
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        guard let _ = input.fromDate.value, let _ = input.toDate.value else { return }
        
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        isShowEmpty.accept(false)
        
        offset = 0
        callApi()
    }
    
    func callApi() {
        guard let _ = input.fromDate.value, let _ = input.toDate.value else { return }
        
        if local.isCallingApi {
            return
        }
        
        local.isCallingApi = true
        
        let parameter = GetHistoryBiddingRequest()
        parameter.offset = offset
        parameter.limit = limit
        parameter.fromDate = input.fromDate.value
        parameter.toDate = input.toDate.value
        parameter.txtSearch = input.textSearch.value ?? ""
        
        HistoryApi.getListBiddingOrderHistory(parameter: parameter) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.local.oldTextSearch = parameter.txtSearch
            
            self.local.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.output.getListBiddingOrderShippingRecords.value : [])
            if oldRecords.count > 0 {
                oldRecords[oldRecords.count - 1].removeAll(where: { $0.isLoadMore })
            }
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.output.getListBiddingOrderShippingRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.arrayRecords else {
                
                self.output.getListBiddingOrderShippingRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += 1
                
                if oldRecords.count > 0 {
                    let fakeData = GetListBiddingOrderShippingRecord(isLoadMore: true)
                    oldRecords[oldRecords.count - 1].append(fakeData)
                }
            }
            
            self.output.getListBiddingOrderShippingRecords.accept(oldRecords)
        }
    }
}
