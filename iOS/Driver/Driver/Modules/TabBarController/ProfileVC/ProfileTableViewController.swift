//
//  ProfileTableViewController.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/18/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import Toast
import KRProgressHUD

class ProfileTableViewController: UITableViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var logoutLabel: UILabel!
    
    var mainCoordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        
        navigationItem.backBarButtonItem = UIBarButtonItem()
        
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = AppColor.hex151522
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        settingsLabel.text = "Cài đặt".localized()
        helpLabel.text = "Trung tâm trợ giúp".localized()
        logoutLabel.text = "Đăng xuất".localized()
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            // profile
            break

        case 1:
            // Cài đặt
            mainCoordinator?.goToSettingsVC()
            
        case 2:
            // Trung tâm trợ giúp
            ConfirmDialogView.show(title: "Vui lòng goị 19008198 để được trợc giúp!".localized(),
                                   yesTitle: "Gọi".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { }, noAction: { })
            
        case 3:
            // Đăng xuất
            ConfirmDialogView.show(title: "Bạn muốn đăng xuất?".localized(),
                                   yesTitle: "Đăng xuất".localized(),
                                   noTitle: "Huỷ bỏ".localized(),
                                   yesAction: { [weak self] in
                                    
                                    self?.logout { [weak self] (data, error) in
                                        if let error = error {
                                            print(error.localizedDescription)
                                            self?.showMessage(error.localizedDescription)
                                            return
                                        }
                                        
                                        self?.removeAll()
                                    }
                }, noAction: { })
            
        default:
            break
        }
    }
    
    func initMainCoordinator() {
        mainCoordinator = MainCoordinator(navigationController: navigationController)
    }
    
    func showMessage(_ message: String) {
        view.makeToast(message,
                       duration: 1,
                       position: CSToastPositionCenter,
                       style: nil)
    }
    
    func removeAll() {
        UserDefaults.standard.save(customObject: ResultResponceLogin(), inKey: "loginModel")
        
        let rootViewController = RootViewController(nibName: RootViewController.className,
                                                    bundle: nil)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = rootViewController
        appDelegate?.window?.makeKeyAndVisible()
    }
    
    func logout(completion: @escaping (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()) {
        BaseClient.shared.requestAPIWithMappable(apiURL.logout.url,
                                                 method: .post,
                                                 parameters: nil,
                                                 headers: nil,
                                                 completion: completion)
    }
}
