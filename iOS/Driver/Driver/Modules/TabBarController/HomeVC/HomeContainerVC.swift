//
//  HomeContainerVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import CarbonKit

class HomeContainerVC: BaseVC {
    private var controllerArray: [HomeVC] = []
    private let homeVC2 = HomeVC(nibName: HomeVC.className, bundle: nil) // Chưa vận chuyển
    private let homeVC3 = HomeVC(nibName: HomeVC.className, bundle: nil) // Đang vận chuyển
    private let homeVC4 = HomeVC(nibName: HomeVC.className, bundle: nil) // Trả hàng
    
    private var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMainCoordinator()
        setupNavigationController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupPageMenu()
    }
    
    // xoá tất cả dữ liệu
    // và gọi lại api
    func reloadData() {
        controllerArray.forEach({ $0.viewModel.reloadData() })
    }
}

extension HomeContainerVC {
    private func setupNavigationController() {
        setupNavigationController(title: "Đơn vận chuyển".localized())
    }
}

extension HomeContainerVC {
    private func setupPageMenu() {
        guard isFirstLoad else { return }
        isFirstLoad = false
        
        setupControllerArray()
        
        let items = [
            HomeType.notShipping.getTitle(),    // Chưa vận chuyển
            HomeType.shipping.getTitle(),       // Đang vận chuyển
            HomeType.return.getTitle()          // Trả hàng
        ]
        
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.toolbar.barTintColor = .white
        carbonTabSwipeNavigation.toolbar.clipsToBounds = true
        carbonTabSwipeNavigation.currentTabIndex = 0
        
        carbonTabSwipeNavigation.setIndicatorColor(AppColor.hex00A359)
        carbonTabSwipeNavigation.setSelectedColor(AppColor.hex00A359, font: .boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setNormalColor(AppColor.hex3A3E41, font: .boldSystemFont(ofSize: 14))
        
        carbonTabSwipeNavigation.setTabExtraWidth(10)
        carbonTabSwipeNavigation.setTabBarHeight(40)
    }
    
    private func setupControllerArray() {
        homeVC2.mainCoordinator = mainCoordinator
        homeVC3.mainCoordinator = mainCoordinator
        homeVC4.mainCoordinator = mainCoordinator
        
        homeVC2.homeType = .notShipping
        homeVC3.homeType = .shipping
        homeVC4.homeType = .return
        
        controllerArray.append(contentsOf: [
            homeVC2,
            homeVC3,
            homeVC4
        ])
    }
}

extension HomeContainerVC: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation,
                                  viewControllerAt index: UInt) -> UIViewController {
        return controllerArray[Int(index)]
    }
}
