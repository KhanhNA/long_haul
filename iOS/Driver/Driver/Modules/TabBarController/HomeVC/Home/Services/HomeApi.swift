//
//  HomeApi.swift
//  Driver
//
//  Created by Hieu Dinh on 8/4/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import Alamofire

class HomeApi {
    // Danh sách đơn hàng
    typealias GetListBiddingOrderShippingResult = BaseMappableResultModel<GetListBiddingOrderShippingRecord>
    typealias GetListBiddingOrderShippingResponseCompletion = BaseMappableResponseModel<GetListBiddingOrderShippingResult>
    typealias GetListBiddingOrderShippingCompletion = (GetListBiddingOrderShippingResponseCompletion?, Error?) -> ()
    
    static func getListBiddingOrderShipping(parameter: GetListBiddingOrderShippingParameter,
                                            completion: @escaping GetListBiddingOrderShippingCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getListBiddingOrderShipping.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": parameter.toJSON()],
                                                 headers: nil,
                                                 completion: completion)
        
    }
}
