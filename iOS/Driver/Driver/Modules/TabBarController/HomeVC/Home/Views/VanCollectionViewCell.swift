//
//  VanCollectionViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class VanCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var idLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        idLabel.superview?.cornerRadius = 8
    }
    
    func setSelected(_ isSelected: Bool, text: String) {
        idLabel.text = text
        
        if isSelected {
            idLabel.superview?.backgroundColor = AppColor.hexCCF0E0
            idLabel.textColor = AppColor.hex00A359
            idLabel.font = .systemFont(ofSize: 14)
        } else {
            idLabel.superview?.backgroundColor = .clear
            idLabel.textColor = AppColor.hex3A3E41
            idLabel.font = .boldSystemFont(ofSize: 14)
        }
    }
}
