//
//  HomeTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var totalWeightLabel: UILabel!
    
    @IBOutlet weak var fromDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReceiveTimeToReceiveTimeLabel: UILabel!
    @IBOutlet weak var toDepotAddressLabel: UILabel!
    @IBOutlet weak var fromReturnTimeToReturnTimeLabel: UILabel!
    
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var reasonButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var showReason: (() -> ())?
    var cargoTypes: [CargoType]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: VanCollectionViewCell.className)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 100, height: 25)
        layout.scrollDirection = .horizontal
        
        collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(getListBiddingOrderShippingRecord: GetListBiddingOrderShippingRecord,
               homeType: HomeType) {
        
        cargoTypes = getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes ?? []
        
        distanceLabel.text = getListBiddingOrderShippingRecord.getDistance()
        quantityLabel.text = getListBiddingOrderShippingRecord.getTotalCargo()
        totalWeightLabel.text = getListBiddingOrderShippingRecord.getTotalWeight()
        
        fromDepotAddressLabel.text = getListBiddingOrderShippingRecord.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getListBiddingOrderShippingRecord.getFromReceiveTimeToReceiveTime()
        
        toDepotAddressLabel.text = getListBiddingOrderShippingRecord.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getListBiddingOrderShippingRecord.getFromReturnTimeToReturnTime()
        
        let id = NSMutableAttributedString()
        id.append(getListBiddingOrderShippingRecord.getId()
            .attributedString
            .color(AppColor.hex24282C))
        
        idLabel.attributedText = id
        
        if homeType == .notShipping {

            collectionView.superview?.isHidden = true
            collectionView.isHidden = true
            reasonButton.isHidden = true
            
        } else if homeType == .shipping {
            
            collectionView.superview?.isHidden = (cargoTypes?.isEmpty == true)
            reasonButton.isHidden = (cargoTypes?.isEmpty == true)
            reasonButton.isHidden = true
            
            collectionView.reloadData()
            
        } else if homeType == .return {
            if getListBiddingOrderShippingRecord.isCancel {
            
                collectionView.superview?.isHidden = false
                collectionView.isHidden = true
                reasonButton.isHidden = false
                
                reasonButton.setTitle("Lý do bị hủy".localized(), for: .normal)
                
                id.append(" - ".attributedString.color(AppColor.hex24282C))
                id.append("Huỷ".localized().attributedString.color(AppColor.hexFF0C20))
                idLabel.attributedText = id
                
            } else {
                
                collectionView.superview?.isHidden = (cargoTypes?.isEmpty == true)
                collectionView.isHidden = (cargoTypes?.isEmpty == true)
                reasonButton.isHidden = true
                
                collectionView.reloadData()
                
                id.append(" - ".attributedString.color(AppColor.hex24282C))
                id.append("Thành công".localized().attributedString.color(AppColor.hex289767))
                idLabel.attributedText = id
            }
        }
    }
    
    @IBAction func showReasonAction(_ sender: Any) {
        showReason?()
    }
}

extension HomeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cargoTypes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(VanCollectionViewCell.self, indexPath) else {
            return UICollectionViewCell()
        }

        if let cargoTypes = cargoTypes {
            let item = cargoTypes[indexPath.item]
            cell.idLabel.text = String(format: "%@ %@",
                                       String(item.cargoQuantity ?? 0),
                                       String(item.type ?? ""))
        } else {
            cell.idLabel.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
