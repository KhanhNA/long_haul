//
//  HomeTableViewHeaderFooterView.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class HomeTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
}
