//
//  HomeVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets

enum HomeType: Int {
    case notShipping = 0
    case shipping = 1
    case `return` = 2
    
    func getTitle() -> String {
        switch self {
        case .notShipping:
            return "Chưa vận chuyển".localized()
        case .shipping:
            return "Đang vận chuyển".localized()
        case .return:
            return "Trả hàng".localized()
        }
    }
}

class HomeVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    let viewModel = HomeViewModel()
    var homeType = HomeType.notShipping
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleSubscribe()
        
        setupViews()
        setupTableView()
        
        viewModel.input.homeType.accept(homeType)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad {
            isFirstLoad = false
            viewModel.callApi()
        }
    }
    
    private func setupViews() {
        searchTextField.placeholder = "Tìm kiếm địa chỉ".localized()
    }
}

extension HomeVC {
    private func handleSubscribe() {
        handleViewModelInput()
        handleViewModelOutput()
    }
    
    private func handleViewModelInput() {
        // cập nhật dữ liệu textSearch từ searchTextField
        searchTextField.rx.text.bind(to: viewModel.input.textSearch)
            .disposed(by: viewModel.disposeBag)
        
        searchTextField.rx.controlEvent([.editingDidEnd]).asObservable().subscribe(onNext: { [weak self] (_) in
            
            guard let self = self,
                self.viewModel.local.oldTextSearch != self.viewModel.input.textSearch.value
                else { return }
            
            self.viewModel.reloadData(isShowLoading: true)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    private func handleViewModelOutput() {
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.getListBiddingOrderShippingRecords.skip(1).subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.refreshControl?.endRefreshing()
            self.isShowEmpty = true
            self.tableView.reloadData()
            
        }).disposed(by: viewModel.disposeBag)
    }
}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    private func setupTableView() {
        tableView.register(headerNibName: HomeTableViewHeaderFooterView.className)
        tableView.register(cellNibName: HomeTableViewCell.className)
        tableView.register(cellNibName: LoadMoreTableViewCell.className)
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsets(top: 9, left: 0, bottom: 0, right: 0)
        
        setEmptyDataSet(for: tableView)
        
        addRefreshControl(for: tableView) { [weak self] in
            self?.viewModel.reloadData(isShowLoading: false)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.output.getListBiddingOrderShippingRecords.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.getListBiddingOrderShippingRecords.value[section].count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableViewHeaderFooterView.className)
        if let headerView = headerView as? HomeTableViewHeaderFooterView {
            
            let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
            let getListBiddingOrderShippingRecord = getListBiddingOrderShippingRecords[section].first
            headerView.titleLabel.text = getListBiddingOrderShippingRecord?.getCode()
            
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
        let getListBiddingOrderShippingRecord = (getListBiddingOrderShippingRecords[indexPath.section])[indexPath.row]
        
        if indexPath.section == getListBiddingOrderShippingRecords.count - 1
            && indexPath.row == getListBiddingOrderShippingRecords[indexPath.section].count - 1
            && getListBiddingOrderShippingRecord.isLoadMore {
            
            guard let cell = tableView.dequeue(LoadMoreTableViewCell.self, indexPath) else {
                return UITableViewCell()
            }
            
            cell.start()
            
            if !viewModel.isLoadMore.value {
                viewModel.isLoadMore.accept(true)
            }
            
            return cell
        }
        
        guard let cell = tableView.dequeue(HomeTableViewCell.self, indexPath) else {
            return UITableViewCell()
        }
        
        cell.setup(getListBiddingOrderShippingRecord: getListBiddingOrderShippingRecord,
                   homeType: homeType)
        
        cell.showReason = {
            ReasonView.show(title: "Lý do hủy".localized(),
                            detail: getListBiddingOrderShippingRecord.note ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let getListBiddingOrderShippingRecords = viewModel.output.getListBiddingOrderShippingRecords.value
        let getListBiddingOrderShippingRecord = (getListBiddingOrderShippingRecords[indexPath.section])[indexPath.row]
        
        guard let id = getListBiddingOrderShippingRecord.id else { return }
        mainCoordinator?.goToDetailVC(id: id, from: homeType)
    }
}
