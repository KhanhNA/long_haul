//
//  GetListBiddingOrderShippingParameter.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetListBiddingOrderShippingParameter: NSObject, Mappable {
    var offset: Int?
    var limit: Int?
    var status: Int?
    var txtSearch: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        offset <- map["offset"]
        limit <- map["limit"]
        status <- map["status"]
        txtSearch <- map["txt_search"]
    }
}
