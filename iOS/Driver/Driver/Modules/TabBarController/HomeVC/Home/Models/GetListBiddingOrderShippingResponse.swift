//
//  GetListBiddingOrderShippingResponse.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetListBiddingOrderShippingRecord: NSObject, Mappable {
    var id: Int?
    var companyID: Int?
    var biddingOrderNumber: String?
    var fromDepot: Depot?
    var toDepot: Depot?
    var totalWeight: Int?
    var totalCargo: Int?
    var price: Double?
    var currencyName: String?
    var distance: Double?
    var type: String?
    var status: String?
    var note: String?
    
    private var _createDate: String?
    var createDate: Date?
    
    var writeDate: String?
    var biddingPackageID: Int?
    
    private var _fromReceiveTime: String?
    private var _toReceiveTime: String?
    private var _fromReturnTime: String?
    private var _toReturnTime: String?
    var fromReceiveTime: Date?
    var toReceiveTime: Date?
    var fromReturnTime: Date?
    var toReturnTime: Date?
    
    private var _maxConfirmTime: String?
    var maxConfirmTime: Date?
    
    var biddingVehicles: BiddingVehicle?
    
    // tất cả qr từ server trả về
    var qrCargoType: [CargoType]?
    
    var isLoadMore = false
    
    var isCancel: Bool {
        return biddingVehicles?.biddingOrderReceive?.status == "-1"
    }
    
    var homeType: HomeType? {
        if biddingVehicles?.biddingOrderReturn?.status == "2"
            || biddingVehicles?.biddingOrderReturn?.status == "-1" {
            
            // trả hàng
            return .return
        }
     
        if biddingVehicles?.biddingOrderReceive?.status == "1" {
            
            // đang vận chuyển
            return .shipping
        }
        
        if biddingVehicles?.biddingOrderReceive?.status == "0" {
            
            // chưa vận chuyển
            return .notShipping
        }
        
        return nil
    }
    
    init(isLoadMore: Bool) {
        self.isLoadMore = isLoadMore
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        companyID <- map["company_id"]
        biddingOrderNumber <- map["bidding_order_number"]
        fromDepot <- map["from_depot"]
        toDepot <- map["to_depot"]
        totalWeight <- map["total_weight"]
        totalCargo <- map["total_cargo"]
        price <- map["price"]
        currencyName <- map["currency_name"]
        distance <- map["distance"]
        type <- map["type"]
        status <- map["status"]
        note <- map["note"]
        
        _createDate <- map["create_date"]
        // convert về date (+ múi giờ)
        if let _createDate = _createDate {
            createDate = Date.UTCToLocal(date: _createDate)?.toDate()
        }
        
        writeDate <- map["write_date"]
        biddingPackageID <- map["bidding_package_id"]
        
        _fromReceiveTime <- map["from_receive_time"]
        _toReceiveTime <- map["to_receive_time"]
        _fromReturnTime <- map["from_return_time"]
        _toReturnTime <- map["to_return_time"]
        
        // convert về date (+ múi giờ)
        if let _fromReceiveTime = _fromReceiveTime {
            fromReceiveTime = Date.UTCToLocal(date: _fromReceiveTime)?.toDate()
        }
        if let _toReceiveTime = _toReceiveTime {
            toReceiveTime = Date.UTCToLocal(date: _toReceiveTime)?.toDate()
        }
        if let _fromReturnTime = _fromReturnTime {
            fromReturnTime = Date.UTCToLocal(date: _fromReturnTime)?.toDate()
        }
        if let _toReturnTime = _toReturnTime {
            toReturnTime = Date.UTCToLocal(date: _toReturnTime)?.toDate()
        }
        
        _maxConfirmTime <- map["max_confirm_time"]
        if let _maxConfirmTime = _maxConfirmTime {
            maxConfirmTime = Date.UTCToLocal(date: _maxConfirmTime)?.toDate()
        }
        
        biddingVehicles <- map["bidding_vehicles"]
        
        qrCargoType <- map["cargo_type"]
    }
    
    func getCode() -> String {
        var title = ""
        
        if let depotCode = fromDepot?.depotCode, !depotCode.isEmpty {
            title = depotCode
        }
        
        if let depotCode = toDepot?.depotCode, !depotCode.isEmpty {
            if !title.isEmpty {
                title += " - "
            }
            title += depotCode
        }
        
        return title
    }
    
    func getFromReceiveTimeToReceiveTime() -> String {
        let fromReceiveTimeString = fromReceiveTime?.toString(with: Global.localFormat)
        let toReceiveTimeString = toReceiveTime?.toString(with: Global.localFormat)
        
        if fromReceiveTimeString == nil || fromReceiveTimeString?.isEmpty == true {
            return toReceiveTimeString ?? ""
        }
        
        if toReceiveTimeString == nil || toReceiveTimeString?.isEmpty == true {
            return fromReceiveTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReceiveTimeString ?? "",
                      toReceiveTimeString ?? "")
    }
    
    func getFromReturnTimeToReturnTime() -> String {
        let fromReturnTimeString = fromReturnTime?.toString(with: Global.localFormat)
        let toReturnTimeString = toReturnTime?.toString(with: Global.localFormat)
        
        if fromReturnTimeString == nil || fromReturnTimeString?.isEmpty == true {
            return toReturnTimeString ?? ""
        }
        
        if toReturnTimeString == nil || toReturnTimeString?.isEmpty == true {
            return fromReturnTimeString ?? ""
        }
        
        return String(format: "%@ - %@",
                      fromReturnTimeString ?? "",
                      toReturnTimeString ?? "")
    }
    
    func getPrice(currency: String = " VND") -> String {
        return String(format: "%@%@", Utils.formatMoney(number: price ?? 0), currency)
    }
    
    func getDistance() -> String {
        return String(format: "%@ km", Utils.formatNumber(number: distance ?? 0))
    }
    
    func getId() -> String {
        return String(format: "ID: %@", biddingOrderNumber ?? "")
    }
    
    func getTotalCargo() -> String {
        return String(format: "%@ cargo", String(totalCargo ?? 0))
    }
    
    func getTotalWeight() -> String {
        return String(format: "%@ kg", String(totalWeight ?? 0))
    }
    
    func getCreateDate() -> String {
        let createDateString = createDate?.toString(with: Global.localFormat)
        return createDateString ?? ""
    }
    
    func getHMS() -> (hours: Int, minutes: Int, seconds: Int) {
        guard let endDate = maxConfirmTime else {
            return (0, 0, 0)
        }
        
        let startDate = Date()
        
        let differenceInSeconds = Int(endDate.timeIntervalSince(startDate))
        return Utils.secondsToHoursMinutesSeconds(seconds: differenceInSeconds)
    }
    
    func isTimeout() -> Bool {
        let (hours, minutes, seconds) = getHMS()
        return hours <= 0 && minutes <= 0 && seconds <= 0
    }
    
    func clone() -> GetListBiddingOrderShippingRecord? {
        return Mapper<GetListBiddingOrderShippingRecord>().map(JSON: toJSON())
    }
}


// MARK: - BiddingVehicle
class BiddingVehicle: NSObject, Mappable {
    var id: Int?
    var code: String?
    var lisencePlate: String?
    var driverPhoneNumber: String?
    var driverName: String?
    var expiryTime: String?
    var companyID: Int?
    var status: String?
    var biddingVehicleDescription: String?
    var vehicleType: Int?
    var weightUnit: String?
    var cargoTypes: [CargoType]? // các qr đã quét nhận hàng (từ server trả về)
    var biddingOrderReceive: BiddingOrder?
    var biddingOrderReturn: BiddingOrder?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        lisencePlate <- map["lisence_plate"]
        driverPhoneNumber <- map["driver_phone_number"]
        driverName <- map["driver_name"]
        expiryTime <- map["expiry_time"]
        companyID <- map["company_id"]
        status <- map["status"]
        biddingVehicleDescription <- map["description"]
        vehicleType <- map["vehicle_type"]
        weightUnit <- map["weight_unit"]
        cargoTypes <- map["cargo_types"]
        biddingOrderReceive <- map["bidding_order_receive"]
        biddingOrderReturn <- map["bidding_order_return"]
    }
}

// MARK: - BiddingOrderRe
class BiddingOrder: NSObject, Mappable {
    var id: Int?
    var biddingOrderID: Int?
    var fromExpectedTime: String?
    var toExpectedTime: String?
    var depotID: String?
    
    private var _actualTime: String?
    var actualTime: Date?
    
    var stockManID: String?
    var status: String?
    var biddingOrderReDescription: String?
    var createDate: String?
    var writeDate: String?
    var biddingOrderVehicleID: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        biddingOrderID <- map["bidding_order_id"]
        fromExpectedTime <- map["from_expected_time"]
        toExpectedTime <- map["to_expected_time"]
        depotID <- map["depot_id"]
        
        _actualTime <- map["max_confirm_time"]
        if let _actualTime = _actualTime {
            actualTime = Date.UTCToLocal(date: _actualTime)?.toDate()
        }
        
        stockManID <- map["stock_man_id"]
        status <- map["status"]
        biddingOrderReDescription <- map["description"]
        createDate <- map["create_date"]
        writeDate <- map["write_date"]
        biddingOrderVehicleID <- map["bidding_order_vehicle_id"]
    }
}

// MARK: - CargoType
class CargoType: NSObject, Mappable {
    var id: Int?
    var length: Int?
    var width: Int?
    var height: Int?
    var longUnitMoved0: String?
    var weightUnitMoved0: String?
    var type: String?
    var fromWeight: String?
    var toWeight: String?
    var priceID: String?
    var price: String?
    var sizeStandardSeq: String?
    var longUnit: String?
    var weightUnit: String?
    var cargoQuantity: Int?
    var totalWeight: Int?
    var cargos: [Cargo]?
    
    var detailModel: DetailModel? {
        let title = DetailTitleModel(imageNamed: "", title: "")
        
        var infoArray = [
            DetailInfoModel(text: "Số lượng cargo".localized(), value: getCargo()),
            DetailInfoModel(text: "Kích thước( L * H * W)".localized(), value: getLHW()),
            DetailInfoModel(text: "Số lượng cargo thực nhận".localized(), value: getQrString())
        ]
        
        if cargos?.isEmpty == false {
            infoArray.append(DetailInfoModel(text: "QR code:".localized()))
        }
        
        return DetailModel(title: title,
                           infoArray: infoArray,
                           isHiddenLineView: (cargos?.isEmpty == false))
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        longUnitMoved0 <- map["long_unit_moved0"]
        weightUnitMoved0 <- map["weight_unit_moved0"]
        type <- map["type"]
        fromWeight <- map["from_weight"]
        toWeight <- map["to_weight"]
        priceID <- map["price_id"]
        price <- map["price"]
        sizeStandardSeq <- map["size_standard_seq"]
        longUnit <- map["long_unit"]
        weightUnit <- map["weight_unit"]
        cargoQuantity <- map["cargo_quantity"]
        totalWeight <- map["total_weight"]
        cargos <- map["cargos"]
    }
    
    func getCargoQuantity() -> String {
        return String(format: "%@ %@", String(cargoQuantity ?? 0), "cargo".localized())
    }
    
    func getWeight() -> String {
        return String(format: "%@ %@", String(totalWeight ?? 0), "tấn".localized())
    }
    
    func getLHW() -> String {
        return String(format: "%@ x %@ x %@ m",
                      String(length ?? 0),
                      String(height ?? 0),
                      String(width ?? 0))
    }
    
    func getCargo() -> String {
        return String(format: "%@ %@", String(cargoQuantity ?? 0), "cargo".localized())
    }
    
    func getQrString() -> String {
        return String(format: "%@ %@", String(cargos?.count ?? 0), "cargo".localized())
    }
    
    func clone() -> CargoType? {
        return Mapper<CargoType>().map(JSON: toJSON())
    }
}

// MARK: - Depot
class Depot: NSObject, Mappable {
    var id: Int?
    var name: String?
    var depotCode: String?
    var address: String?
    var phone: String?
    var street: String?
    var street2: String?
    var cityName: String?
    var latitude: Double?
    var longitude: Double?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        depotCode <- map["depot_code"]
        address <- map["address"]
        phone <- map["phone"]
        street <- map["street"]
        street2 <- map["street2"]
        cityName <- map["city_name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}

// MARK: - Cargo
class Cargo: NSObject, Mappable {
    var id: Int?
    var cargoDescription: String?
    var fromLatitude: Double?
    var qrCode: String?
    var sizeStandard: String?
    var fromLongitude: Double?
    var weight: Int?
    var biddingPackageID: Int?
    var fromDepotID: Int?
    var price: Double?
    var cargoNumber: String?
    var toLongitude: Double?
    var toLatitude: Double?
    var toDepotID: Int?
    var distance: Double?
    var sizeID: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        cargoDescription <- map["description"]
        fromLatitude <- map["from_latitude"]
        qrCode <- map["qr_code"]
        sizeStandard <- map["size_standard"]
        fromLongitude <- map["from_longitude"]
        weight <- map["weight"]
        biddingPackageID <- map["bidding_package_id"]
        fromDepotID <- map["from_depot_id"]
        price <- map["price"]
        cargoNumber <- map["cargo_number"]
        toLongitude <- map["to_longitude"]
        toLatitude <- map["to_latitude"]
        toDepotID <- map["to_depot_id"]
        distance <- map["distance"]
        sizeID <- map["size_id"]
    }
    
    func clone() -> Cargo? {
        return Mapper<Cargo>().map(JSON: toJSON())
    }
}
