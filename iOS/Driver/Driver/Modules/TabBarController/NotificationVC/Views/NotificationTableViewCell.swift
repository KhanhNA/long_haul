//
//  NotificationTableViewCell.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/26/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(notificationRecord: NotificationRecord) {
        let backgroundColor: UIColor = (notificationRecord.isRead == true ? .white : AppColor.hexE7F4FF)
        containerView.backgroundColor = backgroundColor
        titleLabel.text = notificationRecord.title
        timeLabel.text = notificationRecord.getCreateDate()
        iconImageView.setImage(urlString: notificationRecord.image256, placeholder: nil)
    }
}
