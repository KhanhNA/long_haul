//
//  NotificationViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/26/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ObjectMapper

class NotificationViewModel: BaseViewModel {
    // Lịch sử thông báo
    let notificationRecords = BehaviorRelay<[NotificationRecord]>(value: [])
    
    private var isCallingApi = false
    
    override init() {
        super.init()
        
        isLoadMore.subscribe(onNext: { [weak self] (value) in
            guard let self = self, value else { return }
            
            self.callApi()
            
        }).disposed(by: disposeBag)
    }
    
    @objc func reloadData(isShowLoading: Bool = true) {
        if isShowLoading {
            self.isShowLoading.accept(true)
        }
        
        isShowEmpty.accept(false)
        
        offset = 0
        callApi()
    }
    
    func callApi() {
        
        if isCallingApi {
            return
        }
        
        isCallingApi = true
        
        NotificationApi.getBiddingNotificationHistory(offset: String(offset)) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isCallingApi = false
            
            self.isShowLoading.accept(false)
            
            self.isShowEmpty.accept(true)
            
            self.isLoadMore.accept(false)
            
            var oldRecords = (self.offset != 0 ? self.notificationRecords.value : [])
            oldRecords.removeAll(where: { $0.isLoadMore })
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                
                self.notificationRecords.accept(oldRecords)
                
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                
                self.notificationRecords.accept(oldRecords)
                
                return
            }
            
            oldRecords.append(contentsOf: records)
            
            if (result.length ?? 0) == self.limit {
                self.offset += oldRecords.count
                
                let fakeData = NotificationRecord(isLoadMore: true)
                oldRecords.append(fakeData)
            }
            
            self.notificationRecords.accept(oldRecords)
        }
    }
    
    func setIsRead(id: Int?) {
        guard let id = id else { return }
        NotificationApi.setIsRead(id: id) { [weak self] (data, error) in
            guard let self = self else { return }
            
            if let error = error {
                print(error.localizedDescription)
                // self.apiError.accept(error)
                return
            }
            
            let notificationRecords = self.notificationRecords.value
            for item in notificationRecords where item.id == id {
                item.isRead = true
            }
            
            self.notificationRecords.accept(notificationRecords)
        }
    }
    
    func addMessObject(_ messObject: String?) {
        guard let jsonString = messObject,
            let notificationRecord = Mapper<NotificationRecord>().map(JSONString: jsonString) else { return }
        
        var notificationRecords = self.notificationRecords.value
        notificationRecords.insert(notificationRecord, at: 0)
        self.notificationRecords.accept(notificationRecords)
    }
}
