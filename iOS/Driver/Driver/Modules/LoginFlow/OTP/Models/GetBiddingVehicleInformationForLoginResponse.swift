//
//  GetBiddingVehicleInformationForLoginResponse.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class GetBiddingVehicleInformationForLoginRecord: NSObject, Mappable {
    var lisencePlate: String?
    var driverPhoneNumber: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lisencePlate <- map["lisence_plate"]
        driverPhoneNumber <- map["driver_phone_number"]
    }
}
