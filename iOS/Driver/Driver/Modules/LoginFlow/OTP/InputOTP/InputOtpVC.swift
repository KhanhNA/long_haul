//
//  InputOtpVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD

class InputOtpVC: BaseVC {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var lisencePlateTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationController(title: "", barTintColor: AppColor.hexE5E5E5)
        
        titleLabel.text = "Gửi mã OTP".localized()
        subtitleLabel.text = "Mã OTP gửi về số điện thoại tương ứng thông tin xe".localized()
        lisencePlateTextField.placeholder = "Nhập biển kiểm soát".localized()
        continueButton.setTitle("Tiếp tục".localized(), for: .normal)
        continueButton.dropShadow(cornerRadius: 12, shadowColor: AppColor.hex4D00B563, shadowOpacity: 1)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        guard let lisencePlate = lisencePlateTextField.text, !lisencePlate.isEmpty else {
            view.makeToast("Bạn chưa điền thông tin biển kiểm soát!".localized(),
                           duration: 1,
                           position: CSToastPositionCenter,
                           style: nil)
            return
        }
        
        
        KRProgressHUD.show()
        
        OtpApi.getBiddingVehicleInformationForLogin(licensePlate: lisencePlate) { [weak self] (data, error) in
            
            KRProgressHUD.dismiss()
            
            guard let self = self else { return }
            
            if let error = error {
                print(error.localizedDescription)
                self.view.makeToast(error.localizedDescription,
                                    duration: 1,
                                    position: CSToastPositionCenter,
                                    style: nil)
                return
            }
            
            guard let result = data?.result, let records = result.records else {
                return
            }
            
            self.mainCoordinator?.goToConfirmOtpVC()
        }
    }
}
