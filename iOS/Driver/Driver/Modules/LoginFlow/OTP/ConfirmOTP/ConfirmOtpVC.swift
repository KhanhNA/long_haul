//
//  ConfirmOtpVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class ConfirmOtpVC: BaseVC {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var phoneNoLabel: UILabel!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationController(title: "", barTintColor: AppColor.hexE5E5E5)
        
        titleLabel.text = "Xác thực mã OTP".localized()
        subtitleLabel.text = "Vui lòng nhập mã OTP được gửi đến số".localized()
        phoneNoLabel.text = ""
        otpTextField.placeholder = "Mã xác thực".localized()
        warningLabel.text = "Mã xác thực không chính xác. Vui lòng thử lại".localized()
        titleLabel.text = "Xác thực mã OTP".localized()
        confirmButton.setTitle("Xác nhận".localized(), for: .normal)
        resendLabel.text = "Bạn không nhận được mã?".localized()
        resendButton.setTitle("GỬI LẠI MÃ".localized(), for: .normal)
        confirmButton.dropShadow(cornerRadius: 12, shadowColor: AppColor.hex4D00B563, shadowOpacity: 1)
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        
    }
    
    @IBAction func resendAction(_ sender: Any) {
        
    }
}
