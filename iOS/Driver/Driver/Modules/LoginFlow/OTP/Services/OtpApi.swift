//
//  OtpApi.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class OtpApi: NSObject {
    // Lấy thông tin đăng nhập qua biển số xe
    typealias GetBiddingVehicleInformationForLoginRecordResult = BaseMappableResultModel<GetBiddingVehicleInformationForLoginRecord>
    typealias GetBiddingVehicleInformationForLoginResponseCompletion = BaseMappableResponseModel<GetBiddingVehicleInformationForLoginRecordResult>
    typealias GetBiddingVehicleInformationForLoginCompletion = (GetBiddingVehicleInformationForLoginResponseCompletion?, Error?) -> ()
    
    static func getBiddingVehicleInformationForLogin(licensePlate: String,
                                                     completion: @escaping GetBiddingVehicleInformationForLoginCompletion) {
        let parameters: [String : Any] = ["jsonrpc": "2.0",
                                          "params": ["license_plate": licensePlate]]
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingVehicleInformationForLogin.url,
                                                 method: .post,
                                                 parameters: parameters,
                                                 headers: nil,
                                                 completion: completion)
    }
}
