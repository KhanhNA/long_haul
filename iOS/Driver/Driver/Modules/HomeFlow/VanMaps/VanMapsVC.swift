//
//  VanMapsVC.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import GoogleMaps
import CoreLocation
import GooglePlaces
import FittedSheets
import MarqueeLabel

class VanMapsVC: BaseVC {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var fromDepotAddressLabel: MarqueeLabel!
    @IBOutlet weak var fromReceiveTimeToReceiveTimeLabel: UILabel!
    @IBOutlet weak var toDepotAddressLabel: MarqueeLabel!
    @IBOutlet weak var fromReturnTimeToReturnTimeLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    var getListBiddingOrderShippingRecord: GetListBiddingOrderShippingRecord?
    var fromHomeType: HomeType!
    
    private let viewModel = VanMapsViewModel()
    private var sheet: SheetViewController?
    
    private let locationManager = CLLocationManager()
    private var currentLocationMaker: GMSMarker?
    private var polyline: GMSPolyline?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleSubscribe()
        
        setupViews()
    }
    
    private func setupViews() {
        setupNavigationController()
        bottomView.dropShadow(cornerRadius: 12)
        
        searchTextField.placeholder = "Tìm kiếm địa chỉ".localized()
        
        // bottom view
        fromDepotAddressLabel.text = getListBiddingOrderShippingRecord?.fromDepot?.address
        fromReceiveTimeToReceiveTimeLabel.text = getListBiddingOrderShippingRecord?.getFromReceiveTimeToReceiveTime()
        
        toDepotAddressLabel.text = getListBiddingOrderShippingRecord?.toDepot?.address
        fromReturnTimeToReturnTimeLabel.text = getListBiddingOrderShippingRecord?.getFromReturnTimeToReturnTime()
        
        fromDepotAddressLabel.setupMarqueeLabel()
        toDepotAddressLabel.setupMarqueeLabel()
        
        distanceLabel.text = getListBiddingOrderShippingRecord?.getDistance()
        
        configMap()
        
        let updateMapFromTo: () -> () = { [weak self] in
            guard let self = self,
                let fromLatitude = self.getListBiddingOrderShippingRecord?.fromDepot?.latitude,
                let fromLongitude = self.getListBiddingOrderShippingRecord?.fromDepot?.longitude,
                let toLatitude = self.getListBiddingOrderShippingRecord?.toDepot?.latitude,
                let toLongitude = self.getListBiddingOrderShippingRecord?.toDepot?.longitude
                else { return }
            
            let edgeInsets = UIEdgeInsets(top: 200, left: 200, bottom: 200, right: 200)
            self.mapView.update(fromLatitude: fromLatitude,
                           fromLongitude: fromLongitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude,
                           edgeInsets: edgeInsets)
        }
        
        if fromHomeType == .notShipping {
            // nếu chưa vận chuyển
            // vẽ đường đi từ from đến to
            
            updateMapFromTo()
            
        } else if fromHomeType == .shipping {
            // nếu đang vận chuyển
            // vẽ đường đi từ current location đến to
            
        } else {
            // nếu trả hàng
            // vẽ đường đi từ from đến to
            
            updateMapFromTo()
        }
    }
    
    private func setupNavigationController() {
        if fromHomeType == .notShipping || fromHomeType == .shipping {
            
            setupNavigationController(title: fromHomeType.getTitle())
            
        } else {
            
            let title = (getListBiddingOrderShippingRecord?.isCancel == true
                ? "Đơn bị hủy".localized()
                : "Vận chuyển thành công".localized())
            
            setupNavigationController(title: title)
        }
    }
    
    private func handleSubscribe() {
        handleViewModelInput()
        handleViewModelOutput()
    }
    
    private func handleViewModelInput() {
        // cập nhật dữ liệu textSearch từ searchTextField
        searchTextField.rx.text.bind(to: viewModel.input.textSearch)
            .disposed(by: viewModel.disposeBag)
        
        searchTextField.rx.controlEvent([.editingDidEnd]).asObservable().subscribe(onNext: { [weak self] (_) in
            
            guard let self = self,
                self.viewModel.local.oldTextSearch != self.viewModel.input.textSearch.value
                else { return }
            
            // self.viewModel.reloadData(isShowLoading: true)
            
        }).disposed(by: viewModel.disposeBag)
        
        viewModel.input.getListBiddingOrderShippingRecord.accept(getListBiddingOrderShippingRecord)
        viewModel.input.fromHomeType.accept(fromHomeType)
    }
    
    private func handleViewModelOutput() {
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        
        let rawValue = UInt(GMSPlaceField.name.rawValue)
            | UInt(GMSPlaceField.placeID.rawValue)
            | UInt(GMSPlaceField.coordinate.rawValue)
            | GMSPlaceField.addressComponents.rawValue
            | GMSPlaceField.formattedAddress.rawValue
        guard let fields: GMSPlaceField = GMSPlaceField(rawValue: rawValue) else { return }
        
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        sheet = SheetViewController(controller: autocompleteController, sizes: [.fullScreen])
        sheet?.handleColor = .clear
        sheet?.topCornersRadius = 20
        sheet?.extendBackgroundBehindHandle = false
        sheet?.overlayColor = AppColor.hex80C4C4C4
        
        if let vc = sheet {
            present(vc, animated: false, completion: nil)
        }
    }
}

extension VanMapsVC {
    func configMap() {
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0,
                                       bottom: bottomView.frame.height + 25,
                                       right: 10)
        
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        
        requestAlwaysAuthorization()
        
        mapView.alpha = 0
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.5,
                       options: .curveLinear,
                       animations: { [weak self] in self?.mapView.alpha = 1 },
                       completion: nil)
        
        guard let fromLatitude = getListBiddingOrderShippingRecord?.fromDepot?.latitude,
            let fromLongitude = getListBiddingOrderShippingRecord?.fromDepot?.longitude,
            let toLatitude = getListBiddingOrderShippingRecord?.toDepot?.latitude,
            let toLongitude = getListBiddingOrderShippingRecord?.toDepot?.longitude else { return }
        
        _ = mapView.addMarker(latitude: fromLatitude,
                              longitude: fromLongitude,
                              imageName: "home_vc_map_from",
                              title: "",
                              snippet: "")
        
        _ = mapView.addMarker(latitude: toLatitude,
                              longitude: toLongitude,
                              imageName: "home_vc_map_to",
                              title: "",
                              snippet: "")
    }
    
    private func updateDistanceInMeters(currentLocation: CLLocation?) {
        guard let getListBiddingOrderShippingRecord = getListBiddingOrderShippingRecord,
            let location = currentLocation else { return }
        
        var fromLocation: CLLocation?
        var toLocation: CLLocation?
        
        if fromHomeType == .notShipping {
            // nếu chưa vận chuyển
            // hiển thị quãng đường từ current location
            // đến điểm from
            
            // hiển thị quãng đường từ current location -> from -> to
            // hiển thị marker chứa thông tin thời gian + quãng đường
            
            if let lat = getListBiddingOrderShippingRecord.fromDepot?.latitude,
                let lon = getListBiddingOrderShippingRecord.fromDepot?.longitude {
                fromLocation = MapsHelper.getCLLocation(latitude: lat, longitude: lon)
            }
            
        } else if fromHomeType == .shipping {
            // nếu đang vận chuyển
            // hiển thị quãng đường từ current location
            // đến điểm to
            
            // hiển thị quãng đường từ current location -> to
            // hiển thị marker chứa thông tin thời gian + quãng đường
            
            if let lat = getListBiddingOrderShippingRecord.toDepot?.latitude,
                let lon = getListBiddingOrderShippingRecord.toDepot?.longitude {
                toLocation = MapsHelper.getCLLocation(latitude: lat, longitude: lon)
            }
        }
        
        if let location2 = (fromLocation ?? toLocation) {
            DetailApi.getDistanceAndTime(location: location, location2: location2) { [weak self] (data, error) in
                guard let self = self, let data = data else { return }
                
                let leg = data.routes?.first?.legs?.first
                let duration = leg?.duration?.text ?? ""
                let distance = leg?.distance?.text ?? ""
                
                
                var title = duration
                if !duration.isEmpty && !distance.isEmpty {
                    title += " - "
                }
                title += distance
                
                self.currentLocationMaker?.map = nil
                self.currentLocationMaker = self.mapView.addMarker(latitude: location.coordinate.latitude,
                                                                   longitude: location.coordinate.longitude,
                                                                   imageName: "van_detail_vc_current_location_marker",
                                                                   title: title,
                                                                   snippet: "")
            }
        } else {
            currentLocationMaker?.map = nil
            currentLocationMaker = mapView.addMarker(latitude: location.coordinate.latitude,
                                                     longitude: location.coordinate.longitude,
                                                     imageName: "van_detail_vc_current_location_marker",
                                                     title: "",
                                                     snippet: "")
        }
    }
}

extension VanMapsVC: CLLocationManagerDelegate {
    func requestAlwaysAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        locationManager.stopUpdatingLocation()
        
        mapView.setCamera(cllocation: location)
        
        updateDistanceInMeters(currentLocation: location)
        
        if fromHomeType == .notShipping {
            // nếu chưa vận chuyển
            // vẽ đường đi từ from đến to
            
            guard let toLatitude = getListBiddingOrderShippingRecord?.fromDepot?.latitude,
                let toLongitude = getListBiddingOrderShippingRecord?.fromDepot?.longitude else { return }
            
            let edgeInsets = UIEdgeInsets(top: 200, left: 200, bottom: 200, right: 200)
            
            polyline?.map = nil
            mapView.update(fromLatitude: location.coordinate.latitude,
                           fromLongitude: location.coordinate.longitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude,
                           edgeInsets: edgeInsets) { [weak self] polyline in
                            self?.polyline = polyline }
            
        } else if fromHomeType == .shipping {
            // nếu đang vận chuyển
            // vẽ đường đi từ current location đến to
            
            guard let toLatitude = getListBiddingOrderShippingRecord?.toDepot?.latitude,
                let toLongitude = getListBiddingOrderShippingRecord?.toDepot?.longitude else { return }
            
            let edgeInsets = UIEdgeInsets(top: 200, left: 200, bottom: 200, right: 200)
            
            polyline?.map = nil
            mapView.update(fromLatitude: location.coordinate.latitude,
                           fromLongitude: location.coordinate.longitude,
                           toLatitude: toLatitude,
                           toLongitude: toLongitude,
                           edgeInsets: edgeInsets) { [weak self] polyline in
                            self?.polyline = polyline }
        }
    }
}

extension VanMapsVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: ", place.name ?? "")
        print("Place ID: ", place.placeID ?? "")
        print("Place attributions: ", place.attributions ?? "")
        
        sheet?.closeSheet()
        
        mapView.setCamera(cllocationCoordinate2D: place.coordinate)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        sheet?.closeSheet()
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
