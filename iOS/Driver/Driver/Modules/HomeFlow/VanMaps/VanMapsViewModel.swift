//
//  VanMapsViewModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VanMapsViewModel: BaseViewModel {
    struct Input {
        let getListBiddingOrderShippingRecord = BehaviorRelay<GetListBiddingOrderShippingRecord?>(value: nil)
        let fromHomeType = BehaviorRelay<HomeType?>(value: nil)
        let textSearch = BehaviorRelay<String?>(value: nil)
    }
    
    struct Output {
        
    }
    
    struct Local {
        var oldTextSearch: String?
    }
    
    // ---------------------------- //
    
    private (set) var input  = Input()
    private (set) var output = Output()
    private (set) var local  = Local()
}
