//
//  ScannerVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import AVFoundation
import UIKit
import Toast

class ScannerVC: BaseVC {
    @IBOutlet weak var scannerView: UIView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var completion: ((_ code: String) -> ())?
    var totalQrArray = [String]() // các qr hợp lệ (khi quét mã có kết quả phải check contains trong list này)
    var existQrArray = [String]() // các qr đã được thêm (nếu contains thì báo lỗi)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController(title: "Quét mã QR".localized())
        setup()
    }
}

extension ScannerVC {
    func setup() {
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: .main)
            metadataOutput.metadataObjectTypes = metadataOutput.availableMetadataObjectTypes
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.layer.addSublayer(scannerView.layer)
        captureSession.startRunning()
    }
    
    func found(code: String) {
        if !totalQrArray.contains(code) {
            showToast(errorMessage: "Mã QR không có trong danh sách nhận!".localized())
            return
        }
        
        if existQrArray.contains(code) {
            showToast(errorMessage: "Mã QR đã có trong danh sách nhận!".localized())
            return
        }
        
        completion?(code)
        navigationController?.popViewController(animated: true)
    }
    
    func failed() {
        showToast(errorMessage: "Có lỗi xảy ra".localized())
    }
    
    func showToast(errorMessage: String) {
        view.makeToast(errorMessage,
                       duration: 1,
                       position: CSToastPositionCenter,
                       style: nil)
        
        captureSession?.stopRunning()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.captureSession?.startRunning()
        }
    }
}

extension ScannerVC: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession?.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}
