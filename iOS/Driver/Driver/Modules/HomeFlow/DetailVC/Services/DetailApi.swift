//
//  DetailApi.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import CoreLocation

class DetailApi: NSObject {
    // Chi tiết đơn hàng
    typealias GetBiddingOrderDetailByIdResult = BaseMappableResultModel<GetListBiddingOrderShippingRecord>
    typealias GetBiddingOrderDetailByIdResponseCompletion = BaseMappableResponseModel<GetBiddingOrderDetailByIdResult>
    typealias GetBiddingOrderDetailByIdCompletion = (GetBiddingOrderDetailByIdResponseCompletion?, Error?) -> ()
    
    static func getBiddingOrderDetailById(biddingOrderId: String,
                                          completion: @escaping GetBiddingOrderDetailByIdCompletion) {
        BaseClient.shared.requestAPIWithMappable(apiURL.getBiddingOrderDetailById.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["bidding_order_id" : biddingOrderId]],
                                                 headers: nil,
                                                 completion: completion)
        
    }
    
    // Nhận hàng
    typealias DriverConfirmCargoQuantityResult = BaseMappableResultModel<DriverConfirmCargoQuantityRecord>
    typealias DriverConfirmCargoQuantityResponseCompletion = BaseMappableResponseModel<DriverConfirmCargoQuantityResult>
    typealias DriverConfirmCargoQuantityCompletion = (DriverConfirmCargoQuantityResponseCompletion?, Error?) -> ()

    static func driverConfirmCargoQuantity(biddingOrderId: String,
                                           cargoIds: String,
                                           type: String,
                                           confirmTime: String,
                                           completion: @escaping DriverConfirmCargoQuantityCompletion) {
        // type // "1": update || "": tạo mới
        BaseClient.shared.requestAPIWithMappable(apiURL.driverConfirmCargoQuantity.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0",
                                                              "params": ["bidding_order_id": biddingOrderId,
                                                                         "cargo_ids": cargoIds,
                                                                         "type": type,
                                                                         "confirm_time": confirmTime]],
                                                 headers: nil,
                                                 completion: completion)
        
    }
    
    static func getDistanceAndTime(location: CLLocation,
                                   location2: CLLocation,
                                   completion: @escaping (MapsResponse?, Error?) -> ()) {
        
        let origin = String(location.coordinate.latitude) + "," + String(location.coordinate.longitude)
        let destination = String(location2.coordinate.latitude) + "," + String(location2.coordinate.longitude)
        
        BaseClient.shared.requestAPIWithMappable("https://maps.googleapis.com/maps/api/directions/json",
                                                 method: .get,
                                                 parameters: ["mode": "driving",
                                                              "transit_routing_preference": "less_driving",
                                                              "origin": origin,
                                                              "destination": destination,
                                                              "key": GOOGLE_API_KEY],
                                                 headers: nil,
                                                 completion: completion)
    }
}
