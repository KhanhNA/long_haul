//
//  DriverConfirmCargoQuantityResponse.swift
//  Driver
//
//  Created by Hieu Dinh on 8/27/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class DriverConfirmCargoQuantityRecord: NSObject, Mappable {
    var listCargoInvalid: [Int]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        listCargoInvalid <- map["list_cargo_invalid"]
    }
}
