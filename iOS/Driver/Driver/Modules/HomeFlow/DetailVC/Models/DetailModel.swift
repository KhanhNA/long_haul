//
//  DetailModel.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

struct DetailModel {
    var title: DetailTitleModel!
    var infoArray: [DetailInfoModel]!
    var isHiddenLineView = false
    
    init(title: DetailTitleModel,
         infoArray: [DetailInfoModel],
         isHiddenLineView: Bool = false) {
        
        self.title = title
        self.infoArray = infoArray
        self.isHiddenLineView = isHiddenLineView
        
    }
    
    func getCellHeight() -> CGFloat {
        var height: CGFloat = 20
        
        if title != nil && !title.title.isEmpty {
            height += 27.5 // chiều cao của title
        }
        
        height += CGFloat(infoArray.count) * 25 // chiều cao + padding của detail
        
        return height
    }
}
