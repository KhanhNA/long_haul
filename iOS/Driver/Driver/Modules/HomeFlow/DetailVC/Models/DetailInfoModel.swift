//
//  DetailInfoModel.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

struct DetailInfoModel {
    var text = ""
    var value = ""
    var textColor = UIColor.black
    var valueColor = UIColor.black
    
    init(text: String,
         value: String = "",
         textColor: UIColor = .black,
         valueColor: UIColor = .black) {
        
        self.text = text
        self.value = value
        self.textColor = textColor
        self.valueColor = valueColor
        
    }
}
