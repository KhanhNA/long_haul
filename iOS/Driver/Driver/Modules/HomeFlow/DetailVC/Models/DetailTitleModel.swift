//
//  DetailTitleModel.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

struct DetailTitleModel {
    var imageNamed = ""
    var title = ""
    var titleColor = UIColor.black
    
    var isHiddenReasonButton = true
    var isHiddenEditButton = true
    var isHiddenMapButton = true
    
    init(imageNamed: String,
         title: String,
         titleColor: UIColor = .black,
         isHiddenReasonButton: Bool = true,
         isHiddenEditButton: Bool = true,
         isHiddenMapButton: Bool = true) {
        
        self.imageNamed = imageNamed
        self.title = title
        self.titleColor = titleColor
        self.isHiddenReasonButton = isHiddenReasonButton
        self.isHiddenEditButton = isHiddenEditButton
        self.isHiddenMapButton = isHiddenMapButton
        
    }
}
