//
//  MapsResponse.swift
//  Driver
//
//  Created by Hieu Dinh on 8/24/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

class MapsResponse: NSObject, Mappable {
    var routes: [MapsRoute]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        routes <- map["routes"]
    }
}

// MARK: - Route
class MapsRoute: NSObject, Mappable {
    var legs: [Leg]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        legs <- map["legs"]
    }
}

// MARK: - Leg
class Leg: NSObject, Mappable {
    var distance: LegData?
    var duration: LegData?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        distance <- map["distance"]
        duration <- map["duration"]
    }
}

// MARK: - Distance
class LegData: NSObject, Mappable {
    var text: String?
    var value: Double?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
}
