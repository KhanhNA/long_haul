//
//  DetailViewModel.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DetailViewModel: BaseViewModel {
    struct Input {
        let id = BehaviorRelay<Int?>(value: nil) // biddingOrderId
        let fromHomeType = BehaviorRelay<HomeType?>(value: nil)
        let isEditting = BehaviorRelay<Bool>(value: false)
        // check khoảng cách khi nhận hàng
        // nếu khoảng cách hiện tại đến kho nhận lớn hơn distance_check_point show toast màn hình 8-29
        let distance = BehaviorRelay<Double>(value: 0)
    }
    
    struct Output {
        let getListBiddingOrderShippingRecord = BehaviorRelay<GetListBiddingOrderShippingRecord?>(value: nil)
        
        let data = BehaviorRelay<[DetailModel]>(value: [])
        let cargoTypes = BehaviorRelay<[CargoType]>(value: [])
        let selectedCargoType = BehaviorRelay<CargoType?>(value: nil)
        
        let isEnableConfirmButton = BehaviorRelay<Bool>(value: false)
        let driverConfirmCargoQuantitySuccess = BehaviorRelay<Bool>(value: false)
        
        let errorMessageDuration = BehaviorRelay<TimeInterval>(value: 1)
    }
    
    struct Local {
        // khi ở homeType == shipping && isEditting == true
        // clone ouput.getListBiddingOrderShippingRecord
        // khi user cancel (không thực hiện huỷ)
        // thì reset data
        let oldGetListBiddingOrderShippingRecord = BehaviorRelay<GetListBiddingOrderShippingRecord?>(value: nil)
        
        // chỉ set đúng 1 lần khi gọi api detail
        var originGetListBiddingOrderShippingRecord: GetListBiddingOrderShippingRecord?
    }
    
    // ---------------------------- //
    
    private (set) var input  = Input()
    private (set) var output = Output()
    private (set) var local  = Local()
    
    override init() {
        super.init()
        
        output.getListBiddingOrderShippingRecord.skip(1).subscribe(onNext: { [weak self] (getListBiddingOrderShippingRecord) in
            guard let self = self else { return }
            
            self.local.originGetListBiddingOrderShippingRecord = getListBiddingOrderShippingRecord?.clone()
            
            let fromHomeType = getListBiddingOrderShippingRecord?.homeType ?? self.input.fromHomeType.value
            self.input.fromHomeType.accept(fromHomeType)
            
            self.initData()
            
            let cargoTypes = getListBiddingOrderShippingRecord?.biddingVehicles?.cargoTypes ?? []
            self.output.cargoTypes.accept(cargoTypes)
            
            let selectedCargoType = cargoTypes.first(where: { $0.id == self.output.selectedCargoType.value?.id })
            self.output.selectedCargoType.accept(selectedCargoType ?? cargoTypes.first)
            
        }).disposed(by: disposeBag)
        
        
        input.isEditting.subscribe(onNext: { [weak self] (isEditting) in
            
            guard let self = self,
                let getListBiddingOrderShippingRecord = self.output.getListBiddingOrderShippingRecord.value
                else { return }
            
            if isEditting {
                
                self.local.oldGetListBiddingOrderShippingRecord.accept(getListBiddingOrderShippingRecord.clone())
                
            } else {
                
                if let oldGetListBiddingOrderShippingRecord = self.local.oldGetListBiddingOrderShippingRecord.value {
                    self.output.getListBiddingOrderShippingRecord.accept(oldGetListBiddingOrderShippingRecord)
                    self.local.oldGetListBiddingOrderShippingRecord.accept(nil)
                    
                    self.updateEnableConfirmButton()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func checkIsValidDistanceCheckPoint() -> Bool {
        // check khoảng cách khi nhận hàng
        // nếu khoảng cách hiện tại đến kho nhận lớn hơn distance_check_point show toast màn hình 8-29

        let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
        let distance_check_point = loginModel?.distance_check_point ?? ""
        
        guard let distanceCheckPoint = Double(distance_check_point) else { return false }
        
        if input.distance.value <= distanceCheckPoint {
            return true
        }
        
        return false
    }
    
    func updateEnableConfirmButton() {
        let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value
        let originGetListBiddingOrderShippingRecord = local.originGetListBiddingOrderShippingRecord
        
        var cargos = [Cargo]()
        for cargoType in getListBiddingOrderShippingRecord?.biddingVehicles?.cargoTypes ?? [] {
            cargos.append(contentsOf: cargoType.cargos ?? [])
        }
        
        var originCargos = [Cargo]()
        for cargoType in originGetListBiddingOrderShippingRecord?.biddingVehicles?.cargoTypes ?? [] {
            originCargos.append(contentsOf: cargoType.cargos ?? [])
        }
        
        let isSameData = cargos.compactMap({ $0.id }).containsSameElements(as: originCargos.compactMap({ $0.id }))
        output.isEnableConfirmButton.accept(!isSameData)
    }
}

extension DetailViewModel {
    func removeQr(code: String?) {
        guard let code = code, let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value else { return }
        
        for cargoType in getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes ?? [] {
            cargoType.cargos?.removeAll(where: { $0.qrCode == code })
        }
        
        output.cargoTypes.accept(getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes ?? [])
        
        updateEnableConfirmButton()
    }
    
    func addQr(code: String) {
        guard let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value else { return }
        
        if getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes == nil {
            getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes = []
        }
        
        for qrCargoType in getListBiddingOrderShippingRecord.qrCargoType ?? [] {
            
            // tìm cargo tương ứng có qr_code trong getListBiddingOrderShippingRecord.qrCargoType
            if let qrCargo = qrCargoType.cargos?.first(where: { $0.qrCode == code }) {
                
                // tìm cargo tương ứng có id trong getListBiddingOrderShippingRecord.biddingVehicles
                if let cargoType = getListBiddingOrderShippingRecord.biddingVehicles?
                    .cargoTypes?.first(where: { $0.id == qrCargoType.id }) {
                    
                    if cargoType.cargos == nil {
                        cargoType.cargos = []
                    }
                    
                    if let cargo = qrCargo.clone() {
                        cargoType.cargos?.append(cargo)
                    }
                    
                    output.selectedCargoType.accept(cargoType)
                    
                    errorMessage.accept(String(format: "%@ %@", "Quét thành công mã".localized(), code))
                    
                } else {
                    
                    // trường hợp getListBiddingOrderShippingRecord.biddingVehicles không có cargo type
                    if let cargoType = qrCargoType.clone() {
                        
                        cargoType.cargos = []
                        
                        getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes?.append(cargoType)
                        
                        if let cargo = qrCargo.clone() {
                            cargoType.cargos?.append(cargo)
                        }
                        
                        output.selectedCargoType.accept(cargoType)
                        
                        errorMessage.accept(String(format: "%@ %@", "Quét thành công mã".localized(), code))
                    }
                }
            }
        }
        
        output.cargoTypes.accept(getListBiddingOrderShippingRecord.biddingVehicles?.cargoTypes ?? [])
        
        updateEnableConfirmButton()
    }
}

extension DetailViewModel {
    func getCargoIds() -> [String] {
        var cargoIds = [String]()
        for cargoType in output.getListBiddingOrderShippingRecord.value?.biddingVehicles?.cargoTypes ?? [] {
            for cargo in cargoType.cargos ?? [] {
                if let id = cargo.id {
                    cargoIds.append(String(id))
                }
            }
        }
        
        return cargoIds
    }
    
    func callApiConfirm() {
        guard let id = input.id.value else { return }
        
        let cargoIds = getCargoIds()
        
        if input.fromHomeType.value == .shipping && cargoIds.isEmpty {
            errorMessage.accept("Bạn chưa quét cargo. Vui lòng quét cargo để nhận hàng!".localized())
            return
        }
        
        isShowLoading.accept(true)
        
        let dateString = Date().toString()
        let confirmTime = Date.localToUTC(date: dateString) ?? dateString
        
        // type | "1": update || "": tạo mới
        let type = (input.fromHomeType.value == .shipping ? "1" : "")
        
        DetailApi.driverConfirmCargoQuantity(biddingOrderId: String(id),
                                             cargoIds: cargoIds.joined(separator: ","),
                                             type: type,
                                             confirmTime: confirmTime) { [weak self] (data, error) in
                                                
                                                guard let self = self else { return }
                                                self.isShowLoading.accept(false)
                                                
                                                if let error = error {
                                                    print(error.localizedDescription)
                                                    self.apiError.accept(error)
                                                    return
                                                }
                                                
                                                guard let result = data?.result,
                                                    let driverConfirmCargoQuantityRecord = result.record else {
                                                        
                                                    self.errorMessage.accept("Có lỗi xảy ra".localized())
                                                    return
                                                }
                                                
                                                if let listCargoInvalid = driverConfirmCargoQuantityRecord.listCargoInvalid,
                                                    !listCargoInvalid.isEmpty {
                                                    
                                                    self.output.errorMessageDuration.accept(2)
                                                    let message = String(format: "Mã QR %@ đã được nhận hàng tại xe khác!".localized(),
                                                                         listCargoInvalid.compactMap({ String($0) })
                                                                            .joined(separator: ", "))
                                                    self.errorMessage.accept(message)
                                                    return
                                                }
                                                
                                                self.output.driverConfirmCargoQuantitySuccess.accept(true)
                                                self.local.oldGetListBiddingOrderShippingRecord.accept(nil)
                                                self.input.isEditting.accept(false) }
    }
    
    func callApi() {
        guard let id = input.id.value else { return }
        
        isShowLoading.accept(true)
        
        DetailApi.getBiddingOrderDetailById(biddingOrderId: String(id)) { [weak self] (data, error) in
            guard let self = self else { return }
            
            self.isShowLoading.accept(false)
            
            if let error = error {
                print(error.localizedDescription)
                self.apiError.accept(error)
                return
            }
            
            guard let result = data?.result, let getListBiddingOrderShippingRecord = result.records?.first else {
                self.errorMessage.accept("Có lỗi xảy ra".localized())
                return
            }
            
            self.output.getListBiddingOrderShippingRecord.accept(getListBiddingOrderShippingRecord)
        }
    }
}

extension DetailViewModel {
    func initData() {
        var data = [DetailModel]()
        
        if let fromDetailModel = initFromDetailModel() {
            data.append(fromDetailModel)
        }
        
        if let toDetailModel = initToDetailModel() {
            data.append(toDetailModel)
        }
        
        if let listDetailModel = initListDetailModel() {
            data.append(listDetailModel)
        }
        
        output.data.accept(data)
    }
    
    private func initFromDetailModel() -> DetailModel? {
        guard let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value else { return nil }
        
        let isShowReasonButton = (input.fromHomeType.value == .return && getListBiddingOrderShippingRecord.isCancel)
        let title = DetailTitleModel(imageNamed: "detail_vc_from",
                                     title: "Địa chỉ kho lấy hàng".localized(),
                                     isHiddenReasonButton: !isShowReasonButton)
        
        let name = getListBiddingOrderShippingRecord.fromDepot?.name ?? ""
        let phone = getListBiddingOrderShippingRecord.fromDepot?.phone ?? ""
        
        var value = name
        if !name.isEmpty && !phone.isEmpty {
            value += " | "
        }
        value += phone
        
        var infoArray = [
            DetailInfoModel(text:String(format: "%@ %@",
                                        "Từ".localized(),
                                        getListBiddingOrderShippingRecord.getFromReceiveTimeToReceiveTime())),
            DetailInfoModel(text: String(format: "%@ %@",
                                         "Kho".localized(),
                                         value)),
            DetailInfoModel(text: getListBiddingOrderShippingRecord.fromDepot?.address ?? "")
        ]
        
        infoArray.removeAll(where: { $0.text.isEmpty && $0.value.isEmpty })
        
        return DetailModel(title: title, infoArray: infoArray)
    }
    
    private func initToDetailModel() -> DetailModel? {
        guard let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value else { return nil }
        
        let title = DetailTitleModel(imageNamed: "detail_vc_to",
                                     title: "Giao hàng đến".localized(),
                                     isHiddenMapButton: false)
        
        let name = getListBiddingOrderShippingRecord.toDepot?.name ?? ""
        let phone = getListBiddingOrderShippingRecord.toDepot?.phone ?? ""
        
        var value = name
        if !name.isEmpty && !phone.isEmpty {
            value += " | "
        }
        value += phone
        
        var infoArray = [
            DetailInfoModel(text:String(format: "%@ %@",
                                        "Từ".localized(),
                                        getListBiddingOrderShippingRecord.getFromReturnTimeToReturnTime())),
            DetailInfoModel(text: String(format: "%@ %@",
                                         "Kho".localized(),
                                         value)),
            DetailInfoModel(text: getListBiddingOrderShippingRecord.toDepot?.address ?? "")
        ]
        
        infoArray.removeAll(where: { $0.text.isEmpty && $0.value.isEmpty })
        
        return DetailModel(title: title, infoArray: infoArray)
    }
    
    private func initListDetailModel() -> DetailModel? {
        guard let getListBiddingOrderShippingRecord = output.getListBiddingOrderShippingRecord.value else { return nil }
        
        let title = DetailTitleModel(imageNamed: "detail_vc_list",
                                     title: "Danh sách hàng hóa nhận".localized(),
                                     isHiddenEditButton: (input.fromHomeType.value != .shipping))
        
        var infoArray = [
            DetailInfoModel(text: getListBiddingOrderShippingRecord.getId()),
            DetailInfoModel(text: "Tổng số lượng cargo".localized(), value: getListBiddingOrderShippingRecord.getTotalCargo()),
            DetailInfoModel(text: "Tổng cân nặng".localized(), value: getListBiddingOrderShippingRecord.getTotalWeight()),
        ]
        
        if input.fromHomeType.value == .shipping {
            
            infoArray.append(DetailInfoModel(text: "Thực nhận".localized(),
                                             value: getListBiddingOrderShippingRecord.biddingVehicles?
                                                .biddingOrderReceive?.actualTime?
                                                .toString() ?? ""))
            
        } else if input.fromHomeType.value == .return {
            
            infoArray.append(contentsOf: [
                DetailInfoModel(text: "Thực nhận".localized(),
                                value: getListBiddingOrderShippingRecord.biddingVehicles?
                                    .biddingOrderReceive?.actualTime?
                                    .toString() ?? ""),
                DetailInfoModel(text: "Thực trả".localized(),
                                value: getListBiddingOrderShippingRecord.biddingVehicles?
                                    .biddingOrderReturn?.actualTime?
                                    .toString() ?? "")
            ])
            
        }
        
        infoArray.removeAll(where: { $0.text.isEmpty && $0.value.isEmpty })
        
        return DetailModel(title: title, infoArray: infoArray)
    }
}
