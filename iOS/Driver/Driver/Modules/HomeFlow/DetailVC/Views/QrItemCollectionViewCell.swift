//
//  QrItemCollectionViewCell.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class QrItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    var deleteClosure: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func deletecAction(_ sender: Any) {
        deleteClosure?()
    }
}
