//
//  DetailInfoCollectionViewCell.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class DetailInfoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reasonButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var text2Label: UILabel!
    @IBOutlet weak var value2Label: UILabel!
    
    @IBOutlet weak var text3Label: UILabel!
    @IBOutlet weak var value3Label: UILabel!
    
    @IBOutlet weak var text4Label: UILabel!
    @IBOutlet weak var value4Label: UILabel!
    
    @IBOutlet weak var text5Label: UILabel!
    @IBOutlet weak var value5Label: UILabel!
    
    @IBOutlet weak var lineView: UIView!
    
    private lazy var textLabels = [textLabel, text2Label, text3Label, text4Label, text5Label]
    private lazy var valueLabels = [valueLabel, value2Label, value3Label, value4Label, value5Label]
    
    var reasonClosure: (() -> ())?
    var editClosure: (() -> ())?
    var mapClosure: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reasonButton.setTitle("Lý do huỷ".localized(), for: .normal)
    }
    
    func setup(detailModel: DetailModel, isEditting: Bool) {
        
        let detailTitleModel = detailModel.title!
        let detailInfoModels = detailModel.infoArray!
        
        lineView.isHidden = detailModel.isHiddenLineView
        
        iconImageView.image = UIImage(named: detailTitleModel.imageNamed)
        titleLabel.text = detailTitleModel.title
        titleLabel.textColor = detailTitleModel.titleColor
        
        reasonButton.isHidden = detailTitleModel.isHiddenReasonButton
        editButton.isHidden = detailTitleModel.isHiddenEditButton
        mapButton.isHidden = detailTitleModel.isHiddenMapButton
        
        editButton.setTitle(isEditting ? "Huỷ".localized() : "Chỉnh sửa".localized(), for: .normal)
        
        iconImageView.isHidden = detailTitleModel.title.isEmpty
        titleLabel.superview?.isHidden = detailTitleModel.title.isEmpty
        
        for textLabel in textLabels {
            textLabel?.superview?.isHidden = true
        }
        
        for (index, detailInfoModel) in detailInfoModels.enumerated() {
            guard index < textLabels.count
                && index < valueLabels.count else { break }
            
            textLabels[index]?.superview?.isHidden = false
            
            textLabels[index]?.textColor = detailInfoModel.textColor
            valueLabels[index]?.textColor = detailInfoModel.valueColor
            
            textLabels[index]?.text = detailInfoModel.text
            valueLabels[index]?.text = detailInfoModel.value
            
            if detailInfoModel.value.isEmpty {
                valueLabels[index]?.alpha = 0
            } else {
                valueLabels[index]?.alpha = 1
            }
        }
    }
    
    @IBAction func reasonAction(_ sender: Any) {
        reasonClosure?()
    }
    
    @IBAction func editAction(_ sender: Any) {
        editClosure?()
    }
    
    @IBAction func mapAction(_ sender: Any) {
        mapClosure?()
    }
}
