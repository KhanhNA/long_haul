//
//  DetailVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast
import KRProgressHUD
import FittedSheets
import CoreLocation

class DetailVC: BaseVC {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var confirmButton: UIButton!
    
    private let viewModel = DetailViewModel()
    var id: Int! // biddingOrderId
    var fromHomeType: HomeType?
    weak var cargoTypesCollectionView: UICollectionView?
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        handleSubscribe()
        
        confirmButton.setTitle("NHẬN HÀNG".localized(), for: .normal)
        confirmButton.superview?.isHidden = (fromHomeType == .return)
        
        setupNavigationController()
        handleRightBarButtonItem()
        setupCollectionView()
        
        viewModel.callApi()
        
        requestAlwaysAuthorization()
    }
    
    func handleLocation() {
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        
        let cargoIds = viewModel.getCargoIds()
        
        if viewModel.input.fromHomeType.value == .shipping && cargoIds.isEmpty {
            viewModel.errorMessage.accept("Bạn chưa quét cargo. Vui lòng quét cargo để nhận hàng!".localized())
            return
        }
        
        if fromHomeType == .notShipping && !viewModel.checkIsValidDistanceCheckPoint() {
            
            viewModel.errorMessage.accept("Khoảng cách quá xa! Vui lòng đến đúng vị trí kho nhận!".localized())
            return
        }
        
        ConfirmDialogView.show(title: "Xác nhận nhận hàng!".localized(),
                               yesTitle: "Xác nhận".localized(),
                               noTitle: "Huỷ bỏ".localized(),
                               yesAction: { [weak self] in self?.viewModel.callApiConfirm() },
                               noAction: nil)
        
    }
}

extension DetailVC: CLLocationManagerDelegate {
    func requestAlwaysAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard fromHomeType == .notShipping, let location = locations.last else { return }
        
        locationManager.stopUpdatingLocation()

        var fromLocation: CLLocation?
        if let lat = viewModel.output.getListBiddingOrderShippingRecord.value?.fromDepot?.latitude,
            let lon = viewModel.output.getListBiddingOrderShippingRecord.value?.fromDepot?.longitude {
            fromLocation = MapsHelper.getCLLocation(latitude: lat, longitude: lon)
        }
        
        guard let location2 = fromLocation else { return }
        DetailApi.getDistanceAndTime(location: location, location2: location2) { [weak self] (data, error) in
            guard let self = self, let data = data else { return }
            
            let leg = data.routes?.first?.legs?.first
            let distance = leg?.distance?.value ?? 0
            
            self.viewModel.input.distance.accept(distance)
        }
    }
}

extension DetailVC {
    private func setupNavigationController() {
        if fromHomeType == .notShipping || fromHomeType == .shipping {
            
            if let getListBiddingOrderShippingRecord = viewModel.output.getListBiddingOrderShippingRecord.value {
                
                let subtitle = String(format: "(%@)", getListBiddingOrderShippingRecord.getDistance())
                setupCustomNavigationController(title: fromHomeType?.getTitle() ?? "",
                                                subtitle: subtitle)
                
            } else {
                
                setupNavigationController(title: fromHomeType?.getTitle() ?? "")
            }
            
        } else {
            
            if let getListBiddingOrderShippingRecord = viewModel.output.getListBiddingOrderShippingRecord.value {
                
                let title = (getListBiddingOrderShippingRecord.isCancel
                    ? "Đơn bị hủy".localized()
                    : "Vận chuyển thành công".localized())
                
                let subtitle = String(format: "(%@)", getListBiddingOrderShippingRecord.getDistance())
                setupCustomNavigationController(title: title, subtitle: subtitle)
                
            } else {
                
                setupNavigationController(title: "Trả hàng".localized())
            }
        }
    }
    
    private func handleRightBarButtonItem() {
        if fromHomeType == .notShipping
            || (fromHomeType == .shipping && viewModel.input.isEditting.value) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "detail_vc_qr"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(qrAction))
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc private func qrAction() {
        
        var totalQrArray = [String]()
        for cargoType in viewModel.output.getListBiddingOrderShippingRecord.value?.qrCargoType ?? [] {
            totalQrArray.append(contentsOf: cargoType.cargos?.compactMap({ $0.qrCode }) ?? [])
        }
        
        var existQrArray = [String]()
        for cargoType in viewModel.output.getListBiddingOrderShippingRecord.value?.biddingVehicles?.cargoTypes ?? [] {
            existQrArray.append(contentsOf: cargoType.cargos?.compactMap({ $0.qrCode }) ?? [])
        }
        
        mainCoordinator?.goToScannerVC(totalQrArray: totalQrArray, existQrArray: existQrArray, completion: { [weak self] (code) in
            self?.viewModel.addQr(code: code)
        })
    }
}

extension DetailVC {
    private func handleSubscribe() {
        handleViewModelInput()
        handleViewModelOutput()
    }
    
    private func handleViewModelInput() {
        viewModel.input.id.accept(id)
        viewModel.input.fromHomeType.accept(fromHomeType)
    }
    
    private func handleViewModelOutput() {
        // show hide empty on table view
        viewModel.isShowEmpty.subscribe(onNext: { [weak self] (value) in
            
            self?.isShowEmpty = value
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý show / hide loading
        viewModel.isShowLoading.skip(1).subscribe(onNext: { (value) in
            
            DispatchQueue.main.async {
                value ? KRProgressHUD.show() : KRProgressHUD.dismiss()
            }
            
        }).disposed(by: viewModel.disposeBag)
        
        
        // xử lý khi gọi api lỗi
        viewModel.apiError.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let error = value else { return }
            
            self.view.makeToast(error.localizedDescription,
                                duration: 1,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.apiError.accept(nil)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.errorMessage.observeOn(MainScheduler.asyncInstance).subscribe(onNext: { [weak self] (value) in
            guard let self = self, let value = value else { return }
            
            self.view.makeToast(value,
                                duration: self.viewModel.output.errorMessageDuration.value,
                                position: CSToastPositionCenter,
                                style: nil)
            self.viewModel.errorMessage.accept(nil)
            self.viewModel.output.errorMessageDuration.accept(1)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.driverConfirmCargoQuantitySuccess.subscribe(onNext: { [weak self] (driverConfirmCargoQuantitySuccess) in
            guard let self = self, driverConfirmCargoQuantitySuccess else { return }
            
            let isEdit = (self.viewModel.input.fromHomeType.value == .shipping)
            
            var message = ""

            if isEdit {
                message = "Chỉnh sửa nhận hàng thành công!".localized()
            } else {
                message = "Nhận hàng thành công! Đơn của bạn chuyển sang trạng thái vận chuyển".localized()
            }
            
            SuccessDialogView.show(message: message, dismissClosure: nil)
            
            self.viewModel.output.driverConfirmCargoQuantitySuccess.accept(false)
            
            self.viewModel.callApi()
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.getListBiddingOrderShippingRecord.subscribe(onNext: { [weak self] (getListBiddingOrderShippingRecord) in
            guard let self = self else { return }
            
            self.fromHomeType = getListBiddingOrderShippingRecord?.homeType ?? self.fromHomeType
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                guard let self = self else { return }
                self.setupNavigationController()
            }
            
            if self.fromHomeType == .notShipping {
                // nếu chưa vận chuyển
                // cần check khoảng cách khi nhận hàng
                
                self.handleLocation()
            }
            
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.data.subscribe(onNext: { [weak self] (_) in
            self?.collectionView.reloadData()
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.cargoTypes.subscribe(onNext: { [weak self] (_) in
            self?.collectionView.reloadData()
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.selectedCargoType.subscribe(onNext: { [weak self] (selectedCargoType) in
            guard let self = self else { return }
            
            self.collectionView.reloadData()
            
            for (index, cargoType) in self.viewModel.output.cargoTypes.value.enumerated()
                where cargoType.id == selectedCargoType?.id {
                    
                    self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 1),
                                                     at: .init(),
                                                     animated: true)
                    
                    self.cargoTypesCollectionView?.scrollToItem(at: IndexPath(item: index, section: 0),
                                                                at: .centeredHorizontally,
                                                                animated: true)
                    
            }
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.output.isEnableConfirmButton.subscribe(onNext: { [weak self] (isEnableConfirmButton) in
            guard let self = self else { return }
            self.confirmButton.isEnabled = isEnableConfirmButton
            self.confirmButton.backgroundColor = (isEnableConfirmButton ? AppColor.hex289767 : AppColor.hexA4A8AC)
            
        }).disposed(by: viewModel.disposeBag)
        
        
        viewModel.input.isEditting.subscribe(onNext: { [weak self] (_) in
            guard let self = self else { return }
            
            self.collectionView.reloadData()
            self.handleRightBarButtonItem()
            
            self.viewModel.updateEnableConfirmButton()
            
        }).disposed(by: viewModel.disposeBag)
    }
}

extension DetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellNibName: DetailInfoCollectionViewCell.className)
        collectionView.register(cellNibName: CargoTypesCollectionViewCell.className)
        collectionView.register(cellNibName: QrItemCollectionViewCell.className)
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        
        collectionView.collectionViewLayout = layout
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var number = 0
        
        // data
        // cargoTypes + selectedCargoType.detailModel
        // selectedCargoType.cargo
        
        if !viewModel.output.data.value.isEmpty {
            number += 1
        }
        
        if !viewModel.output.cargoTypes.value.isEmpty {
            number += 1
        }
        
        if viewModel.output.selectedCargoType.value?.cargos?.isEmpty == false {
            number += 1
        }
        
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            // data
            //  - Địa chỉ kho lấy hàng
            //  - Giao hàng đến
            //  - Danh sách hàng hóa nhận
            return viewModel.output.data.value.count
            
        case 1:
            var number = 0
            
            // cargoTypes
            // selectedCargoType.detailModel
            
            if !viewModel.output.cargoTypes.value.isEmpty {
                number += 1
            }
            
            if viewModel.output.selectedCargoType.value?.detailModel != nil {
                number += 1
            }
            
            return number
            
        case 2:
            // selectedCargoType.cargo
            let cargo = viewModel.output.selectedCargoType.value?.cargos ?? []
            return cargo.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            // data
            //  - Địa chỉ kho lấy hàng
            //  - Giao hàng đến
            //  - Danh sách hàng hóa nhận
            
            guard let cell = collectionView.dequeue(DetailInfoCollectionViewCell.self, indexPath) else {
                return UICollectionViewCell()
            }
            
            let detailModel = viewModel.output.data.value[indexPath.item]
            cell.setup(detailModel: detailModel, isEditting: viewModel.input.isEditting.value)
            
            cell.reasonClosure = { [weak self] in
                guard let self = self else { return }
                
                let getListBiddingOrderShippingRecord = self.viewModel.output.getListBiddingOrderShippingRecord.value
                ReasonView.show(title: "Lý do hủy".localized(),
                                detail: getListBiddingOrderShippingRecord?.note ?? "")
            }
            
            cell.editClosure = { [weak self] in
                guard let self = self else { return }
                
                if !self.viewModel.input.isEditting.value {
                    ConfirmDialogView.show(title: "Xác nhận chỉnh sửa nhận hàng!".localized(), yesTitle: "Xác nhận".localized(), noTitle: "Huỷ bỏ".localized(), yesAction: { [weak self] in
                        
                        guard let self = self else { return }
                        self.viewModel.input.isEditting.accept(true)
                        
                        }, noAction: nil)
                } else {
                    
                    self.viewModel.input.isEditting.accept(false)
                }
            }
            
            cell.mapClosure = { [weak self] in
                guard let self = self, let fromHomeType = self.fromHomeType else { return }
                
                self.mainCoordinator?
                    .goToVanMapsVC(getListBiddingOrderShippingRecord:
                        self.viewModel.output.getListBiddingOrderShippingRecord.value,
                                   fromHomeType: fromHomeType)
            }
            
            return cell
            
        case 1:
            // cargoTypes
            
            if indexPath.item == 0 {
                guard let cell = collectionView.dequeue(CargoTypesCollectionViewCell.self, indexPath) else {
                    return UICollectionViewCell()
                }
                
                let cargoTypes = viewModel.output.cargoTypes.value
                let selectedCargoType = viewModel.output.selectedCargoType.value
                cell.setup(cargoTypes: cargoTypes, selectedCargoType: selectedCargoType)
                cell.didSelectItemAt = { [weak self] index in
                    guard let self = self else { return }
                    self.viewModel.output.selectedCargoType
                        .accept(self.viewModel.output.cargoTypes.value[index])
                }
                
                cargoTypesCollectionView = cell.collectionView
                
                return cell
            }
            
            // selectedCargoType.detailModel
            
            guard let cell = collectionView.dequeue(DetailInfoCollectionViewCell.self, indexPath) else {
                return UICollectionViewCell()
            }
            if let selectedCargoType = viewModel.output.selectedCargoType.value,
                let detailModel = selectedCargoType.detailModel {
                cell.setup(detailModel: detailModel, isEditting: viewModel.input.isEditting.value)
            }
            return cell
            
        case 2:
            // selectedCargoType.cargo
            
            guard let cell = collectionView.dequeue(QrItemCollectionViewCell.self, indexPath) else {
                return UICollectionViewCell()
            }
            
            let cargo = viewModel.output.selectedCargoType.value?.cargos ?? []
            let qrCode = cargo[indexPath.item].qrCode ?? ""
            
            cell.codeLabel.text = qrCode
            
            if fromHomeType == .notShipping {
                cell.deleteButton.isHidden = false
            } else if fromHomeType == .shipping {
                cell.deleteButton.isHidden = !viewModel.input.isEditting.value
            } else {
                cell.deleteButton.isHidden = true
            }
            
            cell.deleteClosure = {
                let title = String(format: "Xác nhận xóa mã %@?".localized(), qrCode)
                ConfirmDialogView.show(title: title,
                                       yesTitle: "Xác nhận".localized(),
                                       noTitle: "Huỷ bỏ".localized(),
                                       yesAction: { [weak self] in self?.viewModel.removeQr(code: qrCode) },
                                       noAction: nil)
            }
            
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            // data
            //  - Địa chỉ kho lấy hàng
            //  - Giao hàng đến
            //  - Danh sách hàng hóa nhận
            
            let detailModel = viewModel.output.data.value[indexPath.item]
            return CGSize(width: UIScreen.main.bounds.width, height: detailModel.getCellHeight())
            
        case 1:
            // cargoTypes
            
            if indexPath.item == 0 {
                return CGSize(width: UIScreen.main.bounds.width, height: 50)
            }
            
            // selectedCargoType.detailModel
            let detailModel = viewModel.output.selectedCargoType.value?.detailModel
            return CGSize(width: UIScreen.main.bounds.width,
                          height: detailModel?.getCellHeight() ?? 0)
            
        case 2:
            // selectedCargoType.qrArray
            
            return CGSize(width: UIScreen.main.bounds.width / 2, height: 45)
            
        default:
            return .zero
        }
    }
}
