//
//  SettingsVC.swift
//  Driver
//
//  Created by Hieu Dinh on 8/29/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        isShowNavigationSeparator = true
        
        setupNavigationController(title: "Cài đặt".localized())
    }
}
