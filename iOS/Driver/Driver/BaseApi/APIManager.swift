//
//  APIManager.swift
//  OdooCustomer
//
//  Created by dong luong on 8/2/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import Alamofire
import Foundation
import KRProgressHUD
import Reachability

class APIManager: NSObject {
    let reachability = try! Reachability()
    
    // API register with email
    static func register(email: String, username: String, displayName: String, gender: String, password: String,
                         phoneNumber: String, flag: String, idSecret: String, avatar: String,
                         showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.registerEmail,
                        parameters: [
                            "email": email,
                            "username": username,
                            "displayName": displayName,
                            "gender": gender,
                            "password": password,
                            "phoneNumber": phoneNumber,
                            "flag": flag,
                            "idSecret": idSecret,
                            "avatar": avatar
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    static func registerToken(deviceId: String, userId: String, deviceToken: String, os: String,
                              showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.postToken,
                        parameters: [
                            "deviceId": deviceId,
                            "userId": userId,
                            "deviceToken": deviceToken,
                            "os": os
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // api post bill sell
    static func postBillListSell(userSellId: String, statusSell: String, billType: String, page: String,
                                 showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.postSlug(.postManagerOrder,
                            slug: userSellId,
                            parameters: [
                                "statusSell": statusSell,
                                "billType": billType,
                                "page": page
                            ],
                            
                            showIndicator: showIndicator,
                            completion: handler)
    }
    
    // api update status bill sell
    static func postUpdateStatusBill(billId: String, userType: String, billType: String, status: String, page: String,
                                     showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.postSlug(.postUpdateBill,
                            slug: billId,
                            parameters: [
                                "userType": userType,
                                "billType": billType,
                                "status": status,
                                "page": page
                            ],
                            
                            showIndicator: showIndicator,
                            completion: handler)
    }
    
    // api get list food bill
    static func getListFoodBill(id: String,
                                showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.getListFoodBill,
                        parameters: [
                            "id": id
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API login with facebook
    static func loginFacebook(email: String, phoneNumber: String, idSecret: String,
                              showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.loginFacebook,
                        parameters: [
                            "email": email,
                            "phoneNumber": phoneNumber,
                            "idSecret": idSecret
                            
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API update  profile
    static func updateProfile(userID: String, displayName: String, email: String, address: String, userName: String,
                              phoneNumber: String,
                              showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.postUpdateProfile(.registerEmail, slug: "/" + userID,
                                     parameters: ["displayName": displayName,
                                                  "email": email,
                                                  "address": address,
                                                  "userName": userName,
                                                  "phoneNumber": phoneNumber],
                                     
                                     showIndicator: showIndicator,
                                     completion: handler)
    }
    
    // API update password
    static func updatePass(userID: String, pass: String,
                           showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.postUpdateProfile(.updatePass, slug: "/" + userID,
                                     parameters: ["pass": pass],
                                     
                                     showIndicator: showIndicator,
                                     completion: handler)
    }
    
    // API update  facebook profile
    static func updateProfile(userID: String, displayName: String, email: String, userName: String,
                              phoneNumber: String,
                              showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.postUpdateProfile(.registerEmail, slug: "/" + userID,
                                     parameters: ["displayName": displayName,
                                                  "email": email,
                                                  "userName": userName,
                                                  "phoneNumber": phoneNumber],
                                     
                                     showIndicator: showIndicator,
                                     completion: handler)
    }
    
    // api get list bill
    static func getListBillBuyHistory(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListBillBuy,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // api get detail bill
    static func getDetailBill(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.buyProduct,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // API update account
    static func updateAccountImage(typeBackground: Bool, slug: String, displayName: String, email: String, userName: String, avaBackType: String, avatar: UIImage, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        var parameters: Parameters = [:]
        if displayName != "" {
            parameters["displayName"] = displayName
        }
        if email != "" {
            parameters["email"] = email
        }
        if userName != "" {
            parameters["userName"] = userName
        }
        if avaBackType != "" {
            parameters["avaBackType"] = avaBackType
        }
        
        if typeBackground {
            APIManager.uploadImageAndData(.registerEmail, slug: slug,
                                          dataParameter: parameters,
                                          arrayParameter: Parameters(),
                                          dictParameter: Parameters(),
                                          imageParameter: ["background": avatar],
                                          listImageParameter: [UIImage](),
                                          showIndicator: showIndicator,
                                          completion: handler)
        } else {
            APIManager.uploadImageAndData(.registerEmail, slug: slug,
                                          dataParameter: parameters,
                                          arrayParameter: Parameters(),
                                          dictParameter: Parameters(),
                                          imageParameter: ["avatar": avatar],
                                          listImageParameter: [UIImage](),
                                          showIndicator: showIndicator,
                                          completion: handler)
        }
    }
    
    // API upload anh bien lai
    static func updateBienLai(slug: String, receipt: UIImage, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        let parameters: Parameters = [:]
        APIManager.uploadImageAndData(.upLoadImageBienLai, slug: slug,
                                      dataParameter: parameters,
                                      arrayParameter: Parameters(),
                                      dictParameter: Parameters(),
                                      imageParameter: ["receipt": receipt],
                                      listImageParameter: [UIImage](),
                                      showIndicator: showIndicator,
                                      completion: handler)
    }
    
    // API buy product
    static func buyFood(userName: String, email: String, note: String, address: String, type: String, products: [[String: String]], phoneNumber: String, statusBuy: String, statusSell: String,
                        userBuyId: String, userSellId: String, total_price: String,
                        showIndicator: Bool, images: [UIImage], handler: @escaping ApiResultHandler) {
        APIManager.uploadImageAndData(.buyProduct, typePost: false,
                                      dataParameter: ["userName": userName,
                                                      "email": email,
                                                      "note": note,
                                                      "address": address,
                                                      "type": type,
                                                      "phoneNumber": phoneNumber,
                                                      "statusBuy": statusBuy,
                                                      "statusSell": statusSell,
                                                      "userBuyId": userBuyId,
                                                      "userSellId": userSellId,
                                                      "total_price": total_price],
                                      listImageParameter: images,
                                      dictParameter: ["products": products],
                                      showIndicator: showIndicator,
                                      completion: handler)
    }
    
    // api login with email
    static func login(userEmail: String, userPass: String,
                      showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.registerEmail,
                           slug: "/" + userEmail,
                           slug2: "/" + userPass,
                           loadMore: "",
                           parameters: nil,
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // check register facebook
    static func checkRegisterFacebook(idSecret: String,
                                      showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.checkRegisterFacebook,
                           slug: "/" + idSecret,
                           slug2: "",
                           loadMore: "",
                           parameters: nil,
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListPost(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getPostList,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // api get detail post
    static func getDetailProduct(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.detailProduct,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListFollowPost(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListFollowPost,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListPostRecently(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListPostRecently,
                           slug: "",
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListFollow(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListFollow,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListMyPost(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListMyPost,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getUser(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.registerEmail,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    static func getListBank(slug: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.bank,
                           slug: slug,
                           slug2: "",
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // postBank
    static func postBank(userId: String, bankNumber: String, bankName: String, bankUserName: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(apiURL.bank,
                        parameters: [
                            "userId": userId,
                            "bankNumber": bankNumber,
                            "bankName": bankName,
                            "bankUserName": bankUserName
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // review Post
    static func postReviewPost(billId: String, userId: String, postId: String, reviewBody: String, shopId: String, star: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(apiURL.review_posts,
                        parameters: [
                            "billId": billId,
                            "userId": userId,
                            "postId": postId,
                            "reviewBody": reviewBody,
                            "shopId": shopId,
                            "star": star
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    static func postFood(title: String, slug: String, body: String, userId: String, prices: String, sale: String, unitPrices: String, quantity: String,
                         quantityWarehouse: String,
                         unitQuantity: String, productName: String, description: String, shipFee: String,
                         freeShip: String, location: String, type: String,
                         showIndicator: Bool, images: [UIImage], handler: @escaping ApiResultHandler) {
        APIManager.uploadImageAndData(.getPostList, typePost: true,
                                      dataParameter: ["title": title,
                                                      "slug": slug,
                                                      "body": body,
                                                      "userId": userId,
                                                      "prices": prices,
                                                      "sale": sale,
                                                      "unitPrices": unitPrices,
                                                      "quantity": quantity,
                                                      "quantityWarehouse": quantityWarehouse,
                                                      "unitQuantity": unitQuantity,
                                                      "productName": productName,
                                                      "description": description,
                                                      "shipFee": shipFee,
                                                      "freeShip": freeShip,
                                                      "location": location,
                                                      "type": type],
                                      listImageParameter: images,
                                      showIndicator: showIndicator,
                                      completion: handler)
    }
    
    // getComment
    static func getComment(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.comment,
                           slug: slug,
                           slug2: slug2,
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // getReview
    static func getReview(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.review_posts,
                           slug: slug,
                           slug2: slug2,
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // postComment
    static func postComment(userId: String, postId: String, comment: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(apiURL.comment,
                        parameters: [
                            "userId": userId,
                            "postId": postId,
                            "comment": comment
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // check save post
    static func checkFollowPost(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListFollowPost,
                           slug: slug,
                           slug2: slug2,
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // check follower
    static func checkFollowUser(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.getListFollow,
                           slug: slug,
                           slug2: slug2,
                           loadMore: "",
                           parameters: ["page": page],
                           showIndicator: showIndicator,
                           completion: handler)
    }
    
    // save post
    static func savePost(userId: String, postId: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(apiURL.getListFollowPost,
                        parameters: [
                            "userId": userId,
                            "postId": postId
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // post follow
    static func postFollow(shopId: String, customerId: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(apiURL.getListFollow,
                        parameters: [
                            "shopId": shopId,
                            "customerId": customerId
                        ],
                        
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // delete post
    static func deletePost(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.deleteSlug(apiURL.getListFollowPost,
                              slug: slug,
                              slug2: slug2,
                              loadMore: "",
                              parameters: ["page": page],
                              showIndicator: showIndicator,
                              completion: handler)
    }
    
    // delete follow
    static func deleteFollow(slug: String, slug2: String, page: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.deleteSlug(apiURL.getListFollow,
                              slug: slug,
                              slug2: slug2,
                              loadMore: "",
                              parameters: ["page": page],
                              showIndicator: showIndicator,
                              completion: handler)
    }
    
    // API log in with email
    static func startup(handler: @escaping ApiResultHandler) {
        APIManager.get(.startUp,
                       showIndicator: false,
                       completion: handler)
    }
    
    // API push token
    static func pushToken(byToken token: String, handler: @escaping ApiResultHandler) {
        APIManager.post(.syncPushToken,
                        parameters: ["push_token": token],
                        showIndicator: false,
                        completion: handler)
    }
    
    // API log in with email
    static func login(email: String, password: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.loginEmail, parameters: ["email": email,
                                                  "password": password],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API log in with social
    static func loginSocial(access_token: String, provider: Provider, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.socialLogin, parameters: ["access_token": access_token,
                                                   "provider": provider],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API verify phone
    static func verifyPhone(access_token: String, phone: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.verifyPhone,
                        parameters: ["access_token": access_token,
                                     "phone": phone],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API forgot password
    static func forgotPassword(email: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.forgotPassword, parameters: ["email": email], showIndicator: showIndicator, completion: handler)
    }
    
    // API get Recipes
    static func getRecipes(limit: String, type: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getRecipes,
                       parameters: ["limit": limit,
                                    "type": type],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API load more recipes
    static func getLoadMoreRecipes(limit: String, type: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getRecipes,
                       loadMore: lastId,
                       parameters: ["limit": limit,
                                    "type": type],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API get recipe detail
    static func getRecipeDetail(recipeId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getRecipeDetail, loadMore: "",
                       parameters: ["recipe_id": recipeId],
                       showIndicator: showIndicator, completion: handler)
    }
    
    // API favorite recipe
    static func favoriteRecipe(idRecipe: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.favoriteRecipe, parameters: ["recipe_id": idRecipe], showIndicator: showIndicator, completion: handler)
    }
    
    // API search recipe by title
    static func searchRecipe(byTitle title: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.searchRecipe,
                       loadMore: lastId,
                       parameters: ["title": title],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API rate recipe
    static func rateRecipe(byRecipeId recipeId: NSNumber, rate: NSNumber, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.rateRecipe,
                        parameters: ["recipe_id": recipeId,
                                     "rate": rate],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API get recipe popular
    static func getRecipePopular(showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getRecipePopular,
                       parameters: nil,
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API post recipe
    
    // API search food order
    static func searchFoodOrder(byTitle title: String, limit: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.orderFood,
                       parameters: ["limit": limit,
                                    "title": title],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API load more food order
    static func searchLoadMoreFoodOrder(byTitle title: String, limit: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.orderFood,
                       loadMore: lastId,
                       parameters: ["limit": limit,
                                    "title": title],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API find cooker by coordinate
    static func searchCookerByCoordinate(byLat lat: String, lng: String, radius: String, limit: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        if radius == "" {
            APIManager.get(.findCookerByCoordinate,
                           parameters: ["limit": limit,
                                        "lat": lat,
                                        "lng": lng],
                           showIndicator: showIndicator,
                           completion: handler)
        } else {
            APIManager.get(.findCookerByCoordinate,
                           parameters: ["limit": limit,
                                        "lat": lat,
                                        "lng": lng,
                                        "radius": radius],
                           showIndicator: showIndicator,
                           completion: handler)
        }
    }
    
    // API loadmore find cooker by coordinate
    static func searchLoadMoreCookerByCoordinate(byLat lat: String, lng: String, radius: String, limit: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        if radius == "" {
            APIManager.get(.findCookerByCoordinate,
                           loadMore: lastId,
                           parameters: ["limit": limit,
                                        "lat": lat,
                                        "lng": lng],
                           showIndicator: showIndicator,
                           completion: handler)
        } else {
            APIManager.get(.findCookerByCoordinate,
                           loadMore: lastId,
                           parameters: ["limit": limit,
                                        "lat": lat,
                                        "lng": lng,
                                        "radius": radius],
                           showIndicator: showIndicator,
                           completion: handler)
        }
    }
    
    // API Get List Chef by Condition
    
    static func getListChefByCondition(latitude: String, longtitude: String, limit: String, idFood: String, radius: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getListChefByCondition,
                       loadMore: "",
                       parameters: ["limit": limit,
                                    "lat": latitude,
                                    "lng": longtitude,
                                    "food_id": idFood,
                                    "radius": radius],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    static func getLoadmoreListChefByCondition(latitude: String, longtitude: String, limit: String, idFood: String, radius: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getListChefByCondition,
                       loadMore: lastId,
                       parameters: ["limit": limit,
                                    "lat": latitude,
                                    "lng": longtitude,
                                    "food_id": idFood,
                                    "radius": radius],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API get list favor chef
    static func getListFavorChef(limit: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getFavorCooker,
                       parameters: ["limit": limit],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API get load more list favor chef
    static func getLoadMoreListFavorChef(limit: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getFavorCooker,
                       loadMore: lastId,
                       parameters: ["limit": limit],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // Create Order
    
    //    static func createOrderFood(idCooker:String, orderDetails:[[String:Any]], totalPrice:String, address:String, Latitude:String, Longtitude: String, timeExpect:String, note:String, showIndicator:Bool, handler:@escaping ApiResultHandler) {
    //        APIManager.uploadImageAndData(.createOrderFood,
    //                                      loadMore: "",
    //                                      dataParameter: ["account_id":idCooker,
    //                                                      "total_price":totalPrice,
    //                                                      "address":address,
    //                                                      "lat":Latitude,
    //                                                      "lng":Longtitude,
    //                                                      "note":note,
    //                                                      "expect_time":timeExpect],
    //                                      arrayParameter: Parameters(), dictParameter: ["order_details": ["data":orderDetails]],
    //                                      imageParameter: [:],
    //                                      showIndicator: showIndicator,
    //                                      completion: handler)
    //    }
    
    // Get Food Cooker
    static func getFoodCooker(idCooker: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getFoodsOfCooker, loadMore: lastId, parameters: ["account_id": idCooker], showIndicator: showIndicator, completion: handler)
    }
    
    // Get Order Detail
    static func getOrderDetail(idOrder: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getOrderDetail, loadMore: "", parameters: ["order_id": idOrder], showIndicator: showIndicator, completion: handler)
    }
    
    // API post image pratice
    //    static func postImagePratice(byRecipeId recipeId:String, image:UIImage, descrip: String, showIndicator:Bool, handler:@escaping ApiResultHandler) {
    //        APIManager.uploadImageAndData(.postImagePractice,
    //                                      dataParameter: ["recipe_id": recipeId as Any,
    //                                                      "description": descrip as Any],
    //                                      arrayParameter: Parameters(),
    //                                      dictParameter: Parameters(),
    //                                      imageParameter: ["image" : image],
    //                                      listImageParameter: [UIImage](),
    //                                      showIndicator: showIndicator,
    //                                      completion: handler)
    //    }
    
    // API change order status
    static func changeOrderStatus(byOrderId orderId: String, status: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.changeOrderStatus,
                        parameters: ["order_id": orderId,
                                     "status": status],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API get account
    static func getAccount(showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getAccount, showIndicator: showIndicator, completion: handler)
    }
    
    // API get account
    static func getNotifications(_ user_id: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.getSlug(apiURL.notifications, slug: user_id, slug2: "", loadMore: "", parameters: nil, showIndicator: showIndicator, completion: handler)
    }
    
    // API change password
    static func changePassword(byNewPassword newPassword: String, oldPassword: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.changePassword,
                        parameters: ["old_password": oldPassword,
                                     "new_password": newPassword],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API register cooker
    static func registerCooker(showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.registerCooker, showIndicator: showIndicator, completion: handler)
    }
    
    // API update account
    //    static func updateAccount(byFullName fullName: String, email: String, address: String, gender: String, birthday: String, avatar: UIImage, lat: String, lng: String,openTime: String, closeTime: String, showIndicator: Bool, handler:@escaping ApiResultHandler) {
    //        var parameters: Parameters = [:]
    //        if fullName != "" {
    //            parameters["full_name"] = fullName
    //        }
    //        if email != "" {
    //            parameters["email"] = email
    //        }
    //        if address != "" {
    //            parameters["address"] = address
    //            parameters["lat"] = lat
    //            parameters["lng"] = lng
    //        }
    //        if birthday != "" {
    //            parameters["birthday"] = birthday
    //        }
    //        if gender != "" {
    //            parameters["gender"] = gender
    //        }
    //        if openTime != "" {
    //            parameters["open_time"] = openTime
    //        }
    //        if closeTime != "" {
    //            parameters["close_time"] = closeTime
    //        }
    //        if avatar.size.width == 0 {
    //            APIManager.post(.updateAccount,
    //                            parameters: parameters,
    //                            showIndicator: showIndicator,
    //                            completion: handler)
    //        }
    //        else {
    //            APIManager.uploadImageAndData(.updateAccount,
    //                                          dataParameter: parameters,
    //                                          arrayParameter: Parameters(),
    //                                          dictParameter: Parameters(),
    //                                          imageParameter: ["avatar": avatar],
    //                                          listImageParameter: [UIImage](),
    //                                          showIndicator: showIndicator,
    //                                          completion: handler)
    //        }
    //    }
    
    // API get history order
    static func getHistoryOrder(limit: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.historyOrder,
                       parameters: ["limit": limit],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API load more history order
    static func getLoadmoreHistoryOrder(limit: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.historyOrder,
                       loadMore: lastId,
                       parameters: ["limit": limit],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // APi get list share food
    static func getListShareFood(limit: String, lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.listShareFood,
                       loadMore: lastId,
                       parameters: ["limit": limit],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API get detail share food
    static func getDetailShareFood(postId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.detailShareFood,
                       parameters: ["post_id": postId],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API get post comment
    static func getPostComment(limit: String, lastId: String, postId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getComment,
                       loadMore: lastId,
                       parameters: ["limit": limit,
                                    "post_id": postId],
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    // API like unlike post
    static func likePost(postId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.post(.likePost,
                        parameters: ["post_id": postId],
                        showIndicator: showIndicator,
                        completion: handler)
    }
    
    // API like unlike cooker
    static func likeCooker(cookerId: String, handler: @escaping ApiResultHandler) {
        APIManager.post(.likeCooker,
                        parameters: ["cooker_id": cookerId],
                        showIndicator: false,
                        completion: handler)
    }
    
    //    static func createPost(title: String, content: String, image: UIImage, handler:@escaping ApiResultHandler) {
    //        APIManager.uploadImageAndData(.createPost,
    //                                      method: .post,
    //                                      dataParameter: ["title": title,
    //                                                      "content": content],
    //                                      arrayParameter: Parameters(),
    //                                      dictParameter: Parameters(),
    //                                      imageParameter: ["image": image],
    //                                      listImageParameter: [UIImage](),
    //                                      showIndicator: false,
    //                                      completion: handler)
    //    }
    
    static func addFood(foodId: String, price: String, handler: @escaping ApiResultHandler) {
        APIManager.post(.addFood,
                        parameters: ["food_id": foodId,
                                     "price": price],
                        showIndicator: false,
                        completion: handler)
    }
    
    static func delFood(accountFoodId: String, handler: @escaping ApiResultHandler) {
        APIManager.get(.delFood,
                       parameters: ["account_food_id": accountFoodId],
                       showIndicator: false,
                       completion: handler)
    }
    
    static func getOwnRecipe(lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getOwnRecipe,
                       loadMore: lastId,
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    static func getFavorRecipe(lastId: String, showIndicator: Bool, handler: @escaping ApiResultHandler) {
        APIManager.get(.getFavorRecipe,
                       loadMore: lastId,
                       showIndicator: showIndicator,
                       completion: handler)
    }
    
    static func shareRecipe(recipeId: String, handler: @escaping ApiResultHandler) {
        APIManager.post(.shareRecipe,
                        parameters: ["recipe_id": recipeId],
                        showIndicator: true,
                        completion: handler)
    }
    
    static func sharePost(postId: String, handler: @escaping ApiResultHandler) {
        APIManager.post(.sharePost,
                        parameters: ["post_id": postId],
                        showIndicator: true,
                        completion: handler)
    }
    
    static func getRecipePracices(recipeId: String, handler: @escaping ApiResultHandler) {
        APIManager.get(.getRecipePractice,
                       parameters: ["recipe_id": recipeId],
                       showIndicator: false,
                       completion: handler)
    }
    
    static func getSuggestFood(handler: @escaping ApiResultHandler) {
        APIManager.get(.getSuggestFood,
                       showIndicator: false,
                       completion: handler)
    }
    
    static func get(_ api: apiURL, loadMore: String = "", parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        request(api, loadMore: loadMore, method: .get, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    static func getSlug(_ api: apiURL, slug: String = "", slug2: String = "", loadMore: String = "", parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        requestSlug(api, slug: slug, slug2: slug2, loadMore: loadMore, method: .get, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    static func postSlug(_ api: apiURL, slug: String = "", slug2: String = "", loadMore: String = "", parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        requestSlug(api, slug: slug, slug2: slug2, loadMore: loadMore, method: .post, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    static func deleteSlug(_ api: apiURL, slug: String = "", slug2: String = "", loadMore: String = "", parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        requestSlug(api, slug: slug, slug2: slug2, loadMore: loadMore, method: .delete, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    static func post(_ api: apiURL, loadMore: String = "", parameters: Parameters = [:], headers: HTTPHeaders = [:], showIndicator: Bool, completion: @escaping ApiResultHandler) {
        request(api, loadMore: loadMore, method: .post, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    static func postUpdateProfile(_ api: apiURL, slug: String = "", loadMore: String = "", parameters: Parameters = [:], headers: HTTPHeaders = [:], showIndicator: Bool, completion: @escaping ApiResultHandler) {
        requestSlug(api, slug: slug, slug2: "", loadMore: loadMore, method: .post, parameters: parameters, showIndicator: showIndicator, completion: completion)
    }
    
    private static func checkRappleActivityIndicator(api: String) -> Bool {
        if api == apiURL.loginEmail.rawValue ||
            api == apiURL.registerEmail.rawValue ||
            api == apiURL.socialLogin.rawValue ||
            api == apiURL.verifyPhone.rawValue ||
            api == apiURL.forgotPassword.rawValue ||
            api == apiURL.postRecipe.rawValue {
            return true
        }
        return false
    }
    
    private static func uploadImageAndData(_ api: apiURL, loadMore: String = "", typePost: Bool, method: HTTPMethod = .post, dataParameter: Parameters? = nil, listImageParameter: [UIImage] = [UIImage](), dictParameter: Parameters? = nil, headers: HTTPHeaders = [:], showIndicator: Bool, completion: @escaping ApiResultHandler) {
        if showIndicator {
            KRProgressHUD.show()
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in dataParameter! {
                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
            if !typePost {
                for (key, value) in dictParameter! {
                    if let data = try? JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.sortedKeys) {
                        multipartFormData.append(data, withName: key)
                    }
                }
            }
            
            var countImage = 0
            for image in listImageParameter {
                if image.size.width != 0 {
                    let time = Date().timeIntervalSinceReferenceDate
                    //                    let temp = 0.2
                    //                    let imageData = UIImageJPEGRepresentation(image, CGFloat(temp))
                    let imageData = image.resize()
                    //                        multipartFormData.append(imageData, withName: "photos[\(countImage + 1)]", fileName: "foodStep\(countImage + 1).jpg", mimeType: "image/jpg")
                    if typePost {
                        multipartFormData.append(imageData, withName: "photos[\(countImage + 1)]", fileName: "foodStep\(time + 1).jpg", mimeType: "image/jpg")
                        countImage += 1
                    } else {
                        multipartFormData.append(imageData, withName: "receipt[\(countImage + 1)]", fileName: "foodStep\(time + 1).jpg", mimeType: "image/jpg")
                        countImage += 1
                    }
                }
            }
            
        }, usingThreshold: UInt64(),
                         to: api.url + loadMore, method: HTTPMethod.post,
                         headers: nil, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload
                    .validate()
                    .responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            guard let data = value as? [String: Any],
                                let returnCode = data["statusCode"] as? Int else {
                                completion(.failure(.http))
                                return
                            }
                            switch returnCode {
                            case 200:
                                if typePost {
                                    KRProgressHUD.showMessage("Đăng bài thành công")
                                }
                                
                                KRProgressHUD.dismiss()
                                
                                completion(.success(data))
                                
                            default:
                                if let message = data["message"] as? String {
                                    if showIndicator {
                                        KRProgressHUD.showWarning(withMessage: message)
                                    }
                                    completion(.failure(.api(message)))
                                }
                            }
                        case .failure(let responseError):
                            print("responseError: \(responseError)")
                            if showIndicator {
                                KRProgressHUD.showWarning(withMessage: ApiError.http.message)
                            }
                        }
                    }
            case .failure(let encodingError):
                print("encodingError: \(encodingError)")
                if showIndicator {
                    KRProgressHUD.dismiss()
                }
            }
        })
    }
    
    private static func uploadImageAndData(_ api: apiURL, slug: String, loadMore: String = "", method: HTTPMethod = .post, dataParameter: Parameters? = nil, arrayParameter: Parameters? = nil, dictParameter: Parameters? = nil, imageParameter: [String: UIImage]? = nil, listImageParameter: [UIImage] = [UIImage](), headers: HTTPHeaders = [:], showIndicator: Bool, completion: @escaping ApiResultHandler) {
        if showIndicator {
            KRProgressHUD.show()
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in dataParameter! {
                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
            for (key, value) in dictParameter! {
                if let data = try? JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.sortedKeys) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
            for (key, value) in arrayParameter! {
                var count = 0
                for obj in value as! NSArray {
                    if let data = (obj as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                        multipartFormData.append(data, withName: key + "[\(count + 1)]")
                        count += 1
                    }
                }
            }
            
            for (key, value) in imageParameter! {
                //                let temp = 0.2
                //                let imageData = UIImageJPEGRepresentation(value, CGFloat(temp))
                let time = Date().timeIntervalSinceReferenceDate
                let imageData = value.resize()
                multipartFormData.append(imageData, withName: key, fileName: "foodStep\(time + 1).jpg", mimeType: "image/jpg")
            }
            
            var countImage = 0
            for image in listImageParameter {
                if image.size.width != 0 {
                    let time = Date().timeIntervalSinceReferenceDate
                    //                    let temp = 0.2
                    //                    let imageData = UIImageJPEGRepresentation(image, CGFloat(temp))
                    let imageData = image.resize()
                    //                        multipartFormData.append(imageData, withName: "photos[\(countImage + 1)]", fileName: "foodStep\(countImage + 1).jpg", mimeType: "image/jpg")
                    multipartFormData.append(imageData, withName: "photos[\(countImage + 1)]", fileName: "foodStep\(time + 1).jpg", mimeType: "image/jpg")
                    countImage += 1
                }
            }
            
        }, usingThreshold: UInt64(),
                         to: api.url + slug + loadMore, method: HTTPMethod.post,
                         headers: nil, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload
                    .validate()
                    .responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            guard let data = value as? [String: Any],
                                let returnCode = data["statusCode"] as? Int else {
                                completion(.failure(.http))
                                return
                            }
                            switch returnCode {
                            case 200:
                                if (data["message"] as? String) != nil {
                                    KRProgressHUD.dismiss()
                                    
                                    completion(.success(data))
                                }
                            default:
                                if let message = data["message"] as? String {
                                    if showIndicator {
                                        KRProgressHUD.showWarning(withMessage: message)
                                    }
                                    completion(.failure(.api(message)))
                                }
                            }
                        case .failure(let responseError):
                            print("responseError: \(responseError)")
                            if showIndicator {
                                KRProgressHUD.showWarning(withMessage: ApiError.http.message)
                            }
                        }
                    }
            case .failure(let encodingError):
                print("encodingError: \(encodingError)")
                if showIndicator {
                    KRProgressHUD.dismiss()
                }
            }
        })
    }
    
    private static func request(_ api: apiURL, loadMore: String = "", method: HTTPMethod = .get, parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        if showIndicator {
            KRProgressHUD.show()
        }
        print("url ->>>>> ", api.url + loadMore)
        let reguest = Alamofire.request(api.url + loadMore, method: method, parameters: parameters, headers: nil)
        reguest.responseJSON { response in
            switch response.result {
            case .success(let value):
                guard let data = value as? [String: Any],
                    let returnCode = data["statusCode"] as? Int else {
                    completion(.failure(.http))
                    return
                }
                switch returnCode {
                case 200:
                    
                    if (data["message"] as? String) != nil {
                        KRProgressHUD.dismiss()
                        
                        completion(.success(data))
                    }
                default:
                    if let message = data["message"] as? String {
                        if showIndicator {
                            KRProgressHUD.showWarning(withMessage: message)
                        }
                        completion(.failure(.api(message)))
                    }
                }
            case .failure:
                if showIndicator {
                    KRProgressHUD.dismiss()
                }
                completion(.failure(.http))
            }
        }
    }
    
    private static func requestSlug(_ api: apiURL, slug: String = "", slug2: String = "", loadMore: String = "", method: HTTPMethod = .get, parameters: Parameters? = nil, showIndicator: Bool, completion: @escaping ApiResultHandler) {
        if showIndicator {
            KRProgressHUD.show()
        }
        print("url ->>>>> ", api.url + slug + slug2 + loadMore)
        let reguest = Alamofire.request(api.url + slug + slug2 + loadMore, method: method, parameters: parameters, headers: nil)
        reguest.responseJSON { response in
            switch response.result {
            case .success(let value):
                guard let data = value as? [String: Any],
                    let returnCode = data["statusCode"] as? Int else {
                    completion(.failure(.http))
                    return
                }
                switch returnCode {
                case 200:
                    
                    if (data["message"] as? String) != nil {
                        KRProgressHUD.dismiss()
                        
                        completion(.success(data))
                    }
                default:
                    if let message = data["message"] as? String {
                        if showIndicator {
                            KRProgressHUD.showWarning(withMessage: message)
                        }
                        completion(.failure(.api(message)))
                    }
                }
            case .failure:
                if showIndicator {
                    KRProgressHUD.dismiss()
                }
                completion(.failure(.http))
            }
        }
    }
}

