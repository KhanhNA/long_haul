//
//  BaseApiManager.swift
//  OdooCustomer
//
//  Created by dong luong on 7/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import Alamofire
import Foundation
import KRProgressHUD
import ObjectMapper

class BaseApiManager: NSObject {
    
    static func loginOdoo(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<ResultResponceLogin>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.loginOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func getInforloginOdoo(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<ResultResponceLogin>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.getInforLogin.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func updateProfileOdoo(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<ResultResponceLogin>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.updateProfileLogin.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func creatOrder(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ModelCreatOrder?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.creatOrder.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    //    static func getRoutingPlanDay(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<RoutingDay>>?, _ error: Error?) -> Void) {
    //        BaseClient.shared.requestAPI(apiURL.getRoutingPlanDay.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    //    }
    
    //    static func getRoutingPlanDayDetail(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<RoutingDay>>?, _ error: Error?) -> Void) {
    //        BaseClient.shared.requestAPI(apiURL.getRoutingPlanDayDetail.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    //    }
    
    //    static func getRoutingPlanDayDetailForId(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<RoutingDetail>>?, _ error: Error?) -> Void) {
    //        BaseClient.shared.requestAPI(apiURL.getRoutingDetailId.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    //    }
    
    static func searchWarehouse(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<Warehouse>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.searchReadOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func getWarehouse(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<Warehouse>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.getWareHouseOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func checkWarehouseInfo(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: BaseResultResponse<Area>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.checkWareHouseOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    
    static func getDetailPlaceID(placeId: String, showIndicator: Bool, completion: @escaping (_ data: PlaceBaseResponse?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.detailPlaceID.urlMap + "?place_id=" + placeId +  "&fields=formatted_address,address_component" + "&key=" + GOOGLE_API_KEY , method: .get, parameters: nil, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    
    static func searchTextMap(keySearch: String, showIndicator: Bool, completion: @escaping (_ data: SearchTextMapResponse?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.searchTextMapOdoo.urlMap + "?query=" + keySearch + "&key=" + GOOGLE_API_KEY , method: .get, parameters: nil, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func getListService(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<ServiceModel>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.listServiceOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func getListInsurance(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<Insurance>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.listInsuranceOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    //    static func getListProductType(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<ProductType>>?, _ error: Error?) -> Void) {
    //        BaseClient.shared.requestAPI(apiURL.listProductTypeOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    //    }
    
    static func getListSubscribeType(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<Subscribe>>?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.listSubcribeOdoo.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    //    static func getListBillLadingHistory(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: ResponseModel<BaseResultResponse<BillLading>>?, _ error: Error?) -> Void) {
    //        BaseClient.shared.requestAPI(apiURL.listBillLadingHistory.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    //    }
    
    
    
    static func getListNotification(_ parameters: [String: Any], showIndicator: Bool, completion: @escaping (_ data: BaseData?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.notifications.url, method: .post, parameters: parameters, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func getDetailNotification(_ notiId: String, showIndicator: Bool, completion: @escaping (_ data: NotificationDetail?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.notificationDetail.url + "/" + notiId, method: .get, parameters: nil, headers: nil, showIndicator: showIndicator, completion: completion)
    }
    
    static func updateStatusNotification(_ notiId: String, showIndicator: Bool, completion: @escaping (_ data: BaseData?, _ error: Error?) -> Void) {
        BaseClient.shared.requestAPI(apiURL.notificationUpdate.url + "/" + notiId, method: .post, parameters: nil, headers: nil, showIndicator: showIndicator, completion: completion)
    }
}

public class BaseClient: SessionManager {
    public static let shared: BaseClient = {
        let configuration = URLSessionConfiguration.default
        configuration.allowsCellularAccess = false
        configuration.timeoutIntervalForRequest = TimeInterval(90)
        configuration.timeoutIntervalForResource = TimeInterval(90)
        return BaseClient(configuration: configuration)
    }()
    
    // MARK: - Handle Request
    private func _requestAPI(_ url: URLConvertible,
                             method: HTTPMethod,
                             parameters: Parameters?,
                             headers: HTTPHeaders? = nil,
                             showIndicator: Bool? = nil,
                             completion: @escaping (Data?, Error?) -> Void) {
        
        if let showIndicator = showIndicator {
            showIndicator ? KRProgressHUD.show() : KRProgressHUD.dismiss()
        }
        
        let encoding: ParameterEncoding = (method == .get ? URLEncoding.queryString : JSONEncoding.default)
        self.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            
            print("===========================================================================================")
            print("URL request -> ", url)
            
            if let jsonData = try? JSONSerialization.data(withJSONObject: parameters ?? Parameters(), options: JSONSerialization.WritingOptions.prettyPrinted) {
                
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
                print("Params ->", jsonString ?? "")
            } else {
                print("Params ->", parameters ?? Parameters())
            }
            
            if let value = response.value, let jsonData = try? JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted) {
                
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
                print("Response ->", jsonString ?? "")
            } else {
                print("Response ->", response.value ?? "")
            }

            if let _ = showIndicator {
                KRProgressHUD.dismiss()
            }
            
            if let error = response.error {
                
                completion(nil, error)
                
            } else if let data = response.data {
                
                completion(data, nil)
                
            } else {
                
                completion(nil, nil)
                
            }
        }
        
    }
    
    public func requestAPI<T: Codable>(_ url: URLConvertible,
                                       method: HTTPMethod,
                                       parameters: Parameters?,
                                       headers: HTTPHeaders? = nil,
                                       showIndicator: Bool,
                                       completion: @escaping (T?, Error?) -> Void) {
        
        _requestAPI(url, method: method, parameters: parameters, headers: headers, showIndicator: showIndicator) { (data, error) in
            if let data = data {
                
                do {
                    
                    let dataJSONDecoder = try JSONDecoder().decode(T.self, from: data)
                    completion(dataJSONDecoder, nil)
                    
                } catch {
                    
                    print("Catch error -> ", error)
                    completion(nil, nil)
                    
                }
                
            } else {
                completion(nil, nil)
            }
        }
    }
    
    public func requestAPIWithMappable<T: Mappable>(_ url: URLConvertible,
                                                    method: HTTPMethod,
                                                    parameters: Parameters?,
                                                    headers: HTTPHeaders? = nil,
                                                    completion: @escaping (T?, Error?) -> Void) {
        
        _requestAPI(url, method: method, parameters: parameters, headers: headers) { (data, error) in
            if let data = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
                    let dataMapper = Mapper<T>().map(JSON: json)
                    completion(dataMapper, nil)
                    
                } catch {
                    
                    print("Catch error -> ", error)
                    completion(nil, nil)
                    
                }
                
            } else {
                completion(nil, nil)
            }
        }
    }
    
    private func _callPostApiMultipleImage(api:String,
                                           params: Parameters?,
                                           arr_images:[UIImage],
                                           Name:String,
                                           mime_type:String = "image/jpg",
                                           File_name:String = "image_" + String(arc4random()) + ".jpg",
                                           completion: @escaping (Data?, Error?) -> Void) {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for i in 0..<arr_images.count {
                multipartFormData.append(arr_images[i].jpegData(compressionQuality: 0.1)!, withName: Name + "\(i)", fileName: File_name, mimeType: mime_type)
            }
            
            for (key, value) in params! {
                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
        }, to: api) { (response) in
            
            switch response {
                
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                
                upload.responseJSON { response in
                    print(response.result)
                    
                    do {
                        
                        let jsonDict = try JSONSerialization.jsonObject(with: (response.data as Data?)!, options: []) as? [String:AnyObject]
                        
                        if response.result.error != nil {
                            
                            print(response.result.error as Any, terminator: "")
                            
                        }
                        
                        if jsonDict == nil {
                            print("Whoops, Something went wrong. Please, try after sometime.")
                            
                            completion(nil, nil)
                            
                            return
                        }
                        
                        print(jsonDict as Any, terminator: "")
                        
                        if response.result.isSuccess {
                            
                            if let error = response.error {
                                
                                completion(nil, error)
                                
                            } else if let data = response.data {
                                
                                completion(data, nil)
                                
                            } else {
                                
                                completion(nil, nil)
                                
                            }
                            
                            // complition(jsonDict as AnyObject)
                        }
                    } catch let error as NSError {
                        
                        print("json error: \(error.localizedDescription)")
                        completion(nil, error)
                        
                    } catch _ {
                        
                        print("Exception!")
                        print("Whoops, Something went wrong. Please, try after sometime.")
                        
                        completion(nil, nil)
                    }
                    
                }
                
            case .failure(let encodingError):
                
                print("",encodingError.localizedDescription)
                completion(nil, encodingError)
                break
                
            }
        }
    }
    
    public func callPostApiMultipleImage<T: Codable>(api:String,
                                                     params: Parameters?,
                                                     arr_images:[UIImage],
                                                     Name:String,
                                                     mime_type:String = "image/jpg",
                                                     File_name:String = "image_" + String(arc4random()) + ".jpg",
                                                     completion: @escaping (T?, Error?) -> ()) {
        
        let handleCompletion: (Data?, Error?) -> () = { data, error in
            guard let data = data else {
                completion(nil, nil)
                return
            }
            
            do {
                
                let dataJSONDecoder = try JSONDecoder().decode(T.self, from: data)
                completion(dataJSONDecoder, nil)
                
            } catch {
                
                print("Catch error -> ", error)
                completion(nil, error)
                
            }
        }
        
        _callPostApiMultipleImage(api: api,
                                  params: params,
                                  arr_images: arr_images,
                                  Name: Name,
                                  mime_type: mime_type,
                                  File_name: File_name,
                                  completion: handleCompletion)
    }
    
    public func callPostApiMultipleImageWithMappable<T: Mappable>(api:String,
                                                                  params: Parameters?,
                                                                  arr_images:[UIImage],
                                                                  Name:String,
                                                                  mime_type:String = "image/jpg",
                                                                  File_name:String = "image_" + String(arc4random()) + ".jpg",
                                                                  completion: @escaping (T?, Error?) -> ()) {
        
        let handleCompletion: (Data?, Error?) -> () = { data, error in
            guard let data = data else {
                completion(nil, nil)
                return
            }
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
                let dataMapper = Mapper<T>().map(JSON: json)
                completion(dataMapper, nil)
                
            } catch {
                
                print("Catch error -> ", error)
                completion(nil, error)
                
            }
        }
        
        _callPostApiMultipleImage(api: api,
                                  params: params,
                                  arr_images: arr_images,
                                  Name: Name,
                                  mime_type: mime_type,
                                  File_name: File_name,
                                  completion: handleCompletion)
    }
}
