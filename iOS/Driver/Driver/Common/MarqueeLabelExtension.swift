//
//  MarqueeLabelExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/28/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import MarqueeLabel

extension MarqueeLabel {
    func setupMarqueeLabel() {
        type = .continuous
        speed = .duration(9)
        animationCurve = .linear
        fadeLength = 10.0
        trailingBuffer = 20.0
    }
}
