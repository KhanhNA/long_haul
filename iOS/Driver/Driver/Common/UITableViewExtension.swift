//
//  UITableViewExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/10/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

extension UITableView {
    func register(headerNibName: String) {
        register(UINib(nibName: headerNibName, bundle: nil), forHeaderFooterViewReuseIdentifier: headerNibName)
    }
    
    func register(cellNibName: String) {
        register(UINib(nibName: cellNibName, bundle: nil), forCellReuseIdentifier: cellNibName)
    }
    
    func dequeue<T: UITableViewCell>(_ type: T.Type, _ indexPath: IndexPath) -> T? {
        dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T
    }
}
