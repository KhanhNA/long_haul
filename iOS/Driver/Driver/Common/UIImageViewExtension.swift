//
//  UIImageViewExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/28/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import AlamofireImage

extension UIImageView {
    func setImage(urlString: String?, placeholder: UIImage?) {

        var urlString = urlString ?? ""
        urlString = urlString.replacingOccurrences(of: "http://", with: "")
        urlString = urlString.replacingOccurrences(of: "https://", with: "")

        image = placeholder
        
        guard let url = URL(string: "http://" + urlString) else { return }
        
        af_setImage(withURL: url) { [weak self] (data) in
            if case .success(let image2) = data.result {
                self?.image = image2
            }
        }
    }
}
