//
//  MapsHelper.swift
//  Driver
//
//  Created by Hieu Dinh on 8/5/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

import GoogleMaps

class MapsHelper {
    static func getCLLocation(latitude: Double, longitude: Double) -> CLLocation {
        return CLLocation(latitude: CLLocationDegrees(latitude),
                          longitude: CLLocationDegrees(longitude))
    }
    
    static func getCLLocationCoordinate2D(latitude: Double, longitude: Double) -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),
                                      longitude: CLLocationDegrees(longitude))
    }
    
    static func getGMSCameraPosition(latitude: Double,
                                     longitude: Double,
                                     zoom: Float = 12) -> GMSCameraPosition {
        return GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: zoom)
    }
    
    static func addMap(in view: UIView,
                       latitude: Double,
                       longitude: Double) -> GMSMapView {
        
        let mapView = GMSMapView(frame: view.frame)
        mapView.camera = getGMSCameraPosition(latitude: latitude, longitude: longitude)
        
        view.addSubview(mapView)
        
        return mapView
    }
    
    static func addMap(in view: UIView,
                       fromLatitude: Double,
                       fromLongitude: Double,
                       toLatitude: Double,
                       toLongitude: Double,
                       edgeInsets: UIEdgeInsets = .zero,
                       completion: ((_ polyline: GMSPolyline) -> ())? = nil) -> GMSMapView {
        
        let mapView = GMSMapView(frame: view.frame)
        mapView.setCamera(fromLatitude: fromLatitude,
                          fromLongitude: fromLongitude,
                          toLatitude: toLatitude,
                          toLongitude: toLongitude,
                          edgeInsets: edgeInsets)
        
        let source = getCLLocationCoordinate2D(latitude: fromLatitude, longitude: fromLongitude)
        let destination = getCLLocationCoordinate2D(latitude: toLatitude, longitude: toLongitude)
        mapView.drawPolygon(from: source, to: destination) { polyline in
            completion?(polyline)
        }
        
        view.addSubview(mapView)
        
        return mapView
    }
}

private struct MapPath : Decodable {
    var routes : [Route]?
}

private struct Route : Decodable {
    var overview_polyline : OverView?
}

private struct OverView : Decodable {
    var points : String?
}

extension GMSMapView {
    
    func update(fromLatitude: Double,
                fromLongitude: Double,
                toLatitude: Double,
                toLongitude: Double,
                edgeInsets: UIEdgeInsets = .zero,
                completion: ((_ polyline: GMSPolyline) -> ())? = nil) {
        
        setCamera(fromLatitude: fromLatitude,
                  fromLongitude: fromLongitude,
                  toLatitude: toLatitude,
                  toLongitude: toLongitude,
                  edgeInsets: edgeInsets)
        
        let source = MapsHelper.getCLLocationCoordinate2D(latitude: fromLatitude, longitude: fromLongitude)
        let destination = MapsHelper.getCLLocationCoordinate2D(latitude: toLatitude, longitude: toLongitude)
        
        drawPolygon(from: source, to: destination) { polyline in
            completion?(polyline)
        }
    }
    
    func setCamera(cllocationCoordinate2D: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: cllocationCoordinate2D.latitude,
                                              longitude: cllocationCoordinate2D.longitude,
                                              zoom: 10)
        
        animate(to: camera)
    }
    
    func setCamera(cllocation: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: cllocation.coordinate.latitude,
                                              longitude: cllocation.coordinate.longitude,
                                              zoom: 10)
        
        animate(to: camera)
    }
    
    func setCamera(fromLatitude: Double,
                   fromLongitude: Double,
                   toLatitude: Double,
                   toLongitude: Double,
                   edgeInsets: UIEdgeInsets = .zero) {
        
        let source = MapsHelper.getCLLocationCoordinate2D(latitude: fromLatitude, longitude: fromLongitude)
        let destination = MapsHelper.getCLLocationCoordinate2D(latitude: toLatitude, longitude: toLongitude)
        
        let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
        let camera = GMSCameraUpdate.fit(bounds, with: edgeInsets)
        
        animate(with: camera)
    }
    
    func addMarker(latitude: Double,
                   longitude: Double,
                   imageName: String,
                   title: String,
                   snippet: String) -> GMSMarker {
        
        let marker = GMSMarker()
        marker.position = MapsHelper.getCLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.icon = UIImage(named: imageName)
        marker.title = title
        marker.snippet = snippet
        marker.map = self
        
        return marker
    }
    
    //MARK:- Call API for polygon points
    
    func drawPolygon(from source: CLLocationCoordinate2D,
                     to destination: CLLocationCoordinate2D,
                     completion: ((_ polyline: GMSPolyline) -> ())? = nil) {
        
        let googleapis      = "https://maps.googleapis.com/maps/api/directions/json"
        let origin          = "?origin=\(source.latitude),\(source.longitude)"
        let destination     = "&destination=\(destination.latitude),\(destination.longitude)"
        let key             = "&key=\(GOOGLE_API_KEY)"
        
        let urlString = googleapis + origin + destination + key + "&sensor=false&mode=driving"
        
        guard let url = URL(string: urlString) else { return }
        
        DispatchQueue.main.async {
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            session.dataTask(with: url) { [weak self] (data, response, error) in
                
                guard data != nil else { return }
                
                do {
                    let route = try JSONDecoder().decode(MapPath.self, from: data!)
                    
                    if let points = route.routes?.first?.overview_polyline?.points {
                        self?.drawPath(with: points) { polyline in
                            completion?(polyline)
                        }
                    }
                    
                    print(route.routes?.first?.overview_polyline?.points ?? "")
                    
                } catch let error {
                    
                    print("Failed to draw ", error.localizedDescription)
                }
            }.resume()
        }
    }
    
    //MARK:- Draw polygon
    
    private func drawPath(with points: String,
                          strokeWidth: CGFloat = 3,
                          strokeColor: UIColor = AppColor.hex4696FE,
                          completion: ((_ polyline: GMSPolyline) -> ())? = nil) {
        
        DispatchQueue.main.async { [weak self] in
            
            let path = GMSPath(fromEncodedPath: points)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = strokeWidth
            polyline.strokeColor = strokeColor
            polyline.map = self
            
            completion?(polyline)
        }
    }
}
