//
//  Global.swift
//  OdooCustomer
//
//  Created by dong luong on 7/20/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

class Global: NSObject {
    static var isCheckPushViewUpdate:Bool?
    static var langId:Int = 1
    static var isCheckPushUpdateStatus:Bool?
   // static var listBillPackage: [BillPackage]?
//    static var listBillLadingDetail: [BillLadingDetail]?
//    static var billLadingDetail: BillLadingDetail = BillLadingDetail()
    
    static let serverDateFormat = "yyyy-MM-dd HH:mm:ss"
    static let localFormat = "HH'h'mm dd/MM/yyyy"
}

