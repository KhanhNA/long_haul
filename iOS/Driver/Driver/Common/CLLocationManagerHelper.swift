//
//  CLLocationManagerHelper.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import CoreLocation
import Toast
import SDCAlertView

private class ActionLog {
    var vanId: String?
    var latitude: String?
    var longitude: String?
    var createTime: String?
    
    init(vanId: String, item: CLLocationManagerModel) {
        self.vanId = vanId
        latitude = String(item.latitude)
        longitude = String(item.longitude)
        createTime = Date.localToUTC(date: item.createAtString)
    }
    
    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        json["van_id"] = vanId
        json["latitude"] = latitude
        json["longitude"] = longitude
        json["create_time"] = createTime
        
        return json
    }
}

class CLLocationManagerHelper: NSObject {
    static let shared = CLLocationManagerHelper()
    var locationManager: CLLocationManager?
    var timerUpdateLocation: Timer?
    var timerCallApi: Timer?
    var alertController: AlertController?
    
    func setup() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = true
        
        stopUpdatingLocation()
    }
    
    func requestAlwaysAuthorization() {
        locationManager?.requestAlwaysAuthorization()
    }
    
    func startTimerUpdateLocation() {
        requestAlwaysAuthorization()
        
        let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
        let save_log_duration = loginModel?.save_log_duration ?? ""
        
        timerUpdateLocation = Timer.scheduledTimer(timeInterval: TimeInterval(Int(save_log_duration) ?? 10),
                                                   target: self,
                                                   selector: #selector(startUpdatingLocation),
                                                   userInfo: nil,
                                                   repeats: true)
        timerUpdateLocation?.fire()
    }
    
    func startTimerRequestApi() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self else { return }
            
            let loginModel = UserDefaults.standard.retrieve(object: ResultResponceLogin.self, fromKey: "loginModel")
            let duration_request = loginModel?.duration_request ?? ""
            
            self.timerCallApi = Timer.scheduledTimer(timeInterval: TimeInterval(Int(duration_request) ?? 60),
                                                target: self,
                                                selector: #selector(self.callApi),
                                                userInfo: nil,
                                                repeats: true)
            self.timerCallApi?.fire()
        }
    }
    
    private func showToast(message: String) {
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.view.makeToast(message,
                                           duration: 1,
                                           position: CSToastPositionBottom,
                                           style: nil)
    }
    
    @objc private func callApi() {
        handleCallApi { [weak self] (data, error) in
            
            guard let self = self else { return }
            
            if let error = error {
                print(error.localizedDescription)
                self.showToast(message: "Có lỗi xảy ra".localized())
                return
            }
            
            guard let result = data?.boolResult, result == true else {
                
                self.showToast(message: "Có lỗi xảy ra".localized())
                
                return
            }
            
            self.showToast(message: "Định vị vị trí thành công!".localized())
            
            CLLocationManagerModel.addedAllToServer()
        }
    }
    
    typealias CreateActionLogCompletion = (BaseMappableResponseModel<BaseMappableModel>?, Error?) -> ()
    
    private func handleCallApi(completion: @escaping CreateActionLogCompletion) {
        let items = CLLocationManagerModel.getAllNewObjects()
        if items.isEmpty || items.first(where: { $0.isAddToServer == 0 }) == nil {
            return
        }
        
        let actionLogs = items.compactMap({ ActionLog(vanId: "21", item: $0).getJson() })
        if actionLogs.isEmpty {
            return
        }
        
        BaseClient.shared.requestAPIWithMappable(apiURL.createActionLog.url,
                                                 method: .post,
                                                 parameters: ["jsonrpc": "2.0", "params": ["action_logs": actionLogs]],
                                                 headers: nil,
                                                 completion: completion)
    }
    
    @objc private func startUpdatingLocation() {
        locationManager?.startUpdatingLocation()
    }
    
    private func stopUpdatingLocation() {
        locationManager?.stopUpdatingLocation()
    }
}

extension CLLocationManagerHelper: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            
            alertController?.dismiss()
            
        } else {
            
            let openSettings: () -> () = {
                guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            alertController = BaseAlertController.showAndDontAllowDismiss(message: "This app needs your current location".localized(),
                                                                          title: "Open settings".localized(),
                                                                          action: openSettings)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let item = CLLocationManagerModel()
            item.latitude = location.coordinate.latitude
            item.longitude = location.coordinate.longitude
            item.save()
            
            stopUpdatingLocation()
        }
    }
}
