//
//  StringExtension.swift
//  SlideMenuControllerSwift
//

import Foundation
import UIKit
import Localize_Swift

enum ValidateLength: Int {
    case minLength = 0
    case minLength1 = 1
    case minLength8 = 8
    case maxLength16 = 16
    case length19 = 19
    case length20 = 20
    case maxLength64 = 64
    case length4000 = 4000
}

extension String {
    var attributedString: NSAttributedString {
        return NSAttributedString(string: self)
    }
    
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func toDate(with format: String = Global.serverDateFormat) -> Date? {
        Formatter.shared.dateFormatter.dateFormat = format
        return Formatter.shared.dateFormatter.date(from: self)
    }
    
    func toLocalDate(with format: String = Global.serverDateFormat) -> Date? {
        return Date.UTCToLocal(date: self, fromFormat: format, toFormat: format)?
            .toDate(with: Global.serverDateFormat)
    }
    
    func stringForSearch() -> String {
        return lowercased()
            .folding(options: .diacriticInsensitive, locale: nil)
            .replacingOccurrences(of: "đ", with: "d")
    }
    
    func convertStringToDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: self){
            return date
        }
        return Date()
    }
    
    static func underLineText(text: String)-> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSMutableAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedText.length))
        return attributedText
    }
    
    static func className(_ aClass: AnyClass) -> String {
        guard let className = NSStringFromClass(aClass).components(separatedBy: ".").last else {
            fatalError()
        }        
        return className
    }
    func substring(startTo: Int) -> String {
        let startIdx = self.index(self.startIndex, offsetBy: startTo)
        let endIdx = self.endIndex
        return String(self[startIdx..<endIdx])
    }
    
    func subStringWith(start: Int, end: Int) -> String {
        let startIdx = self.index(startIndex, offsetBy: start)
        let endIdx = self.index(startIndex, offsetBy: end)
        return String(self[startIdx...endIdx])
    }
    public subscript(integerIndex: Int) -> Character {
        let index = self.index(startIndex, offsetBy: integerIndex)
        return self[index]
    }
    func split(_ seperator: String) -> [String] {
        return self.components(separatedBy: seperator)
    }
    func nsRange(from range: Range<Index>) -> NSRange {
        let from = range.lowerBound
        let to = range.upperBound
        let location = self.distance(from: startIndex, to: from)
        let length = self.distance(from: from, to: to)
        return NSRange(location: location, length: length)
    }
    func isContainNumber() -> Bool {
        let decimalCharacters = NSCharacterSet.decimalDigits
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters, options: .numeric, range: nil)
        if decimalRange == nil {
            return false
        } else {
            return true
        }
    }
    
    
    
    func checkTextSufficientComplexity() -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: self)
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: self)
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format: "SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: self)
        
        return capitalresult || numberresult || specialresult
    }
    func isContainCapital() -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: self)
        return capitalresult
    }
    func isDate() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if dateFormatter.date(from: self) != nil {
            return true
        } else {
            return false
        }
    }
    func equal(_ string: String) -> Bool {
        return self == string
    }
    
    
    func isValidPhone() -> Bool {
        let phoneTest = NSPredicate(format:"SELF MATCHES[c] %@", Regex.phoneRegEx)
        return phoneTest.evaluate(with: self)
    }
    
    func validLength(minLength: ValidateLength = .minLength, maxLength: ValidateLength = .maxLength64) -> Bool {
        return (self.count >= minLength.rawValue) && (self.count <= maxLength.rawValue)
    }
    
    func isSpaceValue() -> Bool {
        return self.compare(" ") == .orderedSame
    }
    
    func isContainMultipleSpace() -> Bool {
        if self.contains("  ") {
            return true
        }
        return false
    }
    
    func dateRepresentation(format: String, useCurrentTimeZone: Bool = true) -> Date? {
        guard !self.isEmpty, !format.isEmpty else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = locale
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = useCurrentTimeZone ? TimeZone.current : TimeZone(identifier: "UTC")
        
        return dateFormatter.date(from:self)
    }
    
    func toDateFromUTC() -> Date? {
        Formatter.shared.dateFormatter.dateFormat = AppFormatters.ISO8601
        return Formatter.shared.dateFormatter.date(from: self)
    }
    
    func toDateWithFormat(_ format: String) -> Date? {
        guard !format.isEmpty else { return nil }
        Formatter.shared.dateFormatter.dateFormat = format
        return Formatter.shared.dateFormatter.date(from: self)
    }
    
    func getCurrentDate(_ dateFormat: String) -> String {
        let date = Date()
        let locale = Locale(identifier: "en_US_POSIX")
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current// TimeZone(identifier: "UTC")
        return dateFormatter.string(from: date)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func trimLeadingTrailingWhitespaces() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    func trimWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 18)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}

extension Date {
    
    func convertToModified() -> String {
        var result = ""
        let component = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .weekday], from: self)
        let currentDate = Date()
        let difference = Calendar.current.dateComponents([.year, .month, .day, .weekOfYear, .hour, .minute], from: self, to: currentDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        //formatter.locale = Locale(identifier: "vi_VN")
        formatter.locale = Locale(identifier: Localize.currentLanguage())
        let weekDayString = formatter.string(from: self)
        guard let day = difference.day, let currentYear = component.year, let weekOfYear = difference.weekOfYear, let hour = difference.hour, let minute = difference.minute, let currentMonth = component.month, let currentDay = component.day, let diffYear = difference.year, let currentHour = component.hour, let currentMinute = component.minute else {
            return ""
        }
        if diffYear >= 1 {
            result = String(format: "%d Th%d, %d", currentDay, currentMonth, currentYear)
        }
        else {
            if weekOfYear >= 1 {
                result = String(format: "%d Th%d, %02i:%02i", currentDay, currentMonth, currentHour, currentMinute)
            }
            else {
                if day >= 1 {
                    result = String(format: "%@, %02i:%02i", weekDayString, currentHour, currentMinute)
                }
                else {
                    if hour >= 1 {
                        result = String(format: "%02i %@", hour,"lbl_hour_ago".localized())
                    }
                    else {
                        if minute >= 1 {
                            result = String(format: "%02i %@", minute,"lbl_minute_ago".localized())
                        }
                        else {
                            result = "lbl_time_current".localized()
                        }
                    }
                }
            }
        }
        
        return result
    }
    
}
