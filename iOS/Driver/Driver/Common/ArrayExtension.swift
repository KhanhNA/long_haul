//
//  ArrayExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/27/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

// usage
// let a: [Int] = [1, 2, 3, 3, 3]
// let b: [Int] = [1, 3, 3, 3, 2]
// let c: [Int] = [1, 2, 2, 3, 3, 3]
//
// print(a.containsSameElements(as: b)) // true
// print(a.containsSameElements(as: c)) // false
