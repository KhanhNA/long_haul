//
//  AppColor.swift
//  SmartMe
//
//  Created by AQU on 5/22/19.
//  Copyright © 2019 ntq-solution. All rights reserved.
//

import UIKit

struct AppColor {
    static let Primary: UIColor = UIColor(rgb: 0x000000)
    
    static let hex80C4C4C4  = UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 0.5)
    static let hex4D00B563  = UIColor(red: 0, green: 0.71, blue: 0.39, alpha: 0.3)
    
    static let hex00A359    = UIColor(hex: "00A359")
    static let hex3A3E41    = UIColor(hex: "3A3E41")
    static let hex24282C    = UIColor(hex: "24282C")
    static let hex919395    = UIColor(hex: "919395")
    static let hex4696FE    = UIColor(hex: "4696FE")
    static let hex289767    = UIColor(hex: "289767")
    static let hexFF0C20    = UIColor(hex: "FF0C20")
    static let hexE5E5E5    = UIColor(hex: "E5E5E5")
    static let hex151522    = UIColor(hex: "151522")
    static let hexCCF0E0    = UIColor(hex: "CCF0E0")
    static let hexA4A8AC    = UIColor(hex: "A4A8AC")
    static let hexE7F4FF    = UIColor(hex: "E7F4FF")
}

extension UIColor{
    func getCustomBlueColor() -> UIColor{
        return UIColor(red:255, green:233 ,blue:0 , alpha:1.00)
    }
}

class Helper: NSObject {
    static let COLOR_PRIMARY_DEFAULT_APP = #colorLiteral(red: 0.2980392157, green: 0.5803921569, blue: 0.4196078431, alpha: 1)
    static let COLOR_PRIMARY_TITLE = #colorLiteral(red: 0.1764705882, green: 0.1764705882, blue: 0.1764705882, alpha: 1)
    static let COLOR_PRIMARY_BACKGROUD_BUTTON = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    static let COLOR_BACKGROUD_VIEW_WhITE = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let COLOR_PRIMARY_BACKGROUD_TEXTFIELD = #colorLiteral(red: 0.9843137255, green: 0.9843137255, blue: 0.9843137255, alpha: 1)
    static let COLOR_TEXT_BLUE = #colorLiteral(red: 0.2901960784, green: 0.431372549, blue: 0.662745098, alpha: 1)
    static let COLOR_DEVIDER_GRAY = #colorLiteral(red: 0.7137254902, green: 0.7137254902, blue: 0.7137254902, alpha: 1)
    static let COLOR_DARK_PR_MARY = #colorLiteral(red: 0.968627451, green: 0, blue: 0, alpha: 1)
    static let IMAGE_HODER = UIImage(named: "avatar_default-1.png")
    static let COLOR_BG_MESSAGE_HOST = #colorLiteral(red: 0.2509803922, green: 0.5019607843, blue: 1, alpha: 1)
    static let COLOR_BG_MESSAGE_GUESS = #colorLiteral(red: 0.9450980392, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    static let COLOR_SEGMENTTINT = #colorLiteral(red: 0.5764705882, green: 0.5803921569, blue: 0.5882352941, alpha: 1)
    static let COLOR_EFEFEF = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
    static let COLOR_0ALPHA80 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8)
    static let COLOR_BUTTON = #colorLiteral(red: 0.1568627451, green: 0.5921568627, blue: 0.4039215686, alpha: 1)
}

class HeplperFont: NSObject {
    static let FONT_BUTTON = "SFUIText-Bold"
    static let SIZE_BUTTON  = 16
    static let FONT_TEXTFIELD = "SFUIText-Regular"
    static let SIZE_TEXTFIELD = 16
    static let SIZE_TEXT_BLUE = "SFUIText-Semibold"
    static let MEDIUM_12 = "SFUIText-Medium"
    static let SIZE_LABEL_BIG = 16
    static let SIZE_BUTTON14  = 14
}

class HelperRadius: NSObject {
    static let SiZE_BUTTON_4  = 4
    static let SiZE_BUTTON_8  = 8
    static let SiZE_BUTTON_12 = 12
}

class SFUIText: NSObject {
    static let Semibold_16 = UIFont(name: "SFUIText-Semibold", size: 16)
    static let Semibold_28 = UIFont(name: "SFUIText-Semibold", size: 28)
    static let Semibold_14 = UIFont(name: "SFUIText-Semibold", size: 14)
    static let Regular14 = UIFont(name: "SFUIText-Regular", size: 14)
    static let bold_16 = UIFont(name: "SFUIText-Bold", size: 16)
    static let bold_14 = UIFont(name: "SFUIText-Bold", size: 14)
    static let bold_18 = UIFont(name: "SFUIText-Bold", size: 18)
    static let bold_22 = UIFont(name: "SFUIText-Bold", size: 22)
    static let bold_26 = UIFont(name: "SFUIText-Bold", size: 26)
    static let Regula12 = UIFont(name: "SFUIText-Regular", size: 12)
    static let Regular10 = UIFont(name: "SFUIText-Regular", size: 10)
    static let Regular8 = UIFont(name: "SFUIText-Regular", size: 8)
    static let Medium12 = UIFont(name: "SFUIText-Medium", size: 12)
    static let bold_12 = UIFont(name: "SFUIText-Bold", size: 12)
    static let bold_8 = UIFont(name: "SFUIText-Bold", size: 8)
    static let bold_10 = UIFont(name: "SFUIText-Bold", size: 10)
}

class HexString: NSObject {
    static let COLOR_0569 = UIColor(red: 0.569, green: 0.576, blue: 0.584, alpha: 1)
    static let COLOR_0143 = UIColor(red: 0.143, green: 0.157, blue: 0.171, alpha: 1)
    static let COLOR_PLACEHODER_TEXT_FIELD = UIColor(red: 0.569, green: 0.576, blue: 0.584, alpha: 1)
    static let BACK_GROUND_CELL = UIColor(red: 0.984, green: 0.984, blue: 0.984, alpha: 1)
    static let COLOR_0357 = UIColor(red: 0.357, green: 0.369, blue: 0.38, alpha: 1)
    static let COLOR_0227 = UIColor(red: 0.227, green: 0.243, blue: 0.255, alpha: 1)
    static let COLOR_PHONE = UIColor(red: 0.227, green: 0.549, blue: 1, alpha: 1)
    static let COLOR_08 = UIColor(red: 0.8, green: 0.941, blue: 0.878, alpha: 1)
    static let COLOR_08_03 = UIColor(red: 0.8, green: 0.941, blue: 0.878, alpha: 0.3)
    static let COLOR_0157 = UIColor(red: 0.157, green: 0.592, blue: 0.404, alpha: 1)
    static let COLOR_0047 = UIColor(red: 1, green: 0.047, blue: 0.125, alpha: 1)
    static let COLOR_0137 = UIColor(red: 0.137, green: 0.122, blue: 0.125, alpha: 1)
    static let COLOR_RED  = UIColor(red: 1, green: 0.047, blue: 0.125, alpha: 1)
    
    //random background
    static let COLOR_RANDOM1  = UIColor(red: 0.8, green: 0.941, blue: 0.878, alpha: 0.4)
    static let COLOR_RANDOM2  = UIColor(red: 0.847, green: 0.91, blue: 1, alpha: 0.4)
    static let COLOR_RANDOM3  = UIColor(red: 1, green: 0.808, blue: 0.824, alpha: 0.4)
    static let COLOR_RANDOM4  = UIColor(red: 0.984, green: 0.875, blue: 0.686, alpha: 0.4)
    static let COLOR_RANDOM5  = UIColor(red: 0.984, green: 0.984, blue: 0.984, alpha: 1)
    static let COLOR_RANDOM6  = UIColor(red: 0.984, green: 0.875, blue: 0.686, alpha: 1)
    
}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
