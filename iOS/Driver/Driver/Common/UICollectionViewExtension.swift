//
//  UICollectionViewExtension.swift
//  Driver
//
//  Created by Hieu Dinh on 8/11/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit

extension UICollectionView {
    func register(cellNibName: String) {
        register(UINib(nibName: cellNibName, bundle: nil), forCellWithReuseIdentifier: cellNibName)
    }
    
    func dequeue<T: UICollectionViewCell>(_ type: T.Type, _ indexPath: IndexPath) -> T? {
        dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as? T
    }
}
