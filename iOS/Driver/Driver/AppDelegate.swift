//
//  AppDelegate.swift
//  Driver
//
//  Created by dong luong on 8/3/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import IQKeyboardManager
import GooglePlaces
import GoogleMaps
import KRProgressHUD
import UserNotifications
import Firebase

let GOOGLE_API_KEY = "AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk"
let GOOGLE_SEARCH_API_KEY = "AIzaSyDLtkodzb8ivLLg50q19eObdg4zs9MT04I";

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var fcmToken: String?
    
    weak var notificationVC: NotificationVC?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        
        let launchScreenVC = UIStoryboard.init(name: "LaunchScreen",bundle:nil)
        let rootVC = launchScreenVC.instantiateViewController(withIdentifier: "splashController")
        self.window?.rootViewController = UINavigationController(rootViewController: rootVC)
        self.window?.makeKeyAndVisible()
        Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
        window?.makeKeyAndVisible()
        IQKeyboardManager.shared().isEnabled = true
        
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        registerForNotifications()
        
        return true
    }
    
    @objc func dismissSplashController(){
        let mainVC = UIStoryboard.init(name:"Main",bundle:nil)
        let rootVC  = mainVC.instantiateViewController(withIdentifier: "initController")
        self.window?.rootViewController = UINavigationController(rootViewController: rootVC)
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if application.applicationIconBadgeNumber != 0 {
            application.applicationIconBadgeNumber = 0
            
            notificationVC?.viewModel.reloadData(isShowLoading: true)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// xử lý notification
extension AppDelegate {
    func registerForNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                
                print("Permission granted:", granted)
                guard granted else { return }
                
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token:", token)
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        self.fcmToken = fcmToken
        print(fcmToken)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // json khi push notification
        print("==================================================")
        let userInfo = notification.request.content.userInfo as? [String: Any]
        if let jsonData = try? JSONSerialization.data(withJSONObject: userInfo ?? [:],
                                                      options: JSONSerialization.WritingOptions.prettyPrinted) {
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
            print("Response ->", jsonString ?? "")
        } else {
            print("Response ->", userInfo ?? notification.request.content.userInfo)
        }
        
        
        let messObject = userInfo?["mess_object"] as? String
        notificationVC?.viewModel.addMessObject(messObject)
        
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //        // json khi push notification được click
        //        print("==================================================")
        //        let userInfo = response.notification.request.content.userInfo as? [String: Any]
        //        if let jsonData = try? JSONSerialization.data(withJSONObject: userInfo ?? [:],
        //                                                      options: JSONSerialization.WritingOptions.prettyPrinted) {
        //
        //            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
        //            print("Response ->", jsonString ?? "")
        //        } else {
        //            print("Response ->", userInfo ?? response.notification.request.content.userInfo )
        //        }
        //
        //        notificationVC?.viewModel.reloadData(isShowLoading: true)
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                openSettingsFor notification: UNNotification?) { }
}
