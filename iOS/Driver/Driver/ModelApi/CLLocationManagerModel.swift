//
//  CLLocationManagerModel.swift
//  Driver
//
//  Created by Hieu Dinh on 8/12/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import RealmSwift

class CLLocationManagerModel: Object {
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var isAddToServer = 0
    @objc dynamic var createAt = Date() {
        didSet {
            createAtString = createAt.toString()
        }
    }
    @objc dynamic var createAtString = Date().toString()
    
    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
        case isAddToServer = "isAddToServer"
        case createAt = "createAt"
        case createAtString = "createAtString"
    }
    
    func getJson() -> [String: Any] {
        var json = [String: Any]()
        
        json[CodingKeys.latitude.rawValue] = latitude
        json[CodingKeys.longitude.rawValue] = longitude
        json[CodingKeys.isAddToServer.rawValue] = isAddToServer
        json[CodingKeys.createAt.rawValue] = createAt
        json[CodingKeys.createAtString.rawValue] = createAtString
        
        return json
    }
    
    static func getAllNewObjects() -> [CLLocationManagerModel] {
        return CLLocationManagerModel.getAll(filter: "isAddToServer = 0")
    }
    
    static func addedAllToServer() {
        do {
            let realm = try Realm()
            let objects = realm.objects(CLLocationManagerModel.self).filter("isAddToServer = 0")
            
            try realm.write {
                for object in objects {
                    object.isAddToServer = 1
                }
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
}
