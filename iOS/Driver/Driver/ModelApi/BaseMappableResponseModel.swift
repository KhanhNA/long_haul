//
//  BaseMappableResponseModel.swift
//  LongHolder
//
//  Created by Hieu Dinh on 8/5/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import UIKit
import ObjectMapper

// Response
class BaseMappableResponseModel<M: Mappable>: NSObject, Mappable {
    var boolResult: Bool?
    var stringResult: String?
    var result: M?
    var jsonrpc: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        stringResult <- map["result"]
        boolResult <- map["result"]
        result <- map["result"]
        jsonrpc <- map["jsonrpc"]
    }
}

// Result
class BaseMappableResultModel<M: Mappable>: NSObject, Mappable {
    var length: Int?
    var total: Int?
    var arrayRecords: [[M]]?
    var records: [M]?
    var record: M?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        length <- map["length"]
        total <- map["total"]
        arrayRecords <- map["records"]
        records <- map["records"]
        record <- map["records"]
    }
}

// Simple Result (String, Int...)
class BaseMappableSimpleResultModel<M: Any>: NSObject, Mappable {
    var length: Int?
    var total: Int?
    var records: [M]?
    var record: M?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        length <- map["length"]
        total <- map["total"]
        records <- map["records"]
        record <- map["records"]
    }
}

// BaseMappableResponseModel dùng boolResult
class BaseMappableModel: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
}
