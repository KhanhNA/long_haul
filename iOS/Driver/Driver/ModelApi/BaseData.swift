//
//  BaseData.swift
//  OdooCustomer
//
//  Created by dong luong on 7/13/20.
//  Copyright © 2020 dong luong. All rights reserved.
//

import Foundation
import DynamicCodable

struct ResponseModel<M: Codable>: Codable {
    var result : M?
    var jsonrpc : String?
}

class BaseResultResponse<M: Codable>: Codable {
    var length: Int?
    var records: [M]?
    var result : [M]?
}

struct BaseData: Codable {
    var data: NotificationModel?
    var message: String?
    var statusCode: Int?
    
    enum CodingKeys: String, CodingKey {
        case data, message, statusCode
    }
}

class ChangingPassword: Codable {
    var oldPassword: String?
    var newPassword: String?
    var newConfirmPassword: String?
}



struct NotificationDetail: Codable {
    var data: NotificationItem?
    var message: String?
    var statusCode: Int?
}

struct NotificationModel: Codable {
    var data: [NotificationItem]? = []
    var current_page: Int?
    var last_page: Int?
}

struct NotificationItem: Codable {
    var id: Int?
    var code: String?
    var title: String?
    var scope: String?
    var content: String?
    var product_id: String?
    var status: String?
    var created_at: String?
    var notification_type: String?
    var user_id: String?
    //    var user_read: Int?
}


class UserContext: Codable {
    var tz: String?
    var lang: String?
    var uid: Int?
}

class ServerVersionInfo: Codable {
    
}

class CacheHashes: Codable {
    var load_menus: String?
    var qweb: String?
    var translations: String?
}

class ResultResponceLogin: Codable {
    var uid: Int?
    var is_system: Bool?
    var is_admin: Bool?
    var name: String?
    var username: String?
    var phone: String?
    var city: String?
    var street: String?
    var zip: String?
    var company_id: Int?
    var duration_request: String?
    var save_log_duration: String?
    var distance_check_point: String?
}

class Warehouse: Codable {
    var id: Int?
    var name: String?
    var warehouse_code: String?
    var address: String?
    var street: String?
    var street2: String?
    var city_name: String?
    var latitude: Double?
    var longitude: Double?
    var phone: String?
    var display_name: String?
    var placeId: String?
    var areaInfo: Area?
    var fromWareHouseCode: String?
    var wjson_address: String?
    var name_seq: String?
}

//class  Route: Codable {
//    var distance: Distance?
//    var duration: Duration?
//    var endAddress: String?
//    var endLocation: LatLng?
//    var startAddress: String?
//    var startLocation: LatLng?
//    var points: [LatLng]?
//}

class Distance: NSObject,Codable {
    var text: String?
    var value: Double?
    
    convenience init(text: String, value: Double) {
        self.init()
        self.text = text
        self.value = value
    }
    
    override init() {
    }
}

class Duration: NSObject,Codable {
    var text: String?
    var value: Double?
    
    convenience init(text: String, value: Double) {
           self.init()
           self.text = text
           self.value = value
       }
       
       override init() {
       }
}

class Area: Codable {
    var code: String?
    var id: Int?
    var hubInfo: Hub?
    var zoneInfo: Zone?
    var name: String?
    
}

class Hub: Codable {
    var id: Int?
    var latitude: Double?
    var longitude: Double?
    var name: String?
    var address: String?
    var status: String?
    var name_seq: String?
}

class Zone: Codable {
    var id: Int?
    var name: String?
    var code: String?
    var name_seq: String?
    var depotInfo: Hub?
}

class SearchTextMapResponse: Codable {
    var status: String?
    var results: [CustomA]?
}

class CustomA: Codable {
    var geometry: Geometry?
    var formatted_address: String?
    var name: String?
    var place_id: String?
}

class  Geometry: Codable {
    var location: LocationA?
}

class LocationA: Codable {
    var lat: Double?
    var lng: Double?
}


class ServiceModel: Codable {
    var id: Int?
    var name: String?
    var display_name: String?
    var price: Double?
    var description: String?
    var status: String?
}


//class ProductType: Codable {
//    var id: Int?
//    var name_seq: String?
//    var name: String?
//    var net_weight: Double?
//    //var description: String?
//    var status: String?
//    var isSelect: Bool?
//}

//class BillPackage: Codable {
//    var item_name: String?
//    var net_weight: Double?
//    var quantity_package: Int?
//    var length: Double?
//    var width: Double?
//    var height: Double?
//    var capacity: Double?
//    var description: String?
//    var productType: ProductType?
//    var isCheck: Bool?
//}

//class BillLadingDetail: NSObject,Codable {
//    var areaDistances: [AreaDistance]?
//    var billPackages: [BillPackage]?
//    var billService: [BillService]?
//    var warehouse : Warehouse?
//    var expected_from_time: String?
//    var expected_to_time: String?
//    var address: String?
//    var name_seq: String?
//    var status_order: String?
//    var total_weight: Int?
//    var warehouse_name: String?
//    var warehouse_type: String?
//    var wjson_address: String?
//}


class Insurance: Codable {
    var id: Int?
    var code: String?
    var name: String?
    var status: String?
    var amount: Double?
    var display_name: String?
    var description: String
}

class Subscribe: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var subscribe_code: String
    var value: Int?
}

//class RoutingDay: Codable {
//    var id: Int?
//    var routing_plan_day_code: String?
//    var actual_time: String?
//    var expected_from_time: String?
//    var expected_to_time: String?
//    var name: String?
//    var status: String?
//    var date_plan: String?
//    var routing_plan_day_id: Int?
//    var vehicle: Vehicle?
//    var latitude: Double?
//    var longitude: Double?
//    var address: String?
//    var warehouse_name: String?
//    var routing_plan_day_detail: [RoutingDetail]?
//    var image_urls: [String]?
//    
//}

//class RoutingDetail: Codable {
//    var id: Int?
//    var routing_plan_detail_code: String?
//    var type: String?
//    var order_type: String?
//    var insurance_name: String?
//    var service_name: [BillService]?
//    var status: String?
//    var date_plan: String?
//    var expected_from_time: String?
//    var vehicle: Vehicle?
//    var latitude: Double?
//    var longitude: Double?
//    var address: String?
//    var phone: String?
//    var warehouse_name: String?
//    var name: String?
//    var quantity_export: Int?
//    var length: Int?
//    var height: Int?
//    var capacity: Int?
//    var total_weight: Int?
//    var note: String?
//    var item_name: String?
//    var qr_char: String?
//    var isSelect: Bool?
//    var list_bill_package_import: [BillPackage]?
//    var list_bill_package_export: [BillPackage]?
//}

class Vehicle: Codable {
    var id: Int?
    var name: String?
    var license_plate: String?
    var color: String?
    var model_year: String?
    var fuel_type: String?
    var body_length: Int?
    var body_width: Int?
    var height: Int?
    var vehicle_type: String?
    var engine_size: Double?
    var vehicle_status: Int?
}

class BillService: NSObject,Codable {
    var id: Int?
    var name: String?
    var display_name: String?
    var service_code: String?
    var price: Double?
    var descriptionNew: String?
    var status: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case display_name = "display_name"
        case service_code = "service_code"
        case price = "price"
        case descriptionNew = "description"
        case status = "status"
    }
    
    convenience init(id: Int, name: String, display_name: String, service_code: String, price: Double,
                     descriptionNew: String, status: String) {
        self.init()
        self.id = id
        self.name = name
        self.display_name = display_name
        self.service_code = service_code
        self.price = price
        self.descriptionNew = descriptionNew
        self.status = status
    }
    
    
    override init() {
    }
}

class AreaDistance: Codable {
    var fromLocation: LatLng?
    var toLocation: LatLng?
    var from_name_seq: String?
    var to_name_seq: String?
    var type: Int?
    var distance: Double?
    var duration: Double?
    var price: Double?
}

class LatLng: NSObject,Codable {
    var latitude: Double?
    var longitude: Double?
    
    convenience init(latitude: Double, longitude: Double) {
        self.init()
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init() {
    }
}


class ModelCreatOrder: Codable {
    var jsonrpc: String?
    var id: Int?
}


//class BillLading: NSObject, Codable {
//    var id: Int?
//    var total_weight: Double?
//    var total_parcel: Double?
//    var amount: Double?
//    var promotion_code: Double?
//    var total_volume: Double?
//    var tolls: Double?
//    var surcharge: Double?
//    var vat: Double?
//    var name: String?
//    var status: String?
//    var start_date: String?
//    var end_date: String?
//    var company_id: [Any]?
//    var insurance_id: [Any]?
//    var recurrent_id: [Any]?
//    var arrBillLadingDetail: [BillLadingDetail]?
//    var insurance: Insurance?
//    var name_seq: String?
//    var dayOfMonth: Int?
//    var dayOfWeek: Int?
//    var frequency: Int?
//    var subscribe: Subscribe?
//    var startDateStr: String?
//    var total_amount: Int?
//
//    override init() {
//
//    }
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case name
//        case company_id
//        case insurance_id
//        case start_date
//        case end_date
//        case status
//        case recurrent_id
//        case arrBillLadingDetail
//        case total_weight
//        case total_parcel
//        case total_volume
//        case vat
//        case insurance
//        case name_seq
//        case dayOfMonth
//        case dayOfWeek
//        case frequency
//        case subscribe
//        case startDateStr
//        case total_amount
//    }
//
//    required init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decode(Int.self, forKey: .id)
//        name = try values.decode(String.self, forKey: .name)
//        company_id = try values.decodeIfPresent([Any].self, forKey: .company_id)
//        insurance_id = try values.decodeIfPresent([Any].self, forKey: .insurance_id)
//        start_date = try values.decode(String.self, forKey: .start_date)
//        end_date = try values.decode(String.self, forKey: .end_date)
//        status = try values.decode(String.self, forKey: .status)
//        recurrent_id = try values.decodeIfPresent([Any].self, forKey: .recurrent_id)
//        arrBillLadingDetail = try values.decode([BillLadingDetail].self, forKey: .arrBillLadingDetail)
//        total_weight = try values.decode(Double.self, forKey: .total_weight)
//        total_parcel = try values.decode(Double.self, forKey: .total_parcel)
//        total_volume = try values.decode(Double.self, forKey: .total_volume)
//        vat = try values.decode(Double.self, forKey: .vat)
//        insurance = try values.decode(Insurance.self, forKey: .insurance)
//        name_seq = try values.decode(String.self, forKey: .name_seq)
//        dayOfMonth = try values.decode(Int.self, forKey: .dayOfMonth)
//        dayOfWeek = try values.decode(Int.self, forKey: .dayOfWeek)
//        frequency = try values.decode(Int.self, forKey: .frequency)
//        subscribe = try values.decode(Subscribe.self, forKey: .subscribe)
//        startDateStr = try values.decode(String.self, forKey: .startDateStr)
//        total_amount = try values.decode(Int.self, forKey: .total_amount)
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(id, forKey: .id)
//        try container.encode(name, forKey: .name)
//        try container.encodeIfPresent(company_id, forKey: .company_id)
//        try container.encodeIfPresent(insurance_id, forKey: .insurance_id)
//        try container.encode(start_date, forKey: .start_date)
//        try container.encode(end_date, forKey: .end_date)
//        try container.encode(status, forKey: .status)
//        try container.encodeIfPresent(recurrent_id, forKey: .recurrent_id)
//        try container.encode(arrBillLadingDetail, forKey: .arrBillLadingDetail)
//        try container.encode(total_weight, forKey: .total_weight)
//        try container.encode(total_parcel, forKey: .total_parcel)
//        try container.encode(total_volume, forKey: .total_volume)
//        try container.encode(vat, forKey: .vat)
//        try container.encode(insurance, forKey: .insurance)
//        try container.encode(name_seq, forKey: .name_seq)
//        try container.encode(dayOfMonth, forKey: .dayOfMonth)
//        try container.encode(dayOfWeek, forKey: .dayOfWeek)
//        try container.encode(frequency, forKey: .frequency)
//        try container.encode(subscribe, forKey: .subscribe)
//        try container.encode(startDateStr, forKey: .startDateStr)
//        try container.encode(total_amount, forKey: .total_amount)
//    }
//}

class PlaceBaseResponse: Codable {
    var result: AddresscComponents?
}

class AddresscComponents: Codable {
    var address_components: [ModelAddresscComponent]?
}

class ModelAddresscComponent: Codable {
    var long_name: String?
    var short_name: String?
    var types: [String]?
}











