
import UIKit
import KRProgressHUD
import Alamofire
import CoreData
import Toast
import RxSwift
import RxCocoa
import DropDown
import Localize_Swift
import Toast

class RootViewController: BaseViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var txtUserName: CustomDisablePasteUITextField!
    @IBOutlet weak var txtPassWord: UITextField!
    var merchantTypeId:Int?
    @IBOutlet weak var btnForgetPassWord: UIButton!
    @IBOutlet weak var btnEnterCodeIntro: UIButton!
    var isSave:Bool = false
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblNotificationLogin: UILabel!
    @IBOutlet weak var btnSavePassword: UIButton!
    let chooseDropDown = DropDown()
    @IBOutlet weak var ChooseLanggue: UIButton!
    @IBOutlet weak var lblLangue: UILabel!
    @IBOutlet weak var imgLanguage: UIImageView!
    
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    @IBOutlet weak var lblUsePhoneNumber: UILabel!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewPassWord: UIView!
    @IBOutlet weak var heightlblLoginNotification: NSLayoutConstraint!
    @IBOutlet weak var btnStatusPass: UIButton!
    let availableLanguages = Localize.availableLanguages()
    var iconShowPass = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        //        setLoginView()
        
        lblNotificationLogin.isHidden = true
        lblNotificationLogin.textColor = UIColor(red: 1, green: 0.239, blue: 0.302, alpha: 1)
        heightlblLoginNotification.constant = 0
        // Do any additional setup after loading the view.
        
        lblLangue.textColor = UIColor(red: 0.227, green: 0.243, blue: 0.255, alpha: 1)
        lblLangue.font = SFUIText.Regular14
        if(self.iconShowPass) {
            txtPassWord.isSecureTextEntry = false
            btnStatusPass.setImage(UIImage(named: "eye"), for: .normal)
        } else {
            txtPassWord.isSecureTextEntry = true
            btnStatusPass.setImage(UIImage(named: "invisible"), for: .normal)
        }
        iconShowPass = !iconShowPass
        
        setText()
        txtUserName.addTarget(self, action: #selector(textFieldUsernameDidChange(_:)), for: UIControl.Event.editingChanged)
        txtPassWord.addTarget(self, action: #selector(textFieldPasswordDidChange(_:)), for: UIControl.Event.editingChanged)
        
    }
    
    @objc func textFieldUsernameDidChange(_ textField: UITextField) {
        checkEnableButton()
    }
    
    @objc func textFieldPasswordDidChange(_ textField: UITextField) {
        checkEnableButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (navigationController?.topViewController != self) {
            navigationController?.navigationBar.isHidden = false
        }
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    @IBAction func onClickStatusPass(_ sender: Any) {
        if(self.iconShowPass) {
            txtPassWord.isSecureTextEntry = false
            btnStatusPass.setImage(UIImage(named: "eye"), for: .normal)
        } else {
            txtPassWord.isSecureTextEntry = true
            btnStatusPass.setImage(UIImage(named: "invisible"), for: .normal)
        }
        iconShowPass = !iconShowPass
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        super.viewWillAppear(animated)
        heightlblLoginNotification.constant = 0
        lblNotificationLogin.isHidden = true
      
        // TODO: Test
        txtUserName.text = "ABCD123456"
        txtPassWord.text = "admin"
        
        checkEnableButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    func checkEnableButton()  {
        if txtUserName.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? false || txtPassWord.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? false {
            btnLogin.isEnabled = false
            btnLogin.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        } else {
            btnLogin.isEnabled = true
            btnLogin.backgroundColor = Helper.COLOR_BUTTON
        }
    }
    
    @IBAction func onShowLangue(_ sender: Any) {
        setupChooseDropDown()
        chooseDropDown.show()
    }
    
    func setupChooseDropDown() {
        chooseDropDown.anchorView = ChooseLanggue
        chooseDropDown.bottomOffset = CGPoint(x: 0, y: ChooseLanggue.bounds.height)
        chooseDropDown.dataSource = [
            "txt_Viet_Namese".localized(),
            "txt_English".localized(),
            "txt_Myanma".localized()
        ]
        // Action triggered on selection
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            print(index)
            self?.lblLangue.text = item
//            Util.setObject(Int(index), forKey: "IndexLangue")
            if index == 0 {
                Localize.setCurrentLanguage("vi")
            } else if index == 1 {
                Localize.setCurrentLanguage("en")
            } else {
                Localize.setCurrentLanguage("my")
            }
        }
    }
    
    @objc
    func setText() {
        if Localize.currentLanguage().elementsEqual("vi") {
            lblLangue.text = "txt_Viet_Namese".localized()
            imgLanguage.image = UIImage(named: "ic_vietnam")
        } else if Localize.currentLanguage().elementsEqual("my") {
            lblLangue.text = "txt_Myanma".localized()
            imgLanguage.image = UIImage(named: "ic_myanmar")
        } else {
            lblLangue.text = "txt_English".localized()
            imgLanguage.image = UIImage(named: "ic_english")
        }
        lblNotificationLogin.text = "lbl_error_infor_accout".localized()
        txtUserName.placeholder = "txt_UserName".localized()
        txtPassWord.placeholder = "txt_PassWord".localized()
        btnLogin.setTitle("btn_Login".localized(), for: .normal)
        btnForgetPassWord.setTitle("btn_Forget_PassWord".localized(), for: .normal)
        btnEnterCodeIntro.setTitle("btn_Enter_Code_Intro".localized(), for: .normal)
        btnSavePassword.setTitle("btn_Save_Password".localized(), for: .normal)
        Utils.settButton(btnSavePassword, 14, UIColor.clear, HeplperFont.SIZE_TEXT_BLUE, UIColor(red: 0.227, green: 0.549, blue: 1, alpha: 1), 1)
        Utils.settButton(btnForgetPassWord, 14, UIColor.clear, HeplperFont.SIZE_TEXT_BLUE, UIColor(red: 0.227, green: 0.549, blue: 1, alpha: 1), 1)
        Utils.settButton(btnEnterCodeIntro, 14, UIColor.clear, HeplperFont.SIZE_TEXT_BLUE, UIColor(red: 0.227, green: 0.549, blue: 1, alpha: 1), 1)
        Utils.setBorderColor(viewUserName, 0.3, 10)
        Utils.setBorderColor(viewPassWord, 0.3, 10)
        Utils.settButton(btnLogin, CGFloat(HeplperFont.SIZE_BUTTON), Helper.COLOR_BUTTON, HeplperFont.FONT_BUTTON, Helper.COLOR_BACKGROUD_VIEW_WhITE,CGFloat(HelperRadius.SiZE_BUTTON_12))
        lblUsePhoneNumber.textColor = HexString.COLOR_0569
        txtPassWord.textColor = HexString.COLOR_0143
        txtUserName.textColor = HexString.COLOR_0143
        txtPassWord.attributedPlaceholder = NSAttributedString(string: "txt_PassWord".localized(), attributes: [NSAttributedString.Key.foregroundColor: HexString.COLOR_PLACEHODER_TEXT_FIELD])
        txtUserName.attributedPlaceholder = NSAttributedString(string: "txt_UserName".localized(), attributes: [NSAttributedString.Key.foregroundColor: HexString.COLOR_PLACEHODER_TEXT_FIELD])
    }
    
    
    
    @IBAction func onEntercode(_ sender: Any) {
        
    }
    
    @IBAction func onSavePassword(_ sender: Any) {
        if isSave == false {
            imgCheck.image = UIImage(named: "check-box-blue")
            isSave = true
        } else {
            imgCheck.image = UIImage(named: "check-box-empty")
            isSave = false
        }
        
    }
    
    @IBAction func onLogin(_ sender: UIButton) {
        KRProgressHUD.show()
        if txtUserName.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true {
            self.view.makeToast("txt_Please_Login".localized(), duration: 2.0, position: CSToastPositionCenter, style: nil)
            KRProgressHUD.dismiss()
            return
        } else if txtPassWord.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? true {
            self.view.makeToast("txt_Please_Password".localized(), duration: 2.0, position: CSToastPositionCenter, style: nil)
            KRProgressHUD.dismiss()
            return
        }
        
        let params: [String: Any] =
            ["jsonrpc" : 2.0,
             "params": [
                "db": "bidding_happy_v3",
                "login": txtUserName.text?.trimmingCharacters(in: .whitespaces),
                "password": txtPassWord.text?.trimmingCharacters(in: .whitespaces) ]
        ]
        
     if let data = try? JSONSerialization.data(withJSONObject: params),
            let string = String(data: data, encoding: .utf8){
            print(string)
        }
        
        
        BaseApiManager.loginOdoo(params, showIndicator: true) { (datas, error) in
            
            if error != nil {
                print(error ?? "")
                self.view.makeToast(error?.localizedDescription, duration: 1, position: CSToastPositionCenter, style: nil)
            } else {
                print(datas ?? ResultResponceLogin())
                if datas?.result == nil {
                    self.view.makeToast("lbl_error_infor_accout".localized(), duration: 1, position: CSToastPositionCenter, style: nil)
                } else {
                    
                    let resultResponceLogin = datas?.result ?? ResultResponceLogin()
                    UserDefaults.standard.save(customObject: resultResponceLogin, inKey: "loginModel")
                    self.goToTabBarController()
                    return
                    
                    self.callApiGetInforLogin(uId: resultResponceLogin.uid ?? 0, companyId: resultResponceLogin.company_id ?? 0)
                }
            }
        }
    }
    
    func callApiGetInforLogin(uId: Int, companyId: Int)  {
        let params: [String: Any] =
            ["jsonrpc" : 2.0,
             "params": [
                "uID": uId
                ]
        ]
        BaseApiManager.getInforloginOdoo(params, showIndicator: true) { (datas, error) in
            if error != nil {
                print(error ?? "")
                KRProgressHUD.dismiss()
                self.view.makeToast(error?.localizedDescription, duration: 1, position: CSToastPositionCenter, style: nil)
            } else {
                // print(datas ?? ResultResponceLogin())
                if datas?.result == nil {
                    KRProgressHUD.dismiss()
                    self.view.makeToast("lbl_error_infor_accout".localized(), duration: 1, position: CSToastPositionCenter, style: nil)
                } else {
                    KRProgressHUD.dismiss()
                    
                    let resultResponceLogin = datas?.result?.records?.first ?? ResultResponceLogin()
                    resultResponceLogin.uid = uId
                    resultResponceLogin.company_id = companyId
                    UserDefaults.standard.save(customObject: resultResponceLogin, inKey: "loginModel")
                    
//                    let mainViewController = CreatNewOrderViewController(nibName: CreatNewOrderViewController.className, bundle: nil)
//                    let leftViewController = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
//                    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//                    let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
//                    UIApplication.shared.delegate?.window??.rootViewController = slideMenuController
                    
                    self.goToTabBarController()
                    
                }
            }
        }
    }
    
    private func goToTabBarController() {
        let tabBarStoryboard = UIStoryboard(name: TabBarController.className, bundle: nil)
        let tabBarController = tabBarStoryboard.instantiateViewController(withIdentifier: TabBarController.className)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = tabBarController
        appDelegate?.window?.makeKeyAndVisible()
    }
    
    
    @IBAction func onClickForgotPass(_ sender: Any) {
        
    }
    
}

extension UserDefaults {
    
    func save<T:Encodable>(customObject object: T, inKey key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }
    }
    
    func retrieve<T:Decodable>(object type:T.Type, fromKey key: String) -> T? {
        if let data = self.data(forKey: key) {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(type, from: data) {
                return object
            }else {
                print("Couldnt decode object")
                return nil
            }
        }else {
            print("Couldnt find key")
            return nil
        }
    }
    
}

